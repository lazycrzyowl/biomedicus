package edu.umn.biomedicus.ontology.mi;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.type.*;
import opennlp.tools.tokenize.TokenizerModel;
import org.apache.commons.math3.stat.Frequency;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;
import org.uimafit.descriptor.ConfigurationParameter;

import java.io.CharArrayReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

public class MIChunkAE extends CasAnnotator_ImplBase {
    // Parameter: path and name of parser model file.
    public static final String PARAM_GRAMMAR_FILE = "grammarFile";
    static String home = System.getenv("BIOMEDICUS_HOME");
    static final String defaultGrammarFile = Paths.get(home, "models/englishPCFG.ser.gz").toString();

    @NotNull
    private Type tokenType;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type chunkType;
    @NotNull
    private Type lexicalParseConstituentType;
    @NotNull
    private Feature posFeature;
    @NotNull
    private Feature parseFeature;
    @NotNull
    private Feature labelFeature;
    @NotNull
    private Feature chunkLabelFeature;
    private Feature stopwordFeature;
    private Frequency words;
    private Frequency bigrams;

    private HashSet<String> phraseLabels;
    private LexicalizedParser parser;
    private TreebankLanguagePack tlp;
    private GrammaticalStructureFactory grammarFactory;
    private String langPack = "edu.stanford.nlp.trees.PennTreebankLanguagePack";
    double phraseCount = 0;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
        words = new Frequency();
        bigrams = new Frequency();
    }

    @Override
    public void process(CAS cas) throws AnalysisEngineProcessException {
        CAS systemView = CASUtil.getSystemView(cas);
        Set<String> labels = new HashSet<>();
        labels.add("ADJP");
        labels.add("ADVP");
        labels.add("CONJP");
        labels.add("NP");
        labels.add("NP-TMP");
        labels.add("PP");
        labels.add("VP");
        labels.add("WHADVP");
        labels.add("WHNP");
        labels.add("PP");

        for (AnnotationFS phrase : AnnotationUtils.getAnnotations(systemView, Chunk.class))
        {
            phraseCount++;
            String type = phrase.getStringValue(chunkLabelFeature);
            String n_minus_1 = null;
            String n = null;
            for (AnnotationFS token : AnnotationUtils.getCoveredAnnotations(phrase, Token.class))
            {
                boolean stop = token.getBooleanValue(stopwordFeature);
                if (!stop) {
                    n = token.getCoveredText().toLowerCase();
                    words.addValue(n);

                    if (n_minus_1 != null)
                    {
                        List<String> bigram = new ArrayList();
                        String bigramString = n_minus_1 + " " + n;
                        bigrams.addValue(bigramString);
                    }
                    n_minus_1 = n;
                    System.out.print(token.getCoveredText());
                }
            }
            System.out.println();
        }
    }

    @Override
    public void collectionProcessComplete()
    {
        Iterator<Map.Entry<Comparable<?>, Long>> itor = bigrams.entrySetIterator();
        while (itor.hasNext())
        {
            Map.Entry entry = itor.next();
            String key = (String) entry.getKey();
            String[] bigram = entry.getKey().toString().split("\\s");
            Long freq = (Long) entry.getValue();
            double p_xy = freq/phraseCount;
            double p_x = words.getCount(bigram[0])/phraseCount;
            double p_y = words.getCount(bigram[1])/phraseCount;
            double mi = (p_xy) * logWithoutNaN(p_xy / (p_x * p_y));
            System.out.println(key + " | " + mi);

        }


    }

    public static double logWithoutNaN(double value) {
        if (value == 0) {
            return 0.000001; //epsilon
        } else if (value < 0) {
            return 0;
        }
        return Math.log(value);
    }

    public void typeSystemInit(TypeSystem ts) {
        tokenType = ts.getType(Token.class.getCanonicalName());
        sentenceType = ts.getType(Sentence.class.getCanonicalName());
        chunkType = ts.getType(Chunk.class.getCanonicalName());
        lexicalParseConstituentType = ts.getType(LexicalParseConstituent.class.getCanonicalName());
        posFeature = tokenType.getFeatureByBaseName("pos");
        stopwordFeature = tokenType.getFeatureByBaseName("isStopword");
        parseFeature = sentenceType.getFeatureByBaseName("parse");
        chunkLabelFeature = chunkType.getFeatureByBaseName("label");
    }

    private File loadGrammar(URL resource) throws ResourceInitializationException
    {
        try
        {
            File file = PathUtils.resourceToTempFile(resource);
            if (file == null)
                throw new IOException(String.format("Unable to access grammar file: %s", resource));
            return file;
        }
        catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
    }
}
