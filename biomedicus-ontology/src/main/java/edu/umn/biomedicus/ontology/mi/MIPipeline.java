package edu.umn.biomedicus.ontology.mi;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.CASUtil;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.uimafit.factory.TypeSystemDescriptionFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Family History Serial pipeline
 */
public class MIPipeline {

    private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<>();

    static
    {
        TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription(
                "./descriptors/types/TypeSystem.xml");
        TYPE_SYSTEM_DESCRIPTION.set(tsd);
    }

    private TypeSystemDescription tsd;
    private TypeSystem ts;

    /**
     * The pipeline created to process text.
     */
    private AnalysisEngine analysisEngine;

    /**
     * Creates a UIMA analysis engine.
     */
    public MIPipeline(String phraseType) throws InvalidXMLException, IOException, ResourceInitializationException
    {
        String descriptorPath = null;
        if (phraseType.equals("phrase"))
            descriptorPath = "edu/umn/biomedicus/ontology/mi/PhraseExtractorAE.xml";
        else
            descriptorPath = "edu/umn/biomedicus/ontology/mi/ChunkExtractorAE.xml";
        URL descriptorFileUrl = getClass().getClassLoader()
                .getResource(descriptorPath);
        File descriptorFile = new File(descriptorFileUrl.getFile());
        XMLInputSource descriptorSource = new XMLInputSource(descriptorFile);

        ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(
                descriptorSource);
        analysisEngine = UIMAFramework.produceAnalysisEngine(specifier);
        ts = analysisEngine.newCAS().getTypeSystem();
    }

    public TypeSystem getTypeSystem()
    {
        return ts;
    }

    /**
     * Uses the UIMA analysis engine to process the provided document text.
     */
    public CAS process(CAS aCAS) throws UIMAException
    {
        CAS mdView = CASUtil.getMetaDataView(aCAS);
        CAS sysView = CASUtil.getSystemView(aCAS);
        analysisEngine.typeSystemInit(aCAS.getTypeSystem());

        String newDocumentPath = mdView.getSofaDataString();
        analysisEngine.process(aCAS);
        return aCAS;
    }

    public void complete() throws AnalysisEngineProcessException
    {
        analysisEngine.collectionProcessComplete();
    }

    public CAS getCAS() throws ResourceInitializationException
    {
        return analysisEngine.newCAS();
    }
}