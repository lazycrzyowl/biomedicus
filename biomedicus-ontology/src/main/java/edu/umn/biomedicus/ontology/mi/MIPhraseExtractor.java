package edu.umn.biomedicus.ontology.mi;

import edu.umn.biomedicus.DefaultConfig;
import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.FamilyHistoryConstituent;
import org.apache.commons.cli.*;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.util.Properties;

import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

/**
 * Family History Annotation Application.
 */

public class MIPhraseExtractor {

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(MIPhraseExtractor.class);
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static String suffix;

    public MIPhraseExtractor(CollectionReader reader) throws IOException, UIMAException
    {

        // Get pipeline
        MIPipeline uimaPipeline = new MIPipeline("phrase");
        double start = System.currentTimeMillis();
        int documentCount = 0;

        CAS aCAS = uimaPipeline.getCAS();
        TypeSystem ts = aCAS.getTypeSystem();
        Type familyHistoryConstituentType = ts.getType(FamilyHistoryConstituent.class.getCanonicalName());

        while (reader.hasNext()) {
            aCAS.reset();
            reader.getNext(aCAS);

            CAS md = CASUtil.getMetaDataView(aCAS);
            String pathAndFilename = md.getSofaDataString();
            String filename = Paths.get(pathAndFilename).getFileName().toString();
            System.out.println("Processing " + filename);

            // Get gold constituents
            CAS gold = aCAS.getView(Views.GOLD_VIEW);

            uimaPipeline.process(aCAS);

            // Get system constituents
            CAS system = aCAS.getView(Views.SYSTEM_VIEW);
       }
        uimaPipeline.complete();

    }

    public static void main(String[] args) throws IOException, UIMAException, ConfigurationException
    {

        // Get general properties
        DefaultConfig _config = new DefaultConfig();
        System.out.println(_config.getString("biomedicus_cli_banner"));

        // Start logging
        logger.setLevel(Level.OFF);

        // Construction commandline options, get values, and validate what was supplied
        Options cliOptions = getCLIOptions();
        CommandLine cliValues = getCLIValues(cliOptions, args);
        String suffix = ".txt";

        //get Resource Specifier from XML file
        URL resource = Paths.get(args[0]).toUri().toURL();
        XMLInputSource in = new XMLInputSource(resource);
        ResourceSpecifier specifier =
                UIMAFramework.getXMLParser().parseResourceSpecifier(in);

        CollectionReader cReader =
                UIMAFramework.produceCollectionReader(specifier);

        // run
        MIPhraseExtractor app = new MIPhraseExtractor(cReader);
    }

    private static Options getCLIOptions()
    {
        // create the Options
        Options options = new Options();
        options.addOption("i", "input", true, "Input directory containing text files (*.txt).");
        options.addOption("d", "destination", true, "Destination directory to output xmi files.");
        options.addOption("s", "suffix", true, "Suffix/extension of input files to process (default=.txt)");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args)
    {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try
        {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e)
        {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }
}
