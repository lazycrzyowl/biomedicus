package edu.umn.biomedicus.ontology;

import edu.umn.biomedicus.core.featureranges.SentenceFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Chunk;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.CasConsumer_ImplBase;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceProcessException;
import org.apache.uima.resource.metadata.ProcessingResourceMetaData;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by bill0154 on 6/25/14.
 */
public class MutualInformationAE extends CasConsumer_ImplBase
{
    private static final String MI_MATRIX_FILE = "miMatrixFile";
    private static final String SPAN_TYPE_NAME = "spanTypeName";
    @NotNull private static final Logger logger = UIMAFramework.getLogger(MutualInformationAE.class);

    // Variables that hold parameter values
    @NotNull File miMatrixFile;
    @NotNull String spanTypeName;

    // Types used in class
    @NotNull Type spanType;
    Map<String, Type> typeMap = new HashMap<>();

    // Features used in class
    @NotNull private Feature tokenStopwordFeature;

    // Data collectors
    private Map<String, Double> cooccurrenceMatrix;
    private int n;


    // Resource metadata
    @NotNull ProcessingResourceMetaData md;

    @Override
    public void initialize()
    {
        // Get parameter values
        md = getProcessingResourceMetaData();
        miMatrixFile = (File) md.getAttributeValue(MI_MATRIX_FILE);
        spanTypeName = (String) md.getAttributeValue(SPAN_TYPE_NAME);

        // Coccurrence matrix
        cooccurrenceMatrix = new HashMap<String, Double>();

        // Open matrix file resource
        // TODO: matrix db writer goes here. data is too big otherwise
    }

    @Override
    public void processCas(CAS aCAS) throws ResourceProcessException {
        CAS sysCAS = CASUtil.getSystemView(aCAS);
        TypeSystem ts = sysCAS.getTypeSystem();
        spanType = ts.getType(spanTypeName);
        Type tokenType = typeMap.get(Token.class.getCanonicalName());
        List<String> collectedSpans = new ArrayList<>();
        for (AnnotationFS textSpan : AnnotationUtils.getAnnotations(sysCAS, spanType)) {
            collectedSpans.add(textSpan.getCoveredText());
        }

        MutualInformation mi = new MutualInformation(collectedSpans);

    }

    /**
     * Initialize the span types that will be used as the cooccurrence window.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) {

        String[] typeNames = {Token.class.getCanonicalName(),
                Chunk.class.getCanonicalName(),
                Sentence.class.getCanonicalName()};

        for (String type : typeNames)
        {
            typeMap.put(type, typeSystem.getType(type));
        }
        logger.log(Level.FINEST, "MI AE type system initialized.");
    }

}

