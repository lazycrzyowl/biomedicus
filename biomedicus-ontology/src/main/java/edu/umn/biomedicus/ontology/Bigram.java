package edu.umn.biomedicus.ontology;

public class Bigram {
    String n1;
    String n2;

    public Bigram(String n1, String n2)
    {
        this.n1=n1;
        this.n2=n2;
    }
}
