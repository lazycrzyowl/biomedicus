package edu.umn.biomedicus.ontology;

public class SemanticType {
    public String semanticGroupCode;
    public String semanticGroupName;
    public String code;
    public String abbrev;
    public String name;
}