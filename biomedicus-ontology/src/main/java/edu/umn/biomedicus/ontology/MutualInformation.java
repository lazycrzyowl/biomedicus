package edu.umn.biomedicus.ontology;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MutualInformation {
    private static final String digits = "1234567890";
    private static final double EPSILON = 0.000001;
    private Map<String, Double> cooccurrenceMatrix = new HashMap<String, Double>();
    private int n;
    private List<String> terms = new ArrayList<String>();

    public MutualInformation(List<String> textSpans){
        n = textSpans.size();

        for(int i = 0; i < textSpans.size(); i++){
            String[] line = textSpans.get(i).split(" ");
            for(String word: line){
                if(!containsDigits(word)){
                    if(!terms.contains(word)){
                        terms.add(word);
                    }
                }
            }
        }
    }

    private void getMutualInformation(List<String> textSpans){
        double n = this.n;
        double sum;

        for(int f1 = 0; f1 < terms.size()-1; f1++){
            sum = 0;
            for(int f2 = f1 + 1; f2 < terms.size(); f2++){
                Bigram bigram = new Bigram(terms.get(f1), terms.get(f2));

                //frequency of both terms
                double p_xy = containsXandY(bigram, textSpans) / n;

                //Fetch number of documents containing term.f1
                double p_x = containsX(bigram, textSpans) / n;

                //Fetch number of documents containing term.f2
                double p_y = containsY(bigram, textSpans) / n;

                sum += (p_xy) * logWithoutNaN(p_xy / (p_x * p_y));
            }
            cooccurrenceMatrix.put(terms.get(f1), sum);
        }
    }

    private double containsXandY(Bigram bigram, List<String> textSpans)
    {
        double frequency = 0;
        for (String span : textSpans)
        {
            if (span.contains(bigram.n1) && span.contains(bigram.n2)) frequency++;
        }
        return frequency;
    }

    private double containsX(Bigram bigram, List<String> textSpans)
    {
        double frequency = 0;
        for (String span : textSpans)
        {
            if (span.contains(bigram.n1)) frequency++;
        }
        return frequency;
    }

    private double containsY(Bigram bigram, List<String> textSpans)
    {
        double frequency = 0;
        for (String span : textSpans)
        {
            if (span.contains(bigram.n2)) frequency++;
        }
        return frequency;
    }

    private boolean containsDigits(String word)
    {
        for (char c : word.toCharArray())
        {
            if (digits.indexOf(c) != -1) return true;
        }
        return false;
    }

    public static double logWithoutNaN(double value) {
        if (value == 0) {
            return EPSILON;
        } else if (value < 0) {
            return 0;
        }
        return Math.log(value);
    }
}