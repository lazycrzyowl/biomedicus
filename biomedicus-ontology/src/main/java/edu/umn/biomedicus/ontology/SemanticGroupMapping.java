package edu.umn.biomedicus.ontology;

import au.com.bytecode.opencsv.CSVReader;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SemanticGroupMapping {

    @NotNull
    private List<String[]> entries;

    HashMap<String, SemanticType> lookupByTUI = new HashMap<>();
    HashMap<String, SemanticType> lookupByAbbrev = new HashMap<>();

    public SemanticGroupMapping() throws FileNotFoundException, IOException
    {
        URL dataInputURL = getClass().getClassLoader().getResource("edu/umn/biomedicus/ontology/SemGroupData.txt");
        FileReader dataFileReader = new FileReader(dataInputURL.getFile());
        CSVReader reader = new CSVReader(dataFileReader, '|');
        entries = reader.readAll();
        reader.close();
        for (String[] entry : entries)
        {
            SemanticType st = new SemanticType();
            st.semanticGroupCode = entry[0];
            st.semanticGroupName = entry[1];
            st.code = entry[2];
            st.abbrev = entry[3];
            st.name = entry[4];
            lookupByTUI.put(st.code, st);
            lookupByAbbrev.put(st.abbrev, st);
        }

    }

    public SemanticType getSemanticTypeForTUI(String tui)
    {
        SemanticType st = lookupByTUI.get(tui);
        return st;
    }

    public SemanticType getSemanticTypeForAbbrev(String abbrev)
    {
        SemanticType st = lookupByAbbrev.get(abbrev);
        return st;
    }

    public static void main(String[] args) throws Exception{
        //TODO: move to unit test
        SemanticGroupMapping sgm = new SemanticGroupMapping();
        System.out.println("Semantic group: " + sgm.getSemanticTypeForTUI("T048").name);
        System.out.println("Semantic group: " + sgm.getSemanticTypeForTUI("T019").name);
        System.out.println("Semantic group code: " + sgm.getSemanticTypeForTUI("T048").semanticGroupName);
        SemanticType st = sgm.getSemanticTypeForAbbrev("inpr");
        System.out.println("Semantic info by abbrev: " + st.name + ", " + st.semanticGroupName);
        System.out.println("Disorders group: ");
        for (SemanticType semType : sgm.getGroupMembers("Disorders"))
        {
            System.out.println("\t" + semType.name + ", " + semType.semanticGroupName);
        }
    }

    public List<SemanticType> getGroupMembers(String groupName)
    {
        ArrayList<SemanticType> groupMembers = new ArrayList<>();
        for (Map.Entry<String, SemanticType> entry : lookupByTUI.entrySet())
        {
            if (entry.getValue().semanticGroupName.equals(groupName))
            {
                groupMembers.add(entry.getValue());
            }

        }
        return groupMembers;
    }
}
