
abbrevsFile = open("SemAbbrevs.txt", 'r')
typesFile = open("SemGroups.txt",'r')

abbr = {}
for l in abbrevsFile.xreadlines():
    code, abbrev = l.strip().split()
    abbr.setdefault(code, abbrev)

del l
for l in typesFile.xreadlines():
    gui, gname, tui, tname = l.strip().split("|")
    abbrev = abbr[tui]
    print "|".join((gui, gname, tui, abbrev, tname))
