package edu.umn.biomedicus.segmenting;


import edu.umn.biomedicus.core.utils.ContinuousSpan;
import opennlp.tools.util.Span;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class SplitRuleTest {

    @Test
    public void testSplitRule() {
        Pattern pat = Pattern.compile("\n\n");
        SplitRule sr = new SplitRule(pat, 1);
        String sampleText = "HEADINGS:\n\nOften   \n\n   have two newlines after them. Some other text";
        String[] expectedResults = {"HEADINGS:", "Often", "have two newlines after them."};
        List<Span> exampleList = new ArrayList<>();
        exampleList.add(new Span(0, 54));
        List<Span> post = sr.apply(exampleList, sampleText);
        for (int i = 0; i < post.size(); i++)
        {
            Span result = post.get(i);
            assertEquals(expectedResults[i], sampleText.substring(result.getStart(), result.getEnd()));
        }
    }
}
