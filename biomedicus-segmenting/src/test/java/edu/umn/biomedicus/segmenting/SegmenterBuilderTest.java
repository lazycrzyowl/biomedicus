/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.segmenting;

import edu.umn.biomedicus.segmenting.services.OpenNLPSegmentingServiceProvider;
import edu.umn.biomedicus.segmenting.services.StanfordSegmentingServiceProvider;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple SentenceSegmenter.
 */
public class SegmenterBuilderTest {

    @Test
    public void testOpenNLPSegmenterBuilder() throws Exception
    {
        SentenceSegmenter segmenter = new SentenceSegmenter.Builder()
                .service(OpenNLPSegmentingServiceProvider.class)
                .model("edu/umn/biomedicus/segmenting/mts-sent.bin")
                .build();

        assertNotNull(segmenter);

        String[] sentences = segmenter.process("This is a test. And, so is this one.");
        assertTrue(sentences.length == 2);
    }

    @Test
    public void testStanfordSentenceSegmenterBuilder() throws Exception{

        SentenceSegmenter segmenter = new SentenceSegmenter.Builder()
                .service(StanfordSegmentingServiceProvider.class)
                .build();

        assertNotNull(segmenter);
        String[] sentences = segmenter.process("This is a test. And, so is this one.");
        assertTrue(sentences.length == 2);
    }
}
