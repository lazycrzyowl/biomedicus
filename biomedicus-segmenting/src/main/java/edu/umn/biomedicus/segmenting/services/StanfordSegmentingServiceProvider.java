/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.segmenting.services;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.PTBEscapingProcessor;
import edu.stanford.nlp.process.WordToSentenceProcessor;
import edu.umn.biomedicus.core.featureranges.DocumentFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StanfordSegmentingServiceProvider
        implements SegmentingServiceProvider, SharedResourceObject
{


    @Nullable
    private List<Span> results = new ArrayList<>();

    @Override
    public StanfordSegmentingServiceProvider analyze(String documentText) {
        runAnalysis(documentText, null);
        return this;
    }

    private Span[] runAnalysis(String documentText, @NotNull CAS view) {
        List<Span> localResults = new ArrayList<>();
        List<CoreLabel> tokens = new DocumentFeatureRange(view).getCoreLabelTokens();
        new PTBEscapingProcessor<CoreLabel, String, String>().apply(tokens);

        WordToSentenceProcessor<CoreLabel> segmenter = new WordToSentenceProcessor<>();
        for (List<CoreLabel> sentence : segmenter.process(tokens)) {
            int begin = sentence.get(0).get(CoreAnnotations.CharacterOffsetBeginAnnotation.class);
            int end = sentence.get(sentence.size() - 1).get(CoreAnnotations.CharacterOffsetEndAnnotation.class);
            Span span = new Span(begin, end);
            localResults.add(span);
            if (view != null) {
                annotate(span, view, localResults);
            }
        }
        Span[] sentenceArray = new Span[localResults.size()];
        return localResults.toArray(sentenceArray);
    }

    @Override
    public AnnotationFS[] annotateCAS(CAS view) {
        if (results == null) {
            return null;
        }
        List<AnnotationFS> annotationList = new ArrayList<>();
        for (Span span : results) {
            AnnotationFS annot = AnnotationUtils.createAnnotation(view, Token.class.getCanonicalName(), span.getStart(),
                    span.getEnd());
            annotationList.add(annot);
        }
        AnnotationFS[] annotArray = new AnnotationFS[annotationList.size()];
        return annotationList.toArray(annotArray);
    }

    @Override
    public void annotateCAS(CAS view, String documentText) {
        runAnalysis(documentText, view);
    }

    @Override
    public void load(DataResource aData) throws ResourceInitializationException {
        // nothing to load for Stanford.
    }

    private AnnotationFS annotate(edu.umn.biomedicus.core.utils.Span span, CAS view, List<Span> resultsList) {
        // Split titles as separate sentences
        Pattern titles = Pattern.compile("^[A-Z ]{5,}:", Pattern.MULTILINE);
        String coveredText = view.getDocumentText().substring(span.getStart(), span.getEnd());
        Matcher matcher = titles.matcher(coveredText);
        int begin = span.getStart();
        AnnotationFS annot = null;
        int findStart = 0;
        while (matcher.find(findStart)) {
            int end = matcher.end() + begin;
            findStart = end - span.getStart();
            resultsList.add(new Span(begin, end));
            AnnotationUtils.createAnnotation(view, Sentence.class, begin, end);

            if (end < span.getEnd())
                for (int i = end; i < span.getEnd(); i++) {
                    char c = coveredText.charAt(i - begin);
                    if (Character.isLetterOrDigit(c)) {
                        AnnotationUtils.createAnnotation(view, Sentence.class, i, span.getEnd());
                        break;
                    }
                }
        }
        if (findStart == 0)
        {
            AnnotationUtils.createAnnotation(view, Sentence.class.getCanonicalName(),
                    span.getStart(), span.getEnd());
        }
        return annot;
    }

    public Span[] getSpans() {
        if (results == null) {
            return null;
        }
        Span[] spanArray = new Span[results.size()];
        return results.toArray(spanArray);
    }
}
