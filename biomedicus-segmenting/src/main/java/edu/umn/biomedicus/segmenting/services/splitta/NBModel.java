package edu.umn.biomedicus.segmenting.services.splitta;

/**
 * Naive Bayes model, with a few tweaks:
 * <ul><li></li>all feature types are pooled together for normalization (this might help
 * because the independence assumption is so broken for our features)</li>
 * <li>smoothing: add 0.1 to all counts</li>
 * <li>priors are modified for better performance (this is mysterious but works much better)</li>
 * </ul>
 */
public class NBModel extends Model {


}


/*
    @NotNull
    String path;

    public NBModel(String path)
    {
        this.path = path;
    }

    public void train(Document doc)
    {

        HashMap feats = new Counter();
        HashMap totals = new Counter();

        Frag frag = doc.frag;
        while (frag != null) {

            // "entry" is the feat, value tuple
            for (HashMap entry : frag.features.items())
            {
                feats[frag.label][feat + '_' + val] += 1;
            }
            totals.put(frag.label, totals.get(frag.label) += frag.features.length()));
            frag = frag.next;
        }
        // add-1 smoothing and normalization
        System.err.println('smoothing... ');
        double smooth_inc = 0.1 ;
        HashSet all_feat_names = new HashSet(feats[true].keys()).union(new HashSet(feats[false].keys()));
        for (int label : [0,1]) {
            totals[label] += (len(all_feat_names) * smooth_inc)
            for (feat : all_feat_names) {
                feats[label][feat] += smooth_inc;
                feats[label][feat] /= totals[label]
                self.feats[(label, feat)] = feats[label][feat]
            }
        feats[label]['<prior>'] = totals[label] / totals.totalCount()
        self.feats[(label, '<prior>')] = feats[label]['<prior>']

        System.err.println("done!");
    }

}
*/