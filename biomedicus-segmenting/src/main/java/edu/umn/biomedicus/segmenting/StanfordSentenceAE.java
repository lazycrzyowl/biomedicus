package edu.umn.biomedicus.segmenting;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.PTBEscapingProcessor;
import edu.stanford.nlp.process.WordToSentenceProcessor;
import edu.umn.biomedicus.core.featureranges.ContinuousSpan;
import edu.umn.biomedicus.core.featureranges.DocumentFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StanfordSentenceAE extends CasAnnotator_ImplBase {


    public void process(CAS cas) {
        CAS view = CASUtil.getSystemView(cas);
        String documentText = view.getDocumentText();

        List<ContinuousSpan> localResults = new ArrayList<>();
        List<CoreLabel> tokens = new DocumentFeatureRange(view).getCoreLabelTokens();
        new PTBEscapingProcessor<CoreLabel, String, String>().apply(tokens);

        WordToSentenceProcessor<CoreLabel> segmenter = new WordToSentenceProcessor<>();
        for (List<CoreLabel> sentence : segmenter.process(tokens)) {
            int begin = sentence.get(0).get(CoreAnnotations.CharacterOffsetBeginAnnotation.class);
            int end = sentence.get(sentence.size() - 1).get(CoreAnnotations.CharacterOffsetEndAnnotation.class);
            ContinuousSpan span = new ContinuousSpan(begin, end);
            localResults.add(span);
        }
        ContinuousSpan[] candidates = localResults.toArray(new ContinuousSpan[localResults.size()]);
        ContinuousSpan[] splitSpans = splitSymbols(candidates, documentText, ";".toCharArray());
        for (ContinuousSpan candidate : splitSpans)
        {
            if (view != null) {
                annotate(candidate, view, localResults);
            }
        }
    }

    public AnnotationFS annotate(ContinuousSpan span, CAS view, List<ContinuousSpan> resultsList) {
        // Split titles as separate sentences
        Pattern titles = Pattern.compile("^[A-Z ]*:");
        String coveredText = view.getDocumentText().substring(span.getStart(), span.getEnd());
        Matcher matcher = titles.matcher(coveredText);
        int begin = span.getStart();
        AnnotationFS annot = null;
        if (matcher.find()) {
            int end = matcher.end() + begin;
            resultsList.add(new ContinuousSpan(begin, end));
            AnnotationUtils.createAnnotation(view, Sentence.class, begin, end);

            if (end < span.getEnd())
                for (int i = end; i < span.getEnd(); i++) {
                    char c = coveredText.charAt(i - begin);
                    if (Character.isLetterOrDigit(c)) {
                        AnnotationUtils.createAnnotation(view, Sentence.class, i, span.getEnd());
                        break;
                    }
                }
        } else {
            AnnotationUtils.createAnnotation(view, Sentence.class.getCanonicalName(),
                    span.getStart(), span.getEnd());
        }
        return annot;
    }

    /**
     * This method is a way to identify uncommon symbols that should indicate a token boundary.
     * Because many of the corpora did not have enough annotations for some symbols (e.g., "|"),
     * this is used to force a split when there is insufficient counts for MaxEnt to identify
     * the token boundary.
     *
     * @param candidates An array of OpenNLP Spans produced from the maxent tokenizer.
     * @param text       The original text being tokenized.
     * @return A ContinuousSpan array.
     */
    private ContinuousSpan[] splitSymbols(ContinuousSpan[] candidates, String text, char[] splitCharacters) {
        List<ContinuousSpan> spanList = new ArrayList<>();

        for (ContinuousSpan span : candidates) {
            String coveredText = text.substring(span.getStart(), span.getEnd());

            List<Integer> splitIndexes = new ArrayList<>();
            for (char splitChar : splitCharacters) {
                for (int index = coveredText.indexOf(splitChar);
                     index >= 0;
                     index = coveredText.indexOf(splitChar, index + 1))
                {
                    splitIndexes.add(span.getStart() + index);
                }
            }

            if (splitIndexes.size() > 0)
            {
                ContinuousSpan[] currentCandidate = new ContinuousSpan[1];
                currentCandidate[0] = span;
                ContinuousSpan[] splitSpans = splitIndexes(currentCandidate, splitIndexes);
                Collections.addAll(spanList, splitSpans);
            }
            else
            {
                spanList.add(new ContinuousSpan(span.getStart(), span.getEnd()));
            }
        }
        return spanList.toArray(new ContinuousSpan[spanList.size()]);
    }

    /**
     * This method is a way to identify uncommon symbols that should indicate a token boundary.
     * Because many of the corpora did not have enough annotations for some symbols (e.g., "|"),
     * this is used to force a split when there is insufficient counts for MaxEnt to identify
     * the token boundary.
     *
     * @param candidates An array of OpenNLP Spans produced from the maxent tokenizer.
     * @param splitIndexes
     *     A List of integers that identify the indexes the tokenizer should identify as separate tokens.
     * @return A ContinuousSpan array.
     */
    private ContinuousSpan[] splitIndexes(ContinuousSpan[] candidates, List<Integer> splitIndexes) {
        Collections.sort(splitIndexes);
        List<ContinuousSpan> spanList = new ArrayList<>();
        for (ContinuousSpan span : candidates) {
            int begin = span.getStart();
            int end = span.getEnd();
            for (int i = begin; i < end; i++) {

                if (splitIndexes.contains(i)) {
                    // Add span from beginning, if not at beginning
                    if (i - begin > 0) spanList.add(new ContinuousSpan(begin, i));

                    // Add the single character at the target index.
                    // The assumption is that the index contains a symbol that is a token.
                    spanList.add(new ContinuousSpan(i, i + 1));
                    begin = i + 1;
                }
            }
            if (end - begin > 0) spanList.add(new ContinuousSpan(begin, end));
        }
        return spanList.toArray(new ContinuousSpan[spanList.size()]);
    }
}
