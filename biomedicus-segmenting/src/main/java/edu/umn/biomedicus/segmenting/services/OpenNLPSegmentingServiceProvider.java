/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.segmenting.services;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.type.Sentence;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.Span;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OpenNLPSegmentingServiceProvider
        implements SegmentingServiceProvider, SharedResourceObject {


    @Nullable
    private SentenceModel sentenceModel;
    @NotNull
    private SentenceDetectorME sentenceDetector;
    @Nullable
    private Span[] results;

    public void annotateCAS(CAS view, String documentText)
    {
        List<opennlp.tools.util.Span> spanList = new ArrayList<>();
        Pattern pat = Pattern.compile("[A-Z ]*:"); // define a title pattern

        Span[] sentenceCandidates = sentenceDetector.sentPosDetect(documentText);
        for (opennlp.tools.util.Span span : sentenceCandidates)
        {
            AnnotationFS annot = null;
            String coveredText = documentText.substring(span.getStart(), span.getEnd());
            Matcher matcher = pat.matcher(coveredText);
            matcher.find();
            if (matcher.matches())
            {
                int i = 0;
                int start = span.getStart();
                for (char c : coveredText.toCharArray())
                {
                    if (i == matcher.start())
                    {
                        int end = matcher.end();
                        opennlp.tools.util.Span newSpan = new opennlp.tools.util.Span(start, end);
                        spanList.add(newSpan);
                        annot = AnnotationUtils.createAnnotation(view, Sentence.class, start, end);

                        i = end + 1;
                    }
                    else
                    {
                        if (Character.isLetterOrDigit(c))
                        {
                            opennlp.tools.util.Span newSpan = new opennlp.tools.util.Span(start, span.getEnd());
                            spanList.add(newSpan);
                            annot = AnnotationUtils.createAnnotation(view, Sentence.class,
                                    newSpan.getStart(), newSpan.getEnd());
                            i = span.getEnd();
                        }
                    }
                    i++;
                }
                if (start < span.getEnd())
                {
                    spanList.add(new opennlp.tools.util.Span(start, span.getEnd()));
                    annot = AnnotationUtils.createAnnotation(view, Sentence.class, start, span.getEnd());
                    view.addFsToIndexes(annot);
                }
            }
            else {
                spanList.add(span);
                annot = AnnotationUtils.createAnnotation(view, Sentence.class, span.getStart(), span.getEnd());
            }
        }

        opennlp.tools.util.Span[] spanArray = new opennlp.tools.util.Span[spanList.size()];
        results = spanList.toArray(spanArray);

    }

    @Override
    public SegmentingServiceProvider analyze(String documentText)
    {
        results = sentenceDetector.sentPosDetect(documentText);
        return this;
    }

    @Override
    public edu.umn.biomedicus.core.utils.Span[] getSpans()
    {
    	if (results != null) {
    		edu.umn.biomedicus.core.utils.Span[] bcusSpans = new edu.umn.biomedicus.core.utils.Span[results.length];
    	
    		for (int i = 0; i < results.length; i++)
    		{
    			bcusSpans[i] = new edu.umn.biomedicus.core.utils.Span(results[i].getStart(), results[i].getEnd());
    		}
    		return bcusSpans;
    	}
    	else
    	{
    		return null;
    	}
    }

    @Override
    public AnnotationFS[] annotateCAS(CAS view)
    {
    	if (results != null)
    	{
    		List<AnnotationFS> annotationList = new ArrayList<>();
    		for (Span span : results) {
    			AnnotationFS annot = AnnotationUtils.createAnnotation(view, Sentence.class,
    					span.getStart(), span.getEnd());
    			annotationList.add(annot);
    		}
    		AnnotationFS[] annotArray = new AnnotationFS[annotationList.size()];
    		return annotationList.toArray(annotArray);
    	}
    	else
    	{
    		return null;
    	}
    }

    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {
        URL resource = aData.getUrl();
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        }
        catch (IOException e)
        {throw new ResourceInitializationException(e);}

        try
        {
            sentenceModel = new SentenceModel(new FileInputStream(file));
            if (sentenceModel == null) throw new IOException("Failed to load sentence model.");
        }
        catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
        sentenceDetector = new SentenceDetectorME(sentenceModel);
    }
}
