package edu.umn.biomedicus.segmenting.opennlp;


import edu.umn.biomedicus.core.utils.ContinuousSpan;
import edu.umn.biomedicus.segmenting.SplitRule;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.Span;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class MaxEntSentenceSplitter {

    @NotNull private SentenceDetectorME sentenceDetector;
    private List<SplitRule> splitRules;
    private String[] abbrevs;

    private MaxEntSentenceSplitter(SentenceModel sentenceModel)
            throws IOException
    {
        sentenceDetector = new SentenceDetectorME(sentenceModel);
        loadDefaultAbbreviations();

        // Split rules
        splitRules = new ArrayList<>();
        splitRules.add(new SplitRule(Pattern.compile("\\n\\n"), 1));
        splitRules.add(new SplitRule(Pattern.compile(";"), 1));
        splitRules.add(new SplitRule(Pattern.compile("[:;]\\s*\\n"), 1));
        splitRules.add(new SplitRule(Pattern.compile("\\s{4,}"), 0));
    }

    private void loadDefaultAbbreviations()
    {
        abbrevs = new String[]{"Mr\\.", "Ms\\.", "Mrs\\.", "St\\.", "p\\.o\\.", "p\\.i\\.d\\.", "q\\.a\\.m\\.", "q\\.h\\.s\\."};
    }

    private String mutate(String documentText)
    {
        // Change abbreviations that should have a reduced weight for being sent boundaries.
        for (String abbrev : abbrevs)
        {
            String replacement = abbrev.replaceAll("\\.", "_").replaceAll("\\\\","");
            documentText = documentText.replaceAll(abbrev, replacement);
        }

        // Medication dosage rules
        // Using text substitutions to reduce odds of being detected as a sentence boundary.
        // Note:  The regex match length must always be the same as the replacement string
        documentText = documentText.replaceAll("b\\.i\\.d\\.\\s(?![\n$0-9])", "b_i_d_ ");
        documentText = documentText.replaceAll("p\\.r\\.n\\.\\s(?![\n$0-9])", "p_r_n_ ");
        documentText = documentText.replaceAll("t\\.i\\.d\\.\\s(?![\n$0-9])", "t_i_d_ ");
        documentText = documentText.replaceAll("q\\.[0-9][hH]\\.\\s(?![\n$0-9])", "q_4h_ ");
        documentText = documentText.replaceAll("q\\.[0-9]{2}[hH]\\.\\s(?![\n$0-9])", "q_44h_ ");
        documentText = documentText.replaceAll("q\\.a\\.m\\.\\s(?![\n$0-9])", "q_a_m_ ");
        documentText = documentText.replaceAll("q\\.p\\.m\\.\\s(?![\n$0-9])", "q_p_m_ ");
        documentText = documentText.replaceAll("q\\.h\\.s\\.\\s(?![\n$0-9])", "q_h_s_ ");
        documentText = documentText.replaceAll("q\\.d\\.\\s(?![\n$0-9])", "q_d_ ");

        return documentText;
    }

    public static MaxEntSentenceSplitter buildFromModel(File modelFile) throws IOException {
        SentenceModel sentenceModel = new SentenceModel(new FileInputStream(modelFile));
        return new MaxEntSentenceSplitter(sentenceModel);
    }

//    /**
//     * Calls the MaxEnt sentPosDetect method.
//     *
//     * @param text The text to split sentences.
//     * @return A Span[] array designating all token begin/end points.
//     */
//    public Span[] split(String text)
//    {
//        return sentenceDetector.sentPosDetect(text);
//    }

    /**
     * Converts an OpenNLP Span array to ContinuousSpan array
     *
     * @param spans The Span array produced from the MaxEnt sentence splitter.
     * @return A ContinuousSpan array equivalent to the OpenNLP Span array in the parameter.
     */
    public List<ContinuousSpan> toContinuousSpans(Span[] spans) {
        List<ContinuousSpan> results = new ArrayList<>(spans.length);
        for (int i = 0; i < spans.length; i++)
            results.add(new ContinuousSpan(spans[i].getStart(), spans[i].getEnd()));
        return results;
    }

    /**
     * Calls the MaxEnt sentPosDetect method and splits sentence candidates on post-proccessing rules.
     *
     * @param text
     * The original document text.
     * @return
     * A ContinuousSpan array of sentences after the rules have been applied.
     */
    public List<Span> split(String text) {
        String mutatedText = mutate(text);
        Span[] candidates = sentenceDetector.sentPosDetect(mutatedText);
        return splitOnRules(candidates, mutatedText, splitRules);
    }

    /**
     * Calls the MaxEnt sentPosDetect method and then splits token candidates at each
     * of the index locations provided in the splitSymbolIndexes parameter. The
     * reason for this method is that a separate process ran previously
     * could have generated a list of split locations (e.g., a
     * symbol disambiguation module). This method is a convenient way to
     * incorporate that information into sentence splitting.
     *
     * @param text               The original text to split.
     * @param splitIndexes An int[] array of indexes that should be split as single-character tokens.
     * @return spanList
     * A ContinuousSpan array of sentence spans after splitting on parameter indexes.
     */
    public Span[] split(String text, Integer[] splitIndexes) {
        List<Span> candidates = split(text);
        return splitIndexes(candidates, splitIndexes, text);
    }

    /**
     * This method is a way to identify patterns that should indicate a sentence boundary.
     *
     * @param candidates An array of OpenNLP Spans produced from the maxent sentence detector.
     * @param text       The original text being split.
     * @return A ContinuousSpan array.
     */
    private List<Span> splitOnRules(Span[] candidates, String text, List<SplitRule> rules) {
        List<Span> spanList = new ArrayList<>();

        for (Span span : candidates) {
            List<Span> workingList = new ArrayList<>();
            workingList.add(span);

            // Get substring to avoid wasted effort with out-of-bounds regex matches.
            String workingTextRange = text.substring(0, span.getEnd());
            for (SplitRule splitter : rules) {
                workingList = splitter.apply(workingList, workingTextRange);
            }
            spanList.addAll(workingList);
        }
        return spanList;
    }

    /**
     * This method is a way to identify uncommon symbols that should indicate a sentence boundary.
     * Because many of the corpora did not have enough annotations for some symbols (e.g., "|"),
     * this is used to force a split when there is insufficient counts for MaxEnt to identify
     * the token boundary.
     *
     * @param candidates An array of OpenNLP Spans produced from the maxent sentence detector.
     * @param splitIndexes
     *     A List of integers that identify the indexes the sentence detector should identify as sentence boundaries.
     * @return A Span array.
     */
    private Span[] splitIndexes(List<Span> candidates, Integer[] splitIndexes, String documentText) {
        Arrays.sort(splitIndexes);
        List<Integer> splitIndexesList = Arrays.asList(splitIndexes);
        List<Span> spanList = new ArrayList<>();
        for (Span span : candidates) {
            int begin = span.getStart();
            int end = span.getEnd();
            for (int i = begin; i < end; i++) {

                if (splitIndexesList.contains(i)) {
                    // Add span from beginning, if not at beginning
                    if (i - begin > 0) spanList.add(new Span(begin, i));

                    while (Character.isWhitespace(documentText.charAt(i))) {
                        if (i < documentText.length()) i++;
                    }
                    begin = i;
                    if (begin <= documentText.length() && end - begin > 0)
                        span = new Span(begin, end);
                }
                else {
                    spanList.add(new Span(span.getStart(), span.getEnd()));
                }
            }
            if (end - begin > 0) spanList.add(new Span(begin, end));
        }
        return spanList.toArray(new Span[spanList.size()]);
    }
}