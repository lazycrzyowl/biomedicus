package edu.umn.biomedicus.segmenting;

import edu.umn.biomedicus.core.utils.ContinuousSpan;
import opennlp.tools.util.Span;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SplitRule {
    Pattern pattern;
    int offset;

    public SplitRule(Pattern pattern, int offset)
    {
        this.pattern = pattern;
        this.offset = offset;
    }

    public List<Span> apply(List<Span> workingList, String sourceText) {
        List<Span> results = new ArrayList<>();
        for (Span span : workingList) {
            results.addAll(splitSpan(span, sourceText));
        }
        return results;
    }

    public List<Span> splitSpan(Span span, String sourceText)
    {
        List<Span> results = new ArrayList<>();
        Matcher m = pattern.matcher(sourceText);
        int testLen = sourceText.length();
        int previousStart = -1;
        int start = span.getStart();
        int spanEnd = span.getEnd();
        String coveredText = sourceText.substring(start, spanEnd);
        for (boolean find = m.find(start); find != false; find = m.find(start))
        {
            if (start == previousStart)
            {
                if (start + 1 < spanEnd) start++;
                else break;
            }
            previousStart = start;
            int candidateEnd = trimWhitespace(m.start() + offset, sourceText);

            if (candidateEnd < spanEnd)
            {
                results.add(new Span(start, candidateEnd));
                start = skipWhitespace(m.start() + offset, sourceText);
                if (start >= sourceText.length()) break;
            }
            else
            {
                break;
            }
        }
        int finalStart = skipWhitespace(start, sourceText);
        int finalEnd = trimWhitespace(spanEnd, sourceText);
        if (finalStart < finalEnd) // match above fails, but there is text following the previous sentence
            results.add(new Span(start, trimWhitespace(spanEnd, sourceText)));

        return results;
    }

    public int trimWhitespace(int index, String s)
    {
        while (Character.isWhitespace(s.charAt(index - 1))) index--;
        return index;
    }

    public int skipWhitespace(int index, String s)
    {
        if (index > s.length() - 1) return s.length() -1;
        while (Character.isWhitespace(s.charAt(index)))
        {
            index++;
            if (index > s.length() -1) return s.length() - 1;
        }
        return index;
    }
}
