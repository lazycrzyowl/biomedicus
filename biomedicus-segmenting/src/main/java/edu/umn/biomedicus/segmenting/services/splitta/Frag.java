package edu.umn.biomedicus.segmenting.services.splitta;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A fragment of text that ends with a possible sentence boundary
 */
public class Frag {

    @NotNull
    String orig;
    @Nullable
    Frag next = null;
    @NotNull
    boolean endsSeg = false;
    @NotNull
    boolean tokenized = false;
    @Nullable
    String pred = null;
    String label = null;
    String features = null;

    public Frag(String orig)
    {
        this.orig = orig;
        this.next = null;
        this.endsSeg = false;
        this.tokenized = false;
        this.pred = null;
        this.label = null;
        this.features = null;
    }

    public String toString()
    {
        String s = orig;
        if (endsSeg) s += " <EOS> ";
        return s;
    }


    /*
    public Model buildModel(List<String> files, Options options)
    {
        // create a Doc object from some labeled data
        train_corpus = get_data(files, tokenize=options.tokenize)

        // create a new model
        Model model = ModelFactory.getModel(ModelFactory.AlgorithmType.NB, options.model_path);
        model.prep(train_corpus);

        // featurize the training corpus
        train_corpus.featurize(model, verbose=True);

        // run the model's training routine
        model.train(train_corpus);

        // save the model
        model.save();
        return model;
    }

    public load_sbd_model(model_path = "model_nb/", use_svm=False)   {
            sys.stderr.write("loading model from [%s]... " %model_path)
            if use_svm: model = SVM_Model(model_path)
    else: model = NB_Model(model_path)
    model.load()
            sys.stderr.write("done!\n")
            return model

    public sbd_text(model, text, do_tok=True)   {
            """
    A hook for segmenting text in Python code:

            import sbd
            m = sbd.load_sbd_model("/u/dgillick/sbd/splitta/test_nb/", use_svm=False)
    sents = sbd.sbd_text(m, "Here is. Some text")
            """

    data = get_text_data(text, expect_labels=False, tokenize=True)
    data.featurize(model, verbose=False)
            model.classify(data, verbose=False)
    sents = data.segment(use_preds=True, tokenize=do_tok, list_only=True)
            return sents

*/
}



