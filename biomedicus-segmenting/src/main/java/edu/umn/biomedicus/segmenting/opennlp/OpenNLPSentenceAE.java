/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.umn.biomedicus.segmenting.opennlp;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.core.utils.ContinuousSpan;
import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.segmenting.SplitRule;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.Span;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class OpenNLPSentenceAE extends CasAnnotator_ImplBase {

    public static final String PARAM_MODEL_FILE = "modelFile";
    public static final String PARAM_ABBREVIATIONS = "abbreviations";
    private static final Logger logger = UIMAFramework.getLogger(OpenNLPSentenceAE.class);
    private String[] abbreviations;
    @NotNull private Type sentenceType;
    @NotNull private char[] splitCharacters = "|" .toCharArray();
    @NotNull private MaxEntSentenceSplitter sentenceSplitter;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);

        String modelFileName = (String) context.getConfigParameterValue(PARAM_MODEL_FILE);
        abbreviations = (String[]) context.getConfigParameterValue(PARAM_ABBREVIATIONS);
        URL resource = this.getClass().getClassLoader().getResource(modelFileName);
        File modelFile = loadModelFile(resource);

        try {
            sentenceSplitter = MaxEntSentenceSplitter.buildFromModel(modelFile);
        } catch (IOException e) {
            throw new ResourceInitializationException(e);
        }

        if (sentenceSplitter == null)
            throw new ResourceInitializationException(new Throwable("Failed to initialize OpenNLP sentence detector."));
        logger.log(Level.FINEST, "Initialization successful");
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        @NotNull CAS sysCAS = CASUtil.getSystemView(aCAS);
        @NotNull String documentText = sysCAS.getDocumentText();

        List<Span> results = sentenceSplitter.split(documentText);
        for (Span span : results) {
            if (span.getStart()==-1 ||span.getEnd()==-1)
            {
                String error = "error";
            } else {
                AnnotationUtils.createAnnotation(sysCAS, Sentence.class, span.getStart(), span.getEnd());
            }
        }
        logger.log(Level.FINEST, "Document sentence splitting complete.");
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {
        super.typeSystemInit(typeSystem);
        sentenceType = typeSystem.getType(Token.class.getCanonicalName());
        logger.log(Level.FINEST, "OpenNLPTokenAE typeSystem initialization successful.");
    }

    private File loadModelFile(URL resource) throws ResourceInitializationException {
        try {
            File file = PathUtils.resourceToTempFile(resource);
            if (file == null)
                throw new IOException(String.format("Unable to access openNLP sentenceSplitter model file: %s", resource));
            return file;
        } catch (IOException e) {
            throw new ResourceInitializationException(e);
        }
    }

    private SentenceModel loadSentenceModel(File modelFile) throws ResourceInitializationException {
        try {
            SentenceModel model = new SentenceModel(new FileInputStream(modelFile));
            if (model == null)
                throw new IOException("Failed to load tokenizer model from model file.");
            return model;
        } catch (IOException e) {
            throw new ResourceInitializationException(e);
        }
    }
}

