/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.segmenting;

import edu.umn.biomedicus.segmenting.services.OpenNLPSegmentingServiceProvider;
import edu.umn.biomedicus.segmenting.services.StanfordSegmentingServiceProvider;

import static org.junit.Assert.assertTrue;

public class ExampleCode {

    static String test = "Segmenting sentences requires that acronyms/abbreviations do not cause " +
            "false sentence boundaries. Additionally, section titles with unusual punctuation/capitalization " +
            "should be segmented separately from the sentence that follows them because they " +
            "interefere with syntactic processing later in the pipeline. Prescriptions and metabolic scores " +
            "tend to create the most problems, such as prescribed zoloft c.a. p.i.d., with amitriptyline. " +
            "HISTORY OF PRESENT ILLNESS: The patient reports congestion since last Monday.";

    static String[] expectedResults = new String[] {
            "Segmenting sentences requires that acronyms/abbreviations do not cause false sentence boundaries.",
            "Additionally, section titles with unusual punctuation/capitalization should be segmented " +
                    "separately from the sentence that follows them because they interefere with " +
                    "syntactic processing later in the pipeline.",
            "Prescriptions and metabolic scores tend to create the most problems, such as prescribed " +
                    "zoloft c.a. p.i.d., with amitriptyline.",
            "HISTORY OF PRESENT ILLNESS:",
            "The patient reports congestion since last Monday." };

    public static void main(String[] args) throws Exception
    {
                SentenceSegmenter segmenter = new SentenceSegmenter.Builder()
                        .service(OpenNLPSegmentingServiceProvider.class)
                        .model("edu/umn/biomedicus/segmenting/mts-sent.bin")
                        .build();

                String[] results = segmenter.process(test);
                System.out.println(match(results, expectedResults));


                segmenter = new SentenceSegmenter.Builder()
                        .service(StanfordSegmentingServiceProvider.class)
                        .build();

                results = segmenter.process(test);
                System.out.println(match(results, expectedResults));
    }

    private static boolean match(String[] results, String[] expectedResults)
    {
        assertTrue(results.length == expectedResults.length);
        for (int i = 0; i < results.length; i++)
        {
            if (!results[i].equals(expectedResults[i])) return false;
        }
        return true;
    }
}
