package edu.umn.biomedicus.segmenting;

import org.apache.commons.math3.stat.Frequency;
import org.jetbrains.annotations.NotNull;

public class Stats
{
    @NotNull
    Frequency lowerWords;
    @NotNull
    Frequency nonAbbrs;

    public Stats(Frequency lowerWords, Frequency nonAbbrs)
    {
        this.lowerWords = lowerWords;
        this.nonAbbrs = nonAbbrs;
    }

    public Frequency getLowerWords()
    {
        return lowerWords;
    }

    public Frequency getNonAbbrs()
    {
        return nonAbbrs;
    }
}
