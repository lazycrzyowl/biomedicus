package edu.umn.biomedicus.segmenting;

import edu.umn.biomedicus.core.ResourceFactory;
import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.segmenting.services.SegmentingServiceProvider;
import edu.umn.biomedicus.segmenting.services.StanfordSegmentingServiceProvider;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import edu.umn.biomedicus.tokenizing.services.StanfordTokenizingServiceProvider;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SentenceSegmenter {
    @NotNull
    protected AnalysisEngine ae;
    @NotNull
    protected Tokenizer tokenizer;
    @NotNull
    protected CAS aCAS;
    @NotNull
    protected CAS systemView;

    private SentenceSegmenter(Builder builder) throws ResourceInitializationException
    {
        tokenizer = new Tokenizer.Builder()
                .service(StanfordTokenizingServiceProvider.class)
                .build();

        ae = builder.getAE();
        aCAS = ae.newCAS();
        systemView = CASUtil.getSystemView(aCAS);
    }

    public static void main(String[] args) throws Exception
    {
        String test = "Segmenting sentences requires that acronyms/abbreviations do not cause " +
                "false sentence boundaries. Additionally, section titles with unusual punctuation/capitalization " +
                "should be segmented separately from the sentence that follows them because they " +
                "interefere with syntactic processing later in the pipeline. Prescriptions and metabolic scores " +
                "tend to create the most problems, such as prescribed zoloft c.a. p.i.d., with amitriptyline. " +
                "HISTORY OF PRESENT ILLNESS: The patient reports congestion since last Monday.";

        String[] expectedResults = new String[] {
                "Segmenting sentences requires that acronyms/abbreviations do not cause false sentence boundaries.",
                "Additionally, section titles with unusual punctuation/capitalization should be segmented " +
                        "separately from the sentence that follows them because they interefere with " +
                        "syntactic processing later in the pipeline.",
                "Prescriptions and metabolic scores tend to create the most problems, such as prescribed " +
                        "zoloft c.a. p.i.d., with amitriptyline.",
                "HISTORY OF PRESENT ILLNESS:",
                "The patient reports congestion since last Monday." };

        Tokenizer tokenizer = new Tokenizer.Builder()
                .service(StanfordTokenizingServiceProvider.class)
                .build();

        SentenceSegmenter sentSegmenter = new SentenceSegmenter.Builder()
                .service(StanfordSegmentingServiceProvider.class)
                .build();

        CAS aCAS = sentSegmenter.newCAS();
        CAS view = aCAS.createView(Views.SYSTEM_VIEW);
        view.setDocumentText(test);
        tokenizer.process(aCAS);
        Type tokenType = view.getTypeSystem().getType(Token.class.getCanonicalName());
        sentSegmenter.process(aCAS);
        for (AnnotationFS annot : view.getAnnotationIndex())
        {
            Type sentenceType = view.getTypeSystem().getType(Sentence.class.getCanonicalName());
            if (annot.getType() == sentenceType)
                System.out.println(String.format("%s : %s", annot.getType(), annot.getCoveredText()));
        }
    }

    private AnalysisEngine getAE()
    {
        return ae;
    }

    public CAS newCAS() throws ResourceInitializationException
    {
        return ae.newCAS();
    }

    public String[] process(String text) throws AnalysisEngineProcessException
    {
        aCAS.reset();
        CAS systemView = CASUtil.getSystemView(aCAS);
        systemView.setDocumentText(text);
        tokenizer.process(aCAS);
        ae.process(aCAS);
        AnnotationIndex<Sentence> sentenceIndex = (AnnotationIndex<Sentence>) AnnotationUtils.getAnnotations(systemView, Sentence.class);
        int resultSize = sentenceIndex.size();
        String[] sentenceArray = new String[sentenceIndex.size()];
        int i = 0;
        for (AnnotationFS sentence : sentenceIndex)
        {
            sentenceArray[i] = sentence.getCoveredText();
            i++;
        }
        return sentenceArray;
    }

    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        ae.process(aCAS);
    }

    public static class Builder {
        private String descriptor = "edu/umn/biomedicus/segmenting/sentenceAE.xml";
        private List<ServiceProviderDescription> serviceProviders = new ArrayList<>();
        private String serviceProviderClassName;
        private String modelResource;

        public Builder() {}

        public Builder(String descriptor)
        {
            this.descriptor = descriptor;
        }

        public Builder serviceProvider(ServiceProviderDescription serviceDesc)
        {
            serviceProviders.add(serviceDesc);
            return this;
        }

        public Builder service(Class<? extends SegmentingServiceProvider> serviceProvider)
        {
            serviceProviderClassName = serviceProvider.getCanonicalName();
            return this;
        }

        public Builder model(String modelResource)
        {
            this.modelResource = modelResource;
            return this;
        }

        public SentenceSegmenter build() throws ResourceInitializationException
        {
            ServiceProviderDescription segmentingService = null;
            if (serviceProviderClassName != null && modelResource != null)
            {
                segmentingService = new ServiceProviderDescription(
                        "sentenceSegmentingServiceProvider").serviceProvider(serviceProviderClassName)
                        .modelFile(modelResource);
                this.serviceProvider(segmentingService);
            }
            else if (serviceProviderClassName != null)
            {
                segmentingService = new ServiceProviderDescription("sentenceSegmentingServiceProvider")
                        .serviceProvider(serviceProviderClassName);
                serviceProvider(segmentingService);
            }
            return new SentenceSegmenter(this);
        }

        public AnalysisEngine getAE() throws ResourceInitializationException
        {
            URL resource = this.getClass().getClassLoader().getResource(descriptor);
            return ResourceFactory.createAnalysisEngine(resource, serviceProviders);
        }
    }
}

