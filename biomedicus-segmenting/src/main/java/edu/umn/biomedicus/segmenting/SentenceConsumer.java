package edu.umn.biomedicus.segmenting;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.PTBEscapingProcessor;
import edu.stanford.nlp.process.WordToSentenceProcessor;
import edu.umn.biomedicus.core.ClinicalDocument;
import edu.umn.biomedicus.core.featureranges.ContinuousSpan;
import edu.umn.biomedicus.core.featureranges.DocumentFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Sentence;
import org.apache.commons.io.IOUtils;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Simple class to collect all the sentence annotations.
 */
public class SentenceConsumer extends CasAnnotator_ImplBase {

    public static final String PARAM_OUTPUT_FILE = "outputFile";
    private String outputFile;
    BufferedWriter sentenceCollection;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        super.initialize(context);
        outputFile = (String) context.getConfigParameterValue(PARAM_OUTPUT_FILE);
        try
        {
            sentenceCollection = new BufferedWriter(new FileWriter(outputFile));
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    @Override
    public void process(CAS cas) throws AnalysisEngineProcessException
    {
        CAS system = CASUtil.getSystemView(cas);

        ClinicalDocument cd = ClinicalDocument.fromCAS(system);

        for (AnnotationFS sentence : cd.getSentences())
        {
            try {
                String sentenceText = sentence.getCoveredText().replaceAll("\\n", " ");
                sentenceCollection.write(sentence.getCoveredText().replaceAll("\\n"," "));
                sentenceCollection.newLine();
                sentenceCollection.flush();
            } catch (IOException e)
            {
                throw new AnalysisEngineProcessException(e);
            }
        }
    }

    public void TypeSystemInit(TypeSystem typeSystem)
    {
        Type sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
    }
}
