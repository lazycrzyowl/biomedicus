package edu.umn.biomedicus.segmenting;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.segmenting.services.SegmentingServiceProvider;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.uimafit.component.CasAnnotator_ImplBase;

/**
 * Sentence Annotator that uses Maximum Entropy sentence detection from OpenNLP. The ME model is trained using a
 * combination of the Genia corpus and a set of clinical notes.
 */
public class SentenceAE extends CasAnnotator_ImplBase {

    private static final String SEGMENTING_SERVICE_PROVIDER_KEY = "SEGMENTING_SERVICE";

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(SentenceAE.class);
    @NotNull
    private Type sentenceType;
    @NotNull
    private SegmentingServiceProvider segmenter;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        super.initialize(context);
        try
        {
            segmenter = (SegmentingServiceProvider) context.getResourceObject(SEGMENTING_SERVICE_PROVIDER_KEY);
        }
        catch (ResourceAccessException e)
        {
            throw new ResourceInitializationException(e);
        }
    }
    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        logger.log(Level.FINEST, "Tokenizer started processing.");
        CAS sysCAS = CASUtil.getSystemView(aCAS);

        String documentText = sysCAS.getDocumentText();

        // Tokens are required, so check to make sure they exist.
        AnnotationIndex tokens = AnnotationUtils.getAnnotations(sysCAS, Token.class);
        if (tokens.size() == 0)
        {
            if (documentText.length() == 0) return;
            Exception e = new Exception("No token annotations found");
            throw new AnalysisEngineProcessException(e);
        }

        segmenter.annotateCAS(sysCAS, documentText);
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {
        super.typeSystemInit(typeSystem);
        sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
    }
}