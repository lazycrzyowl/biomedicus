package edu.umn.biomedicus.segmenting.services.splitta;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

/**
 * A Document points to the head of a Frag object
 */
public class Document
{
    @NotNull
    Frag frag;

    public Document(Frag frag)
    {
        this.frag = frag;
    }

    public String toString()
    {
        ArrayList<Frag> s = new ArrayList<>();
        Frag curr = frag;
        while (curr != null)
        {
            s.add(curr);
        }
        return StringUtils.join(s, ", ");
    }


    /*
    public Stats getStats()
    {
        Frequency lowerWords = new Frequency();
        Frequency nonAbbrs = new Frequency();

        Frag currentFrag = frag;
        while (currentFrag != null)
        {
            for (String word : frag.tokenized.split())
            {
                String candidate = word.replace(",","");
                if (candidate.matches("[a-zA-Z]+")) // if all alpha
                {
                    if (word.matches("[a-z]+"))  // if all lowercase
                        lowerWords.addValue(candidate);

                    if (!word.endsWith("."))
                        nonAbbrs.addValue(word);
                }
            }
            currentFrag = frag.next();
        }

        Stats stats = new Stats(lowerWords, nonAbbrs);
        return stats;
    }

    public void featurize(Model model)
    {
        Frag currentFrag = frag;
        while (currentFrag != null)
        {
            frag.features = getFeatures(currentFrag, model);
        }
        currentFrag = frag.next();
    }


    /**
     * Output all the text, split according to predications or labels
     *
     * @param usePreds
     * @param tokenize
     * @param output
     * @param listOnly

    public List<String> segment(Boolean usePreds, Boolean tokenize, BufferedOutputStream output, Boolean listOnly)
    throws IOException
    {
        ArrayList<String> sents = new ArrayList<>();
        ArrayList<String> sent = new ArrayList<>();
        ArrayList<String> text = new ArrayList<>();

        double thresh = 0.5;
        Frag currentFrag = frag;

        while (frag != null)
        {
            text = (tokenize) ? frag.tokenized : frag.original;
            sent.add(text);

            if (frag.endsSeg || (usePreds && frag.pred > thresh) || (!usePreds && frag.label > thresh))
            {
                if (frag.origin == null) break;

                String sentText = StringUtils.join(sent, ' ');
                String spacer = (frag.endsSeg) ? "\n\n" : "\n";

                String outputString = sentText + spacer;
                Charset charset = Charset.availableCharsets().get("UTF-8");
                if (output != null)
                    output.write(outputString.getBytes(charset));
                else if (!listOnly)
                    System.out.println(outputString);

                sents.add(sentText);
                sent = new ArrayList<>();
            }
            currentFrag = frag.next();
        }
        return sents;
    }

    public void showResults()
    {
        double thresh = 0.5;
        int total = 0;
        int correct = 0;
        Frag currentFrag = frag;

        while (currentFrag != null)
        {
            String w1 = null;
            String w2 = null;
            total++;
            if (currentFrag.label == (currentFrag.pred > thresh)) correct++;
            else
            {

                String[] splitFragments = currentFrag.tokenized.split();
                String[] endingSplitFrags = Arrays.copyOfRange(
                        splitFragments, splitFragments.length - 2, splitFragments.length);
                w1 = StringUtils.join(endingSplitFrags, ' ');
                if (currentFrag.next != null)
                {
                    String[] w2SplitFragments = currentFrag.next.tokenized.split();
                    String[] w2EndingSplitFrags = Arrays.copyOfRange(w2SplitFragments, 0, 2);

                            w2 = StringUtils.join(w2EndingSplitFrags, ' ');
                } else {
                    w2 = "<EOF>";
                }
                System.out.println(String.format("[%d] [%1.4f] %s?? %s", currentFrag.label, currentFrag.pred, w1, w2));

            }
            currentFrag = frag.next;
        }
    }

*/

}
