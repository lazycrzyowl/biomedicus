package edu.umn.biomedicus.segmenting.services.splitta;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bill0154 on 2/21/14.
 */
public class SVMModel extends Model
{

    @NotNull
    String path;


    public SVMModel(String path)
    {
        this.path = path;

    }

    public void classify(String subject)
    {

    }

    public void train(String doc)
    {

    }
}
