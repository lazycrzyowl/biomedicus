package edu.umn.biomedicus.segmenting.services.splitta;

import org.apache.commons.math3.stat.Frequency;
import org.jetbrains.annotations.NotNull;

/**
 * Abstract Model class holds all relevant information, and includes
 train and classify functions
 */
public abstract class Model
{
    @NotNull
    String path;
    @NotNull
    Frequency feats;
    @NotNull
    Frequency lowerWords;
    @NotNull
    Frequency nonAbbrs;

    public Model()
    {

    }
    public Model(String path)
    {
        feats = new Frequency();
        lowerWords = new Frequency();
        nonAbbrs = new Frequency();
        this.path = path;
    }
}


    /*
    public void prep(Document doc)
    {
        Stats stats = doc.getStats();
        lowerWords = stats.getLowerWords();
        nonAbbrs = stats.getNonAbbrs();
    }

    abstract void train(String doc);


    abstract void classify(String doc);

    /**
     * save model objects in path

    public void save()
    {
        SBDUtil.serialize(feats, path + "feats");
        SBDUtil.serialize(lowerWords, path + "lower_words");
        SBDUtil.serialize(nonAbbrs, path + "non_abbrs");
    }

    /**
     * Load model objects from path
     * @return

    public void load()
    {
        feats = SBDUtil.deserialize(path + "feats");
        lowerWords = SBDUtil.deserialize(path + "lower_words");
        nonAbbrs = SBDUtil.deserialize(path + "non_abbrs");
    }

    */

