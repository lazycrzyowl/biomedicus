package edu.umn.biomedicus.collection.reader;

import au.com.bytecode.opencsv.CSVReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BratReader {
    List<String[]> annotationList;

    public BratReader(String docName, String bratPath)
    {
        annotationList = new ArrayList<>();
        File[] files = getFiles(bratPath);

        for (File f : files)
        {
            List<String[]> rows = getRows(f);
            findFileInBrat(rows, docName);
        }

    }

    private boolean findFileInBrat(List<String[]> rows, String docName)
    {
        for (String[] row : rows)
        {
            if (row[0].equals(docName))
            {
                addAnnotation(row);
                return true;
            }
        }
        return false;
    }

    private void addAnnotation(String[] row)
    {
        annotationList.add(row);
    }

    public File[] getFiles(String bratPath)
    {
        File dir = new File(bratPath);
        File[] files = dir.listFiles(
                new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.toLowerCase().endsWith(".txt");
                    }
                });
        return files;
    }

    public List<String[]> getAnnotations()
    {
        return annotationList;
    }

    public List<String[]> getRows(File f)
    {
        FileReader fr = null;
        List<String[]> rows = null;
        try
        {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(f), "UTF-8");
            CSVReader csv = new CSVReader(reader, '|');
            rows = csv.readAll();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return rows;
    }
}
