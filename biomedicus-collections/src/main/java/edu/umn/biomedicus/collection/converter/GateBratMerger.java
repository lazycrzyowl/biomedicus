/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.collection.converter;


import edu.umn.biomedicus.collection.reader.BratReader;
import edu.umn.biomedicus.collection.reader.GateReader;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.Progress;
import org.apache.uima.util.XMLInputSource;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class GateBratMerger extends org.apache.uima.collection.CollectionReader_ImplBase {
    public static String PARAM_INPUT_DIR = "inputDir";
    @NotNull
    static Type paragraphType;
    @NotNull
    static Type sentenceType;
    @NotNull
    static Feature sentenceClinicalClassFeature;
    @NotNull
    static String corpusPath;
    @NotNull
    GateReader reader;
    @NotNull
    int currentIndex;

    public static void main(String[] args) throws Exception
    {
        GateReader reader = new GateReader(args[0]);

        String resource = "edu/umn/biomedicus/collection/reader/gateCollectionReader.xml";
        URL resourceURL = GateBratMerger.class.getClassLoader().getResource(resource);
        XMLInputSource in = new XMLInputSource(resourceURL);
        ResourceSpecifier specifier =
                UIMAFramework.getXMLParser().parseResourceSpecifier(in);

        //create AE here
        AnalysisEngine ae =
                UIMAFramework.produceAnalysisEngine(specifier);
        CAS aCAS = ae.newCAS();
        TypeSystem typeSystem = aCAS.getTypeSystem();
        sentenceType = typeSystem.getType("edu.umn.biomedicus.type.Sentence");
        sentenceClinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
        paragraphType = typeSystem.getType("edu.umn.biomedicus.type.Paragraph");

        while (reader.hasNext())
        {
            Document doc = reader.getNext();
            String name = doc.getName().substring(0, doc.getName().length() - 6);
            CAS goldView = aCAS.createView("GoldView");
            CAS systemView = aCAS.createView("SystemView");
            systemView.setDocumentText(doc.getContent().toString());
            goldView.setDocumentText(doc.getContent().toString());

            addGateAnnotations(goldView, doc);
            addBratAnnotations(goldView, name, args[0]);

            String outputDir = new File(args[0], "../familyHistoryCorpus").toString();
            File dest = new File(outputDir, name);

            try
            {
                FileOutputStream os = new FileOutputStream(dest);
                XmiCasSerializer.serialize(aCAS, os);
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            } catch (SAXException e)
            {
                e.printStackTrace();
            }
            aCAS.reset();
        }

    }

    public static CAS addGateAnnotations(CAS aCAS, Document doc)
    {
        Map<String, AnnotationSet> annSets = doc.getNamedAnnotationSets();
        Iterator<Map.Entry<String, AnnotationSet>> annotSetIterator = annSets.entrySet().iterator();
        while (annotSetIterator.hasNext())
        {
            Map.Entry<String, AnnotationSet> entry = annotSetIterator.next();
            String key = entry.getKey();
            AnnotationSet annotSet = entry.getValue();
            for (Annotation annot : annotSet)
            {
                String type = annot.getType();
                int begin = annot.getStartNode().getOffset().intValue();
                int end = annot.getEndNode().getOffset().intValue();
                annot.getFeatures();
                if (type.equals("paragraph"))
                {
                    aCAS.createAnnotation(paragraphType, begin, end);
//                    System.out.println("Added annotation for paragraph");
                } else
                {
                    System.out.println("Found other type: " + type);
                }
            }
        }
        return aCAS;
    }

    @Override
    public void getNext(CAS aCAS) throws IOException, CollectionException
    {// null reader just to get type system.
    }

    private static void addBratAnnotations(CAS aCAS, String name, String cPath)
    {
        String documentText = aCAS.getDocumentText();
        BratReader reader = new BratReader(name, cPath);
        for (String[] annotation : reader.getAnnotations())
        {
            String msg = String.format("%s, %s, %s, %s, %s, %s", annotation);
            System.out.println("Adding annotation: " + msg);
            int begin = Integer.parseInt(annotation[3]);
            int end = Integer.parseInt(annotation[4]);
            String sentence = annotation[5];
            int find = documentText.indexOf(sentence);
            int len = sentence.length();
            if (find != -1)
            {
//                System.out.println("Search Sentence: " + sentence);
//                System.out.println("Found  Sentence: " + documentText.substring(find, find + len));
                AnnotationFS ann = aCAS.createAnnotation(sentenceType, find, find + len);
                ann.setStringValue(sentenceClinicalClassFeature, "familyhistory");
                aCAS.addFsToIndexes(ann);
            } else
            {
                System.out.println("\n\n\t\tFAILED TO FIND!!! " + name + "   " + sentence + "\n\n");
            }
        }
    }


    @Override
    public boolean hasNext() throws IOException, CollectionException
    {
        boolean hasNext = reader.hasNext();
        return hasNext;
    }

    @Override
    public Progress[] getProgress()
    {
        return new Progress[0];
    }

    @Override
    public void close() throws IOException
    {

    }

//    @Override
//    public void typeSystemInit(TypeSystem typeSystem)
//    {
//        paragraphType = typeSystem.getType("edu.umn.biomedicus.type.Paragraph");
//        sentenceType = typeSystem.getType("edu.umn.biomedicus.type.Sentence");
//        sentenceClinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
//    }
}
