package edu.umn.biomedicus.collection.reader;

import edu.umn.biomedicus.core.utils.CASUtil;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASRuntimeException;
import org.apache.uima.cas.impl.XmiCasDeserializer;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;


public class PlainTextReader extends CollectionReader_ImplBase {

    public static final String PARAM_INPUT_DIRECTORY = "inputDirectory";
    int totalFiles = 0;
    int currentFileIndex = 0;
    int mCurrentIndex;
    ArrayList<File> mFiles;
    private File inputDirectory;

    @Override
    public void initialize() throws ResourceInitializationException {
        File directory = new File(
                (String) getConfigParameterValue(PARAM_INPUT_DIRECTORY));
        mCurrentIndex = 0;

        //get list of files (not subdirectories) in the specified directory
        mFiles = new ArrayList();
        File[] files = directory.listFiles(
                new FilenameFilter() {
                    public boolean accept(File directory, String fileName) {
                        return fileName.toLowerCase().endsWith(".xmi");
                    }
                }
        );

        for (int i = 0; i < files.length; i++) {

            if (!files[i].isDirectory() && files[i].exists()) {
                mFiles.add(files[i]);
            }
        }
        totalFiles = files.length;
    }

    public boolean hasNext() {
        return (currentFileIndex + 1 <= mFiles.size());
    }

    @Override
    public Progress[] getProgress() {
        return new Progress[]{new ProgressImpl(currentFileIndex, mFiles.size(), Progress.ENTITIES)};
    }

    @Override
    public void close() throws IOException {

    }

    public void getNext(CAS aCAS) throws IOException, CollectionException {
         /*
         * make sure getNext is valid. Shouldn't need this if hasNext is
		 * correct. TODO: maybe remove?
		 */
        if (mFiles.size() < currentFileIndex + 1) {
            throw new RuntimeException("Called 'getNext()' after 'hasNext()' returned false.");
        }

        aCAS.reset();
        File currentFile = mFiles.get(currentFileIndex);
        try {
            XmiCasDeserializer.deserialize(new FileInputStream(currentFile), aCAS);
        } catch (SAXException e) {
            e.printStackTrace();
        }

        // make sure there's a md view.
        CAS md = CASUtil.getMetaDataView(aCAS);
        try {
            md.setSofaDataString(currentFile.toString(), "text/plain");
        } catch (CASRuntimeException e) {
            // Ignore, it means the sofaDataString is already set.
        }

        // make sure there's a system view
        CAS system = CASUtil.getSystemView(aCAS);
        CAS gold = CASUtil.getGoldView(aCAS);
        String docText = gold.getDocumentText();
        String systemDocText = system.getDocumentText();
        if (systemDocText == null)  // not set yet, so copy from gold
        {
            try {
                system.setDocumentText(docText);
            } catch (CASRuntimeException e) {
                e.printStackTrace();
            }
        }
        // increment current file record
        currentFileIndex++;
    }
}
