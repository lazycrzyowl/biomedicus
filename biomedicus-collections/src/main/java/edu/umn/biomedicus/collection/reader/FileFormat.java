package edu.umn.biomedicus.collection.reader;

/**
 * Provides the constants for known file formats used by BioMedICUS
 */
public class FileFormat {
    /** level value */
    private int format;

    /** level name */
    private String formatText;

    /** level value for GATE file formats (which are also XML, but kept separate) */
    public final static int GATE_INT = 80000;

    /** level value for plain text file formats */
    public final static int PLAIN_INT = 70000;

    /** level value for CSV file formats */
    public final static int CSV_INT = 60000;

    /** level value for XMI file formats (which are also XML, but kept separate) */
    public final static int XMI_INT = 50000;

    /** level value for XML file annotation formats */
    public final static int XML_INT = 40000;

    /** level value for BRAT annotation tool file formats */
    public final static int BRAT_INT = 30000;

    /** level value for level "ALL" */
    public final static int ALL_INT = Integer.MIN_VALUE;

    /** message level "OFF" */
    final static public FileFormat GATE = new FileFormat(GATE_INT, "GATE");

    /** message level "SEVERE" */
    final static public FileFormat SEVERE = new FileFormat(PLAIN_INT, "PLAIN");

    /** message level "WARNING" */
    final static public FileFormat WARNING = new FileFormat(CSV_INT, "CSV");

    /** message level "INFO" */
    final static public FileFormat INFO = new FileFormat(XMI_INT, "XMI");

    /** message level "CONFIG" */
    final static public FileFormat CONFIG = new FileFormat(XML_INT, "XML");

    /** message level "FINE" */
    final static public FileFormat FINE = new FileFormat(BRAT_INT, "BRAT");

    /**
     * Instantiate a new FileFormat object.
     *
     * @param formatId
     *          level value
     * @param formatText
     *          level name
     */
    protected FileFormat(int formatId, String formatText) {
        // set level value
        this.format = formatId;
        // set level name
        this.formatText = formatText;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object o) {
        // check if current object o is an instance of Level
        if (o instanceof FileFormat) {
            // cast Object to Level
            FileFormat r = (FileFormat) o;
            // return true if both levels are the same
            return (this.format == r.format);
        } else // if o is not instance of FileFormat, return false
        {
            return false;
        }
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return this.format;
    }

    /**
     * Returns the integer representation of this format.
     *
     * @return int - format value
     */
    public final int toInteger() {
        return format;
    }

    /**
     * Returns the string representation of this format.
     *
     * @return String - format name
     */
    public final String toString()
    {
        return formatText;
    }

}
