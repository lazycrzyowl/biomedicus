/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.collection.reader;

import edu.umn.biomedicus.collection.DocumentReaderException;
import gate.*;
import gate.creole.ResourceInstantiationException;
import gate.util.ExtensionFileFilter;
import gate.util.GateException;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;


/**
 * @author Robert Bill
 */
public class GateReader implements Reader {

    @NotNull
    Corpus corpus;
    @NotNull
    int currentDoc;
    @NotNull
    int totalDocs;

    public GateReader(URL corpusURL) throws ResourceInstantiationException
    {
        GateReader.initializeGate();
        corpus = Factory.newCorpus(corpusURL.toString());
        ExtensionFileFilter filter = new ExtensionFileFilter("XML files", "xml");
        try
        {
            corpus.populate(corpusURL, filter, null, false);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public GateReader(String corpusPath) throws ResourceInstantiationException{
        GateReader.initializeGate();
        corpus = Factory.newCorpus(corpusPath);
        URL directory = null;
        try
        {
            directory = Paths.get(corpusPath).toUri().toURL();
        } catch (MalformedURLException e)
        {
            throw new ResourceInstantiationException(e);
        }
        ExtensionFileFilter filter = new ExtensionFileFilter("XML files", "xml");
        try
        {
            corpus.populate(directory, filter, null, false);
        }
        catch (IOException e)
        {
                throw new ResourceInstantiationException(e);
        }

    }

    public boolean hasNext()
    {
        if (currentDoc < corpus.size() - 1) return true;
        return false;
    }

    public Document getNext() throws DocumentReaderException
    {
        // for debug, track last doc name
        String lastDoc = null;
        currentDoc++;
        return corpus.get(currentDoc);
    }

    private Document toClinicalDocument()
    {
        Document doc = corpus.get(currentDoc);
//        AnnotationSet ann = doc.getAnnotations("paragraph");
//        FeatureMap features = doc.getFeatures();
        AnnotationSet as = doc.getAnnotations("Original markups");
        for (Annotation annot : as)
        {
                    int id = annot.getId();
                    Node begin = annot.getStartNode();
                    Node end = annot.getEndNode();
                    String typeInfo = annot.getType();
                    FeatureMap fMap = annot.getFeatures();
                    DocumentContent content = doc.getContent();
        }
        return null;
    }

    private static void initializeGate() throws ResourceInstantiationException
    {
        if (!Gate.isInitialised())
            try
            {
                Gate.init();
            } catch (GateException e)
            {
                throw new ResourceInstantiationException(e);
            }
    }

    public static Reader getReader(URL corpusURL) throws ResourceInstantiationException
    {
        URI corpusURI = null;
        try
        {
            corpusURI = corpusURL.toURI();
        } catch (URISyntaxException e)
        {
            throw new ResourceInstantiationException(e);
        }

        Path corpusDirectory = Paths.get(corpusURI);
        File corpusDirectoryTestingFile = corpusDirectory.toFile();
        if (!corpusDirectoryTestingFile.exists())
        {
            String msg = String.format("The specified directory does not exist: %s", corpusURL.toString());
            throw new ResourceInstantiationException(msg);
        }
        else if (!corpusDirectoryTestingFile.isDirectory())
        {
            String msg = String.format("The specified path is not a directory: %s", corpusURL.toString());
            throw new ResourceInstantiationException(msg);
        }

        Reader reader = new GateReader(corpusURL);
        return reader;
    }

    public static Reader getReader(String corpusPath) throws ResourceInstantiationException
    {
        Path corpusDirectory = Paths.get(corpusPath);
        File corpusDirectoryTestingFile = corpusDirectory.toFile();
        if (!corpusDirectoryTestingFile.exists())
        {
            String msg = String.format("The specified directory does not exist: %s", corpusPath);
            throw new ResourceInstantiationException(msg);
        }
        else if (!corpusDirectoryTestingFile.isDirectory())
        {
            String msg = String.format("The specified path is not a directory: %s", corpusPath);
            throw new ResourceInstantiationException(msg);
        }

        Reader reader = new GateReader(corpusPath);
        return reader;
    }
}
