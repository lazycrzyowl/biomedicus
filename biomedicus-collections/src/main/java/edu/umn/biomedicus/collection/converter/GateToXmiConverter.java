/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.collection.converter;


import edu.umn.biomedicus.collection.DocumentReaderException;
import edu.umn.biomedicus.collection.reader.BratReader;
import edu.umn.biomedicus.collection.reader.GateReader;
import edu.umn.biomedicus.core.Views;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.creole.ResourceInstantiationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.Progress;
import org.apache.uima.util.XMLInputSource;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;

public class GateToXmiConverter {

    @NotNull
    String inputDir;
    @NotNull
    String outputDir;
    @NotNull
    GateReader reader;
    @NotNull
    int currentIndex;
    @NotNull
    Type paragraphType;
    @NotNull
    Type sentenceType;
    @NotNull
    Feature sentenceClinicalClassFeature;

    public GateToXmiConverter(String inputDir) throws ResourceInstantiationException, FileNotFoundException, SAXException
    {
        this.inputDir = System.getProperty("mts.home");
        this.outputDir = new File(this.inputDir, "../familyHistoryCorpus").toString();

        reader = (GateReader) GateReader.getReader(inputDir);
    }

    public Document getNext() throws AnalysisEngineProcessException
    {
        Document doc = null;
        try
        {
            doc = reader.getNext();
        } catch (DocumentReaderException e)
        {
            throw new AnalysisEngineProcessException();
        }

        return doc;
    }
//
//        if (doc != null)
//        {
//            String docText = doc.getContent().toString();
//            aCAS.setDocumentText(docText);
//            Map<String,AnnotationSet> annotSets = doc.getNamedAnnotationSets();
//            Iterator<Map.Entry<String,AnnotationSet>> annotSetIterator = annotSets.entrySet().iterator();
//            while (annotSetIterator.hasNext())
//            {
//                Map.Entry<String,AnnotationSet> entry = annotSetIterator.next();
//                String key = entry.getKey();
//                AnnotationSet annot = entry.getValue();
//                addAnnotations(aCAS, annot);
//            }
//            String name = doc.getName();
//
//            CAS gold = aCAS.createView(Views.GOLD_VIEW);
//            CAS sys = aCAS.createView(Views.SYSTEM_VIEW);
//            CAS md = aCAS.createView(Views.METADATA_VIEW);
//
//
//            md.setSofaDataString(name.substring(0, name.length() - 6), "text/plain");
//
//
//            addBratAnnotations(aCAS, name.substring(0, name.length() - 6));
//            System.out.println("Name: " + name.substring(0, name.length() - 6));
//            System.out.println("METADATA: " + md.getSofaDataString());
//        }
//    }

    private void addBratAnnotations(CAS aCAS, String name)
    {
        String documentText = aCAS.getDocumentText();
        CAS gold = aCAS.getView(Views.GOLD_VIEW);
        BratReader reader = new BratReader(name, inputDir);
        for (String[] annotation : reader.getAnnotations())
        {
            String msg = String.format("%s, %s, %s, %s, %s, %s", annotation);
            System.out.println("Adding annotation: " + msg);
            int begin = Integer.parseInt(annotation[3]);
            int end = Integer.parseInt(annotation[4]);
            String sentence = annotation[5];
            int find = documentText.indexOf(sentence);
            int len = sentence.length();
            if (find != -1)
            {
                System.out.println("Search Sentence: " + sentence);
                System.out.println("Found  Sentence: " + documentText.substring(find, find + len));
                AnnotationFS ann = gold.createAnnotation(sentenceType, find, find + len);
                ann.setStringValue(sentenceClinicalClassFeature, "familyhistory");
                gold.addFsToIndexes(ann);
            }
            else {
                System.out.println("\n\n\t\tFAILED TO FIND!!! " + name + "   " + sentence + "\n\n");
            }
        }
    }

    private void addAnnotations(CAS aCAS, AnnotationSet annot)
    {
        String doctext = aCAS.getDocumentText();
        for (Annotation a : annot)
        {
            int begin = a.getStartNode().getOffset().intValue();
            int end = a.getEndNode().getOffset().intValue();
            String type = a.getType();
            if (type.equals("FH") || type.equals("SH") || type.equals("FSHsection"))
            {
                String msg = String.format("%s: %s, %d:%d", type, doctext.substring(begin,end), begin, end);
//                System.out.println(msg);
            }
            String msg = String.format("%s : %d, %d", a.getType(), begin, end);
//            System.out.println(msg);
        }
    }

    public boolean hasNext() throws IOException, CollectionException
    {
        boolean hasNext = reader.hasNext();
        return hasNext;
    }

    public Progress[] getProgress()
    {
        return new Progress[0];
    }

    public void close() throws IOException
    {

    }

    public static void main(String[] args) throws Exception
    {
        String resource = "edu/umn/biomedicus/collection/reader/gateCollectionReader.xml";
        URL resourceURL = GateToXmiConverter.class.getClassLoader().getResource(resource);
        XMLInputSource in = new XMLInputSource(resourceURL);
        ResourceSpecifier specifier =
                UIMAFramework.getXMLParser().parseResourceSpecifier(in);

        //create AE here
        AnalysisEngine ae =
                UIMAFramework.produceAnalysisEngine(specifier);



        String inputDir = Paths.get(System.getProperty("mts.home")).toString();
        GateToXmiConverter converter = new GateToXmiConverter(inputDir);

        Document doc = null;
        while (converter.hasNext()) {
            doc = converter.getNext();
            if (doc == null) continue;

            CAS aCAS = ae.newCAS();
            aCAS.setDocumentText(doc.getContent().toString());

            CAS gold = aCAS.createView(Views.GOLD_VIEW);
            gold.setDocumentText(doc.getContent().toString());
            CAS md = aCAS.createView(Views.METADATA_VIEW);
            CAS sys = aCAS.createView(Views.SYSTEM_VIEW);


            converter.typeSystemInit(aCAS.getTypeSystem());

            if (doc != null) {
                Map<String, AnnotationSet> annotSets = doc.getNamedAnnotationSets();
                Iterator<Map.Entry<String, AnnotationSet>> annotSetIterator = annotSets.entrySet().iterator();
                while (annotSetIterator.hasNext()) {
                    Map.Entry<String, AnnotationSet> entry = annotSetIterator.next();
                    String key = entry.getKey();
                    AnnotationSet annot = entry.getValue();
                    converter.addAnnotations(aCAS, annot);
                }
                String name = doc.getName();
                md.setSofaDataString(name.substring(0, name.length() - 6), "text/plain");
                converter.addBratAnnotations(aCAS, name.substring(0, name.length() - 6));
                converter.serialize(aCAS);
                aCAS.reset();
            }
        }
    }

    public void serialize(CAS aCAS)
    {
        String sourceFileName = new File(aCAS.getView(Views.METADATA_VIEW).getDocumentText()).getName();
        Path destFilePath = Paths.get(outputDir, FilenameUtils.removeExtension(sourceFileName) + ".xmi");
        try
        {
            FileOutputStream os = new FileOutputStream(destFilePath.toFile());
            XmiCasSerializer.serialize(aCAS, os);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (SAXException e)
        {
            e.printStackTrace();
        }
    }

//        //String outputName = "test.xmi";
//        ae.process(aCAS);
//        Iterator<CAS> viewItor = aCAS.getViewIterator();
//
//
//        String outputName = aCAS.getView(Views.METADATA_VIEW).getSofaDataString();
//
//        File inputDir = Paths.get(System.getProperty("mts.home")).toFile();
//        File outputDir = new File(inputDir.getPath(), "../familyHistoryCorpus");
//        File dest = new File(outputDir, outputName);
//        try
//        {
//            FileOutputStream os = new FileOutputStream(dest);
//            XmiCasSerializer.serialize(aCAS, os);
//        } catch (FileNotFoundException e)
//        {
//            e.printStackTrace();
//        } catch (SAXException e)
//        {
//            e.printStackTrace();
//        }
//    }


    public void typeSystemInit(TypeSystem typeSystem)
    {
        paragraphType = typeSystem.getType("edu.umn.biomedicus.type.Paragraph");
        sentenceType = typeSystem.getType("edu.umn.biomedicus.type.Sentence");
        sentenceClinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
        //socialHistory sh = typeSystem.getType("something");
        //familyHistory fh = typeSystem.getType("something");
        //fshSection = typeSystem.getType("something");
    }
}
