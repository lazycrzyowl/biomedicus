package edu.umn.biomedicus.lucenelookup.services;

import edu.umn.biomedicus.core.utils.Span;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;

import java.util.List;

public interface LuceneLookupServiceProvider {
    public Span[] analyze(String sentenceText, String[] sentenceAsTokens,
                                           Span[] tokensAsSpans, String[] tags) throws Exception;
    public Span[] getSpans();
}
