package edu.umn.biomedicus.lucenelookup;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Sentence;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;


/**
 * LuceneLookup analysis engine.
 * TODO: add documentation.
 */
public class LuceneLookupAE extends CasAnnotator_ImplBase {
    private static final Logger logger = UIMAFramework.getLogger(LuceneLookupAE.class);

    // Variables for parameters drawn from descriptor file
    public static final String PARAM_USE_RAW_FORMAT = "rawFormat";
    private boolean rawFormat = false;

    public static final String PARAM_HITS_PER_PAGE = "hitsPerPage";
    private int hitsPerPage = 10;

    public static final String PARAM_INDEX_DIRECTORY_PATH = "indexDirectory";

    // Variables for working with lucene
    @NotNull private IndexSearcher searcher;
    @NotNull private QueryParser parser;
    private static final String field = "contents";


    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {

        // Initialize superclass and begin logging
        super.initialize(context);
        logger.log(Level.INFO, "Initilizing idices...");

        // Get configuration parameter that identifies the lucene index directory
        File indexDirectory = Paths.get((String) context.getConfigParameterValue(PARAM_INDEX_DIRECTORY_PATH)).toFile();
        rawFormat = (Boolean) context.getConfigParameterValue(PARAM_USE_RAW_FORMAT);
        hitsPerPage = (Integer) context.getConfigParameterValue(PARAM_HITS_PER_PAGE);

        // Create lucene components
        @NotNull IndexReader reader = createReader(indexDirectory);
        @NotNull Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_46);
        searcher = new IndexSearcher(reader);
        parser = new QueryParser(Version.LUCENE_46, field, analyzer);

        // Log successful initialization
        logger.log(Level.INFO, "Initialization of lucene index successful.");
    }

    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = CASUtil.getSystemView(aCAS);

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, Sentence.class)) {
//            Commented out 4 lines on 2014.05.14. Variables never used.
//            SentenceFeatureRange sentence = new SentenceFeatureRange(sysCAS, sentenceAnnotation);
//            String sentenceText = sentenceAnnotation.getCoveredText();
//            int sentenceStart = sentenceAnnotation.getBegin();
//            edu.umn.biomedicus.core.utils.Span[] chunks = null;

            String line = "heart failure";

            try {
                Query query = parser.parse(line);

//                Commented out following line on 2014.05.14. Variable never used.
//                Query chunkquery = new TermQuery(new Term("chunk", line));

                System.out.println("Searching for: " + query.toString(field));

                TopDocs results = searcher.search(query, 5 * hitsPerPage);
                ScoreDoc[] hits = results.scoreDocs;

                int numTotalHits = results.totalHits;
                System.out.println(numTotalHits + " total matching documents");

                int start = 0;
                int end = Math.min(hits.length, start + hitsPerPage);

                for (int i = start; i < end; i++) {
                    if (rawFormat) {
                        // output raw format is rawFormat is specified in descriptor file
                        System.out.println("doc=" + hits[i].doc + " score=" + hits[i].score);
                        continue;
                    }

                    Document doc = searcher.doc(hits[i].doc);
                    String path = doc.get("path");
                    if (path != null) {
                        System.out.println((i + 1) + ". " + path + " Score: " + hits[i].score);
                        String title = doc.get("title");
                        if (title != null) {
                            System.out.println("   Title: " + doc.get("title"));
                        }
                    } else {
                        System.out.println((i + 1) + ". " + "No path for this document");
                    }

                }

            } catch (org.apache.lucene.queryparser.classic.ParseException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Initialize the required types. No java cover classes are used, so typeSystemInit is required.
     * @param typeSystem
     * the uima typesystem provided by the UIMAFramework.
     * @throws AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {
        // Initialize types
        super.typeSystemInit(typeSystem);
        logger.log(Level.FINEST, "Lucenelookup type system initialized.");
    }

    /**
     * Private method to create the lucene index reader. The reason for this method is to keep component creation
     * as clear as possible in the initialize method. The try/catch code goes here.
     *
     * @param index
     * The filesystem directory that contains the lucene index.
     * @return
     * A IndexReader instance created to read the specified lucene index directory.
     * @throws ResourceInitializationException
     */
    private IndexReader createReader(File index) throws ResourceInitializationException {
        @NotNull IndexReader newReader;
        try {
            newReader = DirectoryReader.open(FSDirectory.open(index));
        } catch (IOException e) {
            throw new ResourceInitializationException(e);
        }
        return newReader;
    }

}

