package edu.umn.biomedicus.lucenelookup;

import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;


/**
 * Unit test for simple Chunker.
 */
public class LuceneLookupTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public LuceneLookupTest(String testName) throws Exception
    {
        super(testName);
        Tokenizer tokenizer = new Tokenizer.Builder().build();
        SentenceSegmenter segmenter = new SentenceSegmenter.Builder().build();
        LuceneLookup lookup = new LuceneLookup.Builder().build();

        //LuceneLookupBuilder.setDescriptorFile("edu/umn/biomedicus/lucenelookup/chunkingAE.xml");


        //AnalysisEngine lookup = LuceneLookupBuilder.getChunker();
        CAS aCAS = lookup.newCAS();
        CAS view = aCAS.createView("System");
        view.setDocumentText("This is a test of the chunker to see if it really works");
        tokenizer.process(aCAS);
        segmenter.process(aCAS);
        lookup.process(aCAS);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( LuceneLookupTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
