/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import java.util.HashMap;

/**
 * This class holds an entry from the edu.umn.biomedicus.acronym manifest. An entry involves 0 to
 * many candidate expansions (aka. long forms) and a set of CUIs that are
 * associated with each long form.
 * 
 * @author Robert Bill
 */
public class AcronymManifestEntry {

	private String acronym;
	private int senseCount; // number of senses possible
	private HashMap<String, Float> expansions = new HashMap<String, Float>();
	private HashMap<String, AcronymManifestCui> cuis = new HashMap<String, AcronymManifestCui>();
	private HashMap<String, String> expansionCuiAssociation = new HashMap<String, String>();

	public AcronymManifestEntry(String acronym) {
		this.acronym = acronym;
	}

	public void setSenseCount(int senseCount) {
		this.senseCount = senseCount;
	}

	public int getSenseCount() {
		return this.senseCount;
	}

	public String getAcronym() {
		return acronym;
	}

	public void addExpansion(String expansion, float probability) {
		expansions.put(expansion, probability);
	}

	public String[] getExpansions() {
		int size = expansions.size();
		String[] results = new String[size];
		return expansions.keySet().toArray(results);
	}

	public float getExpansionProbability(String expansion) {
		return expansions.get(expansion);
	}

	public void addCui(String cuiId, String expansion, String semType) {
		if (cuiId.trim().length() == 0 || cuiId == null)
			return;
		if (cuis.containsKey(cuiId)) {
			// get existing cui and add data to it
			cuis.get(cuiId).addSemType(semType);
		} else {
			// create new cui and add data
			AcronymManifestCui cui = new AcronymManifestCui(cuiId);
			cui.addSemType(semType);
			cuis.put(cuiId, cui);
		}

		expansionCuiAssociation.put(cuiId, expansion);
	}

	public AcronymManifestCui[] getCuis() {
		AcronymManifestCui[] results = new AcronymManifestCui[cuis.keySet().size()];
		int i = 0;
		for (String key : cuis.keySet()) {
			results[i] = cuis.get(key);
			i++;
		}
		return results;
	}

	public String getExpansionForCui(String cuiId) {
		return expansionCuiAssociation.get(cuiId);
	}

	public String getMostFrequentExpansion() {
		String result = "";
		Float max = new Float(0.0);
		for (String key : expansions.keySet()) {
			Float probability = expansions.get(key);
			if (probability > max) {
				result = key;
				max = probability;
			}
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder results = new StringBuilder();
		results.append(acronym);
		results.append(" [");
		for (String expansion : expansions.keySet())
			results.append(expansion).append(" ");
		results.append("] ");
		for (String cui : cuis.keySet()) {
			results.append("[").append("'").append(cui).append("'").append("  [");
			for (String semType : cuis.get(cui).getSemTypes()) {
				results.append(semType).append(" ");
			}
			results.append("]");
		}
		results.append("]");
		return results.toString();
	}
}
