/* 
Copyright 2012 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * This AE evaluates the system edu.umn.biomedicus.acronym tags against the gold edu.umn.biomedicus.acronym tags.
 * 
 * @author Robert Bill
 */
//@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class AcronymDisambiguationEvaluator extends JCasAnnotator_ImplBase {
	private int totalGoldAcronyms = 0;
	private int correctExpansions = 0;
	private int incorrectExpansions = 0;
	private int detected = 0;
	private AcronymManifest manifest = null;

	@SuppressWarnings("unused")
	private int notDetected = 0;

	public static final String PARAM_MANIFEST_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                AcronymLongFormDisambiguationAE.class, "acronymCuiManifestUri");
	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/edu.umn.biomedicus.acronym/AcronymManifest.csv")
	private String acronymCuiManifestUri;

	HashSet<String> missing = new HashSet<String>();
	int numberMissing = 0;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		InputStream manifestInputStream = getClass().getClassLoader().getResourceAsStream(acronymCuiManifestUri);
		try {
			manifest = new AcronymManifest(manifestInputStream);
		} catch (IOException e) {
			System.err.println("Unable to initalize the edu.umn.biomedicus.acronym manifest. Probably check that the manifest file really exists at: "
			                                + acronymCuiManifestUri);
			e.printStackTrace();
			throw new ResourceInitializationException();
		}

	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		JCas goldView = null;
		JCas systemView = null;
		Hashtable<String, String> gold = new Hashtable<String, String>();
		Hashtable<String, String> system = new Hashtable<String, String>();



            /*
		try {
			goldView = jCas.getView(ViewNames.GOLD_VIEW);
			systemView = jCas.getView(ViewNames.SYSTEM_VIEW);
		} catch (CASException e) {
			e.printStackTrace();
		}

		for (Acronym goldAcronym : JCasUtil.select(goldView, Acronym.class)) {
			int begin = goldAcronym.getBegin();
			int end = goldAcronym.getEnd();
			AcronymManifestEntry entry = manifest.getEntry(goldAcronym.getCoveredText());
			if (entry == null) {
				missing.add(goldAcronym.getCoveredText());
				numberMissing++;
			}

			gold.put(new String(begin + ":" + end), goldAcronym.getExpansion());
			totalGoldAcronyms++;
		}

		for (Acronym systemAcronym : JCasUtil.select(systemView, Acronym.class)) {
			int begin = systemAcronym.getBegin();
			int end = systemAcronym.getEnd();
			String expansion = systemAcronym.getExpansion();
			if (expansion == null)
				expansion = "UNKNOWN SENSE";
			system.put(new String(begin + ":" + end), expansion);
		}

		for (String goldTrue : gold.keySet()) {
			if (system.containsKey(goldTrue)) {
				detected++;
				String systemExpansion = system.get(goldTrue);
				String goldExpansion = gold.get(goldTrue);
				if (goldExpansion.equals(systemExpansion)) {
					correctExpansions++;
				} else {
					incorrectExpansions++;
				}
			} else {
				notDetected++;
			}
		}


		*/
	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		System.out.println("------------");
		System.out.println("Total number of edu.umn.biomedicus.acronym annotations in the GOLD view: " + totalGoldAcronyms);
		System.out.println("Number of Gold acronyms without a manifest entry: " + numberMissing);
		System.out.println("Total detected acronyms: " + detected);
		System.out.println("Correct Expansions: " + correctExpansions);
		System.out.println("Incorrect Expansions: " + incorrectExpansions);
		for (String key : missing) {
			System.out.print(key + ", ");
		}
	}
}
