/* 
Copyright 2012 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.acronym;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;

import java.text.NumberFormat;

/**
 * This AE evaluates the system edu.umn.biomedicus.acronym tags against the gold edu.umn.biomedicus.acronym tags.
 * 
 * @author Robert Bill
 */
//@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class AcronymDetectionEvaluator extends JCasAnnotator_ImplBase {

	private int totalTokens = 0;
	private int totalGoldAcronyms = 0;
	private int truePositive = 0;
	private int trueNegative = 0;
	private int falsePositive = 0;
	private int falseNegative = 0;

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {


            /*
		// Create a JCas object for each view.
		JCas goldView = null;
		JCas systemView = null;

		// Create something to hold the location of acronyms found in each view.
		Hashtable<String, String> gold = new Hashtable<String, String>();
		Hashtable<String, String> system = new Hashtable<String, String>();

		// Get the views from the JCas object passed into the analysis engine.
		try {
			goldView = jCas.getView(ViewNames.GOLD_VIEW);
			systemView = jCas.getView(ViewNames.SYSTEM_VIEW);
		} catch (CASException e) {
			e.printStackTrace();
		}

		// Collect gold edu.umn.biomedicus.acronym locations in Hashtable
		for (Acronym goldAcronym : JCasUtil.select(goldView, Acronym.class)) {
			int begin = goldAcronym.getBegin();
			int end = goldAcronym.getEnd();
			gold.put(new String(begin + ":" + end), goldAcronym.getCoveredText());
			totalGoldAcronyms++;
		}

		// Collect system edu.umn.biomedicus.acronym locations in Hashtable
		for (Acronym systemAcronym : JCasUtil.select(systemView, Acronym.class)) {
			int begin = systemAcronym.getBegin();
			int end = systemAcronym.getEnd();
			system.put(new String(begin + ":" + end), systemAcronym.getCoveredText());

			// Determine if the current system edu.umn.biomedicus.acronym exists in the gold view.
			if (!gold.containsKey(systemAcronym)) {
				falsePositive++;
				System.out.println("false positive: '" + systemAcronym.getCoveredText() + "'");
			}
		}

		// Determine which of the gold acronyms are missing from the system view
		for (String goldTrue : gold.keySet()) {
			if (system.containsKey(goldTrue)) {
				truePositive++;
			} else {
				falseNegative++;
			}
		}

		// Calculate true negatives using the other contingency table
		// frequencies
		trueNegative = totalTokens - truePositive - falsePositive - falseNegative;



		*/
	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		System.out.println("------------");
		System.out.println("Total number of edu.umn.biomedicus.acronym annotations in the GOLD view: " + totalGoldAcronyms);
		System.out.println("True Positives: " + truePositive);
		System.out.println("False Positive: " + falsePositive);
		System.out.println("False Negative: " + falseNegative);
		System.out.println("True Negatives: " + trueNegative);
		float precision = (float) truePositive / new Float(truePositive + falsePositive);
		float recall = (float) truePositive / new Float(truePositive + falseNegative);
		float fmeasure = (float) 2 * ((precision * recall) / (precision + recall));
		System.out.println("precision: " + NumberFormat.getPercentInstance().format(precision));
		System.out.println("recall   : " + NumberFormat.getPercentInstance().format(recall));
		System.out.println("F-measure: " + NumberFormat.getPercentInstance().format(fmeasure));
	}
}
