/* 
 Copyright 2012-13 University of Minnesota
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Acronym;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.Span;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.util.ExtendedLogger;
import weka.classifiers.functions.SGDText;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class detects acronyms and adds an annotation for each edu.umn.biomedicus.acronym. Positive detection is one of the following: 1.
 * Exact matches to an edu.umn.biomedicus.acronym manifest 2. Positive outcome from simple rule sets such as all consonants 3. SVM
 * (SGDText) instance prediction using token shape as the attribute. Token shapes are cC for consonants, vV for vowels,
 * 9 for numbers, $ for symbols and '.' and ',' remain unchanged.
 *
 * @author Riea Moon
 * @author Robert Bill
 */
public class AcronymDetectorAE_SVM extends CasAnnotator_ImplBase {
    public static final String PARAM_MODEL_FILE = "modelFile";
    public static final String PARAM_ACRONYM_MANIFEST_FILE = "acronymCuiManifestFile";
    SGDText cls; //Classifier cls;
    // model: /edu/umn/biomedicus/edu.umn.biomedicus.acronym/acronymDetectionSGDPruned.model
    // manifest: /edu/umn/biomedicus/edu.umn.biomedicus.acronym/AcronymManifest.csv
    private String acronymCuiManifestFile = "edu/umn/biomedicus/acronym/AcronymManifest.csv";
    private String modelFile = "edu/umn/biomedicus/acronym/acronymDetectionSGDPruned.model";
    private Type acronymType;
    private AcronymManifest manifest = null;
    private Instances dataRaw;
    private Attribute word;
    private Attribute shape;
    private ExtendedLogger log;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
//        modelFile = (String) context.getConfigParameterValue(PARAM_MODEL_FILE);
//        acronymCuiManifestFile = (String) context.getConfigParameterValue(PARAM_ACRONYM_MANIFEST_FILE);
        try {
            AtomicReference<InputStream> inputStream;
            inputStream = new AtomicReference<InputStream>(
                    getClass().getClassLoader().getResourceAsStream(acronymCuiManifestFile));
            manifest = new AcronymManifest(inputStream.get());
        } catch (IOException e) {
            System.err.println(
                    "AcronymDetectorAE_SVM unable to initalize the edu.umn.biomedicus.acronym manifest. " +
                            "Probably check that the manifest file is at: " + acronymCuiManifestFile);
            e.printStackTrace();
            throw new ResourceInitializationException();
        }
        try {
            cls = (SGDText) weka.core.SerializationHelper.read(getClass().getClassLoader().getResourceAsStream(modelFile));
        } catch (Exception e) {
            System.err.println("AcronymDetectorAE_SVM unable to de-serialize model file " + modelFile);
            System.err.println(e.toString());
            throw new ResourceInitializationException();
        }

        // Create Attribute and Instance information needed for the SGDText classifier
        ArrayList<Attribute> atts = new ArrayList<Attribute>(2);
        ArrayList<String> classVal = new ArrayList<String>();
        classVal.add("ACRONYM");
        classVal.add("NONACRONYM");
        word = new Attribute("word", (ArrayList<String>) null);
        atts.add(word);
        shape = new Attribute("shape", (ArrayList<String>) null);
        atts.add(shape);
        atts.add(new Attribute("@@class@@", classVal));
        dataRaw = new Instances("TestInstances", atts, 0);
    }

    @Override
    public void process(CAS cas) throws AnalysisEngineProcessException {
        CAS system = CASUtil.getSystemView(cas);
        String documentText = system.getDocumentText();


        for (Span token : WhitespaceTokenizer.INSTANCE.tokenizePos(documentText)) {
            String candidate = documentText.substring(token.getStart(), token.getEnd());

            // test for include rules/models first
            if (manifest.containsKey(candidate)
                    || ruleDetection(candidate)
                    || modelDetection(candidate)) {

                // exclude based on rules
                if (exclude(token, documentText)) continue;

                // add annotation for token
                AnnotationFS acronym = system.createAnnotation(
                        acronymType, token.getStart(), token.getEnd());
                system.addFsToIndexes(acronym);
            }
        }
    }

    private boolean exclude(Span token, String documentText)
    {
        String candidate = documentText.substring(token.getStart(), token.getEnd());
        // only one rule so far. The lower-case letter "a" should be excluded
        if (candidate.equals("a")) return true;
        return false;
    }


    protected String getWordShape(String candidate) {
        StringBuffer sb = new StringBuffer();

        for (char c : candidate.toCharArray()) {
            if ("BCDFGHJKLMNPQRSTVWXYZ".indexOf(c) != -1) {
                sb.append('C');
            } else if ("AEIOU".indexOf(c) != -1) {
                sb.append('V');
            } else if ("bcdfghjklmnpqrstvwxyz".indexOf(c) > -1) {
                sb.append('c');
            } else if ("aeiou".indexOf(c) != -1) {
                sb.append('v');
            } else if (".,".indexOf(c) != -1) {
                sb.append(c);
            } else {
                sb.append('$');
            }
        }

        return sb.toString();
    }

    protected boolean ruleDetection(String token) {
        // All Vowels or All Consonants rule
        String tokenShape = getWordShape(token);
        int vowels = 0;
        int consonants = 0;
        int periods = 0;

        for (int i = 0; i < tokenShape.length(); i++) {
            char c = tokenShape.charAt(i);
            if ("vV".indexOf(c) != -1) vowels++;
            if ("cC".indexOf(c) != -1) consonants++;
            if (c == '.') periods++;
        }
        if ((vowels == 0 && consonants > 0) || (consonants == 0 && vowels > 0)) return true;
        if (tokenShape.length() > 1 && tokenShape.length() - 1 <= periods) return true;
        return false;
    }

    protected boolean modelDetection(String token) {
        dataRaw.setClassIndex(dataRaw.numAttributes() - 1);
        Instance inst = new DenseInstance(3);
        inst.setValue(word, token);
        inst.setValue(shape, getWordShape(token));
        inst.setDataset(dataRaw);
        Double outcome = null;
        try {
            outcome = cls.classifyInstance(inst);
        } catch (Exception e) {
            System.err.println(e);
        }
        return outcome != 1.0;
    }

    protected void addAcronymAnnotation(AnnotationFS acronym, CAS cas) {

    }

    @Override
    public void typeSystemInit(TypeSystem ts) {

        acronymType = ts.getType(Acronym.class.getCanonicalName());
    }
}