/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.descriptor.ExternalResource;
import org.uimafit.util.ExtendedLogger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

/**
 * This annotator assigns all possible UMLS concepts to acronyms that might
 * represent a clinical concept. This is useful for concept disambiguation
 * annotators that use UMLS CUI information to CUIs is all possible CUIs
 * produced for all possible candidate long forms of a edu.umn.biomedicus.acronym. For example, the
 * edu.umn.biomedicus.acronym "AI" can have the expansions:<br>
 * <ul>
 * <li>"advanced maternal age" having cuis: C0024915|C0001792</li>
 * <li>"anti-mitochondrial antibody" having cuis: C0003242|C0312621</li>
 * </ul>
 * Therefore, this annotator will assign all four CUIs.
 * 
 * @author Robert Bill
 * 
 */
public class AcronymConceptMapper extends JCasAnnotator_ImplBase {
	public static final String BIOMEDICUS_DB = "biomedicusDb";
	@ExternalResource(key = BIOMEDICUS_DB)
	//private BiomedicusDBResource_Impl biomedicusDb;

	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/dictionaries/SemanticTypeDictionary.csv")
	private String semanticTypeDictionaryFile;

	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/edu.umn.biomedicus.acronym/AcronymManifest.csv")
	private String acronymCuiManifestUri;

	// Lookups for acronyms and semantic types
	//SemanticTypeDictionary semanticTypeDictionary;
	private AcronymManifest manifest;
	private ExtendedLogger logger;
	private Connection con = null;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

//		try {
	//		con = biomedicusDb.getConnection();
//		} catch (ResourceProcessException e1) {
//			System.err.println("Unable to get connection to Biomedicus database in AcronymConceptMapper AE.");
//			throw new ResourceInitializationException();
//		}

		// get logger
		// this.logger = getLogger();
		// this.logger.logrb(Level.INFO, "AcronymConceptMapper", "initialize",
		// "edu.umn.biomedicus.edu.umn.biomedicus.acronym.AcronymConceptMapper",
		// "acronym_concept_mapper_info_initialized");

		InputStream manifestInputStream = getClass().getClassLoader().getResourceAsStream(acronymCuiManifestUri);
		try {
			manifest = new AcronymManifest(manifestInputStream);
		} catch (IOException e) {
			System.err.println("Unable to initalize the edu.umn.biomedicus.acronym manifest. Probably check that the manifest file really exists at: "
			                                + acronymCuiManifestUri);
			e.printStackTrace();
			throw new ResourceInitializationException();
		}

		// get semantic type lookup table so that expanded names of semantic
		// types will be included in the annotation
//		InputStream dictionaryInputStream = getClass().getResourceAsStream(semanticTypeDictionaryFile);
//		try {
//			semanticTypeDictionary = new SemanticTypeDictionary(dictionaryInputStream);
//		} catch (IOException e) {
//			System.err.println("Unable to open the SemanticTypeDictionary.csv file! Check the path and try again.");
//			e.printStackTrace();
//			throw new ResourceInitializationException();
//		}
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {


            /*
		for (Acronym acr : JCasUtil.select(jCas, Acronym.class)) {
			String acronymText = acr.getCoveredText();

			// create a new UmlsConcept that covers the Acronym.
			UmlsConcept concept = new UmlsConcept(jCas, acr.getBegin(), acr.getEnd());
			concept.setAnnotSrc("UMN Acronym Annotator");
			ArrayList<Cui> cuis = new ArrayList<Cui>();
			ResultSet rs = null;
			double p = 0.0;
			String semtype = "";
			String expansion = "";
			String cuiId = "";
			try {
				rs = con.createStatement().executeQuery("SELECT * FROM acronym WHERE acronym='" + acronymText + "'");
				while (rs.next()) {
					cuiId = rs.getString(4);
					if (cuiId == null || cuiId.equals(""))
						continue;
					p = rs.getFloat(3);
					semtype = rs.getString(5);
					expansion = rs.getString(6);
					if (expansion.startsWith("MISTAKE"))
						continue;

					// put together UmlsConcept information
					Cui cui = new Cui(jCas, acr.getBegin(), acr.getEnd());
					cui.setCuiId(cuiId);
					StringArray semTypes = new StringArray(jCas, 1);
					semTypes.set(0, semtype);
					cui.setSemanticType(semTypes);
					cui.setConceptName(expansion);
					cuis.add(cui);
				}
				FSArray cuiList = new FSArray(jCas, cuis.size());
				for (int i = 0; i < cuis.size(); i++) {
					cuiList.set(i, cuis.get(i));
				}
				concept.setCuis(cuiList);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			concept.addToIndexes();
		}


		*/
	}
}