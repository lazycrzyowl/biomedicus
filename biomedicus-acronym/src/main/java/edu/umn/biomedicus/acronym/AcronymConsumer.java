package edu.umn.biomedicus.acronym;

import edu.umn.biomedicus.core.ClinicalDocument;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Sentence;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;


/**
 * Simple class to collect all the sentence annotations.
 */
public class AcronymConsumer extends CasAnnotator_ImplBase {

    public static final String PARAM_OUTPUT_FILE = "outputFile";
    private String outputFile;
    BufferedWriter acronymCollector;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        super.initialize(context);
        outputFile = (String) context.getConfigParameterValue(PARAM_OUTPUT_FILE);
        try
        {
            acronymCollector = (outputFile == null)
                    ? new BufferedWriter(new OutputStreamWriter(System.out))
                    : new BufferedWriter(new FileWriter(outputFile));
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    @Override
    public void process(CAS cas) throws AnalysisEngineProcessException
    {
        CAS system = CASUtil.getSystemView(cas);

        ClinicalDocument cd = ClinicalDocument.fromCAS(system);

        for (AnnotationFS sentence : cd.getSentences())
        {
            try {
                String sentenceText = sentence.getCoveredText().replaceAll("\\n", " ");
                acronymCollector.write(sentence.getCoveredText().replaceAll("\\n", " "));
                acronymCollector.newLine();
                acronymCollector.flush();
            } catch (IOException e)
            {
                throw new AnalysisEngineProcessException(e);
            }
        }
    }

    public void TypeSystemInit(TypeSystem typeSystem)
    {
        Type sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
    }
}
