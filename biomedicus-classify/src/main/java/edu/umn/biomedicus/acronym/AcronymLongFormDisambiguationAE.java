/* 
 Copyright 2011-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

/*
import edu.umn.biomedicus.BiomedicusDBResource_Impl;
import edu.umn.biomedicus.type.Acronym;
import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.UmlsConcept;


*/
/**
 * 
 * @author Robert Bill
 * 
 */
public class AcronymLongFormDisambiguationAE {}


/*
    public class AcronymLongFormDisambiguationAE extends JCasAnnotator_ImplBase {
	public static final String BIOMEDICUS_DB = "biomedicusDb";
	@ExternalResource(key = BIOMEDICUS_DB)
	private BiomedicusDBResource_Impl biomedicusDb;

	public static final String PARAM_MANIFEST_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                AcronymLongFormDisambiguationAE.class, "acronymCuiManifestUri");
	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/edu.umn.biomedicus.acronym/AcronymManifest.csv")
	private String acronymCuiManifestUri;

	public static final String PARAM_WINDOW_SIZE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                AcronymLongFormDisambiguationAE.class, "windowSize");
	@ConfigurationParameter(mandatory = false, defaultValue = "5", description = "The number of tokens for the window size")
	public int windowSize;

	private org.uimafit.util.ExtendedLogger logger;

	private AcronymManifest manifest = null;
	HashMap<String, String> missing = new HashMap<String, String>();
	int notInManifest = 0;
	int totalAcronyms;
	private Connection con = null;
	BufferedWriter missingCuiPairsFile = null;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		InputStream manifestInputStream = this.getClass().getResourceAsStream(acronymCuiManifestUri);
		try {
			manifest = new AcronymManifest(manifestInputStream);
		} catch (IOException e) {
			System.err.println("Unable to initalize the edu.umn.biomedicus.acronym manifest. Probably check that the manifest file really exists at: "
			                                + acronymCuiManifestUri);
			e.printStackTrace();
			throw new ResourceInitializationException();
		}

		try {
			con = biomedicusDb.getConnection();
		} catch (ResourceProcessException e) {
			System.err.println("Failed to get connection to the Biomedicus DB!");
			throw new ResourceInitializationException();
		}
		// get logger
		logger = this.getLogger();
		logger.setLevel(Level.INFO);
		logger.log(Level.INFO, "edu.umn.biomedicus.edu.umn.biomedicus.acronym.AcronymLongFormDisambiguationAE initialized.");
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		HashMap<String, UmlsConcept> conceptCollection = new HashMap<String, UmlsConcept>();
		HashMap<String, Acronym> acronymCollection = new HashMap<String, Acronym>();
		for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
			StringBuilder key = new StringBuilder();
			key.append(concept.getBegin()).append(":").append(concept.getEnd());
			conceptCollection.put(key.toString(), concept);
		}
		for (Acronym edu.umn.biomedicus.acronym : JCasUtil.select(jCas, Acronym.class)) {
			StringBuilder key = new StringBuilder();
			key.append(edu.umn.biomedicus.acronym.getBegin()).append(":").append(edu.umn.biomedicus.acronym.getEnd());
			acronymCollection.put(key.toString(), edu.umn.biomedicus.acronym);
			AcronymManifestEntry entry = manifest.getEntry(edu.umn.biomedicus.acronym.getCoveredText());
			if (entry == null)
				missing.put(edu.umn.biomedicus.acronym.getCoveredText(), "");
		}

		for (String key : acronymCollection.keySet()) {
			Acronym edu.umn.biomedicus.acronym = acronymCollection.get(key);
			UmlsConcept concept = conceptCollection.get(key);
			if (missing.containsKey(edu.umn.biomedicus.acronym.getCoveredText())) {
				notInManifest++;
			}
			AcronymManifestEntry entry = manifest.getEntry(edu.umn.biomedicus.acronym.getCoveredText());
			if (entry == null) {
				continue; // we can not comment on acronyms not in manifest.
			}



			/*
			 * String longForm = entry.getMostFrequentExpansion();
			 * edu.umn.biomedicus.acronym.setExpansion(longForm); if (concept != null &&
			 * concept.getCui() != null) { if
			 * (entry.getExpansionProbability(longForm) < 0.99) { Cui cui =
			 * (Cui) concept.getCui(); longForm = getOptimAcronym(cui); //
			 * longForm = entry.getExpansionForCui(cui.getCuiId());
			 * edu.umn.biomedicus.acronym.setExpansion(longForm);
			 * System.out.println("SETTING CALCULATED VALUE!!!!!!   '" +
			 * longForm + "' because found cui=" + cui.getCuiId() + "     " +
			 * notInManifest); } }
			 *
		}
	}

	public void getOptimAcronym(Cui cui) {
		String optimExpansion = "";
		double maxExpansionRelatedness = 0.0;
		for (String nym : manifest.getKeys()) {
			double maxCuiRelatedness = 0.0;
		}
	}

	/**
	 * Get the sense for the given {@link UmlsConcept} using the specified
	 * windows size of preceding and following concepts. The return value is the
	 * {@link Cui} with the highest mean relatedness to other concepts in the
	 * window.
	 * 
	 * @param concept
	 *            The target {@link UmlsConcept} requiring disambiguation.
	 * @param preceding
	 *            The {@link UmlsConcept} annotations (of count equal to the
	 *            window size specified when this class was instantiated) that
	 *            precede the targeted/ambiguous {@link UmlsConcept}.
	 * @param following
	 *            The following {@link UmlsConcept} annotations (of count equal
	 *            to the window size specified when this class was instantiated)
	 *            that follow the targeted/ambiguous {@link UmlsConcept}.
	 * @return CUI instance
	 */


    /*
	@SuppressWarnings("unused")
	private Cui getSense(List<Cui> targetCandidates, List<UmlsConcept> preceeding, List<UmlsConcept> following,
	                                String method) {
		// Search for maximum sequence of relatedness scores.
		// step 1, get relatedness between history and target
		// step 2, get relatedness between following and target
		// step 3, get max of winning scores
		// step 4, return cui with max ( max relatedness score )
		// step 5, save SD to calculate confidence score in future versions

		List<List<Cui>> context = new ArrayList<List<Cui>>();
		for (int i = 0; i < windowSize; i++) { // loop through CUIs surrounding
			                                   // candidate
			if (preceeding.size() > i) {
				UmlsConcept pastContext = preceeding.get(preceeding.size() - i - 1);
				context.add(getCandidateSenses(pastContext, 800));
			}
			if (following.size() > i) {
				UmlsConcept futureContext = following.get(i);
				context.add(getCandidateSenses(futureContext, 800));
			}
		}
		Cui sense = emitTargetSense(targetCandidates, context);
		return sense;
	}
    */
	/**
	 * Get an array of candidate senses (CUIs) for a {@link UmlsConcept}.
	 * 
	 * @param concept
	 *            The target {@link UmlsConcept}.
	 * @param threshold
	 *            The lowest MetaMap score that should be considered.
	 * @return an array of CUIs as [ {@link Cui}].
	 */


        /*
	private List<Cui> getCandidateSenses(UmlsConcept concept, int threshold) {
		FSArray cuis = concept.getCuis();
		if (cuis.size() == 0)
			logger.log(Level.INFO, "No cuis found in context.");
		ArrayList<Cui> candidates = new ArrayList<Cui>();
		logger.log(Level.INFO, "Retrieving candidate senses for concept '" + concept.getCoveredText() + "'");

		for (int i = 0; i < cuis.size(); i++) {
			Cui cui = (Cui) cuis.get(i);
			if (cui != null) {
				logger.log(Level.INFO, "Candidate CUI " + cui.getCuiId() + " has score " + cui.getScore());
				// TODO: fix threshold testing
				// if (cui.getScore() > threshold) {
				candidates.add(cui);
				// } else {
				// System.out.printf("      Score Too Low for %s becuase it is %d\n",
				// cui.getCuiId(), cui.getScore());
				// }
			} else {
				System.out.print("      Candidate CUI is null.\n");
			}
		}
		return Collections.checkedList(candidates, Cui.class);
	}


*/


	/**
	 * Get the {@link Cui} with the highest max relatedness score to other
	 * concepts in the window.
	 * 
	 * @param concept
	 *            The target {@link UmlsConcept}.
	 * @param threshold
	 *            The lowest MetaMap score that should be considered.
	 * @return an array of CUIs as [ {@link Cui}].
	 */




/*
	private Cui emitTargetSense(List<Cui> target, List<List<Cui>> context) {
		if (target == null || target.size() == 0)
			throw new IllegalArgumentException("Target candidate list is null or empty!");
		if (context == null || context.size() == 0) {
			logger.log(Level.WARNING, "Context candidate list empty! Falling back to MetaMap scores");
			int max = 0;
			Cui sense = null;
			for (Cui cui : target) {
				int score = cui.getScore();
				if (score > max) {
					max = score;
					sense = cui;
				}
			}
			// there's no context, so must pick Cui with highest MetaMap score.
			return sense;
		}

		// There is a context, so optimize for maximum relatedness
		HashMap<String, Double> measureCache = new HashMap<String, Double>();
		int survivor = 0; // The index of the surviving sense (cui)
		double max = 0; // The relatedness score of the surviving sense
		for (int i = 0; i < target.size(); i++) {
			double sumOfRelatedness = 0;
			String t = target.get(i).getCuiId();

			for (int j = 0; j < context.size(); j++) {

				// if a concept in the context is also ambiguous, only consider
				// the closest-related candidate in the max relatedness of the
				// target concept.
				double maxNeighbor = 0;
				for (Cui cui : context.get(j)) {
					String c = cui.getCuiId();
					String cui1 = null;
					String cui2 = null;
					// order the pair low to high for DB lookup.
					if (t.compareTo(c) > 0) {
						cui1 = c;
						cui2 = t;
					} else {
						cui1 = t;
						cui2 = c;
					}
					String key = cui1 + ":" + cui2;
					double m = -1;
					if (measureCache.containsKey(key)) {
						m = measureCache.get(key);
					} else {
						m = getSimilarity(cui1, cui2);
						measureCache.put(key, m);
					}

					// Check for new maximum
					if (m > maxNeighbor) {
						maxNeighbor = m;
					}
				}
				sumOfRelatedness += maxNeighbor;
			}
			double average = sumOfRelatedness / context.size();

			if (average > max) {
				max = average;
				survivor = i;
			}
		}
		return target.get(survivor);
	}

	public double getSimilarity(String cui1, String cui2) {
		String sql = "SELECT res, lin from similarity where cui1=? and cui2=?";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, cui1);
			pstmt.setString(2, cui2);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				double res = rs.getDouble(1);
				double lin = rs.getDouble(2);
				// return -2.978 - 0.596 * lin + 2.036 * res;
				return -0.051 + 1.815 * lin + 7.66 * res;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
		try {
			missingCuiPairsFile.write(cui1 + "<>" + cui2 + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}
}
              */