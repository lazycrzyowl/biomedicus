/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.util.cr.XReader;
import org.uimafit.factory.CollectionReaderFactory;

/*
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.corpora.CorpusFactory_ImplBase;
  */

/**
 * The file is a factory for the edu.umn.biomedicus.acronym corpus developed at UMN. The edu.umn.biomedicus.acronym
 * corpus is not currently publicly available, but after de-identification, it
 * might be.
 * 
 * @author Robert Bill
 * 
 */

public class AcronymFactory {
}
    /*
} extends CorpusFactory_ImplBase {

	// private String xmiDirectory = "resources/corpora/edu.umn.biomedicus.acronym/xmi";
	protected File namesDirectory = null; // new
	                                      // File("resources/corpora/edu.umn.biomedicus.acronym/names");
	private String xmiDirectory = null;

	public AcronymFactory(String corpusFolder) {
		xmiDirectory = new File(corpusFolder, "xmi").toString();
		namesDirectory = new File(xmiDirectory, "../names");

	}

	public CollectionReader createReader() throws ResourceInitializationException {
		String fileName = new File(namesDirectory, "edu.umn.biomedicus.acronym.txt").getPath();
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_NAME_FILES_FILE_NAMES,
		                                new String[] { fileName });
	}

	public CollectionReader createTrainReader() throws ResourceInitializationException {
		String fileName = new File(namesDirectory, "train.txt").getPath();
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_NAME_FILES_FILE_NAMES,
		                                new String[] { fileName });
	}

	public CollectionReader createTestReader() throws ResourceInitializationException {
		String fileName = new File(namesDirectory, "test.txt").getPath();
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_NAME_FILES_FILE_NAMES,
		                                new String[] { fileName });
	}

	private String getFoldFileName(int fold) {
		if (fold == 10) {
			return new File(namesDirectory, "fold10.txt").getPath();
		} else {
			return new File(namesDirectory, "fold0" + fold + ".txt").getPath();
		}
	}

	public CollectionReader createTrainReader(int fold) throws ResourceInitializationException {
		verifyFoldValue(fold);
		List<String> fileNamesList = new ArrayList<String>();
		for (int i = 1; i <= 10; i++) {
			if (i != fold) {
				fileNamesList.add(getFoldFileName(i));
			}
		}

		String[] fileNames = fileNamesList.toArray(new String[fileNamesList.size()]);

		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_NAME_FILES_FILE_NAMES,
		                                fileNames);
	}

	public CollectionReader createTestReader(int fold) throws ResourceInitializationException {
		verifyFoldValue(fold);
		String fileName = getFoldFileName(fold);
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_NAME_FILES_FILE_NAMES,
		                                new String[] { fileName });
	}

	public int numberOfFolds() {
		return 10;
	}

}
 */