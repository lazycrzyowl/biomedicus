package edu.umn.biomedicus.sections;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Section;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is a UIMA analysis engine that writes section and subsection annotations to the UIMA CAS by using regular
 * expressions. The descriptor file for this annotator can accept 3 parameters for regular expressions.
 * <p/>
 * <p>PARAM_INITIAL_SECTION_PATTERN: The regular expression to use when looking for the very first <tt>Section</tt> in
 * the document. </p>
 * <p/>
 * <p>PARAM_SECTION_PATTERN: The regular expression to use when looking for all subsequent <tt>Section</tt> instances in
 * the document. </p>
 * <p/>
 * <p>PARAM_SUBSECTION_PATTERN: The regular expression to use when looking for all the <tt>Subsection</tt> instances
 * within a <tt>Section</tt> body </p>
 * <p/>
 * <p>This class is part of the <a href="https://bitbucket.org/nlpie/biomedicus"> UMN BioMedICUS Application
 * Collections Framework</a>.
 *
 * @author Robert Bill
 * @since January 24, 2014
 */

public class RegexSectionAE extends CasAnnotator_ImplBase {

    public static final String PARAM_SECTION_PATTERN = "initialSectionPattern";
    public static final String PARAM_INITIAL_SECTION_PATTERN = "initialSectionPattern";

    public static final String SECTION_LEVEL_FEATURE_NAME = "level";
    public static final String SECTION_HEADING_FEATURE_NAME = "heading";
    public static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
    public static final String SECTION_HAS_SUBSECTIONS_FEATURE_NAME = "hasSubsections";
    private static final String SECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Section";


    private static final int SECTION_LEVEL = 0;
    private static final int SUBSECTION_LEVEL = 1;
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(RegexSectionAE.class);
    @NotNull
    private Type sectionType;
    @NotNull
    private Feature sectionContentStartFeature;
    @NotNull
    private Feature sectionHasSubsectionsFeature;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Pattern initialSectionPattern = Pattern.compile("([A-Z &]+?)[:\n]");
    @NotNull
    private Pattern sectionPattern = Pattern.compile("\n{2}([A-Z &]+?)[:\n]");
    @NotNull
    private Pattern subsectionPattern = Pattern.compile("([A-Z &]+?)[:\n]");
    @NotNull
    private Feature sectionHeadingFeature;

    /**
     * The getPattern method returns the regex pattern provided within the configuration parameter settings if they
     * exist, and otherwise returns the default pattern.
     */
    private static Pattern getPattern(UimaContext context, String paramKey, Pattern defaultPattern)
    {

        String parameterValue = (String) context.getConfigParameterValue(paramKey);

        if (parameterValue != null)
        {
            return Pattern.compile(parameterValue);
        }

        return defaultPattern;

    }

    /**
     * The initializer for the RegexSectionAE checks for the optional regular expressions: <ul>
     * <li>PARAM_INITAL_SECTION_PATTERN</li> <li>PARAM_SECTION_PATTERN</li> <li>PARAM_SUBSECTION_PATTERN</li> </ul>
     *
     * @param context The UimaContext automatically provided by the UIMA system.
     * @throws org.apache.uima.resource.ResourceInitializationException
     */
    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {

        super.initialize(context);

        // Check for regular expression provided as parameters

        initialSectionPattern = getPattern(context, PARAM_INITIAL_SECTION_PATTERN, initialSectionPattern);
        sectionPattern = getPattern(context, PARAM_SECTION_PATTERN, sectionPattern);
        subsectionPattern = getPattern(context, PARAM_SECTION_PATTERN, subsectionPattern);
    }

    /**
     * The process method applies annotations to the CAS.
     *
     * @param aCAS The CAS object supplied by the UIMA system.
     */
    public void process(CAS aCAS)
    {
        // Currently, all system-generated annotations are in the System view.
        CAS systemView = CASUtil.getSystemView(aCAS);

        List<AnnotationFS> sectionList = getSections(systemView);
        for (AnnotationFS section : sectionList)
        {
            boolean hasSubsections = annotateSubsections(systemView, section);
            if (hasSubsections)
            {
                section.setBooleanValue(sectionHasSubsectionsFeature, true);
            }
        }
    }

    /**
     * The getSections method finds all the sections (as defined by the section regular expression(s)) and returns a
     * List of all sections found
     *
     * @param aCAS The CAS for the current document.
     * @return List containing AnnotationFS instances.
     */
    private List<AnnotationFS> getSections(CAS aCAS)
    {

        @NotNull
        final String documentText = aCAS.getDocumentText();

        @NotNull
        int documentLength = documentText.length();

        @NotNull
        ArrayList<AnnotationFS> sectionCollector = new ArrayList<>();

        // Use the initial regex to get the first section. This is incomplete because must
        // find the start of the next section to find the end of this first section.
        Matcher initialSectionMatcher = initialSectionPattern.matcher(documentText);
        boolean found = initialSectionMatcher.find();

        // set some empty defaults to use in case there are no detectable sections. This will result in the
        // entire document being annotated as an untitled section.
        String heading = "";
        int start = 0;
        int contentStart = 0;
        int end = documentLength - 1;

        if (found)
        {
            heading = initialSectionMatcher.group(1);
            contentStart = initialSectionMatcher.end();
        }

        // The 'lookAheadSection' has the section.begin, but only the completed section has the section.end.
        AnnotationFS lookAheadSection = beginAnnotation(aCAS, SECTION_LEVEL, heading, start, contentStart, end);
        AnnotationFS completedSection = null;

        // Get the subsequent sections
        int currentPosition = contentStart;
        Matcher sectionMatcher = sectionPattern.matcher(documentText);

        while (sectionMatcher.find())
        {
            start = sectionMatcher.start();

            // skip matches found previous to the section content's start
            if (start < currentPosition)
            {
                continue;
            }

            contentStart = sectionMatcher.end();
            heading = sectionMatcher.group(1);

            // Collect the completed section now that it has an end feature
            completedSection = endAnnotation(aCAS, lookAheadSection, start);
            if (contentStart != -1 && start != -1)
            sectionCollector.add(completedSection);
            String debugSectionHeading = completedSection.getStringValue(sectionHeadingFeature);
            int debugContentStart = completedSection.getIntValue(sectionContentStartFeature);
            int debugContentEnd = completedSection.getEnd();

            String debugSectionText = documentText.substring(debugContentStart, debugContentEnd);


            lookAheadSection = beginAnnotation(aCAS, SECTION_LEVEL, heading, start, contentStart, end);
            currentPosition = contentStart;
        }

        // add the final section to indexes
        completedSection = endAnnotation(aCAS, lookAheadSection, end);
        if (end != -1)
            sectionCollector.add(completedSection);

        return sectionCollector;

    }

    /**
     * The annotateSubsections method uses the regular expression for subsection identification on the content of the
     * section annotation supplied in the second parameter. It is not a 'getter' because it only annotates the
     * subsections and does not return them like the <tt>getSections</tt> method does for its respective annotation
     * type.
     *
     * @param aCAS    The CAS for the current document.
     * @param section The <tt>Section</tt> annotation that may contain subsections.
     */
    private boolean annotateSubsections(CAS aCAS, AnnotationFS section)
    {

        @NotNull
        String documentText = aCAS.getDocumentText();

        @NotNull
        int sectionStart = section.getBegin();
        int sectionEnd = section.getEnd();
        int sectionContentStart = section.getIntValue(sectionContentStartFeature);

        @NotNull
        String sectionFullText = documentText.substring(sectionStart, sectionEnd);
        String sectionText = documentText.substring(sectionContentStart, section.getEnd());
        int sectionLength = sectionText.length();
        int currentLocation = sectionContentStart;

        Matcher subsectionMatcher = subsectionPattern.matcher(sectionText);


        // If not subsections are detected, return false.
        boolean found = subsectionMatcher.find();
        if (!found || subsectionMatcher.group(1).trim().equals(""))
        {
            return false;
        }

        // get attributes relative to documentText for annotating
        int start = currentLocation + subsectionMatcher.start();
        int end = currentLocation + sectionLength - 1;
        int contentStart = currentLocation + subsectionMatcher.end();
        String heading = subsectionMatcher.group(1);

        AnnotationFS lookAheadSubsection = beginAnnotation(aCAS, SUBSECTION_LEVEL,
                heading, start, contentStart, sectionEnd);
        AnnotationFS completedSubsection = null;

        while (subsectionMatcher.find())
        {
            start = currentLocation + subsectionMatcher.start();
            contentStart = currentLocation + subsectionMatcher.end();

            heading = subsectionMatcher.group(1);

            completedSubsection = endAnnotation(aCAS, lookAheadSubsection, start);
            lookAheadSubsection = beginAnnotation(aCAS, SUBSECTION_LEVEL, heading, start, contentStart, end);
        }
        completedSubsection = endAnnotation(aCAS, lookAheadSubsection, end);

        return true;  // found subsections

    }

    /**
     * The beginAnnotation method creates an incomplete annotation of the type specified by the 2nd parameter. The
     * annoation is incomplete because the <tt>end</tt> feature of all sections and subsections is unknown until the
     * subsequent section/subsection is found, or the end of the document is found. The <tt>end</tt> feature is set to
     * the document end during instantiation for convenience, but if the annotation's end is <i>not</i> the document
     * end, then the endAnnotation method must be called on the annotation to set the correct <tt>end</tt> value and to
     * add the annotation to the CAS index.
     *
     * @param aCAS         The CAS for the current document.
     * @param sectionLevel An integer representing the section hierarchy. 0=section, 1=subsection.
     * @param heading      The String heading (if found) that identifies this section.
     * @param contentStart int value for where the content starts (or the end of the heading String).
     * @param end          The maximum possible <tt>end</tt> value that the annotation could have. Either the index of
     *                     the last document character, or the index of the last Section character.
     * @return An AnnotationFS of the type specified by the second parameter.
     */
    private AnnotationFS beginAnnotation(CAS aCAS, int sectionLevel, String heading,
                                         int start, int contentStart, int end)
    {

        // Create annotation, but don't add to index here (sectionEnd is unknown at this point).
        AnnotationFS section = aCAS.createAnnotation(sectionType, start, end);

        // Retrieve the features that need to be set
        Feature headingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        Feature sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
        Feature contentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);

        // Set feature values
        section.setStringValue(headingFeature, heading);
        section.setIntValue(contentStartFeature, contentStart);
        section.setIntValue(sectionLevelFeature, sectionLevel);

        return section;

    }

    private AnnotationFS endAnnotation(CAS aCAS, AnnotationFS annotation, int location)
    {

        // Retrieve the feature that need to be set
        Feature endFeature = sectionType.getFeatureByBaseName("end");

        // Set feature value
        annotation.setIntValue(endFeature, location);

        aCAS.addFsToIndexes(annotation);

        return annotation;

    }

    private Section commitSubsection(Section s, int location)
    {

        s.setEnd(location);
        s.addToIndexes();
        return s;

    }

    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {

        // Initialize types
        super.typeSystemInit(typeSystem);
        sectionType = typeSystem.getType(SECTION_ANNOTATION_NAME);
        sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
        sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        sectionHasSubsectionsFeature = sectionType.getFeatureByBaseName(SECTION_HAS_SUBSECTIONS_FEATURE_NAME);
    }

}