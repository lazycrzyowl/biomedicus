/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.cache;

import org.apache.uima.UimaContext;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.Resource_ImplBase;
import org.apache.uima.resource.SharedResourceObject;
import redis.clients.jedis.Jedis;

/**
 * This class connection to a redis service for caching.
 */
public class RedisCacheResource_Impl extends Resource_ImplBase implements SharedResourceObject
{
	public static final String PARAM_REDIS_HOST = "cacheHost";
	private String cacheHost = "localhost";
	Jedis jedis;

	public void set(String key, String value) {
		if (jedis.isConnected()) {
			jedis.set(key, value);
		} else {
			this.jedis = new Jedis(cacheHost);
			jedis.set(key, value);
		}
	}

	public String get(String key) {
		if (!jedis.isConnected()) {
			this.jedis = new Jedis(cacheHost);
		}
		return jedis.get(key);
	}

	public void destroy() {
		jedis.disconnect();
	}

    @Override
    public void load(DataResource aData) throws ResourceInitializationException {
        // Get initial connection
        UimaContext context = aData.getUimaContext();
        String host;
        host = (context == null) ? "localhost" : (String) context.getConfigParameterValue("host");
        connect(host);
    }

    public void connect(String host)
    {
        this.jedis = new Jedis(cacheHost);
        jedis.connect();
    }

    public static RedisCacheResource_Impl build(String host)
    {
        RedisCacheResource_Impl cache = new RedisCacheResource_Impl();
        cache.connect(host);
        return cache;
    }

    public boolean isConnected()
    {
        return jedis.isConnected();
    }

    public void disconnect()
    {
        jedis.disconnect();
    }
}
