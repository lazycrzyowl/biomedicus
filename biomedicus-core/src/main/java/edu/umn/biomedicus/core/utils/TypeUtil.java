package edu.umn.biomedicus.core.utils;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;

public class TypeUtil {
	
    public static Type getType(CAS aCAS, Class<? extends AnnotationFS> typeClass)
    {
        TypeSystem typeSystem = aCAS.getTypeSystem();
        String typeName = typeClass.getCanonicalName();
        Type type = typeSystem.getType(typeName);
        return type;
    }
}
