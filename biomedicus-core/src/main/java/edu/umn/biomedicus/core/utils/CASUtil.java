package edu.umn.biomedicus.core.utils;

import edu.umn.biomedicus.core.Views;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.CASRuntimeException;
import org.apache.uima.cas.admin.CASFactory;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.jcas.JCas;


public class CASUtil
{

    private static CAS getView(CAS aCAS, String viewName)
    {
        CAS cas = null;
        try
        {
            cas = aCAS.getView(viewName);
        } catch (CASRuntimeException e)
        {
            cas = aCAS.createView(viewName);
        }
        return cas;
    }

    public static CAS getSystemView(CAS aCAS)
    {
        return getView(aCAS, Views.SYSTEM_VIEW);
    }

    public static CAS getGoldView(CAS aCAS)
    {
        return getView(aCAS, Views.GOLD_VIEW);
    }

    public static CAS getMetaDataView(CAS aCAS) { return getView(aCAS, Views.METADATA_VIEW); }

    public static CAS createEmptyCAS() {
        CAS aCAS = CASFactory.createCAS().getCAS();
        return aCAS;

    }

    public static JCas getJCas(AnnotationFS annotation)
    {
        CAS cas = annotation.getView();
        String viewName = cas.getViewName();
        try {
            return cas.getJCas();
        } catch (CASException e)
        {
            return null;
        }
    }
}
