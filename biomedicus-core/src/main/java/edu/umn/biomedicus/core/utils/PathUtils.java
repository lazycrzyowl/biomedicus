package edu.umn.biomedicus.core.utils;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class PathUtils {

  /**
   * The getResourceAsStream utility method accepts a String parameter that can either be the
   * String that identifies a classpath resource or a String that identifies a file outside
   * of the classpath. If the name is not found on the classpath, then the String is assumed
   * to be a fully qualified file path.
   *
   * @param resourceName
   * A <tt>String</tt> that specifies a classpath resource or a fully qualified file path.
   *
   * @return
   * An InputStream for the resource.
   *
   * @throws java.io.IOException
   */
  public static InputStream getResourceAsStream(String resourceName) throws IOException {
    InputStream resourceStream = null;

    if (PathUtils.class.getClass().getClassLoader().getResource(resourceName) == null) {
      Path p = Paths.get(resourceName);
      if (Files.exists(p))
        resourceStream = Files.newInputStream(p, StandardOpenOption.READ);
    } else {
      resourceStream = PathUtils.class.getResourceAsStream(resourceName);
    }

    return resourceStream;
  }

    public static File resourceToTempFile(URL resource) throws IOException
    {
        String name = new File(resource.getFile()).getName();
        String tDir = System.getProperty("java.io.tmpdir");
        File file = new File(tDir, name);
        file.deleteOnExit();
        FileUtils.copyURLToFile(resource, file);
        return file;
    }

}
