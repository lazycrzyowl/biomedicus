package edu.umn.biomedicus.core.featureranges;

import org.apache.uima.cas.text.AnnotationFS;

import java.util.List;

public interface FeatureRange {
    public List<AnnotationFS> getFeatureRange();
}
