/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.core.featureranges;

import edu.umn.biomedicus.type.Token;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class DocumentFeatureRange
{
    @NotNull
    Type documentType;
    @NotNull
    Type tokenType;
    @NotNull
    Type sentenceType;
    @NotNull
    Feature posFeature;
    @NotNull
    Feature clinicalClassFeature;
    @NotNull
    CAS view;
    @NotNull
    AnnotationFS documentAnnotation;


    public DocumentFeatureRange(CAS view)
    {
        this.view = view;
        this.documentAnnotation = view.getDocumentAnnotation();
    }

    public List<CoreLabel> getCoreLabelTokens()
    {
        TypeSystem ts = view.getTypeSystem();
        Type tokenType = ts.getType(Token.class.getCanonicalName());
        List<CoreLabel> tokens = new ArrayList<CoreLabel>();
        for (AnnotationFS token : AnnotationUtils.getAnnotations(view, tokenType)) {
            CoreLabel coreLabel = new CoreLabel();
            coreLabel.set(CoreAnnotations.CharacterOffsetBeginAnnotation.class, token.getBegin());
            coreLabel.set(CoreAnnotations.CharacterOffsetEndAnnotation.class, token.getEnd());
            coreLabel.setWord(token.getCoveredText());
            tokens.add(coreLabel);
        }
        return tokens;
    }
}
