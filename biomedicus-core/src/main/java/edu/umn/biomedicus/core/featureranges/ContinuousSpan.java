package edu.umn.biomedicus.core.featureranges;

import java.util.HashMap;
import java.util.Map;

public class ContinuousSpan {
    protected int start;
    protected int end;
    protected String label;
    protected Map<String, String> attributes;


    public ContinuousSpan(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public ContinuousSpan setStart(int start) {
        this.start = start;
        return this;
    }

    public int getEnd() {
        return end;
    }

    public ContinuousSpan setEnd(int end) {
        this.end = end;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public ContinuousSpan setLabel(String label) {
        this.label = label;
        return this;
    }

    public ContinuousSpan setAttribute(String key, String value)
    {
        if (attributes == null)
            attributes = new HashMap<>();
        attributes.put(key, value);
        return this;
    }

    public String getAttribute(String key)
    {
        return (attributes == null) ? null : attributes.get(key);
    }

    public Map getAttributeMap() { return attributes; }

    public boolean contains(ContinuousSpan other) {
        return (start <= other.getStart() && end >= other.getEnd());
    }
}
