package edu.umn.biomedicus.core.utils;


public class Span {
  protected int start;
  protected int end;
  protected String label;


  public Span(int start, int end) {
    this.start = start;
    this.end = end;
  }

  public Span() {}

  public Span setLabel(String label) {
      this.label = label;
      return this; }

  public Span setStart(int start) {
    this.start = start;
      return this;
  }

  public Span setEnd(int end) {
    this.end = end;
      return this;
  }

  public int getStart() {
    return start;
  }

  public int getEnd() { return end; }

  public String getLabel() { return label; }

  public boolean contains(Span other)
  {
      return (start <= other.getStart() && end >= other.getEnd());
  }
}
