package edu.umn.biomedicus.core.servicemanagement;


import edu.umn.biomedicus.DefaultConfig;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.aae.controller.AnalysisEngineController;
import org.apache.uima.aae.jmx.monitor.JmxMonitor;
import org.apache.uima.adapter.jms.JmsConstants;
import org.apache.uima.adapter.jms.activemq.SpringContainerDeployer;
import org.apache.uima.adapter.jms.service.UIMA_Service;
import org.apache.uima.resourceSpecifier.factory.ServiceContext;
import org.apache.uima.resourceSpecifier.factory.impl.ServiceContextImpl;
import org.apache.uima.util.Level;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HPCServiceDeployer {
    private static final Class CLASS_NAME = UIMA_Service.class;
    private UIMA_Service service = new UIMA_Service();

    public HPCServiceDeployer(CompositeConfiguration config)
    {
        Map<String, Object> appCtx = BiomedicusContextFactory.build(config);
        List<String> argsBuilder = new ArrayList<>();
        for (String deploymentDescriptor : config.getStringArray("dd"))
        {
            argsBuilder.add("-dd");
            argsBuilder.add(deploymentDescriptor.toString());
        }

        argsBuilder.add("-saxonURL");
        argsBuilder.add((String) appCtx.get(UimaAsynchronousEngine.DD2SpringXsltFilePath));
        argsBuilder.add("-xslt");
        argsBuilder.add((String) appCtx.get(UimaAsynchronousEngine.SaxonClasspath));


    }

    public static void main(String[] args) throws Exception {
        CompositeConfiguration config = new DefaultConfig().getConfig();
        UIMA_Service service = new UIMA_Service();
        Options options = ServiceDeploymentOptions.build(args);
        CommandLine cl = ServiceDeploymentOptions.getCLIOptions(args);
        String uimaHome = System.getenv("UIMA_HOME");
        String saxonPath = Paths.get(uimaHome, "saxon/saxon8.jar").toUri().toString();
        String dd2sprPath = Paths.get(uimaHome, "bin/dd2spring.xsl").toString();



        String agentDeploy = Paths.get(System.getenv("BIOMEDICUS_HOME"),
                "biomedicus-service/descriptors/agentDeploy.xml").toString();
//        XMLInputSource input = new XMLInputSource(agentDeploy);

        ServiceContext serviceContext = new ServiceContextImpl(
                "Pipeline", "The default BioMedICUS Pipeline", "simpleAE.xml", "PipelineRemoteQ");
//        UimaASAggregateDeploymentDescriptor dd =
//                DeploymentDescriptorFactory.createAggregateDeploymentDescriptor(serviceContext);
//        String test = dd.toXML();
        Map<String,Object> appCtx = BiomedicusContextFactory.build(config);


//        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
//        uimaAsEngine.deploy(dd, appCtx);
//        UIMAFramework.getXMLParser().parseAnalysisEngineDescription(input);

        String[] init_args = {
                "-dd", agentDeploy,
                "-saxonURL", saxonPath,
                "-xslt", dd2sprPath,
                "-defaultBrokerURL", "tcp://localhost:61616"
        };
        // parse command args and run dd2spring to generate spring context
        // files from deployment descriptors
        String contextFiles[] = service.initialize(init_args);
        // If no context files generated there is nothing to do
        if (contextFiles == null) {
            return;
        }
        // Deploy components defined in Spring context files. This method blocks until
        // the container is fully initialized and all UIMA-AS components are succefully
        // deployed.
        SpringContainerDeployer serviceDeployer = service.deploy(contextFiles);

        if (serviceDeployer == null) {
            System.out.println(">>> Failed to Deploy UIMA Service. Check Logs for Details");
            System.exit(1);
        }
        // remove temporary spring context files generated from DD
        for (String contextFile : contextFiles) {
            File file = new File(contextFile);
            if (file.exists()) {
                file.delete();
            }
        }
        // Add a shutdown hook to catch kill signal and to force quiesce and stop
        ServiceShutdownHook shutdownHook = new ServiceShutdownHook(serviceDeployer);
        Runtime.getRuntime().addShutdownHook(shutdownHook);
        // Check if we should start an optional JMX-based monitor that will provide service metrics
        // The monitor is enabled by existence of -Duima.jmx.monitor.interval=<number> parameter. By
        // default
        // the monitor is not enabled.
        String monitorCheckpointFrequency;
        if ((monitorCheckpointFrequency = System.getProperty(JmxMonitor.SamplingInterval)) != null) {
            // Found monitor checkpoint frequency parameter, configure and start the monitor.
            // If the monitor fails to initialize the service is not effected.
            service.startMonitor(Long.parseLong(monitorCheckpointFrequency));
        }

        AnalysisEngineController topLevelControllor = serviceDeployer.getTopLevelController();
        String prompt = "Press 'q'+'Enter' to quiesce and stop the service or 's'+'Enter' to stop it now.\n" +
                "Note: selected option is not echoed on the console.";
        if (topLevelControllor != null) {
            System.out.println(prompt);
            // Loop forever or until the service is stopped
            while (!topLevelControllor.isStopped()) {
                if (System.in.available() > 0) {
                    int c = System.in.read();
                    if (c == 's') {
                        service.stopMonitor();
                        serviceDeployer.undeploy(SpringContainerDeployer.STOP_NOW);
                        System.exit(0);
                    } else if (c == 'q') {
                        service.stopMonitor();
                        serviceDeployer.undeploy(SpringContainerDeployer.QUIESCE_AND_STOP);
                        System.exit(0);

                    } else if (Character.isLetter(c) || Character.isDigit(c)) {
                        System.out.println(prompt);
                    }
                }
                // This is a polling loop. Sleep for 1 sec
                try {
                    if (!topLevelControllor.isStopped())
                        Thread.sleep(1000);
                } catch (InterruptedException ex) {
                }
            } // while
        }
    }


    static class ServiceShutdownHook extends Thread {
        public SpringContainerDeployer serviceDeployer;

        public ServiceShutdownHook(SpringContainerDeployer serviceDeployer) {
            this.serviceDeployer = serviceDeployer;
        }

        public void run() {
            try {
                AnalysisEngineController topLevelController = serviceDeployer.getTopLevelController();
                if (topLevelController != null && !topLevelController.isStopped()) {
                    UIMAFramework.getLogger(CLASS_NAME).logrb(Level.WARNING, CLASS_NAME.getName(),
                            "run", JmsConstants.JMS_LOG_RESOURCE_BUNDLE,
                            "UIMAJMS_caught_signal__INFO", new Object[]{topLevelController.getComponentName()});
                    serviceDeployer.undeploy(SpringContainerDeployer.QUIESCE_AND_STOP);
                    Runtime.getRuntime().halt(0);
                }
            } catch (Exception e) {
                if (UIMAFramework.getLogger(CLASS_NAME).isLoggable(Level.WARNING)) {
                    UIMAFramework.getLogger(CLASS_NAME).logrb(Level.WARNING, CLASS_NAME.getName(),
                            "run", JmsConstants.JMS_LOG_RESOURCE_BUNDLE,
                            "UIMAJMS_exception__WARNING", e);
                }
            }
        }

    }
}
