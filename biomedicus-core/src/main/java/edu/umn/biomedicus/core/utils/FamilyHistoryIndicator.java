package edu.umn.biomedicus.core.utils;

import org.apache.commons.collections4.trie.PatriciaTrie;
import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * The FamilyHistoryIndicator class uses predefined indicator words useful in the detection of family history
 * statements. The primary method in this class is the findIndicator method that accepts a
 * sequence of AnnotationFS instances and determines the location of any of the indicator phrases.
 *
 */
public class FamilyHistoryIndicator {
  @NotNull
  PatriciaTrie<Boolean> trie;

  public FamilyHistoryIndicator(InputStream indicatorDataStream) throws IOException {
    trie = new PatriciaTrie<>();
    BufferedReader br = new BufferedReader(new InputStreamReader(indicatorDataStream));
    String indicatorLine;
    while ((indicatorLine = br.readLine()) != null)   {
      trie.put(indicatorLine.trim().toLowerCase(), true);
    }
  }

  public List<Span> findIndicator(List<AnnotationFS> annotationList) {
    Span indicatorSpan = null;
    StringBuilder sb = null;
    List<Span> indicators = new ArrayList<>();

    for (int i = 0; i < annotationList.size(); i++) {
      AnnotationFS token = annotationList.get(i);
      if (sb == null) {
        sb = new StringBuilder();
      } else {
        sb.append(" ");
      }
      String tokenText = token.getCoveredText().toLowerCase();
      sb.append(tokenText);

      if (trie.containsKey(sb.toString())) {
        indicatorSpan = getSpan(i, indicatorSpan);  // bad reassignment. rework this in future
      } else {
        sb = null;
        // If span is not null, save it and move to new span
        if (indicatorSpan == null) {
          continue;
        } else {
          indicatorSpan.setEnd(i);
          indicators.add(indicatorSpan);
          indicatorSpan = null;
        }

      }
    }
    return indicators;
  }

  private Span getSpan(int currentLocation, Span candidateSpan) {
    if (candidateSpan == null) {
      candidateSpan = new Span();
      candidateSpan.setStart(currentLocation);
    } else {
      candidateSpan.setEnd(currentLocation + 1);
    }

    return candidateSpan;
  }
}
