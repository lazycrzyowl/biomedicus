package edu.umn.biomedicus.core.exceptions;

/**
 * This is the superclass for all exceptions in BioMedICUS.
 * <p>
 * <code>BiomedicusException</code> extends {@link Exception}.
 */
public class BiomedicusException extends Exception {

    private static final long serialVersionUID = 7521732353239537026L;

    /**
     * Creates a new exception with a null message.
     */
    public BiomedicusException() {
        super();
    }

    /**
     * Creates a new exception with the specified cause and a null message.
     *
     * @param aCause
     *          the original exception that caused this exception to be thrown, if any
     */
    public BiomedicusException(Throwable aCause) {
        super(aCause);
    }

    public BiomedicusException(String message, String... args)
    {
        super(String.format(message, args));
    }
}
