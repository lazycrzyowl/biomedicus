/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.core;

import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Sentence;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ClinicalDocument {

    @NotNull
    CAS cas;
    CAS systemView;
    TypeSystem ts;
    AnnotationIndex ai;

    private ClinicalDocument(CAS cas)
    {
        if (cas.getViewName().equals(Views.SYSTEM_VIEW))
        {
            systemView = cas;
        }
        else
        {
            systemView = CASUtil.getSystemView(cas);
        }
        ai = systemView.getAnnotationIndex();
        ts = cas.getTypeSystem();

    }

    public Collection<AnnotationFS> getSentences()
    {
        Type sentenceType = ts.getType(Sentence.class.getCanonicalName());
        return CasUtil.select(systemView, sentenceType);
    }

    public static ClinicalDocument fromCAS(CAS cas)
    {
        return new ClinicalDocument(cas);
    }

}
