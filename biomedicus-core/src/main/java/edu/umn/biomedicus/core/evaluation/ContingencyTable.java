/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.core.evaluation;


import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

/**
 * A basic contingency table
 */
public class ContingencyTable {

    public static final String TP = "tp";
    public static final String FP = "fp";
    public static final String TN = "tn";
    public static final String FN = "fn";

    @NotNull
    HashMap<String, MutableInt> table;

    public ContingencyTable(HashMap<String, String> gold, HashMap<String, String> system)
    {
        initTable();
        for (String key : gold.keySet())
        {
            if (system.containsKey(key))
            {
                increment(ContingencyTable.TP);
            } else
            {
                increment(ContingencyTable.FN);
                String coveredText = gold.get(key);
                System.out.println(String.format("(FN)  key: '%s'   text: '%s'  is not in system view.",
                        key, coveredText));
            }
        }
        for (String key : system.keySet())
        {
            if (!gold.containsKey(key))
            {
                increment(ContingencyTable.FP);
                String coveredText = system.get(key);
                System.out.println(String.format("(FP)  key: '%s'   text: '%s'  is not in gold view.",
                        key, coveredText));
            }
        }
    }

    public ContingencyTable()
    {
        initTable();
    }

    private void initTable()
    {
        table = new HashMap<>();
        table.put(TP, new MutableInt());
        table.put(FP, new MutableInt());
        table.put(TN, new MutableInt());
        table.put(FN, new MutableInt());
    }

    public Integer increment(String quadrant)
    {
        if (table.containsKey(quadrant))
        {
            MutableInt count = table.get(quadrant);
            count.increment();
            return count.toInt();
        }
        return null;
    }

    public Integer get(String quadrant)
    {
        return table.get(quadrant).toInt();
    }

    public double getPrecision()
    {
        int totalPositive = table.get(TP).toInt() + table.get(FP).toInt();
        double precision = (double) table.get(TP).toInt() / totalPositive;
        return precision;
    }

    public double getRecall()
    {
        int totalPositive = table.get(TP).toInt() + table.get(FN).toInt();
        double precision = (double) table.get(TP).toInt() / totalPositive;
        return precision;
    }

    public double getFScore()
    {
        double precision = getPrecision();
        double recall = getRecall();
        return 2 * ((precision * recall)/(precision + recall));
    }

    class MutableInt {
        int value = 0;

        public void increment()
        {
            ++value;
        }

        public int toInt()
        {
            return value;
        }
    }
}
