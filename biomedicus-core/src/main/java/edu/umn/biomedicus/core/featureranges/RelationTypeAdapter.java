/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.core.featureranges;

import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.FamilyHistoryConstituent;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;

public class RelationTypeAdapter
{
    @NotNull
    private CAS view;
    @NotNull
    private Type relationType;
    @NotNull
    private AnnotationFS annotation;

    public RelationTypeAdapter(CAS view, int begin, int end)
    {
        this.view = view;
        typeSystemInit(view.getTypeSystem());
        annotation = view.createAnnotation(relationType, begin, end);
    }

    public RelationTypeAdapter(CAS view, AnnotationFS annotation)
    {
        this.view = view;
        this.annotation = annotation;
        typeSystemInit(view.getTypeSystem());
    }

    public RelationTypeAdapter(CAS view, int begin, int end, String relationshipType)
    {
        this(view, begin, end);
        setRelationshipType(relationshipType);
    }

    public String getRelationshipType()
    {
        return annotation.getStringValue(relationType.getFeatureByBaseName("relationshipType"));
    }

    public void setRelationshipType(String relationshipType)
    {
        annotation.setStringValue(relationType.getFeatureByBaseName("relationshipType"),
                relationshipType);
    }

    public int getBegin() { return annotation.getBegin(); }
    public int getEnd() { return annotation.getEnd(); }
    public String getCoveredText() { return annotation.getCoveredText(); }

    public void typeSystemInit(TypeSystem typeSystem)
    {
        relationType = typeSystem.getType(FamilyHistoryConstituent.class.getCanonicalName());
    }
}
