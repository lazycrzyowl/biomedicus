/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.core.featureranges;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SentenceFeatureRange
{
    @NotNull
    Type tokenType;
    @NotNull
    Type sentenceType;
    @NotNull
    Feature posFeature;
    @NotNull
    Feature clinicalClassFeature;
    @NotNull
    CAS view;
    @NotNull
    AnnotationFS sentenceAnnotation;


    public SentenceFeatureRange(CAS view, AnnotationFS sentenceAnnotation)
    {
        this.view = view;
        this.sentenceAnnotation = sentenceAnnotation;
        typeSystemInit(view.getTypeSystem());
    }

    public void typeSystemInit(TypeSystem typeSystem)
    {
        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
        posFeature = tokenType.getFeatureByBaseName("pos");
        clinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
    }

    public String getClinicalClass()
    {
        return sentenceAnnotation.getStringValue(clinicalClassFeature);
    }

    public List<AnnotationFS> getCoveredTokens()
    {
        return AnnotationUtils.getCoveredAnnotations(view, sentenceAnnotation, tokenType);
    }

    public String[] getCoveredTokensAsStringArray()
    {
        return AnnotationUtils.annotationListToStringArray(getCoveredTokens());
    }

    public String[] getPOSTagsOfCoveredTokens()
    {
        List<AnnotationFS> tokens = getCoveredTokens();
        ArrayList<String> tags = new ArrayList<>();
        for (AnnotationFS token : tokens)
        {
            String tag = token.getStringValue(posFeature);
            tags.add(tag);
        }
        String[] tagArray = new String[tags.size()];
        return tags.toArray(tagArray);
    }

    public Span[] getCoveredTokensAsSpans()
    {
        List<AnnotationFS> tokens = getCoveredTokens();
        Span[] spans = new Span[tokens.size()];
        for (int i = 0; i < tokens.size(); i++)
        {
            AnnotationFS t = tokens.get(i);
            spans[i] = new Span(t.getBegin(), t.getEnd());
        }
        return spans;
    }
}
