package edu.umn.biomedicus.core;

import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.resource.ResourceInitializationException;

import java.net.URL;
import java.util.List;

public class ResourceFactory {
    private String descriptorFileReference;
    private List<ServiceProviderDescription> serviceProviders;

    public static AnalysisEngine createAnalysisEngine(URL resource,
                                                      List<ServiceProviderDescription> serviceProviders)
            throws ResourceInitializationException
    {
        ResourceSpecifier resSpec = ResourceSpecifier.fromDescriptor(resource);
        resSpec.addServiceProviders(serviceProviders);
        return UIMAFramework.produceAnalysisEngine(resSpec.getSpecifier());
    }

    public static AnalysisEngine createAnalysisEngine(URL resource)
            throws ResourceInitializationException
    {
        ResourceSpecifier resSpec = ResourceSpecifier.fromDescriptor(resource);
        return UIMAFramework.produceAnalysisEngine(resSpec.getSpecifier());
    }

    public static TypeSystem createTypeSystem(URL resource)
        throws ResourceInitializationException
    {
        ResourceSpecifier resSpec = ResourceSpecifier.fromDescriptor(resource);

        return null;
    }
}
