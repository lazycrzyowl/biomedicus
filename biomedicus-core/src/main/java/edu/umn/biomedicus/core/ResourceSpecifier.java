package edu.umn.biomedicus.core;

import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import org.apache.uima.UIMAFramework;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.ResourceManagerConfiguration;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class ResourceSpecifier {
    @NotNull
    XMLInputSource in;
    @NotNull
    org.apache.uima.resource.ResourceSpecifier specifier;

    private ResourceSpecifier(org.apache.uima.resource.ResourceSpecifier specifier)
    {
        this.specifier = specifier;
    }

    public org.apache.uima.resource.ResourceSpecifier getSpecifier() { return specifier; }

    public void addServiceProviders(List<ServiceProviderDescription> serviceProviders)
    {
        ResourceManagerConfiguration resMan = (ResourceManagerConfiguration)
                specifier.getAttributeValue("resourceManagerConfiguration");
        if (resMan == null) return;  // The specifier does not allow/use a resourcemanager

        ExternalResourceDescription[] erdList = resMan.getExternalResources();

        for (ServiceProviderDescription serviceProvider : serviceProviders)
        {
            for (ExternalResourceDescription erd : erdList)
            {
                if (erd.getName().equals(serviceProvider.getServiceName()))
                {
                    URL modelFileURL = null;
                    if (serviceProvider.getServiceProviderClassReference() != null)
                    {
                        String spClassReference = serviceProvider.getServiceProviderClassReference();
                        erd.setImplementationName(spClassReference);
                    }
                    String fileRef = serviceProvider.getServiceProviderModelFileReference();
                    if (fileRef != null)
                    {
                        modelFileURL = ResourceFactory.class.getClassLoader()
                                .getResource(serviceProvider.getServiceProviderModelFileReference());
                        erd.getResourceSpecifier().setAttributeValue("fileUrl", modelFileURL.getFile());
                    }
                }
            }
        }
    }

    public static ResourceSpecifier fromDescriptor(URL descriptor)
            throws ResourceInitializationException
    {
        XMLInputSource in = getInputSource(descriptor);
        org.apache.uima.resource.ResourceSpecifier specifier = getSpecifier(in);
        return new ResourceSpecifier(specifier);
    }

    public static XMLInputSource getInputSource(URL resource)
            throws ResourceInitializationException
    {
        XMLInputSource in;
        try
        {
            in = new XMLInputSource(resource);
        }
        catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
        return in;
    }

    protected static org.apache.uima.resource.ResourceSpecifier getSpecifier(XMLInputSource in) throws ResourceInitializationException
    {
        org.apache.uima.resource.ResourceSpecifier specifier;
        try
        {
            specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in);
        }
        catch (InvalidXMLException e)
        {
            throw new ResourceInitializationException(e);
        }
        return specifier;
    }
}
