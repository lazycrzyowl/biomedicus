/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.core.factories;

/**
 * A convenient way to init/get type descriptions
 */
public class TypeFactory {

//    public static MetaDataTypeDesc getMetaDataTypeDesc(CAS aCAS)
//    {
//
//
//    }
//
//    public static FamilyHistoryTypeDesc getFamilyHistoryTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static FamilyHistoryConstituentTypeDesc getFamilyHistoryConstituentTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static SpanTypeDesc getSpanTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static SectionTypeDesc getSectionTypeDesc(CAS aCAS)
//    {
//
//    }
//    public static SentenceTypeDesc getSentenceTypeDesc(CAS aCAS)
//    {
//
//    }
//    public static ChunkTypeDesc getChunkTypeDesc(CAS aCAS)
//    {
//
//    }
//    public static SentenceBoundaryTypeDesc getSentenceBoundaryTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static TokenTypeDesc getTokenTypeDesc(CAS aCAS)
//    {
//
//    }
//    public static TermTypeDesc getTermTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static CuiTypeDesc getCuiTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static UmlsConceptTypeDesc getUmlsConceptTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static SymbolTypeDesc getSymbolTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static AcronymTypeDesc getAcronymTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static ParagraphTypeDesc getParagraphTypeDesc(CAS aCAS)
//    {
//
//    }
//
//    public static RelationTypeDesc getRelationTypeDesc(CAS aCAS)
//    {
//
//    }
}
