

package edu.umn.biomedicus.core.exceptions;

/**
 * Thrown by external service factories.
 * occurred during initialization.*
 */
public class ExternalResourceInitializationException extends BiomedicusException {

    private static final long serialVersionUID = -2521675776941322837L;

    /**
     * Creates a new exception with a null message.
     */
    public ExternalResourceInitializationException() {
        super();
    }

    /**
     * Creates a new exception with the specified cause and a null message.
     *
     * @param aCause
     *          the original exception that caused this exception to be thrown, if any
     */
    public ExternalResourceInitializationException(Throwable aCause) {
        super(aCause);
    }

    /**
     * Creates a new exception with the specified message and cause.
     *
     * @param aMessage
     *          the base name of the resource bundle in which the message for this exception is
     *          located.
     */
    public ExternalResourceInitializationException(String aMessage) {
        super(aMessage);
    }
}
