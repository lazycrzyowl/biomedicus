/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus;

import org.apache.commons.configuration.*;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DefaultConfig implements BiomedicusConfig {


    @NotNull
    private CompositeConfiguration config;

    public DefaultConfig() throws ConfigurationException
    {
        // Build config
        config = new CompositeConfiguration();

        // Get required environment variables
        String biomedicusHome = getBiomedicusHome();
        String userHome = getUserHome();

        // Load System
        config.addConfiguration(new SystemConfiguration());

        // Load Env
        config.addConfiguration(new EnvironmentConfiguration());

        // Load local (the .biomedicus.properties file that is in the user.home dir
        config.addConfiguration(
                getPropertiesConfig(Paths.get(userHome, ".biomedicus.properties"), "local properties"));

        // Load internal properties (the internal.properties file in the $BIOMEDICUS_HOME directory)
        config.addConfiguration(
                getPropertiesConfig(Paths.get(biomedicusHome, "internal.properties"),
                        "internal properties")
        );

        // Some property file properties must be propagated to System.properties.
        addSystemProps(config.getString("system.properties"));
    }

    public void setSystemProps(String propKeysString)
    {
        String[] propKeys = propKeysString.split("\\s|\\s");

        for (String key : propKeys)
        {
            if (key.equals("|"))
            {
                continue;
            }
            String value = config.getString(key);
            System.setProperty(key, value);
        }
    }

    public void setProperty(String key, String value)
    {
        config.setProperty(key, value);
    }

    public String getString(String key)
    {
        return config.getString(key);
    }

    public boolean getBoolean(String key) { return config.getBoolean(key); }

    public int getInt(String key) { return config.getInt(key); }

    public int getInt(String key, int defaultValue) { return config.getInt(key, defaultValue); }

    public CompositeConfiguration getConfig() { return config; }

    public String getBiomedicusHome() throws ConfigurationException
    {
        String results = System.getenv("BIOMEDICUS_HOME");
        if (results == null)
        {
            String mesg = "Environment variable `BIOMEDICUS_HOME` is not set. Exiting.";
            throw new ConfigurationException(mesg);
        }
        return results;
    }

    public String getUserHome() throws ConfigurationException
    {
        String results = System.getProperty("user.home");
        if (results == null)
        {
            String mesg = "User.home property not set. Failed to load local settings. Exiting.";
            throw new ConfigurationException(mesg);
        }
        return results;
    }

    public PropertiesConfiguration getPropertiesConfig(Path filePath, String fileDescription)
            throws ConfigurationException
    {
        if (Files.exists(filePath))
        {
            PropertiesConfiguration localProps = new PropertiesConfiguration(filePath.toFile());
            return localProps;
        } else
        {
            String mesg = String.format("Failed to load %s configuration file at `%s`",
                    fileDescription, filePath.toString());
            throw new ConfigurationException(mesg);
        }
    }

    protected void addSystemProps(String propKeysString)
    {
        // properties that appear in the system.properties field must be added to System
        String[] propKeys = propKeysString.split("\\s|\\s");

        for (String key : propKeys)
        {
            if (key.equals("|"))
            {
                continue;
            }
            String value = config.getString(key);
            System.setProperty(key, value);
        }
    }
}

