/*
 Copyright 2014 University of Minnesota
 All rights reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

package edu.umn.biomedicus.socialhistory;

import edu.umn.biomedicus.core.ResourceFactory;
import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.segmenting.services.StanfordSegmentingServiceProvider;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import edu.umn.biomedicus.tokenizing.services.StanfordTokenizingServiceProvider;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class SocialHistory
{

    @NotNull
    protected AnalysisEngine ae;

    private SocialHistory(Builder builder) throws ResourceInitializationException {
        ae = builder.getAE();
    }

    private AnalysisEngine getAE() {
        return ae;
    }

    public CAS newCAS() throws ResourceInitializationException {
        return ae.newCAS();
    }

    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        ae.process(aCAS);
    }

    public static class Builder
    {
        private String descriptor = "edu/umn/biomedicus/biomedicus-social-history/socialHistoryAE.xml";
        private List<ServiceProviderDescription> serviceProviders = new ArrayList<>();

        public Builder() {
        }

        public Builder(String descriptor) {
            this.descriptor = descriptor;
        }

        public Builder addService(ServiceProviderDescription serviceDesc) {
            serviceProviders.add(serviceDesc);
            return this;
        }

        public SocialHistory build() throws ResourceInitializationException {
            return new SocialHistory(this);
        }

        public AnalysisEngine getAE() throws ResourceInitializationException {
            URL resource = this.getClass().getClassLoader().getResource(descriptor);
            return ResourceFactory.createAnalysisEngine(resource, serviceProviders);
        }
    }
}
