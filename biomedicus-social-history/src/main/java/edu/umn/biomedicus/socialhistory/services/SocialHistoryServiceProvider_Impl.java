package edu.umn.biomedicus.biomedicus-social-history.services;


import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.core.utils.Span;

import org.apache.uima.cas.CAS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * This is the implementation of the biomedicus-social-historyServiceProvider interface.
 *
 * Edit the biomedicus-social-historyServiceProvider interface to match with the module you
 * are working on, then edit this file so that it impliments the interface you defined.
 */
public class biomedicus-social-historyServiceProvider_Impl
        implements SharedResourceObject {


    public List analyze(String[] featureSet)
    {
        // Analyze the features provided
        return new ArrayList();
    }


/**
 * A resource is assocated with each service. Load that external resource here.
 */
    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {
        // The following steps pull a resource out of a jar and into a temp file.
        URL resource = aData.getUrl();
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        }
        catch (IOException e)
        {throw new ResourceInitializationException(e);}

//      Load model here
    }

}
