package edu.umn.biomedicus.socialhistory;

import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;


/**
 * Simple test class for your new annotation engine module.
 */
public class SocialHistoryTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SocialHistoryTest(String testName) throws Exception
    {
        super(testName);
        Tokenizer tokenizer = new Tokenizer.Builder().build();
        SentenceSegmenter segmenter = new SentenceSegmenter.Builder().build();
        SocialHistory socialHistory = new SocialHistory.Builder().build();

        CAS aCAS = socialHistory.newCAS();
        CAS view = aCAS.createView("System");
        view.setDocumentText("This is a test of the SocialHistory to see if it really works");
        tokenizer.process(aCAS);
        segmenter.process(aCAS);
        socialHistory.process(aCAS);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( SocialHistoryTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
