package edu.umn.biomedicus.service;

import java.util.HashMap;
import java.util.Map;

import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import edu.umn.biomedicus.core.utils.CASUtil;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;

/**
 * A sample UIMA application. It creates a UIMA analysis engine, and provides
 *  a method for using it to process text. 
 *
 * This sample creates an asynchronous UIMA analysis engine. 
 *
 * @author Dale Lane (email@dalelane.co.uk)
 */
public class TestPipeline {

    /** The pipeline created to process text. */
    private UimaAsynchronousEngine uimaAsEngine = null;

    /**
     * Creates an asynchronous analysis engine. 
     */
    public TestPipeline() throws Exception
    {
        System.out.println("Sample BioMedICUS application - parallel - NLP-IE");
        System.out.println("==============================================");

        // creating UIMA analysis engine
        uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();

        // preparing map for use in deploying service
        Map<String,Object> deployCtx = new HashMap<String,Object>();
        String uima_home = System.getenv("UIMA_HOME");
        deployCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath, uima_home + "/bin/dd2spring.xsl");
        deployCtx.put(UimaAsynchronousEngine.SaxonClasspath, "file:" + uima_home + "/saxon/saxon8.jar");

        System.out.println("Deploying UIMA service");
        uimaAsEngine.deploy("biomedicus-service/descriptors/aeDeploy.xml", deployCtx);

        // creating aggregate analysis engine
        System.out.println("Deploying analysis engine");
        uimaAsEngine.deploy("biomedicus-service/descriptors/queueDeploy.xml", deployCtx);

        // add callback listener that will be informed when processing completes
        ProcessCallbackListener callback = new ProcessCallbackListener(uimaAsEngine);
        uimaAsEngine.addStatusCallbackListener(callback);

        // preparing map for use in a UIMA client for submitting text to process
        System.out.println("Initialising UIMA client");
        deployCtx.put(UimaAsynchronousEngine.ServerUri, "tcp://localhost:61616");
        deployCtx.put(UimaAsynchronousEngine.Endpoint,  "DemoAnnotatorQueue");
        uimaAsEngine.initialize(deployCtx);
    }

    /**
     * Uses the UIMA analysis engine to process the provided document text.
     */
    public void process(String text) throws CASException, Exception{
        CAS cas = uimaAsEngine.getCAS();
        CASUtil.getSystemView(cas).setDocumentText(text);
        cas.setDocumentText(text);
        uimaAsEngine.sendCAS(cas);
    }
}