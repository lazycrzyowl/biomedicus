/* 
Copyright 2010-13 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */
package edu.umn.biomedicus.resource;

import org.uimafit.component.Resource_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;

import redis.clients.jedis.Jedis;

/**
 * This class is a very simple means to get a connection to a redis service.
 * 
 * @author Robert Bill
 * 
 */
public class RedisCache_Impl extends Resource_ImplBase implements BiomedicusCache {
  public static final String PARAM_REDIS_HOST = "cacheHost";

  @ConfigurationParameter(name = PARAM_REDIS_HOST, defaultValue = "localhost")
  private String host;

  /* redis connection */
  Jedis jedis;

  @Override
  public void afterResourcesInitialized() {
    // Get initial connection
    this.jedis = new Jedis(host);
    jedis.connect();
  }

  public void set(String key, String value) {
    if (jedis.isConnected()) {
      jedis.set(key, value);
    } else {
      this.jedis = new Jedis(host);
      jedis.set(key, value);
    }
  }

  public String get(String key) {
    if (!jedis.isConnected()) {
      this.jedis = new Jedis(host);
    }
    return jedis.get(key);
  }

  public void destroy() {
    jedis.disconnect();
  }
}
