package edu.umn.biomedicus.annotator.text.token;

import edu.umn.biomedicus.type.Token;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

/**
 * A sample UIMA annotator for service testing.
 */
public class TokenAE extends JCasAnnotator_ImplBase {
    private Logger logger;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
        this.logger = context.getLogger();
    }

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
        String message = "Processing 'Token' annotations...";
        if (logger==null) System.out.println(message);
        else logger.log(Level.ALL, message);

		// add an empty annotation to the CAS
        String documentText = jCas.getDocumentText();
        for (String token : documentText.split("\\s")) {
            int begin = documentText.indexOf(token);
            int end = begin + token.length();
            jCas.addFsToIndexes(new Token(jCas, begin, end));
	    }
    }
}