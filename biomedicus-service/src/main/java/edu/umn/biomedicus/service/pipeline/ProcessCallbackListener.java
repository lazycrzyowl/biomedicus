package edu.umn.biomedicus.service.pipeline;

import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.SourceDocumentInformation;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaASProcessStatus;
import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.cas.*;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.collection.EntityProcessStatus;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.apache.uima.util.ProcessTraceEvent;
import org.jetbrains.annotations.NotNull;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ProcessCallbackListener extends UimaAsBaseCallbackListener {

    private static long mStartTime = System.nanoTime() / 1000000;
    private static final Logger logger = UIMAFramework.getLogger(ProcessCallbackListener.class);
    @NotNull private ConcurrentHashMap casMap = new ConcurrentHashMap();
    @NotNull UimaAsynchronousEngine uimaAsEngine;
    @NotNull Path inputDirectory;
    @NotNull Path outputDirectory;
    String springContainerId;
    boolean ignoreErrors = false;
    boolean logCas = false;
    int entityCount = 0;
    long before = -1;
    long size = 0;

    public ProcessCallbackListener(UimaAsynchronousEngine uimaAsEngine,
                                   Path inputDirectory, Path outputDirectory,
                                   String springContainerId)
    {
        this.springContainerId = springContainerId;
        this.inputDirectory = inputDirectory;
        this.outputDirectory = outputDirectory;
        this.uimaAsEngine = uimaAsEngine;
        this.before = System.currentTimeMillis();
        logger.log(Level.FINEST, "Process callback listener initialized.");
    }

    public ProcessCallbackListener(UimaAsynchronousEngine uimaAsEngine,
                                   Path inputDirectory, Path outputDirectory) {
        this.inputDirectory = inputDirectory;
        this.outputDirectory = outputDirectory;
        this.uimaAsEngine = uimaAsEngine;
        this.before = System.currentTimeMillis();
        logger.log(Level.FINEST, "Process callback listener initialized.");
    }

    public ProcessCallbackListener(UimaAsynchronousEngine uimaAsEngine)
    {
        this.outputDirectory = null;
        this.uimaAsEngine = uimaAsEngine;
        this.before = System.currentTimeMillis();
    }

    /**
     * Called when the initialization is completed.
     */
    public void initializationComplete(EntityProcessStatus aStatus) {
        if (aStatus != null && aStatus.isException()) {
            logger.log(Level.SEVERE, "Error on getMeta call to remote service:");
            List exceptions = aStatus.getExceptions();
            for (int i = 0; i < exceptions.size(); i++) {
                ((Throwable) exceptions.get(i)).printStackTrace();
            }
            logger.log(Level.SEVERE, "Terminating Client...");
            stop();
        }
        logger.log(Level.FINEST, "UIMA AS Service Initialization Complete");
    }


    private void stop() {
        try {
            logger.log(Level.WARNING, "Stopping client engine.");
            uimaAsEngine.stop();
        } catch( Exception e) {

        }
        System.exit(0);

    }
    /**
     * Called when the collection processing is completed.
     */    public void collectionProcessComplete(EntityProcessStatus aStatus) {
        if (aStatus != null && aStatus.isException()) {
            logger.log(Level.SEVERE, "Error on collection process complete call to remote service:");
            List exceptions = aStatus.getExceptions();
            for (int i = 0; i < exceptions.size(); i++) {
                ((Throwable) exceptions.get(i)).printStackTrace();
            }
            logger.log(Level.SEVERE, "Terminating Client...");
            stop();
        }
        System.out.print("Completed " + entityCount + " documents");
        if (size > 0) {
            System.out.print("; " + size + " characters");
        }
        System.out.println();
        long elapsedTime = System.nanoTime() / 1000000 - mStartTime;
        System.out.println("Time Elapsed : " + elapsedTime + " ms ");

        String perfReport = uimaAsEngine.getPerformanceReport();
        if (perfReport != null) {
            System.out.println("\n\n ------------------ PERFORMANCE REPORT ------------------\n");
            System.out.println(uimaAsEngine.getPerformanceReport());
        }
    }

    /**
     * Called when the processing of a Document is completed. <br>
     * The process status can be looked at and corresponding actions taken.
     *
     * @param aCas
     *          CAS corresponding to the completed processing
     * @param aStatus
     *          EntityProcessStatus that holds the status of all the events for aEntity
     */
    @Override
    public void entityProcessComplete(CAS aCas, EntityProcessStatus aStatus) {
        if (aStatus != null) {
            if (aStatus.isException()) {
                System.err.println("Error on process CAS call to remote service:");
                List exceptions = aStatus.getExceptions();
                for (int i = 0; i < exceptions.size(); i++) {
                    ((Throwable) exceptions.get(i)).printStackTrace();
                }
                if (!ignoreErrors) {
                    System.err.println("Terminating Client...");
                    stop();
                }
            }
            if (logCas) {
                String ip = "no IP";
                List eList = aStatus.getProcessTrace().getEventsByComponentName("UimaEE", false);
                for (int e = 0; e < eList.size(); e++) {
                    ProcessTraceEvent event = (ProcessTraceEvent) eList.get(e);
                    if (event.getDescription().equals("Service IP")) {
                        ip = event.getResultMessage();
                    }
                }
                String casId = ((UimaASProcessStatus) aStatus).getCasReferenceId();
                if (casId != null) {
                    long current = System.nanoTime() / 1000000 - mStartTime;
                    if (casMap.containsKey(casId)) {
                        Object value = casMap.get(casId);
                        if (value != null && value instanceof Long) {
                            long start = ((Long) value).longValue();
                            System.out.println(ip + "\t" + start + "\t" + (current - start));
                        }
                    }
                }

            } else {
                System.out.print(".");
                if (0 == (entityCount + 1) % 50) {
                    System.out.print((entityCount + 1) + " processed\n");
                }
            }
        }

        CAS system = CASUtil.getSystemView(aCas);

        // if output dir specified, dump CAS to XMI
        if (outputDirectory != null) {
            // try to retrieve the filename of the input file from the CAS
            File outFile = null;
            Type srcDocInfoType = aCas.getTypeSystem().getType(SourceDocumentInformation.class.getCanonicalName());

            if (srcDocInfoType != null) {
                FSIterator iter = aCas.getIndexRepository().getAllIndexedFS(srcDocInfoType);
                if (iter.hasNext()) {
                    FeatureStructure srcDocInfoFs = iter.get();
                    Feature uriFeat = srcDocInfoType.getFeatureByBaseName("uri");
                    Feature offsetInSourceFeat = srcDocInfoType.getFeatureByBaseName("offsetInSource");
                    String uri = srcDocInfoFs.getStringValue(uriFeat);
                    int offsetInSource = srcDocInfoFs.getIntValue(offsetInSourceFeat);
                    File inFile;
                    try {
                        inFile = new File(new URL(uri).getPath());
                        int begin = inFile.toString().indexOf(inputDirectory.toString()) +
                                inputDirectory.toString().length();
                        Path outputFilePath = Paths.get(outputDirectory.toString(),
                                inFile.toString().substring(begin));
                        String outFileName = FilenameUtils.removeExtension(outputFilePath.toString());
                        if (offsetInSource > 0) {
                            outFileName += ("_" + offsetInSource);
                        }
                        outFileName += ".xmi";
                        outFile = new File(outFileName);
                        outFile.getParentFile().mkdirs();
                        outFile.createNewFile();
                    } catch (IOException e1) {
                        try {
                            uimaAsEngine.undeploy(springContainerId);
                            uimaAsEngine.stop();
                        } catch (Exception e)
                        {
                            e.printStackTrace();
                            System.exit(1);
                        }
                    }
                }
            }
            if (outFile == null) {
                outFile = new File(outputDirectory.toFile(), "doc" + entityCount);
            }
            try {
                FileOutputStream outStream = new FileOutputStream(outFile);
                try {
                    XmiCasSerializer.serialize(aCas, outStream);
                } finally {
                    outStream.close();
                }
            } catch (Exception e) {
                System.err.println("Could not save CAS to XMI file");
                e.printStackTrace();
            }
        }

        // update stats
        entityCount++;
        String docText = aCas.getDocumentText();
        if (docText != null) {
            size += docText.length();
        }

        // Called just before sendCas with next CAS from collection reader
    }

    public void onBeforeMessageSend(UimaASProcessStatus status) {
        long current = System.nanoTime() / 1000000 - mStartTime;
        casMap.put(status.getCasReferenceId(), current);
    }
    /**
     * This method is called when a CAS is picked up by remote UIMA AS
     * from a queue right before processing. This callback identifies
     * on which machine the CAS is being processed and by which UIMA AS
     * service (PID).
     */
    public void onBeforeProcessCAS(UimaASProcessStatus status, String nodeIP, String pid) {

    }

}

