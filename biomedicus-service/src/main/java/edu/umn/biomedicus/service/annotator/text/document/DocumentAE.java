package edu.umn.biomedicus.annotator.text.document;

import edu.umn.biomedicus.type.MetaData;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

/**
 * A sample UIMA annotator for service testing.
 */
public class DocumentAE extends JCasAnnotator_ImplBase {

    protected Logger logger;

    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
        this.logger = context.getLogger();
    }

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
        String message = "Processing 'Document' annotations...";
        if (logger==null) System.out.println(message);
        else logger.log(Level.ALL, message);

		// add an empty annotation to the CAS
		jCas.addFsToIndexes(new MetaData(jCas));
	}
}