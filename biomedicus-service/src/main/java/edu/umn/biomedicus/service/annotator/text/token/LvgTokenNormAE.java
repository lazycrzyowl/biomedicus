/* 
Copyright 2010-13 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.annotator.text.token;

import edu.umn.biomedicus.type.Token;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.eclipse.jetty.util.log.Log;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.util.JCasUtil;
import redis.clients.jedis.Jedis;

//import gov.nih.nlm.nls.lvg.Api.NormApi;
//import gov.nih.nlm.nls.lvg.Api.WordIndApi;

/**
 * 
 * @author Philip Ogren
 * @author University of Minnesota Biomedical NLP Group, Robert Bill <bill0154@umn.edu>
 * 
 *         The NormAnnotator finds the normalized version of a token and adds it to the "norm"
 *         attribute of the token type.
 * 
 *         To configure NormAnnotator, LVG must be installed on the computer that it is run on and
 *         the lvg.properties file must be copied into the "{$BIOMEDICUS_HOME}/resources/config"
 *         directory.
 * 
 */

public class LvgTokenNormAE extends JCasAnnotator_ImplBase {

//  protected NormApi normApi;

//  protected WordIndApi wordIndApi;

  protected Logger log;

  protected Jedis jedis;

  @Override
  public void initialize(UimaContext context) throws ResourceInitializationException {
    super.initialize(context);

    /* create APIs */
//    this.normApi = new NormApi("/workspace/biomedicus/descriptors/lvg.properties");
//    this.wordIndApi = new WordIndApi();

    /* create cache */
    this.jedis = new Jedis("localhost", 6379, 10000);
    this.jedis.connect();

    /* create logger */
    this.log = context.getLogger();
  }

  @Override
  public void process(JCas jCas) throws AnalysisEngineProcessException {

    try {
      for (Token token : JCasUtil.select(jCas, Token.class)) {
        String tokenText = token.getCoveredText();
        String norm = norm(tokenText);
        token.setNorm(norm);
      }
    } catch (Exception e) {
      throw new AnalysisEngineProcessException(e);
    }
  }

  private String norm(String token) throws Exception {
    String normalizedToken = null;
    String cacheKey = new StringBuilder("lvg::").append(token).toString();
    if (jedis != null) {
      try {
        normalizedToken = this.jedis.get(cacheKey);
        System.out.println("FOUND IN CACHE:" + cacheKey + ", " + normalizedToken);
      } catch (Exception e) {
        // For any fail, disable cache
        Log.warn("NormAnnotator Cache failed with an exception: " + e);
        Log.warn("Disabling caching for LvgTokenNormAE.");
        this.jedis = null;
      }
      if (normalizedToken != null) {
        return normalizedToken;
      }
//      normalizedToken = norm(normApi, wordIndApi, token);
      jedis.set(cacheKey, normalizedToken);
      return normalizedToken;
    }
    return token;
  }

//  public static String norm(NormApi normApi, WordIndApi wordIndApi, String token) throws Exception {
//    return norm(normApi, wordIndApi, token, true);
//  }

//  private static String norm(NormApi normApi, WordIndApi wordIndApi, String token, boolean recurse)
//          throws Exception {
    // Otherwise, we will invoke the normApi to get the normalization
//    Vector<String> normVector = normApi.Mutate(token);

    // not sure what the contract is of normApi.Mutate, so am hedging my
    // bets in case null or an empty vector is returned
//    if (normVector == null || normVector.size() == 0) {
//      return token;
//    }

//    String norm = normVector.get(normVector.size() - 1);
//    if (norm.indexOf(" ") == -1) {
//      return norm;
//    }

//    if (recurse) {
//      StringBuilder sb = new StringBuilder();
//      Vector<String> wordVector = wordIndApi.Mutate(token);
//      for (String word : wordVector) {
//        sb.append(norm(normApi, wordIndApi, word, false) + "_");
//      }
//      return sb.substring(0, sb.length() - 1);
//    } else {
//      return token;
//    }
//  }
}
