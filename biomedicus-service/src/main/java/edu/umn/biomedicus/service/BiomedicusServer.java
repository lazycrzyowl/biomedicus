package edu.umn.biomedicus.service;

import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import edu.umn.biomedicus.service.pipeline.ServerPipeline;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * A simple application for testing the feasibility of connecting the AHC datacenter to MSI computational resources.
 * <p/>
 * This class resides within a protected data center and awaits connections from the BiomedicusClient applications.
 */
public class BiomedicusServer {

    @NotNull static CompositeConfiguration config;
    @NotNull static final Logger logger = UIMAFramework.getLogger(BiomedicusServer.class);

    public static void main(String[] args) throws Exception {

        config = BiomedicusAHC2MSIConfig.getConfig();

        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
        logger.log(Level.INFO, "Uima asynchronous engine initialized.");
        UimaAsBaseCallbackListener asyncListener = new ProcessCallbackListener(uimaAsEngine);//, tunnel);
        logger.log(Level.INFO, "Callback listener initialized.");

      /* Constructs a class to create and run a UIMA pipeline */
        ServerPipeline uimaPipeline = new ServerPipeline(asyncListener, uimaAsEngine);//, tunnel);

      /* Run the sample document through the pipeline */
        System.out.println("Ready to process document...");
        // TODO: loop through the sample clinical document a few thousand times to get more useful info.
        //uimaPipeline.process(SAMPLE_DOCUMENT_TEXT);
    }
}