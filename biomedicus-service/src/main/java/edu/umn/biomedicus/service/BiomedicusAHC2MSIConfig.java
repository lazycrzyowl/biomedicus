/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.service;

import edu.umn.biomedicus.DefaultConfig;
import org.apache.commons.configuration.*;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class BiomedicusAHC2MSIConfig {


    private static CompositeConfiguration config = null;
    /**
     * BioMedICUS Configuration includes system settings, environment variables, a "local" properties file that defines
     * paths and platform specific settings, and an internal properties file that defines properties in BioMedICUS that
     * do not depend on the local system it runs on.
     * <p/>
     * If a "project" is used, then there is an additional project.properties in the root of the project directory.
     *
     * @throws ConfigurationException
     */
    public static CompositeConfiguration build(@NotNull Path projectHome) throws ConfigurationException
    {
        DefaultConfig defaultConfig = new DefaultConfig();
        config = defaultConfig.getConfig();

        // Load project properties
        Path projectProperties = Paths.get(projectHome.toString(), "project.properties");
        PropertiesConfiguration projectConfig = new PropertiesConfiguration(projectProperties.toFile());
        config.addConfiguration(projectConfig);

        Map<String, String> projectHomeMapping = new HashMap<>();
        projectHomeMapping.put("projectHome", projectHome.toString());
        MapConfiguration projectHomeConfig = new MapConfiguration(projectHomeMapping);
        config.addConfiguration(projectHomeConfig);

        return config;
    }

    public static CompositeConfiguration getConfig(Path projectHome) throws ConfigurationException
    {
        return (config == null) ? BiomedicusAHC2MSIConfig.build(projectHome) : config;
    }

    public static CompositeConfiguration getConfig() throws ConfigurationException{
        if (config == null)
            config = new DefaultConfig().getConfig();
        return config;
    }
}
