package edu.umn.biomedicus.service;

import org.apache.uima.UIMAFramework;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConcurrentUserWatch extends Thread {
    protected Logger logger;
    protected String username;

    public ConcurrentUserWatch(String username) {
        this.username = username;
    }

    public void run() {
        Logger logger = UIMAFramework.getLogger();
        logger.log(Level.INFO, "Initializing AHC2MSI concurrent user watch thread/");

        boolean concurrentUser = false;
        Process p;
        while (true) {
            try {
                p = Runtime.getRuntime().exec("who");
                p.waitFor();

                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = "";
                StringBuffer sb = new StringBuffer();
                while ((line = reader.readLine())!= null) {
                    String username = line.split("\\s+")[0];
                    // The user MUST be billrobe until the application is approved for other users
                    // TODO: get allowed username from properties file in future
                    if (!username.equals(username)) {
                        concurrentUser = true;
                    }
                }
            } catch (IOException e) {
                concurrentUser = true;  // not necessarily true, but in case of an exception, must assume could be True.
            } catch (InterruptedException e) {
                concurrentUser = true;  // not necessarily true, but in case of an exception, must assume could be True.
            }
            if (concurrentUser) {
                System.out.println("CONCURRENT USER IDENTIFIED.");
                // Log message to enterprise logger that is not established yet.
                logger.log(Level.SEVERE, "Concurrent user identified. Execution terminated.");
                System.exit(1);
                // NOTE: On System.exit(), the administrative script that restarts the process kicks in and forces
                //       an immediate restart.
            };
        }
    }
}


