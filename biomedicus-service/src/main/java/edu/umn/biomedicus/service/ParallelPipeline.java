package edu.umn.biomedicus.service;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import edu.umn.biomedicus.core.utils.CASUtil;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.jetbrains.annotations.NotNull;

public class ParallelPipeline {

    @NotNull
    private UimaAsynchronousEngine uimaAsEngine;
    private Path outputDirectory;
    private String agentId;

    /**
     * Creates an asynchronous analysis engine. 
     */
    public ParallelPipeline(Path inputDirectory, Path outputDirectory) throws Exception
    {
        System.out.println("BioMedICUS application - parallel - NLP-IE");
        System.out.println("==============================================");

        this.outputDirectory = outputDirectory;

        // creating UIMA analysis engine
        uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
        ProcessCallbackListener cb = new ProcessCallbackListener(uimaAsEngine, inputDirectory, outputDirectory);
        // add callback listener that will be informed when processing completes
        uimaAsEngine.addStatusCallbackListener(cb);
        // preparing map for use in deploying service
        Map<String,Object> deployCtx = new HashMap<String,Object>();
        String uima_home = System.getenv("UIMA_HOME");
        deployCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath, uima_home + "/bin/dd2spring.xsl");
        deployCtx.put(UimaAsynchronousEngine.SaxonClasspath, "file:" + uima_home + "/saxon/saxon8.jar");

        System.out.println("Deploying UIMA service");
        agentId = uimaAsEngine.deploy("biomedicus-service/descriptors/agentDeploy.xml", deployCtx);

        // creating aggregate analysis engine
        System.out.println("Deploying analysis engine");
        uimaAsEngine.deploy("biomedicus-service/descriptors/serverDeploy.xml", deployCtx);

        // preparing map for use in a UIMA client for submitting text to process
        System.out.println("Initialising UIMA client");
        deployCtx.put(UimaAsynchronousEngine.ServerUri, "tcp://localhost:61616");
        deployCtx.put(UimaAsynchronousEngine.Endpoint,  "DemoAnnotatorQueue");
        uimaAsEngine.initialize(deployCtx);
    }


    public CAS process(String fileName, String text) throws CASException, Exception {
        CAS aCAS = uimaAsEngine.getCAS();
        CAS view = CASUtil.getSystemView(aCAS);
        CAS md = CASUtil.getMetaDataView(aCAS);
        md.setSofaDataString(fileName, "text/plain");
        aCAS.setDocumentText(text);
        view.setDocumentText(text);
        uimaAsEngine.sendCAS(aCAS);
        return aCAS;
    }

    public void collectionProcessingComplete() throws Exception
    {
        uimaAsEngine.undeploy(agentId);
        uimaAsEngine.stop();
    }
}