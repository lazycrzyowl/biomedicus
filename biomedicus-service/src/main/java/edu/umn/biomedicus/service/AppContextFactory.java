package edu.umn.biomedicus.service;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsynchronousEngine;

import java.util.HashMap;
import java.util.Map;

public class AppContextFactory {

    public static Map<String, Object> build(CompositeConfiguration config)
    {

        HashMap<String, Object> appCtx = new HashMap<>();
        appCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath, System.getenv("UIMA_HOME")
                + "/bin/dd2spring.xsl");
        appCtx.put(UimaAsynchronousEngine.SaxonClasspath, "file:" + System.getenv("UIMA_HOME")
                + "/saxon/saxon8.jar");


        // Add Broker URI
        appCtx.put(UimaAsynchronousEngine.ServerUri, config.getString("brokerUrl"));
        // Add Queue Name
        String ep = config.getString("endpoint");
        appCtx.put(UimaAsynchronousEngine.ENDPOINT, config.getString("endpoint"));

        // Add timeouts (UIMA EE expects it in milliseconds, but we use seconds on the command line)
        if (config.getInt("timeout", 0) != 0)
            appCtx.put(UimaAsynchronousEngine.Timeout, config.getInt("timeout", 0) * 1000);

        appCtx.put(UimaAsynchronousEngine.GetMetaTimeout, config.getInt("getmeta_timeout", 100) * 1000);

        if (config.getInt("cpc_timeout", 0) != 0)
            appCtx.put(UimaAsynchronousEngine.CpcTimeout, config.getInt("cpc_timeout", 0) * 1000);

        // Add the Cas Pool Size and initial FS heap size
        appCtx.put(UimaAsynchronousEngine.CasPoolSize, config.getInt("casPoolSize", 2));
        appCtx.put(UIMAFramework.CAS_INITIAL_HEAP_SIZE,
                Integer.valueOf(config.getInt("fsHeapSize", 2000000 / 4)).toString());

        appCtx.put(UimaAsynchronousEngine.SERIALIZATION_STRATEGY,
                config.getString("serializatoinStrategy", "binary"));
        appCtx.put(UimaAsynchronousEngine.UimaEeDebug, config.getString("debug", "false"));

        return appCtx;
    }
}
