/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.service;

import org.apache.commons.cli.*;
import org.apache.commons.configuration.ConfigurationException;

public class ServiceDeploymentOptions {


    private static Options options = null;
    private static CommandLine cl = null;
    /**
     * Options required for the AHC2MSI demonstrations.
     */
    protected static Options build(String[] args) throws ConfigurationException
    {
        // create the Options
        options = new Options();
        String msg = "Project directory (must contain 'input' and 'output' folders, and a project.properties file.";
        Option dd = OptionBuilder.withArgName("dd")
                .withLongOpt("deploymentDescriptor")
                .hasArgs()
                .withDescription("Deployment descriptor")
                .create();
        options.addOption(dd);

        cl = getCLIValues(options, args);
        return options;
    }

    public static CommandLine getCLIOptions(String[] args) throws ConfigurationException
    {
        if (options == null)
            ServiceOptions.build(args);
        if (cl == null)
            cl = getCLIValues(options, args);
        return cl;
    }

    public static CommandLine getCLIValues(Options options, String[] args)
    {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try
        {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e)
        {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    public static void printHelp(String name)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }
}
