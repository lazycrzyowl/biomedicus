package edu.umn.biomedicus.service;

import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Created by bill0154 on 5/27/14.
 */
public class UimaServiceDeployer {

    public static void main(String[] args) throws Exception
    {
        CompositeConfiguration config = BiomedicusAHC2MSIConfig.getConfig();
        Map<String, Object> appCtx = AppContextFactory.build(config);
        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
        Path inputDir = Paths.get("/Users/bill0154/bcproject/input");
        Path outputDir = Paths.get("/Users/bill0154/bcproject/output"); //config.getString("output"));
        uimaAsEngine.addStatusCallbackListener(new ProcessCallbackListener(uimaAsEngine, inputDir, outputDir));
        appCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath,
                System.getenv("UIMA_HOME") + "/bin/dd2spring.xsl");
        appCtx.put(UimaAsynchronousEngine.SaxonClasspath,
                "file:" + System.getenv("UIMA_HOME") + "/saxon/saxon8.jar");
        uimaAsEngine.deploy("/Users/bill0154/Develop/biomedicus/biomedicus-service/descriptors/queueDeploy.xml", appCtx);
    }
}
