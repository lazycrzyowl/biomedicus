/* 
 Copyright 2011 University of Minnesota 
 
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.service;

import edu.umn.biomedicus.tokenizing.TokenAE;
import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.uimafit.factory.AnalysisEngineFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * This is a command-line interface to using the acronym detector.
 */
public class FitExample {

  /** The pipeline created to process text. */
  private static UimaAsynchronousEngine uimaAsEngine = null;

  private static String text = "Th Dr. prescribed tamoxifin b.i.d. to the most recent patient. There should be many sentences to annotate, and this is yet another example.";

  public static void main(String[] args) throws Exception {
    System.out.println("Sample Biomedicus application - parallel - UMN NLP Group");
    System.out.println("==============================================");

    // Get memory object (jCas) and input ready
    // JCas jcas = ComponentFactory.getJCas();
    AnalysisEngineDescription token = AnalysisEngineFactory
            .createPrimitiveDescription(TokenAE.class);

    // Writer out = new OutputStreamWriter(new FileOutputStream(
    // "/workspace/biomedicus/descriptors/annotator/token/icuWhitespaceTokenAE.xml"));
    // token.toXML(out);
    // out.flush();
    // out.close();

    // creating UIMA analysis engine
    uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
    UimaAsBaseCallbackListener asyncListener = new ProcessCallbackListener(uimaAsEngine);
    uimaAsEngine.addStatusCallbackListener(asyncListener);

    // preparing map for use in deploying service
    Map<String, Object> deployCtx = new HashMap<String, Object>();
    deployCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath, System.getenv("UIMA_HOME") + "/bin/dd2spring.xsl");
    deployCtx.put(UimaAsynchronousEngine.SaxonClasspath, "file:" + System.getenv("UIMA_HOME") + "/saxon/saxon8.jar");

    System.out.println("Deploying Biomedicus service");
    uimaAsEngine.deploy("./descriptors/annotator/token/tokenAEDeploy.xml", deployCtx);
    uimaAsEngine.deploy("./descriptors/annotator/sentence/sentenceDeploy.xml", deployCtx);
    uimaAsEngine.deploy("./descriptors/annotator/annotatorB/documentAEDeploy.xml", deployCtx);
    uimaAsEngine.deploy("./descriptors/annotator/annotatorC/conceptDeploy.xml", deployCtx);
    uimaAsEngine.deploy("./descriptors/annotator/annotatorD/clinicalAEDeploy.xml", deployCtx);

    // creating aggregate analysis engine
    System.out.println("Deploying analysis engine");
    uimaAsEngine.deploy("./descriptors/aggregate/deploy.xml", deployCtx);

    // preparing map for use in a UIMA client for submitting text to process
    System.out.println("Initialising UIMA client");
    deployCtx.put(UimaAsynchronousEngine.ServerUri, "tcp://localhost:61616");
    deployCtx.put(UimaAsynchronousEngine.Endpoint, "BiomedicusAnnotatorQueue");
    uimaAsEngine.initialize(deployCtx);

    CAS cas = null;
    try {
      cas = uimaAsEngine.getCAS();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    cas.setDocumentText(text);
    uimaAsEngine.sendCAS(cas);
  }
}
