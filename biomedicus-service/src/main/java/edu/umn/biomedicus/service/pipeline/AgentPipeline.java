package edu.umn.biomedicus.service.pipeline;

import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple Biomedicus demonstration. It creates a asynchronous UIMA analysis engine using
 * Biomedicus components.
 * 
 * 
 * @author UMN NLP Group (nlp@umn.edu)
 */
public class AgentPipeline {

  /** The pipeline created to process text. */
  private UimaAsynchronousEngine uimaAsEngine = null;

  /**
   * Creates an asynchronous analysis engine.
   */
  public AgentPipeline(UimaAsBaseCallbackListener callback, UimaAsynchronousEngine uimaAsEngine)
          throws Exception {

    System.out.println("Biomedicus - parallel mode - UMN NLP Group");
    System.out.println("==============================================");

    // make as engine instance member
    this.uimaAsEngine = uimaAsEngine;

    // preparing map for use in deploying service
    // Note that this requires UIMA-AS is installed in the UIMA_HOME directory
    Map<String, Object> deployCtx = new HashMap<String, Object>();
    deployCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath, System.getenv("UIMA_HOME")
            + "/bin/dd2spring.xsl");
    deployCtx.put(UimaAsynchronousEngine.SaxonClasspath, "file:" + System.getenv("UIMA_HOME")
            + "/saxon/saxon8.jar");

    System.out.println("Deploying Biomedicus service");
    uimaAsEngine.deploy("./descriptors/annotator/token/tokenizerDeploy.xml", deployCtx);
    uimaAsEngine.deploy("./descriptors/annotator/sentence/sentenceDeploy.xml", deployCtx);
    uimaAsEngine.deploy("./descriptors/annotator/document/documentDeploy.xml", deployCtx);
    uimaAsEngine.deploy("./descriptors/annotator/concept/conceptDeploy.xml", deployCtx);
//    uimaAsEngine.deploy("./descriptors/annotator/clinical/clinicalDeploy.xml", deployCtx);

    // Removing the following lines because they are part of the data faucet.
    //
    // creating aggregate analysis engine
    //System.out.println("Deploying analysis engine");
    //uimaAsEngine.deploy("./descriptors/aggregate/deploy.xml", deployCtx);

    // add callback listener that will be informed when processing completes
    //uimaAsEngine.addStatusCallbackListener(callback);

    // preparing map for use in a UIMA client for submitting text to process
    //System.out.println("Initialising UIMA client");
    //deployCtx.put(UimaAsynchronousEngine.ServerUri, "tcp://localhost:61616");
    //deployCtx.put(UimaAsynchronousEngine.Endpoint, "BiomedicusAnnotatorQueue");
    //uimaAsEngine.initialize(deployCtx);
  }

  /**
   * Uses the UIMA analysis engine to process the provided document text.
   */
  public void process(String text) throws CASException, Exception {
    CAS cas = uimaAsEngine.getCAS();
    cas.setDocumentText(text);
    uimaAsEngine.sendCAS(cas);
  }
}












