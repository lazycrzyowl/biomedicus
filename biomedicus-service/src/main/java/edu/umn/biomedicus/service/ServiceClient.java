package edu.umn.biomedicus.service;

import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceProcessException;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Attaches to a remotely (in separate JVM or separate machine) deployed analysis engine service. Once
 */
public class ServiceClient {
    /**
     * Start time of the processing - used to compute elapsed time.
     */
    private static long mStartTime = System.nanoTime() / 1000000;
    public boolean collectionProcessingComplete = false;
    Map<String, Object> appCtx;
    private File collectionReaderDescriptor;
    private File inputDir;
    private File outputDir;
    private boolean logCas = false;
    private String service;
    private UimaAsynchronousEngine uimaEEEngine;
    private String springContainerId;
    private CollectionReader collectionReader;
    private ProcessCallbackListener processCallbackListener;

    /**
     * Constructor for the class. Parses command line arguments and sets the values of fields in this
     * instance. If command line is invalid prints a message and calls System.exit().
     *
     * @param config command line arguments into the program - see class description
     */
    public ServiceClient(CompositeConfiguration config) throws Exception {


        appCtx = AppContextFactory.build(config);

        logCas = (config.containsKey("logCas")) ? config.getBoolean("logCas") : false;
        collectionReaderDescriptor = new File(config.getString("collectionReader"));
        inputDir = new File(config.getString("projectHome"), "input");
        outputDir = new File(config.getString("projectHome"), "output");
        service = config.getString("service");

        uimaEEEngine = new BaseUIMAAsynchronousEngine_impl();
        System.out.println("Attempting to deploy " + service + " ...");

        springContainerId = uimaEEEngine.deploy(service, appCtx);
        @NotNull ProcessCallbackListener callbackListener;

        collectionReader = loadCollectionReader(collectionReaderDescriptor);
        uimaEEEngine.setCollectionReader(collectionReader);

        // add callback listener
        callbackListener = new ProcessCallbackListener(uimaEEEngine, Paths.get(inputDir.toString()),
                Paths.get(outputDir.toString()), springContainerId);
        uimaEEEngine.addStatusCallbackListener(callbackListener);

        // initialize
        uimaEEEngine.initialize(appCtx);
    }

    /**
     * main class.
     *
     * @param args Command line arguments - see class description
     */
    public static void main(String[] args) throws Exception {
        CommandLine cl = ServiceOptions.getCLIOptions(args);
        if (!cl.hasOption("projectdir"))
            ServiceOptions.printHelp("projectdir");
        Path projectHome = Paths.get(cl.getOptionValue("projectdir"));

        CompositeConfiguration config = BiomedicusAHC2MSIConfig.build(projectHome);

        ServiceClient runner = new ServiceClient(config);
        while (!runner.collectionProcessingComplete) {
            try {
                runner.run();
            } catch (ResourceProcessException e) {
                continue;
            } catch (IOException e) {
                runner.shutdown();
            }
        }
    }

    private CollectionReader loadCollectionReader(File descriptorFile) throws IOException {
        @NotNull CollectionReader collectionReader = null;
        @NotNull CollectionReaderDescription collectionReaderDescription;

        // add Collection Reader if specified
        if (descriptorFile != null) {

            try {
                collectionReaderDescription = UIMAFramework.getXMLParser()
                        .parseCollectionReaderDescription(new XMLInputSource(descriptorFile));
            } catch (InvalidXMLException e) {
                throw new IOException(e);
            }

            try {
                if (collectionReaderDescription != null) {
                    collectionReader = UIMAFramework
                            .produceCollectionReader(collectionReaderDescription);
                }
            } catch (ResourceInitializationException e) {
                throw new IOException(e);  // relabel as an I/O problem because it is collection reading
            }
        }
        return collectionReader;
    }

    public void run() throws ResourceProcessException, IOException {

        // run
        if (logCas) {
            System.out.println("\nService-IPaddr\tSent\tDuration");
        }

        uimaEEEngine.process();
        collectionProcessingComplete = true;
    }

    public void shutdown()
    {
        try
        {
            if (springContainerId != null) {
                uimaEEEngine.undeploy(springContainerId);
        }
            uimaEEEngine.stop();

        } catch (Exception e) {
            e.printStackTrace();
            Runtime.getRuntime().halt(-1);
        }
        System.exit(0);
    }
}
