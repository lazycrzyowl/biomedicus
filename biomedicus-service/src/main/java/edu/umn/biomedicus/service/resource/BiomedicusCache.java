package edu.umn.biomedicus.resource;

public abstract interface BiomedicusCache {
  public void set(String key, String value);

  public String get(String key);
}
