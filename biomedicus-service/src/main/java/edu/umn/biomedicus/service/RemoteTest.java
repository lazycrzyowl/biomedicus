package edu.umn.biomedicus.service;

import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import edu.umn.biomedicus.core.utils.CASUtil;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.cas.CAS;

import java.nio.file.Paths;
import java.util.HashMap;

/**
 * Created by bill0154 on 5/19/14.
 */
public class RemoteTest {


    public static void main(String[] args) throws Exception
    {
        //create Map to pass server URI and Endpoint parameters
        HashMap<String, Object> appCtx = new HashMap<>();
        // Add Broker URI on local machine
        appCtx.put(UimaAsynchronousEngine.ServerUri, "tcp://localhost:61616");
        // Add Queue Name
        appCtx.put(UimaAsynchronousEngine.Endpoint, "DemoAnnotatorQueue");
        // Add the Cas Pool Size
        appCtx.put(UimaAsynchronousEngine.CasPoolSize, 2);
        //initialize
        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
        uimaAsEngine.addStatusCallbackListener(new ProcessCallbackListener(uimaAsEngine,
                Paths.get("/Users/bill0154/bcproject/input"), Paths.get("/Users/bill0154/bcproject/output")));
        uimaAsEngine.initialize(appCtx);

        //Prepare a Cas and send it to the service:
        //get an empty CAS from the Cas pool
        CAS cas = uimaAsEngine.getCAS();
        // Initialize it with input data
        cas.setDocumentText("Some text to pass to this service.");
        CAS sys = CASUtil.getSystemView(cas);
        // Send Cas to service for processing
        uimaAsEngine.sendCAS(cas);
    }
}
