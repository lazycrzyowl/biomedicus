package edu.umn.biomedicus.service;

import edu.umn.biomedicus.service.pipeline.AgentPipeline;
import edu.umn.biomedicus.service.pipeline.ProcessCallbackListener;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.util.Properties;
import java.io.IOException;

/**
 * A simple application for testing the feasibility of connecting the AHC datacenter to MSI computational resources.
 * <p/>
 * This is an initial test. No significant processing takes place.
 */
public class BiomedicusClient {

    public static void main(String[] args) throws Exception {
        Properties prop = BiomedicusClient.getProperties();

        // ** NOTE FOR EVALUATION **
        // The "logger" instance will be an enterprise logger that publishes messages to the
        // system logger within the protected health data center at AHC.
        Logger logger = UIMAFramework.getLogger();
        logger.log(Level.INFO, "Initializing AHC2MSI test #1");

      /* Test to make sure no concurrent users */
        if (systemHasConcurrentLogin()) {
            // Log message to enterprise logger.
            logger.log(Level.SEVERE, "Concurrent user identified. Execution terminated.");
            System.exit(1);
        } else {
            logger.log(Level.ALL, "No concurrent users identified. Continuing execution.");
        }

      /* Test to make sure swap is off */
        if (swapIsEnabled()) {
            // Log message to enterprise logger.
            logger.log(Level.SEVERE, "Swap is enabled. If this wasn't just a test, I would refuse to run.");
            //System.exit(1);
        } else {
        logger.log(Level.ALL, "Confirmed swap space is disabled. Continuing execution.");
        }

      /* Test to make sure core dumps are off */
        if (coreDumpsEnabled()) {
            // Log message to enterprise logger.
            logger.log(Level.SEVERE, "Core dumps enabled. Refusing to run.");
            System.exit(1);
        } else {
            logger.log(Level.ALL, "Confirmed that core dumps are disabled. Continuing execution.");
        }

      /* Spawn a concurrent user watch thread */
        ConcurrentUserWatch cuw = new ConcurrentUserWatch("billrobe");
        cuw.start();

      /* Initialize an SSH connection and forward ports */
        AHCConnector tunnel = new AHCConnector(prop.getProperty("rhost"),
                prop.getProperty("username"), prop.getProperty("password"), logger);

      /* The analysis engine for processing text */
        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
        UimaAsBaseCallbackListener asyncListener = new ProcessCallbackListener(uimaAsEngine);

      /* Constructs a class to create and run a UIMA pipeline */
        AgentPipeline uimaPipeline = new AgentPipeline(asyncListener, uimaAsEngine);

      /* Run the sample document through the pipeline */
        System.out.println("Ready to process document...");

      /* Check to make certain the callback listener closed the tunnel */
        int maxWait = 4;
        int curWait = 0;
        while (tunnel.isConnected()) {
            curWait++;
            if (curWait > maxWait) {
                //tunnel.disconnect(); // This will break any files currently within the pipeline.
            }
        }
    }

    public static Properties getProperties() throws Exception {
        Properties prop = new Properties();
        String dir = System.getProperty("user.home");
        InputStream in = new FileInputStream(dir + "/.biomedicus.properties");
        prop.load(in);
        in.close();
        return prop;
    }

    private static boolean systemHasConcurrentLogin() {
        boolean concurrentUser = false;
        Process p;
        try {
            p = Runtime.getRuntime().exec("who");
            p.waitFor();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            StringBuffer sb = new StringBuffer();
            while ((line = reader.readLine())!= null) {
                String username = line.split("\\s+")[0];
                // The user MUST be bill0154
                // TODO: get allowed username from properties file in future
                if (!username.equals("bill0154")) {
                    concurrentUser = true;
                }
            }
        } catch (IOException e) {
            concurrentUser = true;  // not really true, but in case of an exception, must assume could be True.
        } catch (InterruptedException e) {
            concurrentUser = true;  // not really true, but in case of an exception, must assume could be True.
        }
        return concurrentUser;
    }


    public static boolean swapIsEnabled() {
        boolean isSwapEnabled = false;
        Process p;
        try {
            p = Runtime.getRuntime().exec("swapon -s");
            p.waitFor();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            StringBuffer sb = new StringBuffer();
            int lineCount = 0;
            while ((line = reader.readLine())!= null) {
                lineCount++;  // The first line is the headers. If it is more than one line, a swap is enabled
                if (lineCount > 1) {
                    isSwapEnabled = true;
                }
            }
        } catch (IOException e) {
            isSwapEnabled = true;  // not really true, but in case of an exception, must assume could be True.
        } catch (InterruptedException e) {
            isSwapEnabled = true;  // not really true, but in case of an exception, must assume could be True.
        }
        return isSwapEnabled;
    }

    public static boolean coreDumpsEnabled() {
        boolean coreDumpsEnabled = false;
        Process p;
        try {
            p = Runtime.getRuntime().exec("ulimit -c");
            p.waitFor();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            StringBuffer sb = new StringBuffer();
            while ((line = reader.readLine())!= null) {
                sb.append(line);
            }

            int dumpValue = Integer.valueOf(sb.toString());
            if (dumpValue != 0) {
                coreDumpsEnabled = true;
            }
        } catch (IOException e) {
            coreDumpsEnabled = true;  // not really true, but in case of an exception, must assume could be True.
        } catch (InterruptedException e) {
            coreDumpsEnabled = true;  // not really true, but in case of an exception, must assume could be True.
        }
        return coreDumpsEnabled;
    }
}