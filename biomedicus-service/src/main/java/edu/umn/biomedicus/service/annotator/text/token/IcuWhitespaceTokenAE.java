/* 
 Copyright 2013 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.annotator.text.token;

import com.ibm.icu.text.BreakIterator;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;

/**
 * The IcdWhitespaceTokenizer class uses IBM's ICU text toolkit to identify all the sequences not
 * uninterrupted by whitespace.
 * 
 * @author University of Minnesota NLP/IE Group, "Robert Bill" <bill0154@umn.edu>
 * 
 */

public class IcuWhitespaceTokenAE extends JCasAnnotator_ImplBase {

  protected BreakIterator tokenizer;

    @Override
  public void initialize(UimaContext context) {

    this.tokenizer = BreakIterator.getWordInstance();

  }

  @Override
  public void process(JCas jcas) throws AnalysisEngineProcessException {

      // Get raw text from jCas
    String text = jcas.getDocumentText();

    // set document text in tokenizer
    tokenizer.setText(text);

    // mark tokens as those sequences that do not cross whitespace
    int start = tokenizer.first();
    for (int end = tokenizer.next(); end != BreakIterator.DONE; start = end, end = tokenizer.next()) {
      if ((end - start) == 1 && Character.isWhitespace(text.codePointAt(start)))
        continue;
      Token token = new Token(jcas, start, end);
      token.addToIndexes();
    }
  }
}