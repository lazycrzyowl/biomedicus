package edu.umn.biomedicus.service;

import org.apache.commons.cli.*;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.examples.flow.AdvancedFixedFlowController;
import org.apache.uima.util.FileUtils;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Properties;

public class ParallelApplication {

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(ParallelApplication.class);
    @NotNull
    private static long before = -1;
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static Path projectDirectory;
    @NotNull
    private static String suffix;
    private AdvancedFixedFlowController affc;
    private ParallelPipeline uimaPipeline;

    public ParallelApplication(Path configDirectory, Path inputDirectory, Path outputDirectory)
            throws Exception, UIMAException {

//        this.configDirectory = configDirectory;
        this.outputDirectory = outputDirectory;

        // Get pipeline
        uimaPipeline = new ParallelPipeline(inputDirectory, outputDirectory);//configDirectory);
    }

    public void process(Iterator<Path> files) throws Exception {
        int documentCount = 0;
        while (files.hasNext()) {
            double start = System.currentTimeMillis();
            documentCount++;
            Path currentFile = files.next();
            String documentText = FileUtils.file2String(currentFile.toFile());
            System.out.println("Processing " + currentFile.toString());

            // skip empty documents
            if (documentText == null || documentText.trim() == "") {
                continue;
            }

            // run the sample document through the pipeline
            long before = System.currentTimeMillis();
            CAS output = uimaPipeline.process(currentFile.getFileName().toString(), documentText);

            long after = System.currentTimeMillis();
            double end = System.currentTimeMillis();
            double totalTime = (end - start) / 1000;
            double timePerDocument = totalTime / Double.valueOf(documentCount);
            System.out.println("Total number of documents = " + documentCount);
            System.out.println("Total processing time = " + totalTime);
            System.out.println("Processing time per document = " + timePerDocument);
        }
    }

    public static void main(String[] args) throws CASException, Exception {

        // Get general properties
        CompositeConfiguration _config = BiomedicusAHC2MSIConfig.getConfig();
        System.out.println(_config.getString("biomedicus_cli_banner"));

        // Start logging
        logger.setLevel(Level.OFF);

        // Construction commandline options, get values, and validate what was supplied
        Options cliOptions = getCLIOptions();
        CommandLine cliValues = getCLIValues(cliOptions, args);
        String suffix = ".txt";

        if (cliValues.hasOption("projectdir")) {
            // set path for input and output

            inputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"), "input");
            outputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"), "output");
            projectDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"));

            // get suffix, or set to default (.txt)
            String suffix_opt = cliValues.getOptionValue("suffix");
            if (suffix_opt != null && suffix_opt != "") {
                suffix = suffix_opt;
            }

            // display provided options
            System.out.println("Setting project directory to: " + projectDirectory.toString());
            System.out.println("Setting input directory to: " + inputDirectory.toString());
            System.out.println("Setting destination directory to: " + outputDirectory.toString());
            System.out.println("Setting input file suffix to: " + suffix);

        } else {
            printHelp("BioMedICUS Project", cliOptions);
        }

        // Create a DirectoryStream which accepts only filenames ending with specified suffix
        DirectoryStream<Path> ds = Files.newDirectoryStream(inputDirectory, "*" + suffix);

        // constructs a class to create and run a UIMA pipeline
        String od = _config.getString("output_dir");
        ParallelApplication app = new ParallelApplication(
                projectDirectory, inputDirectory, outputDirectory);


        app.process(ds.iterator());
        // constructs a class to create and run a UIMA pipeline
        //ParallelPipeline uimaPipeline = new ParallelPipeline();

        // run the sample document through the pipeline
//        System.out.println("Processing document...");
//        before = System.currentTimeMillis();
        //uimaPipeline.process("Father has diabetes.");
    }

    private static Options getCLIOptions() {
        // create the Options
        Options options = new Options();
//        options.addOption("c", "config", true, "Configuration/descriptor directory to obtain settings");
//        options.addOption("i", "input", true, "Input directory containing text files (*.txt).");
//        options.addOption("d", "destination", true, "Destination directory to output xmi files.");
//        options.addOption("s", "suffix", true, "Suffix/extension of input files to process (default=.txt)");
        options.addOption("p", "projectdir", true, "Location of project directory.");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args) {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }
}
