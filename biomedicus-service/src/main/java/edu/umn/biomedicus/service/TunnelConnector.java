package edu.umn.biomedicus.service;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import java.io.Console;

/**
 * Add service over an SSH tunnel.
 * 
 * @author Robert Bill bill0154@umn.edu
 */
public class TunnelConnector {

    Session session;
    Logger logger;

    /**
     * Initialize tunnel connection 
     */
    public TunnelConnector(String rhost, String username, String password, Logger logger) throws JSchException {
      /* Get an ssh connection to AHC, and forward ports */
        this.logger = logger;
        logger.log(Level.ALL, "Initializing SSH tunnel to data center");
        // 7800-7806 for infinispan. 61616 for activemq,
        // TODO: Make these named variables so that the port definitions are easy to understand.
        // TODO: Get those ports from prop file because they will change

        // Disable infinispan- not using yet
        // int[] ports = {7800, 7801, 7802, 61616, 11222};
        int[] ports = {61616, 11222};

        //TODO: Change to use public/private key
        session = null;
        Console console = System.console();

        //Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        JSch jsch = new JSch();
        session = jsch.getSession(username, rhost, 22);
        session.setPassword(password);
        session.setConfig(config);
        session.connect();

        logger.log(Level.INFO, "SSH Tunnel is connected.");
        logger.log(Level.INFO, "Forwarding ports...");
        int[] assigned_ports = new int[ports.length];
        for (int i=0; i<ports.length;i++) {
            //assigned_ports[i] = session.setPortForwardingL(ports[i], rhost, ports[i]);
            assigned_ports[i] = session.setPortForwardingL(ports[i], rhost, ports[i]);
            logger.log(Level.INFO, String.format("%s:%d --port forwarded--> %s:%d", rhost, ports[i], "localhost", ports[i]));
        }
    }

    public boolean isConnected() {
        return session.isConnected();
    }

    public void disconnect() {
        if (session != null && session.isConnected()) {
            System.out.println("Closing SSH Connection");
            session.disconnect();
        }
    }
}

