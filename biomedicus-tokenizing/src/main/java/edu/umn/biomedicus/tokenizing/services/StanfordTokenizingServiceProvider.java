package edu.umn.biomedicus.tokenizing.services;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class StanfordTokenizingServiceProvider implements TokenizingServiceProvider, SharedResourceObject
{
    @NotNull
    TokenizerFactory<CoreLabel> tokenizerFactory;
    @NotNull
    List<Span> results = new ArrayList<>();

    @Override
    public void load(DataResource dataResource) throws ResourceInitializationException {
        tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
    }

    @Override
    public StanfordTokenizingServiceProvider analyze(String documentText) {
        runAnalysis(documentText, null);
        return this;
    }

    private List<Span> runAnalysis(String documentText, CAS view) {
        TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");

        List<CoreLabel> tokens = tokenizerFactory.getTokenizer(new StringReader(documentText)).tokenize();

        for (CoreLabel token : tokens) {
            int begin = token.beginPosition();
            int end = token.endPosition();
            String tokenText = token.toString();

            if (!documentText.substring(begin, end).trim().equals(""))
            {
                Span span = new Span(begin, end);
                results.add(span);
                if (view != null) annotate(span, view);
            }
        }

        return results;
    }

    @Override
    public void annotateCAS(CAS view, String documentText) {
        runAnalysis(documentText, view);
    }

    private AnnotationFS annotate(Span span, CAS view) {
        AnnotationFS annot = AnnotationUtils.createAnnotation(view, Token.class, span.getStart(),
                span.getEnd());
        return annot;
    }

    @Override
    public AnnotationFS[] annotateCAS(CAS view)
    {
        List<AnnotationFS> annotationList = new ArrayList<>();
        for (Span span : results) {
            AnnotationFS annot = AnnotationUtils.createAnnotation(view, Token.class, span.getStart(),
                    span.getEnd());
            annotationList.add(annot);
        }
        AnnotationFS[] annotArray = new AnnotationFS[annotationList.size()];
        return annotationList.toArray(annotArray);
    }

    @Override
    public Span[] getSpans() {
        Span[] spanArray = new Span[results.size()];
        return results.toArray(spanArray);
    }
}

