/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.tokenizing.services;


import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.type.Token;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class OpenNLPTokenizingServiceProvider
        implements TokenizingServiceProvider, SharedResourceObject {

    @Nullable
    private TokenizerModel tokenModel;
    @NotNull
    private Tokenizer tokenizer;
    @NotNull
    private opennlp.tools.util.Span[] results;

    @Override
    public TokenizingServiceProvider analyze(String documentText)
    {
        results = tokenizer.tokenizePos(documentText);
        return this;
    }

    public Span[] getSpans()
    {
        Span[] spanArray = new Span[results.length];
        for (int i = 0; i < results.length; i++)
        {
            int begin = results[i].getStart();
            int end = results[i].getEnd();
            spanArray[i] = new Span(begin, end);
        }
        return spanArray;
    }

    public AnnotationFS[] annotateCAS(CAS view)
    {
        AnnotationFS[] annotations = new AnnotationFS[results.length];
        Type sentenceType = view.getTypeSystem().getType("edu.umn.biomedicus.type.Sentence");
        for (int i = 0; i < results.length; i++)
        {
            int begin = results[i].getStart();
            int end = results[i].getEnd();
            annotations[i] = AnnotationUtils.createAnnotation(view, Token.class, begin, end);
        }
        return annotations;
    }

    public synchronized void annotateCAS(CAS view, String documentText)
    {
        List<opennlp.tools.util.Span> spanList = new ArrayList<>();
        for (opennlp.tools.util.Span span : tokenizer.tokenizePos(documentText))
        {
            AnnotationFS annot = null;
            String coveredText = documentText.substring(span.getStart(), span.getEnd());
            if (coveredText.indexOf('|') != -1)
            {
                int i = 0;
                int start = span.getStart();
                for (char c : coveredText.toCharArray())
                {
                    if (c == '|')
                    {
                        int end = start + i;
                        opennlp.tools.util.Span newSpan = new opennlp.tools.util.Span(start, end);
                        spanList.add(newSpan);
                        annot = AnnotationUtils.createAnnotation(view, Token.class, start, end);
                        //view.addFsToIndexes(annot);

                        // add pipe
                        spanList.add(new opennlp.tools.util.Span(end, end + 1));
                        annot = AnnotationUtils.createAnnotation(view, Token.class, end, end + 1);
                        //view.addFsToIndexes(annot);
                        start = end + 1;
                        i = -1;
                    }
                    i++;
                }
                if (start < span.getEnd())
                {
                    spanList.add(new opennlp.tools.util.Span(start, span.getEnd()));
                    annot = AnnotationUtils.createAnnotation(view, Token.class, start, span.getEnd());
                    //view.addFsToIndexes(annot);
                }
            }
            else {
                spanList.add(span);
                annot = AnnotationUtils.createAnnotation(view, Token.class,
                                span.getStart(), span.getEnd());
                //view.addFsToIndexes(annot);
            }
        }

        opennlp.tools.util.Span[] spanArray = new opennlp.tools.util.Span[spanList.size()];
        results = spanList.toArray(spanArray);
    }

    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {
        URL resource = aData.getUrl();
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        }
        catch (IOException e)
        {throw new ResourceInitializationException(e);}

        try
        {
            tokenModel = new TokenizerModel(new FileInputStream(file));
            if (tokenModel == null) throw new IOException("Failed to load tokenizer model.");
        }
        catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
        tokenizer = new TokenizerME(tokenModel);
   }
}
