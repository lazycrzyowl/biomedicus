package edu.umn.biomedicus.tokenizing.opennlp;


import edu.umn.biomedicus.core.featureranges.ContinuousSpan;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MaxEntTokenizer {

    @NotNull
    private Tokenizer tokenizer;
    @NotNull
    private TokenizerModel tokenModel;
    @NotNull
    private char[] splitCharacters = "|".toCharArray();

    private MaxEntTokenizer(TokenizerModel tokenModel) throws IOException {
        tokenizer = new TokenizerME(tokenModel);
    }

    public static MaxEntTokenizer buildFromModel(File modelFile) throws IOException {
        TokenizerModel tokenModel = new TokenizerModel(new FileInputStream(modelFile));
        return new MaxEntTokenizer(tokenModel);
    }

    /**
     * Calls the MaxEnt tokenizePOS method.
     *
     * @param text The text to tokenize.
     * @return A Span[] array designating all token begin/end points.
     */
    public Span[] tokenize(String text) {
        return tokenizer.tokenizePos(text);
    }

    /**
     * Converts an OpenNLP Span array to ContinuousSpan array
     *
     * @param spans The Span array produced from the MaxEnt tokenizer.
     * @return A ContinuousSpan array equivalent to the OpenNLP Span array in the parameter.
     */
    public ContinuousSpan[] toContinuousSpans(Span[] spans) {
        ContinuousSpan[] results = new ContinuousSpan[spans.length];
        for (int i = 0; i < spans.length; i++)
            results[i] = new ContinuousSpan(spans[i].getStart(), spans[i].getEnd());
        return results;
    }

    /**
     * Calls the MaxEnt tokenizePOS method and splits token candidates on symbols
     * that are provided the the char[] parameter. The reason for this method is that
     * some symbols that should be split on do not appear frequently enough in the training corpus
     * for the MaxEnt tokenizer to accurately recognize the token boundary.
     *
     * @param text
     * @param splitTokens
     * @return
     */
    public ContinuousSpan[] tokenize(String text, char[] splitTokens) {
        Span[] candidates = tokenize(text);
        return splitSymbols(candidates, text, splitTokens);
    }

    /**
     * Calls the MaxEnt tokenizePOS method and then splits token candidates at each
     * of the index locations provided in the splitSymbolIndexes parameter. The
     * reason for this method is that a separate process ran previous to the
     * tokenizer could have generated a list of split locations (e.g., a
     * symbol disambiguation module). This method is a convenient way to
     * incorporate that information into tokenizing. This is valuable
     * because some symbols do not occur frequently enough in training
     * corpora for MaxEnt to accurately identify them as a token boundary.
     *
     * @param text               The original text to tokenize.
     * @param splitSymbolIndexes An int[] array of indexes that should be split as single-character tokens.
     * @return
     */
    public ContinuousSpan[] tokenize(String text, List<Integer> splitSymbolIndexes) {
        Span[] candidates = tokenize(text);
        return splitIndexes(candidates, splitSymbolIndexes);
    }

    /**
     * This method is a way to identify uncommon symbols that should indicate a token boundary.
     * Because many of the corpora did not have enough annotations for some symbols (e.g., "|"),
     * this is used to force a split when there is insufficient counts for MaxEnt to identify
     * the token boundary.
     *
     * @param candidates An array of OpenNLP Spans produced from the maxent tokenizer.
     * @param text       The original text being tokenized.
     * @return A ContinuousSpan array.
     */
    private ContinuousSpan[] splitSymbols(Span[] candidates, String text, char[] splitCharacters) {
        List<ContinuousSpan> spanList = new ArrayList<>();

        for (Span span : candidates) {
            String coveredText = text.substring(span.getStart(), span.getEnd());

            List<Integer> splitIndexes = new ArrayList<>();
            for (char splitChar : splitCharacters) {
                for (int index = coveredText.indexOf(splitChar);
                     index >= 0;
                     index = coveredText.indexOf(splitChar, index + 1))
                {
                    splitIndexes.add(span.getStart() + index);
                }
            }

            if (splitIndexes.size() > 0)
            {
                Span[] currentCandidate = new Span[1];
                currentCandidate[0] = span;
                ContinuousSpan[] splitSpans = splitIndexes(currentCandidate, splitIndexes);
                Collections.addAll(spanList, splitSpans);
            }
            else
            {
                spanList.add(new ContinuousSpan(span.getStart(), span.getEnd()));
            }
        }
        return spanList.toArray(new ContinuousSpan[spanList.size()]);
    }

    /**
     * This method is a way to identify uncommon symbols that should indicate a token boundary.
     * Because many of the corpora did not have enough annotations for some symbols (e.g., "|"),
     * this is used to force a split when there is insufficient counts for MaxEnt to identify
     * the token boundary.
     *
     * @param candidates An array of OpenNLP Spans produced from the maxent tokenizer.
     * @param splitIndexes
     *     A List of integers that identify the indexes the tokenizer should identify as separate tokens.
     * @return A ContinuousSpan array.
     */
    private ContinuousSpan[] splitIndexes(Span[] candidates, List<Integer> splitIndexes) {
        Collections.sort(splitIndexes);
        List<ContinuousSpan> spanList = new ArrayList<>();
        for (Span span : candidates) {
            int begin = span.getStart();
            int end = span.getEnd();
            for (int i = begin; i < end; i++) {

                if (splitIndexes.contains(i)) {
                    // Add span from beginning, if not at beginning
                    if (i - begin > 0) spanList.add(new ContinuousSpan(begin, i));

                    // Add the single character at the target index.
                    // The assumption is that the index contains a symbol that is a token.
                    spanList.add(new ContinuousSpan(i, i + 1));
                    begin = i + 1;
                }
            }
            if (end - begin > 0) spanList.add(new ContinuousSpan(begin, end));
        }
        return spanList.toArray(new ContinuousSpan[spanList.size()]);
    }
}