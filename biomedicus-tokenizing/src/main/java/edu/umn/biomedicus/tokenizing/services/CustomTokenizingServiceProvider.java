package edu.umn.biomedicus.tokenizing.services;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.Token;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * An early test of an acronym and special symbol aware tokenizer. This annotator requires that
 * the acromym detector and symbol detector appears previously in the pipeline.
 *
 * DO NOT USE, this is just template code
 */
public class CustomTokenizingServiceProvider implements TokenizingServiceProvider, SharedResourceObject
{
    @NotNull
    TokenizerFactory<CoreLabel> tokenizerFactory;
    @NotNull
    List<Span> results = new ArrayList<>();

    @Override
    public void load(DataResource dataResource) throws ResourceInitializationException {
        tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
    }

    @Override
    public CustomTokenizingServiceProvider analyze(String documentText) {
        runAnalysis(documentText, null);
        return this;
    }

    private int addToken(List<String> tokens, StringBuilder sb, int begin, int end, CAS view) {
        Span span = new Span(begin, end);
        results.add(span);
        if (view != null) annotate(span, view);
        ++end;
        int spanEnd = span.getEnd();
        return end;
    }

    private List<Span> runAnalysis(String documentText, CAS view) {
        List<String> tokens = new ArrayList<String>(); StringBuilder sb = new StringBuilder();

        int lastIndex = 0;
        char[] arr = documentText.toCharArray();
        for (int i = 0; i < arr.length; i++) {

            char prior = (i - 1 > 0) ? arr[i - 1] : ' '; char current = arr[i];
            char next = (i + 1 < arr.length) ? arr[i + 1] : ' ';

            // extract acronyms
            // this will actually extract acronyms of any length
            // once it detects this pattern a.b.c
            // it's a greedy lexer that breaks at ' '
            if (Character.isAlphabetic(current) && '.' == next) {

                // Pattern-1	= U.S.A 	(5 chars)
                // Pattern-2	= U.S.A. 	(6 chars)
                if (i + 5 < documentText.length()) {

                    // Pattern-1
                    if (Character.isAlphabetic(arr[i]) && '.' == arr[i + 1] && Character.isAlphabetic(arr[i + 2]) &&
                            '.' == arr[i + 3] && Character.isAlphabetic(arr[i + 4])) {

                        for (; i < arr.length && arr[i] != ' '; i++) {
                            sb.append(arr[i]);
                        }

                        // check for Pattern-2 (trailing '.')
                        if (i + 1 < documentText.length() && '.' == arr[i + 1]) {
                            sb.append(arr[i++]);
                        }

                        lastIndex = addToken(tokens, sb, lastIndex, i, view);

                        sb = new StringBuilder();
                        continue;
                    }
                }
            }

            if ('w' == current && '/' == next) {
                sb.append(current); sb.append(next);
                lastIndex = addToken(tokens, sb, lastIndex, i, view);
                sb = new StringBuilder(); i += 1; continue;
            }

            // extract URLs
            if ('h' == current && 't' == next) {
                if (i + 7 < documentText.length() && "http://".equals(documentText.substring(i, i + 7))) {

                    for (; i < arr.length && arr[i] != ' '; i++) {
                        sb.append(arr[i]);
                    }

                    lastIndex = addToken(tokens, sb, lastIndex, i, view);
                    sb = new StringBuilder();
                    continue;
                }
            }

            // extract windows drive letter paths
            // c:/ or c:\
            if (Character.isAlphabetic(current) && ':' == next) {
                if (i + 2 < documentText.length() && (arr[i + 2] == '\\' || arr[i + 2] == '/')) {
                    sb.append(current); sb.append(next); sb.append(arr[i + 2]); i += 2;
                    continue;
                }
            }

            // keep numbers together when separated by a period
            // "4.0" should not be tokenized as { "4", ".", "0" }
            if (Character.isDigit(current) && '.' == next) {
                if (i + 2 < documentText.length() && Character.isDigit(arr[i + 2])) {
                    sb.append(current); sb.append(next); sb.append(arr[i + 2]); i += 2;
                    continue;
                }
            }

            // keep alpha characters separated by hyphens together
            // "b-node" should not be tokenized as { "b", "-", "node" }
            if (Character.isAlphabetic(current) && '-' == next) {
                if (i + 2 < documentText.length() && Character.isAlphabetic(arr[i + 2])) {
                    sb.append(current); sb.append(next); sb.append(arr[i + 2]); i += 2;
                    continue;
                }
            }

            // TODO: need a greedy look-ahead to
            // avoid splitting this into multiple tokens
            // "redbook@vnet.ibm.com" currently is
            // tokenized as { "redbook@vnet", ".", "ibm", ".", "com" }
            // need to greedily lex all tokens up to the space
            // once the space is found, see if the last 4 chars are '.com'
            // if so, then take the entire segment as a single token
            // don't separate tokens concatenated with an underscore
            // eg. "ws_srv01" is a single token, not { "ws", "_", "srv01" }
            if (Character.isAlphabetic(current) && '_' == next) {
                if (i + 2 < documentText.length() && Character.isAlphabetic(arr[i + 2])) {
                    sb.append(current); sb.append(next); i++; continue;
                }
            }

            // keep tokens like tcp/ip and os/2 and system/z together
            if (' ' != current && '/' == next) {
                sb.append(current); sb.append(next); i++; continue;
            }

            if (' ' == current) {
                lastIndex = addToken(tokens, sb, lastIndex, i, view);
                sb = new StringBuilder();
                continue;
            }

            // don't tokenize on <word>'s or <words>'
            // but do tokenize on '<words>
            if ('\'' == current) {
                if (' ' == prior) {
                    lastIndex = addToken(tokens, new StringBuilder("'"), lastIndex, i, view);
                } else {
                    sb.append(current);
                }

                continue;
            }

            sb.append(current);
        }

        if (0 != sb.length()) {
            addToken(tokens, sb, lastIndex, documentText.length(), view);
        }

        return results;
    }

    @Override
    public void annotateCAS(CAS view, String documentText) {
        runAnalysis(documentText, view);
    }

    private AnnotationFS annotate(Span span, CAS view) {
        AnnotationFS annot = AnnotationUtils.createAnnotation(view, Token.class, span.getStart(),
                span.getEnd());
        return annot;
    }

    @Override
    public AnnotationFS[] annotateCAS(CAS view) {
        List<AnnotationFS> annotationList = new ArrayList<>(); for (Span span : results) {
            AnnotationFS annot = AnnotationUtils.createAnnotation(view, Token.class, span.getStart(),
                    span.getEnd());
            annotationList.add(annot);
        } AnnotationFS[] annotArray = new AnnotationFS[annotationList.size()];
        return annotationList.toArray(annotArray);
    }

    @Override
    public Span[] getSpans() {
        Span[] spanArray = new Span[results.size()]; return results.toArray(spanArray);
    }
}
