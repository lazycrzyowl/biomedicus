/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.tokenizing.services;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.type.Token;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WhitespaceAndSymbolTokenizingServiceProvider
        implements TokenizingServiceProvider, SharedResourceObject {


    private static final int CH_SPECIAL = 0;
    private static final int CH_NUMBER = 1;
    private static final int CH_LETTER = 2;
    private static final int CH_WHITESPACE = 4;
    private static final int CH_PUNCTUATION = 5;
    private static final int CH_NEWLINE = 6;
    private static final int UNDEFINED = -1;
    private static final int INVALID_CHAR = 0;
    private static List<String> punctuations = Arrays.asList(new String[]{".", "!", "?"});

    @NotNull
    List<Span> results = new ArrayList<>();

    @Override
    public WhitespaceAndSymbolTokenizingServiceProvider analyze(@NotNull String documentText)
    {
        // get text content from the CAS
        char[] textContent = documentText.toCharArray();

        int tokenStart = UNDEFINED;
        int currentCharPos = 0;
        int sentenceStart = 0;
        int nextCharType = UNDEFINED;
        char nextChar = INVALID_CHAR;

        while (currentCharPos < textContent.length) {
            char currentChar = textContent[currentCharPos];
            int currentCharType = getCharacterType(currentChar);
            nextChar = getNextChar(textContent, currentCharPos);
            nextCharType = getCharacterType(nextChar);

            // check if current character is a letter or number
            if (currentCharType == CH_LETTER || currentCharType == CH_NUMBER) {

                // check if it is the first letter of a token
                if (tokenStart == UNDEFINED) {
                    // start new token here
                    tokenStart = currentCharPos;
                }
            }

            // check if current character is a whitespace character
            else if (currentCharType == CH_WHITESPACE
                    || currentCharType == CH_SPECIAL
                    || currentCharType == CH_NEWLINE
                    || currentCharType == CH_PUNCTUATION) {

                // terminate current token
                if (tokenStart != UNDEFINED) {
                    // end of current word
                    results.add(new Span(tokenStart, currentCharPos));
                    tokenStart = UNDEFINED;
                }

                if (currentCharType == CH_SPECIAL) {
                    // create token for special character
                    results.add(new Span(currentCharPos, currentCharPos + 1));
                } else if (currentCharType == CH_PUNCTUATION) {
                    // create token for punctuation character
                    results.add(new Span(currentCharPos, currentCharPos + 1));
                }
            }

            // go to the next token
            currentCharPos++;
        } // end of character loop

        // we are at the end of the text terminate open token annotations
        if (tokenStart != UNDEFINED) {
            // end of current word
            results.add(new Span(tokenStart, currentCharPos));
            tokenStart = UNDEFINED;
        }
        return this;
    }

    /**
     * Get the next character from the text stream, or return INVALID_CHAR.
     *
     * @param textContent
     * A char[] holding the entire text.
     *
     * @param currentCharPos
     * An int that is the index of the current point in the text.
     *
     * @return
     * char at the next text location.
     */
    public char getNextChar(char[] textContent, int currentCharPos) {

        // get character class for current and next character
        char nextChar = INVALID_CHAR;
        if ((currentCharPos + 1) < textContent.length) nextChar = textContent[currentCharPos + 1];
        return nextChar;
    }

    /**
     * returns the character type of the given character. Possible character classes are: CH_LETTER for all letters
     * CH_NUMBER for all numbers CH_WHITESPACE for all whitespace characters CH_PUNCTUATUATION for all punctuation
     * characters CH_NEWLINE for all new line characters CH_SPECIAL for all other characters that are not in any of the
     * groups above
     *
     * @param character aCharacter
     * @return returns the character type of the given character
     */
    private static int getCharacterType(char character) {

        switch (Character.getType(character)) {

            // letter characters
            case Character.UPPERCASE_LETTER:
            case Character.LOWERCASE_LETTER:
            case Character.TITLECASE_LETTER:
            case Character.MODIFIER_LETTER:
            case Character.OTHER_LETTER:
            case Character.NON_SPACING_MARK:
            case Character.ENCLOSING_MARK:
            case Character.COMBINING_SPACING_MARK:
            case Character.PRIVATE_USE:
            case Character.SURROGATE:
            case Character.MODIFIER_SYMBOL:
                return CH_LETTER;

            // number characters
            case Character.DECIMAL_DIGIT_NUMBER:
            case Character.LETTER_NUMBER:
            case Character.OTHER_NUMBER:
                return CH_NUMBER;

            // whitespace characters
            case Character.SPACE_SEPARATOR:
                // case Character.CONNECTOR_PUNCTUATION:
                return CH_WHITESPACE;

            case Character.DASH_PUNCTUATION:
            case Character.START_PUNCTUATION:
            case Character.END_PUNCTUATION:
            case Character.OTHER_PUNCTUATION:
                return CH_PUNCTUATION;

            case Character.LINE_SEPARATOR:
            case Character.PARAGRAPH_SEPARATOR:
                return CH_NEWLINE;

            case Character.CONTROL:
                if (character == '\n' || character == '\r') {
                    return CH_NEWLINE;
                } else {
                    // tab is in the char category CONTROL
                    if (Character.isWhitespace(character)) {
                        return CH_WHITESPACE;
                    }
                    return CH_SPECIAL;
                }

            case INVALID_CHAR:
                return UNDEFINED;

            default:
                // the isWhitespace test is slightly more expensive than the above switch,
                // so it is placed here to avoid performance impact.
                // Also, calling code has explicit tests for CH_NEWLINE, and this test should not swallow
                // those
                if (Character.isWhitespace(character)) {
                    return CH_WHITESPACE;
                }
                return CH_SPECIAL;
        }
    }

    @Override
    public Span[] getSpans()
    {
        Span[] spanArray = new Span[results.size()];
        return results.toArray(spanArray);
    }

    @Override
    public AnnotationFS[] annotateCAS(CAS view)
    {
        AnnotationFS[] annotations = new AnnotationFS[results.size()];
        Type sentenceType = view.getTypeSystem().getType("edu.umn.biomedicus.type.Token");
        for (int i = 0; i < results.size(); i++)
        {
            int begin = results.get(i).getStart();
            int end = results.get(i).getEnd();
            annotations[i] = AnnotationUtils.createAnnotation(view, Token.class, begin, end);
        }
        return annotations;
    }

    @Override
    public void annotateCAS(CAS view, String documentText)
    {
        analyze(documentText);
        annotateCAS(view);
    }

    @Override
    public void load(DataResource aDataResource)
    {
        //no-op. No resources used.
    }
}
