/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.tokenizing;


import edu.umn.biomedicus.tokenizing.services.OpenNLPTokenizingServiceProvider;
import edu.umn.biomedicus.tokenizing.services.WhitespaceAndSymbolTokenizingServiceProvider;

public class ExampleCode {

    private static String[] expectedResults;
    private static String[] results;
    private static Tokenizer tokenizer;

    /**
     * This is a simple main class to show how to instantiate the tokenizer with different
     * tokenizing service providers. The service provider does the tokenization. While this is
     * not overly useful for tokenization due to the single service prover, other more
     * complicated annotators benefit from dividing tasks out into interchangeable service providers.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
        String test = "Tokenizing text requires that acronyms/abbreviations are kept together; however, " +
                "other punctuation should be segmented, such as the period at the end of this sentence. " +
                "This is a punctuation-rich test about maybe a Dr. with a Ph.D. weighing 180lbs that took cipro 100mg b.i.d. " +
                "Something difficult to test (evaluate) is parentheticals and odd|separations.";
        String[] expectedResults = {"Tokenizing", "text", "requires", "that", "acronyms/abbreviations",
                "are", "kept", "together", ";", "however", ",", "other", "punctuation", "should", "be", "segmented",
                ",", "such", "as", "the", "period", "at", "the", "end", "of", "this", "sentence", ".", "This", "is",
                "a", "punctuation-rich", "test", "about", "maybe", "a", "Dr.", "with", "a", "Ph.D.", "weighing",
                "180lbs", "that", "took", "cipro", "100mg", "b.i.d", ".", "Something", "difficult", "to", "test",
                "(", "evaluate", ")", "is", "parentheticals", "and", "odd", "|", "separations", "."};

        tokenizer = new Tokenizer.Builder()
                .service(OpenNLPTokenizingServiceProvider.class)
                .model("edu/umn/biomedicus/tokenizing/en-token.bin")
                .build();

        results = tokenizer.process(test);
        System.out.println(match(results, expectedResults));


//        whitespace test

        expectedResults = new String[] {"Tokenizing", "text", "requires", "that", "acronyms", "/", "abbreviations",
                "are", "kept", "together", ";", "however", ",", "other", "punctuation", "should", "be", "segmented",
                ",", "such", "as", "the", "period", "at", "the", "end", "of", "this", "sentence", ".",
                "This", "is", "a", "punctuation", "-", "rich", "test", "about", "maybe", "a", "Dr", ".", "with",
                "a", "Ph", ".", "D", ".", "weighing", "180lbs", "that", "took", "cipro", "100mg", "b", ".",
                "i", ".", "d", ".", "Something", "difficult", "to", "test", "(", "evaluate", ")", "is",
                "parentheticals", "and", "odd", "|", "separations", "."};

        tokenizer = new Tokenizer.Builder()
                .service(WhitespaceAndSymbolTokenizingServiceProvider.class)
                .build();

        results = tokenizer.process(test);
        System.out.println(match(results, expectedResults));

    }

    public static boolean match(String[] results, String[] expectedResults)
    {
        assert results.length == expectedResults.length;
        for (int i = 0; i < results.length; i++)
        {
            if (!results[i].equals(expectedResults[i])) return false;
        }
        return true;
    }
}
