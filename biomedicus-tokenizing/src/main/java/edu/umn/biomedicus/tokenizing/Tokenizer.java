package edu.umn.biomedicus.tokenizing;

import edu.umn.biomedicus.core.ResourceFactory;
import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.tokenizing.services.OpenNLPTokenizingServiceProvider;
import edu.umn.biomedicus.tokenizing.services.TokenizingServiceProvider;
import edu.umn.biomedicus.tokenizing.services.WhitespaceAndSymbolTokenizingServiceProvider;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Tokenizer base application.
 */
public final class Tokenizer
{
    @NotNull
    protected AnalysisEngine ae;
    @NotNull
    protected CAS aCAS;
    @NotNull
    protected CAS systemView;

    private Tokenizer(Builder builder) throws ResourceInitializationException
    {
        ae = builder.getAE();
        aCAS = ae.newCAS();
        systemView = CASUtil.getSystemView(aCAS);
    }

    public static void main(String[] args) throws Exception
    {
        String test = "Tokenizing text requires that acronyms/abbreviations are kept together; however, " +
                "other punctuation should be segmented, such as the period at the end of this sentence. " +
                "This is a punctuation-rich test about maybe a Dr. with a Ph.D. weighing 180lbs that took cipro 100mg b.i.d. " +
                "Something difficult to test (evaluate) is parentheticals and odd|separations.";
        String[] expectedResults = {"Tokenizing", "text", "requires", "that", "acronyms/abbreviations",
                "are", "kept", "together", ";", "however", ",", "other", "punctuation", "should", "be", "segmented",
                ",", "such", "as", "the", "period", "at", "the", "end", "of", "this", "sentence", ".", "This", "is",
                "a", "punctuation-rich", "test", "about", "maybe", "a", "Dr.", "with", "a", "Ph.D.", "weighing",
                "180lbs", "that", "took", "cipro", "100mg", "b.i.d", ".", "Something", "difficult", "to", "test",
                "(", "evaluate", ")", "is", "parentheticals", "and", "odd", "|", "separations", "."};

        Tokenizer tokenizer = new Tokenizer.Builder().build();
        String [] results = tokenizer.process(test);
        assert results.length == expectedResults.length;

    }

    public CAS newCAS() throws ResourceInitializationException { return ae.newCAS(); }

    public void process(CAS aCAS) throws AnalysisEngineProcessException { ae.process(aCAS); }

    public String[] process(String text) throws AnalysisEngineProcessException
    {
        aCAS.reset();
        CAS systemView = CASUtil.getSystemView(aCAS);
        systemView.setDocumentText(text);
        ae.process(aCAS);
        AnnotationIndex<Token> tokenIndex = (AnnotationIndex<Token>) AnnotationUtils.getAnnotations(systemView, Token.class);
        String[] tokenArray = new String[tokenIndex.size()];
        int i = 0;
        for (AnnotationFS token : tokenIndex)
        {
            tokenArray[i] = token.getCoveredText();
            i++;
        }
        return tokenArray;
    }

    public AnalysisEngine getAE() { return ae; }



    public static class Builder
    {
        //private String descriptor = "edu/umn/biomedicus/tokenizing/tokenAE.xml";
        private String descriptor = "edu/umn/biomedicus/tokenizing/opennlp/openNLPTokenAE.xml";
        private List<ServiceProviderDescription> serviceProviders  = new ArrayList<>();
        private Properties properties;
        private String serviceProviderClassName;
        private String modelResource;

        public Builder() {}

        public Builder(String descriptor) throws IOException
        {
            this.descriptor = descriptor;
        }

        public Builder serviceProvider(ServiceProviderDescription serviceDesc)
        {
            serviceProviders.add(serviceDesc);
            return this;
        }

        public Builder service(Class<? extends TokenizingServiceProvider> serviceProvider)
        {
            serviceProviderClassName = serviceProvider.getCanonicalName();
            return this;
        }

        public Builder model(String modelResource)
        {
            this.modelResource = modelResource;
            return this;
        }

        public Tokenizer build()  throws ResourceInitializationException
        {
            ServiceProviderDescription tokenizingService = null;
            if (serviceProviderClassName != null && modelResource != null)
            {
                tokenizingService = new ServiceProviderDescription(
                        "tokenizingServiceProvider").serviceProvider(serviceProviderClassName)
                        .modelFile(modelResource);
                this.serviceProvider(tokenizingService);
            }
            else if (serviceProviderClassName != null)
            {
                tokenizingService = new ServiceProviderDescription("tokenizingServiceProvider")
                        .serviceProvider(serviceProviderClassName);
                serviceProvider(tokenizingService);
            }
            return new Tokenizer(this);
        }

        public AnalysisEngine getAE() throws ResourceInitializationException
        {
            URL resource = this.getClass().getClassLoader().getResource(descriptor);
            return ResourceFactory.createAnalysisEngine(resource, serviceProviders);
        }
    }
}


