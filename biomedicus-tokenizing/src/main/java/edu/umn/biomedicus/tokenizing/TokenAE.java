/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.umn.biomedicus.tokenizing;

import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.tokenizing.services.TokenizingServiceProvider;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

public class TokenAE extends CasAnnotator_ImplBase {

    private static final String TOKENIZING_SERVICE_PROVIDER_KEY = "TOKENIZING_SERVICE";

    @NotNull
    private Type tokenType;

    @NotNull
    private TokenizingServiceProvider tokenizer;

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(TokenAE.class);

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        super.initialize(context);
        try
        {
            tokenizer = (TokenizingServiceProvider) context.getResourceObject(TOKENIZING_SERVICE_PROVIDER_KEY);
        }
        catch (ResourceAccessException e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        logger.log(Level.FINEST, "Tokenizer started processing.");
        CAS sysCAS = CASUtil.getSystemView(aCAS);

        @NotNull String documentText = sysCAS.getDocumentText();
        tokenizer.annotateCAS(sysCAS, documentText);
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {
        super.typeSystemInit(typeSystem);
        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        logger.log(Level.FINEST, "WhitespaceTokenAE typeSystem initialized.");
    }
}
