package edu.umn.biomedicus.tokenizing.stanford;


import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.umn.biomedicus.core.featureranges.ContinuousSpan;
import org.apache.uima.cas.Type;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StanfordTokenizer {
    @NotNull
    TokenizerFactory<CoreLabel> tokenizerFactory;
    @NotNull
    List<edu.umn.biomedicus.core.utils.Span> results = new ArrayList<>();
    @NotNull
    private Type tokenType;
    @NotNull
    private char[] splitCharacters = "|".toCharArray();

    public StanfordTokenizer() {
        tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
    }

    public void initialize() throws ResourceInitializationException {
    }

    /**
     * Calls the stanford tokenizer
     *
     * @param text The text to tokenize.
     * @return A Span[] array designating all token begin/end points.
     */
    public List<ContinuousSpan> tokenize(String text) {
        List<ContinuousSpan> tokenList = new ArrayList<>();
        List<CoreLabel> tokens = tokenizerFactory.getTokenizer(new StringReader(text)).tokenize();

        for (CoreLabel candidate : tokens) {
            int begin = candidate.beginPosition();
            int end = candidate.endPosition();

            if (!text.substring(begin, end).trim().equals("")) {
                tokenList.add(new ContinuousSpan(begin, end));
            }
        }
        return tokenList;
    }

    /**
     * Calls the Stanford tokenize method and splits token candidates on symbols
     * that are provided the the char[] parameter. The reason for this method is that
     * some symbols that should be split on do not appear frequently enough in the training corpus
     * for the MaxEnt tokenizer to accurately recognize the token boundary.
     *
     * @param text
     * @param splitTokens
     * @return
     */
    public List<ContinuousSpan> tokenize(String text, char[] splitTokens) {
        List<ContinuousSpan> candidates = tokenize(text);
        return splitSymbols(candidates, text, splitTokens);
    }

    /**
     * Calls the Stanford tokenize method and then splits token candidates at each
     * of the index locations provided in the splitSymbolIndexes parameter. The
     * reason for this method is that a separate process ran previous to the
     * tokenizer could have generated a list of split locations (e.g., a
     * symbol disambiguation module). This method is a convenient way to
     * incorporate that information into tokenizing. This is valuable
     * because some symbols do not occur frequently enough in training
     * corpora for Stanford to accurately identify them as a token boundary.
     *
     * @param text               The original text to tokenize.
     * @param splitSymbolIndexes An int[] array of indexes that should be split as single-character tokens.
     * @return List of ContinuousSpan instances marking tokens.
     */
    public List<ContinuousSpan> tokenize(String text, List<Integer> splitSymbolIndexes) {
        List<ContinuousSpan> candidates = tokenize(text);
        return splitIndexes(candidates, splitSymbolIndexes);
    }

    /**
     * This method is a way to split on uncommon symbols that should indicate a token boundary.
     * Because many of the corpora did not have enough annotations for some symbols (e.g., "|"),
     * this is used to force a split when there is insufficient counts for models to identify
     * the token boundary.
     *
     * @param candidates A List of ContinuousSpans produced from the tokenize method.
     * @param text       The original text being tokenized.
     * @return A ContinuousSpan array.
     */
    private List<ContinuousSpan> splitSymbols(List<ContinuousSpan> candidates, String text, char[] splitCharacters) {
        List<ContinuousSpan> spanList = new ArrayList<>();

        for (ContinuousSpan span : candidates) {
            String coveredText = text.substring(span.getStart(), span.getEnd());

            List<Integer> splitIndexes = new ArrayList<>();
            for (char splitChar : splitCharacters) {
                for (int index = coveredText.indexOf(splitChar);
                     index >= 0;
                     index = coveredText.indexOf(splitChar, index + 1)) {
                    splitIndexes.add(span.getStart() + index);
                }
            }

            if (splitIndexes.size() > 0) {
                List<ContinuousSpan> currentCandidate = new ArrayList<>();
                currentCandidate.add(span);
                List<ContinuousSpan> splitSpans = splitIndexes(currentCandidate, splitIndexes);
                spanList.addAll(splitSpans);
            } else {
                spanList.add(span);
            }
        }
        return spanList;
    }

    /**
     * This method is a way to identify uncommon symbols that should indicate a token boundary.
     * Because many of the corpora did not have enough annotations for some symbols (e.g., "|"),
     * this is used to force a split when there is insufficient counts for Stanford to identify
     * the token boundary.
     *
     * @param candidates   An array of ContinuousSpans produced from the Stanford tokenizer.
     * @param splitIndexes A List of integers that identify the indexes the tokenizer should identify as separate tokens.
     * @return A ContinuousSpan array.
     */
    private List<ContinuousSpan> splitIndexes(List<ContinuousSpan> candidates, List<Integer> splitIndexes) {
        Collections.sort(splitIndexes);
        List<ContinuousSpan> spanList = new ArrayList<>();
        for (ContinuousSpan span : candidates) {
            int begin = span.getStart();
            int end = span.getEnd();
            for (int i = begin; i < end; i++) {

                if (splitIndexes.contains(i)) {
                    // Add span from beginning, if not at beginning
                    if (i - begin > 0) spanList.add(new ContinuousSpan(begin, i));

                    // Add the single character at the target index.
                    // The assumption is that the index contains a symbol that is a token.
                    spanList.add(new ContinuousSpan(i, i + 1));
                    begin = i + 1;
                }
            }
            if (end - begin > 0) spanList.add(new ContinuousSpan(begin, end));
        }
        return spanList;
    }
}