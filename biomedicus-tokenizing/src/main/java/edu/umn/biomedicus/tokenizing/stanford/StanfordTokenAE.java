/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.umn.biomedicus.tokenizing.stanford;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.umn.biomedicus.core.featureranges.ContinuousSpan;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class StanfordTokenAE extends CasAnnotator_ImplBase {

    private static final Logger logger = UIMAFramework.getLogger(StanfordTokenAE.class);
    @NotNull
    TokenizerFactory<CoreLabel> tokenizerFactory;
    @NotNull
    List<edu.umn.biomedicus.core.utils.Span> results = new ArrayList<>();
    @NotNull
    private Type tokenType;
    @NotNull
    private char[] splitCharacters = "|".toCharArray();
    @NotNull
    StanfordTokenizer tokenizer;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
        tokenizer = new StanfordTokenizer();
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        logger.log(Level.FINEST, "Tokenizer processing new document.");
        @NotNull CAS sysCAS = CASUtil.getSystemView(aCAS);
        @NotNull String documentText = sysCAS.getDocumentText();

        for (ContinuousSpan tokenSpan : tokenizer.tokenize(documentText)) {
            int begin = tokenSpan.getStart();
            int end = tokenSpan.getEnd();

            if (!documentText.substring(begin, end).trim().equals("")) {
                AnnotationFS token = sysCAS.createAnnotation(tokenType, begin, end);
                sysCAS.addFsToIndexes(token);
            }
        }
        logger.log(Level.FINEST, "Document tokenizing complete.");
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {
        super.typeSystemInit(typeSystem);
        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        logger.log(Level.FINEST, "StanfordTokenAE typeSystem initialization successful.");
    }
}
