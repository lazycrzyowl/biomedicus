/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.tokenizing.services;

import com.ibm.icu.text.BreakIterator;
import com.ibm.icu.text.RuleBasedBreakIterator;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.Token;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class BreakIteratorTokenizingServiceProvider
        implements TokenizingServiceProvider, SharedResourceObject {

    @NotNull
    RuleBasedBreakIterator breakIterator;
    @NotNull
    List<Span> results = new ArrayList<>();

    @Override
    public void load(DataResource dataResource) throws ResourceInitializationException
    {
        try (InputStream inputStream = dataResource.getInputStream())
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String rules = FileUtil.loadTextFile(reader);
            breakIterator = new RuleBasedBreakIterator(rules);
        }
        catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    @Override
    public BreakIteratorTokenizingServiceProvider analyze(String documentText)
    {
        runAnalysis(documentText, null);
        return this;
    }

    private List<Span> runAnalysis(String documentText, CAS view)
    {
        breakIterator.setText(documentText);

        int begin = breakIterator.first();
        int end = breakIterator.next();
        while (end != BreakIterator.DONE)
        {
            String token = documentText.substring(begin, end);
            if (!token.trim().equals(""))
            {
                Span span = new Span(begin, end);
                results.add(span);
                if (view != null)
                {
                    annotate(span, view);
                }
            }
            begin = end;
            end = breakIterator.next();
        }

        return results;
    }

    @Override
    public void annotateCAS(CAS view, String documentText)
    {
        runAnalysis(documentText, view);
    }

    private AnnotationFS annotate(Span span, CAS view)
    {
        AnnotationFS annot = AnnotationUtils.createAnnotation(view,
                Token.class, span.getStart(), span.getEnd());
        view.addFsToIndexes(annot);
        return annot;
    }

    @Override
    public AnnotationFS[] annotateCAS(CAS view)
    {
        List<AnnotationFS> annotationList = new ArrayList<>();
        for (Span span : results)
        {
            AnnotationFS annot = AnnotationUtils.createAnnotation(view, Token.class,
                    span.getStart(), span.getEnd());
            annotationList.add(annot);
        }
        AnnotationFS[] annotArray = new AnnotationFS[annotationList.size()];
        return annotationList.toArray(annotArray);
    }

    @Override
    public Span[] getSpans()
    {
        Span[] spanArray = new Span[results.size()];
        return results.toArray(spanArray);
    }
}
