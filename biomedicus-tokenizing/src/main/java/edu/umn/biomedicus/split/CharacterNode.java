/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.split;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class CharacterNode extends Node implements Comparable<CharacterNode> {

	private int character;

	public CharacterNode(int character) {
		this.character = character;
	}

	@Override
	public int compareTo(CharacterNode otherNode) {
		int otherCharacter = otherNode.character;
		return (character < otherCharacter ? -1 : (character == otherCharacter ? 0 : 1));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + character;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CharacterNode other = (CharacterNode) obj;
		if (character != other.character)
			return false;
		return true;
	}

	public int getCharacter() {
		return character;
	}

	public void setCharacter(int character) {
		this.character = character;
	}

	public String toString() {
		return "(" + (char) character + ")";
	}
}
