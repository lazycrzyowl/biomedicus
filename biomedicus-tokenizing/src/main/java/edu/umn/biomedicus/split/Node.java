/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.split;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.ArrayUtils;

import com.ibm.icu.text.UnicodeSet;

/**
 * This class provides the trie-like data structure used by the
 * {@link SplitMatcher} class to find word matches. It is a bit different than a
 * normal trie because it allows for two different kinds of nodes
 * {@link CharacterNode}s and {@link UnicodeSetNode}s. A more important
 * difference is that the graph may also have simple cycles (i.e. a node that
 * cycles back on itself - one of its children nodes are itself.) There is
 * nothing about this class per se that licenses these simple cycles - just be
 * aware that {@link SplitPatterns} will create such cycles.
 * <p>
 * The graph/tree/trie itself is pretty straightforward. A node can have two
 * kinds of children and a pattern id. A pattern id indicates that a node ends a
 * pattern for the given id. {@link SplitPatterns} manages the pattern ids.
 * Therefore, when the matcher has examined at the final character of an input
 * word and one of the nodes corresponding to one of its matches in its list of
 * working matches has a patternId (that is not -1) we know that the node
 * corresponds to the end of that pattern and we have a match.
 * <p>
 * There is one additional piece of book keeping information that is a bit more
 * confusing. The map 'splits' keeps track of the splits that occur between this
 * node and a given child node (the key of the map.) We keep track of the child
 * node the split occurs before because simply saying that a split occurs at a
 * given node does not allow for a split to occur before a node that cycles back
 * on itself (we do not want the split to be repeated.) For example, consider
 * the pattern "a|b+". If we stored the split with the "b" node, then the input
 * word "abbbbb" would have many splits. Instead we store the split with the "a"
 * node along with the child node that it occurs before, the "b" node. This
 * allows for "abbbb" to have a single split at index 1.
 * 
 * @author Philip Ogren
 * 
 */

public class Node {

	// we could alternately use a map here that would have character keys and
	// CharacterNodes for children. This is a common approach. However, it is
	// generally a very large waste of memory for a modest performance gain.
	// Instead we will keep this array sorted and perform binary search.
	// The number of character nodes is not likely to be large and so the binary
	// search should be very fast.
	protected CharacterNode[] characterChildren;
	protected UnicodeSetNode[] unicodeSetChildren;
	// the id of a pattern that ends on this node
	protected int patternId = -1;

	// the key is a child node, the value are the pattern ids that have a split
	// before that node
	protected Map<Node, List<Integer>> splits = new HashMap<Node, List<Integer>>();

	/**
	 * Only a single pattern can end at a given node.
	 * 
	 * @return the pattern id of the pattern (if any) that ends this node. If no
	 *         pattern ends at this node, then -1 is returned.
	 */
	public int getPatternId() {
		return patternId;
	}

	/**
	 * A factory method for cloning a node of either type CharacterNode or
	 * UnicodeSetNode.
	 * 
	 * @param node
	 * @return a cloned copy of the provided node.
	 */
	public static Node createNode(Node node) {
		if (node instanceof CharacterNode) {
			CharacterNode cNode = (CharacterNode) node;
			return new CharacterNode(cNode.getCharacter());
		} else {
			UnicodeSetNode uNode = (UnicodeSetNode) node;
			return new UnicodeSetNode(uNode.getUnicodeSet());

		}
	}

	/**
	 * @param id
	 */
	protected boolean setPatternId(int id) {
		if (patternId == -1) {
			patternId = id;
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Returns a list of all the pattern ids that have a split between this node
	 * and the given child node.
	 * 
	 * @param childNode
	 * @return the pattern ids that have a split between this node and the
	 *         provided child node.
	 */
	public List<Integer> getSplitIds(Node childNode) {
		if (splits.get(childNode) != null)
			return Collections.unmodifiableList(splits.get(childNode));
		return null;
	}

	/**
	 * adds a split between this node and the child node for the provided
	 * pattern id
	 * 
	 * @param childNode
	 * @param patternId
	 */
	public void addSplitId(Node childNode, int patternId) {
		List<Integer> childSplits = splits.get(childNode);
		if (childSplits == null) {
			childSplits = new ArrayList<Integer>();
			splits.put(childNode, childSplits);
		}
		if (!childSplits.contains(patternId))
			childSplits.add(patternId);
	}

	/**
	 * Returns all children of this node for the given character. The returned
	 * list will contain 0 or 1 CharacterNode and 0 or more UnicodeSetNodes
	 * 
	 * @param character
	 * @return all the children nodes for the provided character.
	 */
	public List<Node> getChildren(int character) {
		List<Node> children = new ArrayList<Node>();
		CharacterNode characterChild = getCharacterChild(character);
		if (characterChild != null) {
			children.add(characterChild);
		}

		List<Node> unicodeSetChildren = getUnicodeSetChildren(character);
		children.addAll(unicodeSetChildren);
		return children;
	}

	/**
	 * Returns all children of this node that are UnicodeSetNodes. We must loop
	 * over all of the children of this node that are UnicodeSetNodes to
	 * determine if a character falls within the corresponding unicode set.
	 * 
	 * @param character
	 * @return the child nodes of type {@link UnicodeSetNode} for the given
	 *         character.
	 */
	private List<Node> getUnicodeSetChildren(int character) {
		List<Node> returnValues = new ArrayList<Node>();
		if (unicodeSetChildren != null) {
			for (UnicodeSetNode unicodeSetNode : unicodeSetChildren) {
				if (unicodeSetNode.contains(character)) {
					returnValues.add(unicodeSetNode);
				}
			}
		}
		return returnValues;
	}

	/**
	 * Returns all children of this node that are CharacterNodes. Here we
	 * perform a binary search to find the character node corresponding to the
	 * provided charater if it exists.
	 * 
	 * @param character
	 * @return a {@link CharacterNode} child node for the provided character.
	 */
	public CharacterNode getCharacterChild(int character) {
		if (characterChildren == null) {
			return null;
		}
		int index = Arrays.binarySearch(characterChildren, new CharacterNode(character));
		if (index >= 0) {
			return characterChildren[index];
		}
		return null;
	}

	/**
	 * This method is useful when you are building the graph.
	 * 
	 * @param unicodeSet
	 * @return a child of type {@link UnicodeSetNode} if this node has one with
	 *         the provided unicode set.
	 */
	public UnicodeSetNode getUnicodeSetChild(UnicodeSet unicodeSet) {
		if (unicodeSetChildren == null) {
			return null;
		}
		int index = Arrays.binarySearch(unicodeSetChildren, new UnicodeSetNode(unicodeSet));
		if (index >= 0) {
			return unicodeSetChildren[index];
		}
		return null;
	}

	/**
	 * adds a child node to this node.
	 * 
	 * @param childNode
	 */
	public void add(Node childNode) {
		if (childNode instanceof CharacterNode) {
			add((CharacterNode) childNode);
		} else {
			add((UnicodeSetNode) childNode);
		}
	}

	public void add(CharacterNode childNode) {
		if (characterChildren == null) {
			characterChildren = new CharacterNode[] { childNode };
		}
		int insertionIndex = Arrays.binarySearch(characterChildren, childNode);
		if (insertionIndex < 0) {
			insertionIndex = -(insertionIndex + 1);
			characterChildren = (CharacterNode[]) ArrayUtils.add(characterChildren, insertionIndex, childNode);
		}
	}

	public void add(UnicodeSetNode childNode) {
		if (unicodeSetChildren == null) {
			unicodeSetChildren = new UnicodeSetNode[] { childNode };
		}

		int insertionIndex = Arrays.binarySearch(unicodeSetChildren, childNode);
		if (insertionIndex < 0) {
			insertionIndex = -(insertionIndex + 1);
			unicodeSetChildren = (UnicodeSetNode[]) ArrayUtils.add(unicodeSetChildren, childNode);
		}
	}

	public static void print(PrintStream out, Node node) {
		print(out, node, 0);
	}

	// TODO need to avoid infinite loop if trie is not acyclic
	private static void print(PrintStream out, Node node, int i) {
		out.print(tabs(i));
		if (node instanceof CharacterNode) {
			out.print((char) ((CharacterNode) node).getCharacter());
		}
		if (node instanceof UnicodeSetNode) {
			out.print(((UnicodeSetNode) node).getUnicodeSet().toString());
		}

		String match = "";
		if (node.getPatternId() != -1) {
			match = "patternId = " + node.getPatternId();
		}

		String splitString = "splitIds = " + createSplitString(node);

		out.println("\tsplits:" + splitString + "   matches:" + match);

		if (node.characterChildren != null) {
			for (Node childNode : node.characterChildren) {
				if (childNode == node) {
					out.println(tabs(i + 1) + "cycle");
				} else {
					print(out, childNode, i + 1);
				}
			}
		}
		if (node.unicodeSetChildren != null) {
			for (Node childNode : node.unicodeSetChildren) {
				print(out, childNode, i + 1);
			}
		}

	}

	private static String createSplitString(Node node) {
		StringBuilder sb = new StringBuilder();
		for (Entry<Node, List<Integer>> entry : node.splits.entrySet()) {
			sb.append(entry.getKey().toString() + entry.getValue() + "\t");
		}
		return sb.toString();
	}

	private static String tabs(int numberOfTabs) {
		char[] chars = new char[numberOfTabs];
		Arrays.fill(chars, ' ');
		return new String(chars);
	}

}