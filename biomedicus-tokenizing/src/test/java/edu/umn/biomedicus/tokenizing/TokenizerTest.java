package edu.umn.biomedicus.tokenizing;

import edu.umn.biomedicus.tokenizing.services.OpenNLPTokenizingServiceProvider;
import edu.umn.biomedicus.tokenizing.services.WhitespaceAndSymbolTokenizingServiceProvider;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple Tokenizer.
 */
public class TokenizerTest
{
    String test = "Tokenizing text requires that acronyms/abbreviations are kept together; however, " +
            "other punctuation should be segmented, such as the period at the end of this sentence. " +
            "This is a punctuation-rich test about maybe a Dr. with a Ph.D. weighing 180lbs that took cipro 100mg b.i.d. " +
            "Something difficult to test (evaluate) is parentheticals and odd|separations.";


    @Test
    public void testOpenNLPTokenizer() throws Exception
    {
        String[] expectedResults = {"Tokenizing", "text", "requires", "that", "acronyms/abbreviations",
                "are", "kept", "together", ";", "however", ",", "other", "punctuation", "should", "be", "segmented",
                ",", "such", "as", "the", "period", "at", "the", "end", "of", "this", "sentence", ".", "This", "is",
                "a", "punctuation-rich", "test", "about", "maybe", "a", "Dr.", "with", "a", "Ph.D.", "weighing",
                "180lbs", "that", "took", "cipro", "100mg", "b.i.d", ".", "Something", "difficult", "to", "test",
                "(", "evaluate", ")", "is", "parentheticals", "and", "odd", "|", "separations", "."};

        Tokenizer tokenizer = new Tokenizer.Builder()
                .service(OpenNLPTokenizingServiceProvider.class)
                .model("edu/umn/biomedicus/tokenizing/en-token.bin")
                .build();

        String [] results = tokenizer.process(test);
        assertTrue(match(results, expectedResults));
    }

    private boolean match(String[] results, String[] expectedResults)
    {
        assertTrue(results.length == expectedResults.length);
        for (int i = 0; i < results.length; i++)
        {
            if (!results[i].equals(expectedResults[i])) return false;
        }
        return true;
    }
}
