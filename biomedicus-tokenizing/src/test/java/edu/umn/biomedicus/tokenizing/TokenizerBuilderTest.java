/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.tokenizing;

import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.tokenizing.services.OpenNLPTokenizingServiceProvider;
import edu.umn.biomedicus.tokenizing.services.StanfordTokenizingServiceProvider;
import edu.umn.biomedicus.tokenizing.services.WhitespaceAndSymbolTokenizingServiceProvider;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple Tokenizer.
 */
public class TokenizerBuilderTest {

    CAS aCAS;
    CAS system;

    @Before
    public void setup() throws Exception
    {
        Tokenizer tokenizer = new Tokenizer.Builder().build();
        aCAS = tokenizer.newCAS();
        system = CASUtil.getSystemView(aCAS);
    }

    @Test
    public void testWhitespaceTokenizerBuilder() throws Exception
    {
        Tokenizer tokenizer = new Tokenizer.Builder()
                .service(WhitespaceAndSymbolTokenizingServiceProvider.class)
                .build();

        assertNotNull(tokenizer);
        aCAS.reset();
        system = CASUtil.getSystemView(aCAS);
        system.setDocumentText("This is a test.");
        tokenizer.process(aCAS);
        AnnotationIndex tokenList = AnnotationUtils.getAnnotations(system, Token.class);
        assertTrue(tokenList.size() == 5);
    }

    @Test
    public void testOpenNLPTokenizerBuilder() throws Exception{

        String serviceProviderClassName = OpenNLPTokenizingServiceProvider.class.getCanonicalName();

        ServiceProviderDescription tokenizingService = new ServiceProviderDescription(
                "tokenizingServiceProvider").serviceProvider(serviceProviderClassName)
                .modelFile("edu/umn/biomedicus/tokenizing/en-token.bin");

        Tokenizer tokenizer = new Tokenizer.Builder()
                .service(OpenNLPTokenizingServiceProvider.class)
                .model("edu/umn/biomedicus/tokenizing/en-token.bin")
                .build();

        assertNotNull(tokenizer);
        aCAS.reset();
        system = CASUtil.getSystemView(aCAS);
        system.setDocumentText("This is a test.");
        tokenizer.process(aCAS);
        AnnotationIndex tokenList = AnnotationUtils.getAnnotations(system, Token.class);
        assertTrue(tokenList.size() == 5);
    }

    @Test
    public void testStanfordTokenizerBuilder() throws Exception{
        Tokenizer tokenizer = new Tokenizer.Builder()
                .service(StanfordTokenizingServiceProvider.class)
                .build();

        assertNotNull(tokenizer);
        aCAS.reset();
        system = CASUtil.getSystemView(aCAS);
        system.setDocumentText("This is a test.");
        ArrayList<String> resultList = new ArrayList<>();
        tokenizer.process(aCAS);
        for (AnnotationFS annot : AnnotationUtils.getAnnotations(system, Token.class))
        resultList.add(annot.getCoveredText());
        AnnotationIndex tokenList = AnnotationUtils.getAnnotations(system, Token.class);
        assertTrue(tokenList.size() == 5);
    }
}
