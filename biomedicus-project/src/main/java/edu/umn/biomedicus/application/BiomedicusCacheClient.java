package edu.umn.biomedicus.application;

import org.infinispan.Cache;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BiomedicusCacheClient extends AbstractCacheNode {

    private Cache<String, String> cache;

    private final static Logger logger = Logger.getLogger(BiomedicusCacheServer.class.getName());

    public BiomedicusCacheClient() throws IOException {
        logger.log(Level.INFO, "Initializing infinispan cache node.");
    }

    public void run() {
        // TODO: change code below to use gossipproxy or tcp client/server (hotrod) mode.
        this.cache = getCacheManager().getCache();
        waitForClusterToForm();

        logger.log(Level.INFO, "About to put key, value into cache on node " + getNodeId());
        // Put some information in the cache that we can display on the other node
        cache.put("key", "BiomedicusCacheClientValue");
        logger.log(Level.INFO, "Successfully added key/value pair to cache: " + cache.get("key"));
    }

    public String get(String key) {
        return cache.get(key);
    }

    @Override
    protected int getNodeId() {
        return 1;
    }

}