package edu.umn.biomedicus.application;

import edu.umn.biomedicus.pipeline.AgentPipeline;
import edu.umn.biomedicus.pipeline.ProcessCallbackListener;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * A simple application for testing the feasibility of connecting the AHC datacenter to MSI computational resources.
 * <p/>
 * This is an initial test. No significant processing takes place.
 */
public class BiomedicusClient {

    //TODO: Create a sample clinical document for here instead of short text.
    public static final String SAMPLE_DOCUMENT_TEXT = "some input text left to process";

    public static void main(String[] args) throws Exception {
        Properties prop = BiomedicusClient.getProperties();
        Logger logger = UIMAFramework.getLogger();
        logger.log(Level.INFO, "Initializing AHC2MSI test #1");

      /* Find out what Java properties must be set to
         create core/head dumps. Terminate if those properties
         allow dump files. Note: I don't know if this is possible.
         Investigate this to ensure no dump files are left behind.
      */
        // TODO: Find if there's a way to identify java properties related
        // to core/head dump to prevent running when a dump is enabled.
        //
        // possible option is `ulimit  -c -H 0`
        //           it might also need to use -Xrs option with java command
        // possible other option is `-XX:HeapDumpPath=/dev/null`


      /* Initialize an SSH connection and forward ports */
        AHCConnector tunnel = new AHCConnector(prop.getProperty("rhost"),
                prop.getProperty("username"), prop.getProperty("password"), logger);

      /* Connect to cache service running at AHC */
        // TODO: gossipproxy or tcp cache client because UDP is trouble over tunnel.
        // This is still a problem. It's using UDP. Need to find time to
        // set up gossipproxy, or to use client/server connection over tcp
//        BiomedicusCacheClient cache = new BiomedicusCacheClient();
//        cache.run();
//        String cacheTest = cache.get("CacheValidation");
//        if (cacheTest.equals("Valid cache results")) {
//            logger.log(Level.INFO, "Successful communication with cache if ");
//        }

      /* The analysis engine for processing text */
        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
        UimaAsBaseCallbackListener asyncListener = new ProcessCallbackListener(uimaAsEngine);//, tunnel);

      /* Constructs a class to create and run a UIMA pipeline */
        AgentPipeline uimaPipeline = new AgentPipeline(asyncListener, uimaAsEngine);//, tunnel);

      /* Run the sample document through the pipeline */
        System.out.println("Ready to process document...");
        // TODO: loop through the sample clinical document a few thousand times to get more useful info.
        //uimaPipeline.process(SAMPLE_DOCUMENT_TEXT);

      /* Check to make certain the callback listener closed the tunnel */
        int maxWait = 4;
        int curWait = 0;
        while (tunnel.isConnected()) {
            curWait++;
            if (curWait > maxWait) {
                //tunnel.disconnect(); // This will break any files currently within the pipeline.
            }
        }
        //cache.detach();

      /* Flush memory so subsequent users can not read anything
         that appears in a core dump.
       */
        // TODO: Investigate flushing memory with Java so subsequent users can not read anything in a core dump.
    }

    public static Properties getProperties() throws Exception {
        Properties prop = new Properties();
        String dir = System.getProperty("user.home");
        InputStream in = new FileInputStream(dir + "/.service.properties");
        prop.load(in);
        in.close();
        return prop;
    }
}