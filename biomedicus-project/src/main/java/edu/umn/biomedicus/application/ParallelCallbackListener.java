package edu.umn.biomedicus.application;

import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.EntityProcessStatus;

public class ParallelCallbackListener extends UimaAsBaseCallbackListener
{
    @Override
    public void entityProcessComplete(CAS output, EntityProcessStatus aStatus) {
        // confirm that the expected annotations were added to the CAS
        FSIterator<AnnotationFS> annotationsIterator = output.getAnnotationIndex().iterator();
        while (annotationsIterator.hasNext()) {
            AnnotationFS annotation = (AnnotationFS) annotationsIterator.next();
            System.out.println("  Found: " + annotation.getClass().getName());
        }
    }
}


