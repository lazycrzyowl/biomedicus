package edu.umn.biomedicus.application;

import org.infinispan.Cache;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BiomedicusCacheServer extends AbstractCacheNode {

    private final static Logger logger = Logger.getLogger(BiomedicusCacheServer.class.getName());

    public BiomedicusCacheServer() throws IOException {
        logger.log(Level.INFO, "Initializing the infinispan cache node");
    }

    public void run() {
        // TODO: change code below to use gossipproxy or tcp client/server (hotrod) mode.
        Cache<String, String> cache = getCacheManager().getCache();
        cache.put("CacheValidation", "Valid cache results");
        logger.log(Level.INFO, "Entered CacheValidation key in cache on node " + getNodeId());
    }

    @Override
    protected int getNodeId() {
        return 1;
    }

    public static void main(String[] args) throws IOException {
        BiomedicusCacheServer cacheServer = new BiomedicusCacheServer();
        cacheServer.run();
    }
}