package edu.umn.biomedicus.application;

import edu.umn.biomedicus.pipeline.Pipeline;
import edu.umn.biomedicus.pipeline.ProcessCallbackListener;
import org.apache.commons.cli.*;
import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.util.FileUtils;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;


public class ParallelApplication
{
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(SerialApplication.class);
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static Path projectDirectory;
    @NotNull
    private static String suffix;
    @NotNull
    private BaseUIMAAsynchronousEngine_impl uimaAsEngine;

    public ParallelApplication(Path configDirectory,
                               Path outputDirectory,
                               Iterator<Path> files)
            throws Exception
    {


        // creating UIMA analysis engine
        uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();

        deploy(uimaAsEngine, configDirectory);


        // add callback listener that will be informed when processing completes
        ProcessCallbackListener callback = new ProcessCallbackListener(uimaAsEngine);
//        uimaAsEngine.addStatusCallbackListener(callback);




        // Get pipeline
        Pipeline uimaPipeline = new Pipeline(callback, uimaAsEngine);
        double start = System.currentTimeMillis();
        int documentCount = 0;

        while (files.hasNext())
        {
            documentCount++;
            Path currentFile = files.next();
            String documentText = FileUtils.file2String(currentFile.toFile());

            // skip empty documents
            if (documentText == null || documentText.trim() == "")
            {
                continue;
            }

            // run the sample document through the pipeline
            long before = System.currentTimeMillis();
            CAS output = process(documentText);

            long after = System.currentTimeMillis();
            CAS metadata = output.getView("MetaData");
            String outputName = new File(metadata.getDocumentText()).getName();
            File dest = new File(outputDirectory.toString(), outputName);
            try
            {
                FileOutputStream os = new FileOutputStream(dest);
                XmiCasSerializer.serialize(output, os);
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            } catch (SAXException e)
            {
                e.printStackTrace();
            }
        }

        double end = System.currentTimeMillis();
        double totalTime = (end - start)/1000;
        double timePerDocument = totalTime/Double.valueOf(documentCount);
        System.out.println("Total number of documents = " + documentCount);
        System.out.println("Total processing time = " + totalTime);
        System.out.println("Processing time per document = " + timePerDocument);

    }

    private void deploy(BaseUIMAAsynchronousEngine_impl uimaAsEngine, Path configDir) throws Exception
    {
        // preparing map for use in deploying services
        Map<String,Object> deployCtx = new HashMap<String,Object>();
        deployCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath, System.getenv("UIMA_HOME") + "/bin/dd2spring.xsl");
        deployCtx.put(UimaAsynchronousEngine.SaxonClasspath, "file:" + System.getenv("UIMA_HOME") + "/saxon/saxon8.jar");

        System.out.println("Deploying UIMA services");

        Path tokenDeploy = Paths.get(configDir.toString(), "descriptors/deployment/tokenAEDeploy.xml");
        Path documentDeploy = Paths.get(configDir.toString(), "descriptors/deployment/documentAEDeploy.xml");
        Path sentenceDeploy = Paths.get(configDir.toString(), "descriptors/deployment/sentenceAEDeploy.xml");
        Path posDeploy = Paths.get(configDir.toString(), "descriptors/deployment/posAEDeploy.xml");
        Path chunkDeploy = Paths.get(configDir.toString(), "descriptors/deployment/chunkAEDeploy.xml");

        uimaAsEngine.deploy(documentDeploy.toString(), deployCtx);
        uimaAsEngine.deploy(tokenDeploy.toString(), deployCtx);
        uimaAsEngine.deploy(sentenceDeploy.toString(), deployCtx);
        uimaAsEngine.deploy(posDeploy.toString(), deployCtx);
        uimaAsEngine.deploy(chunkDeploy.toString(), deployCtx);

        // creating aggregate analysis engine
        System.out.println("Deploying analysis engine");
        uimaAsEngine.deploy("./conf/deploy.xml", deployCtx);

        // preparing map for use in a UIMA client for submitting text to process
        System.out.println("Initialising UIMA client");
        deployCtx.put(UimaAsynchronousEngine.ServerUri, "tcp://localhost:61616");
        deployCtx.put(UimaAsynchronousEngine.Endpoint,  "BiomedicusAnnotatorQueue");
        uimaAsEngine.initialize(deployCtx);
    }

    public static void main(String[] args) throws Exception
    {
        // Get general properties
        Configuration _config = new Configuration();
        System.out.println(_config.getString("biomedicus_cli_banner"));

        // Start logging
        logger.setLevel(Level.OFF);

        // Construction commandline options, get values, and validate what was supplied
        Options cliOptions = getCLIOptions();
        CommandLine cliValues = getCLIValues(cliOptions, args);
        String suffix = ".txt";

        if (cliValues.hasOption("projectdir"))
        {
            // set path for input and output

            inputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"), "input");
            outputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"), "output");
            projectDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"));

            // get suffix, or set to default (.txt)
            String suffix_opt = cliValues.getOptionValue("suffix");
            if (suffix_opt != null && suffix_opt != "")
            {
                suffix = suffix_opt;
            }

            // display provided options
            System.out.println("Setting project directory to: " + projectDirectory.toString());
            System.out.println("Setting input directory to: " + inputDirectory.toString());
            System.out.println("Setting destination directory to: " + outputDirectory.toString());
            System.out.println("Setting input file suffix to: " + suffix);

        } else
        {
            printHelp("BioMedICUS Project", cliOptions);
        }

        // Create a DirectoryStream which accepts only filenames ending with specified suffix
        DirectoryStream<Path> ds = Files.newDirectoryStream(inputDirectory, "*" + suffix);

        // constructs a class to create and run a UIMA pipeline
        String od = _config.getString("output_dir");

        // run the sample document through the pipeline
        ParallelApplication app = new ParallelApplication(projectDirectory, outputDirectory, ds.iterator());


    }


    /**
     * Uses the UIMA analysis engine to process the provided document text.
     */
    public CAS process(String text) throws CASException, Exception{
        CAS cas = uimaAsEngine.getCAS();
        cas.setDocumentText(text);
        uimaAsEngine.sendCAS(cas);
        return cas;
    }

    private static Options getCLIOptions()
    {
        // create the Options
        Options options = new Options();
        options.addOption("p", "projectdir", true, "Location of project directory.");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args)
    {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try
        {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e)
        {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }
}

