package edu.umn.biomedicus.pipeline;

import edu.umn.biomedicus.application.AHCConnector;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.EntityProcessStatus;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import java.util.List;

public class ProcessCallbackListener extends UimaAsBaseCallbackListener {

    long before = -1;

    AHCConnector tunnel = null;

    UimaAsynchronousEngine uimaAsEngine = null;

    // prepares a listener for when the analysis engine is complete
    public ProcessCallbackListener(UimaAsynchronousEngine uimaAsEngine, AHCConnector tunnel) {
        this.before = System.currentTimeMillis();
        this.tunnel = tunnel;
        this.uimaAsEngine = uimaAsEngine;
    }

    // prepares a listener for when the analysis engine is complete
    public ProcessCallbackListener(UimaAsynchronousEngine uimaAsEngine) {
        this.before = System.currentTimeMillis();
        this.uimaAsEngine = uimaAsEngine;
    }


    /**
     * This will be called once the text is processed.
     */
    @Override
    public void entityProcessComplete(CAS output, EntityProcessStatus aStatus) {
        // Note: We would normally add the output CAS to
        //  lucene indexes and then send to long-term storage.
        //  However, we're just printing out for now.
        //  Probably even the printing will be stopped to get
        //  timings for the final tests.
        // record the time that this was complete
        long after = System.currentTimeMillis();

        // check exceptions
        if (aStatus != null && aStatus.isException()) {
            List<Exception> exceptions = aStatus.getExceptions();
            for (Exception exception : exceptions) exception.printStackTrace();
            shutdown();
            return;
        }

        // display the time spent processing the text
        System.out.println("Time spent in pipeline: " + (after - before));

        // confirm that the expected annotations were added to the CAS
        System.out.println("Confirming what was added...");
        for (AnnotationFS annotation : output.getAnnotationIndex()) {
            System.out.println("  Found: " + annotation.getClass().getName() + ", "
                    + annotation.getType().toString() + ", " + annotation.getCoveredText() + ", ");
        }

        JCas jcas = null;
        try {
            jcas = output.getJCas();
        } catch (CASException e) {
            e.printStackTrace();
        }
        assert jcas != null;
        for (Annotation annotation : jcas.getAnnotationIndex(Token.type)) {
            Token tok = (Token) annotation;
            System.out.println("Original: " + tok.getCoveredText() + ", Normalized: " + tok.getNorm());
        }

        shutdown();
    }

    public void shutdown() {
        try {
            uimaAsEngine.stop();
            if (tunnel != null) tunnel.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
