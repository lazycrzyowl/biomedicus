package edu.umn.biomedicus.model.services;

import au.com.bytecode.opencsv.CSVReader;
import edu.umn.biomedicus.core.utils.MetaMapAdapter;
import edu.umn.biomedicus.core.utils.Span;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * The MetaMapChunkingServiceProvider provides chunk/phrase/shallowParse information for a sentence
 * using an instance of the MetaMap application that is installed on the local machine. MetaMap
 * is started with the "--phrases_only" option, and the phrase information is converted into
 * a List of Span annotations that denote the identified phrases/chunks.
 */
public class MaxentModelServiceProvider
        implements ModelServiceProvider, SharedResourceObject {

    List<Span> results = new ArrayList<>();
    Span[] resultSpans;

    @NotNull
    String metamapHome;

    /**
     * Service method that returns chunks for a sentence by using
     * MetaMap's phrases-only option.
     *
     * @param sentenceAsTokens
     * The sentence formatted as a String[] of tokens.
     * @param tags
     * The part-of-speech tags for each of the tokens.
     * @return List
     * A list of token spans that denote the location of the identified chunks.
     * @throws Exception
     */
    @Override
    public Span[] analyze(String sentenceText, String[] sentenceAsTokens,
                                                  Span[] tokensAsSpans, String[] tags)
            throws InterruptedException
    {
        List<String[]> entries = null;
        try
        {
            MetaMapAdapter mm = new MetaMapAdapter("--phrases_only");
            String rawResults = mm.getAnalysisString(sentenceText);
            entries = convertRawDataToPhraseData(rawResults);
        }
        catch (IOException e)
        {
            throw new InterruptedException("The MetaMap subprocesses failed. Is 'metamap.home' set?"
            + e.getStackTrace());
        }

        List<Span> results = findBoundaries(sentenceText, tokensAsSpans, entries);
        resultSpans = new Span[results.size()];
        return results.toArray(resultSpans);
    }

    public Span[] getSpans()
    {
        resultSpans = new Span[results.size()];
        return results.toArray(resultSpans);
    }

    /**
     * Method to convert the string results received from MetaMap (raw results) into
     * structured data that can be used to mark chunk boundaries.
     *
     * @param rawResults
     * The string results returned from metamap running with the "--phrases_only" option.
     * @return
     * A List of String arrays that are the phrases identified by metamap.
     *
     * @throws java.io.IOException
     */
    private List<String[]> convertRawDataToPhraseData(String rawResults) throws IOException
    {
        ArrayList<String[]> results = new ArrayList<>();
        String[] rawRows = rawResults.split("\n");
        String[] tokenRows = new String[rawRows.length];
        for (int i = 0; i < rawRows.length; i++)
        {
            // Divide fields returned by metamap that are separated by a pipe ("|")
            String[] fields = rawRows[i].split("\\|");

            // Remove brackets
            String source = fields[5].trim().replace('[', '"').replace(']', '"');
            String oldRow = source.substring(1, source.length() - 1);

            // Create intermediateRow from semi-cleaned data string
            String[] intermediateRow = new CSVReader(new StringReader(oldRow)).readAll().get(0);

            // Add intermediateRow members into final data (newRow)
            ArrayList<String> newRow = new ArrayList<>();
            int singleQuoteCount = 0;
            for (String element : intermediateRow)
            {
                if (element.length() == 1 && element.equals("'"))
                {
                    singleQuoteCount++;
                    if (singleQuoteCount == 2)
                    {
                        newRow.add(",");
                        singleQuoteCount = 0;
                    }
                } else {
                    if (element.startsWith("'"))
                    {
                        element = element.substring(1);
                    }
                    if (element.endsWith("'"))
                    {
                        element = element.substring(0, element.length() - 1);
                    }
                    newRow.add(element);
                }
            }
            results.add(newRow.toArray(new String[newRow.size()]));
        }
        return results;
    }


    /**
     * The findBoundaries method scans the row data turn groups of tokens into
     * Span annotations that mark the begin/end points of chunks identified by metamap.
     *
     * @param sentenceText
     * The sentence as a String.
     * @param chunks
     * The sentence as a List of String arrays where each array constitutes a chunk.
     * @return
     * Returns a List of Spans that identify the begin-end points of each identified chunk.
     */
    private List<Span> findBoundaries(String sentenceText, Span[] tokenSpans, List<String[]> chunks)
    {
        ArrayList<Span> results = new ArrayList<>(chunks.size());
        int currentIndex = 0;
        int offset = tokenSpans[0].getStart();

        int sentenceEnd = tokenSpans[tokenSpans.length - 1].getEnd();
        for (String[] chunkTokens : chunks)
        {
            int size = chunkTokens.length;
            int tokenStart = sentenceText.indexOf(chunkTokens[0], currentIndex);
            int begin = tokenStart + offset;
            int end = 0;
            if (chunkTokens.length == 1)
            {
                end = begin + chunkTokens[0].length();
            }
            else
            {
                String lastToken = chunkTokens[size - 1];
                int currentPointer = sentenceText.indexOf(lastToken, currentIndex);
                end = currentPointer + lastToken.length() + offset;
            }

            Span chunk = new Span(begin, end);
            results.add(chunk);
            currentIndex = end - offset;
        }
        return results;
    }

    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {
        metamapHome = System.getProperty("metamap.path");
        if (metamapHome == null)
        {
            StringBuilder sb = new StringBuilder("\n\n\tThe 'metamap.path' property is not set.");
            sb.append("Set metamap.path (e.g., -Dmetamap.path=/path/to/metamap13) ");
            sb.append("and make sure the skrmedpostctl and wsdserverctl are started.\n\n");
            Exception e = new Exception(sb.toString());
            throw new ResourceInitializationException(e);
        }
    }

}
