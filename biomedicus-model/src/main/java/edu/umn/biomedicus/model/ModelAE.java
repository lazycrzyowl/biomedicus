package edu.umn.biomedicus.model;

import edu.umn.biomedicus.chunking.services.ChunkingServiceProvider;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.model.featureextractors.FeatureExtractor;
import edu.umn.biomedicus.model.features.FeatureSet;
import edu.umn.biomedicus.model.rangegenerators.FeatureRangeGenerator;
import edu.umn.biomedicus.core.featureranges.FeatureRange;
import edu.umn.biomedicus.model.services.ModelServiceProvider;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * A shallow parser that uses the openNLP maximum entropy chunker to identify annotations
 */
public class ModelAE extends CasAnnotator_ImplBase
{
    private static final String MODEL_SERVICE_PROVIDER_KEY = "CHUNKING_SERVICE";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(ModelAE.class);


    @NotNull
    private ModelServiceProvider model;
    @NotNull
    private FeatureRangeGenerator generator;
    @NotNull
    private FeatureExtractor featureExtractor;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {

        super.initialize(context);
        try {
            model = (ModelServiceProvider) context.getResourceObject(MODEL_SERVICE_PROVIDER_KEY);
        } catch (ResourceAccessException e) {
            throw new ResourceInitializationException(e);
        }

    }

    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = aCAS.getView("System");

        for (FeatureRange featureRange : generator) {

            FeatureSet featureSet = featureExtractor.extract(featureRange);
            String sentenceText = sentenceAnnotation.getCoveredText();
            int sentenceStart = sentenceAnnotation.getBegin();
            edu.umn.biomedicus.core.utils.Span[] chunks = null;

            try {
                chunks = chunker.analyze(
                        sentenceText,
                        sentence.getCoveredTokensAsStringArray(),
                        sentence.getCoveredTokensAsSpans(),
                        sentence.getPOSTagsOfCoveredTokens());
            } catch (Exception e) {
                throw new AnalysisEngineProcessException(e);
            }

            for (edu.umn.biomedicus.core.utils.Span chunk : chunks) {
                int begin = chunk.getStart();
                int end = chunk.getEnd();
                String coveredText = sysCAS.getDocumentText().substring(begin, end);
                AnnotationFS chunkAnnotation = sysCAS.createAnnotation(chunkType, begin, end);
                sysCAS.addFsToIndexes(chunkAnnotation);
            }
        }
    }

    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

        // Initialize types
        super.typeSystemInit(typeSystem);

        tokenType = typeSystem.getType(TypeUtil.TOKEN_TYPE_NAME);
        tokenStopwordFeature = tokenType.getFeatureByBaseName("isStopword");
        chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
        sentenceType = typeSystem.getType(TypeUtil.SENTENCE_TYPE_NAME);
        clinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
        logger.log(Level.FINEST, "Chunker type system initialized.");

    }

}

