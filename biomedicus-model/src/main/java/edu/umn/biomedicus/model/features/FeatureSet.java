package edu.umn.biomedicus.model.features;

public interface FeatureSet {
    public String[] getFeatures();
}
