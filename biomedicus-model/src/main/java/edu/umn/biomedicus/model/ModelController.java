/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.chunking;

import edu.umn.biomedicus.chunking.services.OpenNLPChunkingServiceProvider;
import edu.umn.biomedicus.core.ResourceFactory;
import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.segmenting.services.StanfordSegmentingServiceProvider;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import edu.umn.biomedicus.tokenizing.services.StanfordTokenizingServiceProvider;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class Chunker
{

    @NotNull
    protected AnalysisEngine ae;

    private Chunker(Builder builder) throws ResourceInitializationException {
        ae = builder.getAE();
    }

    public static void main(String[] args) throws Exception {
        String test = "This is a new test of chunking. It is usually easy. However, when a lot of punctuation such as the" +
                " Dr. with a Ph.D. prescribed neurontin 100mg b.i.d., then it gets harder.";

        // Get Tokenizer
        String tokenizingServiceProviderName = StanfordTokenizingServiceProvider.class.getCanonicalName();
        ServiceProviderDescription tokenizingServiceProvider =
                new ServiceProviderDescription("tokenizingServiceProvider")
                        .serviceProvider(tokenizingServiceProviderName);
        Tokenizer tokenizer = new Tokenizer.Builder().addService(tokenizingServiceProvider).build();

        ServiceProviderDescription segmenter = new ServiceProviderDescription("sentenceSegmentingServiceProvider")
                .serviceProvider(StanfordSegmentingServiceProvider.class.getCanonicalName());
        //                .serviceProvider(OpenNLPSegmentingServiceProvider.class.getCanonicalName())
        //                .modelFile("edu/umn/biomedicus/segmenting/sentenceME.bin");

        SentenceSegmenter sentSegmenter = new SentenceSegmenter.Builder()
                .addService(segmenter)
                .build();

        ServiceProviderDescription chunkerServiceProvider = new ServiceProviderDescription("chunkingServiceProvider")
                .serviceProvider(OpenNLPChunkingServiceProvider.class.getCanonicalName())
                .modelFile("edu/umn/biomedicus/chunking/en-chunker.bin");

        Chunker chunker = new Builder("edu/umn/biomedicus/chunking/modelAE.xml").build();
//                .addService(chunkerServiceProvider)
//                .build();


        CAS aCAS = chunker.newCAS();
        CAS view = aCAS.createView("System");
        view.setDocumentText(test);
        tokenizer.process(aCAS);
        sentSegmenter.process(aCAS);
        chunker.process(aCAS);

        for (AnnotationFS annot : view.getAnnotationIndex()) {
            Type annotationType = view.getTypeSystem().getType(TypeUtil.CHUNK_TYPE_NAME);
            if (annot.getType() == annotationType) {
                System.out.println(String.format("%s : %s", annot.getType(), annot.getCoveredText()));
            }
        }
    }

    private AnalysisEngine getAE() {
        return ae;
    }

    public CAS newCAS() throws ResourceInitializationException {
        return ae.newCAS();
    }

    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        ae.process(aCAS);
    }

    public static class Builder
    {
        private String descriptor = "edu/umn/biomedicus/chunking/chunkerAE.xml";
        private List<ServiceProviderDescription> serviceProviders = new ArrayList<>();

        public Builder() {
        }

        public Builder(String descriptor) {
            this.descriptor = descriptor;
        }

        public Builder addService(ServiceProviderDescription serviceDesc) {
            serviceProviders.add(serviceDesc);
            return this;
        }

        public Chunker build() throws ResourceInitializationException {
            return new Chunker(this);
        }

        public AnalysisEngine getAE() throws ResourceInitializationException {
            URL resource = this.getClass().getClassLoader().getResource(descriptor);
            return ResourceFactory.createAnalysisEngine(resource, serviceProviders);
        }
    }
}
