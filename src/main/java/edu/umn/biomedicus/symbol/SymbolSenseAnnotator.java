/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.symbol;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.uimafit.component.JCasAnnotator_ImplBase;

import edu.umn.biomedicus.token.TokenCandidate;
import edu.umn.biomedicus.type.Symbol;

/**
 * This annotator annotates the sense of a symbol. The original version is based
 * on the work of Riea Moon(1) and the four symbols addressed in her 2011 paper
 * ('+','-','/', and '#').
 * 
 * Following the implementation of the initial four symbols, this class was
 * expended to include other symbols for the purposes of improving tokenization.
 * The reason this is important to tokenization is that symbols can be
 * classified as being "split symbols" or not. A splitSymbol is one that should
 * be tokenized out as a separate token. For example, the period at the end of a
 * sentence does not contribute meaning to the word it follows, but instead
 * contributes syntactic structure. Therefore, the period in that example would
 * be a "split symbol" and would be tokenized as a separated symbol. However,
 * the string "1,000" should not split on the comma because it is a part of the
 * meaning of the string. In the case of "1,000", this class should mark the
 * comma as "isSplitSymbol=false" and "sense=numericFormatting". Another example
 * is abbreviations and acronyms that use punctuation, such as "Ph.D." Again,
 * the symbols (periods) should be marked as "isSplitSymbol=false" and
 * "sense=abbreviation". Those symbols that are not part of the string's meaning
 * and should be defined as separate (using "isSplitSymbol=true").
 * 
 * This annotator uses a character window; therefore, the CharWindowAnnotator
 * must be in the pipeline before this annotator.
 * 
 * Relevant references:
 * 
 * 1. Moon SR, Pakhomov S, Ryan J, Melton GB. Automated Non-Alphanumeric Symbol
 * Resolution in Clinical Texts. AMIA Annual Symposium Proceedings. 2011. p.
 * 979.
 * 
 * 
 * @author Riea Moon
 * @author Robert Bill
 * 
 */

public class SymbolSenseAnnotator extends JCasAnnotator_ImplBase {
	public ArrayList<Character> manifest = new ArrayList<Character>();

	private Logger logger;
	protected String symbols = "`~!@#$%^&*()_-+={[}]|\\\"':;?/>.<,";
	protected Pattern commaPattern = null;

	// State variables
	int bufferCapitalCount;
	int bufferUpperCaseCount;
	Boolean bufferHasMultipleCapitals;
	Boolean bufferHasInternalPunctuation;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		// get logger
		this.logger = context.getLogger();
		this.logger.logrb(Level.INFO, "SysmbolSenseAnnotator", "typeSystemInit", "initialize",
		                                "edu.umn.biomedicus.token.SymbolSenseAnnotator",
		                                "symbols_sense_annotator_info_initialized");
		commaPattern = Pattern.compile(".*\\d,\\d.*");
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		Type type = jCas.getTypeSystem().getType("edu.umn.biomedicus.type.CharWindow");
		AnnotationIndex<Annotation> windows = jCas.getAnnotationIndex(type);
		char[] documentText = jCas.getDocumentText().toCharArray();

		TokenCandidate candidate = new TokenCandidate();
		for (int i = 0; i < documentText.length; i++) {
			char ch = documentText[i];

			if (Character.isDigit(ch) || Character.isLetter(ch)) {
				candidate.addChar(ch);
			}

			// if it is not alphanumeric, it is a symbol
			// (whitespace is considered a symbol)
			Symbol sym = new Symbol(jCas, i, i + 1);

			// ***** APPLY SYMBOL SENSE RULES **********
			// brackets are always split
			if ("[{}]()".indexOf(ch) > -1) {
				// make all brackets grouping, split tokens
				sym.setIsSplitSymbol(true);
				sym.setSense("grouping");
				sym.addToIndexes(jCas);
				// reset buffer
				candidate = new TokenCandidate();
				continue;
			}

			// pipe, space and newline are always split symbols
			if ("| \n".indexOf(ch) > -1) {
				sym.setIsSplitSymbol(true);
				sym.setSense("separator");
				sym.addToIndexes(jCas);
				// reset buffer
				candidate = new TokenCandidate();
				continue;
			}

			// comma is number format or caesura
			if (ch == ',') {
				// if surrounded by numbers, make it numberFormatting
				Boolean preceededByNumber = Character.isDigit(i - 1);
				Boolean followedByNumber = Character.isDigit(i + 1);
				if (preceededByNumber && followedByNumber) {
					sym.setIsSplitSymbol(false);
					sym.setSense("numberFormat");
				} else {
					sym.setIsSplitSymbol(true);
					sym.setSense("caesura");
				}
				sym.addToIndexes();
				continue;
			}

			if (ch == '.') {
				// if part of an abbreviation/edu.umn.biomedicus.acronym, then not a split symbol.
				// should be followed by whitespace and then a capital letter or
				// end-of-document if it is a stop.
				Boolean whitespaceIsNext = false;
				if (i == documentText.length - 1)
					whitespaceIsNext = true; // end of document really, but same
				else {
					whitespaceIsNext = Character.isWhitespace(documentText[i + 1]);
				}
				if (whitespaceIsNext) {
					if (candidate.hasInternalPeriod() || candidate.getCapitalCount() > 1) {
						// likely edu.umn.biomedicus.acronym/abbreviation use of period. do not
						// split
						sym.setIsSplitSymbol(false);
						sym.setSense("abbreviation");
						sym.addToIndexes();
					}
				}

				boolean foundWhitespace = false;
				boolean capitalAfterWhitespace = false;
				for (int j = 0; j < 50; j++) {
					if ((j == documentText.length - 1)) {
						foundWhitespace = true;
					}

					if (!foundWhitespace && Character.isWhitespace(documentText[j + 1]))
						foundWhitespace = true;
					if (foundWhitespace && !capitalAfterWhitespace) {
						if ((j < documentText.length - 1) && Character.isWhitespace(documentText[j + 1]))
							continue;
						if ((j <= documentText.length - 2) && Character.isUpperCase(documentText[j + 1])) {
							sym.setIsSplitSymbol(true);
							sym.setSense("stop");
						} else if (j > documentText.length - 1) {
							sym.setIsSplitSymbol(true);
							sym.setSense("stop");
						}
					}
				}
				if (!foundWhitespace && !capitalAfterWhitespace) {
					sym.setIsSplitSymbol(false);
					sym.setSense("non-stop"); // This is not really a sense.
					                          // TODO fix to use
					sym.addToIndexes(jCas); // "numericalFormat",
					                        // "abbreviation", etc.
				}
				continue;
			}

			// otherwise not handled yet, so set as split token
			sym.setIsSplitSymbol(true);
			sym.setSense("unknown");
			sym.addToIndexes(jCas);
		}
	}
}
