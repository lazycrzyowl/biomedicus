/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence.evaluation;

import java.io.File;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.evaluation.EngineFactory;
import edu.umn.biomedicus.evaluation.Evaluation;
import edu.umn.biomedicus.evaluation.copy.TokenCopier;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class SentenceAnnotatorFactory implements EngineFactory {

	private String dataWriterFactoryClassName;

	private boolean compressFeatures = true;

	private int featureCutoff = 5;

	/**
	 * 
	 * @param dataWriterFactoryClassName
	 * @param compressFeatures
	 * @param featureCutoff
	 */
	public SentenceAnnotatorFactory(String dataWriterFactoryClassName, boolean compressFeatures, int featureCutoff) {
		// this.dataWriterFactoryClassName = dataWriterFactoryClassName;
		// this.compressFeatures = compressFeatures;
		// this.featureCutoff = featureCutoff;
	}

	/**
	 * The ProbabalisticSentenceBoundaryDetector requires tokens to be in the
	 * CAS when it runs. These tokens may come from the gold-standard data or
	 * they may need to be generated (the data could consist of only sentences.)
	 */
	// public AnalysisEngineDescription createDataWritingAggregate(File
	// modelDirectory)
	// throws ResourceInitializationException {
	//
	// AggregateBuilder builder = new AggregateBuilder();
	//
	// AnalysisEngineDescription patternSentenceBoundaryDetector =
	// AnalysisEngineFactory.createPrimitiveDescription(
	// PatternSentenceBoundaryDetector.class, ComponentFactory.getTypeSystem());
	// builder.add(patternSentenceBoundaryDetector, CAS.NAME_DEFAULT_SOFA,
	// ViewNames.GOLD_VIEW);
	//
	// AnalysisEngineDescription probabilisticSentenceBoundaryDetector =
	// createSentenceDataWriter(
	// modelDirectory.getPath(), compressFeatures, featureCutoff,
	// dataWriterFactoryClassName);
	// builder.add(probabilisticSentenceBoundaryDetector, CAS.NAME_DEFAULT_SOFA,
	// ViewNames.GOLD_VIEW);
	//
	// return builder.createAggregateDescription();
	// }

	public AnalysisEngineDescription createClassifierAggregate(File modelDirectory)
	                                throws ResourceInitializationException {
		AggregateBuilder builder = new AggregateBuilder();

		builder.add(ComponentFactory.createViewTextCopierDescription(ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW));

		// we are always going to copy tokens from the gold view regardless of
		// whether they were provided by
		// the gold-standard data or they were generated.
		AnalysisEngineDescription tokenCopier = AnalysisEngineFactory.createPrimitiveDescription(TokenCopier.class,
		                                ComponentFactory.getTypeSystem());
		builder.add(tokenCopier); // no need to map the views here since they
		// are hard-wired into the copier

		File modelFile = new File(modelDirectory, "model.jar");
		AnalysisEngineDescription sentences = ComponentFactory.createSentenceAnnotator(modelFile.getPath());
		builder.add(sentences, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW);

		return builder.createAggregateDescription();
	}

	public void train(File modelDirectory, String... trainingArguments) throws Exception {
		Evaluation.train(modelDirectory, trainingArguments);
	}

	@Override
	public AnalysisEngineDescription createDataWritingAggregate(File modelDirectory)
	                                throws ResourceInitializationException {
		// TODO Auto-generated method stub
		return null;
	}

	// public static AnalysisEngineDescription createSentenceDataWriter(String
	// outputDirectoryName,
	// boolean compressFeatures, int featureCountCutoff, String
	// dataWriterFactoryClassName)
	// throws ResourceInitializationException {
	//
	// return
	// AnalysisEngineFactory.createPrimitiveDescription(ProbabalisticSentenceBoundaryDetector.class,
	// ComponentFactory.getTypeSystem(),
	// CleartkAnnotator.PARAM_DATA_WRITER_FACTORY_CLASS_NAME,
	// dataWriterFactoryClassName,
	// DirectoryDataWriterFactory.PARAM_OUTPUT_DIRECTORY,
	// outputDirectoryName, DefaultMaxentDataWriterFactory.PARAM_COMPRESS,
	// compressFeatures, DefaultMalletDataWriterFactory.PARAM_COMPRESS,
	// compressFeatures, DefaultMalletCRFDataWriterFactory.PARAM_COMPRESS,
	// compressFeatures, DefaultMultiClassLIBSVMDataWriterFactory.PARAM_CUTOFF,
	// featureCountCutoff, DefaultOVASVMlightDataWriterFactory.PARAM_CUTOFF,
	// featureCountCutoff);
	// }

}
