/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence.evaluation;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.factory.AnalysisEngineFactory;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.evaluation.EvaluationFactory;
import edu.umn.biomedicus.evaluation.FScore;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class SentenceEvaluationFactory implements EvaluationFactory {

	public AnalysisEngineDescription createEvaluationAggregate(File evaluationDirectory)
	                                throws ResourceInitializationException {
		AnalysisEngineDescription evaluator = AnalysisEngineFactory.createPrimitiveDescription(
		                                SentenceEvaluatorAE.class, ComponentFactory.getTypeSystem(),
		                                SentenceEvaluatorAE.PARAM_OUTPUT_DIRECTORY, evaluationDirectory.getPath());
		return evaluator; // it's perfectly fine to return a primitive here -
		// the interface doesn't care.
	}

	public void aggregateEvaluationResults(List<File> evaluationDirectories, File outputDirectory) throws Exception {
		FScore aggregateFScore = new FScore();

		for (File evaluationDirectory : evaluationDirectories) {
			ObjectInputStream input = new ObjectInputStream(new FileInputStream(new File(evaluationDirectory,
			                                SentenceEvaluatorAE.FSCORE_FILE_NAME)));
			FScore fScore = (FScore) input.readObject();
			aggregateFScore.add(fScore);
		}

		PrintStream out = new PrintStream(new File(outputDirectory, SentenceEvaluatorAE.OUTPUT_FILE_NAME));
		aggregateFScore.print(out);
		out.close();
	}

}
