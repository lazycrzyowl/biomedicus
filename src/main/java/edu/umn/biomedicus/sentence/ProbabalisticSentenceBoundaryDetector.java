/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.sentence;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class ProbabalisticSentenceBoundaryDetector { // extends
													 // CleartkAnnotator<Boolean>
													 // {

}
/*
 * The features corresponding 'InsideParens' and 'InsideQuotes' need a little
 * explaining. The idea is that periods that fall inside parentheses or quotes
 * may be less likely to be a sentence delimiter. Instead of trying to
 * definitively make sure a period is or is not inside one of these enclosing
 * character categories, this is instead approximated here. When a left paren is
 * seen we will mark a boolean that tells us we are inside parentheses. When a
 * right paren is seen we will unmark the boolean to let us know that we are now
 * outside the parentheses. So, this will not actually work correctly for nested
 * parentheses. Because nested parentheses are not handled correctly and because
 * a text may be missing a closing parenthesis, we will simply reset these flags
 * every time we come across a sentence boundary. In this way, errors won't
 * propagate throughout the document.
 */
// enclosingParens will be of length three and have the markers for round,
// square, and curly brackets.
// 0=(), 1=[], 2={}
// boolean[] enclosingParens = new boolean[3];
// enclosingQuotes will be of length two and have markers for double and
// single quotes.
// 0=", 1='
// boolean[] enclosingQuotes = new boolean[2];

// @Override
// public void process(JCas jCas) throws AnalysisEngineProcessException {
// try {
// String text = jCas.getDocumentText();
// // we will assume there is never a sentence of zero length at the
// // beginning of the document
// // so, we will start at the second character.
// for (int charOffset = 1; charOffset < text.length(); charOffset++) {
// char c = text.charAt(charOffset);
// if (isSentenceBoundaryChar(c)) {
// proccessSentenceBoundary(jCas, charOffset);
// } else {
// updatedEnclosingChars(c);
// }
// }
// } catch (CleartkExtractorException ce) {
// throw new AnalysisEngineProcessException(ce);
// }
// }
//
// private void proccessSentenceBoundary(JCas jCas, int charOffset) throws
// CleartkProcessingException {
// if the PatternSentenceBoundaryDetector has already placed a sentence
// boundary at this character offset
// then we are not going to worry about learning or classifying this
// character offset because we assume that
// the PatternSentenceBoundayDetector knows what is doing and we
// shouldn't be correcting the work it has
// already done here. Additionally, we do not want to be learning the
// "easy" instances anyways as we want
// our models to focus on the problematic cases.
// if (SentenceBoundaryUtil.getSentenceBoundary(jCas, charOffset) != null) {
// resetEnclosingChars();
// return;
// }
//
// List<Feature> features = extractFeatures(jCas, charOffset);
// if we are training, then we will obtain the outcome for the training
// instance and send it off to the data writer.
// if (isTraining()) {
// boolean outcome = isEndOfSentence(jCas, charOffset);
// Instance<Boolean> instance = new Instance<Boolean>(outcome, features);
// dataWriter.write(instance);
// if (outcome) {
// // if we are at the end of a sentence, then we want to reset the
// // "enclosing" markers so that errors related to
// // e.g. missing parens or nested parens are not propagated
// // through the document.
// resetEnclosingChars();
// }
// }
// if we are not training, then we will classify the features for this
// offset and get an outcome
// if the classifier returns true, then we will create a new sentence
// boundary and reset the "enclosing" markers.
// else {
// boolean outcome = classifier.classify(features);
// if (outcome) {
// SentenceBoundaryUtil.createSentenceBoundary(jCas, charOffset);
// resetEnclosingChars();
// }
// }
// }
//
/**
 * Some of the features extracted here come from Table 1 of the following paper:
 * http://www.aclweb.org/anthology/N/N09/N09-2061.pdf
 * 
 * @param jCas
 * @param charOffset
 * @return
 */

// private List<Feature> extractFeatures(JCas jCas, int charOffset) {
//
// List<Feature> features = new ArrayList<Feature>();
// String leftWord = getLeftWord(jCas, charOffset);
// String rightWord = getRightWord(jCas, charOffset);
//
// // table 1 features
// features.add(new Feature("LW", leftWord));
// features.add(new Feature("RW", rightWord));
// features.add(new Feature("LW_length", leftWord.length()));
// features.add(new Feature("RW_iscap",
// CaseUtil.isInitialUppercase(rightWord)));
// features.add(new Feature("LW_RW", leftWord + "_" + rightWord));
// features.add(new Feature("LW_RWiscap", leftWord + "_" +
// CaseUtil.isInitialUppercase(rightWord)));
//
// // character based features
// String text = jCas.getDocumentText();
// if (charOffset > 0 && charOffset < text.length() - 1) {
// char leftChar = text.charAt(charOffset - 1);
// char rightChar = text.charAt(charOffset + 1);
// // the character immediately adjacent on either side is a letter
// if (Character.isLetter(leftChar) && Character.isLetter(rightChar)) {
// features.add(new Feature("Two_Letters", true));
// }
// }
//
// if (charOffset < text.length() - 1) {
// char rightChar = text.charAt(charOffset + 1);
// // the character to the right is whitespace
// if (Character.isWhitespace(rightChar)) {
// features.add(new Feature("Trailing_Whitespace", true));
// }
// }
//
// if (charOffset > 0) {
// char leftChar = text.charAt(charOffset - 1);
// // the character to the left is whitespace (i.e. maybe it's an
// // initial)
// if (Character.isLetter(leftChar) && !Character.isLowerCase(leftChar)) {
// features.add(new Feature("UpperCaseChar", true));
// }
// }

// //this is a bad feature to use for the genia corpus because all
// sentence-ending periods are followed by two spaces.
// if(charOffset < text.length()-2) {
// char rightChar = text.charAt(charOffset + 1);
// char rightRightChar = text.charAt(charOffset + 2);
// if(Character.isWhitespace(rightChar) &&
// Character.isWhitespace(rightRightChar)) {
// features.add(new Feature("Two_Trailing_Whitespaces", true));
// }
// }

// if (enclosingParens[0] || enclosingParens[1] || enclosingParens[2]) {
// features.add(new Feature("InsideParens", true));
// }

// this feature has no effect on the GENIA data which means there either
// aren't many quotes or
// there is something wrong with this feature (you would expect positive
// or negative impact of some kind!)
// if(enclosingQuotes[0] || enclosingQuotes[1]) {
// features.add(new Feature("InsideQuotes", true));
// }
// return features;
// }
//
// public static String getLeftWord(JCas jCas, int charOffset) {
// String text = jCas.getDocumentText();
//
// StringBuilder word = new StringBuilder();
// boolean nonwhiteSpaceSeen = false;
// for (int offset = charOffset - 1; offset >= 0; offset--) {
// char c = text.charAt(offset);
// if (Character.isWhitespace(c) && !nonwhiteSpaceSeen) {
// continue;
// } else if (Character.isWhitespace(c) && nonwhiteSpaceSeen) {
// break;
// } else {
// nonwhiteSpaceSeen = true;
// word.insert(0, c);
// }
// }
//
// if (word.length() > 0) {
// return word.toString();
// } else {
// return "<BEGIN DOCUMENT>";
// }
// }
//
// public static String getRightWord(JCas jCas, int charOffset) {
// String text = jCas.getDocumentText();
//
// StringBuilder word = new StringBuilder();
// boolean nonwhiteSpaceSeen = false;
// for (int offset = charOffset + 1; offset < text.length(); offset++) {
// char c = text.charAt(offset);
// if (Character.isWhitespace(c) && !nonwhiteSpaceSeen) {
// continue;
// } else if (Character.isWhitespace(c) && nonwhiteSpaceSeen) {
// break;
// } else {
// nonwhiteSpaceSeen = true;
// word.append(c);
// }
// }
//
// if (word.length() > 0) {
// return word.toString();
// } else {
// return "<END DOCUMENT>";
// }
//
// }
//
// public static boolean isEndOfSentence(JCas jCas, int charOffset) {
// Sentence sentence = AnnotationRetrieval.getContainingAnnotation(jCas, new
// Annotation(jCas, charOffset - 1,
// charOffset), Sentence.class);
// if (sentence == null) {
// return false;
// }
// return sentence.getEnd() - 1 == charOffset;
// }
//
// private boolean isSentenceBoundaryChar(char c) {
// return c == '.';
// }
//
// private void updatedEnclosingChars(char c) {
// switch (c) {
// case '"':
// enclosingQuotes[0] = !enclosingQuotes[0];
// break;
// case '\'':
// enclosingQuotes[1] = !enclosingQuotes[1];
// break;
// case '(':
// enclosingParens[0] = true;
// break;
// case ')':
// enclosingParens[0] = false;
// break;
// case '[':
// enclosingParens[1] = true;
// break;
// case ']':
// enclosingParens[1] = false;
// break;
// case '{':
// enclosingParens[2] = true;
// break;
// case '}':
// enclosingParens[2] = false;
// break;
// default:
// break;
// }
// }
//
// private void resetEnclosingChars() {
// Arrays.fill(enclosingParens, false);
// Arrays.fill(enclosingQuotes, false);
// }
//
// }
