/* 
  Copyright 2010-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

/**
 * 
 * 
 *  Sentence annotations are created by first finding sentence boundaries by either using patterns or a classifier.  The former is accomplished by
 *  {@link edu.umn.biomedicus.sentence.PatternSentenceBoundaryDetector} and the latter is accomplished by 
 *  {@link edu.umn.biomedicus.sentence.ProbabalisticSentenceBoundaryDetector}.  Both of these analysis engines create annotations of type 
 *  {@link edu.umn.biomedicus.type.SentenceBoundary}.  A SentenceBoundary should annotate the last character of a sentence.  So, if a sentence ends with
 *  a period ('.'), then the corresponding sentence boundary annotation should cover that period.  This is important to remember for the pattern-based
 *  sentence boundary detection because often what is being looked for directly follows the last character of a sentence rather than the last character
 *  itself.  For example, if consecutive newlines are a pattern used to determine a sentence boundary - the sentence boundary should be the non-whitespace
 *  character that precedes the consecutive newlines.  After sentence boundaries are created from both the pattern-based and classifer-based approaches, 
 *  sentence annotations of type {@link edu.umn.biomedicus.type.Sentence} are created.  This is accomplished by 
 *  {@link edu.umn.biomedicus.sentence.SentenceAnnotator}.  The first sentence will span from the first non-whitespace character to the first 
 *  sentence boundary.  The second sentence will span from the first non-whitespace character following the first sentence boundary to the next
 *  sentence boundary, and so on.  
 *  <p>
 *  A {@link edu.umn.biomedicus.type.SentenceBoundary} can be created for characters which are not in fact sentence boundaries.  This allows you to make
 *  a negative judgment about a character in PatternSentenceBoundaryDetector and make sure that it gets respected downstream.  See, for example, how
 *  PatternSentenceBoundaryDetector creates negative sentence boundaries for decimal points.  
 *  <p>
 *   SplitPatterns added to the pattern-based sentence boundary detection should be very high precision as they take precedence over the probabilistic-based
 *   approach.  During training, sentence boundaries that are discovered by the pattern-based approach will not be added to the training data for the 
 *   classifier-based approach.  As an example, if a pattern such as ".  " (a period followed by two spaces) was added to the pattern-based approach 
 *   (i.e. because this was determined to be a very high precision pattern for finding sentences), then no periods followed by two spaces will be 
 *   used for training the classifier used by the classifier-based approach.  
 *   <p>
 *    Sentence boundaries should always be created by {@link edu.umn.biomedicus.type.SentenceBoundaryUtil#createSentenceBoundary(org.apache.uima.jcas.JCas, int)}.
 *    This method ensures that no more than one sentence boundary is created at a given character offset.  This way, you do not have to worry about 
 *    whether or not to create a duplicate sentence boundary.  I have created a unit test which checks that "new SentenceBoundary()" is never called - so
 *    don't even try it!  
 * 
 *  @author Philip Ogren
 */

package edu.umn.biomedicus.sentence;