/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.SentenceBoundary;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class SentenceAnnotator extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		int sentenceBegin = getBeginningOfNextSentence(jCas, 0);

		for (SentenceBoundary sentenceBoundary : JCasUtil.select(jCas, SentenceBoundary.class)) {
			if (sentenceBoundary.getEnd() < sentenceBegin) {
				continue;
			}
			if (!sentenceBoundary.getSentenceBoundary()) {
				continue;
			}

			Sentence sentence = new Sentence(jCas, sentenceBegin, sentenceBoundary.getEnd());
			sentence.addToIndexes();
			sentenceBegin = getBeginningOfNextSentence(jCas, sentence);
			if (sentenceBegin == -1) {
				break;
			}
		}

	}

	private int getBeginningOfNextSentence(JCas jCas, Sentence sentence) {
		return getBeginningOfNextSentence(jCas, sentence.getEnd());
	}

	private int getBeginningOfNextSentence(JCas jCas, int endOfPreviousSentence) {
		int sentenceBegin = endOfPreviousSentence;
		String text = jCas.getDocumentText();
		text = text.substring(endOfPreviousSentence);
		if (!text.startsWith(text.trim())) {
			sentenceBegin = text.indexOf(text.trim());
			if (sentenceBegin != -1) {
				sentenceBegin += endOfPreviousSentence;
			}
		}
		return sentenceBegin;
	}

}
