/* 
  Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.sentence;

// TODO: testing, x-validation on sentences trained on clinical and genia corpus
import java.io.InputStream;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.Span;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;

import edu.umn.biomedicus.type.Sentence;

/**
 * This annotator uses the OpenNLP MaxEnt sentence boundary detector.
 * 
 * @author Robert Bill
 * 
 */
public class MaxentSentenceBoundaryDetector extends JCasAnnotator_ImplBase {
	private SentenceDetectorME sentenceDetector;

	public static final String PARAM_MODEL_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                MaxentSentenceBoundaryDetector.class, "modelFile");

	@ConfigurationParameter(mandatory = false, defaultValue = "sentence-me-model.bin")
	private String modelFile;

	/**
	 * The initialize method for the MaxentSentenceboundaryDetector. This loads
	 * the model specified and instantiates the SentenceDetector.
	 */
	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		InputStream modelInputStream = this.getClass().getResourceAsStream(modelFile);
		SentenceModel model = null;
		try {
			model = new SentenceModel(modelInputStream);
		} catch (Exception e) {
			System.err.println("Unable to open sentence model file: " + modelFile);
			e.printStackTrace();
		}
		sentenceDetector = new SentenceDetectorME(model);
	}

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// put the entire document in a String var
		String documentText = jcas.getDocumentText();

		// check for nulls. It seems odd, but it is possible with DB readers or
		// other unusual sources. Note that for practical purposes a null string
		// will be equivalent to an empty string.
		if (documentText == null) {
			documentText = "";
		}

		// apply the sentence detector and get the array of detected spans
		Span[] spans = sentenceDetector.sentPosDetect(documentText);
		for (int i = 0; i < spans.length; i++) {
			Sentence sent = new Sentence(jcas);
			sent.setBegin(spans[i].getStart());
			sent.setEnd(spans[i].getEnd());
			sent.addToIndexes();
		}
	}
}
