/* 
  Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;

import edu.umn.biomedicus.type.Line;

/**
 * This class annotates lines in a file. It is only useful for when input files
 * are data-per-line type of files.
 * 
 * @author Robert Bill
 * 
 */
public class LineAnnotator extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		char[] chars = jCas.getDocumentText().toCharArray();

		int lineBegin = 0;

		for (int i = 0; i < chars.length; i++) {
			if (chars[i] == '\n') {
				new Line(jCas, lineBegin, i).addToIndexes();
				lineBegin = i + 1;
			} else if (i == chars.length - 1) {
				new Line(jCas, lineBegin, chars.length).addToIndexes();
			}
		}
	}
}
