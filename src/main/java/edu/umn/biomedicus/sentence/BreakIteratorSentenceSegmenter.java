/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;

import com.ibm.icu.text.BreakIterator;

import edu.umn.biomedicus.type.Sentence;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class BreakIteratorSentenceSegmenter extends JCasAnnotator_ImplBase {

	private BreakIterator breakIterator;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		breakIterator = BreakIterator.getSentenceInstance();
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		String text = jCas.getDocumentText();
		// Replace null with ""
		if (text == null) {
			text = "";
		}
		breakIterator.setText(text);

		int begin = breakIterator.first();
		int end = breakIterator.next();
		while (end != BreakIterator.DONE) {
			int sentenceEnd = end;
			while (Character.isWhitespace(text.charAt(sentenceEnd - 1))) {
				sentenceEnd--;
			}
			String sentence = text.substring(begin, sentenceEnd);
			if (!sentence.trim().equals("")) {
				new Sentence(jCas, begin, end).addToIndexes();
			}
			begin = end;
			end = breakIterator.next();
		}
	}
}
