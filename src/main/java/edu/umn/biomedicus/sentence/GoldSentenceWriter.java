/**
 * 
 */
package edu.umn.biomedicus.sentence;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.CasConsumer_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;

/**
 * This AE writes GOLD sentences to an output file so it is easy to create
 * models to train sentence segmentation tools.
 * 
 * @author Robert Bill
 */
public class GoldSentenceWriter extends CasConsumer_ImplBase {

	public static final String PARAM_OUTPUT_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                GoldSentenceWriter.class, "outputFileName");

	@ConfigurationParameter(name = "outputFile", description = "The path and filename.")
	private String outputFileName;
	private BufferedWriter outputFile;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		// open output file specified in parameters
		try {
			outputFile = new BufferedWriter(new FileWriter(outputFileName, true));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void process(CAS cas) throws AnalysisEngineProcessException {
		JCas jcas;
		try {
			// make sure we're writing only GOLD_VIEW sentences
			jcas = cas.getJCas().getView("GOLD_VIEW");
			for (Sentence sent : JCasUtil.select(jcas, Sentence.class)) {
				try {
					String text = sent.getCoveredText();

					// training files need one sentence per line; therefore, we
					// need to eliminate any newline characters within the
					// sentence.
					outputFile.write(text.replaceAll("\n", " "));
					outputFile.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (CASException e) {
			e.printStackTrace();
		}
	}
}
