/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence;

import edu.umn.biomedicus.type.SentenceBoundary;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import edu.umn.biomedicus.type.SentenceBoundaryUtil;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class PatternSentenceBoundaryDetector extends JCasAnnotator_ImplBase {

	// This pattern should cover both unix newlines (i.e. '\n') and windows
	// newlines (i.e.'\r\n') because
	// '\r' is a whitespace character and so is captured by the optional '\s'
	// characters in the pattern.
	public static final Pattern newlinesPattern = Pattern.compile("\\s*\\n\\s*\\n\\s*");

	// This pattern covers cases in which a period is surrounded by a digit on
	// either side
	public static final Pattern decimalPointPattern = Pattern.compile("[0-9](\\.)(?=[0-9])");

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		String text = jCas.getDocumentText();
		Matcher matcher = newlinesPattern.matcher(text);

		while (matcher.find()) {
			int charOffset = matcher.start() - 1;
			SentenceBoundary sb = new SentenceBoundary(jCas, charOffset, charOffset + 1);
            sb.addToIndexes();
		}

		matcher = decimalPointPattern.matcher(text);
		while (matcher.find()) {
			int charOffset = matcher.start(1);
			SentenceBoundary sb = new SentenceBoundary(jCas, charOffset, charOffset + 1);
            sb.addToIndexes();
		}
	}

}
