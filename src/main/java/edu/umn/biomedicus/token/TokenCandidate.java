package edu.umn.biomedicus.token;

import java.util.ArrayList;

/**
 * Just an experiment. Ignore this class for now.
 * 
 * @author robertbill
 * 
 */
public class TokenCandidate {
	private ArrayList<Character> buffer = null;
	private Boolean isAllCaps = false;
	private Boolean internalSymbol = false;
	private Boolean internalPeriod = false;
	private Boolean nonLeadingSymbol = false;
	private int capitalCount = 0;

	public TokenCandidate() {
		buffer = new ArrayList<Character>();
	}

	public void addChar(Character ch) {
		if (buffer == null)
			buffer = new ArrayList<Character>();
		buffer.add(ch);
		if (Character.isUpperCase(ch)) {
			capitalCount++;
		}
		if (".:&".indexOf(ch) > -1) {
			internalSymbol = true;
			if (ch.equals('.')) {
				internalPeriod = true;
			}

		}
	}

	public Boolean isAllCaps() {
		return isAllCaps;
	}

	public Boolean hasInternalSymbol() {
		return internalSymbol;
	}

	public Boolean hasInternalPeriod() {
		return internalPeriod;
	}

	public int getCapitalCount() {
		return capitalCount;
	}
}
