/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.token;

import java.util.HashMap;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.uimafit.component.JCasAnnotator_ImplBase;

import edu.umn.biomedicus.type.Symbol;
import edu.umn.biomedicus.type.Token;

/**
 * A simple tokenizer that separates by whitespace plus symbols that the
 * {@link #symbolSenseAnnotator} marks as being split symbols.
 * 
 * @author Robert Bill
 * 
 */

public class WhitespaceTokenizer extends JCasAnnotator_ImplBase {
	private Logger logger;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		this.logger = context.getLogger();
		this.logger.logrb(Level.INFO, "WhitespaceTokenizer", "typeSystemInit", "initialize",
		                                "edu.umn.biomedicus.token.WhitespaceTokenizer",
		                                "whitespace_tokenizer_info_initialized");
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		System.out.println("Processing next JCas");

		String docText = jCas.getDocumentText();
		// There is risk of null for text, so replace null with ""
		if (docText == null) {
			docText = "";
		}

		Integer begin = null;
		Integer end = 0;
		char[] text = docText.toCharArray();

		HashMap<Integer, Boolean> symbols = new HashMap<Integer, Boolean>();
		AnnotationIndex<Annotation> symbolsIdx = null;
		Type type = jCas.getTypeSystem().getType("edu.umn.biomedicus.type.Symbol");
		symbolsIdx = jCas.getAnnotationIndex(type);
		for (Annotation annot : symbolsIdx) {
			Symbol sym = (Symbol) annot;
			symbols.put(sym.getBegin(), sym.getIsSplitSymbol());
		}

		for (int i = 0; i < text.length; i++) {
			// expensive to use isWhitespace, but optimize later.
			boolean isWhitespace = Character.isWhitespace(text[i]);

			// no characters found yet if begin == null
			if (begin == null) {
				if (isWhitespace)
					// still, no characters found yet
					continue;
				if (symbols.containsKey(i) && symbols.get(i) == true)
					addToken(jCas, i, i + 1); // annotate split symbol
				else
					begin = i; // found a character- mark beginning of token
			} else {
				if (isWhitespace) {
					addToken(jCas, begin, i);
					begin = null;
				} else if (symbols.containsKey(i) && symbols.get(i) == true) {
					addToken(jCas, begin, i);
					addToken(jCas, i, i + 1);
					begin = null;
				}
			}
		}
		// check if document ends while currently looking for the end of a token
		if (begin != null) {
			addToken(jCas, begin, text.length);
		}
	}

	protected void addToken(JCas jCas, int begin, int end) {
		Token token = new Token(jCas, begin, end);
		token.addToIndexes();
	}
}
