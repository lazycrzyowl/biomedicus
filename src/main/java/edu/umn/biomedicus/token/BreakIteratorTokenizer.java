/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.token;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;

import com.ibm.icu.text.BreakIterator;
import com.ibm.icu.text.RuleBasedBreakIterator;

import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class BreakIteratorTokenizer extends JCasAnnotator_ImplBase {

	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/token/breakiterator/tokenizer_break_rules.txt")
	private String breakRulesUrl;

	private BreakIterator breakIterator;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		try {
			super.initialize(context);

			InputStream inputStream = this.getClass().getResourceAsStream(breakRulesUrl);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String rules = FileUtil.loadTextFile(reader);
			breakIterator = new RuleBasedBreakIterator(rules);
		} catch (Exception e) {
			throw new ResourceInitializationException(e);
		}
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		String text = jCas.getDocumentText();
		// There is risk of null for text, so replace null with ""
		if (text == null) {
			text = "";
		}
		breakIterator.setText(text);

		int begin = breakIterator.first();
		int end = breakIterator.next();
		while (end != BreakIterator.DONE) {
			String token = text.substring(begin, end);
			if (!token.trim().equals("")) {
				new Token(jCas, begin, end).addToIndexes();
			}
			begin = end;
			end = breakIterator.next();
		}
	}
}
