/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token.split;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.ibm.icu.text.UnicodeSet;

/**
 * This class is meant to be somewhat analagous to {@link Pattern} but also has
 * many important differences. In particular, the pattern syntax is greatly
 * simplified, matching is always performed on the entire input, and many
 * patterns can be added to a single instance of this class. In fact, this class
 * is optimized to accommodate a very large number of patterns - potentially
 * hundreds of thousands or millions.
 * <p>
 * For a concise description of the pattern syntax for token splitting please
 * see: <a
 * href="http://code.google.com/p/biomedicus/wiki/Tokenization">Tokenization
 * </a>. A split pattern should consist of sequence of characters with a
 * vertical bar, '|' indicating where a token split should occur. For example,
 * the pattern "abc|de" would match the word "abcde" and the returned
 * {@link SplitMatch} would indicate that the word "abcde" should be split at
 * index 3. A pattern may contain multiple split characters. For example, the
 * pattern "a|b|c" would match the word "abc" and give two splits at index 1 and
 * 2. Every pattern must have at least one unescaped vertical bar in it.
 * <p>
 * A single character can be followed by one of three greedy quantifiers: ?, +,
 * or *. There is no grouping in the pattern syntax. The following is a valid
 * pattern: "a?b+|cd*" which would match "abcd", "bbbcddd", "bc", and "bcddd"
 * (among others.)
 * <p>
 * A unicode set may also be used instead of a character. The syntax for unicode
 * sets are documented here:
 * http://userguide.icu-project.org/strings/unicodeset. So, for example, the
 * pattern "[0-9]+|mg" will match "18mg" and split it at the character offset 2.
 * If you use an unescaped left square bracket, then it must begin a valid
 * unicode set specifier.
 * <p>
 * There are six special use characters that if you want to match them, then
 * they need to be escaped: '[', '|', '?', '+', '\', and '*'. So, for example,
 * the pattern "a|\?" will match the word "a?" and "a|\|" will match "a|".
 * Unfortunately, the string literal must escape the escape character - so the
 * string literals for the pattern examples given in the previous sentence would
 * actually be "a|\\?" and "a|\\|". To match the word
 * "a\" you could use the pattern "a|\\\\".
 * 
 * <p>
 ** This class is used to create a trie-like graph used for matching words by
 * {@link SplitMatcher}.
 * 
 * @author Philip Ogren
 * 
 */

public class SplitPatterns {

	// the top node of the trie-like graph.
	private Node topNode;

	private int idCounter = 0;

	private Map<Integer, String> patternIdMap;

	public SplitPatterns() {
		topNode = new Node();
		patternIdMap = new HashMap<Integer, String>();
	}

	public SplitPatterns(String... patterns) throws SplitPatternException {
		this();
		if (patterns != null) {
			for (String pattern : patterns) {
				add(pattern);
			}
		}
	}

	/**
	 * Adds the given string pattern to the graph and returns an integer id. A
	 * SplitPatternException will be thrown if no vertical bar, '|', is found in
	 * the provided pattern. Also, if a pattern collides with another pattern,
	 * then a SplitPatternException will be thrown. A collision is defined as
	 * two patterns that end on the same node. This would happen if the two
	 * patterns were the same, or if two patterns are the same but differ only
	 * with respect to the quantifiers used. For example, the pattern "ab|cd"
	 * collides with "a*b?|c+d*". Note that even though collisions such as these
	 * are not allowed, this does not prevent multiple matches for a given word
	 * because the patterns "a[b-e]f" and "a[b-c]f" do not collide even though
	 * they both match the word "abf".
	 * 
	 * @param pattern
	 * @return a pattern id for the pattern
	 * @throws SplitPatternException
	 */
	public int add(String pattern) throws SplitPatternException {
		// the implementation of this method is really ugly and should've been
		// accomplished using
		// a syntax parsing tool such as ANTLR or JavaCC.

		verifyPattern(pattern);

		int id = idCounter++;
		// is set to true if there should be a split between the current node
		// and the next node.
		boolean isSplitNode = false;
		// is set to true if a vertical bar is found in the pattern. If we get
		// to the end of the pattern and this is still false, then we will throw
		// an exception.
		boolean splitFound = false;
		patternIdMap.put(id, pattern);

		Node node = topNode;

		// optional nodes (a character followed by a '?' or '*') can be tricky
		// because you can have a series of them. For example,
		// you might have a pattern like "ab?c*d?|e?f?g". The graph needs to be
		// able to match "ag" and put a split between the "a" and "g".
		// To do this the 'a' node needs to be a parent of the 'g' node and
		// needs to mark a split between it and the 'g' node. Similarly, the
		// graph should be able to match "aceg". This means that 'a' needs to be
		// a parent of 'c', 'c' a parent of 'e', and 'e' a parent of 'g'.
		// However, only 'c' should have a split between itself and its child
		// 'e' of these three.
		// parentsOfOptionalNodes keeps track of this information. The key is a
		// parent of the next optional node. The value specifies whether or not
		// there will be a split between the parent node and the optional child
		// node.
		Map<Node, Boolean> parentsOfOptionalNodes = new HashMap<Node, Boolean>();

		/*
		 * The basic goal of this for loop is to make a child node out of the
		 * next character and add it to the current node.
		 */
		for (int i = 0; i < pattern.length();) {
			int codePoint = pattern.codePointAt(i);

			// an unescaped vertical bar '|' is where we mark split nodes
			if (codePoint == '|' && i > 0 && pattern.charAt(i - 1) != '\\') {
				isSplitNode = true;
				i++; // here we know the char count is one
				continue;
			}

			Node childNode = null;

			// we are looking for escaped characters '[', '|', '?', '*', '+',
			// '\'. we will just advance to that character and create a child
			// node for that character (skipping the backslash).
			if (codePoint == '\\') {
				if (i >= pattern.length() - 1) {
					throw new SplitPatternException("a pattern cannot end with an unescaped backslash");
				}
				int nextIndex = i + Character.charCount(codePoint);
				int nextCodePoint = pattern.codePointAt(nextIndex);
				if (nextCodePoint == '[' || nextCodePoint == '|' || nextCodePoint == '?' || nextCodePoint == '*'
				                                || nextCodePoint == '+' || nextCodePoint == '\\') {
					childNode = node.getCharacterChild(nextCodePoint);
					if (childNode == null) {
						childNode = new CharacterNode(nextCodePoint);
					}
					i = nextIndex + Character.charCount(nextCodePoint);
				} else {
					throw new SplitPatternException(
					                                "the only characters that can be escaped are  '[', '|', '?', '*', '+', and '\'.");
				}
			}

			// a left square bracket '[' indicates that a unicode set is in the
			// pattern. So, we find the pattern, make a unicode set out of it,
			// and advance to the end of the unicode set pattern.
			if (codePoint == '[') {
				int patternBegin = i;
				int patternEnd = findUnicodeSetPatternEnd(pattern, patternBegin + 1);
				i = patternEnd;
				String unicodeSetPattern = pattern.substring(patternBegin, patternEnd);
				UnicodeSet unicodeSet = new UnicodeSet(unicodeSetPattern);
				childNode = node.getUnicodeSetChild(unicodeSet);
				if (childNode == null) {
					childNode = new UnicodeSetNode(unicodeSet);
				}
			}

			// if we have found a plus sign then we have the current node point
			// to itself. if the node was a split node, then we want to make
			// sure that we do not add it again to the set of split ids.
			if (codePoint == '+') {
				childNode = node;
				i += Character.charCount(codePoint);
				isSplitNode = false;
			}

			// at this point if the childNode is still null then we know to make
			// it a character node
			if (childNode == null) {
				i += Character.charCount(codePoint);
				childNode = node.getCharacterChild(codePoint);
				if (childNode == null) {
					childNode = new CharacterNode(codePoint);
				}
			}

			// add all the parents that we've kept around because of optional
			// nodes
			if (parentsOfOptionalNodes.size() > 0) {
				for (Entry<Node, Boolean> entry : parentsOfOptionalNodes.entrySet()) {
					entry.getKey().add(childNode);
					if (entry.getValue() || isSplitNode) {
						entry.getKey().addSplitId(childNode, id);
					}
				}
			}
			// add the current node to parentsOfOptionalNodes if the child node
			// is optional
			if (i < pattern.length() && (pattern.codePointAt(i) == '?' || pattern.codePointAt(i) == '*')) {
				parentsOfOptionalNodes.put(node, isSplitNode);
				if (isSplitNode) {
					// if the child node is a split node this tells us that all
					// the parents in parentsOfOptionalNodes
					// now need to be set to true because there is now a split
					// between the parents and any subsequent children.
					for (Node parentNode : parentsOfOptionalNodes.keySet()) {
						parentsOfOptionalNodes.put(parentNode, true);
					}
				}
				if (pattern.codePointAt(i) == '*') {
					childNode.add(childNode);
				}
				i++;
			} else {
				// we only keep nodes in the map as long as we see consecutive
				// optional nodes here we no we have a non-optional node and so
				// we can clear these parents.
				parentsOfOptionalNodes.clear();
			}

			if (isSplitNode) {
				node.addSplitId(childNode, id);
				isSplitNode = false;
				splitFound = true;
			}
			if (i == pattern.length()) {
				boolean success = childNode.setPatternId(id);
				if (!success) {
					String collisionPattern = getPattern(childNode.getPatternId());
					throw new SplitPatternException(
					                                "the pattern added ('"
					                                                                + pattern
					                                                                + "') collides with a pattern that has already been added ('"
					                                                                + collisionPattern
					                                                                + "').  Words found by the pattern just added will appear as though they were found by the collided pattern.");
				}
				if (parentsOfOptionalNodes.size() > 0) {
					for (Entry<Node, Boolean> entry : parentsOfOptionalNodes.entrySet()) {
						success = entry.getKey().setPatternId(id);
						if (!success) {
							String collisionPattern = getPattern(entry.getKey().getPatternId());
							throw new SplitPatternException(
							                                "the pattern added ('"
							                                                                + pattern
							                                                                + "') collides with a pattern that has already been added ('"
							                                                                + collisionPattern
							                                                                + "').  Words found by the pattern just added will appear as though they were found by the collided pattern.");
						}

					}
				}

			}
			node.add(childNode);
			node = childNode;
		}
		if (splitFound)
			return id;
		else {
			throw new SplitPatternException(
			                                "the pattern being added as no non-escaped split character: '|'.  Bad pattern is '"
			                                                                + pattern + "'.");
		}
	}

	private int findUnicodeSetPatternEnd(String pattern, int patternBegin) {
		int nestedBrackets = 0;
		for (int i = patternBegin; i < pattern.length(); i++) {
			if (pattern.charAt(i) == ']' && pattern.charAt(i - 1) != '\\') {
				if (nestedBrackets == 0) {
					return i + 1;
				}
				nestedBrackets--;
			}
			if (pattern.charAt(i) == '[') {
				nestedBrackets++;
			}
		}
		throw new RuntimeException("pattern does not have matching square brackets at offset " + patternBegin);
	}

	public Node getTopNode() {
		return topNode;
	}

	/**
	 * Return the pattern for a given pattern id.
	 * 
	 * @param id
	 * @return a the pattern for the provided pattern id.
	 */
	public String getPattern(int id) {
		return patternIdMap.get(id);
	}

	public void print(PrintStream out) {
		Node.print(out, getTopNode());
	}

	private void verifyPattern(String pattern) throws SplitPatternException {
		if (pattern.length() < 3) {
			throw new SplitPatternException(
			                                "a split pattern must consist of at least three characters.  Bad pattern is '"
			                                                                + pattern + "'.");
		}

		if (pattern.startsWith("|")) {
			throw new SplitPatternException(
			                                "a split pattern must not begin with the split character '|'.  Bad pattern is '"
			                                                                + pattern + "'.");
		}

		if (pattern.endsWith("|") && !pattern.endsWith("\\|")) {
			throw new SplitPatternException(
			                                "a split pattern must not end with the split character '|'.  Bad pattern is '"
			                                                                + pattern + "'.");
		}

	}

}
