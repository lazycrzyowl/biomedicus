/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token.split;

import java.util.ArrayList;
import java.util.List;

import com.ibm.icu.text.UnicodeSet;

/**
 * This class provides a way to split tokens into multiple tokens. The expected
 * use for this class is that it should be run on text that is already
 * tokenized. Each token is passed in turn into the match method and if the word
 * matches a pattern in the list of {@link SplitPatterns} objects that this
 * matcher is initialized, then a corresponding list of {@link SplitMatch} is
 * returned. Generally, the returned list will contain 0 or 1 SplitMatch
 * objects. However, given that unicode sets are allowed in the patterns it is
 * impossible to ensure that more than one pattern isn't matched. A returned
 * SplitMatch provides the character offsets for the input word where it should
 * be split into multiple tokens.
 * <p>
 * This class is initialized with an instance of SplitPatterns which is
 * essentially a suffix tree (or trie) whose structure allows for simple cycles
 * (i.e. a node can cycle back to itself). Matching is performed at the
 * character level and we only count a match if the entire word matches an entry
 * in the trie. That is to say, we will not find any partial matches of the
 * entries in the trie, nor will we match an entry in the trie to part of the
 * input word.
 * <p>
 * Given the SplitPatterns (creating this is the hard part) the matching
 * algorithm is very simple. We start with the first character of the word and
 * find all the children nodes of the trie's top node for which that character
 * belongs. We put these child nodes in a list we call 'matches' which is the
 * working set of nodes that may lead to a match. The matches will initially
 * consist of zero or one {@link CharacterNode} (if there is a character node
 * for that character or not) and zero or more {@link UnicodeSetNode}s. There
 * may, for example, be many {@link UnicodeSet}s that include a given character.
 * The initial list of matches for the first character of the input word
 * constrains the search space to only these nodes and their children. We then
 * proceed to the next character, loop over the list of matches, examine their
 * child nodes and keep those child nodes for which the character matches. These
 * matching child nodes become the list of matches and the previous list of
 * matches is discarded. We repeat this process for each character until we have
 * looked at every character in the word or the list of matches is empty. If we
 * get to the end of the string, then we will return all the matches that
 * correspond to the end of an entry in the trie. This is determined by
 * examining the node for the match and seeing if it has a pattern id associated
 * with it.
 * 
 * @author Philip Ogren
 * 
 */

public class SplitMatcher {

	private SplitPatterns patterns;

	public SplitMatcher(SplitPatterns patterns) {
		this.patterns = patterns;
	}

	public List<SplitMatch> match(String word) {
		// a running list of all of the possible matches at this point.
		List<SplitMatch> matches = new ArrayList<SplitMatch>();
		// we will start off with a single match using the top node.
		matches.add(new SplitMatch(patterns.getTopNode()));

		// we are going to loop over code points rather than characters just to
		// be on the safe side
		for (int i = 0; i < word.length();) {
			int codePoint = word.codePointAt(i);

			List<SplitMatch> nextMatches = new ArrayList<SplitMatch>();

			for (SplitMatch match : matches) {
				List<Node> nodes = match.getNode().getChildren(codePoint);
				for (Node node : nodes) {
					SplitMatch nextMatch;

					// it is possible for a node to point back to itself (i.e.
					// one of it's children is itself - this is how we
					// accomplish the allowance for repeated characters via '*'
					// and '+'). If we have cycled back, then we do not want
					// there to be another split for the "child"node if this
					// node has splits. For example, if our pattern was "b|a+"
					// and we match "baaa" we do not want splits between the 2nd
					// and 3rd character or between the 3rd and 4th character.
					// We only want a split between the 1st and 2nd character.
					if (match.getNode() == node) {
						nextMatch = new SplitMatch(node, match.getSplitIndexes());
					} else {
						nextMatch = new SplitMatch(node, match.getSplitIndexes(), match.getNode(), i);
					}
					nextMatches.add(nextMatch);
				}
			}

			matches = nextMatches;
			if (matches.size() == 0) {
				break;
			}
			// this will generally be equivalent to i++ except for those code
			// points that take more than one char.
			i += Character.charCount(codePoint);
		}

		// at this point we have visited every character in the word - or we
		// have no more matches in our working list of matches. We will only
		// return matches that are complete - i.e. they have a pattern id for
		// the corresponding node.
		List<SplitMatch> returnValues = new ArrayList<SplitMatch>();
		for (SplitMatch match : matches) {
			if (match.isComplete()) {
				returnValues.add(match);
			}
		}

		return returnValues;
	}
}
