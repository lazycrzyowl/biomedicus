/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.token;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.token.split.SplitMatch;
import edu.umn.biomedicus.token.split.SplitMatcher;
import edu.umn.biomedicus.token.split.SplitPatternException;
import edu.umn.biomedicus.token.split.SplitPatterns;
import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class TokenSplitter extends JCasAnnotator_ImplBase {

	public static final String PARAM_SPLIT_PATTERNS_PATH = ConfigurationParameterFactory
	                                .createConfigurationParameterName(TokenSplitter.class, "splitPatternsPath");

	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/token/split/split_patterns.txt")
	private String splitPatternsPath;

	private SplitPatterns patterns;
	private SplitMatcher matcher;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		try {

			InputStream inputStream = this.getClass().getResourceAsStream(splitPatternsPath);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String[] patternStrings;
			patternStrings = FileUtil.loadListOfStrings(reader);
			patterns = new SplitPatterns(patternStrings);
			matcher = new SplitMatcher(patterns);
		} catch (IOException e) {
			throw new ResourceInitializationException(e);
		} catch (SplitPatternException e) {
			throw new ResourceInitializationException(e);
		}
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		List<Token> removeTokens = new ArrayList<Token>();
		List<int[]> addTokens = new ArrayList<int[]>();
		for (Token token : JCasUtil.select(jCas, Token.class)) {
			List<SplitMatch> splitMatches = matcher.match(token.getCoveredText());
			if (splitMatches.size() > 0) {
				SplitMatch splitMatch = splitMatches.get(0);
				int[] splits = splitMatch.getSplits();

				boolean isTokenSplit = false;
				int begin = token.getBegin();
				for (int split : splits) {
					if (split != 0) {
						int end = token.getBegin() + split;
						addTokens.add(new int[] { begin, end });
						begin = end;
						isTokenSplit = true;
					}
				}
				if (isTokenSplit) {
					int end = token.getEnd();
					addTokens.add(new int[] { begin, end });

					removeTokens.add(token);
				}
			}
		}
		for (Token removeToken : removeTokens) {
			removeToken.removeFromIndexes();
		}
		for (int[] tokenOffsets : addTokens) {
			new Token(jCas, tokenOffsets[0], tokenOffsets[1]).addToIndexes();
		}
	}
}
