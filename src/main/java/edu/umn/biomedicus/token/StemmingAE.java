/* 
 Copyright 2011-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Token;

/**
 * This class assigns the stemmed version of a word to the token's "stem"
 * attribute.
 * 
 * @author Robert Bill
 */
public class StemmingAE extends JCasAnnotator_ImplBase {

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		// TODO add parameter for stemming algorithm after more are added.
		// currently, only the porter stemmer is added.
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		PorterStemmer stemmer = new PorterStemmer();
		for (Token token : JCasUtil.select(jCas, Token.class)) {
			String tokenText = token.getCoveredText().toLowerCase();
			stemmer.add(tokenText.toCharArray(), tokenText.length());
			stemmer.stem();
			token.setStem(stemmer.toString());
		}
	}
}
