/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.uima.resource.ResourceProcessException;
import org.uimafit.component.Resource_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;

/**
 * This class is a very simple means to get a connection to the biomedicus
 * database (currently hsqldb only).
 * 
 * @author Robert Bill
 * 
 */
public class BiomedicusDBResource_Impl extends Resource_ImplBase {
	public static final String PARAM_DATABASE_HOME = "databaseHome";
	@ConfigurationParameter(name = PARAM_DATABASE_HOME, defaultValue = "./")
	private String databaseHome;

	protected Connection primaryConnection = null;
	protected Statement statement = null;
	protected String dbPath = null;

	@Override
	public void afterResourcesInitialized() {
		// Get initial connection to hsqldb so subsequent requests are faster.
		try {
			Class.forName("org.hsqldb.jdbcDriver");
			dbPath = new File(databaseHome, "resources/database/biomedicusDB").toString();
			primaryConnection = getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() throws ResourceProcessException {
		Connection con = null;
		try {
			con = DriverManager.getConnection("jdbc:hsqldb:file:" + dbPath, "sa", "");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ResourceProcessException();
		}
		return con;
	}

	public String getBiomedicusHome() {
		return databaseHome;
	}

	public void destroy() {
		try {
			this.statement.close();
			this.primaryConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
