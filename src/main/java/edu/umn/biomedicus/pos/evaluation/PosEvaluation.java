/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.pos.evaluation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.kohsuke.args4j.Option;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;
import edu.umn.biomedicus.evaluation.EngineFactory;
import edu.umn.biomedicus.evaluation.Evaluation;
import edu.umn.biomedicus.evaluation.EvaluationFactory;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class PosEvaluation {

	public static class Options extends Options_ImplBase {

		public static final String OUTPUT_DIRECTORY_NAME_USAGE = "A good name of the output directory will include the task that the tested engine performs "
		                                + "(e.g. 'pos'), the corpus name that the experiment was run on ('genia'), the learner that is being used "
		                                + "(e.g. 'maxent'), and any configuration parameters of interest.  For example, a reasonably good directory "
		                                + "might be: <my-experiment-home>/pos/genia/maxent "
		                                + "\n"
		                                + "The output directory will be used to write output files and directories.  There will generally be one folder corresponding to "
		                                + "each fold in the corpus and a holdout directory each of which will contain a 'model' directory and an 'evaluation' directory. "
		                                + "cross-validation results across all of the folds will be aggregated into an 'evaluation' directory which will sit directly under "
		                                + "the output directory.";
		@Option(name = "-o", aliases = "--outputDirectory", usage = OUTPUT_DIRECTORY_NAME_USAGE, required = true)
		public String outputDirectoryName;

		@Option(name = "-c", aliases = "--corpusFactoryClassName", usage = "specify the class name of the corpus factory", required = true)
		public String corpusFactoryClassName;

		public static final String DATA_WRITER_FACTORY_CLASS_NAME_USAGE = "specify the class name of the data writer factory to use."
		                                + "Valid value for this option include: \n"
		                                + "org.cleartk.classifier.opennlp.DefaultMaxentDataWriterFactory\n"
		                                + "org.cleartk.classifier.mallet.DefaultMalletCRFDataWriterFactory\n"
		                                + "org.cleartk.classifier.libsvm.DefaultMultiClassLIBSVMDataWriterFactory\n"
		                                + "org.cleartk.classifier.mallet.DefaultMalletDataWriterFactory\n"
		                                + "org.cleartk.classifier.svmlight.DefaultOVASVMlightDataWriterFactory";

		@Option(name = "-dwf", aliases = "--dataWriterFactory", usage = DATA_WRITER_FACTORY_CLASS_NAME_USAGE, required = true)
		public String dataWriterFactoryClassName;

		@Option(name = "-z", aliases = "--compressFeatures", usage = "specify whether or not to compress the feature names in the training data.  This will make the files much smaller but more difficult to read when debugging.")
		public String compressFeatures = "true";

		@Option(name = "-fc", aliases = "--featureCutoff", usage = "specify the frequency count cutoff of features to be used in the training data.")
		public int featureCutoff = 5;

		@Option(name = "-t", aliases = "--trainingParams", usage = "specify the parameter values to pass to the learner.  These are the same arguments that you would pass if you were to invoke the trainer directly.", multiValued = true)
		public List<String> trainingParams = new ArrayList<String>();

		@Option(name = "-m", aliases = "--buildCorpusModel", usage = "set this option to 'true' if you just want to build a model that uses the entire corpus as training data.  This is what you should run when you are done evaluating the part-of-speech tagger and you have picked a learner and learning parameters and you want to build a model that will be used for applications.  ")
		public String buildCorpusModel = "false";

	}

	public static void main(String[] args) throws Exception {
		Options options = new Options();
		options.parseArgument(args);

		File outputDirectory = new File(options.outputDirectoryName);
		CorpusFactory corpusFactory = ComponentFactory.createCorpusFactory(options.corpusFactoryClassName);
		boolean compressFeatures = Boolean.parseBoolean(options.compressFeatures);
		EngineFactory engineFactory = new PosEngineFactory(options.dataWriterFactoryClassName, compressFeatures,
		                                options.featureCutoff);
		String[] trainingParams = options.trainingParams.toArray(new String[options.trainingParams.size()]);

		boolean buildCorpusModel = Boolean.parseBoolean(options.buildCorpusModel);
		if (buildCorpusModel) {
			Evaluation.buildCorpusModel(outputDirectory, corpusFactory, engineFactory, trainingParams);
		} else {
			EvaluationFactory evaluationFactory = new PosEvaluationFactory();
			Evaluation.runCrossValidation(outputDirectory, corpusFactory, engineFactory, evaluationFactory,
			                                trainingParams);
			Evaluation.runHoldoutEvaluation(outputDirectory, corpusFactory, engineFactory, evaluationFactory,
			                                trainingParams);
		}
	}
}
