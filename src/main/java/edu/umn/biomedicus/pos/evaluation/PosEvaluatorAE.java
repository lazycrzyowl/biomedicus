/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.pos.evaluation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.evaluation.ConfusionMatrix;
import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Philip Ogren
 * 
 */

@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class PosEvaluatorAE extends JCasAnnotator_ImplBase {

	public static final String OUTPUT_FILE_NAME = "pos-evaluation.txt";
	public static final String ERRORS_FILE_NAME = "errors.txt";
	public static final String CONFUSION_FILE_NAME = "confusion.ser";

	public static final String PARAM_OUTPUT_DIRECTORY = ConfigurationParameterFactory.createConfigurationParameterName(
	                                PosEvaluatorAE.class, "outputDirectory");
	@ConfigurationParameter(mandatory = true)
	private File outputDirectory;

	PrintStream out;
	PrintStream errors;

	ConfusionMatrix confusions = new ConfusionMatrix();

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		if (!outputDirectory.exists())
			outputDirectory.mkdirs();

		try {
			out = new PrintStream(new File(outputDirectory, OUTPUT_FILE_NAME));
			errors = new PrintStream(new File(outputDirectory, ERRORS_FILE_NAME));
		} catch (FileNotFoundException fnfe) {
			throw new ResourceInitializationException(fnfe);
		}

	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas goldView = jCas.getView(ViewNames.GOLD_VIEW);
			JCas systemView = jCas.getView(ViewNames.SYSTEM_VIEW);

			List<Token> goldTokens = (List<Token>) JCasUtil.select(goldView, Token.class);
			List<Token> systemTokens = (List<Token>) JCasUtil.select(systemView, Token.class);
			if (goldTokens.size() != systemTokens.size()) {
				throw new RuntimeException(
				                                "the number of gold tokens does not equal the number of system tokens.  They must be the same.  gold="
				                                                                + goldTokens.size() + ", system="
				                                                                + systemTokens.size());
			}

			for (int i = 0; i < goldTokens.size(); i++) {
				Token goldToken = goldTokens.get(i);
				Token systemToken = systemTokens.get(i);
				if (!goldToken.getCoveredText().equals(systemToken.getCoveredText())) {
					throw new RuntimeException("the gold tokens and the system tokens must be identical: tokenIndex="
					                                + i + ", gold=" + goldToken.getCoveredText() + ", system="
					                                + systemToken.getCoveredText());
				}
				confusions.add(goldToken.getPos(), systemToken.getPos());
				if (!goldToken.getPos().equals(systemToken.getPos())) {
					printError(goldTokens, systemTokens, i);
				}
			}

		} catch (CASException ce) {
			throw new AnalysisEngineProcessException(ce);
		}
	}

	private void printError(List<Token> goldTokens, List<Token> systemTokens, int i) {
		int begin = Math.max(0, i - 2);
		int end = Math.min(goldTokens.size() - 1, i + 3);

		List<Token> goldContextTokens = goldTokens.subList(begin, end);
		List<Token> systemContextTokens = systemTokens.subList(begin, end);

		for (Token goldContextToken : goldContextTokens) {
			errors.print(goldContextToken.getCoveredText() + "/" + goldContextToken.getPos() + " ");
		}
		errors.println();
		for (Token systemContextToken : systemContextTokens) {
			errors.print(systemContextToken.getCoveredText() + "/" + systemContextToken.getPos() + " ");
		}
		errors.println();
		errors.println();

	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {

		printEvaluationResults(confusions, out);
		out.close();
		errors.close();
		try {
			ObjectOutputStream ooo = new ObjectOutputStream(new FileOutputStream(new File(outputDirectory,
			                                CONFUSION_FILE_NAME)));
			ooo.writeObject(confusions);
			ooo.close();
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}

	}

	public static void printEvaluationResults(ConfusionMatrix confusions, PrintStream out) {
		out.println("Part-of-speech tagging evaluation");
		int correct = confusions.getCorrect();
		int wrong = confusions.getWrong();
		int total = correct + wrong;
		float accuracy = (float) correct / total;
		out.println("total\t" + total);
		out.println("correct\t" + correct);
		out.println("wrong\t" + wrong);
		out.println("accuracy:\t" + accuracy);

		out.println();
		out.println("Confusion Matrix");
		confusions.print(out);
	}
}
