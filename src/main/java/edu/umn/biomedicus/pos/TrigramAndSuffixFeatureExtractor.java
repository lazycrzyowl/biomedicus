/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.pos;

import java.util.ArrayDeque;
import java.util.ArrayList;

import edu.umn.biomedicus.type.Token;

/**
 * This class extracts the feature list required for the trigram plus suffix
 * max-ent part-of-speech tagger.
 * 
 * @author Robert Bill
 */
public class TrigramAndSuffixFeatureExtractor {
	private ArrayDeque<Token> history = new ArrayDeque<Token>(3);
	int suffixSize = 3;

	public TrigramAndSuffixFeatureExtractor(int suffixSize) {
		this.suffixSize = suffixSize;
	}

	public String[] getTrigramFeatures(Token token) {
		// word, wordSuffix, pos-1, word-1Suffix, pos-2, word-2Suffix, pos-3,
		// word-3Suffix, suffix, suffix-1, suffix-2, lexAccessPos

		ArrayList<String> features = new ArrayList<String>(8);

		// add the token
		features.add(token.getCoveredText());
		features.add(" ");
		features.add(getSuffix(token.getCoveredText()));
		features.add(" ");

		for (Token preceding : history) {
			String preText = preceding.getCoveredText();
			String prePos = preceding.getPos();
			features.add(prePos);
			features.add(" ");
			features.add(getSuffix(preText));
			features.add(" ");
		}

		history.add(token);
		if (history.size() > 3)
			history.removeFirst();

		String[] featureSet = new String[features.size()];
		return features.toArray(featureSet);
	}

	private String getSuffix(String word) {
		if (word.length() < suffixSize)
			return word;
		else
			return word.substring(word.length() - suffixSize);
	}
}