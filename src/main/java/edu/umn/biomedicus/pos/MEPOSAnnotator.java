/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.pos;

import java.io.File;
import java.io.IOException;

import opennlp.model.GenericModelReader;
import opennlp.model.MaxentModel;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Token;

/**
 * A simple maximum entropy part-of-speech annotator. (A "keep it simple"
 * annotator).
 * 
 * @author Robert Bill
 * 
 */
public class MEPOSAnnotator extends JCasAnnotator_ImplBase {

	private MaxentModel model;
	@ConfigurationParameter(mandatory = false, defaultValue = "posMEModel.bin")
	private String modelFileName;

	// AcronymFeatureExtractor afe = new AcronymFeatureExtractor();

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		String path = context.getDataPath();
		try {
			model = new GenericModelReader(new File(path + "/resources/models/part-of-speech/", modelFileName))
			                                .getModel();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		TrigramAndSuffixFeatureExtractor fe = new TrigramAndSuffixFeatureExtractor(3);

		for (Token token : JCasUtil.select(jCas, Token.class)) {
			String[] predicates = fe.getTrigramFeatures(token);
			double[] outcomes = model.eval(predicates);
			String tag = model.getBestOutcome(outcomes);

			token.setPos(tag);
		}
	}
}