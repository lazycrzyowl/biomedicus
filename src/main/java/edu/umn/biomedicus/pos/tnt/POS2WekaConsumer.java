package edu.umn.biomedicus.pos.tnt;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.HashMap;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Token;

/**
 * Just an experiment at this point. Ignore this class. The is a simple starting
 * point/template for future feature extraction (FESL) experiments.
 * 
 */
public class POS2WekaConsumer extends JCasAnnotator_ImplBase {
	public static final String PARAM_PLACEHOLDER = "modelFileName";
	@ConfigurationParameter(name = PARAM_PLACEHOLDER, mandatory = false, defaultValue = "placeHolder")
	private String placeHolder;
	private HashMap<String, String> tagCollector = new HashMap<String, String>();

	protected BufferedWriter trainingFile;
	public int suffixSize = 3;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		try {
			trainingFile = new BufferedWriter(new FileWriter("pos.dat"));
			// TODO: move to weka arffwriter class
                        // // trainingFile.write("@RELATION pos");
			// trainingFile.newLine();
			// trainingFile.write("@ATTRIBUTE word string");
			// trainingFile.newLine();
			// trainingFile.write("@ATTRIBUTE posMinus1 string");
			// trainingFile.newLine();
			// trainingFile.write("@ATTRIBUTE suffixMinus1 string");
			// trainingFile.newLine();
			// trainingFile.write("@ATTRIBUTE posMonus2 string");
			// trainingFile.newLine();
			// trainingFile.write("@ATTRIBUTE suffixMinus2 string");
			// trainingFile.newLine();
			// trainingFile.write("@ATTRIBUTE pos string");
			// trainingFile.newLine();
			// trainingFile.write("@DATA");
			// trainingFile.newLine();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		// word, wordSuffix, pos-1, word-1Suffix, pos-2, word-2Suffix, pos-3,
		// word-3Suffix, suffix,
		// suffix-1, suffix-2
		ArrayDeque<Token> history = new ArrayDeque(3);
		// add empty tokens for begining of string where there is no history
		// history.add(new Token(jCas, 0, 0));
		// history.add(new Token(jCas, 0, 0));
		// history.add(new Token(jCas, 0, 0));

		for (Token token : JCasUtil.select(jCas, Token.class)) {
			tagCollector.put(token.getPos(), null);
			StringBuilder sample = new StringBuilder();

			// add the token
			sample.append(token.getCoveredText());
			sample.append(" ");
			sample.append(getSuffix(token.getCoveredText()));
			sample.append(" ");

			for (Token preceding : history) {
				String preText = preceding.getCoveredText();
				String prePos = preceding.getPos();
				sample.append(prePos);
				sample.append(" ");
				sample.append(getSuffix(preText));
				sample.append(" ");
			}

			// add gold standard pos info
			sample.append(token.getPos());
			try {
				trainingFile.write(sample.toString());
				trainingFile.newLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			history.add(token);
			if (history.size() > 3)
				history.removeFirst();
		}
	}

	private String getSuffix(String word) {
		if (word.length() < suffixSize)
			return word;
		else
			return word.substring(word.length() - suffixSize);
	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		try {
			trainingFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
