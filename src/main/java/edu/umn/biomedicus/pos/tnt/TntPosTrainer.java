package edu.umn.biomedicus.pos.tnt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

/**
 * This class trains the Biomedicus TNT part-of-speech tagger. You must include
 * this class in a pipeline that uses a collection reader with a "GOLD_VIEW"
 * containing POS information. For example:
 * 
 * <pre>
 * CollectionReader reader = someCorpusFactory.createTrainReader();
 * AnalysisEngineDescription tntTagger = AnalysisEngineFactory.createPrimitiveDescription(TntPosTrainer.class, typeSystem);
 * builder.add(tntTagger, CAS.NAME_DEFAULT_SOFA, ViewNames.GOLD_VIEW);
 * SimplePipeline.runPipeline(reader, builder.createAggregate());
 * </pre>
 * 
 * You can accumulate data from multiple collection readers if you pass a value
 * to the TntPosTrainer.class for ACCUMULATE_DATA. For example:
 * 
 * <pre>
 * tntTagger = AnalysisEngineFactory.createPrimitiveDescription(TntPosTrainer.class,
 *                                 TntPosTrainer.PARAM_ACCUMULATION_MODE, TntPosTrainer.ACCRETE_DATA);
 * </pre>
 * 
 * @author Robert Bill
 * 
 */
public class TntPosTrainer extends JCasAnnotator_ImplBase {
	public static final String ACCRETE_DATA = "ACCRETE_DATA";

	public static final String REPLACE_DATA = "REPLACE_DATA";

	public static final String PARAM_ACCUMULATION_MODE = ConfigurationParameterFactory
	                                .createConfigurationParameterName(TntPosTrainer.class, "accumulationMode");

	@ConfigurationParameter(name = "PARAM_ACCUMULATION_MODE")
	private String accumulationMode = REPLACE_DATA;

	protected ConditionalFreqStore cf;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		accumulationMode = (String) context.getConfigParameterValue(PARAM_ACCUMULATION_MODE);
		// If "accrete" mode, then get existing data. Otherwise, create a new
		// data store.
		if (accumulationMode == TntPosTrainer.ACCRETE_DATA) {
			String modelPath = context.getDataPath() + "/resources/models/part-of-speech/";
			FileInputStream fis = null;
			ObjectInputStream ois = null;
			try {
				fis = new FileInputStream(modelPath + "genia-tnt-model.ser");
				ois = new ObjectInputStream(fis);
				cf = (ConditionalFreqStore) ois.readObject();
			} catch (FileNotFoundException ex) {
				System.out.println("Unable to open the genia-tnt-model.ser file to path: " + modelPath);
				ex.printStackTrace();
			} catch (IOException ex) {
				System.out.println("genia-tnt-model.ser file was opened, but unable to read file.");
				ex.printStackTrace();
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
			}
		} else {
			cf = new ConditionalFreqStore(false);
			// Adding the SPECIALIST lexicon data (in the next line of code)
			// into the word frequency score reduces accuracy. Leave out until I
			// figure out why this happens.
			// cf.buildBaseDictionary(pathToLexAccess);
		}
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		for (Sentence sentence : JCasUtil.select(jCas, Sentence.class)) {
			cf.newSentence(); // Set to beginning of sentence state
			for (Token token : JCasUtil.selectCovered(Token.class, sentence)) {
				cf.feedTape(token.getCoveredText(), token.getPos());
			}
		}
	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		cf.calculateLambdas();
		// @TODO: cf.buildTrellis();
		// @TODO: change this to be a registered resource
		String modelPath = this.getContext().getDataPath() + "/resources/models/part-of-speech/";
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
			fos = new FileOutputStream(modelPath + "genia-tnt-model.ser");
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to write the genia-tnt-model.ser file to path: " + modelPath);
			ex.printStackTrace();
		}
		try {
			oos = new ObjectOutputStream(fos);
			oos.writeObject(cf);
		} catch (IOException ex) {
			System.out.println("genia-tnt-model.ser file was opened, but unable to write to file.");
			ex.printStackTrace();
		}
	}
}
