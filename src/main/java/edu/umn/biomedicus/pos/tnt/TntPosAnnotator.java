/* 
 Copyright 2011-12 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.pos.tnt;

import java.io.InputStream;
import java.util.Collection;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

public class TntPosAnnotator extends JCasAnnotator_ImplBase {

	public static final String PARAM_MODEL_FILE = "modelFile";
	@ConfigurationParameter(name = PARAM_MODEL_FILE, mandatory = false, defaultValue = "/edu/umn/biomedicus/pos/genia-tnt-model.ser")
	private String modelFile;

	protected ConditionalFreqStore cf;
	private TagSearch search;

	@Override
	public void initialize(UimaContext uimaContext) throws ResourceInitializationException {
		super.initialize(uimaContext);

		// Load frequency data
		InputStream modelInputStream = this.getClass().getResourceAsStream(modelFile);
		search = new TagSearch(modelInputStream, 100);
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		Collection<Sentence> sents = JCasUtil.select(jCas, Sentence.class);
		Collection<Token> tokens = JCasUtil.select(jCas, Token.class);

		// Make sure there are sentence annotations in the JCas
		if (sents.size() == 0) {
			System.err.println("POS Tagging failed:  No annotations of 'Sentence' exist in the JCas object. Sentence annotations are required!");
			throw new AnalysisEngineProcessException();
		} else if (tokens.size() == 0) {
			System.err.println("POS Taggin failed: No annotations of 'Token' exist in the JCas object. Token annotations are required!");
			throw new AnalysisEngineProcessException();
		}

		// Loop through each sentence
		for (Sentence sentence : sents) {
			// tell search class that we started a new sentence
			search.newSentence();

			// loop through the tokens of the sentence
			for (Token token : JCasUtil.selectCovered(Token.class, sentence)) {
				search.add(token);
			}
			// bs.endSentence();
		}
	}
}
