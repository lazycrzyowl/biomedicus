/* 
 Copyright 2013 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.corpora.acronym;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.factory.CollectionReaderFactory;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.collectionreaders.XMICollectionReader;
import edu.umn.biomedicus.corpora.CorpusFactory_ImplBase;

/**
 * Factory for edu.umn.biomedicus.acronym corpus. The edu.umn.biomedicus.acronym corpus is available on the UMN
 * digital conservancy site.
 * 
 * @author Robert Bill
 * 
 */
public class AcronymFactory extends CorpusFactory_ImplBase {

	private String xmiDirectory = "resources/corpora/clinical/xmi";
	protected File namesDirectory = new File("resources/corpora/clinical/names");

	public CollectionReader createReader() throws ResourceInitializationException {
		String[] files = this.getFileListFromNamesFile(new File(namesDirectory, "acronyms.txt"));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public CollectionReader createTrainReader() throws ResourceInitializationException {
		String[] files = this.getFileListFromNamesFile(new File(namesDirectory, "train.txt"));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public CollectionReader createTestReader() throws ResourceInitializationException {
		String[] files = this.getFileListFromNamesFile(new File(namesDirectory, "test.txt"));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	private String getFoldFileName(int fold) {
		if (fold == 10) {
			return new File("fold10.txt").getPath();
		} else {
			return new File("fold0" + fold + ".txt").getPath();
		}
	}

	public CollectionReader createTrainReader(int fold) throws ResourceInitializationException {
		verifyFoldValue(fold);
		List<String> fileNamesList = new ArrayList<String>();
		for (int i = 1; i <= 10; i++) {
			if (i != fold) {
				fileNamesList.add(getFoldFileName(i));
			}
		}

		String[] fileNames = fileNamesList.toArray(new String[fileNamesList.size()]);
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, fileNames);
	}

	public CollectionReader createTestReader(int fold) throws ResourceInitializationException {
		verifyFoldValue(fold);
		String fileName = getFoldFileName(fold);
		String[] files = getFileListFromNamesFile(new File(namesDirectory, fileName));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public int numberOfFolds() {
		return 10;
	}

}
