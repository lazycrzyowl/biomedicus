/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.corpora.acronym;

import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.type.Acronym;
import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.UmlsConcept;
import gov.nih.nlm.nls.metamap.Ev;
import gov.nih.nlm.nls.metamap.MetaMapApi;
import gov.nih.nlm.nls.metamap.MetaMapApiImpl;
import gov.nih.nlm.nls.metamap.PCM;
import gov.nih.nlm.nls.metamap.Position;
import gov.nih.nlm.nls.metamap.Result;
import gov.nih.nlm.nls.metamap.Utterance;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import org.uimafit.component.initialize.ConfigurationParameterInitializer;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;

/*
 * Class for internal use at UMN. Not useful for others.
 * 
 * This is a collection reader that populates the JCas with 
 * acronyms and the horizon of tokens surrounding them.
 * 
 *  * @author Robert Bill (2012)
 */
public class AcronymGoldReader extends CollectionReader_ImplBase {
	private class AcronymStore {
		int begin;
		int end;
		String shortForm;
		String longForm;

		public AcronymStore(String shortForm, int begin, int end, String longForm) {
			this.begin = begin;
			this.end = end;
			this.shortForm = shortForm;
			this.longForm = longForm;
		}

		@SuppressWarnings("unused")
		public String getShortForm() {
			return shortForm;
		}

		public int getBegin() {
			return begin;
		}

		public int getEnd() {
			return end;
		}

		public String getLongForm() {
			return longForm;
		}
	}

	public static final String PARAM_ACRONYM_CORPUS_FILE = ConfigurationParameterFactory
	                                .createConfigurationParameterName(AcronymGoldReader.class, "acronymCorpusFile");
	@ConfigurationParameter(description = "names the file that is the edu.umn.biomedicus.acronym corpus to be loaded.", mandatory = true)
	private File acronymCorpusFile;

	private String metaMapConfig = "-a -I --negex -y -r 900";
	MetaMapApi api;
	private HashMap<String, Object> mmCache = new HashMap<String, Object>();
	int currentLine = 0;
	int totalLines = 0;
	String currentAcronym = "";

	@Override
	public void initialize() throws ResourceInitializationException {
		super.initialize();
		ConfigurationParameterInitializer.initialize(this, getUimaContext());

		api = new MetaMapApiImpl();
		api.setOptions(metaMapConfig);
	}

	@Override
	public void getNext(CAS cas) throws IOException, CollectionException {
		JCas annotationsView;
		try {
			annotationsView = cas.getJCas().createView(ViewNames.GOLD_VIEW);
		} catch (CASException e) {
			e.printStackTrace();
			throw new CollectionException(e);
		}

		// Temporarily store acronyms in array because can only set jcas
		// document text 1 time.
		ArrayList<AcronymStore> acronymCollector = new ArrayList<AcronymStore>();

		String[] text = FileUtil.loadListOfStrings(acronymCorpusFile);
		totalLines = text.length;
		StringBuilder document = new StringBuilder();

		// collect all acronyms
		int lineEnd = 0;
		for (int i = currentLine; i < text.length; i++) {
			// line format: "edu.umn.biomedicus.acronym expansion"|"field in note"|"window"
			String[] data = text[currentLine].split("\\|");
			String shortForm = data[0];
			if (currentAcronym.equals(shortForm)) {
				currentLine++;
				String longForm = data[1];
				String actualText = data[2];
				int begin = Integer.valueOf(data[3]);
				int end = Integer.valueOf(data[4]);
				String noteSection = data[5];
				String srcText = data[6];
				AcronymStore nym = new AcronymStore(actualText, begin, end, longForm);
				acronymCollector.add(nym);
				int workingIndex = document.length();
				document.append(srcText).append("\n");
				lineEnd = document.length();
			} else {
				currentAcronym = shortForm;
				break;
			}
		}
		annotationsView.setDocumentText(document.toString());

		// put acronyms in CAS
		for (AcronymStore nym : acronymCollector) {
			createAcronymAnnotation(annotationsView, nym);
		}

		// ViewURIUtil.setURI(cas, currentAcronym);
	}

	public void createAcronymAnnotation(JCas jcas, AcronymStore nym) {
		Acronym acr = new Acronym(jcas, nym.getBegin(), nym.getEnd());
		acr.setSource("UMN Acronym Lexicon");
		acr.setExpansion(nym.getLongForm());
		acr.addToIndexes();
	}

	public void createUmlsConceptAnnotation(JCas jcas, String sense, int begin, int end) throws Exception {
		List<Result> resultList = null;
		if (mmCache.containsKey(sense)) {
			List<Result> list = (List<Result>) mmCache.get(sense);
			resultList = list;
		} else {
			resultList = api.processCitationsFromString(sense);
			mmCache.put(sense, resultList);
		}
		for (Result result : resultList) {
			for (Utterance utterance : result.getUtteranceList()) {
				for (PCM pcm : utterance.getPCMList()) {
					ArrayList<Cui> cuiList = new ArrayList<Cui>();
					for (Ev ev : pcm.getCandidates()) {
						for (@SuppressWarnings("unused")
						Position position : ev.getPositionalInfo()) {
							Cui cui = new Cui(jcas, begin, end);
							cui.setCuiId(ev.getConceptId());
							cui.setScore(Math.abs(ev.getScore()));
							cui.setConceptName(ev.getConceptName());

							// add semantic types
							List<String> semTypesList = ev.getSemanticTypes();
							StringArray semTypes = new StringArray(jcas, semTypesList.size());
							semTypes.copyFromArray(semTypesList.toArray(new String[semTypes.size()]), 0, 0,
							                                semTypes.size());
							cui.setSemanticType(semTypes);
							cuiList.add(cui);
						}
					}

					UmlsConcept concept = new UmlsConcept(jcas, begin, end);
					FSArray cuis = new FSArray(jcas, cuiList.size());
					for (int i = 0; i < cuis.size(); i++) {
						cuis.set(i, cuiList.get(i));
					}
					concept.setCuis(cuis);
					concept.addToIndexes();
				}
			}
		}
	}

	@Override
	public void close() throws IOException {
		// TODO
	}

	@Override
	public Progress[] getProgress() {
		return new Progress[] { new ProgressImpl(currentLine, 37500, Progress.ENTITIES) };
	}

	/***
	 * The hasNext() determines if the entire source file has be fully read.
	 */
	@Override
	public boolean hasNext() throws IOException, CollectionException {
		if (totalLines == 0) {
			return true;
		} else if ((totalLines - currentLine) > 0) {
			return true;
		} else {
			return false;
		}
	}
}
