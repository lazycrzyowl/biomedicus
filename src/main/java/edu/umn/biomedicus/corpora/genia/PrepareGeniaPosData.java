/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.corpora.genia;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class PrepareGeniaPosData implements org.uimafit.component.xwriter.XWriterFileNamer {

	public static void main(String[] args) throws Exception {

		CollectionReader reader = CollectionReaderFactory.createCollectionReader(GeniaPosGoldReader.class,
		                                ComponentFactory.getTypeSystem(), GeniaPosGoldReader.PARAM_GENIA_CORPUS_FILE,
		                                "resources/corpora/genia/GENIAcorpus3.02.pos.xml");

		AnalysisEngineDescription xWriter = AnalysisEngineFactory.createPrimitiveDescription(XWriter.class,
		                                ComponentFactory.getTypeSystem(), XWriter.PARAM_OUTPUT_DIRECTORY_NAME,
		                                "resources/corpora/genia/xmi", XWriter.PARAM_FILE_NAMER_CLASS_NAME,
		                                PrepareGeniaPosData.class.getName());

		SimplePipeline.runPipeline(reader, xWriter);
	}

	int i = 0;

	public String nameFile(JCas jCas) {
		// String uri = ViewURIUtil.getURI(jCas);
		if (++i % 100 == 0) {
			System.out.println("prepared " + i + " of 1999 files.");
		}
		return "not implemented yet.";// uri;
	}
}
