/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.corpora.genia;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.factory.CollectionReaderFactory;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.collectionreaders.XMICollectionReader;
import edu.umn.biomedicus.corpora.CorpusFactory_ImplBase;

/**
 * This class is a collection reader for the Genia biomedicus abstract corpus.
 * 
 * @author Philip Ogren
 * @author Robert Bill
 * 
 */

public class GeniaFactory extends CorpusFactory_ImplBase {

	private String xmiDirectory = "resources/corpora/genia/xmi";
	protected File namesDirectory = new File("resources/corpora/genia/names");

	public CollectionReader createReader() throws ResourceInitializationException {
		String[] files = getFileListFromNamesFile(new File(namesDirectory, "genia.txt"));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public CollectionReader createTrainReader() throws ResourceInitializationException {
		String[] files = getFileListFromNamesFile(new File(namesDirectory, "train.txt"));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public CollectionReader createTestReader() throws ResourceInitializationException {
		String[] files = getFileListFromNamesFile(new File(namesDirectory, "test.txt"));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public CollectionReader createSmallTestReader() throws ResourceInitializationException {
		String[] files = getFileListFromNamesFile(new File(namesDirectory, "small.txt"));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);

	}

	private String getFoldFileName(int fold) {
		if (fold == 10) {
			return new File(namesDirectory, "fold10.txt").getPath();
		} else {
			return new File(namesDirectory, "fold0" + fold + ".txt").getPath();
		}
	}

	public CollectionReader createTrainReader(int foldCount) throws ResourceInitializationException {
		verifyFoldValue(foldCount);
		List<String> fileNamesList = new ArrayList<String>();
		for (int i = 1; i <= 10; i++) {
			if (i != foldCount) {
				String[] fileNames = getFileListFromNamesFile(new File(getFoldFileName(i)));
				fileNamesList.addAll(Arrays.asList(fileNames));
			}
		}
		String[] files = fileNamesList.toArray(new String[fileNamesList.size()]);

		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public CollectionReader createTestReader(int fold) throws ResourceInitializationException {
		verifyFoldValue(fold);
		String[] files = getFileListFromNamesFile(new File(namesDirectory, getFoldFileName(fold)));
		return CollectionReaderFactory.createCollectionReader(XMICollectionReader.class,
		                                ComponentFactory.getTypeSystem(), XMICollectionReader.PARAM_INPUT_DIRECTORY,
		                                xmiDirectory, XMICollectionReader.PARAM_FILE_NAMES, files);
	}

	public int numberOfFolds() {
		return 10;
	}

}
