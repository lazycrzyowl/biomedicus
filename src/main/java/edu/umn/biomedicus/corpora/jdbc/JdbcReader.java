/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.corpora.jdbc;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.DocumentAnnotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import org.uimafit.component.initialize.ConfigurationParameterInitializer;

import edu.umn.biomedicus.type.MetaData;

/*
 * @author Yan Wang (2011)
 * This is a JDBC database collection reader that populates the document collection object with notes from the clinical database.
 *
 */
public class JdbcReader extends CollectionReader_ImplBase {

	public static final String PARAM_DBNAME = "DatabaseName";
	public static final String PARAM_ENCODING = "Encoding";
	public static final String PARAM_LANGUAGE = "Language";
	public static final String PARAM_DBADDRESS = "DatabaseServerIPAddress";
	public static final String PARAM_DBUSER = "DBUser";
	public static final String PARAM_DBPWD = "DBPassword";
	public static final String PARAM_DBQUERY = "DBQuery";

	private ArrayList<ArrayList<String>> mFiles;
	private String mLanguage;
	private int mCurrentIndex;
	private String mDBName;
	private String mDBIP;
	private String mDBUser;
	private String mDBPWD;
	private String mDBQuery;

	@SuppressWarnings("unused")
	private String mSource;
	@SuppressWarnings("unused")
	private String mEncoding;
	@SuppressWarnings("unused")
	private org.apache.uima.util.Logger logger = UIMAFramework.getLogger();

	private ResultSet rs;
	private File[] existingFiles;

	@Override
	public void initialize() throws ResourceInitializationException {
		super.initialize();
		ConfigurationParameterInitializer.initialize(this, getUimaContext());

		// TODO: must implement a FileFilter to make processes resumable
		// File outputFolder = new
		// File("output/700000");
		// existingFiles = outputFolder.listFiles();

		// Collect parameter values
		mEncoding = (String) getConfigParameterValue(PARAM_ENCODING);
		mLanguage = (String) getConfigParameterValue(PARAM_LANGUAGE);
		mDBName = (String) getConfigParameterValue(PARAM_DBNAME);
		mDBIP = (String) getConfigParameterValue(PARAM_DBADDRESS);
		mDBUser = (String) getConfigParameterValue(PARAM_DBUSER);
		mDBPWD = (String) getConfigParameterValue(PARAM_DBPWD);
		mDBQuery = (String) getConfigParameterValue(PARAM_DBQUERY);
		String url = "jdbc:mysql://" + mDBIP + "/" + mDBName + "?user=" + mDBUser + "&password=" + mDBPWD;

		// Set pointer to working index
		mCurrentIndex = 0;

		// Get the database connection and execute the query. The process method
		// uses the resultSet instance.
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url);
			Statement stmt = conn.createStatement();
			rs = stmt.executeQuery(mDBQuery);
			if (rs == null) {
				throw new RuntimeException("The SQL query for the JdbcReader did not return a result set.");
			}
		} catch (ClassNotFoundException e) {
			System.out.println("No driver found for JTDS - check class path.");
			throw new RuntimeException("Failed to load the specified database driver.");
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("There was an unrecoverable error executing the specified SQL statement");
		}
	}

	@Override
	public void getNext(CAS aCAS) throws IOException, CollectionException {
		JCas jcas;
		try {
			jcas = aCAS.getJCas();
		} catch (CASException e) {
			throw new CollectionException(e);
		}

		// variables for each DB fields of interest
		String id;
		String mrn;
		String providerID;
		String visitGUID;
		String documentTypeID;
		// String documentID; // not used now. Add later if someone needs it.
		String text;

		// TODO: parameterize the fields and mapping to jCas.
		// Warning: this is currently specific to a development database. These
		// fields will not work for others.
		try {
			// get all the fields from the datbase into local variables
			id = rs.getString(1);
			mrn = rs.getString(2);
			visitGUID = rs.getString(3);
			providerID = rs.getString(4);
			documentTypeID = rs.getString(5);
			// documentID = rs.getString(6); // not used now- what is this
			// field?
			text = rs.getString(7);

			// put the text/body of the note into the jcas document
			if (text == null) {
				// don't send nulls through pipeline when it is equivalent of an
				// empty string for our needs.
				System.out.println(text);
				text = "";
			}
			jcas.setDocumentText(text);

			// A note on views.
			// There should be a pattern to which view is used and at which
			// time.
			// For now, use the initial view for readers with no GOLD_VIEW
			// If this changes, create a new view here.
			// e.g., JCas sysView = jcas.createView("SYSTEM_VIEW");

			// add all the document information to metadata annotation
			MetaData metadata = new MetaData(jcas, 0, jcas.getDocumentText().length());
			metadata.setDocumentId(id); // it's the only "NOT NULL" field in db
			metadata.setMrn(mrn);
			metadata.setVisitGuId(visitGUID);
			metadata.setProviderGuId(providerID);
			metadata.setDocumentTypeId(documentTypeID);
			metadata.addToIndexes();

			// Also add the UriView that holds the value of the documentId
			// set URI to the record id because mrn is blank sometimes
			JCas uriView = aCAS.getJCas().createView("UriView");
			uriView.setSofaDataURI(id, id);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("SQL runtime exception.");
		} catch (CASException e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to add the 'UriView' to jcas.");
		}

		// set language if it was explicitly specified as a configuration
		// parameter
		if (mLanguage != null) {
			((DocumentAnnotation) jcas.getDocumentAnnotationFs()).setLanguage(mLanguage);
		}
	}

	@Override
	public void close() throws IOException {
		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Failed to close database connection.");
		}
	}

	@Override
	public Progress[] getProgress() {
		return new Progress[] { new ProgressImpl(mCurrentIndex, mFiles.size(), Progress.ENTITIES) };
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		try {
			while (rs.next()) {
				boolean next = true;
				String id = rs.getString(1) + ".xmi";
				// just to be fast, check the cached list
				for (File filename : existingFiles) {
					if (id.equals(filename.getName())) {
						next = false;
						break;
					}
				}
				if (next) {
					return next;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
