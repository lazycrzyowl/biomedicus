package edu.umn.biomedicus.corpora.util;

import gate.corpora.DocumentImpl;
import gate.corpora.DocumentStaxUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * 
 * Utility class for parsing GATE document files. Currently, this class is used
 * internally for annotated clinical notes.
 * 
 */

public class GateCollectionReader {

	private File[] fileStack;
	private int fileNum = 0;
	XMLInputFactory inputFactory = XMLInputFactory.newInstance();

	/**
	 * 
	 * @param gateFiles
	 *            A folder or one specific gate file passed in as a java.io.File
	 *            instance
	 */
	public GateCollectionReader(File gateFiles) {
		if (gateFiles.isFile()) {
			fileStack = new File[1];
			fileStack[0] = gateFiles;
		} else if (gateFiles.isDirectory()) {
			fileStack = gateFiles.listFiles();
			for (File f : gateFiles.listFiles()) {
				System.out.println("Found file: " + f.getName());
			}
		}
		inputFactory.setProperty("javax.xml.stream.isNamespaceAware", false);
	}

	public boolean hasNext() {
		System.out.println("HAS NEXT BEGINS HERE");
		if (fileNum < fileStack.length - 1) {
			return true;
		}
		System.out.println("HAS NEXT ENDS HERE");
		return false;
	}

	public DocumentImpl next() {
		System.out.println("GET NEXT BEGINS HERE");
		DocumentImpl doc = new DocumentImpl(); // The Gate Document
		try {
			System.out.println("Getting file for stack number: " + fileNum);
			System.out.println("file=> " + fileStack[fileNum].getName());
			InputStream input = new FileInputStream(fileStack[fileNum]);
			XMLStreamReader xsr = inputFactory.createXMLStreamReader(input);
			xsr.next();
			DocumentStaxUtils.readGateXmlDocument(xsr, doc);
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			throw new RuntimeException("Unable to find specified Gate file.");
		} catch (XMLStreamException xse) {
			xse.printStackTrace();
			throw new RuntimeException("Unable to read specified Gate file.");
		}
		fileNum++;
		System.out.println("GET NEXT ENDS HERE");
		return doc;
	}

	public String getFileName() {
		System.out.println(fileStack[fileNum].getName());
		return fileStack[fileNum].getName();
	}
}
