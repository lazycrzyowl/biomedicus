/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import java.util.HashSet;

/**
 * This class holds data for a concept unique identifier (CUI). There are times
 * when collection of edu.umn.biomedicus.acronym CUIs are needed without adding them to the CAS.
 * 
 * @author Robert Bill
 */
public class AcronymManifestCui {
	String cuiId = null;
	HashSet<String> semTypes = new HashSet<String>();

	public AcronymManifestCui(String cuiId) {
		this.cuiId = cuiId;
	}

	public String getCuiId() {
		return this.cuiId;
	}

	public String[] getSemTypes() {
		String[] results = new String[semTypes.size()];
		return this.semTypes.toArray(results);
	}

	public void addSemType(String semType) {
		if (!semTypes.contains(semType)) {
			semTypes.add(semType);
		}
	}
}
