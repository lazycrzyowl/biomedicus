/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

/**
 * AcronymManifest.java - A simple dictionary wrapper. This class reads a tab
 * (\t) delimited file and loads the first field of each row into a lookup table
 * (HashMap). The lookup only serves the purpose of exact match edu.umn.biomedicus.acronym
 * detection for those instances where a corpus predicts which exact matches
 * should always be annotated and expanded as acronyms.
 * 
 * @author Robert Bill
 * 
 */
public class AcronymManifest implements SharedResourceObject {
	static final long serialVersionUID = 1;

	// this is the internal lookup for members of the edu.umn.biomedicus.acronym manifest
	HashMap<String, AcronymManifestEntry> lookup = new HashMap<String, AcronymManifestEntry>();

	private String uri;

	public void load(DataResource acronymData) throws ResourceInitializationException {
		uri = acronymData.getUri().toString();
	}

	public String getUri() {
		return uri;
	}

	public AcronymManifest(InputStream manifestInputStream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(manifestInputStream));
		String[] acronyms = FileUtil.loadListOfStrings(reader);
		for (String line : acronyms) {
			String[] fields = line.split(",");

			// put row fields in named variables for clarity
			String acronym = normalizeString(fields[0]);
			int senseCount = Integer.valueOf(fields[1]);
			float probability = Float.valueOf(fields[2]);
			String cuiId = fields[3];
			String semanticType = fields[4];
			String expansion = fields[5].trim();

			// test for conditions that indicate skipping this row
			if (acronym.length() == 0 || acronym == null || cuiId.trim().length() == 0 || cuiId == null)
				continue;

			// If lookup mapping contains the edu.umn.biomedicus.acronym already, add data.
			// Otherwise, create a new entry in the lookup table
			if (lookup.containsKey(acronym)) {
				AcronymManifestEntry entry = lookup.get(acronym);
				entry.addExpansion(expansion, probability);
				entry.setSenseCount(senseCount);
				entry.addCui(cuiId, expansion, semanticType);
			} else {
				AcronymManifestEntry entry = new AcronymManifestEntry(acronym);
				entry.addExpansion(expansion, probability);
				entry.setSenseCount(senseCount);
				entry.addCui(cuiId, expansion, semanticType);
				lookup.put(acronym, entry);
			}
		}
	}

	public boolean containsKey(String key) {
		String normalizedKey = normalizeString((String) key);
		return lookup.containsKey(normalizedKey);
	}

	public String[] getKeys() {
		return (String[]) lookup.keySet().toArray();
	}

	public AcronymManifestEntry getEntry(String acronym) {
		return lookup.get(normalizeString(acronym));
	}

	private String normalizeString(String key) {
		StringBuilder normKey = new StringBuilder();
		for (char c : key.trim().toLowerCase().toCharArray()) {
			if ("!@#$%^&*()_+=-.,".indexOf(c) > -1)
				continue;
			else
				normKey.append(c);
		}
		return normKey.toString();
	}
}
