/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import gov.nih.nlm.nls.lexAccess.Api.LexAccessApi;
import gov.nih.nlm.nls.lexAccess.Api.LexAccessApiResult;

import java.sql.SQLException;

/**
 * 
 * @author Riea Moon
 * @author Robert Bill
 * 
 */
public class AcronymFeatureExtractor {
	/**
	 * This class trains the edu.umn.biomedicus.acronym detector.
	 */
	private LexAccessApi lex;

	// global private variables
	private AcronymManifest acronymManifest;

	public AcronymFeatureExtractor(AcronymManifest manifest, LexAccessApi lex) {
		this.lex = lex;
		this.acronymManifest = manifest;
	}

    public AcronymFeatureExtractor(AcronymManifest manifest) {
        this.acronymManifest = manifest;
    }

	public boolean isAllVowels(String candidate) {
		return candidate.matches("[aeiouAEIOU]+");
	}

	public boolean isAllConsonants(String candidate) {
		return candidate.matches("[^aeiouAEIOU]+");
	}

	public boolean hasConsonantSeparatedVowels(String candidate) {
		// More than one vowel, and the vowels are separated by consonant(s)
		candidate = candidate.toLowerCase();
		return candidate.matches(".*[aeiou]+[bcdfghjklmnpqrstvwxz]+[aeiou]+.*");
	}

	public float getV2L(String acronym) {
		// Assumption #1 is that the number of repeating vowels relative to
		// token length is predictive.
		int repeatingVowelCount = 0;
		boolean priorIsVowel = false;
		String vowels = "AEIOU";
		for (int i = 0; i < acronym.length(); i++) {
			if (vowels.contains(acronym.substring(i, i + 1))) {
				if (priorIsVowel) {
					repeatingVowelCount++;
				} else {
					priorIsVowel = true;
				}
			}
		}
		float v2l = new Float(repeatingVowelCount) / acronym.length();
		return v2l;
	}

	public String knownWord(String token) {
		// TODO: load additional dictionary of small words here.
		for (String wrd : new String[] { "I", "OF", "AND", "THIS", "HIM", "IF", "HE" }) {

			if (token.equals(wrd))
				return "knownWord";
		}
		LexAccessApiResult rs = null;
		try {
			rs = lex.GetLexRecords(token);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String resultString = rs.GetText();
		if ((resultString.indexOf("acronym_of") > -1) || (resultString.indexOf("abbreviation_of") > -1)) {
			return "knownAcronym";
		} else if (resultString.length() == 0) {
			return "unknown";
		} else {
			return "knownWord";
		}
	}

	public boolean inRieaSet(String token) {
		return acronymManifest.containsKey(token);
	}

	public boolean meetsMinimumLength(String token) {
		// currently 2
		if (token.length() > 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean meetsMaximumLength(String token) {
		// quick elimination crazy long tokens. Currently 7
		if (token.length() > 7) {
			return false;
		} else {
			return true;
		}
	}

	public boolean containsAlphaCharacters(String token) {
		// return true if token contains at least one alpha characters.
		// Otherwise, false
		return token.matches("[a-zA-Z]+");
	}

	public boolean allCaps(String token) {
		// return "allCaps" if every character is capitalized.
		// otherwise, return "mixedCase" or "lowercase"
		return token.matches("[A-Z|\\.|\\-|\\&]*");
	}

	public String charactersMatchHistory(String token) {
		// return "charactersMatchHistory" if characters in token match first
		// characters in horizon. horizon = 2 sequential capitalized preceding
		// or following tokens
		// Otherwise, return "noHistoryMatch"
		return "";
	}

	public boolean containsNumber(String token) {
		// return true if contains number. Otherwise, false
		return token.matches(".*[0-9].*");
	}

	public boolean internalPeriodOrDash(String token) {
		// return true if there's a period internal in the token and no other
		// punctuation is allowed.
		if (token.length() < 2) {
			return false;
		} else
			return token.substring(1, token.length() - 1).matches(".*[\\.\\-\\&].*");
	}
}