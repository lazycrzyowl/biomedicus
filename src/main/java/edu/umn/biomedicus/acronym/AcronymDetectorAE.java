/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import edu.umn.biomedicus.type.Acronym;
import opennlp.model.BinaryFileDataReader;
import opennlp.model.GenericModelReader;
import opennlp.model.MaxentModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.Span;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class detects an edu.umn.biomedicus.acronym in clinical notes. It uses rules plus a MaxEnt model trained on identifying features
 * from a clinical edu.umn.biomedicus.acronym data set.
 * @deprecated
 * @author Riea Moon
 * @author Robert Bill
 */
public class AcronymDetectorAE extends JCasAnnotator_ImplBase {

    public static final String PARAM_MODEL_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
            AcronymDetectorAE.class, "modelFile");
    public static final String PARAM_ACRONYM_MANIFEST_FILE = ConfigurationParameterFactory
            .createConfigurationParameterName(AcronymDetectorAE.class, "acronymCuiManifestFile");
    public static final String PARAM_LEXICON_CONFIG_FILE = ConfigurationParameterFactory
            .createConfigurationParameterName(AcronymDetectorAE.class, "lexiconConfigFile");
    @ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/acronym/acronymMEModel.bin")
    private String modelFile;
    @ConfigurationParameter(mandatory = false, defaultValue = "/edu/umn/biomedicus/acronym/AcronymManifest.csv")
    private String acronymCuiManifestFile;
    @ConfigurationParameter(mandatory = true)
    private String lexiconConfigFile;
    //private LexAccessApi lex = null;
    private MaxentModel model = null;
    // private MetaMapApiImpl api = null;
    private AcronymManifest manifest = null;
    private AcronymFeatureExtractor afe = null;
    private WhitespaceTokenizer tokenizer = WhitespaceTokenizer.INSTANCE;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
        // Initialize the Specialist Lexicon
        /*File lexConfigFile = new File(lexiconConfigFile);
        if (lexConfigFile.exists()) {
			lex = new LexAccessApi(lexConfigFile.toString());
		} else {
			System.err.println("Failed to locate the lexAccess.properties file in the $BIOMEDICUS_HOME/resources/config/ directory. "
			                                + "This is the configuration file for your local installation of lexAccess. It is required for edu.umn.biomedicus.acronym detection.");
			throw new ResourceInitializationException();
		}
        */
        // Get edu.umn.biomedicus.acronym manifest
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(acronymCuiManifestFile);
        try {
            manifest = new AcronymManifest(inputStream);
        } catch (IOException e) {
            System.err.println("Unable to initalize the edu.umn.biomedicus.acronym manifest. Probably check that the manifest file is at: "
                    + acronymCuiManifestFile);
            e.printStackTrace();
            throw new ResourceInitializationException();
        }
        afe = new AcronymFeatureExtractor(manifest); //, lex);
        try {
            File mf = new File(modelFile);
            model = new GenericModelReader(
                    new BinaryFileDataReader(this.getClass().getResourceAsStream(modelFile))).getModel(); //) (new File(modelFile)).getModel();
            if (model == null) {
                System.err.println("AcronymDetectorAE unable to load model file.");
                throw new ResourceInitializationException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {
        String documentText = jCas.getDocumentText();
        Span[] tokens = tokenizer.tokenizePos(documentText);
        for (Span span : tokens) {
            int begin = span.getStart();
            int end = span.getEnd();

            // only check strings that begin with a character or number for now.
            // So, skip non alphaNumerics
            while (!(Character.isLetter(documentText.charAt(begin)) || Character.isDigit(documentText.charAt(begin)))) {
                if (begin == documentText.length() - 1)
                    break;
                begin++;
            }

            String splitSymbols = ":(){}[]:;'|\"/<>@,";

            @SuppressWarnings("unused")
            Boolean foundPeriod = false;
            Boolean internalPeriod = false;
            for (int i = begin; i < end; i++) {
                char ch = documentText.charAt(i);
                // Boolean internalPunctuation = false;
                if (splitSymbols.indexOf(ch) > -1) {
                    end = i;
                    break;
                }
                if (ch == '.') {
                    foundPeriod = true;
                    if (i < end - 1) {
                        internalPeriod = true;
                    } else if (!internalPeriod) {
                        end = end - 1;
                    }
                }
            }
            // only check string up until an obvious split symbol
            while (splitSymbols.indexOf(documentText.charAt(end - 1)) > -1) {
                end--;
            }

            // Test the candidate string
            if (end <= begin) {
                end = begin + 1;
            }
            String candidate = documentText.substring(begin, end);
            if (isAcronym(candidate)) {
                // model incorrectly select "In" at the start of sentence, so
                // throw out for now
                String test = candidate.toLowerCase();
                if (test.equals("in") || test.equals("it") || test.equals("its"))
                    continue;
                // if it might be an edu.umn.biomedicus.acronym, check for compound words and for
                // ordinal numbers. Check for Compound words first
                if (candidate.indexOf('-') != -1) {
                    String[] compounds = candidate.split("\\-");

                    // Determine if this string should be segmented on '-'
                    boolean tokenizeCompound = false;
                    HashMap<Integer, Integer> subTokens = new HashMap<Integer, Integer>();
                    for (String compound : compounds) {
                        boolean foundAcronym = isAcronym(compound);
                        if (compound.length() > 1 && !foundAcronym) {
                            tokenizeCompound = true;
                        } else if (foundAcronym) {
                            int subBegin = begin + candidate.indexOf(compound);
                            int subEnd = subBegin + compound.length();
                            subTokens.put(subBegin, subEnd);
                        }
                    }
                    if (tokenizeCompound) {
                        for (Integer newAcronymBegin : subTokens.keySet()) {
                            Integer newAcronymEnd = subTokens.get(newAcronymBegin);
                            Acronym acronym = new Acronym(jCas, newAcronymBegin, newAcronymEnd);
                            acronym.addToIndexes();
                        }

                    }

                    continue;
                }

                // check for ordinals
                String suffix = candidate.substring(candidate.length() - 2).toLowerCase();

                if (candidate.matches("\\d+[a-zA-Z]{2}")) {
                    if ((("stndrdth").indexOf(suffix) % 2) == 0) {
                        continue; // do not annotate ordinal as edu.umn.biomedicus.acronym
                    }
                }
                // decade ordinals end in just "s". For example,
                // "Patient is in his 40s."
                if (candidate.matches("\\d+s{1}"))
                    continue; // do not annotate ordinal as edu.umn.biomedicus.acronym

                Acronym nym = new Acronym(jCas, begin, end);
                nym.addToIndexes(jCas);
            }
        }
    }

    protected boolean isAcronym(String candidate) {

        // Anything that does not have a character can not be an edu.umn.biomedicus.acronym
        // e.g., numbers, dates, symbols
        if (!candidate.matches(".*[a-zA-Z]+.*")) {
            return false;
        }

        // check SPECIALIST lexicon to see if it is a known edu.umn.biomedicus.acronym
        /*String lexInfo = afe.knownWord(candidate);
		if (lexInfo.equals("knownWord")) {
			return false;
		}
        */
        // skip lower case "a" and "i"
        if (candidate.length() == 1) {
            return false;
            // if ("ai".indexOf(candidate) > -1) {
            // return false
        }

        // anything that is all consonants or all vowels is an edu.umn.biomedicus.acronym
        if (afe.isAllConsonants(candidate) || afe.isAllVowels(candidate))
            return true;
        ArrayList<String> featureSet = new ArrayList<String>(10);
        featureSet.add(candidate);
        if (afe.inRieaSet(candidate))
            featureSet.add("inRieaSet");
        featureSet.add("V2L=" + afe.getV2L(candidate));
        featureSet.add("len=" + candidate.length());
        if (afe.meetsMinimumLength(candidate))
            featureSet.add("meetsMinLength ");
        if (afe.meetsMaximumLength(candidate))
            featureSet.add("meetsMaxLength");
        if (afe.containsAlphaCharacters(candidate))
            featureSet.add("containsAlpha");
        if (afe.allCaps(candidate))
            featureSet.add("allCaps");
        if (afe.containsNumber(candidate))
            featureSet.add("containsnumber");
        if (afe.internalPeriodOrDash(candidate))
            featureSet.add("internalPeriodOrDash");
        if (afe.hasConsonantSeparatedVowels(candidate))
            featureSet.add("punctuatedVowels");
        //featureSet.add(lexInfo);
        String[] evalString = new String[featureSet.size()];

        double[] results = model.eval(featureSet.toArray(evalString));
        // index 0 is edu.umn.biomedicus.acronym, index 1 is nonAcronym
        if (results[0] >= results[1]) {
            return true;
        }
        return false; // every test failed
    }
}
