package edu.umn.biomedicus.lvg;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.apache.uima.util.Logger;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.cache.RedisCacheResource_Impl;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;
import org.apache.uima.UIMAFramework;

public class NormAnnotatorFactory {
    private static final Logger logger = UIMAFramework.getLogger(NormAnnotatorFactory.class);

	public static final String LVG_CONFIG_FILE_PROPERTY = "lvg.properties";

	// public static AnalysisEngineDescription createNormAnnotator() throws
	// ResourceInitializationException {
	// return createNormAnnotator((String) null);
	// }

	/**
	 * Create a description for the NormAnnotator using the default cache
	 * 
	 * @return AnalysisEngineDescription
	 * @throws ResourceInitializationException
	 */
	public static AnalysisEngineDescription createNormAnnotator() throws ResourceInitializationException {

		ExternalResourceDescription cache = ExternalResourceFactory.createExternalResourceDescription(
		                                RedisCacheResource_Impl.class, RedisCacheResource_Impl.PARAM_REDIS_HOST,
		                                "localhost");

  	    return createNormAnnotator(cache);
	}

	/**
	 * Create a description for the NormAnnotator that uses a cache supplied as
	 * a parameter
	 * 
	 * @param ExternalResourceDescription
	 *            cache
	 * @return AnalysisEngineDescription
	 * @throws ResourceInitializationException
	 */
	public static AnalysisEngineDescription createNormAnnotator(ExternalResourceDescription cache)
	                                throws ResourceInitializationException {
		String home = System.getenv("BIOMEDICUS_HOME");
		if (home == null) {
			System.err.println("BIOMEDICUS_HOME is not set. Set BIOMEDICUS_HOME and try again.");
			throw new ResourceInitializationException();
		}
		String lvgConfig = ComponentFactory.getResourcePath("config/lvg.properties");
		AnalysisEngineDescription norm = AnalysisEngineFactory.createPrimitiveDescription(LvgTokenNormalizer.class,
		                                ComponentFactory.getTypeSystem(), LvgTokenNormalizer.PARAM_LVG_CONFIG_FILE,
		                                lvgConfig.toString(), LvgTokenNormalizer.PARAM_CACHE, cache);

		return norm;
	}

	public static AnalysisEngineDescription createNormAnnotator(String lvgConfigFileName,
	                                ExternalResourceDescription cache) throws ResourceInitializationException {
		return AnalysisEngineFactory.createPrimitiveDescription(LvgTokenNormalizer.class, ComponentFactory.getTypeSystem(),
		                                LvgTokenNormalizer.PARAM_LVG_CONFIG_FILE, lvgConfigFileName,
		                                LvgTokenNormalizer.PARAM_CACHE, cache);
	}
}
