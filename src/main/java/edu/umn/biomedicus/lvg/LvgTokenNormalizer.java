/* 
Copyright 2010-12 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.lvg;

import edu.umn.biomedicus.cache.BiomedicusCache;
import edu.umn.biomedicus.type.Token;
import gov.nih.nlm.nls.lvg.Api.NormApi;
import gov.nih.nlm.nls.lvg.Api.WordIndApi;
import org.apache.log4j.Logger;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.eclipse.jetty.util.log.Log;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.descriptor.ExternalResource;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.util.JCasUtil;

import java.io.IOException;
import java.util.Vector;

/**
 * 
 * @author Philip Ogren
 * @author University of Minneapolis Biomedical NLP Group
 * 
 *         The NormAnnotator finds the normalized version of a token and adds it
 *         to the "norm" attribute of the token type.
 * 
 *         To configure NormAnnotator, LVG must be installed on the computer
 *         that it is run on and the lvg.properties file must be copied into the
 *         "{$BIOMEDICUS_HOME}/resources/config" directory.
 * 
 */

public class LvgTokenNormalizer extends JCasAnnotator_ImplBase {

	/* ==== Configuration Parameters ==== */
	public static final String PARAM_NORM_DB_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                LvgTokenNormalizer.class, "normDBFileName");
	@ConfigurationParameter(defaultValue = "/edu/umn/biomedicus/dictionaries/normdb")
	public String normDBFileName;

	public static final String PARAM_LVG_CONFIG_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                LvgTokenNormalizer.class, "lvgConfigFile");
	@ConfigurationParameter
	private String lvgConfigFile;

	// public static final String PARAM_CACHE =
	// ConfigurationParameterFactory.createConfigurationParameterName(
	// NormAnnotator.class, "cache");
	// @ConfigurationParameter
	// protected BiomedicusCache cache;
	public static final String PARAM_CACHE = "cache";
	@ExternalResource(key = PARAM_CACHE)
	private BiomedicusCache cache;

	/* === End Configuration Parameters === */

	// database cache is source for all cache lookups
	// private DB cacheContainer = null;
	// private BTreeMap<String, String> normCache;

	// memory cache is those words already looked up so they are more likely to
	// occur again
	// private DB memCacheContainer = null;
	// private BTreeMap<String, String> memNormCache;

	// lvg api instances
	protected NormApi normApi = null;
	protected WordIndApi wordIndApi = null;
	protected Logger log = null;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		/* Check if cache db file is set, and load it accordingly */
		// try {
		// if (normDBFileName != null) {
		// loadNormCache();
		// }
		// } catch (IOException ioe) {
		// throw new ResourceInitializationException(ioe);
		// }

		/* create APIs */
		normApi = new NormApi(lvgConfigFile);
		wordIndApi = new WordIndApi();
		log = Logger.getLogger("edu.umn.biomedicus");

	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {

		try {
			for (Token token : JCasUtil.select(jCas, Token.class)) {
				String tokenText = token.getCoveredText();
				String norm = norm(tokenText);
				token.setNorm(norm);
			}
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
	}

	private String norm(String token) throws Exception {
		// if normCache contains the word, then return the cached norm value
		// limit norm cache to lower case to limit size
		// token = token.toLowerCase();
		String normalizedToken = null;
		String cacheKey = new StringBuilder("lvg::").append(token).toString();
		if (cache != null) {
			try {
				normalizedToken = cache.get(cacheKey);
			} catch (Exception e) {
				// For any reason, disable cache
				Log.warn("NormAnnotator Cache failed with an exception: " + e);
				this.cache = null;
			}
			if (normalizedToken != null) {
				return normalizedToken;
			}
			normalizedToken = norm(normApi, wordIndApi, token);
			cache.set(cacheKey, normalizedToken);
			return normalizedToken;
		}
		return token;
	}

	/**
	 * This method loads 2 caches, one is a file-based db, the other is an
	 * off-heap compressed memory store. The file db is faster than plain
	 * file-based cache, but not as fast as the compressed memory-based cache,
	 * so 2 levels of caching are used. Lookups in the file-based db are placed
	 * in the momory db so subsequent lookups are faster. The memory cache does
	 * OK up to a few million entries, so we do not prune (yet).
	 * 
	 * The use of off-heap mapped memory requires an extra java vm argument of
	 * -XX:MaxDirectMemorySize=3G
	 * 
	 * The amount of memory in that argument should be adjusted according to the
	 * vocabulary size you anticipate.
	 * 
	 * @throws IOException
	 */
	// private void loadNormCache() throws IOException {
	// File dbFile = null;
	// try {
	// dbFile = new
	// File(this.getClass().getResource(normDBFileName).toURI().getRawPath());
	// } catch (URISyntaxException e) {
	// throw new
	// IOException("Failed to open normDB cache file because of invalid URI syntax: "
	// + this.getClass().getResource(normDBFileName));
	// }
	// cacheContainer =
	// DBMaker.newFileDB(dbFile).readOnly().journalDisable().asyncFlushDelay(100)
	// .closeOnJvmShutdown().make();
	// normCache = cacheContainer.getTreeMap("normCache");

	// memCacheContainer =
	// DBMaker.newDirectMemoryDB().journalDisable().asyncFlushDelay(100).compressionEnable()
	// .make();
	// memNormCache = memCacheContainer.createTreeMap("normCache", 120,
	// false, null, null, null);

	// for (String key : normCache.keySet()) {
	// String value = normCache.get(key);
	// memNormCache.put(key, value);
	// }
	// normCache.close();
	// cacheContainer.close();
	// }

	public static String norm(NormApi normApi, WordIndApi wordIndApi, String token) throws Exception {
		return norm(normApi, wordIndApi, token, true);
	}

	private static String norm(NormApi normApi, WordIndApi wordIndApi, String token, boolean recurse) throws Exception {
		// Otherwise, we will invoke the normApi to get the normalization
		Vector<String> normVector = normApi.Mutate(token);

		// not sure what the contract is of normApi.Mutate, so am hedging my
		// bets in case null or an empty vector is returned
		if (normVector == null || normVector.size() == 0) {
			return token;
		}

		String norm = normVector.get(normVector.size() - 1);
		if (norm.indexOf(" ") == -1) {
			return norm;
		}

		if (recurse) {
			StringBuilder sb = new StringBuilder();
			Vector<String> wordVector = wordIndApi.Mutate(token);
			for (String word : wordVector) {
				sb.append(norm(normApi, wordIndApi, word, false) + "_");
			}
			return sb.substring(0, sb.length() - 1);
		} else {
			return token;
		}
	}
}
