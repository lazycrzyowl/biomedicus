/* 
  Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.window;

import java.util.ArrayDeque;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.StringArray;
import org.uimafit.component.JCasAnnotator_ImplBase;

import edu.umn.biomedicus.type.CharWindow;

/**
 * This component is a window annotator. It produces annotations for every range of characters
 * necessary for downstream context access. For example, using this component to create a character
 * window of size 25 will produce an annotation that defines a target character (the middle) and 25
 * tokens on either side of that target (51 total tokens). This allows you to iterate over windows
 * and use the methods getTarget, getPreview, and getReview to examine context.
 * 
 * @author Robert Bill
 * 
 */
public class CharWindowAnnotator extends JCasAnnotator_ImplBase {
  protected ArrayDeque<Character> window;
  protected int horizon = 11;

  // TODO set parameters for size of window
  // assuming 10 for purposes of development.

  @Override
  public void process(JCas jCas) throws AnalysisEngineProcessException {
    char[] text = jCas.getDocumentText().toCharArray();
    int size = text.length;
    int begin = 0;
    int end = horizon + 1;
    int previewSize = 0;
    int reviewSize = 0;

    for (int target = 0; target < size; target++) {
      previewSize = horizon;
      reviewSize = horizon;

      // set the beginning of the window
      begin = target - horizon - 1;
      if (begin < 0) {
        begin = 0;
        reviewSize = target;
      }

      // set the end of the window
      end = target + horizon + 1;
      if (end > size) {
        end = size;
        previewSize = size - target;
      }

      // create the window annotation
      CharWindow win = new CharWindow(jCas, begin, end);
      win.addToIndexes();
      win.setTarget(String.valueOf(text[target]));
      win.setTargetIndex(target);
      win.setPreview(new StringArray(jCas, previewSize));
      win.setReview(new StringArray(jCas, reviewSize));

      // add the characters in the horizons (preview, review)
      for (int i = 0; i < previewSize; i++) {
        win.setPreview(i, String.valueOf(text[target + i]));
      }
      for (int i = 0; i < reviewSize; i++) {
        win.setReview(i, String.valueOf(text[target - i]));
      }
    }
  }
}
