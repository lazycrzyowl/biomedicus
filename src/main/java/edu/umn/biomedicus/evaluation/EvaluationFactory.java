/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.evaluation;

import java.io.File;
import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;

import edu.umn.biomedicus.ViewNames;

/**
 * 
 * @author Philip Ogren
 * 
 */

public interface EvaluationFactory {

	/**
	 * This method creates an aggregate analysis engine that performs
	 * task-specific evaluation using the gold-standard data which should be
	 * found in the {@link ViewNames#GOLD_VIEW} view against the
	 * system-generated data which should be found in the
	 * {@link ViewNames#SYSTEM_VIEW}. All results should be written to the
	 * evaluation directory provided. This directory will generally be specific
	 * to either a fold or the holdout evaluation.
	 * 
	 * @param evaluationDirectory
	 *            a directory where all evaluation results should be written to.
	 * @return an aggregate analysis engine description that describese the
	 *         evaluation pipeline.
	 * @throws ResourceInitializationException
	 */
	public AnalysisEngineDescription createEvaluationAggregate(File evaluationDirectory)
	                                throws ResourceInitializationException;

	/**
	 * This method aggregates results from a number of directories and writes
	 * the aggregated results to the output directory.
	 * 
	 * @param evaluationDirectories
	 *            a list of evaluation directories that correspond directly to
	 *            the evaluation directories passed into multiple calls to
	 *            {@link #createEvaluationAggregate(File)}
	 * @param outputDirectory
	 *            a directory where aggregated results are written to.
	 * @throws Exception
	 */
	public void aggregateEvaluationResults(List<File> evaluationDirectories, File outputDirectory) throws Exception;

}
