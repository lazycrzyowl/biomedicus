/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.evaluation;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class ConfusionMatrix implements Serializable {

	private static final long serialVersionUID = 7016645248546728596L;

	private int correct = 0;
	private int wrong = 0;
	private Map<String, Map<String, Integer>> confusions = new HashMap<String, Map<String, Integer>>();

	public int getCorrect() {
		return correct;
	}

	public int getWrong() {
		return wrong;
	}

	public void add(String outcome1, String outcome2) {
		add(outcome1, outcome2, 1);
	}

	public void add(String outcome1, String outcome2, int count) {
		if (outcome1.equals(outcome2)) {
			correct += count;
		} else {
			wrong += count;
		}

		Map<String, Integer> confusion = confusions.get(outcome1);
		if (confusion == null) {
			confusion = new HashMap<String, Integer>();
			confusions.put(outcome1, confusion);
		}

		int confusionCount = count;
		if (confusion.containsKey(outcome2)) {
			confusionCount = confusion.get(outcome2) + count;
		}
		confusion.put(outcome2, confusionCount);
	}

	public void add(ConfusionMatrix matrix) {
		for (String outcome1 : matrix.confusions.keySet()) {
			Map<String, Integer> confusion = matrix.confusions.get(outcome1);
			for (String outcome2 : confusion.keySet()) {
				add(outcome1, outcome2, confusion.get(outcome2));
			}
		}
	}

	public void print(PrintStream out) {
		List<String> outcome1s = new ArrayList<String>(confusions.keySet());
		Collections.sort(outcome1s);
		for (String outcome1 : outcome1s) {
			int outcome1Count = 0;
			Map<String, Integer> confusion = confusions.get(outcome1);

			List<String> outcome2s = new ArrayList<String>(confusion.keySet());
			Collections.sort(outcome2s);

			for (String outcome2 : outcome2s) {
				outcome1Count += confusion.get(outcome2);
			}
			out.println(outcome1 + " count: " + outcome1Count);

			for (String outcome2 : outcome2s) {
				out.println("\t" + outcome2 + "\t" + confusion.get(outcome2));
			}

			out.println();
		}

	}

}
