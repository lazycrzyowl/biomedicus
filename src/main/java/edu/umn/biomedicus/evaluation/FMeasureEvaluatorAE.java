/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.evaluation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.util.ReflectionUtil;
import org.cleartk.util.ViewURIUtil;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ViewNames;

/**
 * The basic strategy of FMeasureEvaluatorAE is to compute f-score by creating
 * string representations of every annotation of a type in the gold view and the
 * system view and putting these strings into local collection: one for gold
 * annotations and another for system annotations. The string representation are
 * specified by {@link #toString(Annotation)} and may consist of the character
 * offsets or whatever is required to uniquely identify and compare an
 * annotation. If there happens to be two sentences with exactly the same
 * character offsets in the *same* view, then an exception will be thrown. The
 * assumption is that this should never happen and if it does there is a bug in
 * the code that generated the annotations or a problem with the gold-standard
 * data.
 * 
 * After the collection have been populated with string representations of all
 * the gold-standard annotations and the system-generated annotations, the
 * collection are interrogated to count true positives (annotations that occur
 * in both collection), false positives (annotations that occur in only the
 * system annotation collection), and false negatives (annotations that occur in
 * only the gold sentences collection.)
 * 
 * @author Philip Ogren
 * 
 */

@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public abstract class FMeasureEvaluatorAE<TYPE_SYSTEM_TYPE extends Annotation> extends JCasAnnotator_ImplBase {

	public static final String OUTPUT_FILE_NAME = "evaluation.txt";
	public static final String README_FILE_NAME = "README.txt";
	public static final String TRUE_POSITIVES_FILE_NAME = "true-positives.txt";
	public static final String FALSE_POSITIVES_FILE_NAME = "false-positives.txt";
	public static final String FALSE_NEGATIVES_FILE_NAME = "false-negatives.txt";
	public static final String FSCORE_FILE_NAME = "fscore.ser";

	public static final String PARAM_OUTPUT_DIRECTORY = ConfigurationParameterFactory.createConfigurationParameterName(
	                                FMeasureEvaluatorAE.class, "outputDirectory");
	@ConfigurationParameter(mandatory = true)
	protected File outputDirectory;

	protected PrintStream evaluationOut;
	protected PrintStream readmeOut;
	protected PrintStream truePositivesOut;
	protected PrintStream falsePositivesOut;
	protected PrintStream falseNegativesOut;

	protected FScore fScore = new FScore();

	private Class<? extends Annotation> typeClass;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		if (!outputDirectory.exists())
			outputDirectory.mkdirs();

		try {
			evaluationOut = new PrintStream(new File(outputDirectory, OUTPUT_FILE_NAME));
			truePositivesOut = new PrintStream(new File(outputDirectory, TRUE_POSITIVES_FILE_NAME));
			falsePositivesOut = new PrintStream(new File(outputDirectory, FALSE_POSITIVES_FILE_NAME));
			falseNegativesOut = new PrintStream(new File(outputDirectory, FALSE_NEGATIVES_FILE_NAME));
			falseNegativesOut = new PrintStream(new File(outputDirectory, FALSE_NEGATIVES_FILE_NAME));
			readmeOut = new PrintStream(new File(outputDirectory, README_FILE_NAME));
			readmeOut.println(printReadme());
			readmeOut.close();

			// extract the token and sentence classes from the type parameters
			this.typeClass = ReflectionUtil.<Class<? extends Annotation>> uncheckedCast(ReflectionUtil.getTypeArgument(
			                                FMeasureEvaluatorAE.class, "TYPE_SYSTEM_TYPE", this));

		} catch (FileNotFoundException fnfe) {
			throw new ResourceInitializationException(fnfe);
		}

	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {

		try {
			JCas goldView = jCas.getView(ViewNames.GOLD_VIEW);
			JCas systemView = jCas.getView(ViewNames.SYSTEM_VIEW);

			Map<String, TYPE_SYSTEM_TYPE> goldAnnotations = populateAnnotations(goldView);
			Map<String, TYPE_SYSTEM_TYPE> systemAnnotations = populateAnnotations(systemView);

			// Find all the TPs and FNs. There will be one TP for each sentence
			// string that is in both sets of sentence strings.
			// There will be one FN for each sentence in goldSentenceStrings
			// that is not in systemSentenceStrings.
			for (String goldSentenceString : goldAnnotations.keySet()) {
				if (systemAnnotations.containsKey(goldSentenceString)) {
					fScore.TPs++;
					truePositivesOut.println(printTruePositive(goldAnnotations.get(goldSentenceString),
					                                systemAnnotations.get(goldSentenceString), goldView, systemView));
				} else {
					fScore.FNs++;
					falseNegativesOut.println(printFalseNegative(goldAnnotations.get(goldSentenceString), goldView,
					                                systemView));
				}
			}

			// Find all the FPs. There is one FP for each sentence in
			// systemSentenceStrings that is not in goldSentenceStrings
			for (String systemSentenceString : systemAnnotations.keySet()) {
				if (!goldAnnotations.containsKey(systemSentenceString)) {
					fScore.FPs++;
					falsePositivesOut.println(printFalsePositive(systemAnnotations.get(systemSentenceString), goldView,
					                                systemView));
				}
			}

		} catch (CASException ce) {
			throw new AnalysisEngineProcessException(ce);
		}

	}

	protected Map<String, TYPE_SYSTEM_TYPE> populateAnnotations(JCas view) throws AnalysisEngineProcessException {
		Map<String, TYPE_SYSTEM_TYPE> annotations = new HashMap<String, TYPE_SYSTEM_TYPE>();

		for (Annotation annotation : JCasUtil.select(view, typeClass)) {
			@SuppressWarnings("unchecked")
			TYPE_SYSTEM_TYPE goldAnnotation = (TYPE_SYSTEM_TYPE) annotation;
			String goldAnnotationString = toString(goldAnnotation);
			if (annotations.containsKey(goldAnnotationString)) {
				throw new AnalysisEngineProcessException(new Throwable("The same annotation occurs twice in the "
				                                + view.getViewName() + " view in " + ViewURIUtil.getURI(view)
				                                + ".  Duplicate annotation = <" + goldAnnotationString + "> - "
				                                + goldAnnotation.getCoveredText()));
			}
			annotations.put(goldAnnotationString, goldAnnotation);
		}
		return annotations;
	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		fScore.print(evaluationOut);
		evaluationOut.close();
		truePositivesOut.close();
		falsePositivesOut.close();
		falseNegativesOut.close();

		try {
			ObjectOutputStream ooo = new ObjectOutputStream(new FileOutputStream(new File(outputDirectory,
			                                FSCORE_FILE_NAME)));
			ooo.writeObject(fScore);
			ooo.close();
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}

	}

	/**
	 * Overwrite this method if you want to generate your own readme file.
	 */
	protected abstract String printReadme();

	/**
	 * Print out a single false positive example to false-positives.txt
	 */
	protected abstract String printFalsePositive(TYPE_SYSTEM_TYPE systemAnnotation, JCas goldView, JCas systemView)
	                                throws AnalysisEngineProcessException;

	/**
	 * Print out a single false negative example to false-negatives.txt
	 */
	protected abstract String printFalseNegative(TYPE_SYSTEM_TYPE goldAnnotation, JCas goldView, JCas systemView)
	                                throws AnalysisEngineProcessException;

	/**
	 * Print out a single true positive example to true-positives.txt
	 */
	protected abstract String printTruePositive(TYPE_SYSTEM_TYPE goldAnnotation, TYPE_SYSTEM_TYPE systemAnnotation,
	                                JCas goldView, JCas systemView) throws AnalysisEngineProcessException;

	/**
	 * This method should return a string representation of an annotation that
	 * is suitable to uniquely identify the annotation and compare it. For
	 * example, to uniquely identify and compare sentences all that is likely
	 * needed are character offsets. However, to compare part-of-speech tags of
	 * tokens the character offsets and the part-of-speech tags would be
	 * required to determine if a system part-of-speech/token pair is the same
	 * as a gold-standard part-of-speech/token pair
	 * 
	 * @param annotation
	 * @return a string representation of the annotation that can be used for
	 *         hashing an annotation.
	 */
	protected String toString(TYPE_SYSTEM_TYPE annotation) {
		return "" + annotation.getBegin() + "|" + annotation.getEnd();
	}

}
