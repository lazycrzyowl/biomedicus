/* 
 Copyright 20102University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.evaluation.copy;

import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.type.UmlsConcept;

/**
 * Copies UMLSConcepts from one view to another. Used in evaluation.
 * 
 * @author Robert Bill
 * 
 */

@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class UmlsConceptCopier extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas goldView = jCas.getView(ViewNames.GOLD_VIEW);
			JCas systemView = jCas.getView(ViewNames.SYSTEM_VIEW);

			List<UmlsConcept> goldConcepts = (List<UmlsConcept>) JCasUtil.select(goldView, UmlsConcept.class);
			for (UmlsConcept goldConcept : goldConcepts) {
				UmlsConcept systemUmlsConcept = new UmlsConcept(systemView, goldConcept.getBegin(),
				                                goldConcept.getEnd());
				systemUmlsConcept.setCuis(goldConcept.getCuis());
				systemUmlsConcept.addToIndexes();
			}
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
}
