/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.evaluation.copy;

import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.type.Sentence;

/**
 * 
 * @author Philip Ogren
 * 
 */

@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class SentenceCopier extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas goldView = jCas.getView(ViewNames.GOLD_VIEW);
			JCas systemView = jCas.getView(ViewNames.SYSTEM_VIEW);

			List<Sentence> goldSentences = (List<Sentence>) JCasUtil.select(jCas, Sentence.class);
			// AnnotationRetrieval.getAnnotations(goldView, Sentence.class);
			for (Sentence goldSentence : goldSentences) {
				Sentence systemSentence = new Sentence(systemView, goldSentence.getBegin(), goldSentence.getEnd());
				systemSentence.addToIndexes();
			}
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
}
