/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.evaluation.copy;

import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Philip Ogren
 * 
 */

@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class TokenCopier extends JCasAnnotator_ImplBase {

	public static final String PARAM_COPY_POS_TAGS = ConfigurationParameterFactory.createConfigurationParameterName(
	                                TokenCopier.class, "copyPosTags");
	@ConfigurationParameter
	private boolean copyPosTags = false;

	public static final String PARAM_COPY_STEMS = ConfigurationParameterFactory.createConfigurationParameterName(
	                                TokenCopier.class, "copyStems");
	@ConfigurationParameter
	private boolean copyStems = false;

	public static final String PARAM_COPY_NORMS = ConfigurationParameterFactory.createConfigurationParameterName(
	                                TokenCopier.class, "copyNorms");
	@ConfigurationParameter
	private boolean copyNorms = false;

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas goldView = jCas.getView(ViewNames.GOLD_VIEW);
			JCas systemView = jCas.getView(ViewNames.SYSTEM_VIEW);

			List<Token> goldTokens = (List<Token>) JCasUtil.select(goldView, Token.class);
			for (Token goldToken : goldTokens) {
				Token systemToken = new Token(systemView, goldToken.getBegin(), goldToken.getEnd());
				if (copyPosTags) {
					systemToken.setPos(goldToken.getPos());
				}
				if (copyStems) {
					systemToken.setStem(goldToken.getStem());
				}
				if (copyNorms) {
					systemToken.setNorm(goldToken.getNorm());
				}
				systemToken.addToIndexes();
			}
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
}
