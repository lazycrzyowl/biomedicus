/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.evaluation.copy;

import static edu.umn.biomedicus.ViewNames.GOLD_VIEW;
import static edu.umn.biomedicus.ViewNames.SYSTEM_VIEW;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

/**
 * This simple AE copies tokens and sentences from one view to another.
 * 
 * @author Philip
 * 
 */

@SofaCapability(inputSofas = { GOLD_VIEW, SYSTEM_VIEW })
public class SentenceAndTokenCopier extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas view1 = jCas.getView(GOLD_VIEW);
			JCas view2 = jCas.getView(SYSTEM_VIEW);

			for (Token token1 : JCasUtil.select(view1, Token.class)) {
				new Token(view2, token1.getBegin(), token1.getEnd()).addToIndexes();
			}

			for (Sentence sentence1 : JCasUtil.select(view1, Sentence.class)) {
				new Sentence(view2, sentence1.getBegin(), sentence1.getEnd()).addToIndexes();
			}
		} catch (CASException ce) {
			throw new AnalysisEngineProcessException(ce);
		}

	}

}
