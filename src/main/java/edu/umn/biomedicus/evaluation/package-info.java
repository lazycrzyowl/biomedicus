/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

/**
 * The evaluation package provides infrastructure for evaluating an analysis engine
 * against some gold-standard data.  For example, the classes in the package
 * edu.umn.biomedicus.pos.evaluation evaluate the part-of-speech tagger against 
 * gold-standard part-of-speech tags.  Generally, to get a good performing analysis 
 * engine that relies on statistical classification it is important to experiment with
 * using different classsifiers (e.g. MaxEnt, SVM, etc.), tuning parameters (e.g. a linear
 * kernel or polynomial kernel), features (i.e. machine learning features), among other
 * considerations.  This represents a large search space for finding an optimal
 * configuration for your data.  Therefore, it is important to have infrastructure for 
 * quickly changing experimental conditions and evaluating the changes against other 
 * experimental conditions.  This package provides high-level infrastructure for setting
 * up evaluation experiments.       
 * 
 * The core class that implements evaluation tasks that you will want to run is in 
 * {@link edu.umn.biomedicus.evaluation.Evaluation}.  This is a good starting point
 * for people wanting to use this package.   

 *  @author Philip Ogren
 */

package edu.umn.biomedicus.evaluation;