/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.evaluation;

import java.io.PrintStream;
import java.io.Serializable;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class FScore implements Serializable {

	private static final long serialVersionUID = 4795962317489201910L;

	public int TPs = 0;
	public int FPs = 0;
	public int FNs = 0;

	public double precision() {
		return (double) TPs / (TPs + FPs);
	}

	public double recall() {
		return (double) TPs / (TPs + FNs);
	}

	public double fscore() {
		return (double) (2 * TPs) / (2 * TPs + FPs + FNs);
	}

	public void add(FScore fScore) {
		this.TPs += fScore.TPs;
		this.FPs += fScore.FPs;
		this.FNs += fScore.FNs;
	}

	public void print(PrintStream out) {
		out.println("TPs\t" + TPs);
		out.println("FPs\t" + FPs);
		out.println("FNs\t" + FNs);
		out.println("precision\t" + precision());
		out.println("recall\t" + recall());
		out.println("fscore\t" + fscore());
		out.println(TPs + "\t" + FPs + "\t" + FNs + "\t" + precision() + "\t" + recall() + "\t" + fscore());
	}
}
