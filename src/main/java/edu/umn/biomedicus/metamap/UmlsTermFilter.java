package edu.umn.biomedicus.metamap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import pitt.search.semanticvectors.CloseableVectorStore;
import pitt.search.semanticvectors.FlagConfig;
import pitt.search.semanticvectors.LuceneUtils;
import pitt.search.semanticvectors.SearchResult;
import pitt.search.semanticvectors.VectorSearcher;
import pitt.search.semanticvectors.VectorStoreReader;
import pitt.search.semanticvectors.ZeroVectorException;

public class UmlsTermFilter {

	private CloseableVectorStore queryVecReader = null;
	private CloseableVectorStore resultsVecReader = null;
	private LuceneUtils luceneUtils = null;
	private VectorSearcher vecSearcher = null;
	private FlagConfig config = null;

	public UmlsTermFilter() {
		// Semantic vector variables
		FlagConfig config = FlagConfig.getFlagConfig(new String[] {
		                                "-queryvectorfile",
		                                "/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/termvectors.bin",
		                                "-searchvectorfile",
		                                "/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/docvectors.bin",
		                                "-luceneindexpath",
		                                "/Users/robertbill/git/biomedicus/biomedicus/resources/luceneIndex" });
		try {
			this.queryVecReader = VectorStoreReader
			                                .openVectorStore("/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/termvectors.bin",
			                                                                config);
			this.resultsVecReader = VectorStoreReader
			                                .openVectorStore("/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/docvectors.bin",
			                                                                config);
			this.luceneUtils = new LuceneUtils(config);
			this.config = config;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void singletonDebug(String label, CandidateTerm t, HashSet<String> c) {
		System.out.print(label + t.getCoveredText());
		if (c.size() < 2) {
			System.out.print(c);
		} else {
			System.out.print("[" + c.size() + "]");
		}
		System.out.println();
	}

	public LinkedList<SearchResult> getCuiVector(List<CandidateTerm> list) {
		System.out.println(list + " " + list.size());
		for (CandidateTerm x : list) {
			System.out.println("SetEntry: " + x.getCoveredText());
		}
		String[] aTerms = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			aTerms[i] = list.get(i).getCoveredText();
		}

		// search semantic vectors for each permutation of terms

		try {
			System.out.println("TERMS: " + list + ", " + list.size());
			vecSearcher = new VectorSearcher.VectorSearcherCosine(queryVecReader, resultsVecReader, luceneUtils,
			                                config, aTerms);
		} catch (ZeroVectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return vecSearcher.getNearestNeighbors(20);
	}

	private static void intersectDebug(String label, CandidateTerm termA, HashSet<String> a, CandidateTerm termB,
	                                HashSet<String> b, HashSet<String> intersect) {
		System.out.print("    Intersect: " + termA.getCoveredText());
		if (a.size() < 2) {
			System.out.print(a);
		} else {
			System.out.print("[" + a.size() + "]");
		}
		System.out.print(termB.getCoveredText());
		if (b.size() < 2) {
			System.out.print(b);
		} else {
			System.out.print("[" + b.size() + "]");
		}
		if (intersect.size() < 2) {
			System.out.print(intersect);
		} else {
			System.out.print("[" + intersect.size() + "]");
		}
		System.out.println();

	}

	public static HashSet<String> getIntercept(HashSet<String> setA, HashSet<String> setB) {
		HashSet<String> a = new HashSet<String>(setA.size());
		for (String item : setA)
			a.add((String) item.toString());
		HashSet<String> b = new HashSet<String>(setB.size());
		for (String item : setB)
			b.add((String) item.toString());

		// check for nulls or emptys. Both conditions mean intercept is empty
		if (a == null || b == null) {
			return new HashSet<String>();
		} else if (a.size() == 0 || b.size() == 0) {
			return new HashSet<String>();
		}

		// check for wildcard "*"
		if (a.size() == 1 && a.contains("*")) {
			if (b.size() == 1 && b.contains("*")) {
				return a;
			} else if (b.size() >= 1 && !b.contains("*")) {
				return b;
			}
		} else if (a.size() > 1) {
			if (b.size() == 1 && b.contains("*")) {
				return a;
			} else if (!b.contains("*")) {
				a.retainAll(b);
				return a;
			}
		}
		return a;
	}

	/**
	 * Reduces a set of terms to the most probably pairings based on semantic
	 * vectors relatedness scores. NOTE: supplied arguments must be normalized
	 * first because all terms in the vectors were first normalized. If
	 * capitalization or the suffix is wrong, the vector will not be found.
	 * 
	 * @param list
	 *            of concepts to filter
	 * @param w
	 *            the weight of the parameters used for weighting scheme.
	 *            Currently none.
	 * @throws IllegalArgumentException
	 *             if a provided term is obviously not in normalized form.
	 */
	public ArrayList<CandidateTerm> reduce(ArrayList<CandidateTerm> window) {
		if (window == null || window.size() == 0) {
			return new ArrayList<CandidateTerm>();
		}

		// find terms with cuis
		BitSet termsWithCuis = new BitSet(window.size());
		BitSet termPairsWithCuis = new BitSet(window.size());
		BitSet termTriosWithCuis = new BitSet(window.size());
		HashSet<String> candidateCuis = new HashSet<String>();

		for (int i = 0; i < window.size(); i++) {
			CandidateTerm termA = window.get(i);
			HashSet<String> a = termA.getCuis();
			if (a.size() > 0 && termA.getCoveredText().length() > 1) {
				termsWithCuis.set(i);
			}

			singletonDebug("  Singleton: ", termA, a);

			/* ======== inner ======== */
			for (int j = i + 1; j < window.size(); j++) {
				if (i == j)
					continue;
				CandidateTerm termB = window.get(j);
				HashSet<String> b = termB.getCuis();
				HashSet<String> intersect = UmlsTermFilter.getIntercept(a, b);
				if (intersect.size() > 0) {
					intersectDebug("    Intersect: ", termA, a, termB, b, intersect);
					termPairsWithCuis.set(i);
					termPairsWithCuis.set(j);
					candidateCuis.addAll(intersect);
				}

				/* ========= inner ========= */
				for (int k = 0; k < window.size(); k++) {
					if (i == k || j == k)
						continue;
					CandidateTerm termC = window.get(k);
					HashSet<String> c = termC.getCuis();
					HashSet<String> intersectB = UmlsTermFilter.getIntercept(b, c);
					HashSet<String> trioIntersect = UmlsTermFilter.getIntercept(intersect, intersectB);
					if (trioIntersect.size() > 0) {
						for (int x : new int[] { i, j, k }) {
							termTriosWithCuis.set(x);
						}
						System.out.println("        Trios: " + termA.getCoveredText() + ", " + termB.getCoveredText()
						                                + ", " + termC.getCoveredText() + " [" + trioIntersect.size()
						                                + "]");
					}
				}

				/* ======= end inner ======== */
			}
			/* ======== end inner ======== */
		}
		System.out.println(termsWithCuis);
		System.out.println(termPairsWithCuis);
		System.out.println(termTriosWithCuis);
		System.out.println("Sentence has a total dimension of " + candidateCuis.size() + " cuis");
		return window;
	}

	// public ArrayList<NormalizedToken> umlsFilter(NormalizedToken nToken) {
	//
	// }

	public static ArrayList<CandidateTerm> junk(ArrayList<CandidateTerm> window) {
		// Create two hashSets. The intercept will be the survivors.
		// No survivors means the end of a set of candidate terms.
		System.out.println(window);
		CandidateTerm survivor = window.remove(0);
		CandidateTerm neighbor = window.remove(0);
		window.add(new CandidateTerm(null, null));
		window.add(new CandidateTerm(null, null));
		System.out.println(window);
		// Loop through all the candidate terms and get intercepts.
		// remember indexes are all + 2 because already assigned the first 2.
		ArrayList<CandidateTerm> phrase = new ArrayList<CandidateTerm>();
		HashSet<String> survivorCuis = null;
		// for (int i = 0; i < window.size() - 2; i++) {
		while (window.size() > 0) {
			System.out.print("      Testing terms: " + survivor.getTermSummary() + ", " + neighbor.getTermSummary());
			// test if first term is null or empty. That means it is not a term
			if (survivor.getCuis() == null || survivor.getCuis().size() == 0) {
				survivor = neighbor;
				neighbor = window.remove(0);
				System.out.println("   -term 1 discarded.");
				continue;
			}

			// test if newTerm is null or empty, that means it is the end of a
			// term cluster
			if (neighbor.getCuis() == null || neighbor.getCuis().size() == 0) {
				// skip if because neighbor term is punctuation
				if (neighbor.getCoveredText().matches("[!-;:.,]+")) {
					neighbor = window.remove(0);
					System.out.println();
					continue;
				}
				phrase.add(survivor);
				System.out.println("   -term 2 discarded.");

			}

			// If both survivors and newTerm are non-empty, get the intersect.
			// If intersect is empty, the two terms do not co-exist in a SUI.
			// keep only first term in phrase and start new phrase with second
			// term.
			// if intersect is non-empty, both terms co-exist in a SUI.
			// keep both terms in the candidate phrase.
			if (survivorCuis == null || survivorCuis.size() == 0) {
				survivorCuis = survivor.getCuis();
			}
			survivorCuis = UmlsTermFilter.getIntercept(survivor.getCuis(), neighbor.getCuis());
			if (survivorCuis.size() == 0) {
				phrase.add(survivor);
				// for (CandidateTerm x : phrase) {
				// System.out.print(x.getCoveredText() + " ");
				// }
				// System.out.println();
				System.out.println("   -No cuis match. End of phrase.");
				printPhrase(phrase);
				phrase = new ArrayList<CandidateTerm>();
				survivor = neighbor;
				survivorCuis = neighbor.getCuis();
				neighbor = window.remove(0);
			} else {
				phrase.add(survivor);
				survivor = neighbor;
				neighbor = window.remove(0);
				System.out.println("   -successful match with " + survivorCuis.size() + " cui overlaps. Continuing.");
			}
		}
		printPhrase(phrase);
		return window;
	}

	private static void printPhrase(ArrayList<CandidateTerm> phrase) {
		System.out.print("    ****PHRASE: ");
		for (CandidateTerm term : phrase) {
			System.out.print(term.getCoveredText() + " ");
		}
		System.out.println();
	}

	public void destroy() {
		this.queryVecReader.close();
		this.resultsVecReader.close();
	}
}
