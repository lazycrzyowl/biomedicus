/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.extractor;

import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Riea Moon
 * @author Robert Bill
 * 
 *         NOTE: could use abstracts from CALBC
 *         (http://www.ebi.ac.uk/Rebholz-srv/CALBC/corpora/corpora.html).
 * 
 *         TODO: check license for CALBC
 */
public class TokenFeatureExtractor {
  protected Token token;
  protected String text;
  protected String lowerCaseText;

  public TokenFeatureExtractor(Token token) {
    this.token = token;
    this.text = token.getCoveredText();
    this.lowerCaseText = text.toLowerCase();
  }

  public boolean isAllVowels() {
    return text.matches("[aeiouAEIOU]+");
  }

  public boolean isAllConsonants() {
    return text.matches("[^aeiouAEIOU");
  }

  public boolean hasConsonantSeparatedVowels() {
    // More than one vowel, and the vowels are separated by consonant(s)
    return text.matches(".*[aeiou]+[bcdfghjklmnpqrstvwxz]+[aeiou]+.*");
  }

  public float repeatingVowelRatio(String acronym) {
    // The number of repeating vowels divided by token length.
    int repeatingVowelCount = 0;
    boolean priorIsVowel = false;
    String vowels = "AEIOU";
    for (int i = 0; i < acronym.length(); i++) {
      if (vowels.contains(acronym.substring(i, i + 1))) {
        if (priorIsVowel) {
          repeatingVowelCount++;
        } else {
          priorIsVowel = true;
        }
      }
    }
    float v2l = new Float(repeatingVowelCount) / acronym.length();
    return v2l;
  }

  public boolean meetsMinimumLength(int minLength) {
    return text.length() > minLength;
  }

  public boolean meetsMaximumLength(int maxLength) {
    return text.length() <= maxLength;
  }

  public boolean containsAlpha() {
    // return true if token contains at least one alpha character.
    return text.matches(".*[a-zA-Z]+.*");
  }

  public boolean isAllCaps(String token) {
    // return "allCaps" if every alpha character is capitalized.
    return token.matches("[A-Z|\\.|\\-|\\&]*");
  }

  public boolean alphasInHorizon(Token[] horizon) {
    // First check to make sure it contains a letter
    if (!this.containsAlpha())
      return false;
    // collect all the first characters of tokens in the horizon
    StringBuilder re = new StringBuilder();
    for (Token horizonToken : horizon) {
      char ch = horizonToken.getCoveredText().charAt(0);
      if (Character.isLetter(ch))
        re.append(Character.toLowerCase(ch));
    }
    String window = re.toString();

    // check each edu.umn.biomedicus.acronym letter against the collection of letters in the horizon
    for (char ch : text.toLowerCase().toCharArray()) {
      if (Character.isLetter(ch)) {
        if (window.indexOf(ch) == -1)
          return false;
        else
          continue;
      }
    }
    return true;
  }

  public boolean containsNumber(String token) {
    // return true if contains number. Otherwise, false
    return token.matches(".*[0-9].*");
  }

  public boolean internalPeriodOrDash(String token) {
    // return true if there's a period internal in the token and no other
    // punctuation is allowed.
    if (token.length() < 2) {
      return false;
    } else
      return token.substring(1, token.length() - 1).matches(".*[\\.\\-\\&].*");
  }
}