/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.wsd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.resource.ResourceProcessException;
import org.uimafit.component.Resource_ImplBase;
import org.uimafit.descriptor.ExternalResource;

import edu.umn.biomedicus.BiomedicusDBResource_Impl;
import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.UmlsConcept;

/**
 * This class is a resource for looking up semantic similarity scores in a
 * database. The scores are normal scores, but this class uses log probability
 * scores according to a logistic regression line that proved to have the
 * highest AUC when determining if concepts belong to the same category.
 * 
 * notes:
 * 
 * <pre>
 * Coefficients:
 *             Estimate Std. Error z value Pr(>|z|)    
 * (Intercept) -1.09535    0.02388 -45.869  < 2e-16 ***
 * reslin$V4    2.89272    0.18590  15.560  < 2e-16 ***
 * reslin$V5   -5.92729    0.97787  -6.061 1.35e-09 ***
 * 
 * marginal effect:
 *             effect  error  t.value p.value
 * (Intercept) -0.2738 0.0060 -45.9502       0
 * reslin$V4    0.7232 0.0465  15.5570       0
 * reslin$V5   -1.4818 0.2445  -6.0610       0
 * 
 * 
 * </pre>
 * 
 * @author Robert Bill
 * 
 */
public class CuiSimilarityResource_Impl extends Resource_ImplBase {
	public static final String DB_RESOURCE = "biomedicusDb";
	@ExternalResource(key = DB_RESOURCE, mandatory = true)
	private BiomedicusDBResource_Impl biomedicusDb;

	Connection connection = null;

	@Override
	public void afterResourcesInitialized() {
		// Get connection to biomedicusDb
		try {
			connection = biomedicusDb.getConnection();
		} catch (ResourceProcessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method returns the combined lin+resnik method of measuring semantic
	 * similarity. Because there is evidence that using the combined lin+resnik
	 * measures in logistic regression is best in determining category
	 * membership, that method is implements here.
	 * 
	 * @param cui1
	 *            The first CUI ID string of a pair of CUIs for which the
	 *            similarity score is required.
	 * @param cui2
	 *            The second CUI ID string of a pair of CUIs for which the
	 *            similarity score is required.
	 * @return The combined lin+resnik semantic similarity measure for the 2
	 *         cuis.
	 */
	public double getSimilarity(String cui1, String cui2) {
		double relatedness = 0.0;
		// Currently, this is wired to the lin + resnik score because those used
		// in a logistic regression had the highest score in differentiating
		// intra-category from inter-category membership.
		PreparedStatement pstmt = null;
		try {
			pstmt = connection.prepareStatement("SELECT lin, res from similarity where cui1=? and cui2=?");

			// The database keeps CUI pairs ordered, so make sure query has
			// lexicalally lowest CUI first.
			if (cui1.compareTo(cui2) > 0) {
				pstmt.setString(1, cui2);
				pstmt.setString(2, cui1);
			} else {
				pstmt.setString(1, cui1);
				pstmt.setString(2, cui2);
			}

			// Get results
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				double lin = rs.getDouble("lin");
				double resnik = rs.getDouble("res");
				return -2.978 - 0.596 * lin + 2.036 * resnik;
			} else {
				return -1; // no results, so no discernible similarity
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException e) {
			}// do nothing
		}
		return -1; // everything failed, so return no discernible similarity
	}

	/**
	 * Get an array of candidate CUIs for a {@link UmlsConcept}. All CUIs
	 * associated with the concept that have a MetaMap score equal to or greater
	 * than the threshold argument are truned in a List.
	 * 
	 * @param concept
	 *            The target {@link UmlsConcept}.
	 * @param threshold
	 *            The lowest MetaMap score that should be considered.
	 * @return an array of CUIs as [ {@link Cui}].
	 */
	private List<Cui> getCandidateSenses(UmlsConcept concept, int threshold) {
		FSArray cuis = concept.getCuis();
		ArrayList<Cui> candidates = new ArrayList<Cui>();

		for (int i = 0; i < cuis.size(); i++) {
			Cui cui = (Cui) cuis.get(i);
			if (cui != null) {
				// TODO: fix threshold testing
				// if (cui.getScore() > threshold) {
				candidates.add(cui);
				// }
			} else {
				// TODO log that candidate CUI is null
			}
		}
		return Collections.checkedList(candidates, Cui.class);
	}

	/**
	 * If an ambiguous concept has multiple candidate CUIs, then this method
	 * returns the CUI that is most similar to concepts in the surrounding
	 * horizon. The method calculates the most related CUIs
	 * 
	 * @param targetConcept
	 *            The UmlsConcept that is the target concept needed
	 *            disambiguated.
	 * @param horizon
	 *            The list of UmlsConcepts that are in the surrounding context.
	 * @return The CUI that is most similar to the context/horizon.
	 */
	public Cui getMostSimilarCui(UmlsConcept targetConcept, List<UmlsConcept> horizon) {
		double max = 0.0; // cumulative similarity score
		int survivor = 0; // the index of the surviving CUI (CUI with highest
		                  // potential similarity

		// Get candidate CUIs for target concept
		List<Cui> candidateCuis = getCandidateSenses(targetConcept, 900);
		if (candidateCuis.size() == 0)
			throw new IllegalArgumentException("There are no candidate CUIs for target concept!");

		// Measure the similarity for each candidate CUI with the context CUIs
		// to find the candidate with greatest potential similarity.
		for (int i = 0; i < candidateCuis.size(); i++) {
			double sumOfRelatedness = 0;
			String t = candidateCuis.get(i).getCuiId();

			// Loop through context
			for (int j = 0; j < horizon.size(); j++) {
				// if a concept in the context is also ambiguous, only consider
				// the closest-related candidate in the max relatedness of the
				// target concept.
				double maxNeighbor = -1;
				for (Cui cui : getCandidateSenses(horizon.get(j), 900)) {
					String c = cui.getCuiId();
					double m = getSimilarity(t, c);
					if (m > maxNeighbor) {
						maxNeighbor = m;
					}
				}
				sumOfRelatedness += maxNeighbor;
			}
			double average = sumOfRelatedness / horizon.size();
			if (average > max) {
				max = average;
				survivor = i;
			}
		}
		return candidateCuis.get(survivor);
	}

	/**
	 * Given a target CUI and a neighboring concept that has multiple candidate
	 * CUIs, return the score of the most similar pair.
	 * 
	 * @param target
	 *            The target CUIs.
	 * @param otherConcept
	 *            The list of CUIs for a neighboring ambiguous concept.
	 * @return The score for the most similar CUI pair.
	 */
	public double getMaxSimilarity(Cui target, UmlsConcept otherConcept) {
		double maxSimilarity = -1;
		FSArray otherCuis = otherConcept.getCuis();
		for (FeatureStructure candidateFS : otherCuis.toArray()) {
			Cui candidate = (Cui) candidateFS;
			double similarity = getSimilarity(target.getCuiId(), candidate.getCuiId());
			if (similarity > maxSimilarity) {
				maxSimilarity = similarity;
			}
		}
		return maxSimilarity;
	}
}
