package edu.umn.biomedicus.wsd;

/**
 * This abstract class is implemented by all classes that calculate a semantic
 * similarity measure.
 * 
 * @author Robert Bill
 */
public interface SimilarityMeasure {
	public double getRelatedness(String cui1, String cui2);
}
