/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.wsd.similarity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.uima.UIMAException;

import edu.umn.biomedicus.umls.UMLSInterface;
import edu.umn.biomedicus.wsd.SimilarityMeasure;

/**
 * Lesk similarity measure algorithm.
 * 
 * @author Bridget T McInnes
 * @author Siddharth Patwardhan
 * @author Serguei Pakhomov
 * @author Ted Pedersen
 * @author Ying Liu
 * @author Robert Bill
 */
public class Lesk extends DefinitionContentMeasure implements SimilarityMeasure {

	public Lesk(UMLSInterface umls) {
		this.umls = umls;
	}

	@Override
	public double getRelatedness(String concept1, String concept2) {
		if (concept1 == null || concept2 == null) {
			return -1.0; // no meaningful score possible
		}
		String def1 = "";
		String def2 = "";
		try {
			def1 = getDefinition(concept1);
			def2 = getDefinition(concept2);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UIMAException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (def1 == null || def2 == null) {
			return -1; // no meaningful score possible
		}
		HashMap<String, Integer> overlaps = OverlapFinder.getOverlaps(def1, def2);
		double score = 0;
		for (String overlap : overlaps.keySet()) {
			int length = 0;
			int num = 0;
			double value = 0;

			if (overlap.matches("definitionstop")) {
				String[] array = overlap.split("\\s+");
				String overlap_string = "";
				int i = 0;
				for (String s : array) {
					i++;
					if (!s.matches("definitionstop")) {
						length++;
						overlap_string += s;
					}
					if (s.matches("definitionstop") && length < array.length && length > 0) {
						num = overlaps.get(overlap);
						value = num * (Math.pow(length, 2));
						score += value;
						length = 0;
						overlap_string = "";
					}
					if (s.matches("definitionstop") && length == 0)
						continue;
					if (!s.matches("definitionstop") && i == array.length) {
						overlap_string += s + " ";
						num = overlaps.get(overlap);
						value = num * (Math.pow(length, 2));
						score += value;
						length = 0;
						overlap_string = "";
					}
				}
			} else {
				String[] array = overlap.split("\\s+");
				length = array.length;
				num = overlaps.get(overlap);
				value = num * (Math.pow(length, 2));
				score += value;
			}
		}
		return score;
	}
}
