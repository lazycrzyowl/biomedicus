package edu.umn.biomedicus.wsd.similarity;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.factory.JCasFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Token;
import edu.umn.biomedicus.umls.UMLSInterface;

public class DefinitionContentMeasure {
	protected UMLSInterface umls;

	public void init(UMLSInterface umls) {
		this.umls = umls;
	}

	public double getRelatedness(String c1, String c2) {
		return -1.0;
	};

	protected String getDefinition(String cui) throws IllegalArgumentException, SQLException, UIMAException,
	                                IOException {
		String def = "";
		if (cui == null)
			return def;
		if (umls == null)
			throw new SQLException("The connection to a umls datastore is null.");
		String[] rel = { "PAR", "CHD", "ST" };
		String[] sab = { "SNOMEDCT", "MSH", "MDR" };
		HashSet<String> relations = new HashSet<String>(Arrays.asList(rel));
		HashSet<String> sources = new HashSet<String>(Arrays.asList(sab));
		HashSet<String> defs = umls.getExtendedDefinition(cui, relations, sources);

		for (String def1 : defs) {
			// each def is CUI REL CUI SAB : definition, but this should be
			// encapsulated. Just add the definition part after the ":"
			def = def + def1 + " definitionstop ";
		}

		TypeSystemDescription typeDesc = ComponentFactory.getTypeSystem();
		JCas jCas = JCasFactory.createJCas(typeDesc);
		jCas.setDocumentText(def);
		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();

		// TODO replace porter stemmer with LVG
		// AnalysisEngineDescription stem = ComponentFactory.createStemmer();
		AnalysisEngineDescription stop = ComponentFactory.createStopwordAnnotator();

		SimplePipeline.runPipeline(jCas, tokens, stop);
		def = "";
		for (Token token : JCasUtil.select(jCas, Token.class)) {
			String text = token.getCoveredText();
			if (isSymbol(text))
				continue;
			// if (text.equals("") || token.getIsStopword())
			// continue;
			String stemmedText = token.getStem();
			// if (stemmedText != null) {
			// def += stemmedText + " ";
			// } else {
			def += text + " ";
			// }
		}
		return def;
	}

	private boolean isSymbol(String token) {
		return (!token.matches("[a-zA-Z0-9]*"));
	}
}
