package edu.umn.biomedicus.wsd.similarity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class OverlapFinder {
  private static final String MARKER = "###";

  /**
   * This class identifies all the overlapping substrings present in two strings.
   * 
   * @param string0
   *          A string of words.
   * @param string1
   *          Another string of words.
   * @return
   */
  public static HashMap<String, Integer> getOverlaps(String string0, String string1) {
    // Results are a HashMap with keys that are the overlapping strings and
    // values that are the frequency counts of those overlaps
    HashMap<String, Integer> overlapsHash = new HashMap<String, Integer>();
    int matchStartIndex = 0;
    int currIndex = -1;

    // Strings are compared based on word overlaps, so create arrays of
    // words
    String[] S = null, W = null;

    // assign the shortest string to variable S
    if (string0.length() > string1.length()) {
      W = string0.trim().split("\\s+");
      S = string1.trim().split("\\s+");
    } else {
      S = string0.trim().split("\\s+");
      W = string1.trim().split("\\s+");
    }
    int[] overlapsLengths = new int[S.length];

    while (currIndex < S.length - 1) {
      currIndex++;
      if (contains(W, Arrays.copyOfRange(S, matchStartIndex, currIndex + 1), false)) {
        continue;
      } else {
        // XXX shouldn't this be $currIndex - $matchStartIndex + 1 ?
        overlapsLengths[matchStartIndex] = currIndex - matchStartIndex;
        if (overlapsLengths[matchStartIndex] > 0)
          currIndex--;
        matchStartIndex++;
      }
    }
    for (int i = matchStartIndex; i <= currIndex; i++) {
      overlapsLengths[i] = currIndex - i + 1;
    }
    int longestOverlap = 0;
    for (int i = 0; i < overlapsLengths.length; i++) {
      if (overlapsLengths[i] > longestOverlap)
        longestOverlap = overlapsLengths[i];
    }

    while (longestOverlap > 0) {
      for (int i = 0; i <= overlapsLengths.length - 1; i++) {

        if (overlapsLengths[i] < longestOverlap)
          continue;
        int stringEnd = i + longestOverlap;
        if (contains(W, Arrays.copyOfRange(S, i, stringEnd), true)) {
          String temp = new String(S[i]);
          for (int j = i + 1; j < stringEnd; j++) {
            temp += " " + S[j];
          }

          if (overlapsHash.containsKey(temp)) {
            overlapsHash.put(temp.toString(), overlapsHash.get(temp) + 1);
          } else {
            overlapsHash.put(temp, 1);
          }

          // adjust overlap lengths forward
          for (int j = i; j < i + longestOverlap; j++) {
            overlapsLengths[j] = 0;
          }

          // adjust overlap lengths backward
          for (int j = i - 1; j >= 0; j--) {
            if (overlapsLengths[j] <= i - j)
              break;
            overlapsLengths[j] = i - j;
          }
        } else {
          int k = longestOverlap - 1;
          while (k > 0) {
            stringEnd = i + k - 1;
            if (contains(W, Arrays.copyOfRange(S, i, stringEnd), false))
              break;
            k--;
          }
          overlapsLengths[i] = k;
        }
      }
      longestOverlap = Collections.max(Arrays.asList(longestOverlap)) - 1;
    }
    return overlapsHash;
  }

  private static int maxOverlap(Integer[] overlaps) {
    Integer max = overlaps[0];
    for (int i = 1; i < overlaps.length; i++) {
      if (overlaps[i] != null && overlaps[i] > max) {
        max = overlaps[i];
      }
    }
    if (max == null)
      return 0;
    return max;
  }

  public static boolean contains(String outer, String inner) {
    String[] container = outer.trim().split("\\s+");
    String[] sub = inner.trim().split("\\s+");
    return contains(container, sub, false);
  }

  public static boolean contains(String[] array2, String[] array1, boolean replace) {
    int[] test = findAll(array2, array1, replace);
    if (test.length > 0)
      return true;
    return false;
  }

  // returns true if the first array contains the second array, otherwise
  // returns false.
  // The "replace" parameter determines if the matched strings are replaced
  // with a marker.
  // e.g., contains(ArrayList a, ArrayList b, boolean true);
  public static int[] findAll(String[] array2, String[] array1, boolean replace) {
    ArrayList<Integer> indexes = new ArrayList<Integer>();
    if (array2.length < array1.length || array1.length == 0)
      return new int[0];

    // speed up searching with ff table
    int[] t = createFailFastTable(array1);

    int i = 0, j = 0;

    while (j < array2.length) {
      while (i > -1 && array1[i] != null && !array1[i].equals(array2[j]))
        i = t[i];
      i++;
      j++;
      if (i >= array1.length) {
        indexes.add(i);
        if (replace) {
          for (int x = 0; x < array1.length; x++) {
            array2[j - i + x] = MARKER;
          }
        }
        i = t[i];
      }
    }
    int[] results = new int[indexes.size()];
    for (int x = 0; x < indexes.size(); x++) {
      results[x] = indexes.get(x);
    }
    return results;
  }

  private static int[] createFailFastTable(String[] S) {
    int[] t = new int[S.length + 1]; // the fail fast table
    int i, j;
    int m = S.length - 1;

    i = 0;
    j = t[0] = -1;

    while (i < m) {
      while (j > -1 && !S[i].equals(S[j])) {
        j = t[j];
      }
      i++;
      j++;
      if (S[i] != null && S[i].equals(S[j])) {
        t[i] = t[j];
      } else {
        t[i] = j;
      }
    }
    return t;
  }
}