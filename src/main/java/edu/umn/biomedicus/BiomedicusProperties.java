/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * This class loads the required parameters file. The file should be located in
 * the root of $BIOMEDICUS_HOME and should contain the paths to lvg.properties,
 * lexAccess.properties and the metamap settings.
 * 
 * @author Robert Bill
 */
public class BiomedicusProperties {
	private String biomedicusHome;
	private Properties props = new Properties();

	public BiomedicusProperties() {
		biomedicusHome = System.getenv("BIOMEDICUS_HOME");
		if (biomedicusHome == null) {
			System.err.println("The BIOMEDICUS_HOME directory either does not "
			                                + "exist or is not readable by this user. Check the BIOMEDICUS_HOME "
			                                + "environment variable and try again.");
		}
		ComponentFactory.setBiomedicusHome(biomedicusHome);

		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(new File(biomedicusHome, "biomedicus.properties")));
		} catch (IOException e) {
			System.err.println("Failed to load the biomedicus.properties file. Check the accuracy of the "
			                                + "BIOMEDICUS_HOME environment variable and try again.");
		}

	}

	public static void main(String[] args) {
		String biomedicusHome = System.getenv("BIOMEDICUS_HOME");
		if (biomedicusHome == null) {
			System.err.println("The BIOMEDICUS_HOME directory either does not "
			                                + "exist or is not readable by this user. Check the BIOMEDICUS_HOME "
			                                + "environment variable and try again.");
		}

		Properties props = new Properties();
		props.setProperty("lexAccess.properties.path", "/the/path/to/lexAccess.properties");
		props.setProperty("lvg.properties.path", "/the/path/to/lvg.properties");
		props.setProperty("metamap.server", "localhost");
		props.setProperty("metamap.port", "8066");
		props.setProperty("metamap.config", "-a -I --negex -y -r 900");
		try {
			props.store(new FileOutputStream(new File(biomedicusHome, "biomedicus.properties")), "test");
		} catch (FileNotFoundException e) {
			System.err.println("Unable to find BIOMEDICUS_HOME directory. Check the BIOMEDICUS_HOME environment variable and try again.");
		} catch (IOException e) {
			System.err.println("Unable to write to the file: " + biomedicusHome + "/biomedicus.properties");
		}
	}

	public Properties getProperties() {
		return props;
	}

	public String getProperty(String key) {
		return props.getProperty(key);
	}
}
