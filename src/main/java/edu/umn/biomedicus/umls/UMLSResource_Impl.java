/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.umls;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.uimafit.component.Resource_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;

public class UMLSResource_Impl extends Resource_ImplBase {
	public static final String PARAM_UMLS_JDBCURL = "jdbcUrl";
	@ConfigurationParameter(name = PARAM_UMLS_JDBCURL, defaultValue = "jdbc:mysql://localhost:3306/umls")
	private String jdbcUrl;

	public static final String PARAM_USERNAME = "username";
	@ConfigurationParameter(name = PARAM_USERNAME)
	private String username;

	public static final String PARAM_PASSWORD = "password";
	@ConfigurationParameter(name = PARAM_PASSWORD, defaultValue = "")
	private String password;

	protected Connection con;
	protected Statement stmt;
	private String dburl = "";

	// Make a list of valid relationships that are used in the MRRELS UMLS table
	private static final String[] validRelationships = { "RB", "SY", "AQ", "PAR", "RN", "SIB", "RO", "CHD", "RQ", "RL",
	                                "QB", "ST" };

	@Override
	public void afterResourcesInitialized() {
		con = null;
		try {
			java.lang.Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			this.con = DriverManager.getConnection(jdbcUrl, username, password);
			stmt = con.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
			con = null;
		}
	}

	@Override
	public void destroy() {
		try {
			this.stmt.close();
			this.con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		super.destroy();
	}

	/**
	 * Gets the definitions of a CUI from the MRDEF table in UMLS.
	 * 
	 * @param cui
	 *            The String containing the CUI (e.g., "C0000112")
	 * @return ArrayList of CuiDefinition objects, one for each definition.
	 * @throws SQLException
	 */
	public ArrayList<CuiDefinition> getCuiDefinition(String cui) throws SQLException {
		ResultSet rs = getQueryResultSet("SELECT DISTINCT(def), sab FROM MRDEF WHERE cui=?", cui);
		ArrayList<CuiDefinition> results = new ArrayList<CuiDefinition>();
		while (rs.next()) {
			CuiDefinition cuiDef = new CuiDefinition(cui, rs.getString(1), rs.getString(2));
			results.add(cuiDef);
		}
		return results;
	}

	/**
	 * Constructs an extended definition using all of the specified
	 * relationships that exist in the specified sources (SAB). An extended
	 * definition is the collection of definitions used for the specified CUI as
	 * well as definitions of relatives such as parent or child CUIs.
	 * 
	 * @param cui
	 *            Concept Unique Identifier in a String that is a concept for
	 *            which an extended definition should be retrieved.
	 * @param relationships
	 *            A String array (String[]) including all the UMLS abbreviated
	 *            relationships in all capitals. For example, {"PAR","CHD"}
	 *            would include related parent and child cui definitions.
	 *            Important: semantic type is considered a relationship for this
	 *            method, so if you wish to include the semantic type
	 *            definition, include "ST" in this relationships array.
	 * @param sab
	 *            UMLS source abbreviations that should be searched for related
	 *            definitions. For example,{"SNOMEDCT","MDR"} would include the
	 *            snowmedct sources and MedDra sources when looking for
	 *            definitions, but would exclude all others.
	 * @return
	 * @throws SQLException
	 */
	public synchronized HashSet<String> getExtendedDefinition(String cui, Set<String> relationships, Set<String> sources)
	                                throws SQLException, IllegalArgumentException {
		// TODO test sources (sab) for valid contents that are in UMLS
		HashSet<String> definitions = new HashSet<String>();

		// add cui definitions
		ArrayList<CuiDefinition> cuiDefs = this.getCuiDefinition(cui);
		for (CuiDefinition cuiDef : cuiDefs) {
			definitions.add(cuiDef.getDefinition());
		}

		// add related cui definitions
		for (String relation : relationships) {
			// test if requested relationships are valid
			boolean invalid = true; // assume input is not valid
			for (int i = 0; i < validRelationships.length; i++) {
				if (relation.equals(validRelationships[i])) {
					invalid = false; // found in predefined list, is valid
				}
			}
			if (invalid) {
				throw new IllegalArgumentException(relation + " is not a valid relationship in UMLS2011.");
			}

			// direct special relationships to appropriate method
			if (relation.equals("ST")) {
				definitions.addAll(getSemanticTypes(cui));
			} else {
				definitions.addAll(getRelatives(cui, relation));
			}
		}
		return definitions;
	}

	// get definitions of relatives to include in extended definition.
	private ArrayList<String> getRelatives(String cui, String relation) throws SQLException {
		String sql = "SELECT DISTINCT cui2 FROM MRREL WHERE cui1=? AND rel=? AND cui2!=?";
		ArrayList<String> relatives = new ArrayList<String>();
		ResultSet rs = getQueryResultSet(sql, cui, relation, cui);
		while (rs.next()) {
			String relativeCui = rs.getString(1);
			ArrayList<CuiDefinition> cuiDefs = getCuiDefinition(relativeCui);
			for (CuiDefinition cuiDef : cuiDefs) {
				relatives.add(cuiDef.getDefinition());
				// relatives.add(cui + " " + relation + " " + rs.getString(1) +
				// " " + cuiDef.getSab() +
				// " : "
				// + cuiDef.getDefinition());
			}
		}
		return relatives;
	}

	// get semantic type strings to use in extended definition
	private ArrayList<String> getSemanticTypes(String cui) throws SQLException {
		ResultSet rs = getQueryResultSet("SELECT abr, def from MRSTY, SRDEF WHERE TUI=UI and CUI=?", cui);
		ArrayList<String> results = new ArrayList<String>();
		while (rs.next()) {
			String abr = rs.getString(1);
			String def = rs.getString(2);
			results.add(def);
		}
		return results;
	}

	// convenience method for working with UMLS database
	private String getStringFromQueryResults(String sql, String... params) throws SQLException {
		ResultSet rs = getQueryResultSet(sql, params);
		if (rs.next()) {
			return rs.getString(1); // assume index 1
		}
		return null;
	}

	// convenience method for working with UMLS database
	private Set<String> getColumnSetFromQueryResults(String sql, String... params) throws SQLException {
		ResultSet rs = getQueryResultSet(sql, params);
		HashSet<String> results = new HashSet<String>();
		while (rs.next()) {
			results.add(rs.getString(1));
		}
		return results;
	}

	// convenience method for working with UMLS database
	private ArrayList<String> getRowFromQueryResults(String sql, String... params) throws SQLException {
		ResultSet rs = getQueryResultSet(sql, params);
		ArrayList<String> results = new ArrayList<String>();
		while (rs.next()) {
			int columnCount = rs.getMetaData().getColumnCount();
			for (int i = 0; i < columnCount; i++) {
				results.add(rs.getString(i + 1));
			}
		}
		return results;
	}

	// convenience method for working with UMLS database
	private ResultSet getQueryResultSet(String sql, String... params) throws SQLException {
		PreparedStatement query = con.prepareStatement(sql);
		for (int i = 0; i < params.length; i++) {
			query.setString(i + 1, params[i]);
		}
		if (query.execute()) {
			ResultSet rs = query.getResultSet();
			return rs;
		}
		return null;
	}
}
