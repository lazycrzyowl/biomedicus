package edu.umn.biomedicus.umls;

/**
 * CUI definition class. Holds results of UMLS queries for CUI definitions.
 * 
 * @author Robert Bill
 * 
 */
public class CuiDefinition {
  protected String cui = "";
  protected String sab = "";
  protected String def = "";

  public CuiDefinition(String cui, String def, String sab) {
    this.cui = cui;
    this.sab = sab;
    this.def = def;
  }

  public String getSab() {
    return this.sab;
  }

  public void setSab(String sab) {
    this.sab = sab;
  }

  public String getDefinition() {
    return this.def;
  }

  public void setDefinition(String def) {
    this.def = def;
  }

}
