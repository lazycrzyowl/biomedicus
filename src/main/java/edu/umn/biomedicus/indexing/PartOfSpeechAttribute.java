package edu.umn.biomedicus.indexing;

import org.apache.lucene.util.Attribute;

public interface PartOfSpeechAttribute extends Attribute {
	public static enum PartOfSpeech {
		NN, NNP, NP, VB, VBZ, ADJ, DET, PP, UNK
	}

	public void setPartOfSpeech(PartOfSpeech pos);

	public PartOfSpeech getPartOfSpeech();

}
