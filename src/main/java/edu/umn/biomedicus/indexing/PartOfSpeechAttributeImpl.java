package edu.umn.biomedicus.indexing;

import org.apache.lucene.util.AttributeImpl;

public class PartOfSpeechAttributeImpl extends AttributeImpl implements PartOfSpeechAttribute {

	private PartOfSpeech pos = PartOfSpeech.UNK;

	public void setPartOfSpeech(PartOfSpeech pos) {
		this.pos = pos;
	}

	public PartOfSpeech getPartOfSpeech() {
		return pos;
	}

	@Override
	public void clear() {
		pos = PartOfSpeech.UNK;
	}

	@Override
	public void copyTo(AttributeImpl target) {
		((PartOfSpeechAttribute) target).setPartOfSpeech(pos);
	}
}
