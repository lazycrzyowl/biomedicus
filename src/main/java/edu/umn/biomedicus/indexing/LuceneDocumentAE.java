package edu.umn.biomedicus.indexing;

import edu.umn.biomedicus.type.*;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import java.io.File;
import java.io.IOException;

//import pitt.search.lucene.FilePositionDoc;

/**
 * A Lucene index writer for clinical CAS. This analysis engine creates a Lucene
 * index for a corpus. Each document in the corpus is indexed using the standard
 * analyzer; however, the phrases, medical concepts, CUIs and acronyms are also
 * indexed separated so that it is possible to search specifically on those
 * fields.
 * 
 * Phrases are also indexed. The format of that field is specific because the
 * phrase payload contains the phrase type (e.g., NP, VP, etc.).
 * 
 * An example search: "content:heart AND edu.umn.biomedicus.acronym:CHF* AND phrase:stent*"
 * 
 * @author Robert Bill
 * 
 */
public class LuceneDocumentAE extends JCasAnnotator_ImplBase {

	// location of the FSDirectory for the lucene index
	public static final String PARAM_INDEX_DIR = "indexDir";
	@ConfigurationParameter(name = PARAM_INDEX_DIR, mandatory = true)
	private String indexDir;

	public static final String PARAM_INDEX_PHRASES = "indexPhrases";
	@ConfigurationParameter(name = PARAM_INDEX_PHRASES, mandatory = false, defaultValue = "false")
	private Boolean indexPhrases;

	private IndexWriter iwriter;
	private FSDirectory directory;
	private static Version luceneVersion = Version.LUCENE_36;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		Analyzer analyzer = new StandardAnalyzer(luceneVersion);

		try {
			directory = FSDirectory.open(new File(indexDir));
			IndexWriterConfig iconfig = new IndexWriterConfig(luceneVersion, analyzer);
			iwriter = new IndexWriter(directory, iconfig);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The process method of
	 * 
	 */
	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		Document doc = new Document();
		// TODO: add documentID. Should normalize documentID to be in
		// METADATA_VIEW

		// Index content of clinical note
		doc.add(new Field("content", jCas.getDocumentText(), Field.Store.NO, Field.Index.ANALYZED));

		// Index concepts and associated CUIS that appear in clinical note.
		for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
			doc.add(new Field("concept", concept.getCoveredText().trim(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			String preferredTerm = concept.getPreferredTerm();
			if (preferredTerm != null) {
				doc.add(new Field("concept", preferredTerm.trim(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			Cui cui = (Cui) concept.getCuiSense();
			if (cui != null) {
				doc.add(new Field("cui", cui.getCuiId().trim(), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
		}

		// for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class))
		// {
		// final UmlsConcept c = concept;
		// doc.add(new TextField("concept", new TokenStream() {
		// boolean done = false;
		//
		// public org.apache.lucene.analysis.Token
		// next(org.apache.lucene.analysis.Token reusableToken)
		// throws IOException {
		// if (done)
		// return null;
		// done = true;
		// reusableToken.append(c.getCoveredText().trim());
		// // reusableToken.setPayload(new byte[] { 1 });
		// return reusableToken;
		// }
		//
		// @Override
		// public boolean incrementToken() throws IOException {
		// return false;
		// }
		// }));
		//
		// }
		// In order to search by part-of-speech, add pos index
		for (Token tok : JCasUtil.select(jCas, Token.class)) {
			doc.add(new Field("pos", tok.getPos().trim(), Field.Store.YES, Field.Index.NOT_ANALYZED));
		}

		// Index acronyms separately
		for (Acronym nym : JCasUtil.select(jCas, Acronym.class)) {
			doc.add(new Field("edu.umn.biomedicus.acronym", nym.getCoveredText().trim(), Field.Store.YES, Field.Index.NOT_ANALYZED));
		}

		for (Phrase phrase : JCasUtil.select(jCas, Phrase.class)) {
			String pType = phrase.getLabel();
			doc.add(new Field(pType, phrase.getCoveredText().trim(), Field.Store.NO, Field.Index.NOT_ANALYZED));
		}

		try {
			iwriter.addDocument(doc);
		} catch (CorruptIndexException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new AnalysisEngineProcessException();
		}

		String positionalIndexDir = indexDir + "_positional";
		IndexWriterConfig iconf = new IndexWriterConfig(Version.LUCENE_36, new StandardAnalyzer(Version.LUCENE_36));

		try {
			IndexWriter writer = new IndexWriter(FSDirectory.open(new File(positionalIndexDir)), iconf);

			final File docDir = new File(indexDir);
			indexDocs(writer, docDir);
			writer.close();
		} catch (IOException e) {
			// TODO replace template catch clause
			System.err.println("Failed to generate positional index file for semanticvectors.");
			e.printStackTrace();
			throw new AnalysisEngineProcessException();
		}
	}

	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		try {
			iwriter.close();
			directory.close();
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void destroy() {
		try {
			iwriter.close();
		} catch (IOException e) {
		}
	}

	static void indexDocs(IndexWriter writer, File file) throws IOException {
		// Do not try to index files that cannot be read.
		if (file.canRead()) {
			if (file.isDirectory()) {
				String[] files = file.list();
				// An IO error could occur.
				if (files != null) {
					for (int i = 0; i < files.length; i++) {
						// Skip dot files.
						if (!files[i].startsWith(".")) {
							indexDocs(writer, new File(file, files[i]));
						}
					}
				}
			} else {
				//try {
                    // TODO: add positional index
					// writer.addDocument(FilePositionDoc.Document(file));
				//} catch (FileNotFoundException fnfe) {
				//	fnfe.printStackTrace();
				//}
			}
		}
	}
}

// Directory dir = new RAMDirectory();
// Analyzer a = new SimpleAnalyzer();
// IndexWriter writer = new IndexWriter(dir, a, MaxFieldLength.UNLIMITED);
// Document doc = new Document();
// doc.add(new Field("a", "abc", Store.NO, Index.NOT_ANALYZED));
// final Term t = new Term("a", "abc");
// doc.add(new Field(t.field(), new TokenStream() {
// boolean done = false;
// @Override
// public Token next(Token reusableToken) throws IOException {
// if (done) return null;
// done = true;
// reusableToken.setTermBuffer(t.text());
// reusableToken.setPayload(new Payload(new byte[] { 1 }));
// return reusableToken;
// }
// }));
// writer.addDocument(doc);
// writer.commit();
// writer.close();
//
// IndexReader reader = IndexReader.open(dir, true);
// TermPositions tp = reader.termPositions(t);
// tp.next();
// tp.nextPosition();
// System.out.println(tp.getPayloadLength());
// reader.close();
