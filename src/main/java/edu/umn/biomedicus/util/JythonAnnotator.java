/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;

/**
 * Simple annotator for writing annotators in Jython
 * 
 * @author Robert Bill
 * 
 */

public class JythonAnnotator extends JCasAnnotator_ImplBase {
	private Logger logger;
	private PythonInterpreter jython;
	public static final String PARAM_JYTHON_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                JythonAnnotator.class, "jythonFile");
	@ConfigurationParameter
	private String jythonFile;
	private JCasAnnotator_ImplBase myAnnotator;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();

		// Read jython module file
		String jythonModulePath = new File(jythonFile).getParentFile().getPath();
		FileInputStream fis;
		try {
			fis = new FileInputStream(jythonFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new ResourceInitializationException();
		}
		Properties props = new Properties();
		props.setProperty("python.path", jythonModulePath.toString());
		PythonInterpreter.initialize(System.getProperties(), props, new String[] { "" });

		jython = new PythonInterpreter();
		jython.execfile(fis);
		// The jython class name must be the same as the jythonFile name without
		// any extension. This is by convention only, but it does make things
		// more clear.
		String jythonFileName = new File(jythonFile).getName();
		int index = jythonFileName.indexOf('.');
		String className = jythonFileName.substring(0, index);
		jython.exec("jythonClassInstance = " + className + "()");
		jython.set("context", context);
		jython.exec("jythonClassInstance.initialize(context)");
		PyObject adapter = jython.get("jythonClassInstance");
		myAnnotator = (JCasAnnotator_ImplBase) adapter.__tojava__(JCasAnnotator_ImplBase.class);
		logger.logrb(Level.INFO, "JythonAnnotator", "typeSystemInit", "initialize",
		                                "edu.umn.biomedicus.token.JythonAnnotator", "jython_annotator_info_initialized");
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		myAnnotator.process(jCas);
	}
}
