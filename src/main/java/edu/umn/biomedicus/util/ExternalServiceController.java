/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.util;

import java.io.IOException;

/**
 * This interface should be implemented by classes that make use of an external
 * service. Metamap is an example of an external service because the mmserver1x
 * must be running to use the MeatmapAE. However, if controlling the service is
 * left to the system, there is problems in divide-and-conquer approaches to
 * large data sets because Biomeducus would not have the ServiceController
 * ability required to start additional Metamap processes.
 * 
 * STATUS: resource pool designs would work well also, and a
 * ExternalResourcePool interface may make this obsolete.
 * 
 * @author Robert Bill
 * 
 */
public interface ExternalServiceController {
	/**
	 * Tries to start the external service.
	 * 
	 * @return true if service is started successfully, false otherwise.
	 */
	public boolean start() throws IOException;

	/**
	 * Tries to stop the external service.
	 * 
	 * @return true if stopped successfully, false otherwise.
	 */
	public boolean stop();

	/**
	 * @return The server address. Nearly always "localhost" for now.
	 */
	public String getServer();

	/**
	 * Because it is common to run a pool of external servers, the port
	 * information will be a list of ports. For example, when using the metamap
	 * external service, the pool of servers will span several ports.
	 * 
	 * @return The ports used by the service.
	 */
	public int[] getPorts();

	/**
	 * @return true if the server passes a health check. false otherwise.
	 */
	public boolean isRunning();
}
