/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.collectionreaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.uima.UimaContext;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.impl.XmiCasDeserializer;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import org.uimafit.component.CasCollectionReader_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.xml.sax.SAXException;

import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.type.MetaData;

/**
 * This is a generic collection reader for XMI files. The assumptions made in
 * this class is that all the files are in the same directory and the list of
 * files is passed in as a parameter instead of reading a directory list or
 * reading a data source. The list passed in must include only the file names.
 * The PARAM_INPUT_DIRECTORY specifies the locations of the files. <br/>
 * <br/>
 * Not defining the file list in this class (e.g., all .xmi files in a certain
 * directory) allows the collection reader factory to filter the file list so
 * that it is possible to subdivide file lists as training and test, or track
 * completed files in recoverable processes (pass in only incompletely processed
 * file names). The parameter for the list of files is the parameter
 * PARAM_FILE_NAMES.
 * 
 * @author Robert Bill
 * 
 */
public class XMICollectionReader extends CasCollectionReader_ImplBase {

	public static final String PARAM_INPUT_DIRECTORY = ConfigurationParameterFactory.createConfigurationParameterName(
	                                XMICollectionReader.class, "inputDirectory");
	@ConfigurationParameter
	private File inputDirectory; // dir containing all files to process

	public static final String PARAM_FILE_NAMES = ConfigurationParameterFactory.createConfigurationParameterName(
	                                XMICollectionReader.class, "fileList");
	@ConfigurationParameter
	private String[] fileList; // list of files to process

	int totalFiles = 0;
	int currentFileIndex = 0;
	UimaContext context;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		this.context = context;

		// Confirm all files appearing in the file list do exist
		for (String fname : fileList) {
			if (!new File(inputDirectory, fname).exists()) {
				System.err.println("Failed to find file that is listed in the 'test.txt' names file. File name: "
				                                + fname);
				throw new ResourceInitializationException();
			}
		}
		totalFiles = fileList.length;
	}

	@Override
	public void getNext(CAS cas) throws IOException, CollectionException {
		/*
		 * make sure getNext is valid. Shouldn't need this if hasNext is
		 * correct. TODO: maybe remove?
		 */
		if (fileList.length < currentFileIndex + 1) {
			throw new RuntimeException("Called 'getNext()' after 'hasNext()' returned false.");
		}

		File currentFile = new File(inputDirectory, fileList[currentFileIndex]);
		try {
			XmiCasDeserializer.deserialize(new FileInputStream(currentFile), cas);
		} catch (SAXException e) {
			e.printStackTrace();
		}

		// add metadata (filename) to jCas
		JCas jCas = null;
		try {
			jCas = cas.getJCas();
			JCas uriView = jCas.createView(ViewNames.META_VIEW);
			uriView.setDocumentText(currentFile.toString());
			MetaData md = new MetaData(jCas, 0, uriView.getDocumentText().length());
			md.setDocumentId(currentFile.getName());
			md.addToIndexes();
		} catch (CASException e) {
			e.printStackTrace();
		}

		// increment current file record
		currentFileIndex++;
	}

	@Override
	public Progress[] getProgress() {
		return new Progress[] { new ProgressImpl(currentFileIndex, fileList.length, Progress.ENTITIES) };
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return (currentFileIndex + 1 <= fileList.length);
	}
}
