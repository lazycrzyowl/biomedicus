package edu.umn.biomedicus.parser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import edu.ohsu.cslu.grammar.LeftCscSparseMatrixGrammar;
import edu.ohsu.cslu.parser.ParseTask;
import edu.ohsu.cslu.parser.ParseTree;
import edu.ohsu.cslu.parser.ParserDriver;
import edu.ohsu.cslu.parser.spmv.CscSpmvParser;
import edu.umn.biomedicus.type.Phrase;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

public class BerkeleyParserAE extends JCasAnnotator_ImplBase {

	public static final String PARAM_GRAMMAR_FILE = "modelFileName";
	@ConfigurationParameter(name = PARAM_GRAMMAR_FILE, mandatory = true)
	private String grammarFileName;

	final ParserDriver opts = new ParserDriver();
	private CscSpmvParser parser;

	// private String grammarFile;
	HashSet<String> phraseLabels;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		InputStream inputReader = null;
		try {
			inputReader = new FileInputStream(grammarFileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LeftCscSparseMatrixGrammar grammar = loadGrammar(inputReader);
		parser = new CscSpmvParser(opts, grammar);

		phraseLabels = new HashSet<String>(Arrays.asList(new String[] { "ADJP", "ADVP", "CONJP", "NP", "PP", "QP",
		                                "VP", "WHADJP", "WHAVP", "WHNP", "WHPP" }));
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		for (Sentence sent : JCasUtil.select(jCas, Sentence.class)) {
			String sentText = sent.getCoveredText();

			// Create a list of the tokens within the sentence for quick lookups
			ArrayList<Token> tokenList = new ArrayList<Token>();
			for (Token tok : JCasUtil.selectCovered(Token.class, sent)) {
				tokenList.add(tok);
			}

			// Parse the sentence
			ParseTask result = parser.parseSentence(sent.getCoveredText());
			if (result != null) {
				sent.setParse(result.parseBracketString(false, false));
			}

			ParseTree tree = null;
			try {
				tree = ParseTree.readBracketFormat(result.parseBracketString(false, false));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int begin = 0;
			int end = 0;
			for (ParseTree phrase : tree.preOrderTraversal()) {
				String content = phrase.getContents();
				if (phrase.isLeaf()) {
					begin += content.length();
				}
				if (phraseLabels.contains(content)) {
					List<String> elements = phrase.getLeafNodesContent();
					begin = sentText.indexOf(elements.get(0), begin);
					end = begin;
					for (String element : elements) {
						end = sentText.indexOf(element, end) + element.length();
					}
					Phrase p = new Phrase(jCas, begin + sent.getBegin(), end + sent.getBegin());
					p.setLabel(content);
					p.addToIndexes();
				}
			}
		}
	}

	protected LeftCscSparseMatrixGrammar loadGrammar(InputStream grammarInput) throws ResourceInitializationException {
		LeftCscSparseMatrixGrammar grammar = null;
		try {
			grammar = new LeftCscSparseMatrixGrammar(new BufferedReader(new InputStreamReader(new GZIPInputStream(
			                                grammarInput))));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new ResourceInitializationException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ResourceInitializationException();
		}
		return grammar;
	}
}
