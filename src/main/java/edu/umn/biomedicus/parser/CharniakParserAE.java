package edu.umn.biomedicus.parser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import edu.ohsu.cslu.parser.ParseTree;
import edu.umn.biomedicus.type.Phrase;
import edu.umn.biomedicus.type.Sentence;

/**
 * This class uses a system call to get results from the charniak parser.
 * 
 * @author Robert Bill
 * 
 */
public class CharniakParserAE extends JCasAnnotator_ImplBase {
	// Parameter: path to charniak parser
	public static final String PARAM_CHARNIAK_PATH = "charniakPath";
	@ConfigurationParameter(name = PARAM_CHARNIAK_PATH, mandatory = true)
	private String charniakPath;

	private HashSet<String> phraseLabels;
	private Runtime runtime;
	private File tmpFile;
	private String command;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		String prefix = charniakPath;
		String parseIt = new File(prefix, "PARSE/parseIt ").toString();
		String terms = new File(prefix, "DATA/EN/ ").toString();
		command = parseIt + terms;

		runtime = Runtime.getRuntime();
		phraseLabels = new HashSet<String>();
		phraseLabels.addAll(Arrays.asList(new String[] { "ADJP", "ADVP", "CONJP", "NP", "PP", "QP", "VP", "WHADJP",
		                                "WHAVP", "WHNP", "WHPP" }));
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		// TODO Write ALL sentences to tmpFile, one-per-line to make reasonable
		// TODO change to use JNI

		for (Sentence sent : JCasUtil.select(jCas, Sentence.class)) {
			String sentText = sent.getCoveredText();
			System.out.println(sentText);
			String tmpFilePath = writeSentToTmpFile(sentText);
			Process proc = null;
			try {
				proc = runtime.exec(command + tmpFile);
				proc.waitFor();
			} catch (IOException e) {
				e.printStackTrace();
				System.err.println("Failed to execute Charniak parser process. Processing halted. (CharniakParserAE:~67)");
				throw new AnalysisEngineProcessException();
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.err.println("Charniak parser process interrupted. Execution halted. (CharniakParserAE:~71");
				throw new AnalysisEngineProcessException();
			}

			BufferedReader b = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = "";
			StringBuilder charnParse = new StringBuilder();

			try {
				while ((line = b.readLine()) != null) {
					charnParse.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			sent.setParse(charnParse.toString().trim());
			tmpFile.delete();

			// read bracketed parse into ParseTree
			ParseTree tree = null;
			try {
				tree = ParseTree.readBracketFormat(charnParse.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}

			// annotate phrases
			int begin = 0;
			int end = 0;
			for (ParseTree phrase : tree.preOrderTraversal()) {
				String content = phrase.getContents();
				if (phrase.isLeaf()) {
					begin += content.length();
				}
				if (phraseLabels.contains(content)) {
					List<String> elements = phrase.getLeafNodesContent();
					begin = sentText.indexOf(elements.get(0), begin);
					end = begin;
					for (String element : elements) {
						end = sentText.indexOf(element, end) + element.length();
					}
					Phrase p = new Phrase(jCas, begin + sent.getBegin(), end + sent.getBegin());
					p.setLabel(content);
					p.addToIndexes();
				}
			}
		}
	}

	protected String writeSentToTmpFile(String sentence) throws AnalysisEngineProcessException {
		String tmpFilePath = null;
		try {
			tmpFile = FileUtil.createTempFile("charniaksent", ".txt");
			FileWriter fw = new FileWriter(tmpFile);
			BufferedWriter b = new BufferedWriter(fw);
			b.write("<s> ");
			b.write(sentence);
			b.write(" </s>\n");
			b.close();
			fw.close();
			tmpFilePath = tmpFile.getAbsolutePath();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("Unable to find the tmp file needed for the charniak parser. Processing halted. (CharniakParserAE:~106)");
			throw new AnalysisEngineProcessException();
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Unable to reset the tmp file necessary for the charniak parser. Processing halted. (CharniakParserAE:~110)");
		}
		tmpFile.setReadable(true);
		return tmpFilePath;
	}
}
