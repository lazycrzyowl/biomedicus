package edu.umn.biomedicus.parser;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.umn.biomedicus.type.Phrase;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import java.io.CharArrayReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class StanfordParserAE extends JCasAnnotator_ImplBase {
	// Parameter: path and name of parser model file.
	public static final String PARAM_PARSER_FILE = "parserFileAndPath";

    private String parserFileAndPath = "edu/umn/biomedicus/parsing/englishPCFG.ser.gz";

	//
	private HashSet<String> phraseLabels;
	private LexicalizedParser parser;
	private TreebankLanguagePack tlp;
	private GrammaticalStructureFactory grammarFactory;
	private String langPack = "edu.stanford.nlp.trees.PennTreebankLanguagePack";

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		phraseLabels = new HashSet<String>();
		phraseLabels.addAll(Arrays.asList(new String[] { "ADJP", "ADVP", "CONJP", "NP", "PP",
                "QP", "VP", "WHADJP", "WHAVP", "WHNP", "WHPP" }));
		parser = LexicalizedParser.loadModel(parserFileAndPath);
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		parser.setOptionFlags("-maxLength", "80", "-retainTmpSubcategories");
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		// LexicalizedParser parser = LexicalizedParser.loadModel(modelPath);
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();

		for (Sentence sent : JCasUtil.select(jCas, Sentence.class)) {
			ArrayList<HasWord> taggedTokenList = new ArrayList<HasWord>();
			for (Token tok : JCasUtil.selectCovered(Token.class, sent)) {
				TaggedWord word = new TaggedWord(tok.getCoveredText(), tok.getPos());
				word.setBeginPosition(tok.getBegin());
				word.setEndPosition(tok.getEnd());
				taggedTokenList.add(word);
			}
			CharArrayReader reader = new CharArrayReader(sent.getCoveredText().toCharArray());

			// The following lines are if the user wants to use Stanford
			// tokenization and POS tags.
			TokenizerFactory<? extends HasWord> tf = (TokenizerFactory) tlp.getTokenizerFactory();
			Tokenizer tok = tlp.getTokenizerFactory().getTokenizer(reader);
			List wordlist = tok.tokenize();
			Tree tree = parser.parseTree(taggedTokenList); // apply(wordlist);
			sent.setParse(tree.toString());

			for (Object child : tree.toArray()) {
				Tree c = (Tree) child;
				if (c.isPhrasal() && phraseLabels.contains(c.value())) {
					ArrayList<TaggedWord> words = c.taggedYield();
					int begin = words.get(0).beginPosition();
					int end = words.get(words.size() - 1).endPosition();
					Phrase p = new Phrase(jCas, begin, end);
					p.setLabel(c.value());
					p.addToIndexes();
				}
			}
		}
	}
}
