/* 
  Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.local;

/*
 * This class runs a validation of the edu.umn.biomedicus.acronym detector. It does not do cross validation yet, but this provides enough feedback to guide development for now.
 * 
 * NOTES:
 * Currently, there is a large number of false positives due to the gold corpus not having all acronyms annotated. Only target acronyms are annotated. 
 * A manual review of the false positives resulted in 205 false positives, making for an actual precision score of 0.9865 and an F-score of 0.9731. 
 */
import java.io.File;
import java.io.IOException;

import edu.umn.biomedicus.acronym.AcronymDetectorAE;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.component.ViewTextCopierAnnotator;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.acronym.AcronymDetectionEvaluator;
import edu.umn.biomedicus.acronym.AcronymFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;

public class AcronymDetectionXValidationPipeline {

	public static void main(String[] args) throws UIMAException, IOException {
		// get folder directory
		String biomedicusHome = System.getenv("BIOMEDICUS_HOME");
		String corpusLocation = "resources/corpora/edu.umn.biomedicus.acronym";

		// get type system
		final TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

		// Get corpus reader
		System.out.println(new File(biomedicusHome, corpusLocation).toString());
		CorpusFactory corpusFactory = new AcronymFactory(new File(biomedicusHome, corpusLocation).toString());
		CollectionReader reader = corpusFactory.createTestReader();

		// copy GOLD view text into the SYSTEM view
		final AnalysisEngineDescription textCopier = AnalysisEngineFactory.createPrimitiveDescription(
		                                ViewTextCopierAnnotator.class, typeSystem,
		                                ViewTextCopierAnnotator.PARAM_SOURCE_VIEW_NAME, ViewNames.GOLD_VIEW,
		                                ViewTextCopierAnnotator.PARAM_DESTINATION_VIEW_NAME, ViewNames.SYSTEM_VIEW);

		// Create edu.umn.biomedicus.acronym annotator
		AnalysisEngineDescription acronym = AnalysisEngineFactory.createPrimitiveDescription(AcronymDetectorAE.class,
		                                typeSystem, AcronymDetectorAE.PARAM_LEXICON_CONFIG_FILE,
		                                ComponentFactory.getResourcePath("config/lexAccess.properties"),
		                                AcronymDetectorAE.PARAM_MODEL_FILE,
		                                ComponentFactory.getResourcePath("models/acronyms/acronymMEModel.bin"));

		// The evaluator will compare the part-of-speech tags in the SYSTEM_VIEW
		// with those in the GOLD_VIEW
		final AnalysisEngineDescription evaluator = AnalysisEngineFactory.createPrimitiveDescription(
		                                AcronymDetectionEvaluator.class, typeSystem);

		// Use a builder to assemble the components
		final AggregateBuilder builder = new AggregateBuilder();
		builder.add(textCopier);
		builder.add(acronym, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW);
		builder.add(evaluator);

		// runs the collection reader and the aggregate AE.
		SimplePipeline.runPipeline(reader, builder.createAggregate());
	}
}
