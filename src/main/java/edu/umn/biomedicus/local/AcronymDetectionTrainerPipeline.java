/* 
  Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.local;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;

import opennlp.maxent.BasicEventStream;
import opennlp.maxent.GIS;
import opennlp.maxent.PlainTextByLineDataStream;
import opennlp.maxent.io.GISModelWriter;
import opennlp.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.model.AbstractModel;
import opennlp.model.EventStream;
import opennlp.model.OnePassRealValueDataIndexer;

import org.kohsuke.args4j.Option;

import edu.umn.biomedicus.acronym.AcronymFeatureExtractor;
import edu.umn.biomedicus.acronym.AcronymManifest;
import edu.umn.biomedicus.util.Options_ImplBase;
import gov.nih.nlm.nls.lexAccess.Api.LexAccessApi;

/**
 * This class trains the edu.umn.biomedicus.acronym detection model using a gold corpus that
 * contains edu.umn.biomedicus.acronym annotations and is in .xmi format. In order to run this
 * class, you must set the system property of "biomedicus.home" to the root
 * directory in which biomedicus files exist. Additionally, you must set the
 * corpus path on the command line using the '-c' option. For example:<br />
 * <ul>
 * java -Dbiomedicus.home=/workspace/Biomedicus -c /path/to/gold/corpus
 * </ul>
 * 
 * @author Robert Bill
 * 
 */
public class AcronymDetectionTrainerPipeline {
	public static class Options extends Options_ImplBase {
		// A required command-line argument pointing to the directory containing
		// the gold corpus of acronyms.
		@Option(name = "-c", aliases = "--corpusDir", usage = "Specify a path to the folder containing the gold edu.umn.biomedicus.acronym corpus.", required = true)
		public String corpusPath;
	}

	private static RandomAccessFile eventFile = null;
	static AcronymFeatureExtractor afe;
	static LexAccessApi lex;

	/**
	 * This class trains the edu.umn.biomedicus.acronym detection model file.
	 */
	public static void main(String[] args) throws Exception {
		Options options = new Options();
		options.parseArgument(args);

		String biomedicusHomeDir = System.getenv("BIOMEDICUS_HOME");
		if (biomedicusHomeDir == null) {
			System.err.println("No 'biomedicus.home' property set. Try 'export BIOMEDICUS_HOME=/path/to/biomedicus' on the command line before running this application.");
			throw new RuntimeException();
		}

		// get data path. The "lexAccess.properties" placed in the Biomedicus
		// external resources folder
		String dataPath = biomedicusHomeDir;
		String lexConfigFile = new File(dataPath, "/resources/config/lexAccess.properties").toString();

		// create the lexicon and featureExtractor instances
		lex = new LexAccessApi(lexConfigFile);
		FileInputStream fis = new FileInputStream(new File(dataPath,
		                                "/src/main/resources/edu/umn/biomedicus/edu.umn.biomedicus.acronym/AcronymManifest.csv"));
		AcronymManifest manifest = new AcronymManifest(fis);
		afe = new AcronymFeatureExtractor(manifest, lex);

		// define the event file and the model file
		String resourcePath = new File(dataPath, "/resources/models/acronyms/").toString();
		String eventFileName = "eventFile.dat";
		String modelFileName = "acronymMEModel.bin";
		eventFile = new RandomAccessFile(new File(resourcePath + eventFileName), "rw");

		// define the directory that contains training data
		File goldDirectory = new File(options.corpusPath);
		File[] files = goldDirectory.listFiles();
		for (File gData : files) {
			String acronym = gData.getName().substring(15);
			acronym = acronym.substring(0, acronym.indexOf("Window"));
			BufferedReader br = new BufferedReader(new FileReader(gData));
			String line;
			Random rand = new Random();
			int acronymIndex = 0;
			int randWordIndex = 0;

			while ((line = br.readLine()) != null) {
				acronymIndex = -1;
				String[] data = line.split("\\|");
				String[] tokens = data[2].split("\\s");
				if (tokens.length < 5)
					continue;
				randWordIndex = rand.nextInt(tokens.length - 1);
				for (int i = 0; i < tokens.length; i++) {

					if (normalize(tokens[i]).equals(normalize(acronym))) {
						acronymIndex = i;
						break;
					}
				}
				if (acronymIndex == -1)
					continue;
				int count = 0;
				String pattern = "[A-Z]*|.*[0-9\\.\\/\\-].*";
				while (randWordIndex == acronymIndex || tokens[randWordIndex].matches(pattern)) {
					randWordIndex = rand.nextInt(tokens.length - 1);
					if (count > 20)
						break;
					count++;
				}

				processToken(tokens[acronymIndex], true);
				processToken(tokens[randWordIndex], false);
			}
			br.close();
		}
		eventFile.close();

		FileReader eventFileReader = null;

		try {
			eventFileReader = new FileReader(new File(resourcePath, eventFileName));
			EventStream es = new BasicEventStream(new PlainTextByLineDataStream(eventFileReader));
			GIS.SMOOTHING_OBSERVATION = 0.1;
			AbstractModel model = GIS.trainModel(100, new OnePassRealValueDataIndexer(es, 5), true);
			File modelFile = new File(resourcePath + modelFileName);
			GISModelWriter modelWriter = new SuffixSensitiveGISModelWriter(model, modelFile);
			modelWriter.persist();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String normalize(String candidate) {
		String symbols = "-.&";
		candidate = candidate.toLowerCase();
		StringBuilder normalizer = new StringBuilder();
		for (char c : candidate.toCharArray()) {
			if (symbols.indexOf(c) == -1)
				normalizer.append(c);
		}
		return normalizer.toString();
	}

	public static void processToken(String candidate, boolean isAcronym) throws IOException {
		eventFile.writeBytes(candidate + " ");
		if (afe.inRieaSet(candidate))
			eventFile.writeBytes("inRieaSet ");
		eventFile.writeBytes("V2L=" + afe.getV2L(candidate) + " ");
		eventFile.writeBytes("len=" + candidate.length() + " ");
		if (afe.meetsMinimumLength(candidate))
			eventFile.writeBytes("meetsMinLength ");
		if (afe.meetsMaximumLength(candidate))
			eventFile.writeBytes("meetsMaxLength ");
		if (afe.containsAlphaCharacters(candidate))
			eventFile.writeBytes("containsAlpha ");
		if (afe.allCaps(candidate))
			eventFile.writeBytes("allCaps ");
		if (afe.containsNumber(candidate))
			eventFile.writeBytes("containsnumber ");
		if (afe.internalPeriodOrDash(candidate))
			eventFile.writeBytes("internalPeriodOrDash ");
		if (afe.hasConsonantSeparatedVowels(candidate))
			eventFile.writeBytes("punctuatedVowels ");
		if (isAcronym) {
			eventFile.writeBytes("edu.umn.biomedicus.acronym\n");
		} else {
			eventFile.writeBytes("notAcronym\n");
		}
	}
}
