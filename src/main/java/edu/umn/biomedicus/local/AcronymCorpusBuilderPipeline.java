/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.local;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.corpora.acronym.AcronymGoldReader;
import edu.umn.biomedicus.metamap.MetaMapAE;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * Class for internal use at UMN. Not useful to others.
 * 
 * @author Robert Bill
 * 
 */
public class AcronymCorpusBuilderPipeline implements org.uimafit.component.xwriter.XWriterFileNamer {

	public static class Options extends Options_ImplBase {

	}

	public static void main(String[] args) throws Exception {

		Options options = new Options();
		options.parseArgument(args);
		CollectionReaderDescription crd = CollectionReaderFactory.createDescription(AcronymGoldReader.class,
		                                ComponentFactory.getTypeSystem());
		CollectionReader reader = CollectionReaderFactory.createCollectionReader(crd);

		AnalysisEngineDescription metamap = AnalysisEngineFactory.createPrimitiveDescription(MetaMapAE.class,
		                                ComponentFactory.getTypeSystem(), MetaMapAE.PARAM_METAMAP_SERVER, "localhost",
		                                MetaMapAE.PARAM_METAMAP_PORT, "8066");
		AnalysisEngineDescription xWriter = AnalysisEngineFactory.createPrimitiveDescription(XWriter.class,
		                                ComponentFactory.getTypeSystem(), XWriter.PARAM_OUTPUT_DIRECTORY_NAME,
		                                "/workspace/Biomedicus/resources/corpora/edu.umn.biomedicus.acronym/xmi");

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(metamap, CAS.NAME_DEFAULT_SOFA, ViewNames.GOLD_VIEW);
		builder.add(xWriter);
		SimplePipeline.runPipeline(reader, builder.createAggregate());
	}

	public String nameFile(JCas jCas) {
		// String uri = ViewURIUtil.getURI(jCas);
		return "Not implemented.";
	}
}
