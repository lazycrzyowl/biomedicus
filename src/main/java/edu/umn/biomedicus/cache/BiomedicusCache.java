package edu.umn.biomedicus.cache;

public abstract interface BiomedicusCache {
	public void set(String key, String value);

	public String get(String key);
}
