/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus;

import java.io.File;

import edu.umn.biomedicus.acronym.AcronymDetectorAE;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.component.ViewTextCopierAnnotator;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.uimafit.factory.JCasFactory;
import org.uimafit.factory.TypeSystemDescriptionFactory;

import edu.umn.biomedicus.acronym.AcronymConceptMapper;
import edu.umn.biomedicus.corpora.jdbc.JdbcReader;
import edu.umn.biomedicus.evaluation.CorpusFactory;
import edu.umn.biomedicus.metamap.MetaMapAE;
import edu.umn.biomedicus.metamap.UmlsConceptAnnotator;
import edu.umn.biomedicus.parser.StanfordParserAE;
import edu.umn.biomedicus.pos.tnt.TntPosAnnotator;
import edu.umn.biomedicus.sentence.LineAnnotator;
import edu.umn.biomedicus.sentence.MaxentSentenceBoundaryDetector;
import edu.umn.biomedicus.symbol.SymbolSenseAnnotator;
import edu.umn.biomedicus.token.BreakIteratorTokenizer;
import edu.umn.biomedicus.token.StemAnnotator;
import edu.umn.biomedicus.token.StopwordAnnotator;
import edu.umn.biomedicus.token.TokenSplitter;
import edu.umn.biomedicus.token.WhitespaceTokenizer;
import edu.umn.biomedicus.util.XWriterFileNamer;
import edu.umn.biomedicus.window.CharWindowAnnotator;
import edu.umn.biomedicus.wsd.SenseRelateAE;

/**
 * Class to create all essential components.
 * 
 * @author Philip Ogren
 * @author Robert Bill
 */

public class ComponentFactory {

	protected static String biomedicusHome = null;

	public static void setBiomedicusHome(String home) {
		biomedicusHome = home;
	}

	public static String getBiomedicusHome() {
		// check if home directory is set yet.
		if (biomedicusHome == null) {
			biomedicusHome = System.getenv("BIOMEDICUS_HOME");
		}

		// stop everything if home directory is still missing
		if (biomedicusHome == null) {
			biomedicusHome = System.getProperty("user.dir");
			// System.err.println("BIOMEDICUS_HOME is not set. Unable to continue.");
			// System.err.println("Set the environment variable BIOMEDICUS_HOME to the path in which biomedicus is installed and then try again.");
			// throw new RuntimeException();
		}
		return biomedicusHome;
	}

	/**
	 * This methods returns the path to a resources located in the external
	 * resources folder. This folder should be located inside the Biomedicus
	 * root folder.
	 * 
	 * @param resourcePath
	 *            The path, as a String, to the resource within the resources
	 *            folder (without the $BIOMEDICUS_HOME/resources/ prefix).
	 * @return The fully qualified path to the resource in question so that it
	 *         can be passed to the analysis engine that requires it.
	 */
	public static String getResourcePath(String resourcePath) {
		// check if home directory is set yet.
		if (biomedicusHome == null) {
			biomedicusHome = System.getenv("BIOMEDICUS_HOME");
		}

		// stop everything if home directory is still missing
		if (biomedicusHome == null) {
			biomedicusHome = System.getProperty("user.dir");
			// System.err.println("BIOMEDICUS_HOME is not set. Unable to continue.");
			// System.err.println("Set the environment variable BIOMEDICUS_HOME to the path in which biomedicus is installed and then try again.");
			// throw new RuntimeException();
		}

		// build and return the resource path
		String resourceDir = new File(biomedicusHome, "resources").toString();
		File newPath = new File(resourceDir, resourcePath);
		return newPath.toString();
	}

	/**
	 * This methods returns the path to a jython file. All Jython files should
	 * be located in the resources/jython directory
	 * 
	 * @param JythonFile
	 *            The file name, or the path within the jython directory and
	 *            file name. For example, if the file "mytest.py" is located in
	 *            the BIOMEDICUS_HOME/resources/jython directory, then this
	 *            parameter is "mytest.py". If the file is in the path
	 *            BIOMEDICUS_HOME/resources/jython/myapp/mytest.py, then this
	 *            parameter is "myapp/mytest.py". within the resources folder
	 *            (without the $BIOMEDICUS_HOME/jython/ prefix).
	 * @return The fully qualified path to the jython file in question so that
	 *         it can be passed to the analysis engine that requires it.
	 */
	public static String getJythonFile(String fileName) {
		String bcusHome = ComponentFactory.getBiomedicusHome();
		String jythonDir = new File(biomedicusHome, "resources/jython").toString();
		File newPath = new File(jythonDir, fileName);
		return newPath.toString();
	}

	private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<TypeSystemDescription>();
	static {
		TypeSystemDescription tsd = TypeSystemDescriptionFactory
		                                .createTypeSystemDescription("edu.umn.biomedicus.type.TypeSystem");
		TYPE_SYSTEM_DESCRIPTION.set(tsd);
	}

	protected TypeSystemDescription typeSystemDescription;

	/**
	 * Gets the type system.
	 * 
	 * @return TypeSystemDescription
	 */
	public static TypeSystemDescription getTypeSystem() {
		return TYPE_SYSTEM_DESCRIPTION.get();
	}

	private static ThreadLocal<JCas> JCAS = new ThreadLocal<JCas>();

	/**
	 * Gets a new (empty) JCas instance initialized with the Biomedicus type
	 * system.
	 * 
	 * @return JCas
	 * @throws UIMAException
	 */
	public static JCas getJCas() throws UIMAException {
		JCas jCas = JCAS.get();
		if (jCas == null) {
			jCas = JCasFactory.createJCas(getTypeSystem());
			JCAS.set(jCas);
		}
		jCas.reset();
		return jCas;
	}

	/**
	 * Gets a JCas associated with a CAS instance.
	 * 
	 * @param CAS
	 * @return JCas
	 * @throws UIMAException
	 */
	public static JCas getJCas(CAS aCAS) throws UIMAException {
		JCas jCas = null;
		return aCAS.getJCas();
	}

	/**
	 * This method return an analysis engine that implements the Porter stemming
	 * algorithm and assigns the 'stemmed' version of the work to the Token's
	 * "stem" property.
	 * 
	 * @return
	 * @throws ResourceInitializationException
	 */
	public static AnalysisEngineDescription createStemmer() throws ResourceInitializationException {
		AnalysisEngineDescription stemmer = AnalysisEngineFactory.createPrimitiveDescription(StemAnnotator.class,
		                                ComponentFactory.getTypeSystem());
		return stemmer;
	}

	/**
	 * This method returns an analysis engine that loads a predefined stopword
	 * list from a file (one word per line) and adds a boolean value to the
	 * "isStopWord" property of the Token type.
	 * 
	 * @return
	 * @throws ResourceInitializationException
	 */
	public static AnalysisEngineDescription createStopwordAnnotator() throws ResourceInitializationException {
		// set default stopword list file
		return createStopwordAnnotator("PubMedStopwords.txt");
	}

	public static AnalysisEngineDescription createStopwordAnnotator(String fileName)
	                                throws ResourceInitializationException {
		AnalysisEngineDescription stopwordAnnotator = AnalysisEngineFactory.createPrimitiveDescription(
		                                StopwordAnnotator.class, ComponentFactory.getTypeSystem(),
		                                StopwordAnnotator.PARAM_STOPWORD_LIST, fileName);
		return stopwordAnnotator;
	}

	/**
	 * This method returns an analysis engine that copies the text from one view
	 * into another view. For example, if the jCas contains the view
	 * ORIGINAL_VIEW and you wish to copy the text in that view into the
	 * MODIFIED_VIEW, you would the analysis engine necessary to perform that by
	 * calling:
	 * 
	 * <pre>
	 * createViewTextCopierDescription(&quot;ORIGINAL_VIEW&quot;, &quot;MODIFIED_VIEW&quot;)
	 * </pre>
	 * 
	 * @param view1Name
	 *            The name of the source view that already contains the text
	 *            needing to be copied.
	 * @param view2Name
	 *            The name of the destination view that the text will be copied
	 *            into.
	 * @return An AnalysisEngineDescription that will be added to a pipeline or
	 *         builder such as:
	 * 
	 *         <pre>
	 * AnalysisEngineDescription copier = createViewTextCopierDescription(&quot;ORIGINAL&quot;, &quot;STEMMED&quot;);
	 * SimplePipeline.runPipeline(someCollectionReader, copier, someOtherAnalysisEngine);
	 * </pre>
	 * @throws ResourceInitializationException
	 */
	public static AnalysisEngineDescription createViewTextCopierDescription(String view1Name, String view2Name)
	                                throws ResourceInitializationException {
		return AnalysisEngineFactory.createPrimitiveDescription(ViewTextCopierAnnotator.class, getTypeSystem(),
		                                ViewTextCopierAnnotator.PARAM_SOURCE_VIEW_NAME, view1Name,
		                                ViewTextCopierAnnotator.PARAM_DESTINATION_VIEW_NAME, view2Name);

	}

	public static AnalysisEngineDescription createXWriter(String outputDirectoryName)
	                                throws ResourceInitializationException {
		AnalysisEngineDescription aed = AnalysisEngineFactory.createPrimitiveDescription(XWriter.class,
		                                getTypeSystem(), XWriter.PARAM_OUTPUT_DIRECTORY_NAME, outputDirectoryName,
		                                XWriter.PARAM_FILE_NAMER_CLASS_NAME, XWriterFileNamer.class.getName());
		return aed;
	}

	public static ExternalResourceDescription getBiomedicusDb() {
		// Create necessary DB resource
		String biomedicusHome = ComponentFactory.getBiomedicusHome();
		ExternalResourceDescription biomedicusDb = ExternalResourceFactory.createExternalResourceDescription(
		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
		                                biomedicusHome);
		return biomedicusDb;
	}

	public static AnalysisEngineDescription createConceptSenseAnnotator(String umls_user, String umls_password)
	                                throws ResourceInitializationException {
		ExternalResourceDescription biomedicusDb = getBiomedicusDb();
		AnalysisEngineDescription senses = AnalysisEngineFactory.createPrimitiveDescription(SenseRelateAE.class,
		                                getTypeSystem(), SenseRelateAE.BIOMEDICUS_DB, biomedicusDb);
		return senses;
	}

	public static AnalysisEngineDescription createAcronymAnnotator() throws ResourceInitializationException {
		AnalysisEngineDescription acronyms = AnalysisEngineFactory.createPrimitiveDescription(AcronymDetectorAE.class,
		                                getTypeSystem(), AcronymDetectorAE.PARAM_LEXICON_CONFIG_FILE,
		                                ComponentFactory.getResourcePath("config/lexAccess.properties"));

		//                                AcronymDetectorAE.PARAM_MODEL_FILE,
		//                                ComponentFactory.getResourcePath("models/acronyms/acronymMEModel.bin"));
		ExternalResourceDescription biomedicusDb = getBiomedicusDb();
		AnalysisEngineDescription acronymConcepts = AnalysisEngineFactory.createPrimitiveDescription(
		                                AcronymConceptMapper.class, getTypeSystem(),
		                                AcronymConceptMapper.BIOMEDICUS_DB, biomedicusDb);

		// Build it
		AggregateBuilder builder = new AggregateBuilder();
		// builder.add(symbols);
		// builder.add(tokens);
		builder.add(acronyms);
		builder.add(acronymConcepts);
		return builder.createAggregateDescription();
	}

	public static AnalysisEngineDescription createTokenAnnotator() throws ResourceInitializationException {
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(AnalysisEngineFactory.createPrimitiveDescription(BreakIteratorTokenizer.class, getTypeSystem()));
		builder.add(AnalysisEngineFactory.createPrimitiveDescription(TokenSplitter.class, getTypeSystem()));
		return builder.createAggregateDescription();
	}

	public static AnalysisEngineDescription createNewTokenAnnotator() throws ResourceInitializationException {
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(AnalysisEngineFactory.createPrimitiveDescription(CharWindowAnnotator.class, getTypeSystem()));
		builder.add(AnalysisEngineFactory.createPrimitiveDescription(SymbolSenseAnnotator.class, getTypeSystem()));
		builder.add(AnalysisEngineFactory.createPrimitiveDescription(WhitespaceTokenizer.class, getTypeSystem()));
		return builder.createAggregateDescription();
	}

	public static AnalysisEngineDescription createStanfordParser() throws ResourceInitializationException {
		String parserFileAndPath = getResourcePath("models/parse/englishPCFG.ser.gz");
		AnalysisEngineDescription stanfordParserDescription = AnalysisEngineFactory.createPrimitiveDescription(
		                                StanfordParserAE.class, ComponentFactory.getTypeSystem(),
		                                StanfordParserAE.PARAM_PARSER_FILE, parserFileAndPath);
		return stanfordParserDescription;
	}

	public static AnalysisEngineDescription createPosAnnotator() throws ResourceInitializationException {
		AnalysisEngineDescription aed = AnalysisEngineFactory.createPrimitiveDescription(TntPosAnnotator.class,
		                                getTypeSystem());
		// AnalysisEngineDescription aed =
		// AnalysisEngineFactory.createPrimitiveDescription(MEPOSAnnotator.class,
		// ComponentFactory.getTypeSystem());
		return aed;
	}

	public static AnalysisEngineDescription createPosAnnotator(String modelFileName)
	                                throws ResourceInitializationException {
		AnalysisEngineDescription aed = AnalysisEngineFactory.createPrimitiveDescription(TntPosAnnotator.class,
		                                getTypeSystem(), TntPosAnnotator.PARAM_MODEL_FILE, modelFileName);
		return aed;
	}

	public static AnalysisEngineDescription createSentenceAnnotator() throws ResourceInitializationException {
		// if no model given, return the default model trained on genia and the
		// clinical corpus
		return createSentenceAnnotator("sentence-me-model.bin");
	}

	public static AnalysisEngineDescription createSentenceAnnotator(String modelFileName)
	                                throws ResourceInitializationException {
		String modelFile = new File("/edu/umn/biomedicus/sentence", modelFileName).toString();
		return AnalysisEngineFactory.createPrimitiveDescription(MaxentSentenceBoundaryDetector.class, getTypeSystem(),
		                                MaxentSentenceBoundaryDetector.PARAM_MODEL_FILE, modelFile);
	}

	public static AnalysisEngineDescription createLineAnnotator() throws ResourceInitializationException {
		return AnalysisEngineFactory.createPrimitiveDescription(LineAnnotator.class, getTypeSystem());
	}

	// public static AnalysisEngineDescription createSentenceAnnotator(String
	// modelFileName)
	// throws ResourceInitializationException {
	// AggregateBuilder builder = new AggregateBuilder();
	//
	// AnalysisEngineDescription patternSentenceBoundaryDetector =
	// AnalysisEngineFactory.createPrimitiveDescription(
	// PatternSentenceBoundaryDetector.class, ComponentFactory.getTypeSystem());
	// builder.add(patternSentenceBoundaryDetector);
	//
	// AnalysisEngineDescription probabilisticSentenceBoundaryDetector =
	// CleartkAnnotatorDescriptionFactory
	// .createCleartkAnnotator(ProbabalisticSentenceBoundaryDetector.class,
	// getTypeSystem(), modelFileName);
	// builder.add(probabilisticSentenceBoundaryDetector);
	//
	// AnalysisEngineDescription sentenceAnnotator =
	// AnalysisEngineFactory.createPrimitiveDescription(
	// SentenceAnnotator.class, getTypeSystem());
	// builder.add(sentenceAnnotator);
	//
	// return builder.createAggregateDescription();
	// }

	public static CorpusFactory createCorpusFactory(String className) throws Exception {
		Class<?> cls = Class.forName(className);
		Class<? extends CorpusFactory> corpusFactoryCls = cls.asSubclass(CorpusFactory.class);
		CorpusFactory corpusFactory = corpusFactoryCls.newInstance();
		return corpusFactory;
	}

	public static AnalysisEngineDescription createUmlsConceptAnnotator(boolean filter)
	                                throws ResourceInitializationException {
		if (filter) {
			return AnalysisEngineFactory.createPrimitiveDescription(UmlsConceptAnnotator.class, getTypeSystem(),
			                                UmlsConceptAnnotator.PARAM_FILTER,
			                                UmlsConceptAnnotator.Filter.ALL_TERMS_COEXIST_IN_UMLS);
		}
		return AnalysisEngineFactory.createPrimitiveDescription(UmlsConceptAnnotator.class, getTypeSystem());
	}

	public static AnalysisEngineDescription createMetaMapAnnotator() throws ResourceInitializationException {
		return createMetaMapAnnotator("localhost", 8066);
	}

	public static AnalysisEngineDescription createMetaMapAnnotator(String server, int port)
	                                throws ResourceInitializationException {
		return AnalysisEngineFactory.createPrimitiveDescription(MetaMapAE.class, getTypeSystem(),
		                                MetaMapAE.PARAM_METAMAP_SERVER, server, MetaMapAE.PARAM_METAMAP_PORT,
		                                new Integer(port).toString());
	}

	public static CollectionReaderDescription createJdbcReaderDescription(String hostname, String dbname,
	                                String username, String password, String sqlQuery)
	                                throws ResourceInitializationException {
		CollectionReaderDescription crd = CollectionReaderFactory.createDescription(JdbcReader.class, getTypeSystem(),
		                                JdbcReader.PARAM_DBADDRESS, hostname, JdbcReader.PARAM_DBNAME, dbname,
		                                JdbcReader.PARAM_DBUSER, username, JdbcReader.PARAM_DBPWD, password,
		                                JdbcReader.PARAM_DBQUERY, sqlQuery);
		return crd;
	}
}
