/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class ViewNames {

	/**
	 * The gold view is intended for gold-standard labeled data. Annotations in
	 * this view are generally populated from some labeled corpus
	 */
	public static final String GOLD_VIEW = "GOLD_VIEW";
	/**
	 * The system view is intended for system generated labeled data.
	 * Annotations in this view are generally generated from a pipeline of
	 * analysis engines. The intention is that the contents of the system view
	 * will later be compared against the contents of the gold view.
	 */
	public static final String SYSTEM_VIEW = "SYSTEM_VIEW";

	/**
	 * the native view is intended for the original contents of some file that
	 * was loaded in. For example, if the original data was an xml file which
	 * gets parsed to produce plain text along with some annotations, this view
	 * can store the original xml formatted data.
	 */
	public static final String NATIVE_VIEW = "NATIVE_VIEW";

	/**
	 * The meta view is intended for metadata such as the file location, file
	 * name, possibly processing notes, etc.
	 */

	public static final String META_VIEW = "META_VIEW";
}
