package edu.umn.biomedicus.script.tutorial;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;

import org.apache.uima.UimaContext;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Progress;
import org.apache.uima.util.ProgressImpl;
import org.uimafit.component.CasCollectionReader_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;

import edu.umn.biomedicus.type.MetaData;

public class SimpleCollectionReader extends CasCollectionReader_ImplBase {

	public static final String PARAM_INPUT_DIRECTORY = ConfigurationParameterFactory.createConfigurationParameterName(
	                                SimpleCollectionReader.class, "inputDirectory");
	@ConfigurationParameter
	private File inputDirectory;

	int totalFiles = 0;
	int currentFile = -1;
	File[] fileList;

	FileFilter fileFilter = new FileFilter() {
		public boolean accept(File file) {
			if (file.isFile()) {
				return true;
			}
			return false;
		}
	};

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		fileList = inputDirectory.listFiles(fileFilter);
		totalFiles = fileList.length;
	}

	@Override
	public void getNext(CAS cas) throws IOException, CollectionException {
		// increment current file record
		currentFile++;
		if (fileList.length < currentFile + 1) {
			throw new RuntimeException("Called 'getNext()' after 'hasNext()' returned false.");
		}
		System.out.println("Reading " + fileList[currentFile]);
		JCas jcas;
		try {
			jcas = cas.getJCas();
		} catch (CASException e) {
			throw new RuntimeException("Unable to get the jcas" + e);
		}

		// read file
		BufferedReader in = new BufferedReader(new FileReader(fileList[currentFile]));
		String line = new String("");
		String text = new String("");

		line = in.readLine();
		while (line != null) {
			text += line;
			line = in.readLine();
		}

		// put text into the CAS
		jcas.setDocumentText(text);
		MetaData doc = new MetaData(jcas, 0, text.length());
		doc.setDocumentId(fileList[currentFile].getName());
		doc.addToIndexes();
	}

	@Override
	public Progress[] getProgress() {
		return new Progress[] { new ProgressImpl(currentFile, fileList.length, Progress.ENTITIES) };
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return (currentFile + 1 < fileList.length);
	}
}
