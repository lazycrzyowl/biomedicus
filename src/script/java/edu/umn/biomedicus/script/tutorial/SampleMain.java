package edu.umn.biomedicus.script.tutorial;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.kohsuke.args4j.Option;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.util.Options_ImplBase;

public class SampleMain {

	public static class Options extends Options_ImplBase {
		@Option(name = "-o", aliases = "--someOption", usage = "an option", required = false)
		public String someOption;
	}

	public static void main(String[] args) throws UIMAException, IOException {
		Options options = new Options();
		options.parseArgument(args);

		// get type system
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

		// get reader
		CollectionReaderDescription dsc = CollectionReaderFactory.createDescription(SimpleCollectionReader.class,
		                                typeSystem, SimpleCollectionReader.PARAM_INPUT_DIRECTORY,
		                                "/home/bill0154/experiments/tutorial/raw/");
		CollectionReader reader = CollectionReaderFactory.createCollectionReader(dsc);

		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription sentences = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription pos = ComponentFactory.createPosAnnotator();
		AnalysisEngineDescription symbol = AnalysisEngineFactory.createPrimitiveDescription(SampleAnnotator.class,
		                                typeSystem);

		// The xWriter writes out the contents of each CAS (one per sentence) to
		// an XMI file. It is instructive to open one of these
		// xmi files in the CAS Visual Debugger and look at the contents of each
		// view.
		AnalysisEngineDescription xWriter = AnalysisEngineFactory.createPrimitiveDescription(XWriter.class, typeSystem,
		                                XWriter.PARAM_OUTPUT_DIRECTORY_NAME, "/home/bill0154/experiments/tutorial");

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(tokens);
		builder.add(sentences);
		builder.add(pos);
		builder.add(symbol);
		builder.add(xWriter);

		// runs the collection reader and the aggregate AE.
		SimplePipeline.runPipeline(reader, builder.createAggregate());
	}
}
