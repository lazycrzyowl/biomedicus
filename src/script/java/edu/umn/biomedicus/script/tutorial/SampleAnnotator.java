/**
 * 
 */
package edu.umn.biomedicus.script.tutorial;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Symbol;

public class SampleAnnotator extends JCasAnnotator_ImplBase {

	/*
	 * public static final String PARAM_ONE =
	 * ConfigurationParameterFactory.createConfigurationParameterName(
	 * SampleAnnotator.class, "parameterOne");
	 * 
	 * @ConfigurationParameter private String parameterOne;
	 */

	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
	}

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		Pattern plusPattern = Pattern.compile("\\+");
		Pattern minusPattern = Pattern.compile("\\-");

		for (Sentence sentence : JCasUtil.iterate(jcas, Sentence.class)) {
			String sentenceText = sentence.getCoveredText();

			Matcher plusMatcher = plusPattern.matcher(sentenceText);
			Matcher minusMatcher = minusPattern.matcher(sentenceText);

			int start = 0;
			while (plusMatcher.find(start)) {
				int begin = sentence.getBegin() + plusMatcher.start();
				int end = sentence.getBegin() + plusMatcher.end();
				Symbol plus = new Symbol(jcas, begin, end);
				plus.addToIndexes(jcas);
				start = plusMatcher.end();
			}

			start = 0;
			while (minusMatcher.find(start)) {
				int begin = sentence.getBegin() + minusMatcher.start();
				int end = sentence.getBegin() + minusMatcher.end();
				Symbol minus = new Symbol(jcas, begin, end);
				minus.addToIndexes(jcas);
				start = minusMatcher.end();
			}
		}
	}
}
