package edu.umn.biomedicus.script.acronym;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.cleartk.util.cr.FilesCollectionReader;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.factory.JCasFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;

public class AcronymTest {

	/**
	 * @param args
	 * @throws UIMAException
	 * @throws IOException
	 */
	public static void main(String[] args) throws UIMAException, IOException {
		// String testStr = "Status post TIPS procedure on 5/14/04. "
		// + "The patient when admitted was found to be in A fib with RVR.";

		TypeSystemDescription typeSystemDescription = ComponentFactory.getTypeSystem();
		JCas jCas = JCasFactory.createJCas(typeSystemDescription);

		// CorpusFactory corpusFactory = new AcronymsFactory();
		// CollectionReader reader = corpusFactory.createTestReader();

		CollectionReader reader = CollectionReaderFactory.createCollectionReader(FilesCollectionReader.class,
		                                ComponentFactory.getTypeSystem(), FilesCollectionReader.PARAM_ROOT_FILE,
		                                "/workspace/Biomedicus/resources/corpora/edu.umn.biomedicus.acronym/xmi");

		AnalysisEngineDescription acronymDesc = ComponentFactory.createAcronymAnnotator();
		AnalysisEngine acronymDetector = AnalysisEngineFactory.createAnalysisEngine(acronymDesc, "_InitialView");
		AnalysisEngineDescription tokenizerDescription = ComponentFactory.createTokenAnnotator();
		AnalysisEngine tokenizer = AnalysisEngineFactory.createAnalysisEngine(tokenizerDescription, "_InitialView");
		SimplePipeline.runPipeline(reader, tokenizer, acronymDetector);
	}
}
