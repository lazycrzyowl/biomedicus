package edu.umn.biomedicus.script.pos;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.corpora.clinical.ClinicalFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;
import edu.umn.biomedicus.evaluation.copy.SentenceAndTokenCopier;
import edu.umn.biomedicus.pos.SpecialistLexiconAnnotator;
import edu.umn.biomedicus.script.example.ViewNames;

public class LexiconExample {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws UIMAException, RuntimeException, IOException {
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

		// Get corpus reader
		CorpusFactory corpusFactory = new ClinicalFactory();
		CollectionReader reader = corpusFactory.createTestReader();

		AnalysisEngineDescription textCopier = ComponentFactory.createViewTextCopierDescription(ViewNames.GOLD_VIEW,
		                                ViewNames.SYSTEM_VIEW);
		AnalysisEngineDescription sentenceAndTokenCopier = AnalysisEngineFactory.createPrimitiveDescription(
		                                SentenceAndTokenCopier.class, typeSystem);
		AnalysisEngineDescription lexicon = AnalysisEngineFactory.createPrimitiveDescription(
		                                SpecialistLexiconAnnotator.class, typeSystem);

		// Use a builder to assemble the components
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(textCopier);
		builder.add(sentenceAndTokenCopier, ViewNames.VIEW1, ViewNames.GOLD_VIEW, ViewNames.VIEW2,
		                                ViewNames.SYSTEM_VIEW);
		builder.add(lexicon, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW);

		// runs the collection reader and the aggregate AE.
		SimplePipeline.runPipeline(reader, builder.createAggregate());
	}
}
