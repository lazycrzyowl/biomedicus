/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.pos;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.kohsuke.args4j.Option;
import org.uimafit.component.ViewTextCopierAnnotator;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.corpora.genia.GeniaFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;
import edu.umn.biomedicus.evaluation.copy.SentenceAndTokenCopier;
import edu.umn.biomedicus.script.example.POSEvaluator;
import edu.umn.biomedicus.script.example.ViewNames;
import edu.umn.biomedicus.util.Options_ImplBase;

public class RunTntTaggerExperiment {

	public static class Options extends Options_ImplBase {
		// An optional command-line argument allows users to pass in the name of
		// a model file. If no command-line argument is given, the the POS
		// annotator uses the data trained on the entire Genia corpus and 60% of
		// the clinical corpus.
		@Option(name = "-m", aliases = "--modelfile", usage = "Specify a model file", required = false)
		public String modelFile;
	}

	public static void main(String[] args) throws UIMAException, IOException {
		Options options = new Options();
		options.parseArgument(args);
		String modelFileName = options.modelFile;
		if (modelFileName == null) {
			modelFileName = "genia-tnt-model.ser";
		}

		// Get corpus reader
		// CorpusFactory corpusFactory = new ClinicalFactory();
		// CollectionReader reader = corpusFactory.createTestReader();
		CorpusFactory corpusFactory = new GeniaFactory();
		CollectionReader reader = corpusFactory.createTestReader();

		// get type system
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

		// Note on views:
		// GOLD_VIEW = a human annotated version or some other gold standard.
		// SYSTEM_VIEW = the view BiomedICUS works on and adds annotations to.
		// The textCopier creates the SYSTEM_VIEW and set the text of this view
		// to that of the text found in GOLD_VIEW. Only use textCopier on a
		// corpus that has a GOLD_VIEW (hint, the Fairview notes don't have a
		// GOLD_VIEW
		AnalysisEngineDescription textCopier = AnalysisEngineFactory.createPrimitiveDescription(
		                                ViewTextCopierAnnotator.class, typeSystem,
		                                ViewTextCopierAnnotator.PARAM_SOURCE_VIEW_NAME, ViewNames.GOLD_VIEW,
		                                ViewTextCopierAnnotator.PARAM_DESTINATION_VIEW_NAME, ViewNames.SYSTEM_VIEW);

		// After text has been copied to the SYSTEM_VIEW, it is necessary to
		// copy the annotations of that text into the SYSTEM_VIEW:
		// The sentenceAndTokenCopier copies Token and Sentence annotations from
		// the GOLD_VIEW into the SYSTEM_VIEW
		AnalysisEngineDescription sentenceAndTokenCopier = AnalysisEngineFactory.createPrimitiveDescription(
		                                SentenceAndTokenCopier.class, typeSystem);

		// Create other standard components: the opennlp sentence boundary
		// detector and tnt pos tagger
		AnalysisEngineDescription sentenceBoundaryDector = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription tntTagger = ComponentFactory.createPosAnnotator();// modelFileName);

		// The evaluator will compare the part-of-speech tags in the SYSTEM_VIEW
		// with those in the GOLD_VIEW
		AnalysisEngineDescription evaluator = AnalysisEngineFactory.createPrimitiveDescription(POSEvaluator.class,
		                                typeSystem);

		// The xWriter writes out the contents of each CAS (one per sentence) to
		// an XMI file. It is instructive to open one of these
		// xmi files in the CAS Visual Debugger and look at the contents of each
		// view.
		AnalysisEngineDescription xWriter = AnalysisEngineFactory.createPrimitiveDescription(XWriter.class, typeSystem,
		                                XWriter.PARAM_OUTPUT_DIRECTORY_NAME,
		                                "resources/edu/umn/biomedicus/examples/pos/xmi");

		// Use a builder to assemble the components
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(textCopier);
		builder.add(sentenceAndTokenCopier, ViewNames.VIEW1, ViewNames.GOLD_VIEW, ViewNames.VIEW2,
		                                ViewNames.SYSTEM_VIEW);
		builder.add(sentenceBoundaryDector, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW);
		builder.add(tntTagger, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW);
		builder.add(evaluator);
		builder.add(xWriter);

		// runs the collection reader and the aggregate AE.
		SimplePipeline.runPipeline(reader, builder.createAggregate());
	}
}
