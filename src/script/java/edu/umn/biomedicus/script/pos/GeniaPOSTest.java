package edu.umn.biomedicus.script.pos;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.corpora.genia.GeniaFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;
import edu.umn.biomedicus.type.Token;

public class GeniaPOSTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws ResourceInitializationException, UIMAException, IOException {
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();
		CorpusFactory corpusFactory = new GeniaFactory();
		CollectionReader reader = corpusFactory.createTrainReader();

		while (reader.hasNext()) {
			CAS newcas = ComponentFactory.getJCas().getCas();
			reader.getNext(newcas);
			CAS cas = newcas.getView(ViewNames.GOLD_VIEW);
			JCas jcas = cas.getJCas();

			for (Token token : JCasUtil.iterate(jcas, Token.class)) {
				if (token.getCoveredText().equals("in")) {
					System.out.println(token.getCoveredText() + " " + token.getPos());
				}
			}

		}
	}
}