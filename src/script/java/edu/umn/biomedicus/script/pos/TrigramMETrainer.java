package edu.umn.biomedicus.script.pos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;

import opennlp.maxent.BasicEventStream;
import opennlp.maxent.GIS;
import opennlp.maxent.PlainTextByLineDataStream;
import opennlp.maxent.io.GISModelWriter;
import opennlp.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.model.AbstractModel;
import opennlp.model.EventStream;
import opennlp.model.OnePassRealValueDataIndexer;

import org.apache.uima.UIMAException;

import edu.umn.biomedicus.acronym.AcronymFeatureExtractor;
import edu.umn.biomedicus.acronym.AcronymManifest;
import gov.nih.nlm.nls.lexAccess.Api.LexAccessApi;

public class TrigramMETrainer {
	private static RandomAccessFile eventFile = null;

	static AcronymFeatureExtractor afe;
	static LexAccessApi lex;

	/**
	 * @param args
	 * @throws UIMAException
	 * @throws IOException
	 */
	public static void main(String[] args) throws Exception {

		// set properties explicity for now. TODO: create properties file
		System.setProperty("biomedicus.project", "/workspace/Biomedicus");
		System.setProperty("biomedicus.home", "workspace/Biomedicus");

		// get data path. The "lexAccess.properties" placed in the Biomedicus
		// external resources folder
		String dataPath = System.getProperty("biomedicus.home");
		String lexConfigFile = new File(dataPath, "/resources/config/lexAccess.properties").toString();

		// create the lexicon and featureExtractor instances
		lex = new LexAccessApi(lexConfigFile);
		String acronymCuiManifestUri = "/edu/umn/biomedicus/acronym/AcronymManifest.csv";
		AcronymManifest manifest = new AcronymManifest(new FileInputStream(acronymCuiManifestUri));
		afe = new AcronymFeatureExtractor(manifest, lex);

		// define the event file and the model file
		String resourcePath = "/home/bill0154/workspace/Biomedicus/resources/models/part-of-speech/";
		String eventFileName = "pos.dat";
		String modelFileName = "posMEModel.bin";

		FileReader eventFileReader = null;
		try {
			eventFileReader = new FileReader(new File(resourcePath, eventFileName));
			EventStream es = new BasicEventStream(new PlainTextByLineDataStream(eventFileReader));
			GIS.SMOOTHING_OBSERVATION = 0.1;
			AbstractModel model = GIS.trainModel(100, new OnePassRealValueDataIndexer(es, 2), true);
			File modelFile = new File(resourcePath + modelFileName);
			GISModelWriter modelWriter = new SuffixSensitiveGISModelWriter(model, modelFile);
			modelWriter.persist();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
