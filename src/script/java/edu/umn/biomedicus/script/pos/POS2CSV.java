/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.pos;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.component.ViewTextCopierAnnotator;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.TypeSystemDescriptionFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.corpora.genia.GeniaFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;
import edu.umn.biomedicus.pos.tnt.POS2WekaConsumer;
import edu.umn.biomedicus.script.example.ViewNames;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

public class POS2CSV {

	public static void main(String[] args) throws UIMAException, IOException {
		// Get type system
		TypeSystemDescription typeSystem = TypeSystemDescriptionFactory.createTypeSystemDescription(Sentence.class,
		                                Token.class);

		// Create instance of the TNT trainer
		TrainTntTagger trainer = new TrainTntTagger();

		// Get training data. It is in the Genia corpus and clinical corpus
		CorpusFactory geniaCorpusFactory = new GeniaFactory();
		// CorpusFactory clinicalCorpusFactory = new ClinicalFactory();
		CollectionReader geniaReader = geniaCorpusFactory.createTrainReader();

		// Create analysis engines
		AnalysisEngineDescription weka = AnalysisEngineFactory.createPrimitiveDescription(POS2WekaConsumer.class,
		                                POS2WekaConsumer.PARAM_PLACEHOLDER, "REPLACE_DATA");

		// Build aggregate
		AggregateBuilder builder = new AggregateBuilder();
		// builder.add(trainer.getTextCopier(typeSystem));
		// builder.add(tokens, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW);
		// builder.add(sentences, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW);
		builder.add(weka, CAS.NAME_DEFAULT_SOFA, ViewNames.GOLD_VIEW);

		// runs the collection reader and the aggregate AE.
		SimplePipeline.runPipeline(geniaReader, builder.createAggregate());

		// multiple the clinical corpus scores

		// CollectionReader clinicalReader =
		// clinicalCorpusFactory.createTrainReader();
		// Create analysis engines
		// tntTagger =
		// AnalysisEngineFactory.createPrimitiveDescription(TntPosTrainer.class,
		// TntPosTrainer.PARAM_ACCUMULATION_MODE, TntPosTrainer.ACCRETE_DATA);
		//
		// builder = new AggregateBuilder();
		// builder.add(tntTagger, CAS.NAME_DEFAULT_SOFA, ViewNames.GOLD_VIEW);
		// SimplePipeline.runPipeline(clinicalReader,
		// builder.createAggregate());
	}

	public AnalysisEngineDescription getTextCopier(TypeSystemDescription typeSystem) {
		AnalysisEngineDescription textCopier = null;
		try {
			textCopier = AnalysisEngineFactory.createPrimitiveDescription(ViewTextCopierAnnotator.class, typeSystem,
			                                ViewTextCopierAnnotator.PARAM_SOURCE_VIEW_NAME, ViewNames.GOLD_VIEW,
			                                ViewTextCopierAnnotator.PARAM_DESTINATION_VIEW_NAME, ViewNames.SYSTEM_VIEW);
		} catch (ResourceInitializationException e) {
			System.out.println("An error occurred while creating the text copier analysis engine description: "
			                                + e.toString());
		}
		return textCopier;
	}
}
