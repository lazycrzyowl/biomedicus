/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.script.tiny;

/*
 * import edu.umn.biomedicus.BiomedicusTestBase;
 * 
 * /** Text for this "tiny" corpus is from Mark Twain's Roughing It which is in
 * the public domain and freely available from:
 * 
 * http://www.gutenberg.org/ebooks/3177
 * http://www.gutenberg.org/files/3177/3177-h/p3.htm#ch28
 * 
 * @author Philip Ogren
 * 
 * 
 * public class PrepareTinyCorpus extends BiomedicusTestBase {
 * 
 * 
 * 
 * 
 * AnalysisEngine xWriter;
 * 
 * public PrepareTinyCorpus() throws Exception { super.setUp(); xWriter =
 * AnalysisEngineFactory.createPrimitive(XWriter.class, typeSystemDescription,
 * XWriter.PARAM_OUTPUT_DIRECTORY_NAME, "src/test/resources/tinycorpus/xmi"); }
 * 
 * public void prepareTinyCorpus() throws Exception {
 * 
 * createXmiFile(
 * "The first opportunity that offered, I sauntered carelessly away from the cabin, keeping an eye on the other boys, and stopping and contemplating the sky when they seemed to be observing me;"
 * );createXmiFile(
 * "but as soon as the coast was manifestly clear, I fled away as guiltily as a thief might have done and never halted till I was far beyond sight and call. "
 * );createXmiFile(
 * "Then I began my search with a feverish excitement that was brimful of expectation almost of certainty. "
 * );createXmiFile(
 * "I crawled about the ground, seizing and examining bits of stone, blowing the dust from them or rubbing them on my clothes, and then peering at them with anxious hope. "
 * ); createXmiFile("Presently I found a bright fragment and my heart bounded!"
 * );createXmiFile(
 * "I hid behind a boulder and polished it and scrutinized it with a nervous eagerness and a delight that was more pronounced than absolute certainty itself could have afforded."
 * );createXmiFile(
 * "The more I examined the fragment the more I was convinced that I had found the door to fortune."
 * ); createXmiFile("I marked the spot and carried away my specimen. ");
 * createXmiFile(
 * "Up and down the rugged mountain side I searched, with always increasing interest and always augmenting gratitude that I had come to Humboldt and come in time."
 * );createXmiFile(
 * "Of all the experiences of my life, this secret search among the hidden treasures of silver-land was the nearest to unmarred ecstasy."
 * ); createXmiFile("It was a delirious revel.");
 * 
 * }
 * 
 * private void createXmiFile(String text) throws Exception { int numberOfTags =
 * text.split(" ").length; StringBuilder posTags = new StringBuilder(); for (int
 * i = 0; i < numberOfTags; i++) { if (i % 2 == 0) { posTags.append("A "); }
 * else { posTags.append("B "); } }
 * 
 * jCas.reset(); JCas goldView = jCas.createView(ViewNames.GOLD_VIEW);
 * tokenBuilder.buildTokens(goldView, text, text, posTags.toString());
 * SimplePipeline.runPipeline(jCas, xWriter);
 * 
 * }
 * 
 * public static void main(String[] args) throws Exception { PrepareTinyCorpus
 * ptc = new PrepareTinyCorpus(); ptc.prepareTinyCorpus(); } }
 * 
 * }
 */