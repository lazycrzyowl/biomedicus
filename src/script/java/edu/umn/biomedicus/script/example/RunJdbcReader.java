/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.script.example;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.uimafit.component.xwriter.CASDumpWriter;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * 
 * @author Yan Wang
 * 
 */
public class RunJdbcReader {

	public static class Options extends Options_ImplBase {

	}

	public static void main(String[] args) throws Exception {
		Options options = new Options();
		options.parseArgument(args);

		// get a connection to the fairview notes in the mysql db
		String server = "127.0.0.1";
		String dbname = "Fairview";
		String username = "test";
		String password = "test";
		String sqlQuery = "SELECT mrn, visit_guid, provider_guid, document_type_id, document_id,text FROM notes LIMIT 10";

		CollectionReaderDescription rddsc = ComponentFactory.createJdbcReaderDescription(server, dbname, username,
		                                password, sqlQuery);
		CollectionReader reader = CollectionReaderFactory.createCollectionReader(rddsc);

		// get required analysis engines from ComponentFactory
		AnalysisEngineDescription tokenizer = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription sentences = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription pos = ComponentFactory.createPosAnnotator();
		AnalysisEngineDescription casWriter = AnalysisEngineFactory.createPrimitiveDescription(CASDumpWriter.class,
		                                CASDumpWriter.PARAM_OUTPUT_FILE, "/home/bill0154/experiments/cas");

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(tokenizer);
		builder.add(sentences);
		builder.add(pos);
		builder.add(casWriter);

		long start = System.nanoTime();
		SimplePipeline.runPipeline(reader, builder.createAggregate());

		long stop = System.nanoTime();

		double time = (stop - start) / (double) 1000000000;
		System.out.println("time: " + time);

	}
}
