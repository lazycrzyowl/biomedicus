/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.example;

import java.text.NumberFormat;
import java.util.Collection;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;

/**
 * This AE evaluates the system sentence annotations agains the gold
 * annotations.
 */
@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class SentenceEvaluator extends JCasAnnotator_ImplBase {

	private int totalCorrect = 0;
	private int totalWrong = 0;
	private int beginCorrect = 0;
	private int endCorrect = 0;
	private int beginWrong = 0;
	private int endWrong = 0;

	// private Hashtable<String, String> errors;

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas goldView = jCas.getView(ViewNames.GOLD_VIEW);
			JCas systemView = jCas.getView(ViewNames.SYSTEM_VIEW);

			Collection<Sentence> goldSentences = JCasUtil.select(goldView, Sentence.class);
			Collection<Sentence> systemSentences = JCasUtil.select(systemView, Sentence.class);

			// Both the begin and end must be correct before adding one to
			// correct detections.
			for (Sentence goldSentence : goldSentences) {
				for (Sentence systemSentence : systemSentences) {
					if (goldSentence.getBegin() == systemSentence.getBegin()) {
						beginCorrect++;
						if (goldSentence.getEnd() == systemSentence.getEnd()) {
							totalCorrect++;
						} else {
							totalWrong++;
							endWrong++;
						}
					}
				}
			}
		} catch (CASException ce) {
			throw new AnalysisEngineProcessException(ce);
		}

	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		int total = totalCorrect + totalWrong;
		System.out.println("total sentences: " + total);
		System.out.println("correct: " + totalCorrect);
		System.out.println("wrong: " + totalWrong);
		float accuracy = (float) totalCorrect / total;
		System.out.println("accuracy: " + NumberFormat.getPercentInstance().format(accuracy));
	}
}
