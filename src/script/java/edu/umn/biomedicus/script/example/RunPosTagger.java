/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.example;

import java.io.File;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.apache.uima.pear.util.FileUtil;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class RunPosTagger {

	public static void main(String[] args) throws Exception {
		String modelFileName = args[0]; // the file name of the pos tagger model
		String inputFileName = args[1]; // the text file name you want to
		// process

		// instantiate your components
		AnalysisEngineDescription tokenizer = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription sentenceBoundaryDetector = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription posTagger = ComponentFactory.createPosAnnotator(modelFileName);

		// load the text into a CAS
		String text = FileUtil.loadTextFile(new File(inputFileName));
		JCas jCas = ComponentFactory.getJCas();
		jCas.setDocumentText(text);

		// run the pipeline over the CAS
		SimplePipeline.runPipeline(jCas, tokenizer, sentenceBoundaryDetector, posTagger);

		// now inspect the results of the CAS by looking at the tokens,
		// sentences, and tags
		for (Sentence sentence : JCasUtil.select(jCas, Sentence.class)) {
			StringBuilder sb = new StringBuilder();
			for (Token token : JCasUtil.selectCovered(Token.class, sentence)) {
				sb.append(token.getCoveredText() + "/" + token.getPos() + " ");
			}
			System.out.println(sb);
		}
	}
}
