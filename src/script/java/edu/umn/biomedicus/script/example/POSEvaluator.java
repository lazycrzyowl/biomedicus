/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.example;

import java.text.NumberFormat;
import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

/**
 * This AE evaluates the system part-of-speech tags against the gold
 * part-of-speech tags. This is a very simple approach to evaluation of
 * part-of-speech tags that will not likely suffice in real-world scenarios for
 * a number of reasons (e.g. no confusion matrix, assumes gold-standard tokens
 * and sentences in the system view, etc.)
 * 
 * @author Philip Ogren
 * 
 */
@SofaCapability(inputSofas = { ViewNames.GOLD_VIEW, ViewNames.SYSTEM_VIEW })
public class POSEvaluator extends JCasAnnotator_ImplBase {

	private int totalCorrect = 0;
	private int totalWrong = 0;
	private int knownCorrect = 0;
	private int unknownCorrect = 0;
	private int knownWrong = 0;
	private int unknownWrong = 0;

	// private Hashtable<String, String> errors;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		// errors = new Hashtable<String, String>();
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas goldView = jCas.getView(ViewNames.GOLD_VIEW);
			JCas systemView = jCas.getView(ViewNames.SYSTEM_VIEW);

			for (Sentence goldSentence : JCasUtil.iterate(goldView, Sentence.class)) {
				List<Token> goldTokens = JCasUtil.selectCovered(goldView, Token.class, goldSentence);
				List<Token> systemTokens = JCasUtil.selectCovered(systemView, Token.class, goldSentence);
				if (goldTokens.size() == systemTokens.size()) {
					for (int i = 0; i < goldTokens.size(); i++) {
						String text = goldTokens.get(i).getCoveredText();
						boolean isKnown = systemTokens.get(i).getIsKnown();
						String goldPos = goldTokens.get(i).getPos();
						String systemPos = systemTokens.get(i).getPos();
						if (goldPos.equals(systemPos)) {
							totalCorrect++;
							if (isKnown) {
								knownCorrect++;
							} else {
								unknownCorrect++;
							}
						} else {
							totalWrong++;
							if (isKnown) {
								knownWrong++;
							} else {
								unknownWrong++;
							}
							String blank = "                              ";
							System.out.print(new String("Token: " + text + blank).substring(0, 30));
							System.out.print(new String("Gold: " + goldPos + blank).substring(0, 13));
							System.out.print(new String("System: " + systemPos + blank).substring(0, 13));
							if (isKnown) {
								System.out.println("KNOWN");
							} else {
								System.out.println("UNKNOWN");
							}
						}
					}
				} else {
					throw new RuntimeException("number of tokens in gold view differs from "
					                                + "number of tokens in system view");
				}
			}
		} catch (CASException ce) {
			throw new AnalysisEngineProcessException(ce);
		}

	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		int total = totalCorrect + totalWrong;
		System.out.println("total tokens: " + total);
		System.out.println("correct: " + totalCorrect);
		System.out.println("wrong: " + totalWrong);
		float accuracy = (float) totalCorrect / total;
		float knownAccuracy = (float) knownCorrect / (knownCorrect + knownWrong);
		float unknownAccuracy = (float) unknownCorrect / (unknownCorrect + unknownWrong);
		System.out.println("accuracy: " + NumberFormat.getPercentInstance().format(accuracy));
		System.out.println("accuracy on known words = " + NumberFormat.getPercentInstance().format(knownAccuracy));
		System.out.println("accuracy on unknown words = " + NumberFormat.getPercentInstance().format(unknownAccuracy));
	}
}
