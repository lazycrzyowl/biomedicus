/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.example;

import static edu.umn.biomedicus.script.example.ViewNames.VIEW1;
import static edu.umn.biomedicus.script.example.ViewNames.VIEW2;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.SofaCapability;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Token;

/**
 * This simple AE copies tokens from from one view to another.
 */

@SofaCapability(inputSofas = { VIEW1, VIEW2 })
public class TokenCopier extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		try {
			JCas view1 = jCas.getView(VIEW1);
			JCas view2 = jCas.getView(VIEW2);

			for (Token token1 : JCasUtil.iterate(view1, Token.class)) {
				new Token(view2, token1.getBegin(), token1.getEnd()).addToIndexes();
			}

		} catch (CASException ce) {
			throw new AnalysisEngineProcessException(ce);
		}
	}
}
