package edu.umn.biomedicus.script.example;

import java.io.IOException;
import java.sql.SQLException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.JCasFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.wsd.SenseRelateAE;

public class UMLSExample {
	public static void main(String[] args) throws InstantiationException, IllegalAccessException,
	                                ClassNotFoundException, SQLException, UIMAException, IOException {

		TypeSystemDescription typeDesc = ComponentFactory.getTypeSystem();
		JCas jCas = JCasFactory.createJCas(typeDesc);
		AnalysisEngineDescription sents = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription mm = ComponentFactory.createMetaMapAnnotator();
		AnalysisEngineDescription senses = AnalysisEngineFactory.createPrimitiveDescription(SenseRelateAE.class,
		                                typeDesc, SenseRelateAE.BIOMEDICUS_DB, System.getenv("BIOMEDICUS_HOME"));

		jCas.setDocumentText("This is a test of senserelate in UIMA that uses medical (doctor) terms. "
		                                + "Patient denies CVD cardio vascular disease is present");

		SimplePipeline.runPipeline(jCas, sents, mm, senses);
	}
}
