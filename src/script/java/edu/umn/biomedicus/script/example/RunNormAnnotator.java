/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.example;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.cleartk.util.cr.FilesCollectionReader;
import org.kohsuke.args4j.Option;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.lvg.NormAnnotatorFactory;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class RunNormAnnotator {

	public static class Options extends Options_ImplBase {
		@Option(name = "-i", aliases = "--inputDirectory", usage = "specify an input directory", required = true)
		public String inputDirectoryName;
		@Option(name = "-o", aliases = "--outputDirectory", usage = "specify an output directory", required = true)
		public String outputDirectoryName;

	}

	public static void main(String[] args) throws Exception {
		Options options = new Options();
		options.parseArgument(args);

		String inputDirectoryName = options.inputDirectoryName;
		String outputDirectoryName = options.outputDirectoryName;

		CollectionReader reader = CollectionReaderFactory.createCollectionReader(FilesCollectionReader.class,
		                                ComponentFactory.getTypeSystem(), FilesCollectionReader.PARAM_ROOT_FILE,
		                                inputDirectoryName);
		// instantiate your components
		AnalysisEngineDescription tokenizer = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription sentenceBoundaryDetector = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription normAnnotator = NormAnnotatorFactory
		                                .createNormAnnotator();
		AnalysisEngineDescription writer = ComponentFactory.createXWriter(outputDirectoryName);

		long start = System.nanoTime();
		SimplePipeline.runPipeline(reader, sentenceBoundaryDetector, tokenizer, normAnnotator, writer);
		long stop = System.nanoTime();

		double time = (stop - start) / (double) 1000000000;
		System.out.println("time: " + time);
	}
}
