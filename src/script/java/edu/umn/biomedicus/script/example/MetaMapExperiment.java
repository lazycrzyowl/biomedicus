/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.example;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.TypeSystemDescriptionFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.metamap.MetaMapAE;
import edu.umn.biomedicus.sentence.MaxentSentenceBoundaryDetector;
import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Term;
import edu.umn.biomedicus.type.Token;
import edu.umn.biomedicus.type.UmlsConcept;

/**
 * This is a command-line interface to using the Metamap annotator. This class
 * assumes the metamap server (mmserver10) is running on the localhost on the
 * default port
 */
public class MetaMapExperiment {
	public static void main(String[] args) throws ResourceInitializationException, UIMAException, IOException {

		// get type system
		TypeSystemDescription typeSystem = TypeSystemDescriptionFactory.createTypeSystemDescription(Sentence.class,
		                                Token.class, UmlsConcept.class, Cui.class, Term.class);

		// create descriptions for all the components used
		AnalysisEngineDescription tokenizerDescription = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription sentenceBoundaryDector = AnalysisEngineFactory.createPrimitiveDescription(
		                                MaxentSentenceBoundaryDetector.class, typeSystem);
		AnalysisEngine tokenizer = AnalysisEngineFactory.createAnalysisEngine(tokenizerDescription,
		                                CAS.NAME_DEFAULT_SOFA);
		AnalysisEngineDescription metamap = AnalysisEngineFactory.createPrimitiveDescription(MetaMapAE.class,
		                                typeSystem, MetaMapAE.PARAM_METAMAP_SERVER, "localhost",
		                                MetaMapAE.PARAM_METAMAP_PORT, "8066");

		// Build it
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(sentenceBoundaryDector);
		// builder.add(tokenizerDescription);
		builder.add(metamap);

		// Get memory object (jCas) and input ready
		JCas jCas = ComponentFactory.getJCas();
		jCas.setDocumentText("Patient denies symptoms of congestive heart failure despite a low ejection fraction.");
		SimplePipeline.runPipeline(jCas, tokenizer, builder.createAggregate());

		// Pretty-print the results by concept
		for (UmlsConcept concept : JCasUtil.iterate(jCas, UmlsConcept.class)) {
			prettyPrint(concept);
		}
		System.out.println();
	}

	private static void prettyPrint(UmlsConcept concept) {
		System.out.println("UmlsConcept Information for text: " + concept.getCoveredText());

		// Print candidate CUIs
		System.out.print("    CUIs:    ");
		FSArray cuis = concept.getCuis();
		if (cuis == null)
			System.out.println("TROUBLE!!! CUIS IS NULL!!!!");
		for (int i = 0; i < cuis.size(); i++) {
			Cui myCui = (Cui) cuis.get(i);
			if (myCui != null)
				System.out.print(myCui.getCuiId() + " ");
		}
		System.out.print("\n    context: ");

		// Print context information such as negation
		StringArray context = concept.getContext();
		if (context != null) {
			for (int i = 0; i < concept.getContext().size(); i++) {
				System.out.print(concept.getContext(i) + " ");
			}
		}
		System.out.println();
	}
}
