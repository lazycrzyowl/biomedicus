/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.script.sentence;

import static edu.umn.biomedicus.ComponentFactory.getTypeSystem;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.cleartk.util.cr.FilesCollectionReader;
import org.kohsuke.args4j.Option;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * This simple script generates XMI CAS files that contain sentence annotations
 * using the OpenNLP sentence segmenter using the basic English model.
 * 
 * @author Philip Ogren
 * 
 */
public class PrepareCasFilesForSentenceAnnotation {

	public static class Options extends Options_ImplBase {
		@Option(name = "-i", aliases = "--inputDirectoryName", usage = "specify the input directory containing plain text files", required = true)
		public String inputDirectoryName;

		@Option(name = "-o", aliases = "--outputDirectory", usage = "specify the output directory to write XMI CAS files to", required = true)
		public String outputDirectoryName;
	}

	public static void main(String[] args) throws Exception {

		Options options = new Options();
		options.parseArgument(args);

		CollectionReader reader = CollectionReaderFactory.createCollectionReader(FilesCollectionReader.class,
		                                getTypeSystem(), FilesCollectionReader.PARAM_ROOT_FILE,
		                                options.inputDirectoryName);

		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription sentences = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription xwriter = ComponentFactory.createXWriter(options.outputDirectoryName);
		SimplePipeline.runPipeline(reader, tokens, sentences, xwriter);

	}
}
