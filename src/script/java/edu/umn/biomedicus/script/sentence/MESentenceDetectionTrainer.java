package edu.umn.biomedicus.script.sentence;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.sentdetect.SentenceSample;
import opennlp.tools.sentdetect.SentenceSampleStream;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.kohsuke.args4j.Option;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.corpora.clinical.ClinicalFactory;
import edu.umn.biomedicus.corpora.genia.GeniaFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;
import edu.umn.biomedicus.sentence.GoldSentenceWriter;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * This traines the OpenNLP MaxEnt sentence detection model on the genia and
 * clinical corpus
 * 
 * @author Robert Bill
 * 
 */
public class MESentenceDetectionTrainer {

	public static class Options extends Options_ImplBase {
		@Option(name = "-t", aliases = "--trainingFileName", usage = "specify a file path and name for a intermediary (temporary) training file", required = true)
		public String trainingFileName;

		@Option(name = "-m", aliases = "--modelFileName", usage = "specify an output file for the model", required = true)
		public String modelFileName;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws UIMAException, IOException {
		Options options = new Options();
		options.parseArgument(args);
		String trainingFileName = options.trainingFileName;
		String modelFileName = options.modelFileName;

		// Get training data. It is in the Genia corpus and clinical corpus
		CorpusFactory geniaCorpusFactory = new GeniaFactory();
		CorpusFactory clinicalCorpusFactory = new ClinicalFactory();
		CollectionReader geniaReader = geniaCorpusFactory.createTrainReader();
		CollectionReader clinicalReader = clinicalCorpusFactory.createTrainReader();

		// Create analysis engines
		AnalysisEngineDescription sentenceWriter = AnalysisEngineFactory.createPrimitiveDescription(
		                                GoldSentenceWriter.class, GoldSentenceWriter.PARAM_OUTPUT_FILE,
		                                trainingFileName);

		// Build aggregate
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(sentenceWriter, CAS.NAME_DEFAULT_SOFA, "GOLD_VIEW");

		// runs the collection reader and the aggregate AE.
		SimplePipeline.runPipeline(geniaReader, builder.createAggregate());
		SimplePipeline.runPipeline(clinicalReader, builder.createAggregate());

		// create the new model
		ObjectStream<String> lineStream = new PlainTextByLineStream(new FileInputStream(trainingFileName), "UTF-8");
		ObjectStream<SentenceSample> sampleStream = new SentenceSampleStream(lineStream);

		SentenceModel model = SentenceDetectorME.train("en", sampleStream, true, null, 5, 100);

		BufferedOutputStream modelOut = null;
		try {
			modelOut = new BufferedOutputStream(new FileOutputStream(modelFileName));
			model.serialize(modelOut);
		} finally {
			if (modelOut != null)
				modelOut.close();
		}
	}
}
