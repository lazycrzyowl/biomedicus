/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.script.symbolicvector;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.kohsuke.args4j.Option;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.corpora.clinical.FairviewReader;
import edu.umn.biomedicus.script.projects.CooccurrenceCasConsumer;
import edu.umn.biomedicus.script.projects.SentenceCollectorByCui;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * This class finds the definitions of each of the SMQ CUIs and then outputs a
 * symbolic row for each definition. A symbolic row is a space-separated list of
 * all the terms in the definition that are mapped by metamap.
 * 
 * @author Robert Bill
 * 
 */
public class UMLSDefinitionHarvester {

	public static class Options extends Options_ImplBase {
		@Option(name = "-f", aliases = "--inputFolder", usage = "specify an input folder", required = false)
		public String folder;

		@Option(name = "-o", aliases = "--outputFile", usage = "specify an output file", required = false)
		public String outputFile;
	}

	/**
	 * The main method requires the path and name of a file where output should
	 * be written. The assigned task is to list CUIs and concepts as one tuple
	 * per line under the heading of the documentID
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws ResourceInitializationException {
		Options options = new Options();
		options.parseArgument(args);
		String folder = options.folder;
		folder = "0";
		String outputFile = options.outputFile;
		CollectionReaderDescription crd = CollectionReaderFactory.createDescription(FairviewReader.class,
		                                ComponentFactory.getTypeSystem(), FairviewReader.PARAM_FOLDER, folder);
		CollectionReader reader = CollectionReaderFactory.createCollectionReader(crd);
		AnalysisEngineDescription cooccurrence = AnalysisEngineFactory.createPrimitiveDescription(
		                                SentenceCollectorByCui.class, ComponentFactory.getTypeSystem(),
		                                CooccurrenceCasConsumer.PARAM_OUTPUT_FILE, outputFile);

		// buid it
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(cooccurrence);
		try {
			SimplePipeline.runPipeline(reader, builder.createAggregate());
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("IOException. Unable to write output with XWriter.");
		} catch (UIMAException e) {
			e.printStackTrace();
			throw new RuntimeException("UIMAException. See stack trace.");
		}
	}
}
