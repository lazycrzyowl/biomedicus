/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.script.count;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.util.cr.FilesCollectionReader;
import org.kohsuke.args4j.Option;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Token;
import edu.umn.biomedicus.util.Options_ImplBase;

/**
 * 
 * @author Philip Ogren
 * 
 *         <p>
 *         This script takes an input directory (or file) and looks at all the
 *         files in it and counts up the words as determined by the tokenizer.
 *         The output file contains each word and its frequency in descending
 *         order of frequency. If you pass in a very large corpus to this
 *         script, then you may want to increase the maximum java heapsize.
 */
public class CountWords {

	public static class Options extends Options_ImplBase {
		@Option(name = "-i", aliases = "--inputDirectory", usage = "specify an input directory", required = true)
		public String inputDirectoryName;

		@Option(name = "-o", aliases = "--outputFileName", usage = "specify an output file", required = true)
		public String outputFileName;

	}

	public static void main(String[] args) throws Exception {
		Options options = new Options();
		options.parseArgument(args);

		String inputDirectoryName = options.inputDirectoryName;
		String outputFileName = options.outputFileName;

		CollectionReader reader = CollectionReaderFactory.createCollectionReader(FilesCollectionReader.class,
		                                ComponentFactory.getTypeSystem(), FilesCollectionReader.PARAM_ROOT_FILE,
		                                inputDirectoryName);
		// instantiate your components
		AnalysisEngineDescription sentences = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription tokenizer = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription wordCounter = AnalysisEngineFactory
		                                .createPrimitiveDescription(WordCounter.class,
		                                                                ComponentFactory.getTypeSystem(),
		                                                                WordCounter.PARAM_OUTPUT_FILE, outputFileName);

		SimplePipeline.runPipeline(reader, sentences, tokenizer, wordCounter);

	}

	public static class WordCounter extends JCasAnnotator_ImplBase {

		Map<String, Integer> wordCounts = new HashMap<String, Integer>();

		PrintStream out;

		public static final String PARAM_OUTPUT_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
		                                WordCounter.class, "outputFile");
		@ConfigurationParameter
		protected File outputFile;

		int documentCount = 0;

		@Override
		public void initialize(UimaContext context) throws ResourceInitializationException {
			super.initialize(context);
			try {
				out = new PrintStream(outputFile);
			} catch (FileNotFoundException e) {
				throw new ResourceInitializationException(e);
			}
		}

		@Override
		public void process(JCas jCas) throws AnalysisEngineProcessException {
			for (Token token : JCasUtil.iterate(jCas, Token.class)) {
				String word = token.getCoveredText();
				int count = 0;
				if (wordCounts.containsKey(word)) {
					count = wordCounts.get(word);
				}
				count++;
				wordCounts.put(word, count);
			}
			if (++documentCount % 100 == 0) {
				System.out.println(String.format("processed %d documents and %d words", documentCount,
				                                wordCounts.size()));
			}
		}

		@Override
		public void collectionProcessComplete() throws AnalysisEngineProcessException {
			super.collectionProcessComplete();

			int totalWords = 0;
			for (Entry<String, Integer> entry : wordCounts.entrySet()) {
				totalWords += entry.getValue();
			}

			List<Entry<String, Integer>> entries = new ArrayList<Entry<String, Integer>>(wordCounts.entrySet());
			Collections.sort(entries, new Comparator<Entry<String, Integer>>() {

				@Override
				public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
					int comparison = o1.getValue().compareTo(o2.getValue());
					if (comparison == 0) {
						return o1.getKey().compareTo(o2.getKey());
					} else {
						return -comparison;
					}
				}

			});

			out.println("total number of tokens: " + totalWords);
			for (Entry<String, Integer> entry : entries) {
				out.println(entry.getKey() + "\t" + entry.getValue());
			}
			out.close();
		}
	}

}
