package edu.umn.biomedicus.script.mytutorial;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.ComponentFactory;

public class SampleMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws ResourceInitializationException, UIMAException, IOException {
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

		CollectionReaderDescription dsc = CollectionReaderFactory.createDescription(SimpleCollectionReader.class,
		                                typeSystem, SimpleCollectionReader.PARAM_INPUT_DIRECTORY,
		                                "/home/bill0154/experiments/tutorial");
		CollectionReader reader = CollectionReaderFactory.createCollectionReader(dsc);

		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription sentences = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription pos = ComponentFactory.createPosAnnotator();
		AnalysisEngineDescription xwriter = AnalysisEngineFactory.createPrimitiveDescription(XWriter.class,
		                                XWriter.PARAM_OUTPUT_DIRECTORY_NAME,
		                                "/home/bill0154/experiments/tutorial/output");

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(tokens);
		builder.add(sentences);
		builder.add(pos);
		builder.add(xwriter);

		SimplePipeline.runPipeline(reader, builder.createAggregate());
	}
}
