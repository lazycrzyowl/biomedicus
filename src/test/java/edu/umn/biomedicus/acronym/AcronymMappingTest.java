/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.UmlsConcept;

public class AcronymMappingTest extends BiomedicusTestBase {

	@Test
	public void testAcronymDetection() throws Exception {
		// ExternalResourceDescription extDesc =
		// ExternalResourceFactory.createExternalResourceDescription(
		// AcronymManifest.class, new File("somemodel.bin"));

		ExternalResourceDescription biomedicusDB = ComponentFactory.getBiomedicusDb();
		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription acronymDetector = ComponentFactory.createAcronymAnnotator();
		AnalysisEngineDescription acronymMapper = AnalysisEngineFactory.createPrimitiveDescription(
		                                AcronymConceptMapper.class, ComponentFactory.getTypeSystem(),
		                                AcronymConceptMapper.BIOMEDICUS_DB, biomedicusDB);
		// ExternalResourceFactory.createDependencyAndBind(acronymMapper,
		// UimaUtil.MODEL_PARAMETER, AcronymManifest.class, "", aParams)
		jCas.setDocumentText("There is a CHF, AKA heart something ACL problem with Pt. who AB with ATG does not take his lasik p.i.d.");
		SimplePipeline.runPipeline(jCas, tokens, acronymDetector, acronymMapper);

		// we are basically just testing that the model loads and doesn't throw
		// an exception.
		for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
			assertNotNull(concept);
			// System.out.print("Concept-> " + concept.getCoveredText());
			// System.out.print("   CUIs: ");
			FSArray cuis = concept.getCuis();
			assertNotNull(cuis);
			for (int i = 0; i < cuis.size(); i++) {
				Cui myCui = (Cui) cuis.get(i);
				if (myCui != null)
					assertNotNull(myCui);
			}
		}
	}
}
