/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.acronym;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Assert;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusDBResource_Impl;
import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Acronym;
import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.UmlsConcept;
import edu.umn.biomedicus.wsd.SenseRelateAE;

public class AcronymDisambiguationTest extends BiomedicusTestBase {

	@Test
	public void testAcronymDisambiguation() throws Exception {
		String username = "robertbill";
		String password = "";
		TypeSystemDescription typeDesc = ComponentFactory.getTypeSystem();

		// Create necessary DB resource
		String biomedicusHome = ComponentFactory.getBiomedicusHome();
		ExternalResourceDescription biomedicusDb = ExternalResourceFactory.createExternalResourceDescription(
		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
		                                biomedicusHome);

		AnalysisEngineDescription acronyms = ComponentFactory.createAcronymAnnotator();
		AnalysisEngineDescription sents = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription mm = ComponentFactory.createMetaMapAnnotator();
		AnalysisEngineDescription senses = AnalysisEngineFactory.createPrimitiveDescription(SenseRelateAE.class,
		                                typeDesc, SenseRelateAE.BIOMEDICUS_DB, biomedicusDb);
		AnalysisEngineDescription asd = AnalysisEngineFactory.createPrimitiveDescription(
		                                AcronymLongFormDisambiguationAE.class, typeDesc,
		                                AcronymLongFormDisambiguationAE.BIOMEDICUS_DB, biomedicusDb);

		jCas.setDocumentText("The patient has CHF but not AB.");
		SimplePipeline.runPipeline(jCas, acronyms, sents, mm, senses, asd);

		// we are basically just testing that the model loads and doesn't throw
		// an exception.
		for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
			// prettyPrint(concept);
			Assert.assertNotNull(concept.getCoveredText());
		}

		for (Acronym acronym : JCasUtil.select(jCas, Acronym.class)) {
			Assert.assertNotNull(acronym.getCoveredText());
			// System.out.printf("Acronym: %s, has dominant sense: %s\n",
			// edu.umn.biomedicus.acronym.getCoveredText(), edu.umn.biomedicus.acronym.getExpansion());
		}
		jCas.reset();
	}

	private static void prettyPrint(UmlsConcept concept) {
		if (concept.getCuis().size() == 0)
			return;

		String conceptText = concept.getCoveredText();
		System.out.printf("UmlsConcept Information for text: %s   (%s)", concept.getCoveredText(),
		                                concept.getAnnotSrc());
		StringArray context = concept.getContext();
		if ((context != null) && context.size() > 0) {
			System.out.print("  context: ");
			for (int i = 0; i < concept.getContext().size(); i++) {
				System.out.print(concept.getContext(i) + " ");
			}
		}
		System.out.println();
		for (int i = 0; i < concept.getCuis().size() - 1; i++) {
			Cui cui = (Cui) concept.getCuis(i);
			if (cui == null || cui.getScore() < 900)
				continue;
			System.out.printf("     %-25s", cui.getConceptName());
			System.out.printf("[%s:%s]", cui.getBegin(), cui.getEnd());
			System.out.printf("   CUI: %9s", cui.getCuiId());
			System.out.print(new String("    Score: " + cui.getScore() + "              ").substring(0, 16));
			System.out.print("    Semantic Type: ");
			for (String semType : cui.getSemanticType().toArray()) {
				System.out.print(semType + " ");
			}
			System.out.println();
		}
		Cui sense = (Cui) concept.getCuiSense();
		if (sense == null)
			System.out.println("    Semantic Relatedness scores missing. Can not disambiguate");
		else
			System.out.println("    Preferred sense = " + sense.getCuiId());
		System.out.println();
	}
}
