/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.lvg;

import static org.junit.Assert.assertEquals;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.cache.RedisCacheResource_Impl;
import edu.umn.biomedicus.type.Token;
import gov.nih.nlm.nls.lvg.Api.NormApi;
import gov.nih.nlm.nls.lvg.Api.WordIndApi;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class NormAnnotatorTest extends BiomedicusTestBase {

	@Test
	public void testAnnotator() throws Exception {
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();
		String lvgConfigFileName = ComponentFactory.getResourcePath("config/lvg.properties");

		ExternalResourceDescription cache = ExternalResourceFactory.createExternalResourceDescription(
		                                RedisCacheResource_Impl.class, RedisCacheResource_Impl.PARAM_REDIS_HOST,
		                                "localhost");

		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription norm = AnalysisEngineFactory.createPrimitiveDescription(LvgTokenNormalizer.class,
		                                ComponentFactory.getTypeSystem(), LvgTokenNormalizer.PARAM_LVG_CONFIG_FILE,
		                                lvgConfigFileName, LvgTokenNormalizer.PARAM_CACHE, cache);

		tokenBuilder.buildTokens(jCas, "you're shipping");
		SimplePipeline.runPipeline(jCas, tokens, norm);

		// for (Token token : JCasUtil.select(jCas, Token.class)) {
		// log.info("Test TOKEN: " + token.getCoveredText() + " Norm: " +
		// token.getNorm());
		// }
		Token token = JCasUtil.selectByIndex(jCas, Token.class, 1);
		assertEquals("you_re", token.getNorm());
		token = JCasUtil.selectByIndex(jCas, Token.class, 2);
		log.info("---------------------");
		assertEquals("ship", token.getNorm());
		jCas.reset();
	}

	@Test
	public void testAnnotator2() throws Exception {
		String lvgConfigFileName = ComponentFactory.getResourcePath("config/lvg.properties");
		ExternalResourceDescription cache = ExternalResourceFactory.createExternalResourceDescription(
		                                RedisCacheResource_Impl.class, RedisCacheResource_Impl.PARAM_REDIS_HOST,
		                                "localhost");
		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();

		AnalysisEngineDescription norm = AnalysisEngineFactory.createPrimitiveDescription(LvgTokenNormalizer.class,
		                                ComponentFactory.getTypeSystem(), LvgTokenNormalizer.PARAM_LVG_CONFIG_FILE,
		                                lvgConfigFileName, LvgTokenNormalizer.PARAM_CACHE, cache);
		tokenBuilder.buildTokens(jCas, "you're shipping");
		SimplePipeline.runPipeline(jCas, tokens, norm);

		Token token = JCasUtil.selectByIndex(jCas, Token.class, 1);
		log.info("TOKEN: " + token.getCoveredText() + " Norm: " + token.getNorm() + " Expected: " + "you_re");
		assertEquals("you_re", token.getNorm());
		token = JCasUtil.selectByIndex(jCas, Token.class, 2);
		log.info("TOKEN: " + token.getCoveredText() + " Norm: " + token.getNorm() + " Expected: " + "shipp");
		assertEquals("ship", token.getNorm());
		jCas.reset();
	}

	public void testNormApi() throws Exception {
		String lvgConfigFileName = System.getProperty(NormAnnotatorFactory.LVG_CONFIG_FILE_PROPERTY);
		NormApi normApi = new NormApi(lvgConfigFileName);
		WordIndApi wordIndApi = new WordIndApi();

		for (String word : new String[] { "signaling", "dna_binding", "you_re", "is", "of", "loss_of_function" }) {
			log.info(word);
		}
		assertEquals("signaling", LvgTokenNormalizer.norm(normApi, wordIndApi, "signaling"));
		assertEquals("dna_binding", LvgTokenNormalizer.norm(normApi, wordIndApi, "dna_binding"));
		assertEquals("you_re", LvgTokenNormalizer.norm(normApi, wordIndApi, "you_re"));
		assertEquals("is", LvgTokenNormalizer.norm(normApi, wordIndApi, "is"));
		assertEquals("of", LvgTokenNormalizer.norm(normApi, wordIndApi, "of"));
		assertEquals("loss_of_function", LvgTokenNormalizer.norm(normApi, wordIndApi, "loss-of-function"));
	}
}
