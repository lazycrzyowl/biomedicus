/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.umn.biomedicus.BiomedicusTestBase;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class ProbabalisticSentenceBoundaryDetectorTest extends BiomedicusTestBase {

	@Test
	public void testGetLeftWord() throws Exception {
		String text = "This is a sentence.  Nice! ";
		tokenBuilder.buildTokens(jCas, text, "This is a sentence . Nice !");
		testLeftWord("<BEGIN DOCUMENT>", 0); // T
		testLeftWord("T", 1); // h
		testLeftWord("Th", 2); // i
		testLeftWord("Thi", 3); // s
		testLeftWord("This", 4); //
		testLeftWord("This", 5); // i
		testLeftWord("i", 6); // s
		testLeftWord("is", 7); //
		testLeftWord("is", 8); // a
		testLeftWord("a", 9); //
		testLeftWord("a", 10); // s
		testLeftWord("s", 11); // e
		testLeftWord("se", 12); // n
		testLeftWord("sen", 13); // t
		testLeftWord("sent", 14); // e
		testLeftWord("sente", 15); // n
		testLeftWord("senten", 16); // c
		testLeftWord("sentenc", 17); // e
		testLeftWord("sentence", 18); // .
		testLeftWord("sentence.", 19); //
		testLeftWord("sentence.", 20); //
		testLeftWord("sentence.", 21); // N
		testLeftWord("N", 22); // i
		testLeftWord("Ni", 23); // c
		testLeftWord("Nic", 24); // e
		testLeftWord("Nice", 25); // !
		testLeftWord("Nice!", 26); //
	}

	private void testLeftWord(String expectedWord, int i) {
		// String actualWord =
		// ProbabalisticSentenceBoundaryDetector.getLeftWord(jCas, i);
		// assertEquals(expectedWord, actualWord);
	}

	private void testRightWord(String expectedWord, int i) {
		// String actualWord =
		// ProbabalisticSentenceBoundaryDetector.getRightWord(jCas, i);
		// assertEquals(expectedWord, actualWord);
	}

	@Test
	public void testGetRightToken() throws Exception {
		String text = "Another for testing. This!Was.  Too much.";
		tokenBuilder.buildTokens(jCas, text, "Another for testing . This ! Was . Too much .");
		testRightWord("nother", 0); // A
		testRightWord("other", 1); // n
		testRightWord("ther", 2); // o
		testRightWord("her", 3); // t
		testRightWord("er", 4); // h
		testRightWord("r", 5); // e
		testRightWord("for", 6); // r
		testRightWord("for", 7); //
		testRightWord("or", 8); // f
		testRightWord("r", 9); // o
		testRightWord("testing.", 10); // r
		testRightWord("testing.", 11); //
		testRightWord("esting.", 12); // t
		testRightWord("sting.", 13); // e
		testRightWord("ting.", 14); // s
		testRightWord("ing.", 15); // t
		testRightWord("ng.", 16); // i
		testRightWord("g.", 17); // n
		testRightWord(".", 18); // g
		testRightWord("This!Was.", 19); // .
		testRightWord("This!Was.", 20); //
		testRightWord("his!Was.", 21); // T
		testRightWord("is!Was.", 22); // h
		testRightWord("s!Was.", 23); // i
		testRightWord("!Was.", 24); // s
		testRightWord("Was.", 25); // !
		testRightWord("as.", 26); // W
		testRightWord("s.", 27); // a
		testRightWord(".", 28); // s
		testRightWord("Too", 29); // .
		testRightWord("Too", 30); //
		testRightWord("Too", 31); //
		testRightWord("oo", 32); // T
		testRightWord("o", 33); // o
		testRightWord("much.", 34); // o
		testRightWord("much.", 35); //
		testRightWord("uch.", 36); // m
		testRightWord("ch.", 37); // u
		testRightWord("h.", 38); // c
		testRightWord(".", 39); // h
		testRightWord("<END DOCUMENT>", 40); // .

		// Token rightToken =
		// ProbabalisticSentenceBoundaryDetector.getRightWord(jCas, 0); //A
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 1); //n
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 2); //o
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 3); //t
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 4); //h
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 5); //e
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 6); //r
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 7); //
		// assertEquals("for", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 8); //f
		// assertEquals("testing", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 9); //o
		// assertEquals("testing", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 10); //r
		// assertEquals("testing", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 11); //
		// assertEquals("testing", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 12); //t
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 13); //e
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 14); //s
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 15); //t
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 16); //i
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 17); //n
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 18); //g
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 19); //.
		// assertEquals("This", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 20); //
		// assertEquals("This", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 21); //T
		// assertEquals("!", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 22); //h
		// assertEquals("!", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 23); //i
		// assertEquals("!", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 24); //s
		// assertEquals("!", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 25); //!
		// assertEquals("Was", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 26); //W
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 27); //a
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 28); //s
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 29); //.
		// assertEquals("Too", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 30); //
		// assertEquals("Too", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 31); //
		// assertEquals("Too", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 32); //T
		// assertEquals("much", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 33); //o
		// assertEquals("much", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 34); //o
		// assertEquals("much", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 35); //
		// assertEquals("much", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 36); //m
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 37); //u
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 38); //c
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 39); //h
		// assertEquals(".", rightToken.getCoveredText());
		// rightToken = ProbabalisticSentenceBoundaryDetector.getRightWord(jCas,
		// 40); //.
		// assertNull(rightToken);
	}

	@Test
	public void testIsSentenceBoundary() throws Exception {
		String text = "Sentence 1. Sent.ence 2. Sentence 3.";
		tokenBuilder.buildTokens(jCas, text, "Sentence 1 . \n Sent . ence 2 . \n Sentence 3 .");

		assertEquals(".", text.substring(10, 11));
		// assertTrue(ProbabalisticSentenceBoundaryDetector.isEndOfSentence(jCas,
		// 10));
		// assertEquals(".", text.substring(16, 17));
		// assertFalse(ProbabalisticSentenceBoundaryDetector.isEndOfSentence(jCas,
		// 16));
		// assertEquals(".", text.substring(23, 24));
		// assertTrue(ProbabalisticSentenceBoundaryDetector.isEndOfSentence(jCas,
		// 23));
		// assertEquals(".", text.substring(35, 36));
		// assertTrue(ProbabalisticSentenceBoundaryDetector.isEndOfSentence(jCas,
		// 35));
	}
}
