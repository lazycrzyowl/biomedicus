/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.sentence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.SentenceBoundary;
import edu.umn.biomedicus.type.SentenceBoundaryUtil;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class SentenceAnnotatorTest extends BiomedicusTestBase {

	@Test
	public void testSentenceAnnotator() throws Exception {
		jCas.setDocumentText("  They ran.  They danced.  They cried.  They laughed.   ");
		SentenceBoundary sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 10);
		assertEquals(".", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 24);
		assertEquals(".", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 37);
		assertEquals(".", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 52);
		assertEquals(".", sentenceBoundary.getCoveredText());

		AnalysisEngine sentenceAnnotator = AnalysisEngineFactory.createPrimitive(SentenceAnnotator.class,
		                                typeSystemDescription);
		sentenceAnnotator.process(jCas);

		Sentence sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 0);
		assertEquals("They ran.", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 1);
		assertEquals("They danced.", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 2);
		assertEquals("They cried.", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 3);
		assertEquals("They laughed.", sentence.getCoveredText());

		jCas.reset();
		jCas.setDocumentText("They ran.");
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 8);
		assertEquals(".", sentenceBoundary.getCoveredText());
		sentenceAnnotator.process(jCas);

		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 0);
		assertEquals("They ran.", sentence.getCoveredText());

		jCas.reset();
		jCas.setDocumentText("ABCD");
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 0);
		assertEquals("A", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 1);
		assertEquals("B", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 2);
		assertEquals("C", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 3);
		assertEquals("D", sentenceBoundary.getCoveredText());
		sentenceAnnotator.process(jCas);

		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 0);
		assertEquals("A", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 1);
		assertEquals("B", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 2);
		assertEquals("C", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 3);
		assertEquals("D", sentence.getCoveredText());

		jCas.reset();
		jCas.setDocumentText("\t    They ran.       \n   \t They sang.    They warbled.");
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 13);
		assertEquals(".", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 36);
		assertEquals(".", sentenceBoundary.getCoveredText());
		sentenceBoundary = SentenceBoundaryUtil.createSentenceBoundary(jCas, 53);
		assertEquals(".", sentenceBoundary.getCoveredText());
		sentenceAnnotator.process(jCas);

		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 0);
		assertEquals("They ran.", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 1);
		assertEquals("They sang.", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(jCas, Sentence.class, 2);
		assertEquals("They warbled.", sentence.getCoveredText());

	}

	@Test
	public void testGeniaSentenceAnnotator() throws Exception {
		AnalysisEngineDescription sentenceAnnotator = ComponentFactory.createSentenceAnnotator();

		jCas.setDocumentText("This a test sentence about nothing.  Here's another sentence.  Do you like it?");

		SimplePipeline.runPipeline(jCas, sentenceAnnotator);

		// we are basically just testing that the model loads and doesn't throw
		// an exception and creates some sentences.
		List<Sentence> sentences = new ArrayList<Sentence>(JCasUtil.select(jCas, Sentence.class));
		assertTrue(sentences.size() > 0);

	}

}
