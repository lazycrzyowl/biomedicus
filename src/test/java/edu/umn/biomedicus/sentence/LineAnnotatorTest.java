/* 
  Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.sentence;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.junit.Assert;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.type.Line;

/**
 * 
 * @author Robert Bill
 * 
 */
public class LineAnnotatorTest extends BiomedicusTestBase {

	@Test
	public void testSentenceAnnotator() throws Exception {
		// notice the last line in the sample text does not have a newline.
		// notice there are empty lines (allow blank lines without error)
		jCas.setDocumentText("Line one for testing.\nLine two for testing.\n\nLine three for testing.");

		AnalysisEngine lines = AnalysisEngineFactory.createPrimitive(LineAnnotator.class, typeSystemDescription);
		lines.process(jCas);
		for (Line line : JCasUtil.select(jCas, Line.class)) {
			// System.out.println("line-> " + line.getCoveredText());
			Assert.assertTrue(line.getCoveredText().trim().equals(line.getCoveredText()));
		}
	}
}
