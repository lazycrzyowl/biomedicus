/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.sentence;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.type.SentenceBoundaryUtil;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class PatternSentenceBoundaryDetectorTest extends BiomedicusTestBase {

	@Test
	public void testDecimalPoints() throws Exception {
		AnalysisEngine patternSentenceBoundaryDetector = AnalysisEngineFactory.createPrimitive(
		                                PatternSentenceBoundaryDetector.class, typeSystemDescription);

		jCas.setDocumentText("0.1");
		SimplePipeline.runPipeline(jCas, patternSentenceBoundaryDetector);
		assertNotNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 1));
		jCas.reset();
		jCas.setDocumentText("0.111.5.4.AA.B.1.2");
		SimplePipeline.runPipeline(jCas, patternSentenceBoundaryDetector);
		assertNotNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 1));
		assertFalse(SentenceBoundaryUtil.getSentenceBoundary(jCas, 1).getSentenceBoundary());
		assertNotNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 5));
		assertNotNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 7));
		assertNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 9));
		assertNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 12));
		assertNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 14));
		assertNotNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 16));
	}

	@Test
	public void testNewlines() throws Exception {
		AnalysisEngine patternSentenceBoundaryDetector = AnalysisEngineFactory.createPrimitive(
		                                PatternSentenceBoundaryDetector.class, typeSystemDescription);

		jCas.setDocumentText("\n\nA\n\n");
		SimplePipeline.runPipeline(jCas, patternSentenceBoundaryDetector);
		assertNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 0));
		assertNotNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 2));
		assertTrue(SentenceBoundaryUtil.getSentenceBoundary(jCas, 2).getSentenceBoundary());
		jCas.reset();

		jCas.setDocumentText("A. \n     \r\n    This");
		SimplePipeline.runPipeline(jCas, patternSentenceBoundaryDetector);
		assertNotNull(SentenceBoundaryUtil.getSentenceBoundary(jCas, 1));
	}

}
