/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.indexing;

import java.util.Date;
import java.util.Locale;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Test;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusDBResource_Impl;
import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.corpora.genia.GeniaFactory;
import edu.umn.biomedicus.wsd.SenseRelateAE;

public class LuceneIndexingTest extends BiomedicusTestBase {

	@Test
	public static void main(String[] args) throws Exception {
		// basic log4j configuration
		BasicConfigurator.configure();
		PropertyConfigurator.configure("log4j.properties");
		Locale locale = new Locale("en", "US");

		// get folder directory
		String biomedicusHome = System.getenv("BIOMEDICUS_HOME");
		String corpusLocation = "resources/corpora/edu.umn.biomedicus.acronym";

		// get type system
		final TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

		// Create necessary DB resource
		ExternalResourceDescription biomedicusDB = ExternalResourceFactory
		                                .createExternalResourceDescription(BiomedicusDBResource_Impl.class);

		// Create collection reader
		GeniaFactory geniaFactory = new GeniaFactory();
		CollectionReader reader = geniaFactory.createSmallTestReader();
		// ClinicalFactory clinicalFactory = new ClinicalFactory();
		// CollectionReader reader = clinicalFactory.createTestReader(1);

		// Using a gold corpus for testing, so it already has token, sentence,
		// and pos annotations
		// AnalysisEngineDescription tokenDesc =
		// ComponentFactory.createTokenAnnotator();
		// AnalysisEngine tokens =
		// AnalysisEngineFactory.createAnalysisEngine(tokenDesc,
		// ViewNames.GOLD_VIEW);

		AnalysisEngineDescription sentDesc = ComponentFactory.createSentenceAnnotator();
		// AnalysisEngine sents =
		// AnalysisEngineFactory.createAnalysisEngine(sentDesc,
		// ViewNames.GOLD_VIEW);

		AnalysisEngineDescription nymDesc = ComponentFactory.createAcronymAnnotator();
		// AnalysisEngine nyms =
		// AnalysisEngineFactory.createAnalysisEngine(nymDesc,
		// ViewNames.GOLD_VIEW);

		AnalysisEngineDescription mmDesc = ComponentFactory.createMetaMapAnnotator();
		// AnalysisEngine mm =
		// AnalysisEngineFactory.createAnalysisEngine(mmDesc,
		// ViewNames.GOLD_VIEW);

		AnalysisEngineDescription senseDesc = AnalysisEngineFactory.createPrimitiveDescription(SenseRelateAE.class,
		                                typeSystem, SenseRelateAE.BIOMEDICUS_DB, biomedicusDB);
		// AnalysisEngine senses =
		// AnalysisEngineFactory.createAnalysisEngine(senseDesc,
		// ViewNames.GOLD_VIEW);

		AnalysisEngineDescription lucDesc = AnalysisEngineFactory.createPrimitiveDescription(LuceneDocumentAE.class,
		                                typeSystem, LuceneDocumentAE.PARAM_INDEX_DIR,
		                                "/Users/robertbill/Documents/luceneTest");
		// AnalysisEngine lucas =
		// AnalysisEngineFactory.createAnalysisEngine(lucDesc,
		// ViewNames.GOLD_VIEW);

		AnalysisEngineDescription parserDesc = ComponentFactory.createStanfordParser();
		AnalysisEngineDescription posDesc = ComponentFactory.createPosAnnotator();

		// Build it
		AggregateBuilder builder = new AggregateBuilder();
		// builder.add(nymDesc);
		builder.add(sentDesc);
		builder.add(mmDesc);
		builder.add(senseDesc);
		builder.add(parserDesc);
		builder.add(lucDesc);
		AnalysisEngineDescription cpeDesc = builder.createAggregateDescription();
		AnalysisEngine cpe = AnalysisEngineFactory.createAnalysisEngine(cpeDesc, ViewNames.GOLD_VIEW);
		// SimplePipeline.runPipeline(reader, nymDesc, sentDesc, mmDesc,
		// senseDesc, lucDesc);

		Date start = new Date();
		SimplePipeline.runPipeline(reader, cpe);
		Date end = new Date();
		System.out.println("Runtime for 10 notes is: " + (end.getTime() - start.getTime()) / 1000.0 + " seconds");

		// // Now search the index:
		// FSDirectory directory = FSDirectory.open(new
		// File("/Users/robertbill/Documents/luceneTest"));
		// IndexReader idxReader = IndexReader.open(directory); //
		// DirectoryReader.open(directory);
		// IndexSearcher isearcher = new IndexSearcher(idxReader);
		//
		// // Parse a simple query that searches for "text":
		// Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);

		// QueryParser parser = new QueryParser(Version.LUCENE_40, "", new
		// StandardAnalyzer());
		// Query query = parser.parse("text");
		// Hits hits = isearcher.search(query);
		// assertEquals(1, hits.length());
		//
		// // Iterate through the results:
		// for (int i = 0; i < hits.length(); i++) {
		// Document hitDoc = hits.doc(i);
		// assertEquals("This is the text to be indexed.",
		// hitDoc.get("fieldname"));
		// }
		//
		// isearcher.close();
	}
}
