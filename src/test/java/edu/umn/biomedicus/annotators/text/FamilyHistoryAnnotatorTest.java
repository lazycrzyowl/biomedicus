/* 
 Copyright 2013 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.annotators.text;

import java.util.Date;

import org.apache.log4j.PropertyConfigurator;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Test;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.uimafit.pipeline.SimplePipeline;

/*


import edu.umn.biomedicus.BiomedicusDBResource_Impl;
import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.corpora.genia.GeniaFactory;
import edu.umn.biomedicus.lvg.NormAnnotatorFactory;

*/
public class FamilyHistoryAnnotatorTest  {

  /*
	@Test
	public static void main(String[] args) throws Exception {
		// basic log4j configuration
		PropertyConfigurator.configure("log4j.properties");
		// Locale locale = new Locale("en", "US");

		// get type system
		final TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

		// Create necessary DB resource
		ExternalResourceDescription biomedicusDB = ExternalResourceFactory.createExternalResourceDescription(
		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
		                                "./");

		// Create collection reader
		GeniaFactory geniaFactory = new GeniaFactory();
		CollectionReader reader = geniaFactory.createSmallTestReader();
		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription lvg = NormAnnotatorFactory.createNormAnnotator();
		AnalysisEngineDescription sentDesc = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription posDesc = ComponentFactory.createPosAnnotator();

		// Build it
		AggregateBuilder builder = new AggregateBuilder();
		// builder.add(nymDesc);
		builder.add(sentDesc);
		AnalysisEngineDescription cpeDesc = builder.createAggregateDescription();
		AnalysisEngine cpe = AnalysisEngineFactory.createAnalysisEngine(cpeDesc, ViewNames.GOLD_VIEW);
		// SimplePipeline.runPipeline(reader, nymDesc, sentDesc, mmDesc,
		// senseDesc, lucDesc);

		Date start = new Date();
		SimplePipeline.runPipeline(reader, cpe);
		Date end = new Date();
		System.out.println("Runtime for 10 notes is: " + (end.getTime() - start.getTime()) / 1000.0 + " seconds");
	}

	*/
}
