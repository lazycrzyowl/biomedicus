/* 
  Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.evaluation;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class FScoreTest {

	@Test
	public void testFScore() throws Exception {
		FScore fs = new FScore();
		fs.TPs = 9;
		fs.FPs = 1;
		fs.FNs = 1;
		assertEquals(.9d, fs.recall(), 0.0001);
		assertEquals(.9d, fs.precision(), 0.0001);
		assertEquals(.9d, fs.fscore(), 0.0001);
	}
}
