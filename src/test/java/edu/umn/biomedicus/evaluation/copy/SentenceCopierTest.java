/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.evaluation.copy;

import static org.junit.Assert.assertEquals;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.junit.Test;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.type.Sentence;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class SentenceCopierTest extends BiomedicusTestBase {

	@Test
	public void testSentenceCopier() throws Exception {
		JCas goldView = jCas.createView(ViewNames.GOLD_VIEW);
		JCas systemView = jCas.createView(ViewNames.SYSTEM_VIEW);

		tokenBuilder.buildTokens(goldView, "Sentence 1.\nSentence 2\nSentence 3");
		systemView.setDocumentText("Sentence 1.\nSentence 2\nSentence 3");
		AnalysisEngine sentenceCopier = AnalysisEngineFactory.createPrimitive(SentenceCopier.class,
		                                typeSystemDescription);
		SimplePipeline.runPipeline(jCas, sentenceCopier);

		Sentence sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 0);
		assertEquals("Sentence 1.", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 1);
		assertEquals("Sentence 2", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 2);
		assertEquals("Sentence 3", sentence.getCoveredText());

		jCas.reset();
		goldView = jCas.createView(ViewNames.GOLD_VIEW);
		systemView = jCas.createView(ViewNames.SYSTEM_VIEW);
		tokenBuilder.buildTokens(goldView, "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
		                                "A B C\nD E F G H I\nJ K L M N O\n P Q R S T U V W \n X Y Z");
		systemView.setDocumentText("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		SimplePipeline.runPipeline(jCas, sentenceCopier);

		sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 0);
		assertEquals("ABC", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 1);
		assertEquals("DEFGHI", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 2);
		assertEquals("JKLMNO", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 3);
		assertEquals("PQRSTUVW", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(systemView, Sentence.class, 4);
		assertEquals("XYZ", sentence.getCoveredText());
	}

	@Test
	public void testSentenceCopierWithDifferentViews() throws Exception {
		JCas view1 = jCas.createView("View1");
		JCas view2 = jCas.createView("View2");

		tokenBuilder.buildTokens(view1, "Sentence 1.\nSentence 2\nSentence 3");
		view2.setDocumentText("Sentence 1.\nSentence 2\nSentence 3");
		AnalysisEngineDescription sentenceCopier = AnalysisEngineFactory.createPrimitiveDescription(
		                                SentenceCopier.class, typeSystemDescription);

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(sentenceCopier, ViewNames.GOLD_VIEW, "View1", ViewNames.SYSTEM_VIEW, "View2");

		SimplePipeline.runPipeline(jCas, builder.createAggregate());

		Sentence sentence = JCasUtil.selectByIndex(view2, Sentence.class, 0);
		assertEquals("Sentence 1.", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(view2, Sentence.class, 1);
		assertEquals("Sentence 2", sentence.getCoveredText());
		sentence = JCasUtil.selectByIndex(view2, Sentence.class, 2);
		assertEquals("Sentence 3", sentence.getCoveredText());

	}

}
