/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.evaluation.copy;

import static org.junit.Assert.assertEquals;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.jcas.JCas;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ViewNames;
import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class TokenCopierTest extends BiomedicusTestBase {

	@Test
	public void testTokenCopier() throws Exception {

		JCas goldView = jCas.createView(ViewNames.GOLD_VIEW);
		JCas systemView = jCas.createView(ViewNames.SYSTEM_VIEW);

		tokenBuilder.buildTokens(goldView, "A B C D E F G\nH I J K");
		systemView.setDocumentText("A B C D E F G\nH I J K");
		AnalysisEngine tokenCopier = AnalysisEngineFactory.createPrimitive(TokenCopier.class, typeSystemDescription);
		SimplePipeline.runPipeline(jCas, tokenCopier);

		Token token = JCasUtil.selectByIndex(systemView, Token.class, 0);
		assertEquals("A", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 1);
		assertEquals("B", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 2);
		assertEquals("C", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 3);
		assertEquals("D", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 4);
		assertEquals("E", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 5);
		assertEquals("F", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 6);
		assertEquals("G", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 7);
		assertEquals("H", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 8);
		assertEquals("I", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 9);
		assertEquals("J", token.getCoveredText());
		token = JCasUtil.selectByIndex(systemView, Token.class, 10);
		assertEquals("K", token.getCoveredText());

		jCas.reset();
		goldView = jCas.createView(ViewNames.GOLD_VIEW);
		systemView = jCas.createView(ViewNames.SYSTEM_VIEW);

		tokenBuilder.buildTokens(goldView, "ABCD", "A B C D", "1 2 3 4", "a b c d");
		JCasUtil.selectByIndex(goldView, Token.class, 0).setNorm("_a_");
		JCasUtil.selectByIndex(goldView, Token.class, 1).setNorm("_b_");
		JCasUtil.selectByIndex(goldView, Token.class, 2).setNorm("_c_");
		JCasUtil.selectByIndex(goldView, Token.class, 3).setNorm("_d_");

		systemView.setDocumentText("ABCD");

		tokenCopier = AnalysisEngineFactory.createPrimitive(TokenCopier.class, typeSystemDescription,
		                                TokenCopier.PARAM_COPY_POS_TAGS, true, TokenCopier.PARAM_COPY_STEMS, true,
		                                TokenCopier.PARAM_COPY_NORMS, true);
		SimplePipeline.runPipeline(jCas, tokenCopier);

		token = JCasUtil.selectByIndex(systemView, Token.class, 0);
		assertEquals("A", token.getCoveredText());
		assertEquals("1", token.getPos());
		assertEquals("a", token.getStem());
		assertEquals("_a_", token.getNorm());

		token = JCasUtil.selectByIndex(systemView, Token.class, 1);
		assertEquals("B", token.getCoveredText());
		assertEquals("2", token.getPos());
		assertEquals("b", token.getStem());
		assertEquals("_b_", token.getNorm());

		token = JCasUtil.selectByIndex(systemView, Token.class, 2);
		assertEquals("C", token.getCoveredText());
		assertEquals("3", token.getPos());
		assertEquals("c", token.getStem());
		assertEquals("_c_", token.getNorm());

		token = JCasUtil.selectByIndex(systemView, Token.class, 3);
		assertEquals("D", token.getCoveredText());
		assertEquals("4", token.getPos());
		assertEquals("d", token.getStem());
		assertEquals("_d_", token.getNorm());
	}

}
