/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.metamap;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.lvg.NormAnnotatorFactory;
import edu.umn.biomedicus.type.Sentence;

/**
 * 
 * @author UMN Biomedical NLP Group
 * 
 */

public class UmlsConceptAnnotatorTest extends BiomedicusTestBase {

	// @Test
	public static void main(String[] args) throws Exception { // testMetamapAnnotator()
		                                                      // throws
		                                                      // Exception {
		StringBuilder sb = new StringBuilder();
		// sample from 1. Matykiewicz P, Duch W, Pestian J. Nonambiguous Concept
		// Mapping in Medical Domain. Artificial Intelligence and Soft
		// Computing-ICAISC. 2006;941�50.
		sb.append("Fever, left flank pain, pyelonephritis. ");
		sb.append("The right kidney is normal in sonographic appearance with no evidence of scarring, hydronephrosis or calculi. ");
		sb.append("It measures XXXX cm which is normal for patient�s age. The left kidney is enlarged. It measures xx cm in length. ");
		sb.append("No focal areas of abnormal echogenicity or scarring are seen. ");
		sb.append("No hydronephrosis or calculi are identified. Images of the bladder demonstrate no abnormality. ");
		sb.append("Enlargement of the left kidney which may suggest acute pyelonephritis. ");
		sb.append("This could also represent a normal variant. Normal appearing right kidney.");
		// File samples = new File("/Users")
		AnalysisEngineDescription toks = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription stop = ComponentFactory.createStopwordAnnotator();
		AnalysisEngineDescription sents = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription norm = NormAnnotatorFactory.createNormAnnotator();
		AnalysisEngineDescription cuis = ComponentFactory.createUmlsConceptAnnotator(true);
		JCas jCas = ComponentFactory.getJCas();
		jCas.setDocumentText(sb.toString());
		SimplePipeline.runPipeline(jCas, toks, stop, norm, sents, cuis);

		// for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class))
		// {
		// assertNotNull(concept.getCuis());
		// }
		for (Sentence sent : JCasUtil.select(jCas, Sentence.class)) {
			System.out.println(sent.getParse());
		}
	}
}
