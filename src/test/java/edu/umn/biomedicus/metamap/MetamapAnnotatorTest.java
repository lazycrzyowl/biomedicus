/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.metamap;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.UmlsConcept;

/**
 * 
 * @author Robert Bill
 * 
 */

public class MetamapAnnotatorTest extends BiomedicusTestBase {

	@Test
	public void testMetamapAnnotator() throws Exception {
		AnalysisEngineDescription sents = ComponentFactory.createSentenceAnnotator();
		AnalysisEngineDescription metamap = ComponentFactory.createMetaMapAnnotator();
		jCas.setDocumentText("The M.D. prescribed lasik b.i.d. for 3 weeks.\n The patient denies chest pain is associated with his congestive heart failure.");
		SimplePipeline.runPipeline(jCas, sents, metamap);

		for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
			assertNotNull(concept.getCuis());
		}
	}

	@Test
	public void testContainerType() throws Exception {
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();
		AnalysisEngineDescription lines = ComponentFactory.createLineAnnotator();
		AnalysisEngineDescription metamap = AnalysisEngineFactory.createPrimitiveDescription(MetaMapAE.class,
		                                typeSystem, MetaMapAE.PARAM_CONTAINER_CLASS, "edu.umn.biomedicus.type.Line");
		jCas.setDocumentText("The M.D. prescribed lasik b.i.d. for 3 weeks.\n The patient denies chest pain is associated with his congestive heart failure.");
		SimplePipeline.runPipeline(jCas, lines, metamap);

		for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
			assertNotNull(concept.getCuis());
		}
	}
}
