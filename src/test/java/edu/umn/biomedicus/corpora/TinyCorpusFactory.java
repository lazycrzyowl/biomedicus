/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.corpora;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.util.cr.XReader;
import org.uimafit.component.NoOpAnnotator;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.CollectionReaderFactory;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.evaluation.CorpusFactory;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class TinyCorpusFactory implements CorpusFactory {

	private String xmiDirectory = "src/test/resources/tinycorpus/xmi";

	public CollectionReader createReader() throws ResourceInitializationException {
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_SUFFIXES,
		                                new String[] { ".xmi" });
	}

	public CollectionReader createTrainReader() throws ResourceInitializationException {
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_FILE_NAMES, new String[] {
		                                                                "1.xmi", "2.xmi", "3.xmi", "4.xmi", "5.xmi",
		                                                                "6.xmi", "7.xmi", "8.xmi", "9.xmi", "10.xmi" });
	}

	public CollectionReader createTestReader() throws ResourceInitializationException {
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_FILE_NAMES,
		                                new String[] { "11.xmi" });
	}

	private String getFoldName(int fold) {
		return "" + fold + ".xmi";
	}

	public CollectionReader createTrainReader(int fold) throws ResourceInitializationException {
		List<String> fileNamesList = new ArrayList<String>();
		for (int i = 1; i <= 10; i++) {
			if (i != fold) {
				fileNamesList.add(getFoldName(i));
			}
		}

		String[] fileNames = fileNamesList.toArray(new String[fileNamesList.size()]);

		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_FILE_NAMES, fileNames);
	}

	public CollectionReader createTestReader(int fold) throws ResourceInitializationException {
		String fileName = getFoldName(fold);
		return CollectionReaderFactory.createCollectionReader(XReader.class, ComponentFactory.getTypeSystem(),
		                                XReader.PARAM_ROOT_FILE, xmiDirectory, XReader.PARAM_FILE_NAMES,
		                                new String[] { fileName });
	}

	/**
	 * We don't need to do any preprocessing here because all of the data is
	 * already in the xmi files.
	 */
	public AnalysisEngineDescription createPreprocessor() throws ResourceInitializationException {
		return AnalysisEngineFactory.createPrimitiveDescription(NoOpAnnotator.class, ComponentFactory.getTypeSystem());
	}

	public int numberOfFolds() {
		return 10;
	}

}
