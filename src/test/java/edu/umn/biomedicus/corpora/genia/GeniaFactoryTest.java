/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.corpora.genia;

import static org.junit.Assert.assertEquals;

import org.apache.uima.collection.CollectionReader;
import org.cleartk.util.cr.XReader;
import org.junit.Test;

import edu.umn.biomedicus.BiomedicusTestBase;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class GeniaFactoryTest extends BiomedicusTestBase {

	public void testReaderCounts() throws Exception {
		if (!RUN_LONG_TESTS) {
			log.info(MESSAGE_SKIP_LONG_TEST);
			return;
		}

		GeniaFactory geniaFactory = new GeniaFactory();
		testCollectionReaderCount(geniaFactory.createReader(), 1999);
		testCollectionReaderCount(geniaFactory.createTrainReader(), 1600);
		testCollectionReaderCount(geniaFactory.createTestReader(), 399);
		for (int i = 1; i < 10; i++) {
			testCollectionReaderCount(geniaFactory.createTrainReader(i), 1440);
			testCollectionReaderCount(geniaFactory.createTestReader(i), 160);
		}
	}

	@Test
	public void testSmallReaderCounts() throws Exception {
		SmallGeniaFactory smallGeniaFactory = new SmallGeniaFactory();
		testCollectionReaderCount(smallGeniaFactory.createReader(), 15);
		testCollectionReaderCount(smallGeniaFactory.createTrainReader(), 10);
		testCollectionReaderCount(smallGeniaFactory.createTestReader(), 4);
		for (int i = 1; i < 10; i++) {
			testCollectionReaderCount(smallGeniaFactory.createTrainReader(i), 9);
			testCollectionReaderCount(smallGeniaFactory.createTestReader(i), 1);
		}
	}

	@Test
	public void testFoldFiles() throws Exception {
		GeniaFactory geniaFactory = new GeniaFactory();
		CollectionReader foldReader = geniaFactory.createTrainReader(1);
		String[] fileNames = (String[]) foldReader.getMetaData().getConfigurationParameterSettings()
		                                .getParameterValue(XReader.PARAM_NAME_FILES_FILE_NAMES);
		assertEquals(9, fileNames.length);
		assertEquals("resources/corpora/genia/names/fold02.txt", fileNames[0]);
		assertEquals("resources/corpora/genia/names/fold03.txt", fileNames[1]);
		assertEquals("resources/corpora/genia/names/fold04.txt", fileNames[2]);
		assertEquals("resources/corpora/genia/names/fold05.txt", fileNames[3]);
		assertEquals("resources/corpora/genia/names/fold06.txt", fileNames[4]);
		assertEquals("resources/corpora/genia/names/fold07.txt", fileNames[5]);
		assertEquals("resources/corpora/genia/names/fold08.txt", fileNames[6]);
		assertEquals("resources/corpora/genia/names/fold09.txt", fileNames[7]);
		assertEquals("resources/corpora/genia/names/fold10.txt", fileNames[8]);

		foldReader = geniaFactory.createTrainReader(5);
		fileNames = (String[]) foldReader.getMetaData().getConfigurationParameterSettings()
		                                .getParameterValue(XReader.PARAM_NAME_FILES_FILE_NAMES);
		assertEquals(9, fileNames.length);
		assertEquals("resources/corpora/genia/names/fold01.txt", fileNames[0]);
		assertEquals("resources/corpora/genia/names/fold02.txt", fileNames[1]);
		assertEquals("resources/corpora/genia/names/fold03.txt", fileNames[2]);
		assertEquals("resources/corpora/genia/names/fold04.txt", fileNames[3]);
		assertEquals("resources/corpora/genia/names/fold06.txt", fileNames[4]);
		assertEquals("resources/corpora/genia/names/fold07.txt", fileNames[5]);
		assertEquals("resources/corpora/genia/names/fold08.txt", fileNames[6]);
		assertEquals("resources/corpora/genia/names/fold09.txt", fileNames[7]);
		assertEquals("resources/corpora/genia/names/fold10.txt", fileNames[8]);

	}

}
