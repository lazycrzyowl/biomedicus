/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.corpora;

import org.junit.Test;

import edu.umn.biomedicus.BiomedicusTestBase;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class TinyCorpusFactoryTest extends BiomedicusTestBase {

	@Test
	public void testSmallReaderCounts() throws Exception {
		TinyCorpusFactory tinyCorpusFactory = new TinyCorpusFactory();
		testCollectionReaderCount(tinyCorpusFactory.createReader(), 11);
		testCollectionReaderCount(tinyCorpusFactory.createTrainReader(), 10);
		testCollectionReaderCount(tinyCorpusFactory.createTestReader(), 1);
		for (int i = 1; i < 10; i++) {
			testCollectionReaderCount(tinyCorpusFactory.createTrainReader(i), 9);
			testCollectionReaderCount(tinyCorpusFactory.createTestReader(i), 1);
		}
	}

}
