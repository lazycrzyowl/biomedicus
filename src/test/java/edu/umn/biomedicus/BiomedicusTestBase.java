/**
 * This class borrows heavily from org.cleartk.test.CleartkTestBase and org.cleartk.ToolkitTestBase which
 * is subject to the following copyright and license.  Modifications include using the Biomedicus type system and
 * merging the two files.
 * 
 * Copyright (c) 2010, Regents of the University of Colorado 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. 
 * Neither the name of the University of Colorado at Boulder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. 
 */
package edu.umn.biomedicus;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.uimafit.component.NoOpAnnotator;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.JCasFactory;
import org.uimafit.pipeline.JCasIterable;
import org.uimafit.testing.factory.TokenBuilder;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

/**
 * <br>
 * Copyright (c) 2010, Regents of the University of Colorado <br>
 * All rights reserved.
 * 
 * @author Philip Ogren
 */

public class BiomedicusTestBase {

	private static final String RUN_LONG_TESTS_PROPERTY = "biomedicus.run.long.tests";
	protected static final boolean RUN_LONG_TESTS = System.getProperty(RUN_LONG_TESTS_PROPERTY) != null;
	protected static final String MESSAGE_SKIP_LONG_TEST = String.format(
	                                "Skipping test because it takes a long time to run.  To run this test, supply -D%s at the "
	                                                                + "command line.", RUN_LONG_TESTS_PROPERTY);

	protected static final Logger log = Logger.getLogger(BiomedicusTestBase.class.getName());

	private static ThreadLocal<JCas> JCAS = new ThreadLocal<JCas>();
	private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<TypeSystemDescription>();
	private static ThreadLocal<TokenBuilder<Token, Sentence>> TOKEN_BUILDER = new ThreadLocal<TokenBuilder<Token, Sentence>>();

	protected JCas jCas;
	protected TypeSystemDescription typeSystemDescription;
	protected TokenBuilder<Token, Sentence> tokenBuilder;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	protected File outputDirectory;
	protected String outputDirectoryName;

	static {
		try {
			TypeSystemDescription tsd = ComponentFactory.getTypeSystem();
			TYPE_SYSTEM_DESCRIPTION.set(tsd);

			TokenBuilder<Token, Sentence> tb = new TokenBuilder<Token, Sentence>(Token.class, Sentence.class, "pos",
			                                "stem");
			TOKEN_BUILDER.set(tb);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * we do not want to create a new JCas object every time we run a test
	 * because it is expensive (~100ms on my laptop). Instead, we will have one
	 * JCas per thread sitting around that we will reset everytime a new test is
	 * called.
	 */
	@Before
	public void setUp() throws Exception {
		typeSystemDescription = TYPE_SYSTEM_DESCRIPTION.get();

		jCas = JCAS.get();
		if (jCas == null) {
			jCas = JCasFactory.createJCas(typeSystemDescription);
			JCAS.set(jCas);
		}
		jCas.reset();

		outputDirectory = folder.newFolder("output");
		outputDirectoryName = outputDirectory.getPath();

		tokenBuilder = TOKEN_BUILDER.get();

	}

	public int getCollectionReaderCount(CollectionReader reader) throws UIMAException, IOException {
		AnalysisEngine ae = AnalysisEngineFactory.createPrimitive(NoOpAnnotator.class, typeSystemDescription);

		int count = 0;
		JCasIterable jCases = new JCasIterable(reader, ae);
		for (@SuppressWarnings("unused")
		JCas jcs : jCases) {
			count++;
		}
		return count;
	}

	public void testCollectionReaderCount(CollectionReader reader, int expectedCount) throws UIMAException, IOException {
		assertEquals(expectedCount, getCollectionReaderCount(reader));
	}

}
