/* 
Copyright 2012 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */
package edu.umn.biomedicus.util;

import java.util.Locale;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;

public class JythonAnnotatorTest extends BiomedicusTestBase {

	@Test
	public void testJythonAnnotator() throws Exception {
		Locale locale = Locale.US;
		String jythonFile = ComponentFactory.getJythonFile("SampleAnnotator.py");
		AnalysisEngineDescription jythonAnnotator = AnalysisEngineFactory.createPrimitiveDescription(
		                                JythonAnnotator.class, ComponentFactory.getTypeSystem(),
		                                JythonAnnotator.PARAM_JYTHON_FILE, jythonFile);
		jCas.setDocumentText("This is a sample sentence to test the jython annotator.");
		SimplePipeline.runPipeline(jCas, jythonAnnotator);
	}
}
