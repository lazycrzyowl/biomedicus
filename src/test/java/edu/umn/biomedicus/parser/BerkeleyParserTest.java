/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.parser;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Phrase;
import edu.umn.biomedicus.type.Sentence;

/**
 * 
 * @author Robert Bill
 * 
 */

public class BerkeleyParserTest extends BiomedicusTestBase {

	@Test
	public void berkeleyTest() throws Exception {
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();
		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
		AnalysisEngineDescription pos = ComponentFactory.createPosAnnotator();
		AnalysisEngineDescription sents = ComponentFactory.createSentenceAnnotator();

		AnalysisEngineDescription parser = AnalysisEngineFactory.createPrimitiveDescription(BerkeleyParserAE.class,
		                                typeSystem, BerkeleyParserAE.PARAM_GRAMMAR_FILE,
		                                ComponentFactory.getResourcePath("models/parse/eng.sm5.gr.20120207.gz"));

		jCas.setDocumentText("The M.D. prescribed lasik b.i.d. for 3 weeks.\n The patient denies chest pain is associated with his congestive heart failure.\n This is a test.");
		SimplePipeline.runPipeline(jCas, tokens, sents, pos, parser);

		for (Sentence sentence : JCasUtil.select(jCas, Sentence.class)) {
			System.out.println(sentence.getCoveredText());
			System.out.println(sentence.getParse());
			assertNotNull(sentence.getParse());
			for (Phrase phrase : JCasUtil.selectCovered(Phrase.class, sentence)) {
				System.out.println("    " + phrase.getLabel() + " phrase: " + phrase.getCoveredText());
			}
			System.out.println();
		}
	}
}
