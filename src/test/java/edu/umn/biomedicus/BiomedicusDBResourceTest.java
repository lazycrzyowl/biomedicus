/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Test;
import org.uimafit.factory.ExternalResourceFactory;

public class BiomedicusDBResourceTest extends BiomedicusTestBase {

	@Test
	public void testDbConnect() throws Exception {
		ExternalResourceDescription dbRes = ExternalResourceFactory.createExternalResourceDescription(
		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
		                                ComponentFactory.getBiomedicusHome());

		assertNotNull(dbRes);
	}
}
