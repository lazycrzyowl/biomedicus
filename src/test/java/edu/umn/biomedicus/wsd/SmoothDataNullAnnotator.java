package edu.umn.biomedicus.wsd;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ExternalResource;

public class SmoothDataNullAnnotator extends JCasAnnotator_ImplBase {
	public static final String SMOOTH_DATA_RESOURCE = "smootherResource";
	@ExternalResource(key = SMOOTH_DATA_RESOURCE, mandatory = true)
	private DataSmoother smootherResource;

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		System.out.println(smootherResource);
	}

}