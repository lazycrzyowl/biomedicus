package edu.umn.biomedicus.wsd;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Assert;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;

public class ResourceTest {

	@Test
	public void resourceTest() throws Exception {
		ExternalResourceDescription dbResource = ExternalResourceFactory.createExternalResourceDescription(
		                                DbResource.class, DbResource.PARAM_JDBC_URL, "jdbc:hsqldb:file://file");

		ExternalResourceDescription dataSmoother = ExternalResourceFactory.createExternalResourceDescription(
		                                DataSmoother.class, DataSmoother.DB_RESOURCE, dbResource);

		AnalysisEngineDescription aed = AnalysisEngineFactory.createPrimitiveDescription(SmoothDataNullAnnotator.class,
		                                SmoothDataNullAnnotator.SMOOTH_DATA_RESOURCE, dataSmoother);

		AnalysisEngine ae = AnalysisEngineFactory.createAggregate(aed);
		Assert.assertNotNull(ae);
	}
}
