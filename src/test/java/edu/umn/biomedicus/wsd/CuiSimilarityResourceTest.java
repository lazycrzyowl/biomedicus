/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.wsd;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusDBResource_Impl;
import edu.umn.biomedicus.BiomedicusTestBase;

public class CuiSimilarityResourceTest extends BiomedicusTestBase {

	@Test
	public void testAcronymDetection() throws Exception {
		String biomedicusHome = System.getenv("BIOMEDICUS_HOME");

		// Create necessary resources
		ExternalResourceDescription biomedicusDb = ExternalResourceFactory.createExternalResourceDescription(
		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
		                                biomedicusHome);

		ExternalResourceDescription cuiSimilarity = ExternalResourceFactory.createExternalResourceDescription(
		                                CuiSimilarityResource_Impl.class, CuiSimilarityResource_Impl.DB_RESOURCE,
		                                biomedicusDb);

		// Create annotator
		AnalysisEngineDescription testAnnotatorDescription = AnalysisEngineFactory.createPrimitiveDescription(
		                                NoOpSimilarityTestAnnotator.class, NoOpSimilarityTestAnnotator.CUI_SIMILARITY,
		                                cuiSimilarity);

		// Check the external resource was injected
		AnalysisEngine ae = AnalysisEngineFactory.createAggregate(testAnnotatorDescription);
		assertNotNull(ae);
		jCas.setDocumentText("There is a CHF problem with Pt. who takes lasik p.i.d.");
		SimplePipeline.runPipeline(jCas, ae);
	}
}
