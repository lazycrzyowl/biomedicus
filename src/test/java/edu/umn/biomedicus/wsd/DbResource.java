package edu.umn.biomedicus.wsd;

import org.uimafit.component.Resource_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;

public class DbResource extends Resource_ImplBase {
	public static final String PARAM_JDBC_URL = "jdbcUrl";
	@ConfigurationParameter(name = PARAM_JDBC_URL)
	private String jdbcUrl;

	@Override
	public void afterResourcesInitialized() {
		System.out.println(jdbcUrl);
	}

	public boolean test() {
		return true;
	}
}
