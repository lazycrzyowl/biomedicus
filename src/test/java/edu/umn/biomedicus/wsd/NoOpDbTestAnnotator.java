/* 
 Copyright 2011-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.wsd;

/**
 * Simple annotator to test the DB resource
 */
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceProcessException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ExternalResource;

import edu.umn.biomedicus.BiomedicusDBResource_Impl;

public class NoOpDbTestAnnotator extends JCasAnnotator_ImplBase {
	public static final String BIOMEDICUS_DB = "biomedicusDb";
	@ExternalResource(key = BIOMEDICUS_DB)
	private BiomedicusDBResource_Impl biomedicusDb;

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		int count = 0;
		try {
			Connection con = null;
			try {
				con = biomedicusDb.getConnection();
			} catch (ResourceProcessException e) {
				e.printStackTrace();
			}
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM similarity LIMIT 10");
			while (rs.next()) {
				// System.out.printf("%s, %s, %.3f, %.3f \n", rs.getString(1),
				// rs.getString(2), rs.getFloat(5),
				// rs.getFloat(7));
				count++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		assertTrue(count > 0);
	}
}