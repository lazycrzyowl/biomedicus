package edu.umn.biomedicus.wsd;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;

import edu.umn.biomedicus.BiomedicusDBResource_Impl;

public class CuiSimTest {

	/**
	 * @param args
	 */
	@Test
	public void simTest() throws Exception {

		ExternalResourceDescription biomedicusDb = ExternalResourceFactory.createExternalResourceDescription(
		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
		                                "/workspace/Biomedicus");

		ExternalResourceDescription cuiSimilarity = ExternalResourceFactory.createExternalResourceDescription(
		                                CuiSimilarityResource_Impl.class, CuiSimilarityResource_Impl.DB_RESOURCE,
		                                biomedicusDb);

		AnalysisEngineDescription aed = AnalysisEngineFactory.createPrimitiveDescription(
		                                NoOpSimilarityTestAnnotator.class, NoOpSimilarityTestAnnotator.CUI_SIMILARITY,
		                                cuiSimilarity);

		// Check the external resource was injected
		AnalysisEngine ae = AnalysisEngineFactory.createAggregate(aed);
		ae.process(ae.newJCas());
	}
}
