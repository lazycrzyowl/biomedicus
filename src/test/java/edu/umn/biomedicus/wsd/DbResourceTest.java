/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.wsd;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;

/**
 * Simple test of biomedicusdb resource
 * 
 * @author Robert Bill
 * 
 */
public class DbResourceTest extends BiomedicusTestBase {

	@Test
	public void testAcronymDetection() throws Exception {
		String biomedicusHome = ComponentFactory.getBiomedicusHome();

		// Create necessary resources
		ExternalResourceDescription biomedicusDB = ComponentFactory.getBiomedicusDb();

		// Create annotator
		AnalysisEngineDescription testAnnotator = AnalysisEngineFactory.createPrimitiveDescription(
		                                NoOpDbTestAnnotator.class, NoOpDbTestAnnotator.BIOMEDICUS_DB, biomedicusDB);

		assertNotNull(testAnnotator);
		jCas.setDocumentText("This text doesn't really matter.");
		SimplePipeline.runPipeline(jCas, testAnnotator);

		jCas.reset();
	}
}
