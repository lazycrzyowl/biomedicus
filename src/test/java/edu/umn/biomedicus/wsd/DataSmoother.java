package edu.umn.biomedicus.wsd;

import org.uimafit.component.Resource_ImplBase;
import org.uimafit.descriptor.ExternalResource;

public class DataSmoother extends Resource_ImplBase {
	public static final String DB_RESOURCE = "dbResource";
	@ExternalResource(key = DB_RESOURCE, mandatory = true)
	private DbResource dbResource;

	@Override
	public void afterResourcesInitialized() {
		System.out.println("Acquired Data Resource: " + dbResource.test());
	}
}
