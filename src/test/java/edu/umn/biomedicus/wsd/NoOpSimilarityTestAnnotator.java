/* 
 Copyright 2011-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.wsd;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ExternalResource;

/**
 * Simple analysis engine to test the external resource
 * CuiSimilarityResourec_Impl
 * 
 * @author Robert Bill
 */
public class NoOpSimilarityTestAnnotator extends JCasAnnotator_ImplBase {
	public static final String CUI_SIMILARITY = "similarityRes";
	@ExternalResource(key = CUI_SIMILARITY, mandatory = true)
	private CuiSimilarityResource_Impl similarityRes;

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		assertNotNull(similarityRes);
		// System.out.printf("Similarity: %.3f\n",
		// similarityRes.getSimilarity("C0000727", "C0000731"));
	}
}