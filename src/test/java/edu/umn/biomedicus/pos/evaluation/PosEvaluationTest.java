/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.pos.evaluation;

import org.junit.Test;

import edu.umn.biomedicus.BiomedicusTestBase;

/**
 * 
 * @author Philip Ogren
 * 
 */
public class PosEvaluationTest extends BiomedicusTestBase {

	@Test
	public void testRunPosEvaluation() throws Exception {
		if (!RUN_LONG_TESTS) {
			log.info(MESSAGE_SKIP_LONG_TEST);
			return;
		}

		// not ready yet...
		// String[] args = new String[] { "-o", outputDirectory.getPath(), "-c",
		// TinyCorpusFactory.class.getName(),
		// "-dwf", DefaultMaxentDataWriterFactory.class.getName(), "-t", "2",
		// "-t", "1" };
		// PosEvaluation.main(args);

	}

}
