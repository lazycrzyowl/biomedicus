/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.junit.Test;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusTestBase;
import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Sentence;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class TokenizerTest extends BiomedicusTestBase {

	AnalysisEngineDescription tokenizer;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		tokenizer = ComponentFactory.createTokenAnnotator();
	}

	@Test
	public void testsBasics() throws Exception {
		testTokens("1,000 units.", "1,000 units .");
		testTokens("``|;@#`%*+/-", "` ` | ; @ # ` % * + / -");
		testTokens("\"Really!?\" she said.", "\" Really ! ? \" she said .");
		testTokens("PM&R asdf&qwerty & so on.", "PM&R asdf & qwerty & so on .");
		testTokens("taking 18mg of advil.", "taking 18 mg of advil .");
	}

	// Unit tests derived from email sent from James Ryan on Oct 25th 2010.
	@Test
	public void testJR_20101025() throws Exception {
		testTokens("XRAY HIP UNILATERAL 1 VIEW LEFT  **DATE[Jul 01 07]     1703 HOURS",
		                                "XRAY HIP UNILATERAL 1 VIEW LEFT * * DATE [ Jul 01 07 ] 1703 HOURS");
		testTokens("**AGE[90+]-year-old", "* * AGE [ 90 + ] - year - old");
		testTokens("**ID-NUM", "* * ID - NUM");
		testTokens("100/48", "100 / 48");
		testTokens("kidney disease since 04/2008.", "kidney disease since 04 / 2008 .");
		testTokens("Has +2 DP and PT pulses, normal sensation throughout his lower extremities.",
		                                "Has + 2 DP and PT pulses , normal sensation throughout his lower extremities .");
		testTokens("Significant drop in her H and H.", "Significant drop in her H and H .");
		testTokens("We are continuing to work this up.", "We are continuing to work this up .");
		testTokens("He said that he usually gets A's", "He said that he usually gets A's");
		testTokens("At the beginning of 2008, she had a serum creatinine in the range of 1.4-1.5.",
		                                "At the beginning of 2008 , she had a serum creatinine in the range of 1.4 - 1.5 .");
		testTokens("year-old A-Service Chem-7 11-year-old Bender-Gestalt DAT-DAF Children-IV WISC-IV above-average self-absorbed self-focused DSV-IV full-time",
		                                "year - old A - Service Chem - 7 11 - year - old Bender - Gestalt DAT - DAF Children - IV WISC - IV above - average self - absorbed self - focused DSV - IV full - time");

		// testTokens("1. and 2. should be tokens","1. and 2. should be tokens",
		// true);

	}

	private void testTokens(String inputString, String string) throws UIMAException, IOException {
		testTokens(inputString, string, false);
	}

	private void testTokens(String inputString, String tokenString, boolean printTokens) throws UIMAException,
	                                IOException {
		jCas.reset();
		jCas.setDocumentText(inputString);
		new Sentence(jCas, 0, inputString.length()).addToIndexes();
		SimplePipeline.runPipeline(jCas, tokenizer);

		TokenTestUtil.testTokens(jCas, tokenString, printTokens);
	}

}
