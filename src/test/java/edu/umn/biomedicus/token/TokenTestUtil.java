/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Token;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class TokenTestUtil {

	public static void testTokens(JCas jCas, String string) throws UIMAException, IOException {
		testTokens(jCas, string, false);
	}

	public static void testTokens(JCas jCas, String expectedTokensString, boolean printTokens) throws UIMAException,
	                                IOException {

		String[] expectedTokens = expectedTokensString.split("\\s+");

		List<Token> actualTokens = new ArrayList<Token>(JCasUtil.select(jCas, Token.class));

		if (printTokens) {
			for (Token token : actualTokens) {
				System.out.println("token: " + token.getCoveredText());
			}
		}

		assertEquals(expectedTokens.length, actualTokens.size());
		for (int i = 0; i < expectedTokens.length; i++) {
			assertEquals(expectedTokens[i], actualTokens.get(i).getCoveredText());
		}
	}
}
