/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusTestBase;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class TokenSplitterTest extends BiomedicusTestBase {

	AnalysisEngineDescription tokenSplitter;

	@Test
	public void testSplitter() throws Exception {
		AnalysisEngineDescription tokenSplitter = AnalysisEngineFactory.createPrimitiveDescription(TokenSplitter.class,
		                                typeSystemDescription, TokenSplitter.PARAM_SPLIT_PATTERNS_PATH,
		                                "/edu/umn/biomedicus/token/split/test1-split-patterns.txt");
		tokenBuilder.buildTokens(jCas, "150mg 6in. ABCD1234");
		test(jCas, tokenSplitter, "150 mg 6 in. ABCD 1234");

		jCas.reset();
		tokenBuilder.buildTokens(jCas, "222mg 12in AADFa1 A1");
		test(jCas, tokenSplitter, "222 mg 12 in AADFa1 A 1");

	}

	private void test(JCas jCas, AnalysisEngineDescription tokenSplitter, String expectedTokensString) throws Exception {
		SimplePipeline.runPipeline(jCas, tokenSplitter);
		TokenTestUtil.testTokens(jCas, expectedTokensString);
	}
}
