/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token.split;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class SplitMatcherTest {

	@Test
	public void testExamples() throws SplitPatternException {
		SplitPatterns patterns = new SplitPatterns("[a-z]a|t+", "wom|[bczm]at?", "[0-9]+|mg");
		SplitMatcher matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 2 }, "bat", "cat", "zat", "hat", "fat", "batt", "zattttt");
		test(matcher, false, null, "bt", "Cat", "zAt", "hate", "fatr");
		test(matcher, true, new int[] { 3 }, "wombat", "womcat", "womzat", "wommat", "womba", "womca", "womza", "womma");
		test(matcher, false, null, "womat", "wombcat", "womb", "Wombat");
		test(matcher, true, new int[] { 2 }, "18mg", "20mg", "11mg", "10mg");
		test(matcher, true, new int[] { 4 }, "1800mg", "2011mg", "1111mg", "1000mg");
		test(matcher, true, new int[] { 1 }, "1mg", "2mg", "1mg", "0mg");
	}

	@Test
	public void testStringMatching() throws Exception {
		SplitPatterns patterns = new SplitPatterns("a|a", "a|b", "a|a|c", "a|bb", "ab|c", "a|b|a", "ab|d", "b|z",
		                                "b|y", "b|m", "b|a", "ba|a");
		SplitMatcher matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1 }, "aa", "ab", "bz", "by", "bm", "ba");
		test(matcher, true, new int[] { 1, 2 }, "aac", "aba");
		test(matcher, true, new int[] { 1 }, "abb");
		test(matcher, true, new int[] { 2 }, "abc", "abd", "baa");
		test(matcher, false, null, "b", "abcd", "az", "aaa", "abe", "zzz", "z", "", "bw", "baaa", "aab", "bac", "bbb");

		patterns = new SplitPatterns("\\||\\|", "\\||c\\|", "\\[|]|\\|", "\\[|\\*\\+\\?\\|");
		matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1 }, "||", "|c|", "[*+?|");
		test(matcher, true, new int[] { 1, 2 }, "[]|");

		patterns = new SplitPatterns("\\[\\[|\\[\\[", "\\[a|bc", "ab|c\\[", "abc|\\[abc", "\\[\\[\\[|]", "\\[|]",
		                                "\\[|\\\\]");
		matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1 }, "[]", "[\\]");
		test(matcher, true, new int[] { 2 }, "[[[[", "[abc", "abc[");
		test(matcher, true, new int[] { 3 }, "abc[abc", "[[[]");

		patterns = new SplitPatterns("a|\\?");
		matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1 }, "a?");

		patterns = new SplitPatterns("a|\\\\");
		matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1 }, "a\\");

		patterns = new SplitPatterns("a|\\\\\\?");
		matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1 }, "a\\?");

	}

	@Test
	public void testPatternMatching() throws Exception {
		SplitPatterns patterns = new SplitPatterns("[0-9]|[a-z]", "[0-9]|[a-z][A-Z][02468]");
		SplitMatcher matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1 }, "0a", "1b", "2c", "3d", "0aA2", "9tZ8");
		test(matcher, false, null, "a0", "a", "2", "33cc", "0aA1", "8aa2");
	}

	@Test
	public void testSplitting() throws Exception {
		SplitPatterns patterns = new SplitPatterns();
		int patternId = patterns.add("[0-9]|[a-z]");
		SplitMatcher matcher = new SplitMatcher(patterns);
		List<SplitMatch> matches = matcher.match("0a");
		SplitMatch match = matches.get(0);
		int matchedPatternId = match.getPatternId();
		assertEquals(patternId, matchedPatternId);
		String matchedPattern = patterns.getPattern(matchedPatternId);
		assertEquals("[0-9]|[a-z]", matchedPattern);
		test(matcher, "0a", 1);

		patterns = new SplitPatterns("[0-9][0-9]|[a-z]|[A-Z][A-Z]");
		matcher = new SplitMatcher(patterns);
		matches = matcher.match("03aBB");
		assertEquals(1, matches.size());
		match = matches.get(0);
		matchedPatternId = match.getPatternId();
		assertEquals("[0-9][0-9]|[a-z]|[A-Z][A-Z]", patterns.getPattern(matchedPatternId));
		test(matcher, "03aBB", 2, 3);

		patterns = new SplitPatterns("a|b|c|d", "a|b|c|d|e", "abcde|f");
		matcher = new SplitMatcher(patterns);
		matches = matcher.match("abcd");
		matchedPatternId = match.getPatternId();
		matchedPattern = patterns.getPattern(matchedPatternId);
		assertEquals("a|b|c|d", matchedPattern);
		test(matcher, "abcd", 1, 2, 3);
		test(matcher, "abcde", 1, 2, 3, 4);
		test(matcher, "abcdef", 5);

	}

	@Test
	public void testPlus() throws Exception {
		SplitPatterns patterns = new SplitPatterns("a+|b+");
		SplitMatcher matcher = new SplitMatcher(patterns);
		test(matcher, 1, "ab");
		test(matcher, 2, "aab");
		test(matcher, 3, "aaab");
		test(matcher, 3, "aaabbb");
		test(matcher, 10, "aaaaaaaaaab");
		test(matcher, 1, "abb");
		test(matcher, 5, "aaaaabbbbb");

		patterns = new SplitPatterns("a+|bc+");
		matcher = new SplitMatcher(patterns);
		test(matcher, 1, "abc");
		test(matcher, 2, "aabcc");
		test(matcher, 1, "abccccc");
		test(matcher, 10, "aaaaaaaaaabc");

		patterns = new SplitPatterns("a+|b+|c+|d+");
		matcher = new SplitMatcher(patterns);
		test(matcher, true, new int[] { 1, 2, 3 }, "abcd");
		test(matcher, true, new int[] { 3, 6, 9 }, "aaabbbcccddd");
		test(matcher, true, new int[] { 1, 7, 11 }, "abbbbbbccccd");

	}

	@Test
	public void testOptional() throws Exception {
		SplitPatterns patterns = new SplitPatterns("a|bc?d");
		SplitMatcher matcher = new SplitMatcher(patterns);
		test(matcher, "abcd", 1);
		test(matcher, "abd", 1);

		patterns = new SplitPatterns("a|bc?d?e");
		matcher = new SplitMatcher(patterns);
		test(matcher, "abcde", 1);
		test(matcher, "abce", 1);
		test(matcher, "abde", 1);
		test(matcher, "abe", 1);

		patterns = new SplitPatterns("a|bc?d*e");
		matcher = new SplitMatcher(patterns);
		test(matcher, "abcddddddde", 1);
		test(matcher, "abcdde", 1);
		test(matcher, "abcde", 1);
		test(matcher, "abce", 1);
		test(matcher, "abde", 1);
		test(matcher, "abdde", 1);
		test(matcher, "abdddddddde", 1);
		test(matcher, "abe", 1);

		patterns = new SplitPatterns("ab|c?d*e");
		matcher = new SplitMatcher(patterns);
		test(matcher, 2, "abcde");
		test(matcher, 2, "abcdde");
		test(matcher, 2, "abcde");
		test(matcher, 2, "abce");
		test(matcher, 2, "abde");
		test(matcher, 2, "abdde");
		test(matcher, 2, "abdddddddde");
		test(matcher, 2, "abe");

		patterns = new SplitPatterns("ab|c?|d*e");
		matcher = new SplitMatcher(patterns);
		test(matcher, "abe", 2);
		test(matcher, "abcde", 2, 3);
		test(matcher, "abce", 2, 3);
		test(matcher, "abde", 2);
		test(matcher, "abddddde", 2);

		patterns = new SplitPatterns("abc?|d*e");
		matcher = new SplitMatcher(patterns);

		test(matcher, "abcde", 3);
		test(matcher, "abe", 2);
		test(matcher, "abde", 2);

	}

	@Test
	public void testDifficult() throws Exception {
		SplitPatterns patterns = new SplitPatterns("a*|b*");
		SplitMatcher matcher = new SplitMatcher(patterns);
		test(matcher, "ab", 1);
		test(matcher, "aaaab", 4);
		test(matcher, "aaaabbbb", 4);

		patterns = new SplitPatterns("a|bc?");
		matcher = new SplitMatcher(patterns);
		test(matcher, "abc", 1);
		test(matcher, "ab", 1);

		patterns = new SplitPatterns("a|bccc?d");
		matcher = new SplitMatcher(patterns);
		test(matcher, "abccd", 1);
		test(matcher, "abcccd", 1);

		patterns = new SplitPatterns("a?|b");
		matcher = new SplitMatcher(patterns);
		test(matcher, "ab", 1);
		test(matcher, "b", 0);

		patterns = new SplitPatterns("a|b?");
		matcher = new SplitMatcher(patterns);
		test(matcher, "ab", 1);
		test(matcher, "a");

		patterns = new SplitPatterns("a?b?c?d?|e");
		matcher = new SplitMatcher(patterns);
		test(matcher, "abcde", 4);
		test(matcher, "abce", 3);
		test(matcher, "abe", 2);
		test(matcher, "ae", 1);
		test(matcher, "e", 0);

		test(matcher, "bcde", 3);
		test(matcher, "cde", 2);
		test(matcher, "de", 1);

		test(matcher, "ace", 2);
		test(matcher, "bde", 2);
		test(matcher, "ade", 2);

		test(matcher, "be", 1);
		test(matcher, "ce", 1);

		patterns = new SplitPatterns("a|b?|c?|d?|e");
		matcher = new SplitMatcher(patterns);
		test(matcher, "abcde", 1, 2, 3, 4);
		test(matcher, "ae", 1);

		patterns = new SplitPatterns("ab?c*d?|e?f?g");
		matcher = new SplitMatcher(patterns);
		test(matcher, "ag", 1);
		test(matcher, "aceg", 2);
		test(matcher, "abcccccccdefg", 10);
		test(matcher, "acfg", 2);

		patterns = new SplitPatterns("[a-z]+[0-9]+[A-Z]+|[a-z]+[0-9]+[A-Z]+");
		matcher = new SplitMatcher(patterns);
		test(matcher, "a0Aa0A", 3);
		test(matcher, "a0ABCabcd01234ZYXW", 5);

		patterns = new SplitPatterns("[a-z]?[0-9]?[A-Z]?|[a-z]?[0-9]?[A-Z]?");
		matcher = new SplitMatcher(patterns);
		test(matcher, "a0Aa0A", 3);
		test(matcher, "aa", 1);
		test(matcher, "a0b1", 2);
		test(matcher, "aA1", 2);

		patterns = new SplitPatterns("[a-z]*[0-9]*[A-Z]?|[a-z]?[0-9]*[A-Z]*");
		matcher = new SplitMatcher(patterns);
		test(matcher, "a0Aa0A", 3);
		test(matcher, "a0a0", 2);
		test(matcher, "a0b1", 2);
		test(matcher, "aA1", 2);
		test(matcher, "abcdefghij0Z1", 12);

	}

	@Test
	public void testPlusSplit() throws Exception {
		SplitPatterns patterns = new SplitPatterns("a|b?c+d*");
		SplitMatcher matcher = new SplitMatcher(patterns);
		test(matcher, "abcd", 1);
		test(matcher, "acd", 1);
	}

	@Test(expected = SplitPatternException.class)
	public void testEscape() throws SplitPatternException {
		new SplitPatterns("abcd\\");

	}

	@Test(expected = SplitPatternException.class)
	public void testEscape2() throws SplitPatternException {
		new SplitPatterns("abcd\\n");

	}

	@Test(expected = SplitPatternException.class)
	public void testCollision1() throws Exception {
		new SplitPatterns("a|bc?", "ab|c?d?");
	}

	@Test(expected = SplitPatternException.class)
	public void testCollision2() throws Exception {
		new SplitPatterns("a|bc?", "a|bc");
	}

	private void test(SplitMatcher matcher, String word, int... expectedSplits) {
		List<SplitMatch> matches = matcher.match(word);
		assertEquals(1, matches.size());
		SplitMatch match = matches.get(0);
		int[] actualSplits = match.getSplits();
		assertEquals("looking for word = " + word, expectedSplits.length, actualSplits.length);
		for (int i = 0; i < expectedSplits.length; i++) {
			assertEquals(expectedSplits[i], actualSplits[i]);
		}
	}

	/*
	 * here we have two nearly identical patterns differing only by where the
	 * splitIds occur strings like 33aBB should have two matches.
	 */

	@Test(expected = SplitPatternException.class)
	public void testDups() throws Exception {
		new SplitPatterns("[0-9][0-9]|[a-z]|[A-Z][A-Z]", "[0-9]|[0-9][a-z][A-Z]|[A-Z]");
	}

	@Test(expected = SplitPatternException.class)
	public void testBeginSplit() throws Exception {
		new SplitPatterns("|abcd");
	}

	@Test(expected = SplitPatternException.class)
	public void testEndSplit() throws Exception {
		new SplitPatterns("abcd|");
	}

	@Test(expected = SplitPatternException.class)
	public void testNoSplit() throws Exception {
		new SplitPatterns("abcd");
	}

	private void test(SplitMatcher matcher, int i, String word) {
		test(matcher, true, new int[] { i }, word);

	}

	private void test(SplitMatcher matcher, boolean isMatch, int[] expectedSplits, String... words) {
		for (String word : words) {
			List<SplitMatch> matches = matcher.match(word);
			assertEquals("looking for word = " + word, isMatch, matches.size() == 1);
			if (isMatch) {
				int patternId = matches.get(0).getPatternId();
				int[] actualSplits = matches.get(0).getSplits(patternId);
				assertEquals("looking for word = " + word, expectedSplits.length, actualSplits.length);
				for (int i = 0; i < expectedSplits.length; i++) {
					assertEquals("looking for word = " + word, expectedSplits[i], actualSplits[i]);
				}
			}
		}
	}

	@Test
	@Ignore
	public void testname() throws Exception {
		// adding the second pattern should collide with the first and raise an
		// exception.
		SplitPatterns patterns = new SplitPatterns("a|bccd", "a|bccc?d");
		patterns.print(System.out);

	}
}
