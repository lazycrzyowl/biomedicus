/* 
Copyright 2011-2012 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */
package edu.umn.biomedicus.cache;

/**
 * Simple annotator to test the DB resource
 */
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.junit.Assert;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ExternalResource;

public class NoOpCacheAnnotator extends JCasAnnotator_ImplBase {
	public static final String CACHE = "cache";
	@ExternalResource(key = CACHE)
	private RedisCacheResource_Impl cache;

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		System.out.println("Adding to cache...");
		cache.set("foo", "bar");
		System.out.println("Getting from cache...");
		Assert.assertTrue("bar".equals(cache.get("foo")));
	}
}