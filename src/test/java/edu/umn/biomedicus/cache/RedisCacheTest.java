/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.cache;

import static org.junit.Assert.assertNotNull;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.ExternalResourceFactory;
import org.uimafit.pipeline.SimplePipeline;

import edu.umn.biomedicus.BiomedicusTestBase;

public class RedisCacheTest extends BiomedicusTestBase {

	@Test
	public void testDbConnect() throws Exception {
		ExternalResourceDescription cache = ExternalResourceFactory.createExternalResourceDescription(
		                                RedisCacheResource_Impl.class, RedisCacheResource_Impl.PARAM_REDIS_HOST,
		                                "localhost");

		// Create annotator
		AnalysisEngineDescription testAnnotator = AnalysisEngineFactory.createPrimitiveDescription(
		                                NoOpCacheAnnotator.class, NoOpCacheAnnotator.CACHE, cache);

		assertNotNull(testAnnotator);
		jCas.setDocumentText("This text doesn't really matter.");
		SimplePipeline.runPipeline(jCas, testAnnotator);

		jCas.reset();
	}

}
