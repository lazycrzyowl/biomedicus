/* 
Copyright 2010-12 University of Minnesota  
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.lvg;

import edu.umn.biomedicus.cache.BiomedicusCache;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.Token;
import gov.nih.nlm.nls.lvg.Api.NormApi;
import gov.nih.nlm.nls.lvg.Api.WordIndApi;
import org.apache.log4j.Logger;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.mapdb.BTreeMap;
import org.mapdb.DB;

import java.nio.file.Paths;
import java.util.Vector;

/**
 * 
 * @author Philip Ogren
 * @author University of Minneapolis Biomedical NLP Group
 * 
 *         The NormAnnotator finds the normalized version of a token and adds it
 *         to the "norm" attribute of the token type.
 * 
 *         To configure NormAnnotator, LVG must be installed on the computer
 *         that it is run on and the lvg.properties file must be copied into the
 *         "{$BIOMEDICUS_HOME}/resources/config" directory.
 * 
 */

public class LvgAE extends CasAnnotator_ImplBase {

	public static final String PARAM_LVG_CONFIG_FILE = "lvgConfigFile";
    public static final String PARAM_LEX_ACCESS_CONFIG_FILE = "lexAccessConfigFile";

    private String lvgConfigFile;
    private String lexAccessConfigFile;
    private LvgTokenNormalizer normalizer;
    private Type tokenType;
    private Feature normFeature;
    private Feature posFeature;

	public static final String PARAM_CACHE = "cache";
	private BiomedicusCache cache;

	// database cache is source for all cache lookups
	private DB cacheContainer = null;
	private BTreeMap<String, String> normCache;

	// lvg api instances
	protected NormApi normApi = null;
	protected WordIndApi wordIndApi = null;
	protected Logger log = null;

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
        // get necessary config files
        lvgConfigFile = getLvgConfigFile(context);
        lexAccessConfigFile = getLexAccessConfigFile(context);

        // create normalizer class
        normalizer = new LvgTokenNormalizer(lvgConfigFile, lexAccessConfigFile);

        // get cache   TODO:  detach cache from LvgTokenNormalizer
//        try {
//            cache = (BiomedicusCache) context.getResourceObject("cacheResource");
//        }
//        catch (ResourceAccessException e)
//        {
//            throw new ResourceInitializationException(e);
//        }
	}

    private String getLvgConfigFile(UimaContext context)
    {
        String configFile = (String) context.getConfigParameterValue(PARAM_LVG_CONFIG_FILE);
        if (configFile == null)
        {
            // Get from environment vars if not specified in descriptor
            String home = System.getenv("LVG_HOME");
            configFile = Paths.get(home, "data/config/lvg.properties").toString();
        }
        return configFile;
    }

    private String getLexAccessConfigFile(UimaContext context)
    {
        String configFile = (String) context.getConfigParameterValue(PARAM_LEX_ACCESS_CONFIG_FILE);
        if (configFile == null)
        {
            // Get from environment vars if not specified in descriptor
            String home = System.getenv("LEX_ACCESS_HOME");
            configFile = Paths.get(home, "data/config/lexAccess.properties").toString();
        }
        return configFile;
    }

	@Override
	public void process(CAS cas) throws AnalysisEngineProcessException {
        CAS sys = CASUtil.getSystemView(cas);
        for (AnnotationFS token : AnnotationUtils.getAnnotations(sys, Token.class))
        {
            String tokenText = token.getCoveredText();
            String pos = token.getStringValue(posFeature);
            String norm;
            try
            {
                norm = normalizer.norm(tokenText, pos);
            }
            catch (Exception e)
            {
                throw new AnalysisEngineProcessException(e);
            }
			token.setStringValue(normFeature, norm);
        }
	}

    public void typeSystemInit(TypeSystem ts)
    {
        tokenType = ts.getType(Token.class.getCanonicalName());
        normFeature = tokenType.getFeatureByBaseName("norm");
        posFeature = tokenType.getFeatureByBaseName("pos");
    }
}
