/* 
Copyright 2010-14 University of Minnesota
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.lvg;

import edu.umn.biomedicus.cache.RedisCacheResource_Impl;
import gov.nih.nlm.nls.lexAccess.Api.LexAccessApi;
import gov.nih.nlm.nls.lexAccess.Api.LexAccessApiResult;
import gov.nih.nlm.nls.lexCheck.Lib.LexRecord;
import gov.nih.nlm.nls.lvg.Api.NormApi;
import gov.nih.nlm.nls.lvg.Api.WordIndApi;
import org.apache.log4j.Logger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * 
 * @author Philip Ogren
 * @author University of Minneapolis Biomedical NLP Group
 * 
 *         The NormAnnotator finds the normalized version of a token and adds it
 *         to the "norm" attribute of the token type.
 * 
 *         To configure NormAnnotator, LVG must be installed on the computer
 *         that it is run on and the lvg.properties file must be copied into the
 *         "{$BIOMEDICUS_HOME}/resources/config" directory.
 * 
 */

public class LvgTokenNormalizer {

	public static final String PARAM_NORM_DB_FILE = "normDBFileName";
	private String normDBFileName = "/edu/umn/biomedicus/dictionaries/normdb";

	public static final String PARAM_LVG_CONFIG_FILE = "lvgConfigFile";
    private String lvgConfigFile;

    public static final String LEX_ACCESS_CONFIG_FILE = "lexAccessConfigFile";
    private String lexAccessConfigFile;

	public static final String PARAM_CACHE = "cache";
	private RedisCacheResource_Impl cache;

	// lvg api instances
	protected NormApi normApi;
	protected WordIndApi wordIndApi;
    protected LexAccessApi lexAccessApi;
	protected Logger log;
    protected PennPos2SpecialistCategoryMap posMap;

	public LvgTokenNormalizer(String lvgConfigFile, String lexAccessConfigFile) {
        this.normDBFileName = normDBFileName;
        posMap = new PennPos2SpecialistCategoryMap();
        this.lexAccessConfigFile = lexAccessConfigFile;
        this.lvgConfigFile = lvgConfigFile;
        initialize(lvgConfigFile, lexAccessConfigFile);
    }

    private void initialize(String lvgConfigFile, String lexAccessConfigFile)
    {
		normApi = new NormApi(lvgConfigFile);
		wordIndApi = new WordIndApi();
        lexAccessApi = new LexAccessApi(lexAccessConfigFile);
        cache = RedisCacheResource_Impl.build("localhost");
	}

	public String norm(String token, String pos) throws Exception {
		// if normCache contains the word, then return the cached norm value
		// limit norm cache to lower case to limit size
		String subject = token.toLowerCase();
		String normalizedToken = null;
		String cacheKey = new StringBuilder("lvg::")
                .append(subject).append("::")
                .append(pos).toString();
		if (cache != null) {
			try {
				normalizedToken = cache.get(cacheKey);
			} catch (Exception e) {
				// For any reason, disable cache
                e.printStackTrace();
				this.cache = null;
			}
			if (normalizedToken != null) {
				return normalizedToken;
			} else {
                return normalizer(subject, pos, cacheKey);
            }
		}
        else
        {
            return normalizer(subject, pos, cacheKey);
        }
	}

    public String normalizer(String subject, String pos, String cacheKey) throws Exception
    {
        String normalizedToken = norm(normApi, wordIndApi, subject, pos);
        cache.set(cacheKey, normalizedToken);
        return (normalizedToken != null) ? normalizedToken : subject;
    }

	public String norm(NormApi normApi, WordIndApi wordIndApi, String token, String pos) throws Exception {
        String subject = token.toLowerCase();
        String value = norm(normApi, wordIndApi, subject, pos, true);
        return value;
	}

	private String norm(NormApi normApi, WordIndApi wordIndApi, String token, String pos, boolean recurse)
            throws Exception {
		// Otherwise, we will invoke the normApi to get the normalization
		Vector<String> normVector = normApi.Mutate(token);

		// not sure what the contract is of normApi.Mutate, so am hedging my
		// bets in case null or an empty vector is returned
		if (normVector == null || normVector.size() == 0) {
			return token;
		}

        if (normVector.size() == 1) return normVector.get(0);

        // if more than one choice, find the base that matches the POS category
		for (String candidate : normVector)
        {
            LexAccessApiResult results = lexAccessApi.GetLexRecords(candidate);
            for (LexRecord record : results.GetJavaObjs())
            {
                String cat = record.GetCategory();
                String wordCategory = posMap.getSpecialistCategory(pos);
                if (cat.equals(wordCategory))
                    return candidate;
            }
        }

        // if no match found yet, return the first entry
        String norm = normVector.get(0);
        return norm;
	}

    public static void main(String[] args) throws Exception
    {
        String lvg_home = System.getenv("LVG_HOME");
        Path lvgFilePath = Paths.get(lvg_home, "/data/config/lvg.properties");
        String lex_home = System.getenv("LEX_ACCESS_HOME");
        Path lexFilePath = Paths.get(lex_home, "/data/config/lexAccess.properties");
        LvgTokenNormalizer lvg = new LvgTokenNormalizer(
                lvgFilePath.toString(), lexFilePath.toString());
        String token = "left";
        String pos = "ADJ";

        System.out.println(String.format("Normalized form for token '%s' is: %s", token, lvg.norm(token, pos)));
    }

    private class PennPos2SpecialistCategoryMap {
        Map<String, String> lookup = new HashMap<>();

        public PennPos2SpecialistCategoryMap() {
            lookup.put("CC", "conj");
            lookup.put("CD", null);
            lookup.put("DT", "det");
            lookup.put("EX", "adj");
            lookup.put("FW", null);
            lookup.put("IN", "prep");
            lookup.put("JJ", "adj");
            lookup.put("JJR", "adj");
            lookup.put("JJS", "adj");
            lookup.put("ADJ", "adj");
            lookup.put("LS", null);
            lookup.put("MD", "modal");
            lookup.put("NN", "noun");
            lookup.put("NNP", "noun");
            lookup.put("NNPS", "noun");
            lookup.put("NNS", "noun");
            lookup.put("PDT", "pron");
            lookup.put("POS", null);
            lookup.put("PRP", "pron");
            lookup.put("PRP$", "pron");
            lookup.put("RB", "adv");
            lookup.put("RBR", "adv");
            lookup.put("RBS", "adv");
            lookup.put("RP", null);
            lookup.put("SYM", null);
            lookup.put("TO", "adv");
            lookup.put("UH", null);
            lookup.put("VB", "verb");
            lookup.put("VBD", "verb");
            lookup.put("VBG", "verb");
            lookup.put("VBN", "verb");
            lookup.put("VBP", "verb");
            lookup.put("VBZ", "verb");
            lookup.put("WDT", "pron");
            lookup.put("WP", "pron");
            lookup.put("WP$", "pron");
            lookup.put("WRB", "adv");
            lookup.put("#", null);
            lookup.put("$", null);
            lookup.put("'", null);
            lookup.put("\"", null);
            lookup.put(",", null);
            lookup.put("(", null);
            lookup.put(")", null);
            lookup.put(".", null);
            lookup.put(":", null);
            lookup.put(";", null);
        }

        public String getSpecialistCategory(String pennPOS)
        {
            return lookup.get(pennPOS);
        }
    }
}
