package edu.umn.biomedicus.lvg;

import edu.umn.biomedicus.core.ResourceFactory;
import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import edu.umn.biomedicus.tokenizing.services.TokenizingServiceProvider;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Normalizer base application.
 */
public final class LVG
{
    @NotNull
    protected AnalysisEngine ae;
    @NotNull
    protected CAS aCAS;
    @NotNull
    protected CAS systemView;
    @NotNull
    protected Feature normFeature;
    @NotNull
    protected TypeSystem ts;
    @NotNull
    protected Tokenizer tokenizer;

    private LVG(Builder builder) throws ResourceInitializationException
    {
        ae = builder.getAE();
        aCAS = ae.newCAS();
        ts = aCAS.getTypeSystem();
        normFeature = ts.getType(Token.class.getCanonicalName()).getFeatureByBaseName("norm");
        systemView = CASUtil.getSystemView(aCAS);
        tokenizer = new Tokenizer.Builder().build();
    }

    public static void main(String[] args) throws Exception
    {
        String test = "Tokenizing text requires that acronyms/abbreviations are kept together; however, " +
                "other punctuation should be segmented, such as the period at the end of this sentence. " +
                "This is a punctuation-rich test about maybe a Dr. with a Ph.D. weighing 180lbs that took cipro 100mg b.i.d. " +
                "Something difficult to test (evaluate) is parentheticals and odd|separations.";
        String[] expectedResults = {"Tokenizing", "text", "requires", "that", "acronyms/abbreviations",
                "are", "kept", "together", ";", "however", ",", "other", "punctuation", "should", "be", "segmented",
                ",", "such", "as", "the", "period", "at", "the", "end", "of", "this", "sentence", ".", "This", "is",
                "a", "punctuation-rich", "test", "about", "maybe", "a", "Dr.", "with", "a", "Ph.D.", "weighing",
                "180lbs", "that", "took", "cipro", "100mg", "b.i.d", ".", "Something", "difficult", "to", "test",
                "(", "evaluate", ")", "is", "parentheticals", "and", "odd", "|", "separations", "."};

        LVG normalizer = new LVG.Builder().build();
        CAS cas = normalizer.process(test);
        CAS sys = CASUtil.getSystemView(cas);
        for (AnnotationFS token : AnnotationUtils.getAnnotations(sys, Token.class))
        {
            String word = token.getCoveredText();
            String norm = token.getStringValue(normalizer.normFeature);
            System.out.println(String.format("%s : %s", word, norm));
        }
    }

    public CAS newCAS() throws ResourceInitializationException { return ae.newCAS(); }

    public void process(CAS aCAS) throws AnalysisEngineProcessException { ae.process(aCAS); }

    public CAS process(String text) throws AnalysisEngineProcessException
    {
        aCAS.reset();
        CAS systemView = CASUtil.getSystemView(aCAS);
        systemView.setDocumentText(text);
        tokenizer.process(aCAS);
        this.process(aCAS);
        return aCAS;
    }

    public AnalysisEngine getAE() { return ae; }



    public static class Builder
    {
        private String descriptor = "edu/umn/biomedicus/lvg/lvgAE.xml";
        private List<ServiceProviderDescription> serviceProviders  = new ArrayList<>();
        private Properties properties;
        private String serviceProviderClassName;
        private String modelResource;

        public Builder() {}

        public Builder(String descriptor) throws IOException
        {
            this.descriptor = descriptor;
        }

        public Builder serviceProvider(ServiceProviderDescription serviceDesc)
        {
            serviceProviders.add(serviceDesc);
            return this;
        }

        public Builder service(Class<? extends TokenizingServiceProvider> serviceProvider)
        {
            serviceProviderClassName = serviceProvider.getCanonicalName();
            return this;
        }

        public Builder model(String modelResource)
        {
            this.modelResource = modelResource;
            return this;
        }

        public LVG build()  throws ResourceInitializationException
        {
            ServiceProviderDescription tokenizingService = null;
            if (serviceProviderClassName != null && modelResource != null)
            {
                tokenizingService = new ServiceProviderDescription(
                        "cacheService").serviceProvider(serviceProviderClassName)
                        .modelFile(modelResource);
                this.serviceProvider(tokenizingService);
            }
            else if (serviceProviderClassName != null)
            {
                tokenizingService = new ServiceProviderDescription("cacheService")
                        .serviceProvider(serviceProviderClassName);
                serviceProvider(tokenizingService);
            }
            return new LVG(this);
        }

        public AnalysisEngine getAE() throws ResourceInitializationException
        {
            URL resource = this.getClass().getClassLoader().getResource(descriptor);
            return ResourceFactory.createAnalysisEngine(resource, serviceProviders);
        }
    }
}


