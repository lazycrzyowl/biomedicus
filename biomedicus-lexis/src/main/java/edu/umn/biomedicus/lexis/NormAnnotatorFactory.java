package edu.umn.biomedicus.lexis;

import org.apache.uima.UIMAFramework;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;

public class NormAnnotatorFactory {
    private static final Logger logger = UIMAFramework.getLogger(NormAnnotatorFactory.class);

	public static final String LVG_CONFIG_FILE_PROPERTY = "lvg.properties";

	// public static AnalysisEngineDescription createNormAnnotator() throws
	// ResourceInitializationException {
	// return createNormAnnotator((String) null);
	// }

	/**
	 * Create a description for the NormAnnotator using the default cache
	 * 
	 * @return AnalysisEngineDescription
	 * @throws ResourceInitializationException
	 */

/*
	public static AnalysisEngineDescription createNormAnnotator() throws ResourceInitializationException {

		ExternalResourceDescription cache = ExternalResourceFactory.createExternalResourceDescription(
		                                RedisCacheResource_Impl.class, RedisCacheResource_Impl.PARAM_REDIS_HOST,
		                                "localhost");

  	    return createNormAnnotator(cache);
	}

*/

	/**
	 * Create a description for the NormAnnotator that uses a cache supplied as
	 * a parameter
	 * 
	 * @param ExternalResourceDescription
	 *            cache
	 * @return AnalysisEngineDescription
	 * @throws ResourceInitializationException
	 */

/*
	public static AnalysisEngineDescription createNormAnnotator(ExternalResourceDescription cache)
	                                throws ResourceInitializationException {
		String home = System.getenv("BIOMEDICUS_HOME");
		if (home == null) {
			System.err.println("BIOMEDICUS_HOME is not set. Set BIOMEDICUS_HOME and try again.");
			throw new ResourceInitializationException();
		}
		String lvgConfig = ComponentFactory.getResourcePath("config/lvg.properties");
		AnalysisEngineDescription norm = AnalysisEngineFactory.createPrimitiveDescription(LvgTokenNormalizer.class,
		                                ComponentFactory.getTypeSystem(), LvgTokenNormalizer.PARAM_LVG_CONFIG_FILE,
		                                lvgConfig.toString(), LvgTokenNormalizer.PARAM_CACHE, cache);

		return norm;
	}

	public static AnalysisEngineDescription createNormAnnotator(String lvgConfigFileName,
	                                ExternalResourceDescription cache) throws ResourceInitializationException {
		return AnalysisEngineFactory.createPrimitiveDescription(LvgTokenNormalizer.class, ComponentFactory.getTypeSystem(),
		                                LvgTokenNormalizer.PARAM_LVG_CONFIG_FILE, lvgConfigFileName,
		                                LvgTokenNormalizer.PARAM_CACHE, cache);
	}



	*/
}
