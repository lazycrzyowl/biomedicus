package edu.umn.biomedicus.lexis;

import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class String2CuiMapResource implements SharedResourceObject {

    private Map<String, String> mMap = new HashMap<>();

    /**
     *
     */
    public void load(DataResource aData) throws ResourceInitializationException
    {
        try (InputStream inStr = aData.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inStr)))
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                // the first comma on each line separates key from value.
                // Keys cannot contain whitespace.
                int position = line.indexOf(',');
                String key = line.substring(0, position);
                String val = line.substring(position + 1);
                mMap.put(key, val.trim());
            }
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    /**
     *
     */
    public String get(String aKey)
    {
        return mMap.get(aKey);
    }
}

