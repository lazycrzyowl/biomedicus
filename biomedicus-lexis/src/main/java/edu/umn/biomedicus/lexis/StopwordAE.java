/* 
 Copyright 2011-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.lexis;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

/**
 * This class assigns a boolean value to tokens that are in a specified stopword file.
 *
 * @author Robert Bill
 */
public class StopwordAE extends CasAnnotator_ImplBase {

    public static final String PARAM_STOPWORD_LIST = "stopwordsFilename";
    @NotNull
    Type tokenType;
    @NotNull
    Feature tokenStopwordFeature;
    HashMap<String, Integer> stopwords = new HashMap<String, Integer>();
    String symbols = "\"`~!@#$%^&*()_-+=}]{[:;?/>.<,|'\\";
    private String stopwordsFilename;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        super.initialize(context);
        URL inst = getClass().getClassLoader().getResource("edu/umn/biomedicus/lexis/PubMedStopwords.txt");
        String[] wordList = null;
        try
        {
            wordList = FileUtil.loadListOfStrings(inst);
        } catch (IOException e) {
            throw new ResourceInitializationException(e);
        }

        for (String word : wordList)
        {
            if (word.equals("") || word == null)
            {
                continue;
            }
            stopwords.put(word.toLowerCase().trim(), 1);
        }
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);
        for (AnnotationFS token : AnnotationUtils.getAnnotations(sysCAS, tokenType))
        {
            String tokenText = token.getCoveredText().toLowerCase().trim();
            if (stopwords.containsKey(tokenText))
            {
                token.setBooleanValue(tokenStopwordFeature, true);
            } else if (tokenText.length() == 1 && symbols.contains(tokenText))
            {
                token.setBooleanValue(tokenStopwordFeature, true);
            }
        }
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem)
    {
        tokenType = typeSystem.getType("edu.umn.biomedicus.type.Token");
        tokenStopwordFeature = tokenType.getFeatureByBaseName("isStopword");
    }
}
