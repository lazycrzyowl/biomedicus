#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.${artifactId};

import org.apache.commons.configuration.ConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.util.FileUtils;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;
import edu.umn.biomedicus.${artifactId}.${artifactId}Configuration;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Properties;

/**
 * Family History Annotation Application.
 */
public class ${artifactId}SerialApplication {

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(${artifactId}SerialApplication.class);
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static Path projectDirectory;
    @NotNull
    private static String suffix;

    public ${artifactId}SerialApplication(Path configDirectory,
                                          Path outputDirectory,
                                          Iterator<Path> files)
            throws IOException, UIMAException
    {

        // Get pipeline
        ${artifactId}SerialPipeline uimaPipeline = new ${artifactId}SerialPipeline(configDirectory);
        double start = System.currentTimeMillis();
        int documentCount = 0;

        while (files.hasNext())
        {
            documentCount++;
            Path currentFile = files.next();
            String documentText = FileUtils.file2String(currentFile.toFile());

            // skip empty documents
            if (documentText == null || documentText.trim() == "")
            {
                continue;
            }

             // run the sample document through the pipeline
            long before = System.currentTimeMillis();
            CAS output = uimaPipeline.process(currentFile.toString(), documentText);

            long after = System.currentTimeMillis();
            CAS metadata = output.getView("MetaData");
            String outputName = new File(metadata.getDocumentText()).getName();
            File dest = new File(outputDirectory.toString(), outputName);
            try
            {
                FileOutputStream os = new FileOutputStream(dest);
                XmiCasSerializer.serialize(output, os);
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            } catch (SAXException e)
            {
                e.printStackTrace();
            }
        }

        double end = System.currentTimeMillis();
        double totalTime = (end - start)/1000;
        double timePerDocument = totalTime/Double.valueOf(documentCount);
        System.out.println("Total number of documents = " + documentCount);
        System.out.println("Total processing time = " + totalTime);
        System.out.println("Processing time per document = " + timePerDocument);

    }

    public static void main(String[] args) throws IOException, UIMAException, ConfigurationException
    {
        // Get general properties
        ${artifactId}Configuration _config = new ${artifactId}Configuration();
        System.out.println(_config.getString("biomedicus_cli_banner"));

        // Start logging
        logger.setLevel(Level.OFF);

        // Construction commandline options, get values, and validate what was supplied
        Options cliOptions = getCLIOptions();
        CommandLine cliValues = getCLIValues(cliOptions, args);
        String suffix = ".txt";

        if (cliValues.hasOption("projectdir"))
        {
            // set path for input and output

            inputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"), "input");
            outputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"), "output");
            projectDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("projectdir"));

            // get suffix, or set to default (.txt)
            String suffix_opt = cliValues.getOptionValue("suffix");
            if (suffix_opt != null && suffix_opt != "")
            {
                suffix = suffix_opt;
            }

            // display provided options
            System.out.println("Setting project directory to: " + projectDirectory.toString());
            System.out.println("Setting input directory to: " + inputDirectory.toString());
            System.out.println("Setting destination directory to: " + outputDirectory.toString());
            System.out.println("Setting input file suffix to: " + suffix);

        } else
        {
            printHelp("BioMedICUS Project", cliOptions);
        }

        // Create a DirectoryStream which accepts only filenames ending with specified suffix
        DirectoryStream<Path> ds = Files.newDirectoryStream(inputDirectory, "*" + suffix);

        // constructs a class to create and run a UIMA pipeline
        String od = _config.getString("output_dir");
        ${artifactId}SerialApplication app = new ${artifactId}SerialApplication(
                projectDirectory, outputDirectory, ds.iterator());

    }

    private static Options getCLIOptions()
    {
        // create the Options
        Options options = new Options();
                options.addOption("p", "projectdir", true, "Location of project directory.");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args)
    {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try
        {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e)
        {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }
}