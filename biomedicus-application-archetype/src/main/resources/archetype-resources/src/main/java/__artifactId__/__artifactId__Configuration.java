#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.${artifactId};


import org.apache.commons.configuration.*;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

public class ${artifactId}Configuration
{

    @NotNull
    private CompositeConfiguration config;

    public ${artifactId}Configuration() throws ConfigurationException
    {
        config = new CompositeConfiguration();
        config.addConfiguration(new SystemConfiguration());
        config.addConfiguration(new EnvironmentConfiguration());

        // Get BIOMEDICUS_HOME and the Configuration directory first
        final String home = System.getenv("BIOMEDICUS_HOME");

    }

    public void setSystemProps(String propKeysString)
    {
        String[] propKeys = propKeysString.split("${symbol_escape}${symbol_escape}s|${symbol_escape}${symbol_escape}s");

        for (String key : propKeys)
        {
            if (key.equals("|")) continue;
            String value = config.getString(key);
            System.setProperty(key, value);
        }
    }

    public void setProperty(String key, String value)
    {
        config.setProperty(key, value);
    }

    public String getString(String key) {
        return config.getString(key);
    }
}