#cd biomedicus-family-history
#mvn compile
#mvn assembly:single
#cd ..
mvn dependency:build-classpath -Dmdep.outputFile=cp.txt

export CLASSPATH=`cat cp.txt`
export CLASSPATH=$CLASSPATH:`cat biomedicus-family-history/cp.txt`
export CLASSPATH=$CLASSPATH:biomedicus-family-history/target/classes
export CLASSPATH=$CLASSPATH:biomedicus-family-history/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-document/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-core/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-sections/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-tokenizing/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-lexis/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-segmenting/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-chunking/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-relations/target/classes/
export CLASSPATH=$CLASSPATH:biomedicus-mapping/target/classes/

#mvn exec:exec -Dexec.executable="edu.umn.biomedicus.familyHistory.FamilyHistorySerialApplication" -Dexec.workingdir="/Users/bill0154/Develop/bitbucket/biomedicus" -Dexec.args="-i /Users/bill0154/mts -d /Users/bill0154/temp/junk"


java edu.umn.biomedicus.applications.FamilyHistorySerialApplication -i $1 -d $2
