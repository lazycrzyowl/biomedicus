package edu.umn.biomedicus.parsing;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.type.LexicalParseConstituent;
import edu.umn.biomedicus.type.Phrase;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import opennlp.tools.tokenize.TokenizerModel;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;
import org.uimafit.descriptor.ConfigurationParameter;

import java.io.CharArrayReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class StanfordParserAE extends CasAnnotator_ImplBase {
    // Parameter: path and name of parser model file.
    public static final String PARAM_GRAMMAR_FILE = "grammarFile";
    static String home = System.getenv("BIOMEDICUS_HOME");
    static final String defaultGrammarFile = Paths.get(home, "models/englishPCFG.ser.gz").toString();

    @NotNull
    private Type tokenType;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type lexicalParseConstituentType;
    @NotNull
    private Feature posFeature;
    @NotNull
    private Feature parseFeature;
    @NotNull
    private Feature labelFeature;

    private HashSet<String> phraseLabels;
    private LexicalizedParser parser;
    private TreebankLanguagePack tlp;
    private GrammaticalStructureFactory grammarFactory;
    private String langPack = "edu.stanford.nlp.trees.PennTreebankLanguagePack";

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);

        // Load the parser grammar file
        String grammarParam = (String) context.getConfigParameterValue(PARAM_GRAMMAR_FILE);
        String grammar = (grammarParam==null) ? defaultGrammarFile : grammarParam;
//        URL resource = this.getClass().getClassLoader().getResource(grammar);
//        File grammarFile = loadGrammar(resource);
        parser = LexicalizedParser.loadModel(grammar); //File.toString());
        parser.setOptionFlags("-maxLength", "80", "-retainTmpSubcategories");

        // Set phrase information
        phraseLabels = new HashSet<String>();
        phraseLabels.addAll(Arrays.asList(new String[]{"ADJP", "ADVP", "CONJP", "NP", "PP",
                "QP", "VP", "WHADJP", "WHAVP", "WHNP", "WHPP", "S"}));

        //TreebankLanguagePack tlp = new PennTreebankLanguagePack();

    }

    @Override
    public void process(CAS cas) throws AnalysisEngineProcessException {
        CAS systemView = CASUtil.getSystemView(cas);

        // LexicalizedParser parser = LexicalizedParser.loadModel(modelPath);
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();

        for (AnnotationFS sent : AnnotationUtils.getAnnotations(systemView, Sentence.class)) {
            ArrayList<HasWord> taggedTokenList = new ArrayList<>();
            for (AnnotationFS tok : AnnotationUtils.getCoveredAnnotations(sent, Token.class)) {
                TaggedWord word = new TaggedWord(tok.getCoveredText(), tok.getStringValue(posFeature));
                word.setBeginPosition(tok.getBegin());
                word.setEndPosition(tok.getEnd());
                taggedTokenList.add(word);
            }
            CharArrayReader reader = new CharArrayReader(sent.getCoveredText().toCharArray());

            Tree tree = parser.parseTree(taggedTokenList); // apply(wordlist);

            if (tree == null)
            {
                sent.setStringValue(parseFeature, "unknown");
                return;
            }
            else {
                sent.setStringValue(parseFeature, tree.toString());

                for (Object child : tree.toArray()) {
                    Tree c = (Tree) child;
                    if (c.isPhrasal()) { // && phraseLabels.contains(c.value())) {
                        ArrayList<TaggedWord> words = c.taggedYield();
                        int begin = words.get(0).beginPosition();
                        int end = words.get(words.size() - 1).endPosition();
                        AnnotationFS p = systemView.createAnnotation(lexicalParseConstituentType, begin, end);
                        p.setStringValue(labelFeature, c.value());
                        systemView.addFsToIndexes(p);
                    }
                }
            }
        }
    }

    public void typeSystemInit(TypeSystem ts) {
        tokenType = ts.getType(Token.class.getCanonicalName());
        sentenceType = ts.getType(Sentence.class.getCanonicalName());
        lexicalParseConstituentType = ts.getType(LexicalParseConstituent.class.getCanonicalName());
        posFeature = tokenType.getFeatureByBaseName("pos");
        parseFeature = sentenceType.getFeatureByBaseName("parse");
        labelFeature = lexicalParseConstituentType.getFeatureByBaseName("label");
    }

    private File loadGrammar(URL resource) throws ResourceInitializationException
    {
        try
        {
            File file = PathUtils.resourceToTempFile(resource);
            if (file == null)
                throw new IOException(String.format("Unable to access grammar file: %s", resource));
            return file;
        }
        catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
    }
}
