package edu.umn.biomedicus.${artifactId};

import edu.umn.biomedicus.core.featureranges.SentenceFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * Skeleton for a annotation engine module
 */
public class ${artifactId}AE extends CasAnnotator_ImplBase
{

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(${artifactId}AE.class);

    @NotNull
    Type tokenType;
    @NotNull
    Type chunkType;
    @NotNull
    Type sentenceType;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        super.initialize(context);
        // Load any external resources or any object with a run-once initialation before processing

    }

/**
 * The process method is called once for each document that goes through the pipeline.
 * Your annotation should be inside the process method.
 *
 * Note how the AnnotationUtils class is used to interate over
 */
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, Sentence.class)) {

            for (AnnotationFS token : AnnotationUtils.getCoveredAnnotations(sentenceAnnotation, Token.class))
            {
                // Do something with tokens...

                // Add some new annotation here...
            }

        }
    }

    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

        // Initialize types
        super.typeSystemInit(typeSystem);
        tokenType = typeSystem.getType(TypeUtil.TOKEN_TYPE_NAME);
        chunkType = typeSystem.getType(TypeUtil.CHUNK_TYPE_NAME);
        sentenceType = typeSystem.getType(TypeUtil.SENTENCE_TYPE_NAME);
        // etc.
        logger.log(Level.FINEST, "${artifactId} type system initialized.");

    }

}

