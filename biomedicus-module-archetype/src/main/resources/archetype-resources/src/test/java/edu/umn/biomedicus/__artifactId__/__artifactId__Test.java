package edu.umn.biomedicus.${artifactId};

import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;


/**
 * Simple test class for your new annotation engine module.
 */
public class ${artifactId}Test
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ${artifactId}Test(String testName) throws Exception
    {
        super(testName);
        Tokenizer tokenizer = new Tokenizer.Builder().build();
        SentenceSegmenter segmenter = new SentenceSegmenter.Builder().build();
        ${artifactId} ${artifactId}_impl = new ${artifactId}.Builder().build();


        AnalysisEngine ${artifactId} = ${artifactId}Builder.get${artifactId}();
        CAS aCAS = ${artifactId}.newCAS();
        CAS view = aCAS.createView("System");
        view.setDocumentText("This is a test of the ${artifactId} to see if it really works");
        tokenizer.process(aCAS);
        segmenter.process(aCAS);
        ${artifactId}.process(aCAS);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( ${artifactId}Test.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
