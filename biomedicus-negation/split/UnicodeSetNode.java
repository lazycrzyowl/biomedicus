/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token.split;

import com.ibm.icu.text.UnicodeSet;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class UnicodeSetNode extends Node implements Comparable<UnicodeSetNode> {

	private UnicodeSet unicodeSet;

	public UnicodeSetNode(UnicodeSet unicodeSet) {
		this.unicodeSet = unicodeSet;
	}

	public boolean contains(int character) {
		return unicodeSet.contains(character);
	}

	public UnicodeSet getUnicodeSet() {
		return unicodeSet;
	}

	@Override
	public int compareTo(UnicodeSetNode otherNode) {
		UnicodeSet othersUnicodeSet = otherNode.unicodeSet;
		return unicodeSet.compareTo(othersUnicodeSet);
	}
}
