<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (c) 2014 University of Minnesota
  ~
  ~  All rights reserved.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~  http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>biomedicus</artifactId>
        <groupId>edu.umn.biomedicus</groupId>
        <version>0.0.1</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>

    <groupId>edu.umn</groupId>
    <artifactId>biomedicus-application</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>biomedicus-application</name>
    <description>Biomedical Information Collection and Understanding System</description>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>



    <dependencies>

        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>13.0</version>
        </dependency>
        <dependency>
            <groupId>commons-configuration</groupId>
            <artifactId>commons-configuration</artifactId>
            <version>1.6</version>
        </dependency>
        <dependency>
            <groupId>commons-cli</groupId>
            <artifactId>commons-cli</artifactId>
            <version>1.2</version>
        </dependency>
        <dependency>
            <groupId>edu.umn.biomedicus</groupId>
            <artifactId>biomedicus-core</artifactId>
            <version>0.0.1</version>
        </dependency>
        <dependency>
            <groupId>edu.umn.biomedicus</groupId>
            <artifactId>biomedicus-document</artifactId>
            <version>0.0.1</version>
        </dependency>
        <dependency>
            <groupId>edu.umn.biomedicus</groupId>
            <artifactId>biomedicus-tokenizing</artifactId>
            <version>0.0.1</version>
        </dependency>
        <dependency>
            <groupId>edu.umn.biomedicus</groupId>
            <artifactId>biomedicus-segmenting</artifactId>
            <version>0.0.1</version>
        </dependency>
        <dependency>
            <groupId>edu.umn.biomedicus</groupId>
            <artifactId>biomedicus-tagging</artifactId>
            <version>0.0.1</version>
        </dependency>
        <dependency>
            <groupId>edu.umn.biomedicus</groupId>
            <artifactId>biomedicus-chunking</artifactId>
            <version>0.0.1</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.4</version>
        </dependency>
    </dependencies>
    <build>
        <directory>target</directory>
        <outputDirectory>target/classes</outputDirectory>
        <finalName>biomedicus-project-0.0.1.jar</finalName>
        <testOutputDirectory>target/test-classes</testOutputDirectory>
        <sourceDirectory>src/main/java</sourceDirectory>
        <!-- scriptSourceDirectory>src/main/scripts</scriptSourceDirectory -->
        <testSourceDirectory>src/test/java</testSourceDirectory>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
            </testResource>
        </testResources>
        <plugins>
             <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <id>add-uima-sources</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>add-source</goal>
                        </goals>
                        <configuration>
                            <sources>
                                <source>${project.build.directory}/src/main/java</source>
                            </sources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <mainClass>edu.umn.biomedicus.application.BiomedicusClient</mainClass>
                        </manifest>
                    </archive>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <id>make-assembly</id>
                        <phase>install</phase>
                        <goals><goal>single</goal></goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>2.8</version>
                <executions>
                    <execution>
                        <id>build-classpath</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>build-classpath</goal>
                        </goals>
                        <configuration>
                            <outputFile>cp.txt</outputFile>
                            <!--<mdep.outputFile>cp.txt</mdep.outputFile>-->

                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    <organization>
        <name>University of Minnesota Institute for Health Informatics NLP Group</name>
        <url>http://www.bmhi.umn.edu/ihi/research/nlpie/index.htm</url>
    </organization>


</project>
