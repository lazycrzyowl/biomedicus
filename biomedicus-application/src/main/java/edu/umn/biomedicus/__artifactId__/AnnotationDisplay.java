/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package edu.umn.biomedicus.__artifactId__;

import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

public class AnnotationDisplay extends CasAnnotator_ImplBase {

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(AnnotationDisplay.class);

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        logger.log(Level.FINEST, "Start of AnnotationDisplay.");
        CAS sysCAS = aCAS.getView("System");
        for (AnnotationFS annot : sysCAS.getAnnotationIndex())
        {
            String format = String.format("%s : %s", annot.getType(), annot.getCoveredText());
            System.out.println(format);
        }
    }
}
