/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.__artifactId__;


import org.apache.commons.configuration.*;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

public class Configuration
{

    @NotNull
    private CompositeConfiguration config;

    public static void main(String[] args) throws Exception
    {
        Configuration c = new Configuration();
    }

    public Configuration() throws ConfigurationException
    {
        config = new CompositeConfiguration();
        config.addConfiguration(new SystemConfiguration());
        config.addConfiguration(new EnvironmentConfiguration());

        // Get BIOMEDICUS_HOME and the Configuration directory first
        final String home = System.getenv("BIOMEDICUS_HOME");

        // Get biomedicus.property file data and set in System.properties
        URL bioPropURL = Configuration.class.getResource("/edu/umn/biomedicus/biomedicus.properties");
        PropertiesConfiguration bp = new PropertiesConfiguration(bioPropURL);
        config.addConfiguration(bp);

        // Some properties in the biomedicus.property file need to be in system properties. Add these here
        String systemProps = config.getString("pipeline.properties");
        setSystemProps(systemProps);

        try
        {
            //Process p = Runtime.getRuntime().exec("cp /Users/bill0154/temp/backup/97_*.txt /Users/bill0154/temp/fh-output/");
        } catch (Exception e)
        {
            System.out.println(e);
        }

    }

    public void setSystemProps(String propKeysString)
    {
        String[] propKeys = propKeysString.split("\\s|\\s");

        for (String key : propKeys)
        {
            if (key.equals("|")) continue;
            String value = config.getString(key);
            System.setProperty(key, value);
        }
    }

    public void setProperty(String key, String value)
    {
        config.setProperty(key, value);
    }

    public String getString(String key) {
        return config.getString(key);
    }
}