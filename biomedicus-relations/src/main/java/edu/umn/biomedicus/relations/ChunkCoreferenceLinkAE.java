/* 
Copyright 2010-14 University of Minnesota
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.relations;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.type.Sentence;
import opennlp.tools.coref.Linker;
import opennlp.tools.parser.Parser;
import opennlp.tools.tokenize.Tokenizer;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 *
 *  The ChunkCoreferenceLinkAE analyzes chunks to find those that use coreferences.
 *  The resolution of coreference is less complicated because family history statements
 *  rarely refer to more than the family member and the observation. Resolution is done
 *  by tracking most recent family members with gender and most recent observations.
 *
 */

public class ChunkCoreferenceLinkAE extends CasAnnotator_ImplBase {

  private static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";
  private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
  private static final String SECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Section";
  private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
  private static final String SUBSECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Subsection";
  private static final String FAMILY_HISTORY_ANNOTATION_NAME = "edu.umn.biomedicus.type.FamilyHistory";
  private static final String SECTION_HEADING_FEATURE_NAME = "heading";
  private static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
  private static final String SECTION_LEVEL_FEATURE_NAME = "level";
  private static final String SECTION_HAS_SUBSECTIONS_FEATURE_NAME = "hasSubsections";
  private static final String CHUNK_LABEL_FEATURE_NAME = "label";

  private static final String DEFAULT_MODEL_FILE = "/edu/umn/biomedicus/annotators/text/en-token.bin";

  private static final int SECTION_LEVEL = 0;
  private static final int SUBSECTION_LEVEL = 1;

  @NotNull
  private Type tokenType;

  @NotNull
  private Type chunkType;

  @NotNull
  private Type sentenceType;

  @NotNull
  private Type sectionType;

  @NotNull
  private Type familyHistoryType;

  @NotNull
  private Type subsectionType;

  @NotNull
  private Feature sectionContentStartFeature;

  @NotNull
  private Feature sectionHeadingFeature;

  @NotNull
  private Feature sectionHasSubsectionsFeature;

  @NotNull
  private Feature sectionLevelFeature;

  @NotNull
  private Feature chunkLabelFeature;

  @NotNull
  private Feature chunkFHRoleFeature;

  @NotNull
  private PropositionMapper propositionMapper;

  @NotNull
  private Tokenizer tokenizer;

  @NotNull
  private String corefModelDirectory;

  @NotNull
  private String parserModelFile;

  @NotNull
  private String[] sofaNames;

  @NotNull
  private static final Logger logger = UIMAFramework.getLogger(ChunkCoreferenceLinkAE.class);

  @NotNull
  private Parser _parser = null;

  @NotNull
  private Linker corefLinker;


  @Override
  public void initialize(UimaContext context) throws ResourceInitializationException {

    super.initialize(context);

    String applicationBase = System.getenv("BIOMEDICUS_HOME");

  }

  @Override
  public void process(CAS aCAS) throws AnalysisEngineProcessException {

    AnnotationIndex<Sentence> sentences = (AnnotationIndex<Sentence>) AnnotationUtils.getAnnotations(aCAS, sentenceType);
    int currentSentence = 0;

    for (AnnotationFS sentence : sentences) {
      List<AnnotationFS> sentenceChunks = AnnotationUtils.getCoveredAnnotations(aCAS, sentence, chunkType);

      for (AnnotationFS chunk : sentenceChunks) {
        resolveCoreferences(chunk);
      }
    }
  }

  private void resolveCoreferences(AnnotationFS chunk) {
    String chunkText = chunk.getCoveredText().toLowerCase();
    boolean containsFamilyWord = containsRelative(chunkText);
    if (containsRelative(chunkText) || containsPersonalPronoun(chunkText)) {
      chunk.setStringValue(chunkFHRoleFeature, "family");
    } else if (chunk.getStringValue(chunkLabelFeature).equals("NP")) {
    }
  }

  public boolean containsRelative(String chunkText) {
    String[] familyMembers = {"father", "mother", "daughter","sister","brother","sibling","cousin","aunt","uncle"};
    for (String f : familyMembers) {
      int i = chunkText.indexOf(f);
      if (i != -1) return true;
    }
    return false;
  }

  public boolean containsPersonalPronoun(String chunkText) {
    String[] ppns = {"he","she","they","who"};
    String[] chunkTokens = chunkText.split("\\s");
    for (String p : ppns) {
      for (String t : chunkTokens) {
        if (t.equals(p)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
   *
   * @param typeSystem
   * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
   */
  @Override
  public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

    // Initialize types
    super.typeSystemInit(typeSystem);
    tokenType = typeSystem.getType(TOKEN_ANNOTATION_NAME);
    chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
    sectionType = typeSystem.getType(SECTION_ANNOTATION_NAME);
    subsectionType = typeSystem.getType(SUBSECTION_ANNOTATION_NAME);
    sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
    familyHistoryType = typeSystem.getType(FAMILY_HISTORY_ANNOTATION_NAME);


    sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
    sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
    sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
    sectionHasSubsectionsFeature = sectionType.getFeatureByBaseName(SECTION_HAS_SUBSECTIONS_FEATURE_NAME);

    chunkLabelFeature = chunkType.getFeatureByBaseName(CHUNK_LABEL_FEATURE_NAME);
    chunkFHRoleFeature = chunkType.getFeatureByBaseName("fhRole");

  }
}


