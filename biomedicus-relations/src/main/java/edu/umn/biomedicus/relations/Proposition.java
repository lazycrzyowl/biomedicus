package edu.umn.biomedicus.relations;

import org.apache.uima.cas.text.AnnotationFS;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Proposition {
    List<AnnotationFS> predication = new ArrayList<>();
    AnnotationFS familyArgument = null;
    String familyCode = null;
    AnnotationFS observationArgument = null;
    String observationCode = null;
    AnnotationFS predicationIndicator = null;

    AnnotationFS negationArgument = null;
    Boolean negationStatus = false;

    String vitalStatusIndicator;

    public void setFamilyCode(String code) {
        this.familyCode = code;
    }

    public void setNegationArgument(AnnotationFS argument) {
        negationArgument = argument;
    }

    public AnnotationFS getFamilyArgument() {
        return this.familyArgument;
    }

    public void setFamilyArgument(AnnotationFS argument) {
        familyArgument = argument;
    }

    public AnnotationFS getObservationArgument() {
        return this.observationArgument;
    }

    public void setObservationArgument(AnnotationFS argument) {
        observationArgument = argument;
    }

    public AnnotationFS getPredicationIndicator() {
        return this.predicationIndicator;
    }

    public void setPredicationIndicator(AnnotationFS predicate) {
        predicationIndicator = predicate;
    }

    public Boolean getNegationStatus() {
        return this.negationStatus;
    }

    public void setNegationStatus(Boolean negation) {
        this.negationStatus = negation;
    }

    public AnnotationFS getNegationArgument() {
        return negationArgument;
    }

    public String getVitalStatus() {
        return this.vitalStatusIndicator;
    }

    public void setVitalStatus(String vitalStatus) {
        this.vitalStatusIndicator = vitalStatus;
    }

    public String getPredications() {
        StringBuilder predicationText = new StringBuilder();
        for (AnnotationFS annotation : predication) {
            predicationText.append("{");
            predicationText.append(annotation.getCoveredText());
            predicationText.append("}");
        }

        return predicationText.toString();
    }

}
