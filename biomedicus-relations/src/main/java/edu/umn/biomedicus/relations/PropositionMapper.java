package edu.umn.biomedicus.relations;

import edu.umn.biomedicus.core.utils.LexiconUtil;
import edu.umn.biomedicus.core.utils.Span;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;


public class PropositionMapper {

    @NotNull
    List<AnnotationFS> chunkList;
    @NotNull
    Type getFamilyHistoryConstituentType;
    @NotNull
    List<AnnotationFS> tokenList;
    @NotNull
    AnnotationFS sentenceAnnotation;
    @NotNull
    String sentenceText;
    @NotNull
    CAS aCAS;
    @NotNull
    Type familyHistoryConstituentType;
    @NotNull
    Feature negatedFeature;
    @NotNull
    Type familyHistoryType;
    @NotNull
    Type observationConstituentType;
    @NotNull
    Type predicationIndicationType;
    List<Proposition> propositionList = new ArrayList<>();
    @NotNull
    private Feature chunkLabelFeature;
    @NotNull
    private Feature fhIdentifierFeature;

    public PropositionMapper(CAS aCAS, Type familyHistoryConstituentType,
                             AnnotationFS sentenceAnnotation, List<AnnotationFS> tokenList,
                             List<AnnotationFS> chunkList, Feature chunkLabelFeature,
                             Feature fhIdentifierFeature, Feature negatedFeature)
    {
        this.aCAS = aCAS;
        this.sentenceAnnotation = sentenceAnnotation;
        this.sentenceText = sentenceAnnotation.getCoveredText();
        this.chunkList = chunkList;
        this.tokenList = tokenList;
        this.chunkLabelFeature = chunkLabelFeature;
        this.fhIdentifierFeature = fhIdentifierFeature;
        this.familyHistoryConstituentType = familyHistoryConstituentType;
        this.negatedFeature = negatedFeature;
        this.familyHistoryType = familyHistoryConstituentType;
        TypeSystem typeSystem = aCAS.getTypeSystem();

    }

    private HashMap<String, AnnotationFS> splitAnnotation(AnnotationFS annotation,
                                                          Span conjunction,
                                                          AnnotationFS tail,
                                                          Type constituentType)
    {
        // step 1- split on conjunction
        String chunkText = "";
        boolean negated = false;
        HashMap<String, AnnotationFS> resultMap = new HashMap();
        List<Span> expansion = new ArrayList<>();
        if (conjunction != null)
        {
            if (tail == null)
            {
                chunkText = annotation.getCoveredText();
            } else
            {
                Feature negatedFeature = tail.getType().getFeatureByBaseName("negated");
                negated = tail.getBooleanValue(negatedFeature);
                chunkText = tail.getCoveredText();
            }
            Span span1 = new Span(annotation.getBegin(), conjunction.getStart());
            Span span2 = new Span(conjunction.getEnd(), annotation.getEnd());
            AnnotationFS constituent = aCAS.createAnnotation(constituentType, span1.getStart(), span1.getEnd());
            negatedFeature = constituent.getType().getFeatureByBaseName("negated");
            constituent.setBooleanValue(negatedFeature, negated);
            tail = aCAS.createAnnotation(constituentType, span2.getStart(), span2.getEnd());
            int conjunctionStart = conjunction.getStart() - annotation.getBegin();
            int conjunctionEnd = conjunction.getEnd() - annotation.getBegin();
            if (chunkText != null)
            {
                String conjunctionText = chunkText.substring(conjunctionStart, conjunctionEnd);
                if (LexiconUtil.containsNegationWord(conjunctionText))
                {
                    tail.setBooleanValue(negatedFeature, true);
                }
            }
            resultMap.put("head", constituent);
            resultMap.put("tail", tail);
        }
        return resultMap;
    }

    private HashMap<String, AnnotationFS> splitFamily(AnnotationFS family, Span conjunction, AnnotationFS tail, Type constituentType)
    {
        // step 1- split on conjunction
        Feature negatedFeature = constituentType.getFeatureByBaseName("negated");
        String chunkText = "";
        HashMap<String, AnnotationFS> resultMap = new HashMap();
        List<Span> expansion = new ArrayList<>();
        boolean negated = false;
        if (conjunction != null)
        {
            if (tail == null)
            {
                chunkText = family.getCoveredText();
            } else
            {
                chunkText = tail.getCoveredText();
            }
            Span span1 = new Span(family.getBegin(), conjunction.getStart());
            Span span2 = new Span(conjunction.getEnd(), family.getEnd());
            AnnotationFS familyConstituent = aCAS.createAnnotation(familyHistoryConstituentType, span1.getStart(), span1.getEnd());
            tail = aCAS.createAnnotation(constituentType, span2.getStart(), span2.getEnd());
            int conjunctionStart = conjunction.getStart() - family.getBegin();
            int conjunctionEnd = conjunction.getEnd() - family.getBegin();
            String conjunctionText = chunkText.substring(conjunctionStart, conjunctionEnd);
            if (LexiconUtil.containsNegationWord(conjunctionText))
            {
                tail.setBooleanValue(negatedFeature, true);
            }
            resultMap.put("family", familyConstituent);
            resultMap.put("tail", tail);
        }
        return resultMap;
    }

    private List<Proposition> resolveCoordination(AnnotationFS family, AnnotationFS observation, AnnotationFS indicator)
    {
        ArrayList<AnnotationFS> familyHistoryPredication = new ArrayList<>();
        ArrayList<AnnotationFS> familyMembers = new ArrayList<>();
        ArrayList<AnnotationFS> observations = new ArrayList<>();
        AnnotationFS tail = null;
        List<Span> famCoordinations = LexiconUtil.findCoordination(family.getCoveredText());
        if (famCoordinations.size() == 0)
        {
            AnnotationFS familyConstituent = aCAS.createAnnotation(familyHistoryConstituentType, family.getBegin(), family.getEnd());
            familyMembers.add(familyConstituent);
        }
        for (Span conjunction : famCoordinations)
        {
            // convert to document index location
            Span docLocationConjunction = new Span(family.getBegin() + conjunction.getStart(), family.getBegin() + conjunction.getEnd());

            HashMap<String, AnnotationFS> famResultMap = splitAnnotation(family, docLocationConjunction, tail, familyHistoryConstituentType);
            AnnotationFS familyConstituent = famResultMap.get("head");
            familyMembers.add(familyConstituent);
            tail = famResultMap.get("tail");
        }

        List<Span> obsCoordinations = LexiconUtil.findCoordination(observation.getCoveredText());
        if (obsCoordinations.size() == 0)
        {
            AnnotationFS observationConstituent = aCAS.createAnnotation(observationConstituentType, observation.getBegin(),
                    observation.getEnd());
            observations.add(observationConstituent);
        }
        for (Span conjunction : obsCoordinations)
        {
            Span docLocationConjunction = new Span(observation.getBegin() + conjunction.getStart(), observation.getBegin() + conjunction.getEnd());

            HashMap<String, AnnotationFS> obsResultMap = splitAnnotation(observation, docLocationConjunction, tail, observationConstituentType);
            AnnotationFS observationConstituent = obsResultMap.get("head");
            observations.add(observationConstituent);
            tail = obsResultMap.get("tail");
        }

        for (AnnotationFS fam : familyMembers)
        {
            for (AnnotationFS obs : observations)
            {
                int begin = Math.min(fam.getBegin(), obs.getBegin());
                int end = Math.max(fam.getEnd(), obs.getEnd());
                AnnotationFS familyHistory = aCAS.createAnnotation(familyHistoryType, begin, end);
                aCAS.addFsToIndexes(familyHistory);
                Feature famFeature = familyHistoryType.getFeatureByBaseName("familyConstituent");
                Feature obsFeature = familyHistoryType.getFeatureByBaseName("observationConstituent");
                Feature predFeature = familyHistoryType.getFeatureByBaseName("indicator");
                String debugFam = fam.getCoveredText();
                String debugObs = obs.getCoveredText();
                familyHistory.setStringValue(famFeature, debugFam);
                familyHistory.setStringValue(obsFeature, debugObs);
                AnnotationFS pred = aCAS.createAnnotation(predicationIndicationType, indicator.getBegin(), indicator.getEnd());
                familyHistory.setStringValue(predFeature, pred.getCoveredText());
                //aCAS.addFsToIndexes(familyHistory);

            }
        }
        return new ArrayList<Proposition>();
    }

    private BitSet getFamilyChunks(List<AnnotationFS> constituentList)
    {
        int candidateLength = constituentList.size();
        BitSet familyChunks = new BitSet(candidateLength);
        for (int i = 0; i < candidateLength; i++)
        {
            AnnotationFS chunk = constituentList.get(i);
            Feature chunkLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
            String chunkLabel = chunk.getStringValue(chunkLabelFeature);
            if (chunkLabel.equals("family"))
            {
                familyChunks.set(i);
            }
        }
        return familyChunks;
    }

    private BitSet getObservationChunks(List<AnnotationFS> chunkList, BitSet familyChunks)
    {
        int candidateLength = chunkList.size();
        BitSet observationChunks = new BitSet(candidateLength);
        for (int i = 0; i < candidateLength; i++)
        {
            AnnotationFS chunk = chunkList.get(i);
            Feature chunkLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
            String label = chunk.getStringValue(chunkLabelFeature);
            if (label.equals("observation"))
            {
                if (!familyChunks.get(i))
                {
                    observationChunks.set(i);
                }
            }
        }
        return observationChunks;
    }

    private void printChunks()
    {
        Feature familyHistoryConstituentIdentifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
        Feature familyHistoryConstituentLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (AnnotationFS chunk : chunkList)
        {
            String label = chunk.getStringValue(familyHistoryConstituentLabelFeature);
            String text = chunk.getCoveredText();
            String identifier = chunk.getStringValue(familyHistoryConstituentIdentifierFeature);
            String msg = String.format("\t%d = %s{%s:%s}", i, label, text, identifier);
            System.out.println(msg);
            i++;
        }
        System.out.println();
    }

    private BitSet getIndicatorChunks(List<AnnotationFS> chunkList)
    {
        int candidateLength = chunkList.size();
        BitSet candidateChunks = new BitSet(candidateLength);

        for (int i = 0; i < candidateLength; i++)
        {
            AnnotationFS chunk = chunkList.get(i);
            Feature chunkLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
            String label = chunk.getStringValue(chunkLabelFeature);
            if (label.equals("indicator"))
            {
                candidateChunks.set(i);
            }
            String chunkText = chunk.getCoveredText();
            String debug = chunkText;

        }

        return candidateChunks;
    }

    private BitSet getDetailChunks(List<AnnotationFS> chunkList)
    {
        int candidateLength = chunkList.size();
        BitSet detailChunks = new BitSet(candidateLength);
        for (int i = 0; i < candidateLength; i++)
        {
            AnnotationFS chunk = chunkList.get(i);
            String chunkText = chunk.getCoveredText();
            if (LexiconUtil.containsDetailWord(chunkText))
            {
                String chunkLabel = chunk.getStringValue(chunkLabelFeature);
                if (chunkLabel == null)
                {

                } else
                {
                    if (chunkLabel.equals("VP") || chunkLabel.equals("PP"))

                    {
                        detailChunks.set(i);
                    }
                }
            }
        }
        return detailChunks;
    }

    private BitSet getVitalStatusChunks(List<AnnotationFS> chunkList)
    {
        int candidateLength = chunkList.size();
        BitSet detailChunks = new BitSet(candidateLength);
        for (int i = 0; i < candidateLength; i++)
        {
            AnnotationFS chunk = chunkList.get(i);
            String chunkText = chunk.getCoveredText();
            if (LexiconUtil.containsVitalWord(chunkText))
            {
                detailChunks.set(i);
            }
        }
        return detailChunks;
    }

    private BitSet getNegationChunks(List<AnnotationFS> chunkList, AnnotationFS sentenceAnnotation)
    {
        int sentenceStart = sentenceAnnotation.getBegin();
        int workingIndex = sentenceAnnotation.getBegin();
        int candidateLength = chunkList.size();
        BitSet negationChunks = new BitSet(candidateLength);
        for (int i = 0; i < candidateLength; i++)
        {
            AnnotationFS chunk = chunkList.get(i);
            int end = chunk.getEnd();
            if (end < workingIndex)
                workingIndex = chunk.getBegin();
            int realBegin = workingIndex - sentenceStart;
            int realEnd = end - sentenceStart;
            String chunkText = sentenceText.substring(realBegin, realEnd);
            if (LexiconUtil.containsNegationWord(chunkText))
            {
                negationChunks.set(i);
            }
            workingIndex = end;
        }
        return negationChunks;
    }

    private BitSet getLabelledChunks(List<AnnotationFS> chunkList, String label)
    {
        int candidateLength = chunkList.size();
        BitSet labelledChunks = new BitSet(candidateLength);
        for (int i = 0; i < candidateLength; i++)
        {
            AnnotationFS chunk = chunkList.get(i);
            Feature chunkLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
            String chunkLabel = chunk.getStringValue(chunkLabelFeature);
            if (chunkLabel.equals(label))
            {
                labelledChunks.set(i);
            }
        }
        return labelledChunks;
    }

    private BitSet getAgeDiagChunks(List<AnnotationFS> chunkList, AnnotationFS sentence)
    {
        return new BitSet();
    }

    public List getPropositions()
    {
//        printChunks();
        BitSet familyChunks = getFamilyChunks(chunkList);
        BitSet observationChunks = getObservationChunks(chunkList, familyChunks);
        BitSet indicatorChunks = getIndicatorChunks(chunkList);
        BitSet vitalChunks = getVitalStatusChunks(chunkList);
        BitSet negationChunks = getNegationChunks(chunkList, sentenceAnnotation);
        BitSet ageDeathChunks = getLabelledChunks(chunkList, "ageDeath");
        BitSet ageDiagChunks = getLabelledChunks(chunkList, "ageDiag");
//        System.out.println("VITALS: " + vitalChunks);
//        System.out.println("NEGATION: " + negationChunks);

//        showPredications(familyChunks, observationChunks, indicatorChunks, vitalChunks, negationChunks);
//        String[] lex = LexiconUtil.checkLexicon(sentenceText);
//        System.out.println();
        List<Proposition> predicationList = generatePredicationChart(familyChunks, observationChunks, indicatorChunks,
                vitalChunks, negationChunks, chunkList);
        return predicationList;
    }

    private AnnotationFS mergeIndicators(CAS aCAS, List<AnnotationFS> indicatorList)
    {
        if (indicatorList.size() == 0) return null;
        int begin = indicatorList.get(0).getBegin();
        int end = indicatorList.get(indicatorList.size() - 1).getEnd();
        AnnotationFS newIndicator = aCAS.createAnnotation(familyHistoryConstituentType, begin, end);
        newIndicator.setStringValue(chunkLabelFeature, "indicator");

        for (AnnotationFS ind : indicatorList)
        {
            aCAS.removeFsFromIndexes(ind);
        }
        return newIndicator;
    }

    private List<Proposition> getFamIndObsPredications(BitSet family, BitSet indicators,BitSet observations,
                                                       List<AnnotationFS> constituentList)
    {
        List<Proposition> results = new ArrayList<>();
        HashMap<String,List<AnnotationFS>> entities = new HashMap<>();
        entities.put("family", new ArrayList<AnnotationFS>());
        entities.put("observation", new ArrayList<AnnotationFS>());
        entities.put("indicator", new ArrayList<AnnotationFS>());
        entities.put("vitalStatus", new ArrayList<AnnotationFS>());
        entities.put("negation", new ArrayList<AnnotationFS>());
        entities.put("ageDiag", new ArrayList<AnnotationFS>());
        entities.put("ageDeath", new ArrayList<AnnotationFS>());
        for (int i = 0; i < constituentList.size(); i++)
        {
            AnnotationFS constituent = constituentList.get(i);
            String label = constituent.getStringValue(chunkLabelFeature);
            List<AnnotationFS> res = entities.get(label);
            res.add(constituent);
        }

        AnnotationFS newIndicator = mergeIndicators(aCAS, entities.get("indicator"));
        for (AnnotationFS fam : entities.get("family"))
        {
            Proposition prop = null;
            for (AnnotationFS obs : entities.get("observation"))
            {
                prop = new Proposition();
                prop.setFamilyArgument(fam);
                prop.setObservationArgument(obs);
                prop.setPredicationIndicator(newIndicator);
                for (AnnotationFS negation : entities.get("negation"))
                {
                    prop.setNegationArgument(negation);
                    prop.setNegationStatus(true);
                }
                results.add(prop);
            }

            for (AnnotationFS vital : entities.get("vitalStatus"))
            {
                assert prop != null;
                prop.setVitalStatus(vital.getCoveredText());
            }


        }
        return results;
    }

    private List<Proposition> getFamObsPredications(BitSet family, BitSet obervations, List<AnnotationFS> constituentList)
    {
        List<Proposition> results = new ArrayList<>();
        List<HashMap> entityList = new ArrayList<>();
        HashMap<String,List<AnnotationFS>> entities = new HashMap<>();
        entities.put("family", new ArrayList<AnnotationFS>());
        entities.put("observation", new ArrayList<AnnotationFS>());
        entities.put("indicator", new ArrayList<AnnotationFS>());
        entities.put("vitalStatus", new ArrayList<AnnotationFS>());
        entities.put("negation", new ArrayList<AnnotationFS>());
        entities.put("ageDeath", new ArrayList<AnnotationFS>());
        entities.put("ageDiag", new ArrayList<AnnotationFS>());

        boolean inEntity = false;
        for (int i = 0; i < constituentList.size(); i++)
        {
            AnnotationFS constituent = constituentList.get(i);
            String label = constituent.getStringValue(chunkLabelFeature);
            List<AnnotationFS> res = entities.get(label);
            res.add(constituent);
        }

        for (AnnotationFS f : entities.get("family"))
        {
            for (AnnotationFS o : entities.get("observation"))
            {
                Proposition prop = new Proposition();
                prop.setFamilyArgument(f);
                prop.setObservationArgument(o);
                results.add(prop);
            }
        }
        return results;
//
//            if (label.equals("family") && inEntity)
//            {
//                entityList.add(entities);
//                entities = new HashMap<>();
//                entities.put("family", new ArrayList<AnnotationFS>());
//                entities.put("observation", new ArrayList<AnnotationFS>());
//                entities.put("indicator", new ArrayList<AnnotationFS>());
//                entities.put("vitalStatus", new ArrayList<AnnotationFS>());
//                entities.put("negation", new ArrayList<AnnotationFS>());
//                entities.get("family").add(constituent);
//            }
//            else
//            {
//                if (label.equals("family"))
//                    inEntity = true;
//                List<AnnotationFS> res = entities.get(label);
//                res.add(constituent);
//                if (i == constituentList.size() - 1)
//                    entityList.add(entities);
//            }
//        }
//
//        for (HashMap<String,List<AnnotationFS>> currentEntities : entityList)
//        {
//            for (AnnotationFS fam : currentEntities.get("family"))
//            {
//                for (AnnotationFS obs : currentEntities.get("observation"))
//                {
//                    Proposition prop = new Proposition();
//                    prop.setFamilyArgument(fam);
//                    prop.setObservationArgument(obs);
//                    results.add(prop);
//                }
//            }
//        }
//        return results;
    }

    private List<Proposition> getObsPredications(BitSet observations, List<AnnotationFS> constituentList)
    {
        List<Proposition> results = new ArrayList<>();
        HashMap<String,List<AnnotationFS>> entities = new HashMap<>();
        entities.put("family", new ArrayList<AnnotationFS>());
        entities.put("observation", new ArrayList<AnnotationFS>());
        entities.put("indicator", new ArrayList<AnnotationFS>());
        entities.put("negation", new ArrayList<AnnotationFS>());
        entities.put("vitalStatus", new ArrayList<AnnotationFS>());
        entities.put("ageDeath", new ArrayList<AnnotationFS>());
        entities.put("ageDiag", new ArrayList<AnnotationFS>());
        for (int i = 0; i < constituentList.size(); i++)
        {
            AnnotationFS constituent = constituentList.get(i);
            String label = constituent.getStringValue(chunkLabelFeature);
            entities.get(label).add(constituent);
        }

        for (AnnotationFS obs : entities.get("observation"))
            {
                Proposition prop = new Proposition();
                prop.setObservationArgument(obs);
                results.add(prop);
        }
        return results;
    }

    private List<Proposition> getObsIndPredications(BitSet observations, BitSet indicators, List<AnnotationFS> constituentList)
    {
        List<Proposition> results = new ArrayList<>();
        HashMap<String,List<AnnotationFS>> entities = new HashMap<>();
        entities.put("observation", new ArrayList<AnnotationFS>());
        entities.put("indicator", new ArrayList<AnnotationFS>());
        entities.put("vitalStatus", new ArrayList<AnnotationFS>());
        entities.put("negation", new ArrayList<AnnotationFS>());
        entities.put("ageDeath", new ArrayList<AnnotationFS>());
        entities.put("ageDiag", new ArrayList<AnnotationFS>());
        for (int i = 0; i < constituentList.size(); i++)
        {
            AnnotationFS constituent = constituentList.get(i);
            String label = constituent.getStringValue(chunkLabelFeature);
            List<AnnotationFS> res = entities.get(label);
            res.add(constituent);
        }

        AnnotationFS newIndicator = mergeIndicators(aCAS, entities.get("indicator"));
            for (AnnotationFS obs : entities.get("observation"))
            {
                Proposition prop = new Proposition();
                prop.setObservationArgument(obs);
                prop.setPredicationIndicator(newIndicator);
                results.add(prop);
            }
        return results;
    }

    private List<Proposition> getObsFamPredications(BitSet observations, BitSet family, List<AnnotationFS> constituentList)
    {
        List<Proposition> results = new ArrayList<>();
        HashMap<String,List<AnnotationFS>> entities = new HashMap<>();
        entities.put("family", new ArrayList<AnnotationFS>());
        entities.put("observation", new ArrayList<AnnotationFS>());
        entities.put("vitalStatus", new ArrayList<AnnotationFS>());
        entities.put("indicator", new ArrayList<AnnotationFS>());
        entities.put("ageDeath", new ArrayList<AnnotationFS>());
        entities.put("ageDiag", new ArrayList<AnnotationFS>());
        entities.put("negation", new ArrayList<AnnotationFS>());

        for (int i = 0; i < constituentList.size(); i++)
        {
            AnnotationFS constituent = constituentList.get(i);
            String label = constituent.getStringValue(chunkLabelFeature);
            entities.get(label).add(constituent);
        }

        for (AnnotationFS fam : entities.get("family"))
        {
            for (AnnotationFS obs : entities.get("observation"))
            {
                Proposition prop = new Proposition();
                prop.setFamilyArgument(fam);
                prop.setObservationArgument(obs);
                results.add(prop);
            }
        }
        return results;

    }

    private List<Proposition> getIndPredications(BitSet indicators, List<AnnotationFS> constituentList)
    {
        List<Proposition> results = new ArrayList<>();
        HashMap<String,List<AnnotationFS>> entities = new HashMap<>();
        entities.put("family", new ArrayList<AnnotationFS>());
        entities.put("indicator", new ArrayList<AnnotationFS>());
        entities.put("vitalStatus", new ArrayList<AnnotationFS>());
        entities.put("observation", new ArrayList<AnnotationFS>());
        entities.put("ageDeath", new ArrayList<AnnotationFS>());
        entities.put("ageDiag", new ArrayList<AnnotationFS>());
        entities.put("negation", new ArrayList<AnnotationFS>());
        for (int i = 0; i < constituentList.size(); i++)
        {
            AnnotationFS constituent = constituentList.get(i);
            String label = constituent.getStringValue(chunkLabelFeature);
            entities.get(label).add(constituent);
        }

        AnnotationFS newIndicator = mergeIndicators(aCAS, entities.get("indicator"));
        Proposition prop = new Proposition();
        prop.setPredicationIndicator(newIndicator);
        results.add(prop);
        return results;
    }

    private List<Proposition> getIndFamPredications(BitSet indicators, BitSet family, List<AnnotationFS> constituentList)
    {
        List<Proposition> results = new ArrayList<>();
        HashMap<String,List<AnnotationFS>> entities = new HashMap<>();
        entities.put("family", new ArrayList<AnnotationFS>());
        entities.put("indicator", new ArrayList<AnnotationFS>());
        entities.put("vitalStatus", new ArrayList<AnnotationFS>());
        entities.put("observation", new ArrayList<AnnotationFS>());
        entities.put("ageDeath", new ArrayList<AnnotationFS>());
        entities.put("ageDiag", new ArrayList<AnnotationFS>());
        entities.put("negation", new ArrayList<AnnotationFS>());

        for (int i = 0; i < constituentList.size(); i++)
        {
            AnnotationFS constituent = constituentList.get(i);
            String label = constituent.getStringValue(chunkLabelFeature);
            entities.get(label).add(constituent);
        }

        AnnotationFS newIndicator = mergeIndicators(aCAS, entities.get("indicator"));
        for (AnnotationFS fam : entities.get("family"))
        {
                Proposition prop = new Proposition();
                prop.setFamilyArgument(fam);
                prop.setPredicationIndicator(newIndicator);
                results.add(prop);
        }
        return results;
    }

    private List<Proposition> getCompoundListPredications(BitSet family,
                                                          BitSet indicators,
                                                          BitSet observations,
                                                          List<AnnotationFS> constituentList)
    {

        return new ArrayList<>();
    }

    private List<List<AnnotationFS>> splitConstituents(List<AnnotationFS> candidates)
    {
        BitSet complete = new BitSet();
        BitSet current = new BitSet();
        complete.set(0);
        complete.set(1);
        complete.set(2);

        int breakpoint = 1;
        for (AnnotationFS c : candidates)
        {
            String label = c.getStringValue(chunkLabelFeature);
            if (label.equals("family")) current.set(0);
            else if (label.equals("indicator")) current.set(1);
            else if (label.equals("observation")) current.set(2);

            if (complete.equals(current)) break;
            else breakpoint++;
        }

        List<List<AnnotationFS>> results = new ArrayList<>();
        List<AnnotationFS> firstHalf = new ArrayList<>();
        for (int i = 0; i < breakpoint; i++)
        {
            firstHalf.add(candidates.get(i));
        }

        List<AnnotationFS> secondHalf = new ArrayList<>();
        for (int j = breakpoint; j < candidates.size(); j++)
        {
            secondHalf.add(candidates.get(j));
        }

        results.add(firstHalf);
        results.add(secondHalf);
        return results;
    }

    private List<Proposition> generatePredicationChart(BitSet family, BitSet observations,
                                                       BitSet indicators,
                                                       BitSet vitals, BitSet negation,
                                                       List<AnnotationFS> constituentList)
    {

        String pattern = getPattern(family, observations, indicators, constituentList);
        List<Proposition> results = new ArrayList<>();
//        System.out.println("Pattern: " + pattern);

        switch (pattern) {
            case "FamIndObs":
                results = getFamIndObsPredications(family, indicators, observations, constituentList);
                results.size(); // for debug
                return results;
            case "FamObs":
                results = getFamObsPredications(family, observations, constituentList);
                results.size();
                return results;
            case "FamObsFamObs":
                results = getFamObsPredications(family, observations, constituentList);
                results.size();
                return results;
            case "ObsInd":
                results = getObsIndPredications(observations, indicators, constituentList);
                results.size();
                return results;
            case "Ind":
                results = getIndPredications(indicators, constituentList);
                results.size();
                return results;
            case "Obs":
                results = getObsPredications(observations, constituentList);
                results.size();
                return results;
            case "ObsIndFam":
                results = getFamIndObsPredications(observations, indicators, family, constituentList);
                results.size();
                return results;
            case "IndObs":
                results = getObsIndPredications(indicators, observations, constituentList);
                results.size();
                return results;
            case "IndFam":
                results = getIndFamPredications(indicators, family, constituentList);
                results.size();
                return results;
            case "ObsFam":
                results = getObsFamPredications(observations, family, constituentList);
                results.size();
                return results;
            case "IndFamIndObs":
                indicators = removeLeadingIndicators(indicators, family, observations, constituentList);
                results = getFamIndObsPredications(family, indicators, observations, constituentList);
                results.size();
                return results;
            case "IndFamIndObsIndFam":
                results = getCompoundListPredications(family, indicators, observations, constituentList);
                results.size();
                return results;
            case "FamIndObsFamIndObs":
                List<List<AnnotationFS>> splitConstituents = splitConstituents(constituentList);
                for (List<AnnotationFS> currentList : splitConstituents)
                {
                    results.addAll(getFamIndObsPredications(family, indicators, observations, currentList));
                }
                results.size(); // for debug
                return results;
            case "FamObsFamIndObs":
                results = getFamIndObsPredications(family, indicators, observations, constituentList);
                results.size(); // for debug
                return results;


            case "":
                Proposition p = new Proposition(); // empty prop because no values
                results.add(p);
                return results;

        }



//        System.out.println("No Pattern Found: " + pattern);

//        int len = constituentList.size();
//        boolean foundFam1 = false;
//        int fam1 = 0;
//        int fam2;
//
//        if (family.isEmpty())
//        {
//            return generatePredicationChartWithoutFamily(observations, indicators, vitals, negation, constituentList);
//        }
//
//        for (int i = 0; i < len; i++)
//        {
//            if (family.get(i))
//            {
//                if (foundFam1)
//                {
//                    fam2 = i;
//                    System.out.print("Merging family chunks to: ");
//                    for (int j = fam1; j <= fam2; j++)
//                    {
//                        System.out.print(constituentList.get(j).getCoveredText() + " ");
//                    }
//                    System.out.println();
//                } else
//                {
//                    foundFam1 = true;
//                    fam1 = i;
//                }
//            } else if (observations.get(i) && foundFam1)
//            {
//                // Don't merge
//                foundFam1 = false;
//            }
//        }


//        boolean foundObservation = false;
//        boolean foundFam = false;
//        boolean foundIndicator = false;
//        int famPred = 0;
//        int obsPred = 0;
//        int indPred = 0;
//        String vital = null;
//        boolean negated = false;
//
//        for (int i = 0; i < len; i++)
//        {
//            if (vitals.get(i))
//            {
//                vital = constituentList.get(i).getCoveredText();
//            }
//            if (negation.get(i))
//            {
//                negated = true;
//            }
//            if (family.get(i))
//            {
//                foundFam = true;
//                famPred = i;
//
//                if (foundObservation && foundIndicator)
//                {
//                    Feature familyConstituentIdentifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
//                    AnnotationFS famCon = constituentList.get(famPred);
//                    AnnotationFS indCon = constituentList.get(indPred);
//                    AnnotationFS obsCon = constituentList.get(obsPred);
//                    String msg = String.format("\tPredication marked at 603: {%s:%s} {%s:%s} {%s:%s}",
//                            famCon.getCoveredText(), famCon.getStringValue(familyConstituentIdentifierFeature),
//                            indCon.getCoveredText(), indCon.getStringValue(familyConstituentIdentifierFeature),
//                            obsCon.getCoveredText(), obsCon.getStringValue(familyConstituentIdentifierFeature));
//
//
//                    System.out.print(msg);
//
//                    if (negated)
//                    {
//                        System.out.print(" NEGATED ");
//                    }
//                    if (vital != null)
//                    {
//                        System.out.println("Vital: " + vital);
//                    } else
//                    {
//                        System.out.println();
//                    }
//                    foundObservation = false;
//                    foundFam = false;
//                    foundIndicator = false;
//                    negated = false;
//                    vital = null;
//                }
//            } else if (indicators.get(i))
//            {
//                foundIndicator = true;
//                indPred = i;
//            } else if (observations.get(i))
//            {
//                foundObservation = true;
//                obsPred = i;
//                if (foundFam && foundIndicator)
//                {
//                    AnnotationFS familyConstituent = constituentList.get(famPred);
//                    AnnotationFS observationConstituent = constituentList.get(obsPred);
//                    AnnotationFS indicator = constituentList.get(indPred);
//                    String msg = String.format("\tPredication marked at 641: {%s} {%s} {%s}",
//                            familyConstituent.getCoveredText(), indicator.getCoveredText(),
//                            observationConstituent.getCoveredText());
//                    System.out.print(msg);
//
//                    Proposition prop = new Proposition();
//                    //List<Proposition> propList = resolveCoordination(familyConstituent, observationConstituent, indicator);
//
//                    if (negated)
//                    {
//                        System.out.print(" NEGATED ");
//                        prop.setNegationStatus(true);
//                    }
//
//                    if (vital != null)
//                    {
//                        System.out.println("Vital: " + vital);
//                        prop.setVitalStatus(vital);
//                    } else
//                    {
//                        System.out.println();
//                    }
//
//                    results.add(prop);
//                    foundObservation = false;
//                    foundFam = false;
//                    foundIndicator = false;
//                    negated = false;
//                    vital = null;
//                }
//            } else if (i == len && !foundFam && !foundObservation)
//            {
//                // add empty predication because a statement exists, but there isn't anything to map observation or family to
//                if (!foundFam && !foundObservation)
//                {
//                    System.out.println("Adding empty predication.");
//                }
//                System.out.println("\n");
//
//            }
//
//
//        }
        return results;
    }

    protected BitSet removeLeadingIndicators(BitSet indicators, BitSet family, BitSet observations, List<AnnotationFS> constituents)
    {
        int len = constituents.size();
        for (int i = 0; i < len; i++)
        {
            boolean fam = family.get(i);
            boolean obs = observations.get(i);
            if (fam || obs)
            {
                return indicators;
            }
            indicators.clear(i);
        }

        return indicators;
    }


    protected List<Proposition> generatePredicationChartWithoutFamily(
            BitSet observations, BitSet indicators, BitSet vitals, BitSet negation, List<AnnotationFS> constituentList
    )
    {
        ArrayList<Proposition> results = new ArrayList<>();
        int len = constituentList.size();
        boolean foundObservation = false;
        boolean foundFam = false;
        boolean foundIndicator = false;
        int famPred = 0;
        int obsPred = 0;
        int indPred = 0;
        String vital = null;
        boolean negated = false;

        for (int i = 0; i < len; i++)
        {
            if (vitals.get(i))
            {
                vital = constituentList.get(i).getCoveredText();
            }
            if (negation.get(i))
            {
                negated = true;
            }
            if (foundObservation)
            {
                Feature familyConstituentIdentifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
                AnnotationFS famCon = constituentList.get(famPred);
                AnnotationFS indCon = constituentList.get(indPred);
                AnnotationFS obsCon = constituentList.get(obsPred);
                String msg = String.format("\tPredication marked at 719: {%s:%s} {%s:%s} { %s:%s}",
                        famCon.getCoveredText(), famCon.getStringValue(familyConstituentIdentifierFeature),
                        indCon.getCoveredText(), indCon.getStringValue(familyConstituentIdentifierFeature),
                        obsCon.getCoveredText(), obsCon.getStringValue(familyConstituentIdentifierFeature));
                System.out.print(msg);

                if (negated)
                {
                    System.out.print(" NEGATED ");
                }
                if (vital != null)
                {
                    System.out.println("Vital: " + vital);
                } else
                {
                    System.out.println();
                }
                foundObservation = false;
                foundFam = false;
                foundIndicator = false;
                negated = false;
                vital = null;
            } else if (indicators.get(i))
            {
                foundIndicator = true;
                indPred = i;
            } else if (observations.get(i))
            {
                foundObservation = true;
                obsPred = i;
                    AnnotationFS familyConstituent = constituentList.get(famPred);
                    AnnotationFS observationConstituent = constituentList.get(obsPred);
                    AnnotationFS indicator = constituentList.get(indPred);
                    String msg = String.format("\t  Predication marked at 752: {%s} {%s} {%s}",
                            familyConstituent.getCoveredText(), indicator.getCoveredText(),
                            observationConstituent.getBegin());
                    System.out.print(msg);

                    Proposition prop = new Proposition();
                    //List<Proposition> propList = resolveCoordination(familyConstituent, observationConstituent, indicator);

                    if (negated)
                    {
                        System.out.print(" NEGATED ");
                        prop.setNegationStatus(true);
                    }

                    if (vital != null)
                    {
                        System.out.println("Vital: " + vital);
                        prop.setVitalStatus(vital);
                    } else
                    {
                        System.out.println();
                    }

                    results.add(prop);
                    foundObservation = false;
                    foundFam = false;
                    foundIndicator = false;
                    negated = false;
                    vital = null;
            } else if (i == len && !foundFam && !foundObservation)
            {
                // add empty predication because a statement exists, but there isn't anything to map observation or family to
                if (!foundFam && !foundObservation)
                {
                    System.out.println("Adding empty predication.");
                }
                System.out.println("\n");

            }
        }
        return results;
    }

    protected void generateStatement(Proposition prop)
    {
        propositionList.add(prop);

    }

    private String getPattern(BitSet family, BitSet observation, BitSet indicator, List<AnnotationFS> constituentList)
    {
        int len = constituentList.size();
        StringBuilder pattern = new StringBuilder();
        String previousMatch = "";
        for (int i = 0; i < len; i++)
        {
            if (family.get(i) && !previousMatch.equals("Fam"))
            {
                pattern.append("Fam");
                previousMatch = "Fam";
            }
            if (indicator.get(i) && !previousMatch.equals("Ind"))
            {
                pattern.append("Ind");
                previousMatch = "Ind";
            }
            if (observation.get(i) && !previousMatch.equals("Obs"))
            {
                pattern.append("Obs");
                previousMatch = "Obs";
            }
        }
        return pattern.toString();


    }
    private enum Position {START_OF_PROPOSITION, WITHIN_PROPOSITION}


    private enum ArgumentClass {FAMILY, OBSERVATION}

}
