#Overview

The BioMedical Information Collection and Understanding System (BioMedICUS) leverages open source solutions for
text analysis and provides new analytic tools for processing and analyzing text of biomedical and clinical reports.
The system is being developed by our biomedical NLP/IE program at the University of Minnesota.
This is a collaborative project that aims to serve biomedical and clinical researchers, allowing for customization
with different texts.

# Prerequisites

Getting and building BioMedICUS requires [git](http://git-scm.com), [maven](http://maven.apache.org) and
[UIMA](http://uima.apache.org). So, the first steps are:

            1. install git
            2. install maven
            3. install apache-uima-as-2.4.2

# Getting BioMedICUS

At a command prompt, change directories to where you want to install BioMedICUS, and then enter:

            $> git clone https://bitbucket.org/nlpie/biomedicus.git

# Setting the _HOME environment variables

Building and running BioMedICUS requires the `UIMA_HOME`, `JAVA_HOME` and `BIOMEDICUS_HOME` environment variables to be set.
 The JAVA_HOME must be set the jdk1.7. Earlier versions of Java will not work.
 `BIOMEDICUS_HOME` must be set to the destination folder in the git clone step.
 `UIMA_HOME` is the path to your local apache-uima-2.4.2 folder.

            $> cd biomedicus
            $> export BIOMEDICUS_HOME=`pwd`
            $> export UIMA_HOME=/path/to/apache-uima-as-2.4.2
            $> export JAVA_HOME=/path/to/jdk1.7

Note: On Mac OSX, you can set the JAVA_HOME environment variable with
`export JAVA_HOME=$(/usr/libexec/java_home)`

# Building Biomedicus

Once you have downloaded the source using git, then change directories into the `biomedicus` folder and install
using maven. The commands should be:

            $> cd biomedicus
            $> mvn install

# Projects

If you wish to use BioMedICUS to analyze text without having to do any programming, then you need the
biomedicus-project module. Within the biomedicus-project module there is a folder named "template." Copy
the template folder to a location where you want to work on a project. The location you copied that folder to is your project directory. Next, copy the text files you wish
to annoation into the "input" folder within your project directory. Then, run the project pipeline
with:

            $> sh bin/biomedicus -p /path/to/where/you/copied/the/template/folder

When executing the above command completes, the "output" folder within your project directory should contain xml-encoded files that you can view with the UIMA AnnotationView (see below). 

# Archetypes

If you wish to build an application or module within BioMedICUS, then it is easiest to start with the archetype.
The archetype generator produces a scaffold/skeleton of an application or module to jump-start development.

The root pom.xml file that is in the BIOMEDICUS_HOME folder should contain module definitions for all the BioMedICUS modules. When creating applications or modules using Maven archetypes, maven will add references to this root pom.xml file. Should you wish to change names, delete or move the folders created with archetypes, then you will need to edit the $BIOMEDICUS_HOME/pom.xml file. 

## Application

An application keeps the xml descriptor files outside jar files so that they're easy to change. BioMedICUS has
a maven archetype to create an application scaffold. To create a new application, go to the $BIOMEDICUS_HOME
folder in a command prompt and then enter:

            $> mvn archetype:generate -DarchetypeCatalog=local

You should see a choice of 2 archetypes:

            Choose archetype:
            1: local -> edu.umn:biomedicus-application-archetype (BioMedICUS Application Archetype)
            2: local -> edu.umn:biomedicus-module-archetype (BioMedICUS Module Archetype)
            Choose a number or apply filter (format: [groupId:]artifactId, case sensitive contains): :

Enter the number for a biomedicus-application-archetype. You will
be prompted for the following settings (followed by recommended values):

            'groupId': : edu.umn.biomedicus
            'artifactId': : your_application_name
            'version': : 0.0.1
            'package': : edu.umn.biomedicus
            Y: : Y

Maven should have created an application with your_application_name. You can compile and use this application
 immediately with:

             $> cd your_application_name
             $> mvn compile
             $> sh bin/run.sh

When the application is done running, you should see files in the "output" folder. These are the results from
annotation files present in the "input" folder. Some of the sample notes from the MTSamples.org site are
in the "input" folder for testing.

## Analysis Engine Module

An analysis engine module is something that defines a simple analysis engine. The differences from an application
are that an analysis engine module does not have the application or configuration classes. It only has those
classes necessary to create a new analysis engine. Note that the analysis engine descriptor file is kept in
the src/resources directory. This means you must repackage, or re-install the module every time you change the
contents of the descriptor file.

Generating an analysis engine module is the same as an application, except that you choose the
biomedicus-module-archetype during the scaffold generation.


# Project vs. Module vs. Application

Modules either within or using BioMedICUS can be described as either being a project, module or applications. The difference doesn't matter much except for the template code used to start new work using BioMedICUS. Descriptions of the differences follow.

## Project
Creating a project means you do not intend to write any java code. A project uses only UIMA descriptor files to control the settings and operations of existing annotators. A projec is intended for users wanting to use the annotations created by BioMedICUS, but not developing new modules or applications.

## Module
A module is a simple analysis engine (annotator) without all the overhead of complicated UIMA applications. All XML descriptor files are kept in JARs instead of system files.
This means you must repackage every time there is a change to the descriptor/specifier (xml) files. Use this to
create reusable components. For example, a drug identification module or a new concept mapping module.

## Application
An application is a mix of flow controllers, configuration settings, descriptor files and annotators to compose a
full pipeline instead of a single annotator as in the case of a module. XML files are kept in the file system (instead of in jars so they can be easily changed; therefore, when you make changes to xml files, there is no need to repack. This is what end user would use to develop new applications requiring full pipelines (e.g., multiple annotators).

# Using UIMA's annotationViewer

You must install the apache-UIMA package first, set the UIMA_HOME directory in the environment variables, and then
you can run any processing pipeline on a specified document with the DocumentAnalyzer. The steps are:

			1. run the AnnotationViewer

					$> sh bin/annotationViewer.sh

            2. choose the TypeSystem file in
            `BIOMEDICUS_HOME/biomedicus-core/src/main/resources/edu/umn/biomedicus/type/TypeSystem.xml`

# Committing code
Developers wishing to contribute code are invaded to add their work to feature branches of the BioMedICUS git repository on bitbucket.org. 

Core developers can commit to the develop branch. For those wishing to commit code to the develop branch, here is an example scenario:

            $> git clone https://robertwbill@bitbucket.org/nlpie/biomedicus.git
            $> # Change to the develop branch
            $> git -b development
            $> # Make some sort of change (next line)
            $> vi biomedicus-mapping/pom.xml
            $> # Commit your change
            $> git commit -a -m "Description of change made"
            $> git push -u origin development

# Annotator status board

Annotator                | Method           | status
----------               | ------           | ------
Tokenizer                | Maxent           | production
Tokenizer                | Whitespace       | production
Tokenizer                | Stanford         | development
Tokenizer                | IMB-ICU          | development
Sentence Segmenting      | OpenNLP Maxent   | development
Sentence Segmenting      | Stanford         | development
POS                      | Maxent           | development
Chunking                 | Maxent           | development
Chunking                 | Metamap          | development
Normalizing              | LVG              | development
Mapping                  | Metamap          | development
Mapping                  | Dictionary       | development
Indexing                 | Lucene           | development
Indexing                 | ElasticSearch    | development
Acronym Detection        | SVM              | development
Acronym Disambiguation   | SVM              | design
Lexical Parse            | Stanford         | design
Lexical Parse            | OpenNLP          | design
Lexical Parse            | Charniak         | design
Dependency Parse         | Link             | design
Section Segmenting       | RegEx            | development
Symbol Disambiguation    | SVM              | design


About Us
========
 NLP-TAB is developed by the
 [University of Minnesota Institute for Health Informatics NLP/IE Group](http://www.bmhi.umn.edu/ihi/research/nlpie/) and
 the [Open Health NLP Consortium](http://ohnlp.org/index.php/Main_Page).

Other Resources
===============

###BioMedICUS

 *   [Demo](http://athena.ahc.umn.edu/biomedicus/)
 *   [Source Code](https://bitbucket.org/nlpie/biomedicus)

###NLP-TAB

 *   [Demo](http://athena.ahc.umn.edu/nlptab)
 *   [Java Source Code](http://bitbucket.org/nlpie/nlptab)
 *   [Web-app Source Code](http://bitbucket.org/nlpie/nlptab-webapp)
 *   [Corpus](http://bitbucket.org/nlpie/nlptab-corpus)

###NLP/IE Group Resources

 *   [Website](http://www.bmhi.umn.edu/ihi/research/nlpie/resources/index.htm)
 *   [Demos](http://athena.ahc.umn.edu/)

Acknowledgements
================
Funding for this work was provided by:

 *	1 R01 LM011364-01 NIH-NLM
 *	1 R01 GM102282-01A1 NIH-NIGMS
 *	U54 RR026066-01A2 NIH-NCRR
