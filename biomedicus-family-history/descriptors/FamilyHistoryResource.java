
package edu.umn.biomedicus.familyhistory;


import edu.umn.biomedicus.applications.pipelines.FamilyHistoryCLIPipeline;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.FamilyHistory;
import org.apache.uima.UIMAException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Example resource class hosted at the URI path "/myresource"
 */
@Path("/familyhistory")
@Singleton
public class FamilyHistoryResource {
    TypeSystem ts;
    Type familyHistoryConstituentType;
    Type familyHistoryType;
    Type tokenType;
    Type chunkType;
    Feature labelFeature;

    String collData = "{\n" +
            "    \"entity_types\": [\n" +
            "        {\n" +
            "            \"type\": \"FamilyHistoryConstituent\",\n" +
            "            \"labels\": [ \"Constituent\", \"Con\" ],\n" +
            "            \"bgColor\": \"#7fa2ff\",\n" +
            "            \"borderColor\": \"darken\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"relation_types\": [\n" +
            "        {\n" +
            "            \"type\": \"Predication\",\n" +
            "            \"labels\": [ \"Predication\", \"Pred\" ],\n" +
            "            \"dashArray\": \"3,3\",\n" +
            "            \"color\": \"purple\",\n" +
            "            \"args\": [\n" +
            "                {\n" +
            "                    \"role\": \"Family\",\n" +
            "                    \"targets\": [ \"FamilyHistoryConstituent\" ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"role\": \"Observation\",\n" +
            "                    \"targets\": [ \"FamilyHistoryConstituent\" ]\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    ],\n" +
            "    \"event_types\": [\n" +
            "        {\n" +
            "            \"type\": \"Indicator\",\n" +
            "            \"labels\": [ \"Possession\", \"Posses\" ],\n" +
            "            \"bgColor\": \"lightgreen\",\n" +
            "            \"borderColor\": \"darken\",\n" +
            "            \"arcs\": [\n" +
            "                {\n" +
            "                    \"type\": \"Family\",\n" +
            "                    \"labels\": [ \"Family\", \"Fam\" ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"type\": \"Observation\",\n" +
            "                    \"labels\": [ \"Observation\", \"Obs\" ],\n" +
            "                    \"color\": \"green\"\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}";
    @GET
    @Produces("text/pain")
    public String solicit()
    {
        return "This would normally return a .jsp viewable, but working on maven dependencies now."; //Viewable("/test.jsp");
    }

    @NotNull
    private FamilyHistoryCLIPipeline uimaPipeline;
    String message;

    @POST
    @Produces("text/plain")  //html")
    @Consumes("application/x-www-form-urlencoded")
    public String analyze(@FormParam("input") String sample, @Context HttpServletRequest request)
    {
        System.setProperty("metamap.path", "/Users/bill0154/Applications/public_mm/bin/metamap13");
        request.setAttribute("sample", sample);
        request.setAttribute("collData", collData);

        CAS output = null;
        try
        {
            output = getCAS(sample);
        }
        catch (ResourceInitializationException e)
        {
            request.setAttribute("error", e.getStackTrace().toString());
        }

        // housekeeping: init types, create model and get system view
        if (output != null) initTypes(output);
        Map<String, List<Constituent>> model = createModel();
        CAS systemView = CASUtil.getSystemView(output);

        getFamily(systemView, model);
        getObservations(systemView, model);
        getConstituents(systemView, model);

        List<Predication> predicationList = getPredications(systemView);
        request.setAttribute("predications", predicationList);

//        results.append("\n");
        //return Response.ok(new Viewable("/test.jsp", model)).build();
        return "result string"; //results.toString();

    }

    private void getFamily(CAS systemView, Map<String, List<Constituent>> model)
    {
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
        {
            Constituent con = new Constituent(annot);
            if (con.isFamily())
                model.get("family").add(con);
        }
    }

    private void getConstituents(CAS systemView, Map<String, List<Constituent>> model)
    {
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
        {
            Constituent con = new Constituent(annot);
            model.get("constituent").add(con);
        }
    }

    private void getObservations(CAS systemView, Map<String,List<Constituent>> model)
    {
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
        {
            Constituent con = new Constituent(annot);
            if (con.isObservation())
                model.get("observation").add(con);
        }
    }

    private List<Predication> getPredications(CAS systemView)
    {
        List<Predication> predicationList = new ArrayList<>();
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryType))
        {
            predicationList.add(new Predication(annot));
        }
        return predicationList;
    }

    private CAS getCAS(String sample) throws ResourceInitializationException
    {
        // Get pipeline
        FamilyHistoryCLIPipeline uimaPipeline;
        try
        {
            uimaPipeline = new FamilyHistoryCLIPipeline();
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        } catch (InvalidXMLException e)
        {
            throw new ResourceInitializationException(e);
        }

        HashMap<String, Object> model = new HashMap<>();

        CAS output = null;
        if (uimaPipeline == null)
        {
            Exception e = new Exception("Unable to create UIMA pipeline.");
            throw new ResourceInitializationException(e);
        }

        try
        {
            output = uimaPipeline.process("command-line", sample);
        } catch (UIMAException e)
        {
            e.printStackTrace();
        }

        return output;
    }

    private void initTypes(CAS aCAS)
    {
        ts = aCAS.getTypeSystem();
        //familyHistoryType = ts.getType("edu.umn.biomedicus.type.FamilyHistory");
        familyHistoryType = TypeUtil.getType(aCAS, FamilyHistory.class);
        familyHistoryConstituentType = ts.getType("edu.umn.biomedicus.type.FamilyHistoryConstituent");
        tokenType = ts.getType("edu.umn.biomedicus.type.Token");
        chunkType = ts.getType("edu.umn.biomedicus.type.Chunk");
        labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
    }

    public Map<String,List<Constituent>> createModel()
    {
        // create model
        Map<String, List<Constituent>> model = new HashMap<>();
        model.put("family", new ArrayList<Constituent>());
        model.put("observation", new ArrayList<Constituent>());
        model.put("vitalstatus", new ArrayList<Constituent>());
        model.put("negation", new ArrayList<Constituent>());
        model.put("constituent", new ArrayList<Constituent>());
        return model;
    }
}
