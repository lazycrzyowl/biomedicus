\d{1,2} years of age
age of \d{1,2}
age of \d{1,2} months?
age \d{1,2} years?
age \d{1,2} months?
age of \d{1,2}
age \d{1,2}
unknown age
early \d0s
late \d0s
at birth
\d\ds
\d{2}
