/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.typeWrapper;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class FamilyHistory {
    private static final String FHC_TYPE_NAME = "edu.umn.biomedicus.type.FamilyHistory";
    @NotNull
    private Feature familyConstituentFeature;
    @NotNull
    private Feature familyMappingFeature;
    @NotNull
    private Feature observationConstituentFeature;
    @NotNull
    private Feature observationMappingFeature;
    @NotNull
    private Feature vitalStatusFeature;
    @NotNull
    private Feature negatedFeature;
    @NotNull
    private Feature indicatorFeature;
    @NotNull
    private Feature certaintyFeature;
    @NotNull
    private Feature sideOfFamilyFeature;
    @NotNull
    private Type familyHistoryType;
    @NotNull
    private Feature constituentSideOfFamilyFeature;
    @NotNull
    CAS aCAS;
    @NotNull
    AnnotationFS familyHistoryAnnotation;

    private int getMax(FamilyHistoryConstituent... constituents)
    {
        int maxIdx = 0;
        for (FamilyHistoryConstituent c : constituents)
        {
            if (c != null && c.getBegin() > maxIdx)
                maxIdx = c.getEnd();
        }
        return maxIdx;
    }

    private int getMin(FamilyHistoryConstituent... constituents)
    {
        int minIdx = 100000000;
        for (FamilyHistoryConstituent c : constituents)
        {
            if (c != null && c.getBegin() < minIdx)
                minIdx = c.getBegin();
        }
        return minIdx;
    }
    public FamilyHistory(@NotNull CAS aCAS, FamilyHistoryConstituent familyConstituent,
                         FamilyHistoryConstituent indicatorConstituent,
                         FamilyHistoryConstituent observationConstituent)
    {
        typeSystemInit(aCAS.getTypeSystem());
        int begin = getMin(familyConstituent, indicatorConstituent, observationConstituent);
        int end = getMax(familyConstituent, indicatorConstituent, observationConstituent);

        familyHistoryAnnotation = aCAS.createAnnotation(familyHistoryType, begin, end);
        if (familyConstituent != null)
            familyHistoryAnnotation.setStringValue(familyConstituentFeature, familyConstituent.getCoveredText());
        if (indicatorConstituent != null)
            familyHistoryAnnotation.setStringValue(indicatorFeature, indicatorConstituent.getCoveredText());
        if (observationConstituent != null)
            familyHistoryAnnotation.setStringValue(observationConstituentFeature, observationConstituent.getCoveredText());

        this.aCAS = aCAS;
    }

    @NotNull
    public String getFamilyConstituent()
    {
        return familyHistoryAnnotation.getStringValue(familyConstituentFeature);
    }

    public void setFamilyConstituent(@NotNull String family)
    {
        familyHistoryAnnotation.setStringValue(familyConstituentFeature, family);
    }

    @Nullable
    public String getFamilyMapping()
    {
        return familyHistoryAnnotation.getStringValue(familyMappingFeature);
    }

    public void setFamilyMapping(@Nullable String familyID)
    {
        familyHistoryAnnotation.setStringValue(familyMappingFeature, familyID);
    }

    @NotNull
    public String getObservationConstituent()
    {
        return familyHistoryAnnotation.getStringValue(observationConstituentFeature);
    }

    public void setObservationConstituent(@NotNull String observationConstituent)
    {
        familyHistoryAnnotation.setStringValue(observationConstituentFeature, observationConstituent);
    }

    @Nullable
    public String getObservationMapping()
    {
        return familyHistoryAnnotation.getStringValue(observationMappingFeature);
    }

    public void setObservationMapping(@Nullable String observationMapping)
    {
        familyHistoryAnnotation.setStringValue(observationMappingFeature, observationMapping);
    }

    @Nullable
    public String getVitalStatus()
    {
        return familyHistoryAnnotation.getStringValue(vitalStatusFeature);
    }

    public void setVitalStatus(@Nullable String vitalStatus)
    {
        familyHistoryAnnotation.setStringValue(vitalStatusFeature, vitalStatus);
    }

    public boolean isNegated()
    {
        return familyHistoryAnnotation.getBooleanValue(negatedFeature);
    }

    @Nullable
    public String getIndicator()
    {
        return familyHistoryAnnotation.getStringValue(indicatorFeature);
    }

    public void setIndicator(@NotNull String indicator)
    {
        familyHistoryAnnotation.setStringValue(indicatorFeature, indicator);
    }

    @Nullable
    public String getCertainty()
    {
        return familyHistoryAnnotation.getStringValue(certaintyFeature);
    }

    public void setCertainty(@NotNull String certainty)
    {
        familyHistoryAnnotation.setStringValue(certaintyFeature, certainty);
    }

    public String getCoveredText()
    {
        return familyHistoryAnnotation.getCoveredText();
    }


    public boolean getNegated()
    {
        return familyHistoryAnnotation.getBooleanValue(negatedFeature);
    }

    public void setNegated(boolean negated)
    {
        familyHistoryAnnotation.setBooleanValue(negatedFeature, negated);
    }

    public String getSideOfFamily()
    {
        return familyHistoryAnnotation.getStringValue(constituentSideOfFamilyFeature);
    }

    public void setSideOfFamily(String sideOfFamily)
    {
        familyHistoryAnnotation.setStringValue(sideOfFamilyFeature, sideOfFamily);
    }

    public void addToIndexes(CAS goldView)
    {
        aCAS.addFsToIndexes(familyHistoryAnnotation);
    }

    public void typeSystemInit(TypeSystem typeSystem)
    {
        familyHistoryType = typeSystem.getType("edu.umn.biomedicus.type.FamilyHistory");
        familyConstituentFeature = familyHistoryType.getFeatureByBaseName("familyConstituent");
        familyMappingFeature = familyHistoryType.getFeatureByBaseName("familyMapping");
        observationConstituentFeature = familyHistoryType.getFeatureByBaseName("observationConstituent");
        observationMappingFeature = familyHistoryType.getFeatureByBaseName("observationMapping");
        vitalStatusFeature = familyHistoryType.getFeatureByBaseName("vitalStatus");
        indicatorFeature = familyHistoryType.getFeatureByBaseName("indicatorFeature");
        negatedFeature = familyHistoryType.getFeatureByBaseName("negated");
        certaintyFeature = familyHistoryType.getFeatureByBaseName("certainty");
        sideOfFamilyFeature = familyHistoryType.getFeatureByBaseName("sideOfFamily");
    }
}
