/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.typeWrapper;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;

public class FamilyHistoryConstituent {
    private static final String FHC_TYPE_NAME = "edu.umn.biomedicus.type.FamilyHistoryConstituent";
    @NotNull
    private Type familyHistoryConstituentType;
    @NotNull
    private Feature constituentLabelFeature;
    @NotNull
    private Feature constituentIdentifierFeature;
    @NotNull
    private Feature constituentIdentifierSourceFeature;
    @NotNull
    private Feature constituentNegatedFeature;
    @NotNull
    private Feature constituentSideOfFamilyFeature;
    @NotNull
    private Feature constituentCertaintyFeature;
    @NotNull
    private Feature constituentTemporalSpecificityFeature;
    @NotNull
    private Feature constituentObservationTypeFeature;
    @NotNull
    CAS aCAS;
    @NotNull
    AnnotationFS constituentAnnotation;

    public FamilyHistoryConstituent(CAS aCAS, int begin, int end)
    {
        this.aCAS = aCAS;
        typeSystemInit(aCAS.getTypeSystem());
        constituentAnnotation = aCAS.createAnnotation(familyHistoryConstituentType, begin, end);
    }

    public FamilyHistoryConstituent(CAS aCAS, AnnotationFS constituentAnnotation)
    {
        this.aCAS = aCAS;
        this.constituentAnnotation = constituentAnnotation;
        typeSystemInit(aCAS.getTypeSystem());
    }

    public FamilyHistoryConstituent(CAS aCAS, int begin, int end, String label)
    {
        this(aCAS, begin, end);
        setLabel(label);
    }

    public String getCoveredText()
    {
        return constituentAnnotation.getCoveredText();
    }

    public int getBegin() { return constituentAnnotation.getBegin(); }
    public int getEnd() { return constituentAnnotation.getEnd(); }

    public boolean isFamily()
    {
        String label = constituentAnnotation.getStringValue(constituentLabelFeature);
        return label != null && label.equals("family");
    }

    public boolean isObservation()
    {
        String label = constituentAnnotation.getStringValue(constituentLabelFeature);
        return label != null && label.equals("observation");
    }

    public String getLabel()
    {
        return constituentAnnotation.getStringValue(constituentLabelFeature);
    }

    public void setLabel(String label)
    {
        constituentAnnotation.setStringValue(constituentLabelFeature, label);
    }

    public String getIdentifier()
    {
        return constituentAnnotation.getStringValue(constituentIdentifierFeature);
    }

    public void setFeature(String featureName, String featureValue)
    {
        Feature feature = familyHistoryConstituentType.getFeatureByBaseName(featureName);
        constituentAnnotation.setStringValue(feature, featureValue);
    }

    public void setFeature(String featureName, boolean featureValue)
    {
        Feature feature = familyHistoryConstituentType.getFeatureByBaseName(featureName);
        constituentAnnotation.setBooleanValue(feature, featureValue);
    }

    public void setIndentifier(String ident)
    {
        constituentAnnotation.setStringValue(constituentIdentifierFeature, ident);
    }

    public String getIdentifierSource()
    {
        return constituentAnnotation.getStringValue(constituentIdentifierSourceFeature);
    }

    public void setIdentifierSource(String idSource)
    {
        constituentAnnotation.setStringValue(constituentIdentifierSourceFeature, idSource);
    }

    public boolean getNegated()
    {
        return constituentAnnotation.getBooleanValue(constituentNegatedFeature);
    }

    public void setNegated(boolean negated)
    {
        constituentAnnotation.setBooleanValue(constituentNegatedFeature, negated);
    }

    public String getSideOfFamily()
    {
        return constituentAnnotation.getStringValue(constituentSideOfFamilyFeature);
    }

    public void setSideOfFamily(String sideOfFamily)
    {
        constituentAnnotation.setStringValue(constituentSideOfFamilyFeature, sideOfFamily);
    }

    public void setCertainty(String certainty)
    {
        constituentAnnotation.setStringValue(constituentCertaintyFeature, certainty);
    }

    public void getCertainty()
    {
        constituentAnnotation.getStringValue(constituentCertaintyFeature);
    }

    public void setTemporalSpecificity(String temporalSpecificity)
    {
        constituentAnnotation.setStringValue(constituentTemporalSpecificityFeature, temporalSpecificity);
    }

    public void getTemporalSpecificity()
    {
        constituentAnnotation.getStringValue(constituentTemporalSpecificityFeature);
    }

    public void setObservationType(String observationType)
    {
        constituentAnnotation.setStringValue(constituentObservationTypeFeature, observationType);
    }

    public void getObservationType()
    {
        constituentAnnotation.getStringValue(constituentObservationTypeFeature);
    }

    public void addToIndexes(CAS aCAS)
    {
        aCAS.addFsToIndexes(constituentAnnotation);
    }

    public void mergeSpan(CAS view, int otherBegin, int otherEnd)
    {
        int begin = Math.min(constituentAnnotation.getBegin(), otherBegin);
        int end = Math.max(constituentAnnotation.getEnd(), otherEnd);
        String label = constituentAnnotation.getStringValue(constituentLabelFeature);
        String identifier = constituentAnnotation.getStringValue(constituentIdentifierFeature);
        String identifierSource = constituentAnnotation.getStringValue(constituentIdentifierSourceFeature);
        boolean negated = constituentAnnotation.getBooleanValue(constituentNegatedFeature);
        String sideOfFamily = constituentAnnotation.getStringValue(constituentSideOfFamilyFeature);
        view.removeFsFromIndexes(constituentAnnotation);

        constituentAnnotation = view.createAnnotation(familyHistoryConstituentType, begin, end);
        constituentAnnotation.setStringValue(constituentLabelFeature, label);
        constituentAnnotation.setStringValue(constituentIdentifierFeature, identifier);
        constituentAnnotation.setStringValue(constituentIdentifierSourceFeature, identifierSource);
        constituentAnnotation.setBooleanValue(constituentNegatedFeature, negated);
        constituentAnnotation.setStringValue(constituentSideOfFamilyFeature, sideOfFamily);
        view.addFsToIndexes(constituentAnnotation);
    }

    public void typeSystemInit(TypeSystem typeSystem)
    {
        familyHistoryConstituentType = typeSystem.getType("edu.umn.biomedicus.type.FamilyHistoryConstituent");
        constituentLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
        constituentIdentifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
        constituentIdentifierSourceFeature = familyHistoryConstituentType.getFeatureByBaseName("identifierSource");
        constituentNegatedFeature = familyHistoryConstituentType.getFeatureByBaseName("negated");
        constituentSideOfFamilyFeature = familyHistoryConstituentType.getFeatureByBaseName("sideOfFamily");
        constituentCertaintyFeature = familyHistoryConstituentType.getFeatureByBaseName("certainty");
        constituentTemporalSpecificityFeature = familyHistoryConstituentType.getFeatureByBaseName("temporalSpecificity");
        constituentObservationTypeFeature = familyHistoryConstituentType.getFeatureByBaseName("observationType");
    }
}
