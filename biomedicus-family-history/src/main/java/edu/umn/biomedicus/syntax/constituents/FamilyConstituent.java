package edu.umn.biomedicus.syntax.constituents;


public class FamilyConstituent {

  int begin;
  int end;
  String label;
  String code;
  boolean negated = false;



  public FamilyConstituent(int begin, int end, String label, String code) {
    this.begin = begin;
    this.end = end;
    this.label = label;
    this.code = code;
  }

  public void setNegation(boolean negated) {
    this.negated = negated;
  }
}
