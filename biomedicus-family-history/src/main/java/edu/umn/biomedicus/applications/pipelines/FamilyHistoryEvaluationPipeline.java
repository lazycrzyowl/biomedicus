/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.applications.pipelines;

import edu.umn.biomedicus.core.Views;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASRuntimeException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.uimafit.factory.TypeSystemDescriptionFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Family History Serial pipeline
 */
public class FamilyHistoryEvaluationPipeline {

    private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<>();

    static
    {
        TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription(
                "./descriptors/types/TypeSystem.xml");
        TYPE_SYSTEM_DESCRIPTION.set(tsd);
    }

    private TypeSystemDescription tsd;
    /**
     * The pipeline created to process text.
     */
    private AnalysisEngine analysisEngine;

    /**
     * Creates a UIMA analysis engine.
     */
    public FamilyHistoryEvaluationPipeline(String descriptorResource) throws InvalidXMLException, IOException, ResourceInitializationException
    {
        URL descriptorFileUrl = getClass().getClassLoader()
                .getResource(descriptorResource);
        File descriptorFile = new File(descriptorFileUrl.getFile());
        XMLInputSource descriptorSource = new XMLInputSource(descriptorFile);

        System.out.println("Creating analysis engine");
        ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(
                descriptorSource);
        analysisEngine = UIMAFramework.produceAnalysisEngine(specifier);
    }

    /**
     * Uses the UIMA analysis engine to process the provided document text.
     */
    public CAS process(CAS aCAS, String currentFile) throws UIMAException
    {
        analysisEngine.typeSystemInit(aCAS.getTypeSystem());
        analysisEngine.process(aCAS);
        return aCAS;
    }

    public CAS getCAS() throws ResourceInitializationException
    {
        return analysisEngine.newCAS();
    }
}