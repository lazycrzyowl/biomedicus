/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.applications.evaluation;

import edu.umn.biomedicus.applications.pipelines.FamilyHistoryEvaluationPipeline;
import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.evaluation.ContingencyTable;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.type.FamilyHistoryConstituent;
import org.apache.commons.cli.*;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.Logger;
import org.apache.uima.util.XMLInputSource;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Properties;

/**
 * Family History Annotation Application.
 */
public class FamilyHistoryVitalStatusEvaluator
{

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(FamilyHistoryVitalStatusEvaluator.class);
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static String suffix;
    Feature labelFeature;

    public FamilyHistoryVitalStatusEvaluator(CollectionReader reader) throws IOException, UIMAException {
        String pipelineDescriptor = "META-INF/familyhistory/familyHistoryFMEvaluation.xml";
        FamilyHistoryEvaluationPipeline uimaPipeline = new FamilyHistoryEvaluationPipeline(pipelineDescriptor);

        CAS aCAS = uimaPipeline.getCAS();
        TypeSystem ts = aCAS.getTypeSystem();
        Type familyHistoryConstituentType = ts.getType(FamilyHistoryConstituent.class.getCanonicalName());
        labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");

        GoldAnnotationCollection goldCollection = new GoldAnnotationCollection(aCAS);
        SystemAnnotationCollection systemCollection = new SystemAnnotationCollection(aCAS);

        while (reader.hasNext()) {
            aCAS.reset();
            reader.getNext(aCAS);

            CAS md = CASUtil.getMetaDataView(aCAS);
            String pathAndFilename = md.getSofaDataString();
            String filename = Paths.get(pathAndFilename).getFileName().toString();

            // Get gold constituents
            CAS gold = aCAS.getView(Views.GOLD_VIEW);
            goldCollection.collect(gold, filename);

            uimaPipeline.process(aCAS, filename);

            // Get system constituents
            CAS system = aCAS.getView(Views.SYSTEM_VIEW);
            systemCollection.collect(system, filename);


        }

        evaluate(goldCollection, systemCollection);

    }

    public static void main(String[] args) throws IOException, UIMAException, ConfigurationException {
        // Construction commandline options, toInt values, and validate what was supplied
        Options cliOptions = getCLIOptions();
        CommandLine cliValues = getCLIValues(cliOptions, args);

        //get Resource Specifier from XML file
        URL resource = FamilyHistoryVitalStatusEvaluator.class.getClassLoader().getResource
                ("edu/umn/biomedicus/familyHistory/familyHistoryConstituentsCollectionReader.xml");
        XMLInputSource in = new XMLInputSource(resource);
        ResourceSpecifier specifier =
                UIMAFramework.getXMLParser().parseResourceSpecifier(in);

        CollectionReader cReader =
                UIMAFramework.produceCollectionReader(specifier);

        // run
        FamilyHistoryVitalStatusEvaluator fh = new FamilyHistoryVitalStatusEvaluator(cReader);

    }

    private static Options getCLIOptions() {
        // create the Options
        Options options = new Options();
        options.addOption("i", "input", true, "Input directory containing text files (*.txt).");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args) {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options) {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }

    public void evaluate(GoldAnnotationCollection gold,
                         SystemAnnotationCollection system) {
        System.out.println("\nEvaluating family vital status identificiation...");
        HashMap<String, String> goldFamily = gold.getVitalStatus();
        HashMap<String, String> sysFamily = system.getVitalStatus();

        ContingencyTable ct = new ContingencyTable(goldFamily, sysFamily);

        System.out.println("\t True  Positives: " + ct.get(ContingencyTable.TP));
        System.out.println("\t False Positives: " + ct.get(ContingencyTable.FP));
        System.out.println("\t False Negatives: " + ct.get(ContingencyTable.FN));
        System.out.println("\t Precision: " + ct.getPrecision());
        System.out.println("\t Recall: " + ct.getRecall());
        System.out.println("\t F-Score: " + ct.getFScore());
    }
}