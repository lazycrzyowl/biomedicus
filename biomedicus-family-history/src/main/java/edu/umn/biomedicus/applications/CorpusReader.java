package edu.umn.biomedicus.applications;

import gate.Annotation;
import gate.AnnotationSet;
import gate.FeatureMap;
import gate.Node;
import gate.corpora.DocumentImpl;
import gate.corpora.DocumentStaxUtils;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cc.mallet.util.CommandOption.List;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.TreeSet;


public class CorpusReader {


    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
    private File[] fileStack;
    private int fileNum = 0;

    public static void main(String[] args)
    {
        File gateFiles = new File("/Users/bill0154/SpiderOak Hive/projects/shfh/mtsamples_consult_gate_xml/allMTS/");
        CorpusReader cr = new CorpusReader(gateFiles);
        while (cr.hasNext())
        {
        	DocumentImpl doc = cr.next();
        	String name = cr.getFileName();        	
        }
    }

    public CorpusReader(File gateFiles)
    {
    	ArrayList<File> files = new ArrayList<>();
        for (File f : gateFiles.listFiles())
        {
        	files.add(f);

            DocumentImpl doc = new DocumentImpl();

            AnnotationSet anset = doc.getAnnotations().get();

            TreeSet<Integer> sentenceBoundaryPoints = new TreeSet<Integer>();
            for (Annotation annotation : anset) {
                Node startNode = annotation.getStartNode();
                Node endNode = annotation.getEndNode();
                if (annotation.getType().equals("FH")) {
                    int start = startNode.getOffset().intValue();
                    int end = endNode.getOffset().intValue();
                    FeatureMap features = annotation.getFeatures();
                }
            }
        }
        File[] allFiles = new File[files.size()];
        fileStack = files.toArray(allFiles);
    }


    public boolean hasNext()
    {
        System.out.println("HAS NEXT BEGINS HERE");
        if (fileNum < fileStack.length - 1)
        {
            return true;
        }
        System.out.println("HAS NEXT ENDS HERE");
        return false;
    }

    public DocumentImpl next()
    {
        System.out.println("GET NEXT BEGINS HERE");
        DocumentImpl doc = new DocumentImpl(); // The Gate Document
        try
        {
            InputStream input = new FileInputStream(fileStack[fileNum]);
            XMLStreamReader xsr = inputFactory.createXMLStreamReader(input);
            xsr.next();
            DocumentStaxUtils.readGateXmlDocument(xsr, doc);
        } catch (FileNotFoundException fnfe)
        {
            fnfe.printStackTrace();
            throw new RuntimeException("Unable to find specified Gate file.");
        } catch (XMLStreamException xse)
        {
            xse.printStackTrace();
            throw new RuntimeException("Unable to read specified Gate file.");
        }
        fileNum++;

        return doc;
    }

    public String getFileName()
    {
        return fileStack[fileNum].getName();
    }
}


