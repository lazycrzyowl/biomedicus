/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.applications.evaluation;

import edu.umn.biomedicus.type.FamilyHistoryConstituent;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class SystemAnnotationCollection {

    HashMap<String, HashMap<String, String>> systemAccumulator;
    TypeSystem ts;
    Type familyHistoryConstituentType;
    Feature labelFeature;
    CAS systemCAS;

    public SystemAnnotationCollection(CAS systemCAS)
    {
        this.systemCAS = systemCAS;
        systemAccumulator = new HashMap<>();
        systemAccumulator.put("family", new HashMap<String, String>());
        systemAccumulator.put("observation", new HashMap<String, String>());
        systemAccumulator.put("negation", new HashMap<String, String>());
        systemAccumulator.put("vitalStatus", new HashMap<String, String>());
        systemAccumulator.put("indicator", new HashMap<String, String>());
        systemAccumulator.put("certainty", new HashMap<String, String>());
        systemAccumulator.put("ageDeath", new HashMap<String, String>());
        systemAccumulator.put("ageDiag", new HashMap<String, String>());

        ts = systemCAS.getTypeSystem();
        familyHistoryConstituentType = ts.getType(FamilyHistoryConstituent.class.getCanonicalName());
        labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
    }

    public void collect(CAS system, String filename)
    {

        for (AnnotationFS sysAnnot : system.getAnnotationIndex())
        {
            Type annotType = sysAnnot.getType();
            if (annotType == familyHistoryConstituentType)
            {
                String label = sysAnnot.getStringValue(labelFeature);
                String key = new StringBuilder(filename)
                        .append(':')
                        .append(sysAnnot.getBegin())
                        .append(':')
                        .append(sysAnnot.getEnd())
                        .toString();
                add(label, key, sysAnnot.getCoveredText());
            }
        }
    }

    private void add(String label, String key, String annot)
    {

        switch (label) {
            case "certainty":
                systemAccumulator.get("certainty").put(key, annot);
                break;
            case "negation":
                systemAccumulator.get("negation").put(key, annot);
                break;
            case "SubObservation":
            case "observation":
                systemAccumulator.get("observation").put(key, annot);
                break;
            case "SideFamily":
            case "family":
                systemAccumulator.get("family").put(key, annot);
                break;
            case "vitalStatus":
                systemAccumulator.get("vitalStatus").put(key,annot);
                break;
            case "ageDeath":
                systemAccumulator.get("ageDeath").put(key, annot);
                break;
            case "ageDiag":
                systemAccumulator.get("ageDiag").put(key, annot);
                break;
            case "SubjQuantity":
                break; // no clue
            case "ObsOtherMod":
                break; // no clue
            case "ObsRelevance":
                break; // no clue
            case "ObsStrength":
                break; // no clue
            case "ObsTemporal":
                break; // no clue
            case "OtherStatement":
                break; // no clue
            default:
                break;
        }

    }

    public HashMap<String, String> getFamily()
    {
        return systemAccumulator.get("family");
    }
    public HashMap<String, String> getObservations() { return systemAccumulator.get("observation"); }
    public HashMap<String, String> getNegation() { return systemAccumulator.get("negation"); }
    public HashMap<String, String> getVitalStatus() { return systemAccumulator.get("vitalStatus"); }
    public HashMap<String, String> getAgeDeath() { return systemAccumulator.get("ageDeath"); }
    public HashMap<String, String> getAgeDiag() { return systemAccumulator.get("ageDiag"); }
}
