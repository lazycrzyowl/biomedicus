package edu.umn.biomedicus.applications.pipelines;

import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.uimafit.factory.TypeSystemDescriptionFactory;

import java.io.File;
import java.io.IOException;

/**
 * A sample pipeline for testing the AHC2MSI connection.
 * This test does not make use of activeMQ.
 */
public class SerialPipeline {

  /** The pipeline created to process text. */
  private AnalysisEngine analysisEngine = null;

  private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<TypeSystemDescription>();
  static {
    TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription(
            "./descriptors/type/TypeSystem.xml");
    TYPE_SYSTEM_DESCRIPTION.set(tsd);
  }

  /**
   * Creates a UIMA analysis engine.
   */
  public SerialPipeline() throws InvalidXMLException, IOException, ResourceInitializationException {
    System.out.println("Sample Biomedicus application - serial - UMN NLP Group");
    System.out.println("============================================");

    System.out.println("Accessing analysis engine descriptor file");
    File descriptorFile = new File("./descriptors/serial/analysisEngine.xml");
    XMLInputSource descriptorSource = new XMLInputSource(descriptorFile);

    // TypeSystemDescription typeSystem = TypeSystemDescriptionFactory
    // .createTypeSystemDescription("./descriptors/annotator/icuWhitespaceTokenAE.xml");
    // TypePriorities typePriorities = TypePrioritiesFactory
    // .createTypePriorities(TokenAnnotation.class);
    // AnalysisEngineFactory.createPrimitiveDescription(IcuWhitespaceTokenAE.class, typeSystem,
    // typePriorities);

    System.out.println("Creating analysis engine");
    ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(
            descriptorSource);
    analysisEngine = UIMAFramework.produceAnalysisEngine(specifier);
  }

  /**
   * Uses the UIMA analysis engine to process the provided document text.
   */
  public JCas process(String text) throws UIMAException {
    JCas jcas = analysisEngine.newJCas();
    jcas.setDocumentText(text);
    analysisEngine.process(jcas);
    return jcas;
  }
}