/* 
 Copyright 2011 University of Minnesota 
 
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.applications;

import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.cas.CAS;
import org.apache.uima.util.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Family History Annotation Engine Main() class
 */
public class FamilyHistoryParallelApp {

    /** The pipeline created to process text. */
    private static UimaAsynchronousEngine familyHistoryAE = null;

    private static String text = "The Dr. prescribed tamoxifin b.i.d. to the most recent patient. There should be many sentences to annotate, and this is yet another example.";

    public static void main(String[] args) throws Exception {
        Logger logger = UIMAFramework.getLogger();

        // Writer out = new OutputStreamWriter(new FileOutputStream(
        // "/workspace/biomedicus/descriptors/annotator/token/icuWhitespaceTokenAE.xml"));
        // token.toXML(out);
        // out.flush();
        // out.close();

        // creating UIMA analysis engine
        familyHistoryAE = new BaseUIMAAsynchronousEngine_impl();
        UimaAsBaseCallbackListener asyncListener = new edu.umn.biomedicus.applications.pipelines.FamilyHistoryCallbackListener(familyHistoryAE);
        familyHistoryAE.addStatusCallbackListener(asyncListener);

        // preparing map for use in deploying services
        Map<String, Object> deployCtx = new HashMap<String, Object>();
        deployCtx.put(UimaAsynchronousEngine.DD2SpringXsltFilePath, System.getenv("UIMA_HOME")
                + "/bin/dd2spring.xsl");
        deployCtx.put(UimaAsynchronousEngine.SaxonClasspath, "file:" + System.getenv("UIMA_HOME")
                + "/saxon/saxon8.jar");

        System.out.println("Deploying Biomedicus services");
        familyHistoryAE.deploy("./descriptors/annotators/token/whitespaceTokenAEDeploy.xml", deployCtx);
//        familyHistoryAE.deploy("./descriptors/annotators/sentence/sentenceDeploy.xml", deployCtx);
//        familyHistoryAE.deploy("./descriptors/annotators/annotatorB/documentAEDeploy.xml", deployCtx);
//        familyHistoryAE.deploy("./descriptors/annotators/annotatorC/conceptDeploy.xml", deployCtx);
//        familyHistoryAE.deploy("./descriptors/annotators/annotatorD/clinicalAEDeploy.xml", deployCtx);

        // creating aggregate analysis engine
        System.out.println("Deploying analysis engine");
        familyHistoryAE.deploy("./descriptors/aggregates/deploy.xml", deployCtx);

        // preparing map for use in a UIMA client for submitting text to process
        System.out.println("Initialising UIMA client");
        deployCtx.put(UimaAsynchronousEngine.ServerUri, "tcp://localhost:61616");
        deployCtx.put(UimaAsynchronousEngine.Endpoint, "FamilyHistoryAnnotatorQueue");
        familyHistoryAE.initialize(deployCtx);

        // Get memory object (CAS) and input target text
        CAS cas = familyHistoryAE.getCAS();

        try {
            cas = familyHistoryAE.getCAS();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        cas.setDocumentText(text);
        familyHistoryAE.sendCAS(cas);
    }
}
