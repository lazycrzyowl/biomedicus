/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.applications.evaluation;

import edu.umn.biomedicus.type.FamilyHistoryConstituent;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class GoldAnnotationCollection {

    HashMap<String, HashMap<String, String>> goldAccumulator;
    TypeSystem ts;
    Type familyHistoryConstituentType;
    Feature labelFeature;
    CAS goldCAS;

    public GoldAnnotationCollection(CAS goldCAS)
    {
        this.goldCAS = goldCAS;
        goldAccumulator = new HashMap<>();
        goldAccumulator.put("family", new HashMap<String, String>());
        goldAccumulator.put("observation", new HashMap<String, String>());
        goldAccumulator.put("negation", new HashMap<String, String>());
        goldAccumulator.put("vitalStatus", new HashMap<String, String>());
        goldAccumulator.put("indicator", new HashMap<String, String>());
        goldAccumulator.put("certainty", new HashMap<String, String>());
        goldAccumulator.put("AgeDeath", new HashMap<String, String>());
        goldAccumulator.put("AgeDiag", new HashMap<String, String>());

        ts = goldCAS.getTypeSystem();
        familyHistoryConstituentType = ts.getType(FamilyHistoryConstituent.class.getCanonicalName());
        labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
    }

    public void collect(CAS goldCAS, String filename)
    {

        for (AnnotationFS goldAnnot : goldCAS.getAnnotationIndex())
        {
            Type annotType = goldAnnot.getType();
            if (annotType == familyHistoryConstituentType)
            {
                String label = goldAnnot.getStringValue(labelFeature);
                String key = new StringBuilder(filename)
                        .append(':')
                        .append(goldAnnot.getBegin())
                        .append(":")
                        .append(goldAnnot.getEnd())
                        .toString();

                add(label, key, goldAnnot.getCoveredText());
            }
        }
    }

    private void add(String label, String key, String text)
    {

        switch (label) {
            case "ObsCertainty":
                goldAccumulator.get("certainty").put(key, text);
                break;
            case "ObsNegation":
                goldAccumulator.get("negation").put(key, text);
                break;
            case "OtherStatement":
            case "SubObservation":
            case "Observation":
                goldAccumulator.get("observation").put(key, text);
                break;
            case "SideFamily":
            case "FM":
                goldAccumulator.get("family").put(key, text);
                break;
            case "SubjVitalStatus":
                goldAccumulator.get("vitalStatus").put(key, text);
                break;
            case "AgeDeath":
                goldAccumulator.get("AgeDeath").put(key, text);
                break;
            case "AgeDiag":
                goldAccumulator.get("AgeDiag").put(key, text);
            case "SubjQuantity":
                break; // no clue
            case "ObsOtherMod":
                break; // no clue
            case "ObsRelevance":
                break; // no clue
            case "ObsStrength":
                break; // no clue
            case "ObsTemporal":
                break; // no clue
            default:
                break;
        }
    }

    public HashMap<String, String> getFamily()
    {
        return goldAccumulator.get("family");
    }
    public HashMap<String, String> getObservations() { return goldAccumulator.get("observation"); }
    public HashMap<String, String> getNegation() { return goldAccumulator.get("negation"); }
    public HashMap<String, String> getVitalStatus() { return goldAccumulator.get("vitalStatus"); }
    public HashMap<String, String> getAgeDeath() { return goldAccumulator.get("AgeDeath"); }
    public HashMap<String, String> getAgeDiag() { return goldAccumulator.get("AgeDiag"); }
}
