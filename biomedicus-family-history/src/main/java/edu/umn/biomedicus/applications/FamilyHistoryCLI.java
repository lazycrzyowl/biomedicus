/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.applications;

import edu.umn.biomedicus.applications.pipelines.FamilyHistoryCLIPipeline;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.familyhistory.Predication;
import edu.umn.biomedicus.type.FamilyHistory;
import org.apache.commons.cli.*;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.uimafit.util.JCasUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Family History Annotation Application.
 */
public class FamilyHistoryCLI {

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(FamilyHistoryCLI.class);
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static String suffix;
    @NotNull
    private Type familyHistoryConstituentType;
    @NotNull
    private Type familyHistoryType;

    public FamilyHistoryCLI() throws IOException, UIMAException
    {

        // Get pipeline
        FamilyHistoryCLIPipeline uimaPipeline = new FamilyHistoryCLIPipeline();

        // command loop
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        String line;
        System.out.print("> ");
        while ((line = input.readLine()) != null) {
            if (line == null || line.trim().length() == 0)
            {
                System.out.print("> ");
                continue;
            }
            CAS output = uimaPipeline.process("command-line", line);
            CAS systemView = CASUtil.getSystemView(output);
            TypeSystem ts = output.getTypeSystem();
            familyHistoryConstituentType = ts.getType("edu.umn.biomedicus.type.FamilyHistoryConstituent");
            familyHistoryType = ts.getType("edu.umn.biomedicus.type.FamilyHistory");
            Type tokenType = ts.getType("edu.umn.biomedicus.type.Token");
            Type chunkType = ts.getType("edu.umn.biomedicus.type.Chunk");
            Feature labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");

            for (AnnotationFS annot : systemView.getAnnotationIndex(tokenType))
            {
                if (!annot.getType().equals(familyHistoryConstituentType))
                {
                    System.out.println(String.format("Token : %s", annot.getCoveredText()));
                }
            }
            System.out.println();

            for (AnnotationFS annot : systemView.getAnnotationIndex(chunkType))
            {
                System.out.println(String.format("Chunk : %s", annot.getCoveredText()));
            }
            System.out.println();

            for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
            {
                System.out.println(String.format("FamilyHistoryComponent : %1$-12s : %2$-50s",
                        annot.getStringValue(labelFeature), annot.getCoveredText()));
            }

            System.out.println();

            JCas jcas = systemView.getJCas().getView("SystemView");
            for (FamilyHistory pred : JCasUtil.select(jcas, FamilyHistory.class))
            {
                //Predication predWrap = new Predication(pred);
                System.out.println(pred.getFamilyConstituent() + "<->" + pred.getObservationConstituent());
                System.out.print("\tAttributes: ");
                if (pred.getIndicator() != null) System.out.print(String.format("Indicator: %s ", pred.getIndicator()));
                if (pred.getSideOfFamily() != null) System.out.print(String.format("Side of family: %s ", pred.getSideOfFamily()));
                if (pred.getCertainty() != null) System.out.print(String.format("Certainty: %s ", pred.getCertainty()));
                if (pred.getNegated() == true) System.out.print(String.format("Negated  "));
                if (pred.getVitalStatus() != null && !pred.getVitalStatus().equals(""))
                    System.out.print(String.format("Vital Status %s ", pred.getVitalStatus()));
                System.out.println("\n");
            }


            System.out.println();
            System.out.print("> ");
        }
    }

    public static void main(String[] args) throws IOException, UIMAException, ConfigurationException
    {
        // Get general properties
        Configuration _config = new Configuration();
        System.out.println(_config.getString("biomedicus_cli_banner"));
        System.setProperty("metamap.path", "/Users/bill0154/Applications/public_mm/bin/metamap13");
        // Start logging
        logger.setLevel(Level.OFF);


        // run
        FamilyHistoryCLI fh = new FamilyHistoryCLI();
        
    }

    private static Options getCLIOptions()
    {
        // create the Options
        Options options = new Options();
        options.addOption("i", "input", true, "Input directory containing text files (*.txt).");
        options.addOption("d", "destination", true, "Destination directory to output xmi files.");
        options.addOption("s", "suffix", true, "Suffix/extension of input files to process (default=.txt)");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args)
    {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try
        {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e)
        {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }
}