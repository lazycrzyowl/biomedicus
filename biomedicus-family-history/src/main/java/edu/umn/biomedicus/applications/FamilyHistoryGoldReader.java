package edu.umn.biomedicus.applications;

import edu.umn.biomedicus.applications.pipelines.FamilyHistoryGoldReaderPipeline;
import org.apache.commons.cli.*;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.util.FileUtils;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Properties;

/**
 * Family History Annotation Application.
 */
public class FamilyHistoryGoldReader {

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(FamilyHistoryGoldReader.class);
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static String suffix;

    public FamilyHistoryGoldReader(Path outputDirectory, Iterator<Path> files) throws IOException, UIMAException
    {

        // Get pipeline
        FamilyHistoryGoldReaderPipeline uimaPipeline = new FamilyHistoryGoldReaderPipeline();
        double start = System.currentTimeMillis();
        int documentCount = 0;

        while (files.hasNext())
        {
            documentCount++;
            Path currentFile = files.next();
            String documentText = FileUtils.file2String(currentFile.toFile());

            // skip empty documents
            if (documentText == null || documentText.trim().equals(""))
            {
                continue;
            }

//            if (!currentFile.endsWith("97_1147.txt")) continue;

            // run the sample document through the pipeline
            long before = System.currentTimeMillis();
            CAS output = uimaPipeline.process(currentFile.toString(), documentText);

            FamilyHistoryResultAnalyzer.evaluate(output);
            long after = System.currentTimeMillis();
            CAS metadata = output.getView("MetaData");
            String outputName = new File(metadata.getDocumentText()).getName();
            File dest = new File(outputDirectory.toString(), outputName);
            try
            {
                FileOutputStream os = new FileOutputStream(dest);
                XmiCasSerializer.serialize(output, os);
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            } catch (SAXException e)
            {
                e.printStackTrace();
            }
        }

        double end = System.currentTimeMillis();
        double totalTime = (end - start)/1000;
        double timePerDocument = totalTime/Double.valueOf(documentCount);
        System.out.println("Total number of documents = " + documentCount);
        System.out.println("Total processing time = " + totalTime);
        System.out.println("Processing time per document = " + timePerDocument);

    }

    public static void main(String[] args) throws IOException, UIMAException, ConfigurationException
    {
        // Get general properties
        Configuration _config = new Configuration();
        System.out.println(_config.getString("biomedicus_cli_banner"));

        // Start logging
        logger.setLevel(Level.OFF);

        // Construction commandline options, get values, and validate what was supplied
        Options cliOptions = getCLIOptions();
        CommandLine cliValues = getCLIValues(cliOptions, args);
        String suffix = ".txt";

        if (cliValues.hasOption("input") && cliValues.hasOption("destination"))
        {
            // set path for input and output
            inputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("input"));
            //outputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("destination"));

            // Use the configuration setting for output dir if one is not specified at cl
            outputDirectory = cliValues.getOptionValue("destination") != null ?
                    FileSystems.getDefault().getPath(cliValues.getOptionValue("destination")) :
                    FileSystems.getDefault().getPath(_config.getString("output_dir"));
            // get suffix, or set to default (.txt)
            String suffix_opt = cliValues.getOptionValue("suffix");
            if (suffix_opt != null && !suffix_opt.equals(""))
            {
                suffix = suffix_opt;
            }

            // display provided options
            System.out.println("Setting input directory to: " + inputDirectory.toString());
            System.out.println("Setting input file suffix to: " + suffix);
            System.out.println("Setting destination directory to: " + outputDirectory.toString());

        } else
        {
            printHelp("FamilyHistoryGoldReader", cliOptions);
        }

        // Create a DirectoryStream which accepts only filenames ending with specified suffix
        DirectoryStream<Path> ds = Files.newDirectoryStream(inputDirectory, "*" + suffix);

        // constructs a class to create and run a UIMA pipeline
        String od = _config.getString("output_dir");
        
    }

    private static Options getCLIOptions()
    {
        // create the Options
        Options options = new Options();
        options.addOption("i", "input", true, "Input directory containing text files (*.txt).");
        options.addOption("d", "destination", true, "Destination directory to output xmi files.");
        options.addOption("s", "suffix", true, "Suffix/extension of input files to process (default=.txt)");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args)
    {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try
        {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e)
        {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }
}