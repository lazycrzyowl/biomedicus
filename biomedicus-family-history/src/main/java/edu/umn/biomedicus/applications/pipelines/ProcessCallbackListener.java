package edu.umn.biomedicus.applications.pipelines;

import org.apache.uima.aae.client.UimaAsBaseCallbackListener;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.EntityProcessStatus;

import java.util.List;

public class ProcessCallbackListener extends UimaAsBaseCallbackListener {
  // prepares a listener for when the analysis engine is complete

  UimaAsynchronousEngine uimaAsEngine = null;

  long before = -1;

  public ProcessCallbackListener(UimaAsynchronousEngine uimaAsEngine) {
    this.uimaAsEngine = uimaAsEngine;
    this.before = System.currentTimeMillis();
  }

  /**
   * This will be called once the text is processed.
   */
  @Override
  public void entityProcessComplete(CAS output, EntityProcessStatus aStatus) {
    // record the time that this was complete
    long after = System.currentTimeMillis();

    // check exceptions
    if (aStatus != null && aStatus.isException()) {
      List<Exception> exceptions = aStatus.getExceptions();
      for (int i = 0; i < exceptions.size(); i++) {
        ((Throwable) exceptions.get(i)).printStackTrace();
      }
      try {
        uimaAsEngine.stop();
      } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      return;
    }

    // display the time spent processing the text
    System.out.println("Time spent in pipeline: " + (after - before));

    // confirm that the expected annotations were added to the CAS
    System.out.println("Confirming what was added...");
    FSIterator<AnnotationFS> annotationsIterator = output.getAnnotationIndex().iterator();
    while (annotationsIterator.hasNext()) {
      AnnotationFS annotation = annotationsIterator.next();
      System.out.println("  Found: " + annotation.getClass().getName() + ", "
              + annotation.getType().toString() + ", " + annotation.getCoveredText());
    }

    try {
      uimaAsEngine.stop();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
