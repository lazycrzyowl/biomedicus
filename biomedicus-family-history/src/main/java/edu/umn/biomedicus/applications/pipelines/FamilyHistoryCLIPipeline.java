/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.applications.pipelines;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import edu.umn.biomedicus.type.Sentence;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.uimafit.factory.TypeSystemDescriptionFactory;

import java.io.IOException;
import java.net.URL;

/**
 * Family History Serial pipeline
 */
public class FamilyHistoryCLIPipeline {

    private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<>();

    static
    {
        TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription(
                "./descriptors/types/TypeSystem.xml");
        TYPE_SYSTEM_DESCRIPTION.set(tsd);
    }

    private TypeSystemDescription tsd;
    private TypeSystem ts;

    /**
     * The pipeline created to process text.
     */
    private AnalysisEngine analysisEngine;
    private AnalysisEngine tokenizer;
    private AnalysisEngine segmenter;

    public static String getDescriptorFile()
    {
        URL descriptorFileUrl = FamilyHistoryCLIPipeline.class.getClassLoader().getResource(
                "edu/umn/biomedicus/familyhistory/familyHistoryCLIAnalysisEngine.xml");
        return descriptorFileUrl.toString();
    }

    /**
     * Creates a UIMA analysis engine.
     */
    public FamilyHistoryCLIPipeline() throws InvalidXMLException, IOException, ResourceInitializationException
    {
        String resourceName = "edu/umn/biomedicus/familyhistory/familyHistoryCLIAnalysisEngine.xml";
        URL descriptorFileUrl = Thread.currentThread().getContextClassLoader().getResource(resourceName);
        //getClass().getClassLoader()
        //        .getResource("edu/umn/biomedicus/familyhistory/familyHistoryCLIAnalysisEngine.xml");
        System.out.println("Loaded descriptorFileURL: " + descriptorFileUrl);
        String fileDescriptorString = descriptorFileUrl.getFile();
        //File descriptorFile = new File(descriptorFileUrl);
        XMLInputSource descriptorSource = new XMLInputSource(descriptorFileUrl);

        System.out.println("Creating analysis engine");
        ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(
                descriptorSource);
        analysisEngine = UIMAFramework.produceAnalysisEngine(specifier);
        ts = analysisEngine.newCAS().getTypeSystem();
        tokenizer = new Tokenizer.Builder().getAE();
        //segmenter = new SentenceSegmenter.Builder().getAE();

    }

    public TypeSystem getTypeSystem()
    {
        return ts;
    }

    /**
     * Uses the UIMA analysis engine to process the provided document text.
     */
    public CAS process(String documentPath, String text) throws UIMAException
    {
        CAS aCAS = analysisEngine.newCAS();
        aCAS.setDocumentText(text);
        CAS mdView = aCAS.createView(Views.METADATA_VIEW);
        CAS sysView = aCAS.createView(Views.SYSTEM_VIEW);
        Type sentenceType = TypeUtil.getType(aCAS, Sentence.class);
        mdView.setSofaDataString(documentPath, "text/plain");
        sysView.setDocumentText(text);
//        AnnotationFS firstSent = sysView.createAnnotation(sentenceType, 0, text.length());
//        AnnotationUtils.setAnnotationFeatureStringValue(firstSent, "clinicalClass", "familyhistory");
//        sysView.addFsToIndexes(firstSent);
        analysisEngine.typeSystemInit(aCAS.getTypeSystem());

        tokenizer.process(aCAS);
//      segmenter.process(aCAS);

        for (AnnotationFS sent : AnnotationUtils.getAnnotations(aCAS, Sentence.class))
        {
            AnnotationUtils.setAnnotationFeatureStringValue(sent, "label", "familyhistory");
        }

        analysisEngine.process(aCAS);
        return aCAS;
    }

    public CAS getCAS() throws ResourceInitializationException
    {
        return analysisEngine.newCAS();
    }
}