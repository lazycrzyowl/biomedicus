/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus;

import edu.umn.biomedicus.collection.reader.GateReader;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.FileUtils;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

/**
 * the relation annotations include:

 AgeDeathFam
 CertainObs
 FamObsArg
 NegObs
 ObsFamDeath
 OtherModObs
 QuanSubj
 RelevanceObs
 SideAttrSubj
 SideObsArg
 StrengthObs
 SubObs
 TemporalObs
 VitalStatusSubj

 the attribute annotations include:

 CertaintyDegree
 ObservationType
 SideFamilyType
 TimeSpecificity

 The type annotations include:

 AgeDeath
 FM
 ObsCertainty
 ObsNegation
 ObsOtherMod
 ObsRelevance
 ObsStrength
 ObsTemporal
 Observation
 OtherStatement
 SideFamily
 SubObservation
 SubjQuantity
 SubjVitalStatus

 */
public class BratToXmi {
    public static String PARAM_INPUT_DIR = "inputDir";
    @NotNull
    TypeSystem typeSystem;
    @NotNull
    private Type paragraphType;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Feature sentenceClinicalClassFeature;
    @NotNull
    private final String corpusPath;
    @NotNull
    GateReader reader;
    @NotNull
    private CAS aCAS;
    @NotNull
    private CAS goldView;
    @NotNull
    private CAS systemView;
    @NotNull
    private HashSet<String[]> familyAnnotationTypes;
    @NotNull
    private HashMap<String,String> familyConstituents;
    @NotNull
    private HashMap<String,String> observationConstituents;
    @NotNull
    private HashMap<String,String> vitalConstituents;
    @NotNull
    private HashMap<String,String> certaintyConstituents;


    public BratToXmi(@NotNull String corpusPath) throws IOException, InvalidXMLException, ResourceInitializationException
    {
        this.corpusPath = corpusPath;
        AnalysisEngine ae = getAE();
        aCAS = ae.newCAS();
        goldView = aCAS.createView("GoldView");
        systemView = aCAS.createView("SystemView");

        TypeSystem typeSystem = aCAS.getTypeSystem();
        typeSystemInit(typeSystem);
        familyAnnotationTypes = new HashSet(Arrays.asList(new String[]{"Family",
                "FM", "SubjVitalStatus", "Observation"}));
        familyConstituents = new HashMap<>();
        observationConstituents = new HashMap<>();
        vitalConstituents = new HashMap<>();
        certaintyConstituents = new HashMap<>();
    }

    public void setDocumentText(String text)
    {
        systemView.setDocumentText(text);
        goldView.setDocumentText(text);
    }

    private AnalysisEngine getAE() throws IOException, InvalidXMLException, ResourceInitializationException
    {
        String resource = "edu/umn/biomedicus/collection/reader/gateCollectionReader.xml";
        URL resourceURL = BratToXmi.class.getClassLoader().getResource(resource);
        XMLInputSource in = new XMLInputSource(resourceURL);
        ResourceSpecifier specifier =
                UIMAFramework.getXMLParser().parseResourceSpecifier(in);

        //create AE here
        return  UIMAFramework.produceAnalysisEngine(specifier);
    }

    private static File[] getFileList(String path)
    {
        Path filePath = Paths.get(path);
        return filePath.toFile().listFiles(
                new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name)
                    {
                        return name.endsWith(".txt");
                    }
                }
        );
    }

    @NotNull
    public String getCorpusPath()
    {
        return corpusPath;
    }

    public static void main(String[] args) throws Exception
    {

        for (File file : getFileList(args[0]))
        {
            BratToXmi converter = new BratToXmi(args[0]);
            CharSequence oldSuffix = ".txt";
            CharSequence newSuffix = ".ann";
            String annotationFile = file.toString().replace(oldSuffix, newSuffix);
            String documentText = FileUtils.file2String(file);
            converter.setDocumentText(documentText);

            String[] annotations = FileUtils.file2String(new File(annotationFile)).split("\n");

            converter.addSentences(documentText);
            converter.addBratAnnotations(annotations);

            String outputDir = new File(converter.getCorpusPath(), "../familyHistoryCorpus").toString();
            File destDir = new File(file.getParentFile().getAbsolutePath(), "../familyHistoryCorpus");
            CharSequence xmiSuffix = ".xmi";
            String destFile = file.getName().replace(oldSuffix, xmiSuffix);
            File dest = new File(destDir.toString(), destFile);
            System.out.println("Destination: " + dest);

            try
            {
                FileOutputStream os = new FileOutputStream(dest);
                XmiCasSerializer.serialize(converter.aCAS, os);
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
        }
    }

    public void addSentences(String documentText)
    {
        String[] lines = documentText.split("\\n");
        int currentIdx = 0;
        for (String line : lines)
        {
            String[] dataRow = line.split("\\|");
            int begin = documentText.indexOf(dataRow[5], currentIdx);
            int end = begin + dataRow[5].length();
            AnnotationFS goldSent = goldView.createAnnotation(sentenceType, begin, end);
            AnnotationFS sysSent = systemView.createAnnotation(sentenceType, begin, end);
            goldView.addFsToIndexes(goldSent);
            systemView.addFsToIndexes(sysSent);
            currentIdx = end;
        }
    }

    private void addBratAnnotations(String[] annotations)
    {
        BratAnnotations brat = new BratAnnotations(goldView);
        for (String annotation : annotations)
        {
            String[] row = annotation.split("[\\t*|\\s*]", 5);
            dispatch(goldView, brat, row);
        }
    }

    private void dispatch(CAS goldView, BratAnnotations brat, String[] row)
    {
        char c = row[0].charAt(0);
        switch (c) {
            case 'A':
                brat.addAttributeAnnotation(goldView, row);
                break;
            case 'R':
                brat.getRelationAnnotation(goldView, row);
                break;
            case 'T':
                brat.addTypeAnnotation(goldView, row);
                break;
            default:
        }
    }

    public String getConstituentType(String level)
    {
        switch (level) {
            case "FM":
                return "family";
            case "SideFamily":
                return "family";
            case "observation":
                return "observation";
            case "indicator":
                return "indicator";
            default:
                return null;
        }
    }

    public void typeSystemInit(TypeSystem typeSystem)
    {
        this.typeSystem = typeSystem;
        paragraphType = typeSystem.getType("edu.umn.biomedicus.type.Paragraph");
        sentenceType = typeSystem.getType("edu.umn.biomedicus.type.Sentence");
        sentenceClinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
    }
}
