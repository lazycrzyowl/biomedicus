/*
 * Copyright University of Minnesota 2014
 */
package edu.umn.biomedicus.relations;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.methods.LexiconUtil;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;
import org.linkgrammar.LGConfig;
import org.uimafit.component.CasAnnotator_ImplBase;
import relex.Document;
import relex.ParseStats;
import relex.ParsedSentence;
import relex.Sentence;
import relex.algs.SentenceAlgorithmApplier;
import relex.anaphora.Antecedents;
import relex.anaphora.Hobbs;
import relex.chunk.ChunkRanker;
import relex.chunk.LexChunk;
import relex.chunk.RelationChunker;
import relex.concurrent.RelexContext;
import relex.corpus.DocSplitter;
import relex.corpus.DocSplitterFactory;
import relex.feature.FeatureNode;
import relex.morphy.Morphy;
import relex.morphy.MorphyFactory;
import relex.output.OpenCogScheme;
import relex.parser.LGParser;
import relex.parser.LocalLGParser;
import relex.parser.RemoteLGParser;
import relex.stats.SimpleTruthValue;
import relex.stats.TruthValue;
import relex.tree.PhraseMarkup;
import relex.tree.PhraseTree;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

// import relex.corpus.QuotesParensSentenceDetector;

/**
 * The RelationExtractor class provides the central processing point for parsing sentences and extracting dependency
 * relationships from them.  The main() proceedure is usable as a stand-alone document analyzer; it supports several
 * flags modifying the displayed output.
 * <p/>
 * The primary interface is the processSentence() method, which accepts one sentence at a time, parses it, and extracts
 * relationships from it. This method is stateful: it also performs anaphora resolution.
 */
public class RelexRelationAE extends CasAnnotator_ImplBase {
    public static final int verbosity = 1;

    public static final int DEFAULT_MAX_PARSES = 4;
    public static final int DEFAULT_MAX_SENTENCE_LENGTH = 1024;
    public static final int DEFAULT_MAX_PARSE_SECONDS = 30;
    public static final int DEFAULT_MAX_PARSE_COST = 1000;
    private static final String SECTION_HEADING_FEATURE_NAME = "heading";
    private static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
    private static final String SECTION_LEVEL_FEATURE_NAME = "level";
    private static final int SECTION_LEVEL = 0;
    private static final int SUBSECTION_LEVEL = 1;


    @NotNull
    public Type sentenceType;
    @NotNull
    public Type sectionType;
    @NotNull
    private Feature sectionLevelFeature;
    @NotNull
    private Feature sentenceClinicalClassFeature;

    @NotNull
    OpenCogScheme opencog;

    public boolean do_tree_markup;
    /**
     * Anaphora resolution
     */
    // XXX these should probably be moved to class Document!
    public Antecedents antecedents;
    public boolean do_anaphora_resolution;
    /**
     * Stanford parser compatibility mode
     */
    public boolean do_stanford;
    /**
     * Penn tagset compatibility mode
     */
    public boolean do_penn_tagging;
    /**
     * Expand preposition markup to two dependencies.
     */
    public boolean do_expand_preps;
    /**
     * Document - holder of sentences
     */
    Document doco;
    /* ---------------------------------------------------------- */
    // Provide some basic timing info
    Long starttime;
    TreeMap<String, Long> sumtime;
    TreeMap<String, Long> cnttime;
    private boolean _is_inited;
    private boolean _use_sock;
    private String _lang;
    private String _dict_path;
    /**
     * The LinkParserClient to be used - this class isn't thread safe!
     */
    private RelexContext relContext;
    /**
     * Syntax processing
     */
    private LGParser parser;
    /**
     * Dependency processing
     */
    private SentenceAlgorithmApplier sentenceAlgorithmApplier;
    /**
     * Penn tree-bank style phrase structure markup.
     */
    private PhraseMarkup phraseMarkup;

	/* ---------------------------------------------------------- */
        /* Control parameters, etc. */
    /**
     * Set the max number of parses. This will NOT reduce processing time; all parses are still computed, but only this
     * many are returned.
     */

	/* ---------------------------------------------------------- */
    private Hobbs hobbs;
    /**
     * Statistics
     */
    private ParseStats stats;
    private Feature sectionHeadingFeature;
    private Feature sectionContentStartFeature;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        _dict_path =  (String) context.getConfigParameterValue("parser.dict.path");
        //"/Users/bill0154/Develop/bitbucket/FamilyHistory/resources/data";

        do_anaphora_resolution = false;
        do_tree_markup = false;

        do_stanford = false;
        do_penn_tagging = false;
        do_expand_preps = false;

        if (_is_inited)
        {
            return;
        }
        _is_inited = true;

        parser = _use_sock ? new RemoteLGParser() : new LocalLGParser();
        if (null != _lang)
        {
            parser.setLanguage(_lang);
        }
        String debug_dict_path = _dict_path;
        parser.setDictPath(_dict_path);
        LGConfig lgConf = parser.getConfig();
        parser.getConfig().setStoreConstituentString(true);
        parser.getConfig().setStoreSense(true);
        Morphy morphy = MorphyFactory.getImplementation(MorphyFactory.DEFAULT_SINGLE_THREAD_IMPLEMENTATION);
        relContext = new RelexContext(parser, morphy);

        sentenceAlgorithmApplier = new SentenceAlgorithmApplier();

        parser.getConfig().setMaxLinkages(DEFAULT_MAX_PARSES);
        parser.getConfig().setMaxCost(DEFAULT_MAX_PARSE_COST);
        parser.getConfig().setMaxParseSeconds(DEFAULT_MAX_PARSE_SECONDS);

        boolean allowSkipped = true; // TODO: change this default to use a parameter grabbed from context
        parser.getConfig().setAllowSkippedWords(allowSkipped);

        // Hobbs-algo stuff.
        phraseMarkup = new PhraseMarkup();
        antecedents = new Antecedents();
        hobbs = new Hobbs(antecedents);

        doco = new Document();

        stats = new ParseStats();
        sumtime = new TreeMap<String, Long>();
        cnttime = new TreeMap<String, Long>();

        do_anaphora_resolution = true;
        do_tree_markup = true;
        do_stanford = true;
        do_penn_tagging = true;
        do_expand_preps = true;

        opencog = new OpenCogScheme();
        opencog.setShowAnaphora(true);

        DocSplitter ds = DocSplitterFactory.create();

    }

    private static void prt_chunks(ArrayList<LexChunk> chunks)
    {
        for (LexChunk ch : chunks)
        {
            System.out.println(ch.toString());
        }
        System.out.println("\n======\n");
    }

    // Punish chunks whose length is other than 3.
    private static void discriminate(ChunkRanker ranker)
    {
        ArrayList<LexChunk> chunks = ranker.getChunks();
        for (LexChunk ch : chunks)
        {
            int sz = ch.size();
            double weight = sz - 3;
            if (weight < 0)
            {
                weight = -weight;
            }
            weight = 1.0 - 0.2 * weight;

            // twiddle the confidence of the chunk
            TruthValue tv = ch.getTruthValue();
            SimpleTruthValue stv = (SimpleTruthValue) tv;
            double confidence = stv.getConfidence();
            confidence *= weight;
            stv.setConfidence(confidence);
        }
    }

    private static String escape(String sent)
    {
        sent = sent.replace("&", "&amp;");
        sent = sent.replace("<", "&lt;");
        sent = sent.replace(">", "&rt;");
        return sent;
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        String sentence = null;
        int maxParses = 1;
        int maxParseSeconds = 6;
        PrintWriter html = null;
        CAS sysCAS = aCAS.getView("System");

        int sentence_count = 0;

        String sectionHeading = "?";
        String subsectionHeading = "?";


        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, sentenceType))
            {
                String clinicalClass = sentenceAnnotation.getStringValue(sentenceClinicalClassFeature);

                // Only look at sentences that are candidates for containing family history statements
                if (clinicalClass == null || !clinicalClass.equals("familyhistory"))
                    continue;


                String sentenceText = sentenceAnnotation.getCoveredText();
                // Identify all the section annotations that cover this sentence. Currently, it can only be
            // an optional section annotation and an optional subsection annotation. In the event there is no
            // covering section annotation detected, the default values for "sectionHeading" and "subsectionnHeading"
            // are set to "?".
            List<AnnotationFS> sectionAnnotationList = AnnotationUtils.getCoveringAnnotations(sysCAS, sectionType, sentenceAnnotation);

//            System.out.println("CURRENT SENTENCE: " + sentenceText);
            Sentence sntc = processSentence("patient possesses " + sentenceText);
            doco.addSentence(sntc);

            sentence_count++;
            stats.bin(sntc);

            int np = sntc.getParses().size();
            if (np > maxParses)
            {
                np = maxParses;
            }

            // chunk ranking stuff
            ChunkRanker ranker = new ChunkRanker();
            double parse_weight = 1.0 / ((double) np);
            double votes = 1.0e-20;
            votes += 1.0;
            votes = 1.0 / votes;
            votes *= parse_weight;

            // Print output
            int numParses = 0;
            for (ParsedSentence parse : sntc.getParses())
            {

                PhraseTree ptree = new PhraseTree(parse.getLeft());
                parse.setPhraseString(ptree.toString());

                RelationChunker relChunker = new RelationChunker();

                //System.out.println(parse.getLeft());
      // **** Phrase String here ***          System.out.println(parse.getPhraseString());
                //System.out.println(parse.getLinkString());
                //System.out.println(parse.getMetaData());
//                System.out.println("Relation Chunks:");
//
//
//                List<LexChunk> relChunkList = relChunker.getChunks();
//
//                for (LexChunk c : relChunkList) {
//                    System.out.println("Relation Chunk: " + c.toString());
//                }

                //System.out.println(RelationChunker);
//                String relations = NLGInputView.printRelations(parse);
//                System.out.println("All sentence relations:");
//                System.out.println(relations);
//
//                System.out.println(parse.getPhraseTree());
//                System.out.println("Dependency relations:\n");
//                String rel = SimpleView.printRelations(parse);


//                LogicView logicView = new LogicView();
//                logicView.loadRules();
//                System.out.println("Relex2Logic output:");
//                System.out.println(logicView.printRelationsNew(parse));



                opencog.setParse(parse);
                //System.out.println(opencog.toString());
//                String ocRelScheme = opencog.relSchemeToString();

                //System.out.println(ocRelScheme);
             // ***** HERE IT IS **** System.out.println(opencog.linkSchemeToString());


//                int chunkListSize = ranker.getChunks().size();
//                if (0 < chunkListSize)
//                {
//                    discriminate(ranker);
////                    System.out.println("\nLexical Chunks:\n" +
////                            ranker.toString());
//                }
//
//                if (do_anaphora_resolution)
//                {
////                    System.out.println("\nAntecedent candidates:\n"
////                            + antecedents.toString());
//                }
//
//                // Print out the stats every now and then.
//                if (sentence_count % 5 == 0)
//                {
////                    System.err.println("\n" + stats.toString());
//                }
            }
        }
//        System.out.println(opencog.printDocument(doco));
    }

    /**
     * Clear out the cache of old sentences.
     * <p/>
     * The Anaphora resolver keeps a list of sentences previously seen, so that anaphora resolution can be done. When
     * starting the parse of a new text, this cache needs to be cleaned out. This is the way to do so.
     */
    public void clear()
    {
        antecedents.clear();
        hobbs = new Hobbs(antecedents);
    }

    public Sentence processSentence(String sentence)
    {
        starttime = System.currentTimeMillis();

        Sentence sntc = null;
        try
        {
            if (verbosity > 0)
            {
                starttime = System.currentTimeMillis();
            }
            sntc = parseSentence(sentence);
            if (verbosity > 0)
            {
                reportTime("Link-parsing: ");
            }

            for (ParsedSentence parse : sntc.getParses())
            {
                if (do_expand_preps)
                {
                    parse.getLeft().set("expand-preps", new FeatureNode("T"));
                }

                // The actual relation extraction is done here.
                sentenceAlgorithmApplier.applyAlgs(parse, relContext);
                if (do_stanford)
                {
                    sentenceAlgorithmApplier.extractStanford(parse, relContext);
                }
                if (do_penn_tagging)
                {
                    sentenceAlgorithmApplier.pennTag(parse, relContext);
                }

                // Also do a Penn tree-bank style phrase structure markup.
                if (do_tree_markup)
                {
                    phraseMarkup.markup(parse);

                    // Repair the entity-mangled tree-bank string.
                    PhraseTree pt = new PhraseTree(parse.getLeft());
                    parse.setPhraseString(pt.toString());
                }

            }

            // Assign a simple parse-ranking score, based on LinkGrammar data.
            sntc.simpleParseRank();

            // Perform anaphora resolution
            if (do_anaphora_resolution)
            {
                hobbs.addParse(sntc);
                hobbs.resolve(sntc);
            }
        } catch (Exception e)
        {
            System.err.println("Error: Failed to process sentence: " + sentence);
            e.printStackTrace();
        }
        if (verbosity > 0)
        {
            reportTime("RelEx processing: ");
        }
        return sntc;
    }

	/* ---------------------------------------------------------- */

    /**
     * Parses a sentence, using the parser. The private ArrayList of currentParses is filled with the ParsedSentences.
     */
    private Sentence
    parseSentence(String sentence)
    {
        if (sentence == null)
        {
            return null;
        }

        Sentence sent = null;
        if (sentence.length() < DEFAULT_MAX_SENTENCE_LENGTH)
        {
            sent = parser.parse(sentence);
        } else
        {
            System.err.println("Sentence too long, len=" + sentence.length()
                    + " : " + sentence);
            sent = new Sentence();
        }
        return sent;
    }

    private void reportTime(String msg)
    {
        Long now = System.currentTimeMillis();
        Long elapsed = now - starttime;
        starttime = now;

        Long sum = sumtime.get(msg);
        Long cnt = cnttime.get(msg);
        if (sum == null)
        {
            sum = 0L;
            cnt = 0L;
        }
        cnt++;
        sum += elapsed;
        sumtime.put(msg, sum);
        cnttime.put(msg, cnt);

        Long avg = sum / cnt;
        System.err.println(msg + elapsed + " milliseconds (avg="
                + avg + " millisecs, cnt=" + cnt + ")");
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem)  throws AnalysisEngineProcessException
    {
        sentenceType = typeSystem.getType("edu.umn.biomedicus.type.Sentence");
        sectionType = typeSystem.getType("edu.umn.biomedicus.type.Section");
        sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
        sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
        sentenceClinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");


    }

    public boolean isFamilyHistory(String sectionHeading, String subsectionHeading, String sentenceText)
        {
        StringBuilder testTextSb = new StringBuilder();
        testTextSb.append(sectionHeading.toLowerCase());
        testTextSb.append(subsectionHeading.toLowerCase());
        testTextSb.append(sentenceText.toLowerCase());
        boolean sectionResult = LexiconUtil.containsFamilyWord(sectionHeading);
        boolean subsectionResult = LexiconUtil.containsFamilyWord(subsectionHeading);
        boolean sentenceResult = LexiconUtil.containsFamilyWord(sentenceText);
        if (sectionResult || subsectionResult) return true;
        return false;
    }

}
