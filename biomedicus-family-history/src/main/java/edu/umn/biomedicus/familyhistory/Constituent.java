/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.familyhistory;


import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;

public class Constituent {
    private String coveredText;
    private String label;
    private String identifier;
    private boolean negated;
    private String sideOfFamily;
    private String observationType;
    private int begin;
    private int end;

    public Constituent(){}

    public Constituent(AnnotationFS annot)

    {
        TypeSystem ts = annot.getCAS().getTypeSystem();
        Type familyHistoryConstituentType = ts.getType("edu.umn.biomedicus.type.FamilyHistoryConstituent");
        Feature labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
        Feature identifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
        Feature negatedFeature = familyHistoryConstituentType.getFeatureByBaseName("negated");
        Feature observationTypeFeature = familyHistoryConstituentType.getFeatureByBaseName("observationType");

        label = annot.getStringValue(labelFeature);
        coveredText = annot.getCoveredText();
        identifier = annot.getStringValue(identifierFeature);
        begin = annot.getBegin();
        end = annot.getEnd();

        if (label.equals("observation"))
            observationType = annot.getStringValue(observationTypeFeature);

    }

    public boolean isFamily()
    {
        return label.equals("family");
    }

    public boolean isObservation()
    {
        return label.equals("observation");
    }

    public String getCoveredText()
    {
        return coveredText;
    }

    public void setCoveredText(String coveredText)
    {
        this.coveredText = coveredText;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public int getBegin()
    {
        return begin;
    }

    public void setBegin(int begin)
    {
        this.begin = begin;
    }

    public int getEnd()
    {
        return end;
    }

    public void setEnd(int end)
    {
        this.end = end;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public boolean isNegated()
    {
        return negated;
    }

    public void setNegated(boolean negated)
    {
        this.negated = negated;
    }

    public String getSideOfFamily()
    {
        return sideOfFamily;
    }

    public void setSideOfFamily(String sideOfFamily)
    {
        this.sideOfFamily = sideOfFamily;
    }

    public String getObservationType()
    {
        return observationType;
    }

    public void setObservationType(String observationType)
    {
        this.observationType = observationType;
    }

}
