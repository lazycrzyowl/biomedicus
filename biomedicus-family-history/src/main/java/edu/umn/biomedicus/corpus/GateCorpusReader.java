package edu.umn.biomedicus.corpus;

import edu.umn.biomedicus.core.exceptions.ExternalResourceInitializationException;
import gate.*;
import gate.util.ExtensionFileFilter;
import gate.util.GateException;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.net.URL;


public class GateCorpusReader {

    @NotNull
    String location;

    public GateCorpusReader(String location)
            throws ExternalResourceInitializationException
    {
        this.location = location;

        // Initialize Gate api
        try
        {
            Gate.init();
        } catch (GateException e)
        {
            throw new ExternalResourceInitializationException(e);
        }
//        File corpusDirectory = new File(location);
    }

    public static void main(String[] args) throws Exception
    {
        Gate.init();
        Corpus corpus = Factory.newCorpus("/Users/bill0154/upmc/converted");
        File directory = new File("/Users/bill0154/upmc/original");
        ExtensionFileFilter filter = new ExtensionFileFilter("XML files", "xml");
        URL url = directory.toURL();
        corpus.populate(url, filter, null, false);
        System.out.println();
        for (Document d : corpus)
        {
            DocumentContent doc = d.getContent();
            AnnotationSet ann = d.getAnnotations();
            System.out.println(d.getName());
            AnnotationSet as = d.getAnnotations();
            for (Annotation annot : as)
            {
                System.out.println(annot.getType());
                if (annot.getType().equals("FH") )
                {
                    int id = annot.getId();
                    Node begin = annot.getStartNode();
                    Node end = annot.getEndNode();
                    String typeInfo = annot.getType();
                    FeatureMap fMap = annot.getFeatures();
                    DocumentContent content = d.getContent();
                    System.out.println(content.toString());
                }
            }
            System.out.println(d.getAnnotations());
        }

//        URL u =....; // URL of a serial datastore directory
//        SerialDataStore sds = new SerialDataStore(u.toString());
//        sds.open();
//        // getLrIds returns a list of LR Ids, so we get the first one
//        Object lrId = sds.getLrIds("gate.corpora.DocumentImpl").get(0);
//
//        // we need to tell the factory about the LR’s ID in the data
//        // store, and about which datastore it is in − we do this
//        // via a feature map:
//        FeatureMap features = Factory.newFeatureMap();
//        features.put(DataStore.LR_ID_FEATURE_NAME, lrId);
//        features.put(DataStore.DATASTORE_FEATURE_NAME, sds);
//        Document doc = (Document)
//                Factory.createResource("gate.corpora.DocumentImpl", features);

//        List docIds = ds.getLrIds("gate.corpora.DocumentImpl");
//        for (Object id : docIds)
//        {
//            Document d = (Document) Factory.createResource("gate.corpora.DocumentImpl",
//                    gate.Utils.featureMap(DataStore.DATASTORE_FEATURE_NAME, ds,
//                            DataStore.LR_ID_FEATURE_NAME, id));
//            try
//            {
//                File outputFile = new File("Users/bill0154/umc/original");
//                DocumentStaxUtils.writeDocument(d, outputFile);
//            } finally
//            {
//                Factory.deleteResource(d);
//            }
//        }

    }
}
