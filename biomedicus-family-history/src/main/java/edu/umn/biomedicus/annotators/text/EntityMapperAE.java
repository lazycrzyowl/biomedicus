package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.chunking.services.ChunkingServiceProvider;
import edu.umn.biomedicus.core.featureranges.SentenceFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Finds family members and disorders/procedures
 */
public class EntityMapperAE extends CasAnnotator_ImplBase {

    private static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";
    private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
    private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    private static final String POS_FEATURE_NAME = "pos";
    private static final String LABEL_FEATURE_NAME = "label";
    private static final String SOURCE_FEATURE_NAME = "source";
    private static final String CHUNKING_SERVICE_PROVIDER_KEY = "CHUNKING_SERVICE";
    private static final String MODEL_FILE = "/edu/umn/biomedicus/annotators/text/en-chunker.bin";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(EntityMapperAE.class);
    @NotNull
    POSTaggerME tagger;
    @NotNull
    private Type chunkType;
    @NotNull
    private Type tokenType;
    @NotNull
    private Feature tokenStopwordFeature;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Feature clinicalClassFeature;
    @NotNull
    private String[] sofaNames;
    @NotNull
    private POSModel posModel;
    @NotNull
    private ChunkingServiceProvider chunker;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {

        super.initialize(context);
        try
        {
            chunker = (ChunkingServiceProvider) context.getResourceObject(CHUNKING_SERVICE_PROVIDER_KEY);
        }
        catch (ResourceAccessException e)
        {
            throw new ResourceInitializationException(e);
        }

    }

    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        CAS sysCAS = aCAS.getView("System");
        String line;

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, sentenceType))
        {
            SentenceFeatureRange sent = new SentenceFeatureRange(sysCAS, sentenceAnnotation);

            String clinicalClass = sent.getClinicalClass();
            if (clinicalClass == null || !clinicalClass.equals("familyhistory")) continue;

            String sentenceText = sentenceAnnotation.getCoveredText();
            int sentenceStart = sentenceAnnotation.getBegin();
            List<AnnotationFS> tokenList = sent.getCoveredTokens();
            String[] tokensAsStrings = AnnotationUtils.annotationListToStringArray(tokenList);
            Span[] tokensAsSpans = sent.getCoveredTokensAsSpans();
            String[] tags = sent.getPOSTagsOfCoveredTokens();
            edu.umn.biomedicus.core.utils.Span[] chunks = null;

            try
            {
                chunks = chunker.analyze(sentenceText, tokensAsStrings, tokensAsSpans, tags);
            }
            catch (Exception e)
            {
                throw new AnalysisEngineProcessException(e);
            }

            for (edu.umn.biomedicus.core.utils.Span chunk : chunks)
            {
                String chunkText = sentenceText.substring(chunk.getStart(), chunk.getEnd());

                System.out.println("Found chunk: " + chunkText);
                AnnotationFS chunkAnnotation = sysCAS.createAnnotation(chunkType, chunk.getStart(), chunk.getEnd());
                sysCAS.addFsToIndexes(chunkAnnotation);
            }
        }
    }

    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {

        // Initialize types
        super.typeSystemInit(typeSystem);

        tokenType = typeSystem.getType(TOKEN_ANNOTATION_NAME);
        tokenStopwordFeature = tokenType.getFeatureByBaseName("isStopword");
        chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
        sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
        clinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
        logger.log(Level.INFO, "Sentence Annotator type system initialized.");

    }

}

