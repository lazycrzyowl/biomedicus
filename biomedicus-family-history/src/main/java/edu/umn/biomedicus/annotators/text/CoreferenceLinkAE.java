/* 
Copyright 2010-14 University of Minnesota
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.relations.PropositionMapper;
import edu.umn.biomedicus.type.Sentence;
import opennlp.tools.coref.DefaultLinker;
import opennlp.tools.coref.DiscourseEntity;
import opennlp.tools.coref.Linker;
import opennlp.tools.coref.LinkerMode;
import opennlp.tools.coref.mention.DefaultParse;
import opennlp.tools.coref.mention.Mention;
import opennlp.tools.parser.*;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 *

 *
 */

public class CoreferenceLinkAE extends CasAnnotator_ImplBase {

  private static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";
  private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
  private static final String SECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Section";
  private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
  private static final String SUBSECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Subsection";
  private static final String FAMILY_HISTORY_ANNOTATION_NAME = "edu.umn.biomedicus.type.FamilyHistory";
  private static final String SECTION_HEADING_FEATURE_NAME = "heading";
  private static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
  private static final String SECTION_LEVEL_FEATURE_NAME = "level";
  private static final String SECTION_HAS_SUBSECTIONS_FEATURE_NAME = "hasSubsections";
  private static final String CHUNK_LABEL_FEATURE_NAME = "label";

  private static final String DEFAULT_MODEL_FILE = "/edu/umn/biomedicus/annotators/text/en-token.bin";

  private static final int SECTION_LEVEL = 0;
  private static final int SUBSECTION_LEVEL = 1;

  @NotNull
  private Type tokenType;

  @NotNull
  private Type chunkType;

  @NotNull
  private Type sentenceType;

  @NotNull
  private Type sectionType;

  @NotNull
  private Type familyHistoryType;

  @NotNull
  private Type subsectionType;

  @NotNull
  private Feature sectionContentStartFeature;

  @NotNull
  private Feature sectionHeadingFeature;

  @NotNull
  private Feature sectionHasSubsectionsFeature;

  @NotNull
  private Feature sectionLevelFeature;

  @NotNull
  private Feature chunkLabelFeature;

  @NotNull
  private PropositionMapper propositionMapper;

  @NotNull
  private Tokenizer tokenizer;

  @NotNull
  private String corefModelDirectory;

  @NotNull
  private String parserModelFile;

  @NotNull
  private String[] sofaNames;

  @NotNull
  private static final Logger logger = UIMAFramework.getLogger(FamilyHistoryDetectorAE.class);

  @NotNull
  private Parser _parser = null;

  @NotNull
  private Linker corefLinker;


  @Override
  public void initialize(UimaContext context) throws ResourceInitializationException {

    super.initialize(context);

    String applicationBase = System.getenv("BIOMEDICUS_HOME");

    // Create the tokenizer
    @NotNull
    InputStream tokenizerModelInputStream;

    try {
      // Loading tokenizer model
      tokenizerModelInputStream = getClass().getClassLoader().getResourceAsStream(DEFAULT_MODEL_FILE);
      TokenizerModel tokenModel = new TokenizerModel(tokenizerModelInputStream);
      tokenizerModelInputStream.close();

      tokenizer = new TokenizerME(tokenModel);

    } catch (final IOException e) {
      throw new ResourceInitializationException(e);
    }
    // Create the parser
    String parserModelPath = "/edu/umn/biomedicus/annotators/text/en-parser-chunking.bin";
    ParserModel parseModel = null;
    InputStream parserModelInputStream = getClass().getClassLoader().getResourceAsStream(parserModelPath);
    try {
      parseModel = new ParserModel(parserModelInputStream);
    }  catch (IOException e) {
      throw new ResourceInitializationException(e);
    }
    _parser =  ParserFactory.create(parseModel);

    // Create the coreference linker
    corefModelDirectory = new File(applicationBase, "resources/opennlp/crModels").toString();
    try {
      corefLinker = new DefaultLinker(corefModelDirectory, LinkerMode.TEST);
    } catch (final IOException ioe) {
      ioe.printStackTrace();
    }

  }

  @Override
  public void process(CAS aCAS) throws AnalysisEngineProcessException {

    List<Mention> document = new ArrayList<Mention>();
    DiscourseEntity[] entityMentions = null;
    AnnotationIndex<Sentence> sentences = (AnnotationIndex<Sentence>) AnnotationUtils.getAnnotations(aCAS, sentenceType);
    int currentSentence = 0;

    for (Sentence sentence : sentences) {
      List<AnnotationFS> sentenceTokens = AnnotationUtils.getCoveredAnnotations(aCAS, sentence, tokenType);

      Parse parse = parseSentence(sentence, sentenceTokens);
      DefaultParse parseWrapper = new DefaultParse(parse, currentSentence);
      Mention[] extents = corefLinker.getMentionFinder().getMentions(parseWrapper);

      for (int i=0; i < extents.length; i++) {
        if (extents[i].getParse() == null) {
          final Parse snp = new Parse(parse.getText(),
                  extents[i].getSpan(), "NML", 1.0, 0);
          parse.insert(snp);
          // setting a new Parse for the current extent
          extents[i].setParse(new DefaultParse(snp, currentSentence));
        }
      }

      document.addAll(Arrays.asList(extents));
      currentSentence++;
    }

    if (!document.isEmpty()) {
      corefLinker.getEntities(document.toArray(new Mention[0]));
    }

  }

  private Span[] tokensToSpans(AnnotationFS sentenceAnnotation, List<AnnotationFS> sentenceTokens) {
    int sentenceStart = sentenceAnnotation.getBegin();
    Span[] spans = new Span[sentenceTokens.size()];
    for (int i = 0; i < sentenceTokens.size(); i++) {
      AnnotationFS token = sentenceTokens.get(i);
      int start = token.getBegin() - sentenceStart;
      int end = token.getEnd() - sentenceStart;
      spans[i] = new Span(start, end);
    }

    return spans;
  }

  private Parse parseSentence(AnnotationFS sentenceAnnotation, List<AnnotationFS> sentenceTokens) {

    String nodeType;
    String label = null;

    String sentenceText = sentenceAnnotation.getCoveredText();
    Span rootSpan = new Span(0, sentenceText.length());

    Parse p = new Parse(sentenceText, rootSpan, AbstractBottomUpParser.INC_NODE, 1, 0);

    Span[] spans = tokensToSpans(sentenceAnnotation, sentenceTokens);

    for (int i=0; i < spans.length; i++) {
      Span span = spans[i];
      Parse parseConstituent = new Parse(sentenceText, span, AbstractBottomUpParser.TOK_NODE, 0, i);
      p.insert(parseConstituent);
    }

    Parse actualParse = parse(p);
    return actualParse;
  }

  private Parse parse(Parse p) {
    // lazy initializer
    if (_parser == null) {
      InputStream modelIn = null;
      try {

        final ParserModel parseModel = new ParserModel(modelIn);

        _parser = ParserFactory.create(parseModel);
      } catch (IOException ioe) {
        ioe.printStackTrace();
      } finally {
        if (modelIn != null) {
          try {
            modelIn.close();
          } catch (final IOException e) {} // oh well!
        }
      }
    }
    if (p == null) return null;
    return _parser.parse(p);
  }

  /**
   * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
   *
   * @param typeSystem
   * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
   */
  @Override
  public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

    // Initialize types
    super.typeSystemInit(typeSystem);
    tokenType = typeSystem.getType(TOKEN_ANNOTATION_NAME);
    chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
    sectionType = typeSystem.getType(SECTION_ANNOTATION_NAME);
    subsectionType = typeSystem.getType(SUBSECTION_ANNOTATION_NAME);
    sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
    familyHistoryType = typeSystem.getType(FAMILY_HISTORY_ANNOTATION_NAME);


    sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
    sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
    sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
    sectionHasSubsectionsFeature = sectionType.getFeatureByBaseName(SECTION_HAS_SUBSECTIONS_FEATURE_NAME);

    chunkLabelFeature = chunkType.getFeatureByBaseName(CHUNK_LABEL_FEATURE_NAME);

  }
}


