/* 
Copyright 2010-14 University of Minnesota
All rights reserved. 

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0 

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an "AS IS" BASIS, 
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
See the License for the specific language governing permissions and 
limitations under the License.
 */

package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.methods.FamilyHistoryIndicator;
import edu.umn.biomedicus.relations.PropositionMapper;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import weka.classifiers.bayes.NaiveBayesMultinomialText;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.SerializationHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *  The SocialHistoryDetector examines each sentence to determine if a
 *  sentence contains social history. If true, then a social history
 *  annotation is added
 *
 *  @author Robert Bill
 *
 */

public class MSTAE extends CasAnnotator_ImplBase {

    private static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";
    private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
    private static final String SECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Section";
    private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    private static final String SUBSECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Subsection";
    private static final String FAMILY_HISTORY_ANNOTATION_NAME = "edu.umn.biomedicus.type.FamilyHistory";
    private static final String SECTION_HEADING_FEATURE_NAME = "heading";
    private static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
    private static final String SECTION_LEVEL_FEATURE_NAME = "level";
    private static final String SECTION_HAS_SUBSECTIONS_FEATURE_NAME = "hasSubsections";
    private static final String CHUNK_LABEL_FEATURE_NAME = "label";

    private static final int SECTION_LEVEL = 0;
    private static final int SUBSECTION_LEVEL = 1;


    @NotNull
    private Type tokenType;

    @NotNull
    private Type chunkType;

    @NotNull
    private Type sentenceType;

    @NotNull
    private Type sectionType;
    @NotNull
    private Feature tokenStopwordFeature;


    @NotNull
    private Type subsectionType;

    @NotNull
    private Feature sectionContentStartFeature;

    @NotNull
    private Type familyHistoryType;

    @NotNull
    private Type observationConstituentType;

    @NotNull
    private Type predicationIndicationType;

    @NotNull
    private Feature sectionHeadingFeature;

    @NotNull
    private Feature sectionHasSubsectionsFeature;

    @NotNull
    private Feature sectionLevelFeature;

    @NotNull
    private Feature chunkLabelFeature;

    @NotNull
    private PropositionMapper propositionMapper;

    @NotNull
    private Type familyConstituentType;

    private static final String PARAM_MODEL_FILE = "modelFile";

    @NotNull
    private String modelFile = "edu/umn/biomedicus/familyhistory/FamilyHistoryDetector_NaiveBayesNullStemmer.model";
    // RB, 1/30/2014: changed to use Naive Bayes temporarily for testing. Faster, and maybe same results as kernel
    //private String modelFile = "/edu/umn/biomedicus/annotators/text/FH_SGDText_20130811.model";

    @NotNull
    private String indicatorWordsFile = "edu/umn/biomedicus/familyhistory/FamilyHistoryIndicatorWords.txt";

    @NotNull
    private FamilyHistoryIndicator indicatorWordDetector;

    @NotNull
    private String[] sofaNames;

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(FamilyHistoryDetectorAE.class);

    @NotNull
    NaiveBayesMultinomialText nb;

    @NotNull
    TypeSystem typeSystem;

    // RB, 1/30/2014: changed to use Naive Bayes temporarily for testing. Faster, and maybe same results as kernel
    // SGDText cls; //Classifier cls;

    private Instances dataRaw;
    private Attribute section;
    private Attribute subsection;
    private Attribute sentence;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {

        super.initialize(context);


        // load the model file
        try {
            //cls = (SGDText) SerializationHelper.read(this.getClass().getResourceAsStream(modelFile));
            nb = (NaiveBayesMultinomialText) SerializationHelper.read(getClass().getClassLoader().getResourceAsStream(modelFile));
        } catch (Exception e) {
            System.err.println("FamilyHistoryDetectorAE unable to de-serialize model file " + modelFile);
            System.err.println(e.toString());
            throw new ResourceInitializationException(e);
        }

        // load the dictionary for family history indicator words
        InputStream indicatorWordStream = getClass().getClassLoader().getResourceAsStream(indicatorWordsFile);
        try {
            indicatorWordDetector = new FamilyHistoryIndicator(indicatorWordStream);
        } catch (IOException e) {
            throw new ResourceInitializationException(e);
        }

        // Create Attribute and Instance information needed for the NaiveBayesMultinomial classifier
        ArrayList<Attribute> attributes = new ArrayList<>(3);
        ArrayList<String> classVal = new ArrayList<>();
        classVal.add("FH");  // Family History is 0.0 due to being the first nominal value
        classVal.add("NFH");  // Not Family History is 1.0 due to being the second nominal value

        section = new Attribute("section", (ArrayList<String>) null);
        attributes.add(section);
        subsection = new Attribute("subsection", (ArrayList<String>) null);
        attributes.add(subsection);
        sentence = new Attribute("sentence", (ArrayList<String>) null);
        attributes.add(sentence);

        attributes.add(new Attribute("@@class@@", classVal));
        dataRaw = new Instances("TestInstances", attributes, 0);
        dataRaw.setClassIndex(dataRaw.numAttributes() - 1);

        logger.setLevel(Level.OFF);
        logger.log(Level.INFO, "FamilyHistoryDetectorAE initialized successfully.");

    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);
        String sectionHeading = "?";
        String subsectionHeading = "?";

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, sentenceType)) {

            String sentenceText = sentenceAnnotation.getCoveredText();
            List<AnnotationFS> tokens = AnnotationUtils.getCoveredAnnotations(sysCAS, sentenceAnnotation, tokenType);
            String[] tokensAsStrings = AnnotationUtils.annotationListToStringArray(tokens, tokenStopwordFeature);

        }

    }



    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

        // Initialize types
        super.typeSystemInit(typeSystem);
        tokenType = typeSystem.getType(TOKEN_ANNOTATION_NAME);
        chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
        sectionType = typeSystem.getType(SECTION_ANNOTATION_NAME);
        subsectionType = typeSystem.getType(SUBSECTION_ANNOTATION_NAME);
        sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
        familyHistoryType = typeSystem.getType(FAMILY_HISTORY_ANNOTATION_NAME);
        familyConstituentType = typeSystem.getType("edu.umn.biomedicus.type.FamilyConstituent");
        predicationIndicationType = typeSystem.getType("edu.umn.biomedicus.type.PredicationIndicator");
        observationConstituentType = typeSystem.getType("edu.umn.biomedicus.type.ObservationConstituent");

        tokenStopwordFeature = tokenType.getFeatureByBaseName("isStopword");
        sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
        sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
        sectionHasSubsectionsFeature = sectionType.getFeatureByBaseName(SECTION_HAS_SUBSECTIONS_FEATURE_NAME);

        chunkLabelFeature = chunkType.getFeatureByBaseName(CHUNK_LABEL_FEATURE_NAME);

        logger.log(Level.INFO, "Sentence Annotator type system initialized.");

    }
}


