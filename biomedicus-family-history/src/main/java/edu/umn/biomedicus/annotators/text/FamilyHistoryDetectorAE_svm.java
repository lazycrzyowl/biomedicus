/* 
 Copyright 2012-13 University of Minnesota
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.annotators.text;

import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.cas.CAS;

/**
 * This class detects acronyms and adds an annotation for each edu.umn.biomedicus.acronym. Positive detection is one of the following: 1.
 * Exact matches to an edu.umn.biomedicus.acronym manifest 2. Positive outcome from simple rule sets such as all consonants 3. SVM
 * (SGDText) instance prediction using token shape as the attribute. Token shapes are cC for consonants, vV for vowels,
 * 9 for numbers, $ for symbols and '.' and ',' remain unchanged.
 *
 * @author Riea Moon
 * @author Robert Bill
 *
 *
 */


public class FamilyHistoryDetectorAE_svm extends CasAnnotator_ImplBase {

  public void process(CAS aCAS) {

  }
/*
    public static final String PARAM_MODEL_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
            AcronymDetectorAE_SVM.class, "modelFile");
    public static final String PARAM_ACRONYM_MANIFEST_FILE = ConfigurationParameterFactory
            .createConfigurationParameterName(AcronymDetectorAE_SVM.class, "acronymCuiManifestFile");
    @ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/edu.umn.biomedicus.acronym/acronymDetectionSGDPruned.model")
    private String modelFile;
    @ConfigurationParameter(mandatory = false, defaultValue = "/edu/umn/biomedicus/edu.umn.biomedicus.acronym/AcronymManifest.csv")
    private String acronymCuiManifestFile;
    SGDText cls; //Classifier cls;
    private AcronymManifest manifest = null;
    private Instances dataRaw;
    private Attribute word;
    private Attribute shape;
    private ExtendedLogger log;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
        try {
            AtomicReference<InputStream> inputStream;
            inputStream = new AtomicReference<InputStream>(this.getClass().getResourceAsStream(acronymCuiManifestFile));
            manifest = new AcronymManifest(inputStream.get());
        } catch (IOException e) {
            System.err.println("AcronymDetectorAE_SVM unable to initalize the edu.umn.biomedicus.acronym manifest. Probably check that the manifest file is at: "
                    + acronymCuiManifestFile);
            e.printStackTrace();
            throw new ResourceInitializationException();
        }
        try {
            cls = (SGDText) weka.core.SerializationHelper.read(this.getClass().getResourceAsStream(modelFile));
        } catch (Exception e) {
            System.err.println("AcronymDetectorAE_SVM unable to de-serialize model file " + modelFile);
            System.err.println(e.toString());
            throw new ResourceInitializationException();
        }

        // Create Attribute and Instance information needed for the SGDText classifier
        ArrayList<Attribute> atts = new ArrayList<Attribute>(2);
        ArrayList<String> classVal = new ArrayList<String>();
        classVal.add("ACRONYM");
        classVal.add("NONACRONYM");
        word = new Attribute("word", (ArrayList<String>) null);
        atts.add(word);
        shape = new Attribute("shape", (ArrayList<String>) null);
        atts.add(shape);
        atts.add(new Attribute("@@class@@", classVal));
        dataRaw = new Instances("TestInstances", atts, 0);
        log.info("AcronymDetectorAE_SVM initialized successfully.");
    }

    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {
        for (Token token : JCasUtil.select(jCas, Token.class)) {
            String candidate = token.getCoveredText();

//            if (manifest.containsKey(candidate)
//                    || ruleDetection(token)
//                    || modelDetection(token)) addAcronymAnnotation(jCas, token);
        }
    }

    protected String getWordShape(String candidate) {
        StringBuffer sb = new StringBuffer();

        for (char c : candidate.toCharArray()) {
            if ("BCDFGHJKLMNPQRSTVWXYZ".indexOf(c) != -1) {
                sb.append('C');
            } else if ("AEIOU".indexOf(c) != -1) {
                sb.append('V');
            } else if ("bcdfghjklmnpqrstvwxyz".indexOf(c) > -1) {
                sb.append('c');
            } else if ("aeiou".indexOf(c) != -1) {
                sb.append('v');
            } else if (".,".indexOf(c) != -1) {
                sb.append(c);
            } else {
                sb.append('$');
            }
        }

        return sb.toString();
    }

    protected boolean ruleDetection(Token token) {
        String tokenShape = getWordShape(token.getCoveredText());
        int vowels = 0;
        int consonants = 0;
        int periods = 0;

        for (int i = 0; i < tokenShape.length(); i++) {
            char c = tokenShape.charAt(i);
            if ("vV".indexOf(c) != -1) vowels++;
            if ("cC".indexOf(c) != -1) consonants++;
            if (c == '.') periods++;
        }
        if ((vowels == 0 && consonants > 0) || (consonants == 0 && vowels > 0)) return true;
        if (tokenShape.length() > 1 && tokenShape.length() - 1 <= periods) return true;
        return false;
    }

    protected boolean modelDetection(Token token) {
        dataRaw.setClassIndex(dataRaw.numAttributes() - 1);
        Instance inst = new DenseInstance(3);
        inst.setValue(word, token.getCoveredText());
        inst.setValue(shape, getWordShape(token.getCoveredText()));
        inst.setDataset(dataRaw);
        Double outcome = null;
        try {
            outcome = cls.classifyInstance(inst);
        } catch (Exception e) {
            System.err.println(e);
        }
        return outcome != 1.0;

    }

    protected void addAcronymAnnotation(JCas jcas, Token token) {
        Acronym a = new Acronym(jcas, token.getBegin(), token.getEnd());
        a.addToIndexes();
    }

*/
}
