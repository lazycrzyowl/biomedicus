/*
 * Copyright University of Minnesota 2014
 */
package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.chunking.services.ChunkingServiceProvider;
import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.methods.LexiconUtil;
import edu.umn.biomedicus.relations.Proposition;
import edu.umn.biomedicus.relations.PropositionMapper;
import edu.umn.biomedicus.type.*;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// import relex.corpus.QuotesParensSentenceDetector;

/**
 * The RelationExtractor class provides the central processing point for parsing sentences and extracting dependency
 * relationships from them.  The main() proceedure is usable as a stand-alone document analyzer; it supports several
 * flags modifying the displayed output.
 * <p/>
 * The primary interface is the processSentence() method, which accepts one sentence at a time, parses it, and extracts
 * relationships from it. This method is stateful: it also performs anaphora resolution.
 */
public class FamilyHistoryRelationAE extends CasAnnotator_ImplBase {
    private final String SECTION_HEADING_FEATURE_NAME = "heading";
    private final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
    private final String SECTION_LEVEL_FEATURE_NAME = "level";
    private final String SENTENCE_CLINICAL_CLASS_FEATURE_NAME = "clinicalClass";
    private final String REASONING_SERVICE_PROVIDER_KEY = "REASONING_SERVICE";
    private final String CHUNKING_SERVICE_PROVIDER_KEY = "CHUNKING_SERVICE";
    private final String METADATA_ANNOTATION_NAME = "edu.umn.biomedicus.type.MetaData";
    private final String METADATA_DOCUMENTID_FEATURE_NAME = "documentId";

    private final int SECTION_LEVEL = 0;
    private final int SUBSECTION_LEVEL = 1;

    @NotNull
    private Type familyHistoryType;
    @NotNull
    private ChunkingServiceProvider chunker;
    @NotNull
    private Feature negatedFeature;
    @NotNull
    public Type tokenType;
    @NotNull
    public Type sentenceType;
    @NotNull
    public Type sectionType;
    @NotNull
    public Feature familyConstituentFeature;
    @NotNull
    public Feature observationConstituentFeature;
    @NotNull
    Feature observationMappingFeature;
    @NotNull
    private Type metaDataType;
    @NotNull
    private Feature documentIdFeature;
    @NotNull
    public Type familyHistoryConstituentType;
    @NotNull
    public Feature familyHistoryConstituentLabelFeature;
    @NotNull
    public Feature familyHistoryConstituentIdentifierFeature;
    @NotNull
    public Feature familyMappingFeature;
    @NotNull
    public Feature predicationNegationFeature;
    @NotNull
    public Feature tokenClinicalClassFeature;
    @NotNull
    public Feature vitalStatusFeature;
    @NotNull
    private Feature sectionLevelFeature;
    @NotNull
    private Feature sectionHeadingFeature;
    @NotNull
    private Feature sectionContentStartFeature;
    @NotNull
    private Feature sentenceClinicalClassFeature;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        try
        {
            chunker = (ChunkingServiceProvider) context.getResourceObject(CHUNKING_SERVICE_PROVIDER_KEY);
        } catch (ResourceAccessException e)
        {
            throw new ResourceInitializationException(e);
        }

    }

    private String getDocumentId(CAS aCAS)
    {
        String documentId = "";
        AnnotationIndex<AnnotationFS> md = aCAS.getAnnotationIndex(metaDataType);
        Iterator<AnnotationFS> mdIterator = md.iterator();
        if (mdIterator.hasNext())
        {
            AnnotationFS metaData = mdIterator.next();
            documentId = metaData.getStringValue(documentIdFeature);
        }
        return documentId;
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        String documentId = getDocumentId(aCAS.getView(Views.METADATA_VIEW));
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);
        //int documentEnd = sysCAS.getDocumentText().length() - 1;

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, sentenceType))
        {
            String clinicalClass = sentenceAnnotation.getStringValue(sentenceClinicalClassFeature);

            // Only look at sentences that are candidates for containing family history statements
            if (clinicalClass == null || !clinicalClass.equals("familyhistory"))
                continue;

            // Look at the constituent chunks identified in previous steps of the pipeline
            // These chunks should include observationConstituents, familyConstituents, identifiers, and vitalStatus
            List<AnnotationFS> highLevelTokens = AnnotationUtils.getCoveredAnnotations(
                    sysCAS, sentenceAnnotation, familyHistoryConstituentType);

            for (AnnotationFS familyConstituent : highLevelTokens)
            {
                AnnotationUtils.removeCoveredAnnotations(sysCAS, familyConstituent, tokenType);

                // create high-level tokens
                int begin = familyConstituent.getBegin();
                int end = familyConstituent.getEnd();
                String label = familyConstituent.getStringValue(familyHistoryConstituentLabelFeature);
                AnnotationFS highLevelToken = sysCAS.createAnnotation(tokenType, begin, end);
                highLevelToken.setStringValue(tokenClinicalClassFeature, label);
                sysCAS.addFsToIndexes(highLevelToken);
            }

            String sentenceText = sentenceAnnotation.getCoveredText();
//            List<AnnotationFS> newTokenList = AnnotationUtils.getCoveredAnnotations(sysCAS, sentenceAnnotation, tokenType);
//            for (AnnotationFS token : newTokenList)
//            {
//                String cc = token.getStringValue(tokenClinicalClassFeature);
//                System.out.println("\t\tTOKEN: " + token.getCoveredText() + " : " + cc);
//            }
//            String[] sentenceAsTokens = AnnotationUtils.annotationListToStringArray(newTokenList);
//            System.out.print(sentenceText + ", ");
//
//            StringBuilder relPatt = new StringBuilder("\tRelationship pattern: ");
//            for (AnnotationFS chunk : chunks)
//            {
//                String label = chunk.getStringValue(familyHistoryConstituentLabelFeature);
//                String code = label.substring(0,3);
//                relPatt.append(code);
//                relPatt.append(" ");
//                String text = chunk.getCoveredText();
//                String identifier = chunk.getStringValue(familyHistoryConstituentIdentifierFeature);
//                String msg = String.format("\t%s = %s{%s:%s}", code, label, text, identifier);
//                System.out.println(msg);
//
//            }
//            System.out.println(relPatt);
//            System.out.println();


            List<Proposition> propositionList = null;
            if (sentenceText.toLowerCase().startsWith("noncontributory") ||
                    sentenceText.toLowerCase().startsWith("unremarkable"))
            {

                Proposition prop = new Proposition();
                int begin = sentenceAnnotation.getBegin();
                int end = sentenceAnnotation.getEnd();
//                int len = sentenceText.length();
                AnnotationFS observationConstituent = sysCAS.createAnnotation(familyHistoryConstituentType,
                        begin, end);
                observationConstituent.setStringValue(familyHistoryConstituentLabelFeature, "observation");
                prop.setObservationArgument(observationConstituent);
                propositionList = new ArrayList<>();
                propositionList.add(prop);
                annotateProposition(sysCAS, documentId, sentenceText, prop);
                continue;
            }
            else
            {

                List<AnnotationFS> tokens = AnnotationUtils.getCoveredAnnotations(sysCAS, sentenceAnnotation, tokenType);

                PropositionMapper propositionMapper = new PropositionMapper(sysCAS, familyHistoryConstituentType,
                        sentenceAnnotation, tokens, highLevelTokens, familyHistoryConstituentLabelFeature,
                        familyHistoryConstituentIdentifierFeature, negatedFeature);
                propositionList = propositionMapper.getPropositions();

            }
            for (Proposition prop : propositionList)
            {

                //List<Proposition> coordinatedProps = propositionMapper.resolveCoordination(prop);
                String msg = "\"%s\", \"%s\", \"%s\", \"%s\"";

//                AnnotationFS familyConstituent = prop.getFamilyArgument();
                String fam = getConstituentString(prop.getFamilyArgument());

                //AnnotationFS indicationConstituent = prop.getPredicationIndicator();
                //String ind = (indicationConstituent != null) ? indicationConstituent.getCoveredText() : "null";
                String ind = getConstituentString(prop.getPredicationIndicator());

//                AnnotationFS observationConstituent = prop.getObservationArgument();
//                String obs = (observationConstituent != null) ? observationConstituent.getCoveredText() : "null";
                String obs = getConstituentString(prop.getObservationArgument());


//                System.out.println(String.format(msg, documentId, sentenceText, fam, obs));
                annotateProposition(sysCAS, documentId, sentenceText, prop);

            }

            //reasoner.getPredications(chunks);
        }

    }

    private String getConstituentString(AnnotationFS constituent)
    {
        if (constituent != null)
        {
            String matchedText = constituent.getCoveredText();
            String id = constituent.getStringValue(familyHistoryConstituentIdentifierFeature);
            //return String.format("%s:%s", matchedText, id);
            return String.format("%s", matchedText);
        }
        return null;
    }

    private int getBegin(Proposition p)
    {
        List<AnnotationFS> constituents = new ArrayList<>();
        constituents.add(p.getFamilyArgument());
        constituents.add(p.getObservationArgument());
        constituents.add(p.getPredicationIndicator());
        int minimum = 999999999;

        for (AnnotationFS ann : constituents)
        {
            if (ann == null) continue;
            int begin = ann.getBegin();
            if (begin < minimum)
                minimum = begin;
        }

        return minimum;
    }

    private int getEnd(Proposition p)
    {
        List<AnnotationFS> constituents = new ArrayList<>();
        AnnotationFS test = p.getFamilyArgument();
        constituents.add(p.getFamilyArgument());
        test = p.getObservationArgument();
        constituents.add(p.getObservationArgument());
        test = p.getPredicationIndicator();
        constituents.add(p.getPredicationIndicator());
        int maximum = 0;
        for (AnnotationFS ann : constituents)
        {
            if (ann == null) continue;
            int end = ann.getEnd();
            if (end > maximum)
                maximum = end;
        }
        return maximum;
    }

    private void annotateProposition(CAS aCAS, String documentId, String sentenceText,
                                     Proposition p)
    {
        String text = aCAS.getDocumentText();
        int begin = getBegin(p);

        // if all parts are null in proposition, skip it
        if (begin == 999999999)
            return;
        int end = getEnd(p);
        String coveredText = text.substring(begin, end);
        String fam = "";
        String obs = "";
        String famMapping  = "";
        String obsMapping = "";
        String neg = "";
        String vital = "";
        if (p.getFamilyArgument() != null)
        {
            fam = p.getFamilyArgument().getCoveredText();
            famMapping = p.getFamilyArgument().getStringValue(familyHistoryConstituentIdentifierFeature);
        }
        if (p.getObservationArgument() != null)
        {

            obs = p.getObservationArgument().getCoveredText();
            obsMapping = p.getObservationArgument().getStringValue(familyHistoryConstituentIdentifierFeature);
        }

        if (LexiconUtil.containsNegationWord(sentenceText))
        {
            AnnotationFS obsArg = p.getObservationArgument();
            AnnotationFS negArg = p.getNegationArgument();
            if (obsArg != null && negArg != null) {
                int obsBegin = obsArg.getBegin();
                int negBegin = negArg.getBegin();
                int testBegin = Math.min(obsBegin, negBegin);
                int testEnd = Math.max(obsBegin, negBegin);
                String testText = text.substring(testBegin, testEnd).toLowerCase();
                if (testText.indexOf(" but ") == -1 && testText.indexOf(" and ") == -1)
                    neg = "NEGATED";
            } else {
                neg = "NEGATED";
            }
        }
        if (p.getVitalStatus() != null && !p.getVitalStatus().equals(""))
            vital = p.getVitalStatus();

        System.out.println(String.format("RelationAE Production: \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\"",
                documentId, sentenceText, fam, obs, neg, vital));


        AnnotationFS prop = aCAS.createAnnotation(familyHistoryType, begin, end);
        prop.setStringValue(familyConstituentFeature, fam);
        prop.setStringValue(familyMappingFeature, famMapping);
        prop.setStringValue(observationConstituentFeature, obs);
        prop.setStringValue(observationMappingFeature, obsMapping);
        if (neg.equals("NEGATED"))
            prop.setBooleanValue(predicationNegationFeature, true);
        prop.setStringValue(vitalStatusFeature, vital);

        aCAS.addFsToIndexes(prop);
        String familyConstituent;
        String familyMapping;
        String sideOfFamily;
        String observationConstituent;
        String observationMapping;
        String vitalStatus;
        String indicator;
        String certainty;
        boolean negated;
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem)  throws AnalysisEngineProcessException
    {
        Iterator<Type> typeIterator = typeSystem.getTypeIterator();
        while (typeIterator.hasNext())
        {
            Type t = typeIterator.next();
            // add init with generics here instead of the long list below.
        }
        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
        sectionType = typeSystem.getType(Section.class.getCanonicalName());
        familyHistoryConstituentType = typeSystem.getType(FamilyHistoryConstituent.class.getCanonicalName());

        negatedFeature = familyHistoryConstituentType.getFeatureByBaseName("negated");
        sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
        sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
        sentenceClinicalClassFeature = sentenceType.getFeatureByBaseName(SENTENCE_CLINICAL_CLASS_FEATURE_NAME);
        tokenClinicalClassFeature = tokenType.getFeatureByBaseName("clinicalClass");
        familyHistoryConstituentLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
        familyHistoryConstituentIdentifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
        String name = FamilyHistory.class.getCanonicalName();


        familyHistoryType = typeSystem.getType(FamilyHistory.class.getCanonicalName());
        predicationNegationFeature = familyHistoryType.getFeatureByBaseName("negated");
        vitalStatusFeature = familyHistoryType.getFeatureByBaseName("vitalStatus");
        familyConstituentFeature = familyHistoryType.getFeatureByBaseName("familyConstituent");
        familyMappingFeature = familyHistoryType.getFeatureByBaseName("familyMapping");
        observationConstituentFeature = familyHistoryType.getFeatureByBaseName("observationConstituent");
        observationMappingFeature = familyHistoryType.getFeatureByBaseName("observationMapping");
        metaDataType = typeSystem.getType(METADATA_ANNOTATION_NAME);
        documentIdFeature = metaDataType.getFeatureByBaseName(METADATA_DOCUMENTID_FEATURE_NAME);
    }

    public boolean isFamilyHistory(String sectionHeading, String subsectionHeading, String sentenceText)
    {
        StringBuilder testTextSb = new StringBuilder();
        testTextSb.append(sectionHeading.toLowerCase());
        testTextSb.append(subsectionHeading.toLowerCase());
        testTextSb.append(sentenceText.toLowerCase());
        boolean sectionResult = LexiconUtil.containsFamilyWord(sectionHeading);
        boolean subsectionResult = LexiconUtil.containsFamilyWord(subsectionHeading);
        if (sectionResult || subsectionResult) return true;
        return false;
    }

}
