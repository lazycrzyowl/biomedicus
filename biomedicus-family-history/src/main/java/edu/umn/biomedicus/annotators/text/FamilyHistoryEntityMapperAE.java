/* 
 Copyright 2014 University of Minnesota
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.annotators.text;


/*
make date into temporal information.
next step is classifying temporal as diagnosis date or vital status date.
evaluate by hand.
make first evaluation of termporal as binary.
call it "temporal statement"
look at preceding text for the preposition element.
*/

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.mapping.services.FilteringServiceProvider;
import edu.umn.biomedicus.mapping.services.MappingServiceProvider;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class FamilyHistoryEntityMapperAE extends CasAnnotator_ImplBase {
    final static String SENTENCE_TYPE_NAME = "edu.umn.biomedicus.type.Sentence";
    final static String MAPPING_SERVICE_PROVIDER_KEY = "MAPPING_SERVICE";
    final static String FILTER_SERVICE_PROVIDER_KEY = "FILTERING_SERVICE";
    @NotNull
    private MappingServiceProvider mapper;
    @NotNull
    private FilteringServiceProvider filter;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type chunkType;
    @NotNull
    private Feature clinicalClassFeature;


    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);

        try
        {
            mapper = (MappingServiceProvider) context.getResourceObject(MAPPING_SERVICE_PROVIDER_KEY);
            filter = (FilteringServiceProvider) context.getResourceObject(FILTER_SERVICE_PROVIDER_KEY);
        }
        catch (ResourceAccessException e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = aCAS.getView("System"); // always operate on System view
        String text = sysCAS.getDocumentText();
        int size = text.length();

        for (AnnotationFS sentence : AnnotationUtils.getAnnotations(sysCAS, sentenceType))
        {
            List<AnnotationFS> chunks = edu.umn.biomedicus.core.utils.AnnotationUtils.getCoveredAnnotations(sysCAS, sentence, chunkType);
            String clinicalClass = sentence.getStringValue(clinicalClassFeature);
            if (clinicalClass == null || !clinicalClass.equals("familyhistory")) continue;

            System.out.println("\nSentence: " + sentence.getCoveredText());
            for (AnnotationFS chunk : chunks)
            {
                String mesg = String.format("\tchunk: %s (%d:%d)", chunk.getCoveredText(), chunk.getBegin(), chunk.getEnd());
                System.out.println(mesg);
            }
            try
            {
                //mapper.getMapping(sentence.getCoveredText());
            }
            catch (Exception e)
            {
                throw new AnalysisEngineProcessException(e);
            }

        }
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

        super.typeSystemInit(typeSystem);
        sentenceType = typeSystem.getType(SENTENCE_TYPE_NAME);
        chunkType = typeSystem.getType("edu.umn.biomedicus.type.Chunk");
        clinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
        System.out.println(clinicalClassFeature);

    }
}
