package edu.umn.biomedicus.annotators.text;

import au.com.bytecode.opencsv.CSVReader;
import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.type.Sentence;
import opennlp.tools.sentdetect.SentenceDetectorME;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Sentence Annotator that uses Maximum Entropy sentence detection from OpenNLP. The ME model is trained using a
 * combination of the Genia corpus and a set of clinical notes.
 */
public class SentenceEvaluatorAE extends CasAnnotator_ImplBase {

    public static final String PARAM_MODEL_FILE = "model_file";
    public static final String OUTPUT_FILE_NAME = "pos-evaluation.txt";
    public static final String ERRORS_FILE_NAME = "errors.txt";
    public static final String CONFUSION_FILE_NAME = "confusion.ser";
    private static final String DEFAULT_MODEL_FILE = "/edu/umn/biomedicus/annotators/text/sentenceME.bin";
    private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    private static final String SECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Section";
    private static final String SECTION_HAS_SUBSECTIONS_FEATURE_NAME = "hasSubsections";
    private static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
    private static final String SECTION_HEADING_FEATURE_NAME = "heading";
    private static final String SECTION_LEVEL_FEATURE_NAME = "level";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(SentenceAE.class);
    @NotNull
    Feature sentenceClinicalClass;
    @NotNull
    CSVReader reader;
    @NotNull
    List<String[]> entries;
    @NotNull
    private String modelFile;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type metaDataType;
    @NotNull
    private Type sectionType;
    @NotNull
    private Feature sectionContentStartFeature;
    @NotNull
    private Feature sectionHeadingFeature;
    @NotNull
    private Feature sectionLevelFeature;
    @NotNull
    private Feature sectionHasSubsectionsFeature;
    @NotNull
    private SentenceDetectorME sentenceDetector;

    HashMap<String, AnnotationFS> goldFamilyHistory;
    HashMap<String, AnnotationFS> systemFamilyHistory;


    int tp;
    int fp;
    int fn;
    int tn;  // this is from a closed-world assumption

    public static void printEvaluationResults()
    {

    }

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {

        super.initialize(context);
        URL ins = getClass().getClassLoader().getResource("edu/umn/biomedicus/familyhistory/reference.txt");

        try
        {
            reader = new CSVReader(new FileReader(ins.getFile()));
            entries = reader.readAll();
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    public List<Span> getRanges(String fileId)
    {
        List<Span> results = new ArrayList<>();
        for (String[] row : entries)
        {
            String documentId = row[0];
            if (fileId.equals(documentId))
            {
                Span s = new Span(Integer.valueOf(row[1]), Integer.valueOf(row[2]));
                results.add(s);
            }
        }
        return results;
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        CAS systemView = aCAS.getView(Views.SYSTEM_VIEW);
        CAS goldView = aCAS.getView(Views.GOLD_VIEW);

        String documentId = AnnotationUtils.getDocumentId(aCAS, metaDataType);
        List<Span> ranges = getRanges(documentId);

        AnnotationIndex<Sentence> goldSentences = (AnnotationIndex<Sentence>) AnnotationUtils.getAnnotations(goldView, Sentence.class);
        AnnotationIndex<Sentence> systemSentences = (AnnotationIndex<Sentence>) AnnotationUtils.getAnnotations(systemView, Sentence.class);

        // convert to lists containing only the sentences that have the clinical class feature set to "familyhistory"
        goldFamilyHistory = getFamilyHistorySentences(goldSentences);
        systemFamilyHistory = getFamilyHistorySentences(systemSentences);

        for (String key : goldFamilyHistory.keySet())
        {
            if (systemFamilyHistory.containsKey(key))
            {
                tp++;
            } else
            {
                fn++;
            }
        }

        for (String key : systemFamilyHistory.keySet())
        {
            if (goldFamilyHistory.containsKey(key))
            {
            } else
            {
                fp++;
            }
        }

    }

    public HashMap<String, AnnotationFS> getFamilyHistorySentences(AnnotationIndex<? extends AnnotationFS> sentences)
    {
        HashMap<String, AnnotationFS> results = new HashMap<>();
        for (AnnotationFS sentence : sentences)
        {
            String clinicalClass = sentence.getStringValue(sentenceClinicalClass);

            if (clinicalClass != null && clinicalClass.equals("familyhistory"))
            {
                // create key out of span
                String key = new StringBuilder()
                        .append(sentence.getBegin())
                        .append(':')
                        .append(sentence.getEnd()).toString();
                 results.put(key, sentence);
            }
        }

        return results;
    }

    public void commitAnnotations(CAS aCAS, List<AnnotationFS> annotations)
    {
        for (AnnotationFS a : annotations)
        {
            aCAS.addFsToIndexes(a);
        }
    }

    @Override
    public void collectionProcessComplete() throws AnalysisEngineProcessException
    {
        System.out.println("Family History Sentence Detection evaluation");
        System.out.println("total:\t" + goldFamilyHistory.size());
        System.out.println("True positives:\t" + tp);
        System.out.println("True negatives:\t" + tn);
        System.out.println("False positives:\t" + fp);
        System.out.println("False negatives:\t" + fn);
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {
        super.typeSystemInit(typeSystem);
        sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
        sentenceClinicalClass = sentenceType.getFeatureByBaseName("clinicalClass");
        sectionType = typeSystem.getType(SECTION_ANNOTATION_NAME);
        sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
        sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
        sectionHasSubsectionsFeature = sectionType.getFeatureByBaseName(SECTION_HAS_SUBSECTIONS_FEATURE_NAME);
        metaDataType = typeSystem.getType("edu.umn.biomedicus.type.MetaData");
    }
}