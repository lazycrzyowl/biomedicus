package edu.umn.biomedicus.annotators.text;


import edu.umn.biomedicus.type.Section;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SectionAnnotator extends JCasAnnotator_ImplBase {

  @NotNull
  Logger logger;


  @Override
  public void initialize(UimaContext context)  throws ResourceInitializationException {
    super.initialize(context);
    logger = context.getLogger();
    logger.log(Level.INFO, "Section annotator initialized.");
  }

	@Override
	public void process(JCas arg0) throws AnalysisEngineProcessException {
		String docText = arg0.getDocumentText();

		String[] cmd;
		cmd = new String[2];
		cmd[0] =  "resources/Perl/section_extract.pl" ;
		cmd[1] = docText;

		Process p = null;
		try {
			p = Runtime.getRuntime().exec(cmd );
			if (p == null) throw new AnalysisEngineProcessException();
		} catch (IOException e) {
			throw new AnalysisEngineProcessException(e);
		}		

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		String line;
		
		try {
			while((line =stdInput.readLine()) != null){
				
				Pattern pattern = Pattern.compile("(.*?)\\|(.*?)\\|(.*?)\\|(.*?)\\|(.*?)\\|(.*)");
				Matcher matcher = pattern.matcher(line);
				if ( matcher.find() ){

					if  ( !matcher.group(4).equals("-1")  && !matcher.group(2).equals("-1")){

						Section scAnnt = new Section( arg0 );
						scAnnt.setBegin( Integer.parseInt(matcher.group(5)) - matcher.group(3).length() );
						scAnnt.setEnd( Integer.parseInt( matcher.group(5) ) + Integer.parseInt( matcher.group(6) ) );
						scAnnt.setHeading(matcher.group(3));
						scAnnt.setContentStart(Integer.parseInt(matcher.group(5)));
            int myBegin = scAnnt.getBegin();
            int myEnd = scAnnt.getEnd();
            String myHeader = scAnnt.getHeading();
            int myContentStart = scAnnt.getContentStart();
            int x = 21;
						scAnnt.addToIndexes();	
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
