package edu.umn.biomedicus.annotators.text;

import org.jetbrains.annotations.NotNull;

public class DictionaryEntry {
    @NotNull
    private String entry;
    @NotNull
    private int length;
    @NotNull
    private String id;


    public DictionaryEntry(String dictionaryRow)
    {
        String[] entryIdentifierPair = dictionaryRow.split("\\|");
        this.entry = entryIdentifierPair[0].trim();
        this.id = entryIdentifierPair[1].trim();
        this.length = entry.length();
    }


    @NotNull
    public String getEntry()
    {
        return entry;
    }

    public void setEntry(@NotNull String entry)
    {
        this.entry = entry;
    }

    @NotNull
    public int length()
    {
        return length;
    }

    @NotNull
    public String getId()
    {
        return id;
    }

    public void setId(@NotNull String id)
    {
        this.id = id;
    }

}

