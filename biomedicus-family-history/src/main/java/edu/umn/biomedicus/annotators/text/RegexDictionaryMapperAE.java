package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.core.utils.Span;
import edu.umn.biomedicus.type.FamilyHistoryConstituent;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Sentence Annotator that uses Maximum Entropy sentence detection from OpenNLP. The ME model is trained using a
 * combination of the Genia corpus and a set of clinical notes.
 */
public class RegexDictionaryMapperAE extends CasAnnotator_ImplBase {

    public static String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    public static String FAMILYHISTORY_ANNOTATION_NAME = "edu.umn.biomedicus.type.FamilyHistoryConstituent";
    public static String CHUNK_LABEL_FEATURE_NAME = "label";
    public static String CHUNK_FHROLE_FEATURE_NAME = "fhRole";

    // The character following a match candidate must be a space, symbol, or s for plural
    public static final String terminatingChars = " .,;:'[{]}|-_=+!?s";

    // Define parameters

    public static final String PARAM_FH_ROLE = "fhRoleParameter";
    public static final String PARAM_DICTIONARY = "dictionaryFileParameter";

    @NotNull
    private String fhRoleParameter;
    @NotNull
    private String dictionaryFileParameter;

    @NotNull
    private Type familyHistoryConstituentType;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type tokenType;

    @NotNull
    private Feature labelFeature;
    @NotNull
    Feature identifierFeature;
    @NotNull
    Feature identifierSource;
    @NotNull
    private UimaContext context;
    @NotNull
    private HashMap<String, String> dictionary;
    @NotNull
    private List<RegexDictionaryEntry> smTerms;

    @NotNull
    Logger logger = UIMAFramework.getLogger(RegexDictionaryMapperAE.class);

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {

        this.context = context;
        super.initialize(context);

        fhRoleParameter = (String) context.getConfigParameterValue(PARAM_FH_ROLE);
        dictionaryFileParameter = (String) context.getConfigParameterValue(PARAM_DICTIONARY);
        URL resource = getClass().getClassLoader().getResource(dictionaryFileParameter);
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }

        smTerms = new ArrayList<>();
        try
        {

            List<String> rawTerms = FileUtils.readLines(file);
            for (String s : rawTerms)
            {
                smTerms.add(new RegexDictionaryEntry(s));
            }
        } catch (Exception e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    private Matcher getFirstMatch(int begin, String comparableText)
    {

        String comparable = comparableText.substring(begin);
        for (RegexDictionaryEntry term: smTerms) {
            int offset = term.length();
            Matcher matcher = term.getPattern().matcher(comparableText);
            if (matcher.find(0))
                return matcher;
        }
        return null;
    }

    private boolean sentenceContainsVitalStatus(CAS view, AnnotationFS sentence)
    {
        List<AnnotationFS> constituentList = AnnotationUtils.getCoveredAnnotations(view,
                sentence, familyHistoryConstituentType);
        for (AnnotationFS constituent : constituentList)
        {
            String label = constituent.getStringValue(labelFeature);
            if (label.equals("vitalStatus")) {
                String coveredText = constituent.getCoveredText().toLowerCase();

                if (coveredText.equals("alive") || coveredText.equals("living"))
                {
                    continue;
                }
                else
                { return true; }
            }
        }
        return false;
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);

        int workingIndex = 0;
        for (AnnotationFS sentence : AnnotationUtils.getAnnotations(sysCAS, Sentence.class))
        {
            String comparableText = sentence.getCoveredText().toLowerCase();
            if (!sentenceContainsVitalStatus(sysCAS, sentence)) continue;

            List<Span> existingMatches = new ArrayList<Span>();
            for (RegexDictionaryEntry entry : smTerms)
            {
                Matcher matcher = entry.getPattern().matcher(comparableText);
                if (matcher.find())
                {
                    int begin = matcher.start() + sentence.getBegin();
                    int end = matcher.end() + sentence.getBegin();
                    Span span = new Span(begin, end);

                    // make sure the match isn't a substring of a longer match
                    boolean isNewMatch = true;
                    for (Span s : existingMatches)
                    {
                        if (s.contains(span)) isNewMatch = false;
                    }

                    if (isNewMatch) {
                        AnnotationFS chunk = sysCAS.createAnnotation(familyHistoryConstituentType, begin, end);
                        String test = chunk.getCoveredText();
                        chunk.setStringValue(labelFeature, fhRoleParameter);
                        sysCAS.addFsToIndexes(chunk);
                        existingMatches.add(span);
                    }
                }
            }
        }
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

        // Initialize types
        super.typeSystemInit(typeSystem);

        // Initialize type
        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
        familyHistoryConstituentType = typeSystem.getType(FamilyHistoryConstituent.class.getCanonicalName());

        // Initialize features
        labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
        identifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
    }
}