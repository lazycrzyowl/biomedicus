/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package edu.umn.biomedicus.annotators.text;

import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class WhitespaceTokenAE extends CasAnnotator_ImplBase {

  private static final int CH_SPECIAL = 0;
  private static final int CH_NUMBER = 1;
  private static final int CH_LETTER = 2;
  private static final int CH_WHITESPACE = 4;
  private static final int CH_PUNCTUATION = 5;
  private static final int CH_NEWLINE = 6;
  private static final int UNDEFINED = -1;
  private static final int INVALID_CHAR = 0;

  public static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";

  @NotNull
  private Type tokenType;

  @NotNull
  private Type sentenceType;

  private String[] sofaNames;

  private static List<String> punctuations = Arrays.asList(new String[]{".", "!", "?"});

  @NotNull
  private static final Logger logger = UIMAFramework.getLogger(WhitespaceTokenAE.class);

  /*
   * (non-Javadoc)
   * 
   * @see org.apache.uima.analysis_component.CasAnnotator_ImplBase#process(org.apache.uima.cas.CAS)
   */
  @Override
  public void process(CAS aCAS) throws AnalysisEngineProcessException {

    logger.log(Level.INFO, "Tokenizer started processing.");
    CAS sysCAS = aCAS.getView("System");

    // get text content from the CAS
    char[] textContent = sysCAS.getDocumentText().toCharArray();

    int tokenStart = UNDEFINED;
    int currentCharPos = 0;
    int sentenceStart = 0;
    int nextCharType = UNDEFINED;
    char nextChar = INVALID_CHAR;

    while (currentCharPos < textContent.length) {
      char currentChar = textContent[currentCharPos];
      int currentCharType = getCharacterType(currentChar);
      nextChar = getNextChar(textContent, currentCharPos);
      nextCharType = getCharacterType(nextChar);

      // check if current character is a letter or number
      if (currentCharType == CH_LETTER || currentCharType == CH_NUMBER) {

        // check if it is the first letter of a token
        if (tokenStart == UNDEFINED) {
          // start new token here
          tokenStart = currentCharPos;
        }
      }

      // check if current character is a whitespace character
      else if (currentCharType == CH_WHITESPACE
              || currentCharType == CH_SPECIAL
              || currentCharType == CH_NEWLINE
              || currentCharType == CH_PUNCTUATION) {

        // terminate current token
        if (tokenStart != UNDEFINED) {
          // end of current word
          createAnnotation(sysCAS, tokenType, tokenStart, currentCharPos);
          tokenStart = UNDEFINED;
        }

        if (currentCharType == CH_SPECIAL) {
          // create token for special character
          createAnnotation(sysCAS, tokenType, currentCharPos, currentCharPos + 1);
        } else if (currentCharType == CH_PUNCTUATION) {
          // create token for punctuation character
          createAnnotation(sysCAS, tokenType, currentCharPos, currentCharPos + 1);
        }
      }

      // go to the next token
      currentCharPos++;
    } // end of character loop

    // we are at the end of the text terminate open token annotations
    if (tokenStart != UNDEFINED) {
      // end of current word
      createAnnotation(sysCAS, tokenType, tokenStart, currentCharPos);
      tokenStart = UNDEFINED;
    }

  logger.log(Level.INFO, "WhitespaceTokenAE completed processing.");
  }
  /**
   * Get the next character from the text stream, or return INVALID_CHAR.
   *
   * @param textContent
   * A char[] holding the entire text.
   *
   * @param currentCharPos
   * An int that is the index of the current point in the text.
   *
   * @return
   * char at the next text location.
   */
  public char getNextChar(char[] textContent, int currentCharPos) {

    // get character class for current and next character
    char nextChar = INVALID_CHAR;
    if ((currentCharPos + 1) < textContent.length) nextChar = textContent[currentCharPos + 1];
    return nextChar;

  }

  /**
   * Get a list of all SOfA's that should be tokenized.
   *
   * @param aCAS The CAS for the current document.
   * @return List containing a CAS for each SOfA that should be tokenized.
   */
  private List<CAS> getSofaList(CAS aCAS) {
    ArrayList<CAS> casCollector = new ArrayList<>();
    new ArrayList<CAS>();
    // check if sofa names are available
    if (sofaNames != null && sofaNames.length > 0) {

      // get sofa names
      for (int i = 0; i < sofaNames.length; i++) {
        Iterator it = aCAS.getViewIterator(sofaNames[i]);
        while (it.hasNext()) {
          // add sofas to the cas List to process
          casCollector.add((CAS) it.next());
        }
      }
    } else {
      // use default sofa for the processing
      casCollector.add(aCAS);
    }

    return casCollector;
  }


  /**
   * create an annotation of the given type in the CAS using startPos and endPos.
   *
   * @param annotationType annotation type
   * @param startPos       annotation start position
   * @param endPos         annotation end position
   */
  private void createAnnotation(CAS aCAS, Type annotationType, int startPos, int endPos) {
    AnnotationFS annotation = aCAS.createAnnotation(annotationType, startPos, endPos);
    aCAS.addFsToIndexes(annotation);
  }

  /**
   * returns the character type of the given character. Possible character classes are: CH_LETTER for all letters
   * CH_NUMBER for all numbers CH_WHITESPACE for all whitespace characters CH_PUNCTUATUATION for all punctuation
   * characters CH_NEWLINE for all new line characters CH_SPECIAL for all other characters that are not in any of the
   * groups above
   *
   * @param character aCharacter
   * @return returns the character type of the given character
   */
  private static int getCharacterType(char character) {

    switch (Character.getType(character)) {

      // letter characters
      case Character.UPPERCASE_LETTER:
      case Character.LOWERCASE_LETTER:
      case Character.TITLECASE_LETTER:
      case Character.MODIFIER_LETTER:
      case Character.OTHER_LETTER:
      case Character.NON_SPACING_MARK:
      case Character.ENCLOSING_MARK:
      case Character.COMBINING_SPACING_MARK:
      case Character.PRIVATE_USE:
      case Character.SURROGATE:
      case Character.MODIFIER_SYMBOL:
        return CH_LETTER;

      // number characters
      case Character.DECIMAL_DIGIT_NUMBER:
      case Character.LETTER_NUMBER:
      case Character.OTHER_NUMBER:
        return CH_NUMBER;

      // whitespace characters
      case Character.SPACE_SEPARATOR:
        // case Character.CONNECTOR_PUNCTUATION:
        return CH_WHITESPACE;

      case Character.DASH_PUNCTUATION:
      case Character.START_PUNCTUATION:
      case Character.END_PUNCTUATION:
      case Character.OTHER_PUNCTUATION:
        return CH_PUNCTUATION;

      case Character.LINE_SEPARATOR:
      case Character.PARAGRAPH_SEPARATOR:
        return CH_NEWLINE;

      case Character.CONTROL:
        if (character == '\n' || character == '\r') {
          return CH_NEWLINE;
        } else {
          // tab is in the char category CONTROL
          if (Character.isWhitespace(character)) {
            return CH_WHITESPACE;
          }
          return CH_SPECIAL;
        }

      case INVALID_CHAR:
        return UNDEFINED;

      default:
        // the isWhitespace test is slightly more expensive than the above switch,
        // so it is placed here to avoid performance impact.
        // Also, calling code has explicit tests for CH_NEWLINE, and this test should not swallow
        // those
        if (Character.isWhitespace(character)) {
          return CH_WHITESPACE;
        }
        return CH_SPECIAL;
    }
  }

  @Override
  public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

    // Initialize types
    super.typeSystemInit(typeSystem);
    tokenType = typeSystem.getType(TOKEN_ANNOTATION_NAME);
    logger.log(Level.INFO, "WhitespaceTokenAE typeSystem initialized.");

  }

  @Override
  public void initialize(UimaContext context) throws ResourceInitializationException {

    super.initialize(context);
    sofaNames = (String[]) getContext().getConfigParameterValue("SofaNames");

    logger.setLevel(Level.OFF);
    logger.log(Level.INFO, "WhitespaceTokenAE initialized.");

  }

}
