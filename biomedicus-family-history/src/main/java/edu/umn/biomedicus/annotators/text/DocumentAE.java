package edu.umn.biomedicus.annotators.text;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.io.File;

/**
 * Sentence Annotator that uses Maximum Entropy sentence detection from OpenNLP. The ME model is trained using a
 * combination of the Genia corpus and a set of clinical notes.
 */
public class DocumentAE extends CasAnnotator_ImplBase {

  public static String METADATA_ANNOTATION_NAME = "edu.umn.biomedicus.type.MetaData";
  public static String METADATA_DOCUMENTID_FEATURE_NAME = "documentId";

  @NotNull
  private Type metaDataType;

  @NotNull
  private Feature documentIdFeature;

  @NotNull
  private UimaContext context;

  @Override
  public void initialize(UimaContext context) throws ResourceInitializationException {

    this.context = context;
    super.initialize(context);

  }

  @Override
  public void process(CAS aCAS) throws AnalysisEngineProcessException {
    CAS mdCAS = aCAS.getView("MetaData");
    String sofaStr = mdCAS.getSofaDataString();
    String fileName = new File(sofaStr).getName();
    AnnotationFS documentId = mdCAS.createAnnotation(metaDataType,0,1);
    documentId.setStringValue(documentIdFeature,fileName);
    mdCAS.addFsToIndexes(documentId);
    System.out.println("Document: " + documentId);
  }

  @Override
  public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

    // Initialize types
    super.typeSystemInit(typeSystem);
    metaDataType = typeSystem.getType(METADATA_ANNOTATION_NAME);
    documentIdFeature = metaDataType.getFeatureByBaseName(METADATA_DOCUMENTID_FEATURE_NAME);

  }
}