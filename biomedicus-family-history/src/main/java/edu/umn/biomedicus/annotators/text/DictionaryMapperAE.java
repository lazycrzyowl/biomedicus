package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.type.FamilyHistoryConstituent;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Sentence Annotator that uses Maximum Entropy sentence detection from OpenNLP. The ME model is trained using a
 * combination of the Genia corpus and a set of clinical notes.
 */
public class DictionaryMapperAE extends CasAnnotator_ImplBase {

    public static String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    public static String FAMILYHISTORY_ANNOTATION_NAME = "edu.umn.biomedicus.type.FamilyHistoryConstituent";
    public static String CHUNK_LABEL_FEATURE_NAME = "label";
    public static String CHUNK_FHROLE_FEATURE_NAME = "fhRole";

    // The character following a match candidate must be a space, symbol, or s for plural
    public static final String terminatingChars = " .,;:'[{]}|-_=+!?s";

    // Define parameters

    public static final String PARAM_FH_ROLE = "fhRoleParameter";
    public static final String PARAM_DICTIONARY = "dictionaryFileParameter";

    @NotNull
    private String fhRoleParameter;
    @NotNull
    private String dictionaryFileParameter;

    @NotNull
    private Type familyHistoryConstituentType;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type tokenType;

    @NotNull
    private Feature labelFeature;
    @NotNull
    Feature identifierFeature;
    @NotNull
    Feature identifierSource;
    @NotNull
    private UimaContext context;
    @NotNull
    private HashMap<String, String> dictionary;
    @NotNull
    private List<DictionaryEntry> smTerms;

    @NotNull
    Logger logger = UIMAFramework.getLogger(DictionaryMapperAE.class);

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {

        this.context = context;
        super.initialize(context);

        fhRoleParameter = (String) context.getConfigParameterValue(PARAM_FH_ROLE);
        dictionaryFileParameter = (String) context.getConfigParameterValue(PARAM_DICTIONARY);
        URL resource = getClass().getClassLoader().getResource(dictionaryFileParameter);
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }

        smTerms = new ArrayList<>();
        try
        {

            List<String> rawTerms = FileUtils.readLines(file);
            for (String s : rawTerms)
            {
                smTerms.add(new DictionaryEntry(s));
            }
        } catch (Exception e)
        {
            throw new ResourceInitializationException(e);
        }
    }

    private DictionaryEntry getFirstMatch(int begin, String comparableText)
    {

        String comparable = comparableText.substring(begin);
        for (DictionaryEntry term: smTerms)
        {
            int offset = term.length();
            if (comparable.startsWith(term.getEntry()))
            {
                if (offset < comparable.length()) {
                    char c = comparable.charAt(offset);

                    if (terminatingChars.indexOf(c) != -1) {
                        // There must be a whitespace somewhere near the match or else it is
                        // just a false positive within a word start. Check for the whitespace
                        // before making a positive term match.
                        if (Character.isWhitespace(c))
                            return term;

                        int terminatingPlus1 = offset + 1;
                        int max = comparable.length();
                        if (terminatingPlus1 < comparable.length()) {
                            char ws = comparable.charAt(terminatingPlus1);
                            if (Character.isWhitespace(ws) || ws == '.')
                                return term;
                        } else {
                            return term;
                        }
                    }
                }
                else
                {
                    return term;
                }
            }
        }
        return null;
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);
//        String documentText = sysCAS.getDocumentText();

        int workingIndex = 0;
        for (AnnotationFS sentence : AnnotationUtils.getAnnotations(sysCAS, Sentence.class))
        {
            String comparableText = sentence.getCoveredText().toLowerCase();
            int sentenceBegin = sentence.getBegin();
            for (AnnotationFS token : AnnotationUtils.getCoveredAnnotations(sysCAS, sentence, tokenType))
            {
                // If the previous match covered multiple tokens, then skip those tokens now
                int tokenBegin = token.getBegin();
                if (tokenBegin < workingIndex)
                    continue;

                // Check if a match starts at this position and add an annotation if it does
                DictionaryEntry matchingTerm = getFirstMatch(tokenBegin- sentence.getBegin(), comparableText);
                if (matchingTerm == null) continue;

                // Add the annotation
                int begin = token.getBegin();
                int end = begin + matchingTerm.length();
                AnnotationFS chunk = sysCAS.createAnnotation(familyHistoryConstituentType, begin, end);
                chunk.setStringValue(labelFeature, fhRoleParameter);
                chunk.setStringValue(identifierFeature, matchingTerm.getId());
                sysCAS.addFsToIndexes(chunk);

                String msg = String.format("\tAdding constituent: %s{%s:%s}", fhRoleParameter, matchingTerm.getEntry(),
                        matchingTerm.getId());
                logger.log(Level.FINEST, msg);
                // Update the working index to the end of the match
                workingIndex = end;
            }
        }
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

        // Initialize types
        super.typeSystemInit(typeSystem);

        // Initialize type
        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
        familyHistoryConstituentType = typeSystem.getType(FamilyHistoryConstituent.class.getCanonicalName());

        // Initialize features
        labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
        identifierFeature = familyHistoryConstituentType.getFeatureByBaseName("identifier");
    }
}