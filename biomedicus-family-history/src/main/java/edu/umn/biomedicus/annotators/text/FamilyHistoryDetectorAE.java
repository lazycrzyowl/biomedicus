/*
Copyright 2010-14 University of Minnesota
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.methods.FamilyHistoryIndicator;
import edu.umn.biomedicus.methods.LexiconUtil;
import edu.umn.biomedicus.methods.RelationFromParseExtractor;
import edu.umn.biomedicus.methods.Span;
import edu.umn.biomedicus.relations.PropositionMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import relex.output.OpenCogScheme;
import weka.classifiers.bayes.NaiveBayesMultinomialText;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.SerializationHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The FamilyHistoryDetector examines each sentence to determine if a sentence contains social history. If true, then a
 * social history annotation is added
 *
 * @author Robert Bill
 */

public class FamilyHistoryDetectorAE extends CasAnnotator_ImplBase {

    private static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";
    private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
    private static final String SECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Section";
    private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    private static final String SUBSECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Subsection";
    private static final String FAMILY_HISTORY_ANNOTATION_NAME = "edu.umn.biomedicus.type.FamilyHistory";
    private static final String SECTION_HEADING_FEATURE_NAME = "heading";
    private static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
    private static final String SECTION_LEVEL_FEATURE_NAME = "level";
    private static final String SECTION_HAS_SUBSECTIONS_FEATURE_NAME = "hasSubsections";
    private static final String CHUNK_LABEL_FEATURE_NAME = "label";

    private static final int SECTION_LEVEL = 0;
    private static final int SUBSECTION_LEVEL = 1;
    private static final String PARAM_MODEL_FILE = "modelFile";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(FamilyHistoryDetectorAE.class);
    @NotNull
    NaiveBayesMultinomialText nb;
    @NotNull
    TypeSystem typeSystem;
    @NotNull
    private Type tokenType;
    @NotNull
    private Type chunkType;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type sectionType;
    @NotNull
    private Type subsectionType;
    @NotNull
    private Type familyHistoryConstituentType;
    @NotNull
    private Feature familyHistoryConstituentLabelFeature;
    @NotNull
    private Feature sectionContentStartFeature;
    @NotNull
    private Type familyHistoryType;
    @NotNull
    private Type observationConstituentType;
    @NotNull
    private Type predicationIndicationType;
    @NotNull
    private Feature fhRoleFeature;
    @NotNull
    private Feature clinicalClassFeature;
    @NotNull
    private Feature sectionHeadingFeature;
    @NotNull
    private Feature sectionHasSubsectionsFeature;
    @NotNull
    private Feature sectionLevelFeature;
    @NotNull
    private Feature chunkLabelFeature;
    @NotNull
    OpenCogScheme opencog;
    @NotNull
    RelationFromParseExtractor relex;

    // RB, 1/30/2014: changed to use Naive Bayes temporarily for testing. Faster, and maybe same results as kernel
    //private String modelFile = "/edu/umn/biomedicus/annotators/text/FH_SGDText_20130811.model";
    @NotNull
    private PropositionMapper propositionMapper;
    @NotNull
    private Type familyConstituentType;
    @NotNull
    private String modelFile = "edu/umn/biomedicus/familyhistory/FamilyHistoryDetector_NaiveBayesNullStemmer.model";
    @NotNull
    private String indicatorWordsFile = "edu/umn/biomedicus/familyhistory/FamilyHistoryIndicatorWords.txt";
    @NotNull
    private FamilyHistoryIndicator indicatorWordDetector;
    @NotNull
    private String[] sofaNames;

    // RB, 1/30/2014: changed to use Naive Bayes temporarily for testing. Faster, and maybe same results as kernel
    // SGDText cls; //Classifier cls;
    private Instances dataRaw;
    private Attribute section;
    private Attribute subsection;
    private Attribute sentence;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {

        super.initialize(context);

        // load the model file
        try
        {
            //cls = (SGDText) SerializationHelper.read(this.getClass().getResourceAsStream(modelFile));
            InputStream resourceInputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(modelFile);
            nb = (NaiveBayesMultinomialText) SerializationHelper.read(resourceInputStream);
        } catch (Exception e)
        {
            System.err.println("FamilyHistoryDetectorAE unable to de-serialize model file " + modelFile);
            System.err.println(e.toString());
            throw new ResourceInitializationException(e);
        }

        // load the dictionary for family history indicator words
        InputStream indicatorWordStream = getClass().getClassLoader().getResourceAsStream(indicatorWordsFile);
        try
        {
            indicatorWordDetector = new FamilyHistoryIndicator(indicatorWordStream);
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }

        // Create Attribute and Instance information needed for the NaiveBayesMultinomial classifier
        ArrayList<Attribute> attributes = new ArrayList<>(3);
        ArrayList<String> classVal = new ArrayList<>();
        classVal.add("FH");  // Family History is 0.0 due to being the first nominal value
        classVal.add("NFH");  // Not Family History is 1.0 due to being the second nominal value

        section = new Attribute("section", (ArrayList<String>) null);
        attributes.add(section);
        subsection = new Attribute("subsection", (ArrayList<String>) null);
        attributes.add(subsection);
        sentence = new Attribute("sentence", (ArrayList<String>) null);
        attributes.add(sentence);

        attributes.add(new Attribute("@@class@@", classVal));
        dataRaw = new Instances("TestInstances", attributes, 0);
        dataRaw.setClassIndex(dataRaw.numAttributes() - 1);

        opencog = new OpenCogScheme();
        opencog.setShowAnaphora(true);

//        relex = new RelationFromParseExtractor();

        logger.log(Level.INFO, "FamilyHistoryDetectorAE initialized successfully.");

    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);
        String sectionHeading = "?";
        String subsectionHeading = "?";

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, sentenceType))
        {

            String sentenceText = sentenceAnnotation.getCoveredText();
            int sentLen = sentenceAnnotation.getEnd() - sentenceAnnotation.getBegin();
            if (sentLen < 9) continue;

            // Identify all the section annotations that cover this sentence. Currently, it can only be
            // an optional section annotation and an optional subsection annotation. In the event there is no
            // covering section annotation detected, the default values for "sectionHeading" and "subsectionnHeading"
            // are set to "?".
            List<AnnotationFS> sectionAnnotationList = AnnotationUtils.getCoveringAnnotations(sysCAS, sectionType, sentenceAnnotation);
            for (AnnotationFS sectionAnnotation : sectionAnnotationList)
            {
                int sectionLevel = sectionAnnotation.getIntValue(sectionLevelFeature);
                if (sectionLevel == SECTION_LEVEL)
                {
                    sectionHeading = sectionAnnotation.getStringValue(sectionHeadingFeature);
                    subsectionHeading = "";  // new sections always terminate old subsections
                } else if (sectionLevel == SUBSECTION_LEVEL)
                {
                    subsectionHeading = sectionAnnotation.getStringValue(sectionHeadingFeature);
                }
            }


            if (isFamilyHistory(sysCAS, sectionHeading, subsectionHeading, sentenceAnnotation))
            {
                String coveredText= sentenceAnnotation.getCoveredText();
                if (!StringUtils.containsAny(coveredText, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray()))
                     continue;
                if (isPsychiatric(coveredText))
                    continue;
                sentenceAnnotation.setStringValue(clinicalClassFeature, "familyhistory");
            }
        }

    }

    private boolean isPsychiatric(String text)
    {
        String[] indicators = {"psychiatr", "behavior", "upset"};
        for (String str : indicators)
        {
            if (text.indexOf(str) != -1) return true;
        }
        return false;
    }

    private void showChunks(List<AnnotationFS> chunks)
    {
        System.out.print("CHUNKS: ");
        for (AnnotationFS chunk : chunks)
        {
            System.out.print(chunk.getStringValue(fhRoleFeature) + chunk.getCoveredText() + "(" + chunk.getStringValue(chunkLabelFeature) + ") ");
        }
        System.out.println("\n");
    }

    private void showIndicatorSeparation(List<AnnotationFS> tokens, List<Span> indicatorSpans)
    {
        if (indicatorSpans.size() > 0)
        {
            Span currentSpan = indicatorSpans.remove(0);
            System.out.print("PHRASE: { ");

            for (int i = 0; i < tokens.size(); i++)
            {
                if (i == currentSpan.getStart())
                {
                    System.out.print("}   IND: { " + tokens.get(i).getCoveredText());
                } else if (i == currentSpan.getEnd())
                {
                    System.out.print(" }   PHRASE: { " + tokens.get(i).getCoveredText());
                    if (!indicatorSpans.isEmpty())
                    {
                        currentSpan = indicatorSpans.remove(0);
                    }
                } else
                {
                    System.out.print(" " + tokens.get(i).getCoveredText());
                }
            }
        }
        System.out.println();
    }

    public boolean isFamilyHistory(CAS sysCAS, String sectionHeading, String subsectionHeading,
                                   AnnotationFS sentenceAnnotation)
    {
        String sentenceText = sentenceAnnotation.getCoveredText();

        // This one's obvious
        if (sentenceText.toLowerCase().indexOf("family history") != -1) return true;
        Matcher matcher = Pattern.compile("[A-Z ]*:").matcher(sentenceText);
        if (matcher.find()) return false;

        boolean sectionResult = LexiconUtil.containsFamilyWord(sectionHeading);
        boolean subsectionResult = LexiconUtil.containsFamilyWord(subsectionHeading);

//        boolean sentenceResult = false;
//        for (AnnotationFS constituent : AnnotationUtils.getCoveredAnnotations(
//                sysCAS, sentenceAnnotation, familyHistoryConstituentType))
//        {
//            String labelFeature = constituent.getStringValue(familyHistoryConstituentLabelFeature);
//            if (labelFeature.equals("family")) sentenceResult = true;
//        }

        if (sectionResult || subsectionResult)
        {
            boolean reject = LexiconUtil.containsExclusionPhrase(sectionHeading);
            return !reject;
        }

//        if (sentenceResult)
//        {

            boolean obs = LexiconUtil.containsObservationWord(sentenceText);
            boolean detail = LexiconUtil.containsDetailWord(sentenceText);

            List<AnnotationFS> famHistConstituents = AnnotationUtils.getCoveredAnnotations(sysCAS, sentenceAnnotation,
                    familyHistoryConstituentType);

            if (famHistConstituents.size() > 0)
            {
                boolean foundFamily = false;
                boolean foundObs = false;
                boolean foundInd = false;
                for (AnnotationFS constituent : famHistConstituents)
                {
                    String label = constituent.getStringValue(familyHistoryConstituentLabelFeature);
                    if (label.equals("family"))
                        foundFamily = true;
                    else if (label.equals("observation"))
                        foundObs = true;
                    else if (label.equals("indicator"))
                        foundInd = true;
                }
                if (foundFamily && foundObs && foundInd)
                {
                   // looks promising, but check evaluation link;
                    boolean reject = LexiconUtil.containsExclusionPhrase(sentenceText);
                    if (!reject) return true;
                }
            }
//        }
        return false;

    }

    public String sentenceToPredications(CAS sysCAS, AnnotationFS sentenceAnnotation)
    {
        List<AnnotationFS> chunks = AnnotationUtils.getCoveredAnnotations(sysCAS, sentenceAnnotation, chunkType);


        return "PREDICATION";
    }

    protected void setArgument(AnnotationFS argument)
    {

    }

    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {

        // Initialize types
        super.typeSystemInit(typeSystem);
        tokenType = typeSystem.getType(TOKEN_ANNOTATION_NAME);
        chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
        sectionType = typeSystem.getType(SECTION_ANNOTATION_NAME);
        subsectionType = typeSystem.getType(SUBSECTION_ANNOTATION_NAME);
        sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
        familyHistoryType = typeSystem.getType(FAMILY_HISTORY_ANNOTATION_NAME);
        familyHistoryConstituentType = typeSystem.getType("edu.umn.biomedicus.type.FamilyHistoryConstituent");
        familyHistoryConstituentLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
//        predicationIndicationType = typeSystem.getType("edu.umn.biomedicus.type.PredicationIndicator");
//        observationConstituentType = typeSystem.getType("edu.umn.biomedicus.type.ObservationConstituent");
        clinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
        sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
        sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
        sectionHasSubsectionsFeature = sectionType.getFeatureByBaseName(SECTION_HAS_SUBSECTIONS_FEATURE_NAME);

        chunkLabelFeature = chunkType.getFeatureByBaseName(CHUNK_LABEL_FEATURE_NAME);
        fhRoleFeature = chunkType.getFeatureByBaseName("fhRole");

        logger.log(Level.INFO, "Sentence Annotator type system initialized.");

    }
}


