package edu.umn.biomedicus.annotators.text;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;

public class RegexDictionaryEntry {
    @NotNull
    private String entry;
    @NotNull
    private int length;
    @NotNull
    private String id;
    @NotNull
    Pattern pattern;


    public RegexDictionaryEntry(String dictionaryRow)
    {
        this.entry = dictionaryRow;
        this.pattern = Pattern.compile(this.entry);
        this.length = entry.length();
    }

    @NotNull
    public Pattern getPattern() { return pattern; }

    @NotNull
    public String getEntry()
    {
        return entry;
    }

    public void setEntry(@NotNull String entry)
    {
        this.entry = entry;
    }

    @NotNull
    public int length()
    {
        return length;
    }

    @NotNull
    public String getId()
    {
        return id;
    }

    public void setId(@NotNull String id)
    {
        this.id = id;
    }

}

