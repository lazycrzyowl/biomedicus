package edu.umn.biomedicus.annotators.text;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.util.Span;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.uimafit.component.CasAnnotator_ImplBase;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Sentence Annotator that uses Maximum Entropy sentence detection from OpenNLP. The ME model is trained using a
 * combination of the Genia corpus and a set of clinical notes.
 */
public class SentenceAE extends CasAnnotator_ImplBase {

    public static final String PARAM_MODEL_FILE = "model_file";
    private static final String DEFAULT_MODEL_FILE = "edu/umn/biomedicus/annotators/text/sentenceME.bin";
    private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    private static final String SECTION_ANNOTATION_NAME = "edu.umn.biomedicus.type.Section";
    private static final String SECTION_HAS_SUBSECTIONS_FEATURE_NAME = "hasSubsections";
    private static final String SECTION_CONTENT_START_FEATURE_NAME = "contentStart";
    private static final String SECTION_HEADING_FEATURE_NAME = "heading";
    private static final String SECTION_LEVEL_FEATURE_NAME = "level";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(SentenceAE.class);
    @NotNull
    private String modelFile;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Type sectionType;
    @NotNull
    private Feature sectionContentStartFeature;
    @NotNull
    private Feature sectionHeadingFeature;
    @NotNull
    private Feature sectionLevelFeature;
    @NotNull
    private Feature sectionHasSubsectionsFeature;
    @NotNull
    private SentenceDetectorME sentenceDetector;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {

        super.initialize(context);
        String sentModelFileName = "edu/umn/biomedicus/annotators/text/sentenceME.bin";
        @NotNull
        InputStream is = getClass().getClassLoader().getResourceAsStream(sentModelFileName);

        SentenceModel model = null;
        try
        {
            model = new SentenceModel(is);
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
        sentenceDetector = new SentenceDetectorME(model);

    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        CAS sysCAS = aCAS.getView("System");

        String documentText = sysCAS.getDocumentText();
        ArrayList<AnnotationFS> sentenceCollector = new ArrayList<>();

        for (AnnotationFS section : AnnotationUtils.getAnnotations(sysCAS, sectionType))
        {

            // apply the sentence detector and get the array of detected spans
            int sectionStart = section.getBegin();
            int sectionEnd = section.getEnd();
            int sectionContentStart = section.getIntValue(sectionContentStartFeature);
            int sectionLevel = section.getIntValue(sectionLevelFeature);
            String debugSectionText = section.getCoveredText();
            int sectionLength = debugSectionText.length();
            String sectionHeading = section.getStringValue(sectionHeadingFeature);

            String sectionText = documentText.substring(sectionContentStart, sectionEnd);

            // Add section heading as a sentence. This might be optional in the future so that
            // headings are skipped when iterating sentences. Check with the group on this.

            for (Span span : sentenceDetector.sentPosDetect(sectionText))
            {
                int start = sectionContentStart + span.getStart();
                int end = sectionContentStart + span.getEnd();
                AnnotationFS annotation = sysCAS.createAnnotation(sentenceType, start, end);
                sentenceCollector.add(annotation);
            }
        }
        commitAnnotations(sysCAS, sentenceCollector);


    }

    public void commitAnnotations(CAS aCAS, List<AnnotationFS> annotations)
    {
        for (AnnotationFS a : annotations)
        {
            aCAS.addFsToIndexes(a);
        }
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {
        super.typeSystemInit(typeSystem);
        sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
        sectionType = typeSystem.getType(SECTION_ANNOTATION_NAME);
        sectionContentStartFeature = sectionType.getFeatureByBaseName(SECTION_CONTENT_START_FEATURE_NAME);
        sectionHeadingFeature = sectionType.getFeatureByBaseName(SECTION_HEADING_FEATURE_NAME);
        sectionLevelFeature = sectionType.getFeatureByBaseName(SECTION_LEVEL_FEATURE_NAME);
        sectionHasSubsectionsFeature = sectionType.getFeatureByBaseName(SECTION_HAS_SUBSECTIONS_FEATURE_NAME);
    }
}