package edu.umn.biomedicus.semantics;

import org.apache.uima.cas.text.AnnotationFS;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Predication {
  List<AnnotationFS> predication = new ArrayList<>();
  AnnotationFS familyArgument = null;
  String familyCode = null;
  AnnotationFS observationArgument = null;
  String observationCode = null;
  AnnotationFS predicationIndicator = null;

  Boolean negationStatus = false;

  String vitalStatusIndicator;

  public void setPredicationIndicator(AnnotationFS predicate) {
    predicationIndicator = predicate;
  }

  public void setFamilyArgument(AnnotationFS argument) {
    familyArgument = argument;
  }

  public void setFamilyCode(String code) {
    this.familyCode = code;
  }

  public void setObservationArgument(AnnotationFS argument) {
    observationArgument = argument;
  }

  public void setNegationStatus(Boolean negation) {
    this.negationStatus = negation;
  }

  public void setVitalStatus(String vitalStatus) {
    this.vitalStatusIndicator = vitalStatus;
  }

  public AnnotationFS getFamilyArgument() {
    return this.familyArgument;
  }

  public AnnotationFS getObservationArgument() {
    return this.observationArgument;
  }

  public AnnotationFS getPredicationIndicator() {
    return this.predicationIndicator;
  }

  public Boolean getNegationStatus() {
    return this.negationStatus;
  }

  public String getVitalStatus() {
    return this.vitalStatusIndicator;
  }

  public String getPredications() {
    StringBuilder predicationText = new StringBuilder();
    for (AnnotationFS annotation : predication) {
      predicationText.append("{");
      predicationText.append(annotation.getCoveredText());
      predicationText.append("}");
    }

    return predicationText.toString();
  }

}
