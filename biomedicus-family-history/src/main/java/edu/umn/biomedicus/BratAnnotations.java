/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus;

import edu.umn.biomedicus.typeWrapper.FamilyHistory;
import edu.umn.biomedicus.typeWrapper.FamilyHistoryConstituent;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *     The brat annotations for family history include the following base types:
 *
 * AgeDeath
 * FM
 * ObsCertainty
 * ObsNegation
 * ObsOtherMod
 * ObsRelevance
 * ObsStrength
 * ObsTemporal
 * Observation
 * OtherStatement
 * SideFamily
 * SubObservation
 * SubjQuantity
 * SubjVitalStatus
 */

public class BratAnnotations {
    private Type familyHistoryConstituentType;
    private Feature constituentLabelFeature;
    private Feature negatedFeature;
    private HashMap<String, FamilyHistoryConstituent> typeMap;
    private ArrayList<FamilyHistory> fhList;

    public BratAnnotations(CAS goldView)
    {
        typeSystemInit(goldView.getTypeSystem());
        typeMap = new HashMap<>();
    }

    public void addTypeAnnotation(CAS goldView, String[] typeAnnotation)
    {
        FamilyHistoryConstituent constituent = createTypeAnnotation(goldView, typeAnnotation, typeAnnotation[1]);
        typeMap.put(typeAnnotation[0], constituent);
    }

    public void addAttributeAnnotation(CAS goldView, String[] bratAnnotation)
    {
        FamilyHistoryConstituent constituent = typeMap.get(bratAnnotation[2]);

        switch (bratAnnotation[1]) {
            case "CertaintyDegree":
                constituent.setCertainty(bratAnnotation[3]);
                break;
            case "ObservationType":
                constituent.setObservationType(bratAnnotation[3]);
                break;
            case "SideFamilyType":
                // sets maternal/paternal value
                constituent.setSideOfFamily(bratAnnotation[3]);
                break;
            case "TimeSpecificity":
                constituent.setTemporalSpecificity(bratAnnotation[3]);
                break;
            default:
        }
    }

    public FamilyHistory getRelationAnnotation(CAS goldView, String[] relationAnnotation)
    {
        FamilyHistoryConstituent family = null;
        switch (relationAnnotation[1]) {
            case "AgeDeathFam":
                // attaches to a family constituent
            case "CertainObs":
            case "FamObsArg":
                // This is the core FamilyHistory type in biomedicus
                FamilyHistory fh = createFamilyHistoryAnnotation(goldView, relationAnnotation);
                break;
            case "NegObs":
                // negation of an observation
            case "ObsFamDeath":
            case "OtherModObs":
                // ignored for now
            case "QuanSubj":
                // ignored for now
            case "RelevanceObs":
                // ignored for now
            case "SideAttrSubj":

            case "SideObsArg":
            case "StrengthObs":
            case "SubObs":
            case "TemporalObs":
            case "VitalStatusSubj":
                // Family member vital status
            default:
        }
        return null;
    }

    private FamilyHistoryConstituent createTypeAnnotation(CAS goldView, String[] typeAnnotation, String label)
    {
        int begin = Integer.parseInt(typeAnnotation[2]);
        int end = Integer.parseInt(typeAnnotation[3]);
        FamilyHistoryConstituent fhc = new FamilyHistoryConstituent(goldView, begin, end, label);
        fhc.addToIndexes(goldView);
        System.out.println("Adding type: " + label + "  :  " + fhc.getCoveredText());
        return fhc;
    }

    private FamilyHistory createFamilyHistoryAnnotation(CAS goldView, String[] relationAnnotation)
    {
        String familyId = relationAnnotation[2].split(":")[1];
        String obsId = relationAnnotation[3].split(":")[1];

        FamilyHistoryConstituent family = typeMap.get(familyId);
        FamilyHistoryConstituent obs = typeMap.get(obsId);
        FamilyHistory fhAnnot = new FamilyHistory(goldView, family, null, obs);
        fhAnnot.addToIndexes(goldView);
        return fhAnnot;
    }

    public void typeSystemInit(TypeSystem typeSystem)
    {
        familyHistoryConstituentType = typeSystem.getType("edu.umn.biomedicus.type.FamilyConstituent");
        constituentLabelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
        negatedFeature = familyHistoryConstituentType.getFeatureByBaseName("negated");
    }
}
