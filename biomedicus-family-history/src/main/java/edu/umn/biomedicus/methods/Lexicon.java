package edu.umn.biomedicus.methods;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Lexicon {

  BufferedReader CSVFile;
  ArrayList<String[]> allData;
  HashMap<String[], String[]> data;
  public Lexicon() {
    data = new HashMap<String[], String[]>();
    allData = new ArrayList<String[]>();

    String fileName = new File(System.getenv().get("BIOMEDICUS_HOME"), "resources/lexicon.csv").toString();
    try {
      CSVFile = new BufferedReader(new FileReader(fileName));
      String dataRow = CSVFile.readLine(); // Read first line.
      // The while checks to see if the data is null. If
      // it is, we've hit the end of the file. If not,
      // process the data.

      while (dataRow != null){
        String[] dataArray = dataRow.split(",");
        String[] key = dataArray[2].toLowerCase().split(LexiconUtil.NONALPHA_SPLIT);
        int debugLen = dataArray.length;
        allData.add(dataArray);
        data.put(key, dataArray);
        dataRow = CSVFile.readLine(); // Read next line of data.
      }
      // Close the file once all data has been read.
      CSVFile.close();

      // End the printout with a blank line.
    } catch (IOException e) {

    }
  }

  public String[] checkLex(String sentence) {
    for (String[] data : allData) {
      if (data[2].toLowerCase().equals(sentence.toLowerCase())) {
        return data;
      }
    }
  return null;
  }
}