package edu.umn.biomedicus.methods;

import edu.umn.biomedicus.annotators.text.WhitespaceTokenAE;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by bill0154 on 1/25/14.
 */
public class SentenceWrapper {

  private static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";
  private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
  private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
  private static final String POS_FEATURE_NAME = "pos";
  private static final String TYPE_FEATURE_NAME = "type";
  private static final String SOURCE_FEATURE_NAME = "source";

  private static final String MODEL_FILE = "/edu/umn/biomedicus/annotators/text/en-chunker.bin";

  @NotNull
  private Type chunkType;

  @NotNull
  private Type tokenType;

  @NotNull
  private Type sentenceType;

  @NotNull
  private AnnotationFS sentence;

  @Nullable
  List<AnnotationFS> tokenizedSentence = null;

  @Nullable
  List<String> tokensAsStrings = null;

  @NotNull
  String[] posTags;

  @NotNull
  private String[] sofaNames;

  @NotNull
  private POSModel posModel;

  @NotNull
  POSTaggerME tagger;

  @NotNull
  CAS aCAS;

  @NotNull
  private static final Logger logger = UIMAFramework.getLogger(WhitespaceTokenAE.class);

  public SentenceWrapper(CAS aCAS, AnnotationFS sentence) throws IOException {

    this.sentence = sentence;
    this.tokenizedSentence = getTokenizedSentence(aCAS, sentence);

  }

  public List<AnnotationFS> getTokenizedSentence(CAS aCAS, AnnotationFS sentenceAnnotation) {

    if (tokenizedSentence == null) {

      tokenizedSentence = new ArrayList<>();
      tokensAsStrings = new ArrayList<>();

      int sentenceBegin = sentenceAnnotation.getBegin();
      int sentenceEnd = sentenceAnnotation.getEnd();

      for (AnnotationFS token : aCAS.getAnnotationIndex(tokenType)) {

        int tokenBegin = token.getBegin();
        int tokenEnd = token.getEnd();

        if (tokenBegin >= sentenceBegin && tokenEnd <= sentenceEnd) {
          tokenizedSentence.add(token);
          tokensAsStrings.add(token.getCoveredText());
        }
      }
    }

    return tokenizedSentence;

  }

  private List<String> getStringArray(List<AnnotationFS> tokenizedSentence) {

    if (tokensAsStrings == null) {
      return new ArrayList<>(0);
    } else {
      return tokensAsStrings;
    }
  }

}
