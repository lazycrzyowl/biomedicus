package edu.umn.biomedicus.methods;


import java.util.ArrayList;
import java.util.List;

public class LexiconUtil {

    public static String[] FAMILY = {"father", "mother", "fathers", "mothers", "daughter", "uncle", "aunt", "sibling",
            "brother", "sister", "family", "cousin", "niece", "nephew", "child", "twin", "parent",
            "maternal", "paternal", "mom", "dad", "sons", "daughters", "uncles", "aunts", "relative",
    "grandparent"};

    public static String[] OBSERVATIONS = {"a-fib", "aodm", "active smoker", "addison's disease",
            "adult-onset diabetes", "alzheimer", "alzheimer disease", "alzheimer's disease",
            "benign breast lump", "cad", "chf", "chf", "copd", "cva", "cancer", "cholesterol",
            "coronary artery bypass graph", "cabg", "coronary", "coronary artery disease", "dm",
            "diabetes", "et", "epstein-barr virus positive", "gu cancer", "htn", "healthy",
            "heart disease", "heartburn", "hodgkin lymphoma", "htn", "huntington disease",
            "huntington disease", "hypertension", "ipf", "mi", "nonsmoker", "obesity", "observation",
            "parkinson disease", "parkinson's disease", "sle", "svt", "tias", "uris", "active smoker",
            "acute myocardial infarction", "adult-onset diabetes", "alcohol", "alcohol abuse",
            "alcohol-related complications", "alcoholism", "alive and well", "allergies", "allergy",
            "anesthesia difficulties", "aneurysm", "angina", "antibiotics", "anxiety", "arthritis", "asthma",
            "attempted suicide twice", "bile duct cancer", "bipolar", "bladder cancer", "bleeding diathesis",
            "bleeding disorder", "bleeding disorders", "blindness", "blood clot", "bone cancer", "brain cancer",
            "brain tumor", "breast ca", "breast cancer", "breast carcinoma", "bypass", "bypass surgery", "cancer",
            "cancer of breast", "cancer of the esophagus", "cancer.", "cancers", "carcinoma", "cardiac disease",
            "cardiac surgery", "cardiovascular disease", "cardiovascular problems", "cataract", "cause unknown",
            "celiac disease", "cerebral hemorrhage", "cerebral palsy", "cerebrovascular accident",
            "cerebrovascular accident", "stroke", "cerebrovascular accidents", "challenges",
            "child death less than 1 year of age", "cholesterol elevated", "cholesterol is normal",
            "cholesterol status", "cholesterol status borderline elevated", "cholesterol status elevated",
            "chronic renal insufficiency", "cirrhosis", "clotting disorders", "colon", "colon ca", "colon cancer",
            "colon cancer.", "colon polyps", "committed suicide", "complications from heart disease",
            "complications from melanoma", "complications of huntington disease", "complications of a dental procedure",
            "condition", "congenital heart disease", "congestive heart failure", "coronary artery disease",
            "coronary disease", "coronary heart disease", "deep venous thrombosis of leg", "dvt", "dementia",
            "dementing illness", "depression", "diabetes", "diabetes mellitus", "diabetes mellitus type 2",
            "diabetic retinopathy", "dialysis", "diseases", "disorders", "doing fine", "drank", "drink",
            "ear infection", "ear infections", "early atherosclerotic heart disease", "early onset diabetes",
            "elevated cholesterol", "emphysema", "end-stage renal disease", "endocrine neoplasia syndromes",
            "enlarged prostate", "epilepsy", "esophageal cancer", "established coronary artery disease", "ex-smoker",
            "food allergies", "foot deformities", "frequent heartburn symptoms", "from central portion of mexico",
            "gastric cancer", "gastric carcinoma", "gastric problems", "genetic syndrome",
            "gestational diabetes mellitus", "glaucoma", "goiter", "good health", "gout attacks",
            "headaches/migraines", "healthy", "hearing loss", "heart attack", "heart disease", "heart failure",
            "heart problems", "heart stopped", "heavy smoker", "hematologic malignancies", "high blood pressure",
            "high cholesterol", "high cholesterol", "high triglycerides", "hypercholesterolemia",
            "hypercoagulable condition", "hypercoagulable state", "hyperlipidemia", "hypertension",
            "hypertension", "hypertrophic cardiomyopathy", "hypothyroidism", "in good health",
            "incessant nonsustained ventricular tachycardia", "increased cholesterol", "infection",
            "inflammatory arthritis", "inoperable brain tumour", "irritable bowel syndrome", "joint problems",
            "kidney and heart failure", "kidney cancer", "kidney disease", "kidney failure", "known cause",
            "kyphosis", "lazy eye", "leg deformities in only one leg", "leukemia", "liver and heart failure",
            "liver cancer", "living and well", "lumbar spine disease", "lung cancer", "lung cancer resection",
            "lung disease", "lupus", "malignancies", "married", "medical problems", "medication", "medications",
            "memory problems", "mesothelioma", "mesothelioma.", "metastatic brain involvement", "metastatic melanoma",
            "migraine", "mild dementing illness", "motor vehicle accident", "multiple myeloma", "murmur",
            "myocardial infarction", "nervous breakdown", "nervous condition", "neurologic diseases",
            "neurologic or rheumatologic issues", "neurological conditions", "neurological disorders",
            "neurological history", "neuromuscular diseases", "neuropathy", "non-small cell lung cancer",
            "nonconvulsive seizure", "nonsmoker", "nursing home", "obese", "obesity", "old age", "open heart surgery",
            "osteoarthritis", "other complaints", "other neurological disease", "other problems",
            "other psychiatric illnesses", "otitis media", "over-the-counter medications", "otc medications",
            "overweight", "pacemaker placed", "pancreatic cancer", "pediatric arthritis", "pes cavus",
            "pneumonia", "polio", "polyps", "positive ppd contacts", "positive asbestos exposure",
            "precancerous polyps", "premature atherosclerotic heart disease", "premature coronary artery disease",
            "prematurely born", "primary brain tumor", "problems", "problems with their knees", "prostate cancer",
            "psoriasis", "psoriatic arthritis", "renal cysts", "renal disease", "respiratory infection",
            "rheumatoid arthritis", "rupture of appendix", "schizophrenia", "second-hand smoke exposures",
            "secondary to cancer", "see a specialist", "seizures", "sickle cell anemia", "sickle cell trait",
            "significant medical problems", "sleep apnea", "small ventricular septal defec", "smoke", "smoked",
            "smoker", "smokers", "some type of paralysis", "stomach cancer", "stomach problems", "stones", "stress",
            "stress disorder", "stroke", "sudden cardiac death", "suicide attempts", "syndromes", "psychiatric",
            "thoracic aortic aneurysm", "thrombotic complication", "thyroid disease", "thyroid problems",
            "thyroid surgery", "twins", "type 2 diabetes", "type i diabetes", "type ii diabetes", "type of cancer",
            "type-two diabetes", "unknown cause", "unspecified heart problem", "use", "uterine cancer",
            "vaginal cancer", "vaginal deliveries", "vascular strokes", "vegetarian",
            "severe leg deformans", "alive"};

    public static String[] DETAILS = {"active", "affects the", "alive and", "alive in", "alive with", "also had",
            "also there is", "also with", "and also", "appears to have", "are", "are noted", "are on", "as well",
            "associated with", "at", "committed", "contracted a", "currently has", "deceased", "declared", "denies",
            "developed", "diagnosed with", "did have", "died", "died after", "died at", "died for", "died from",
            "died in", "died of", "died with", "does", "does have", "does not", "due to", "elevated", "for", "had", "has",
            "has a history of", "has had", "have", "have died", "have had", "in", "in her family",
            "includes", "is", "is a", "is also", "is in", "is notable for", "is present in",
            "is struggling with", "knows nothing of", "negative", "negative for", "negative history", "no", "of",
            "no history of", "no significant family history", "noncontributory", "none", "nonsignificant", "nonsmoker",
            "normal", "not available", "notable for", "nothing", "on", "pass away from", "passed away",
            "passed away from", "pertinent", "pertinent for", "positive for", "positive for family history with",
            "possibly", "post", "prior history of", "remarkable for", "remarkable only for", "reported to have",
            "secondary to", "significant for", "significant for history of", "significant positive", "status",
            "strong family history", "strong family history for", "strong family history of", "strong for",
            "strongly positive for", "succumbed to", "suffered", "suffers from", "there also is", "there is",
            "there is a strong family history of", "there is also", "there is also history of",
            "there is family history of", "there is history of", "treats with", "unaware of", "unchanged", "unknown",
            "unobtainable", "unremarkable", "was", "was diagnosed", "was diagnosed with", "were", "were on",
            "were reported", "who had a", "who has", "with", "with history of"};

    public static String[] EXCLUSIONS = {"mother reported", "report", "reported", "family background",
            "mother states", "father states", "unemployed", "admission", "she is currently", "he is currently"};

    public static String[] NEGATION = {"no", "not", "denies", "without", "never", "hasn", "no one", "negative"};

    public static String[] VITAL = {"dead", "died", "death", "alive", "live", "lives", "passed away", "passed", "deceased", "fatal"};

    public static String[] CONJUNCTION = {"and not", " but not ", " and ", "but ", "both", " nor ", " or ", " yet ", " so "};
    public static String REGEX_SPLIT = "[\\.\\s:;\\(\\)\\-\\?!]";
    public static String NONALPHA_SPLIT = "[^a-zA-Z]+";
    public static Lexicon lex = new Lexicon();

    public static boolean containsFamilyWord(String text)
    {
        String[] testText = text.toLowerCase().split(NONALPHA_SPLIT);
        for (String fam : FAMILY)
        {
            for (String token : testText)
            {
                if (token.startsWith(fam) || token.endsWith(fam))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsNegationWord(String text)
    {
        String[] testText = text.toLowerCase().split(NONALPHA_SPLIT);
        for (String neg : NEGATION)
        {
            for (String token : testText)
            {
                if (token.equals(neg))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsObservationWord(String text)
    {
        String testText = text.toLowerCase();
        for (String obs : OBSERVATIONS)
        {
            if (testText.indexOf(obs) != -1)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean containsDetailWord(String text)
    {
        String testText = text.toLowerCase();
        for (String detail : DETAILS)
        {
            if (testText.indexOf(detail) != -1)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean containsVitalWord(String text)
    {
        String testText = text.toLowerCase();
        for (String vital : VITAL)
        {
            if (testText.indexOf(vital) != -1)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean containsExclusionPhrase(String text)
    {
        String testText = text.toLowerCase();
        for (String ex : EXCLUSIONS)
        {
            if (testText.indexOf(ex) != -1)
            {
                return true;
            }
        }
        return false;
    }

    public static List<Span> findCoordination(String text)
    {
        ArrayList<Span> coordinations = new ArrayList<>();
        String testText = text.toLowerCase();
        for (String conjunction : CONJUNCTION)
        {
            int index = testText.indexOf(conjunction);
            if (index != -1)
            {
                Span result = new Span();
                result.setStart(index);
                result.setEnd(index + conjunction.length());
                coordinations.add(result);
            }
        }
        return coordinations;
    }

    public static String[] checkLexicon(String text)
    {
        return lex.checkLex(text.toLowerCase());
    }
}
