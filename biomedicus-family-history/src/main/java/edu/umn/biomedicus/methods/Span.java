package edu.umn.biomedicus.methods;


public class Span {
  protected int start;
  protected int end;

  public Span(int start, int end) {
    this.start = start;
    this.end = end;
  }

  public Span() {}

  public void setStart(int start) {
    this.start = start;
  }

  public void setEnd(int end) {
    this.end = end;
  }

  public int getStart() {
    return start;
  }

  public int getEnd() {
    return end;
  }

}
