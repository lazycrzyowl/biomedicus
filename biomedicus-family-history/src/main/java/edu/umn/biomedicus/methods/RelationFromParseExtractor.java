package edu.umn.biomedicus.methods;


import org.apache.uima.cas.Feature;
import org.jetbrains.annotations.NotNull;
import org.linkgrammar.LGConfig;
import relex.Document;
import relex.ParseStats;
import relex.ParsedSentence;
import relex.Sentence;
import relex.algs.SentenceAlgorithmApplier;
import relex.anaphora.Antecedents;
import relex.anaphora.Hobbs;
import relex.chunk.ChunkRanker;
import relex.chunk.RelationChunker;
import relex.concurrent.RelexContext;
import relex.corpus.DocSplitter;
import relex.corpus.DocSplitterFactory;
import relex.feature.FeatureNode;
import relex.morphy.Morphy;
import relex.morphy.MorphyFactory;
import relex.output.OpenCogScheme;
import relex.parser.LGParser;
import relex.parser.LocalLGParser;
import relex.parser.RemoteLGParser;
import relex.tree.PhraseMarkup;
import relex.tree.PhraseTree;

import java.util.TreeMap;

public class RelationFromParseExtractor {
    public static final int verbosity = 1;

    public static final int DEFAULT_MAX_PARSES = 4;
    public static final int DEFAULT_MAX_SENTENCE_LENGTH = 1024;
    public static final int DEFAULT_MAX_PARSE_SECONDS = 30;
    public static final int DEFAULT_MAX_PARSE_COST = 1000;
    public boolean do_tree_markup;
    public Antecedents antecedents;
    public boolean do_anaphora_resolution;
    public boolean do_stanford;
    public boolean do_penn_tagging;
    public boolean do_expand_preps;

    Document doco;

    // Provide some basic timing info
    Long starttime;
    TreeMap<String, Long> sumtime;
    TreeMap<String, Long> cnttime;
    @NotNull
    OpenCogScheme opencog;
    private boolean _is_inited;
    private boolean _use_sock;
    private String _lang;
    private String _dict_path;
    private RelexContext relContext;
    private LGParser parser;
    private SentenceAlgorithmApplier sentenceAlgorithmApplier;
    private PhraseMarkup phraseMarkup;
    private Hobbs hobbs;
    private ParseStats stats;
    private Feature sectionHeadingFeature;
    private Feature sectionContentStartFeature;

    public RelationFromParseExtractor()
    {

        _dict_path = System.getProperty("parser.dict.path");
        //(String) context.getConfigParameterValue("parser.dict.path");
        //"/Users/bill0154/Develop/bitbucket/FamilyHistory/resources/data";

        do_anaphora_resolution = false;
        do_tree_markup = false;
        do_stanford = false;
        do_penn_tagging = false;
        do_expand_preps = false;

        parser = new LocalLGParser();
        
        String debug_dict_path = _dict_path;
        parser.setDictPath(_dict_path);
        LGConfig lgConf = parser.getConfig();
        parser.getConfig().setStoreConstituentString(true);
        parser.getConfig().setStoreSense(true);
        Morphy morphy = MorphyFactory.getImplementation(MorphyFactory.DEFAULT_SINGLE_THREAD_IMPLEMENTATION);
        relContext = new RelexContext(parser, morphy);

        sentenceAlgorithmApplier = new SentenceAlgorithmApplier();

        parser.getConfig().setMaxLinkages(DEFAULT_MAX_PARSES);
        parser.getConfig().setMaxCost(DEFAULT_MAX_PARSE_COST);
        parser.getConfig().setMaxParseSeconds(DEFAULT_MAX_PARSE_SECONDS);

        boolean allowSkipped = true; // TODO: change this default to use a parameter grabbed from context
        parser.getConfig().setAllowSkippedWords(allowSkipped);

        // Hobbs-algo stuff.
        phraseMarkup = new PhraseMarkup();
        antecedents = new Antecedents();
        hobbs = new Hobbs(antecedents);

        doco = new Document();

        stats = new ParseStats();
        sumtime = new TreeMap<String, Long>();
        cnttime = new TreeMap<String, Long>();

        do_anaphora_resolution = true;
        do_tree_markup = true;
        do_stanford = true;
        do_penn_tagging = true;
        do_expand_preps = true;

        opencog = new OpenCogScheme();
        opencog.setShowAnaphora(true);

        DocSplitter ds = DocSplitterFactory.create();

    }

    public void getRelationsFromParse(String sentenceText)
    {
        String sentence = null;
        int maxParses = 1;
        int maxParseSeconds = 6;
        int sentence_count = 0;

        Sentence sntc = processSentence(sentenceText);
        doco.addSentence(sntc);

        sentence_count++;
        stats.bin(sntc);

        int np = sntc.getParses().size();
        if (np > maxParses)
        {
            np = maxParses;
        }

        // chunk ranking stuff
        ChunkRanker ranker = new ChunkRanker();
        double parse_weight = 1.0 / ((double) np);
        double votes = 1.0e-20;
        votes += 1.0;
        votes = 1.0 / votes;
        votes *= parse_weight;

        // Print output
        int numParses = 0;
        for (
                ParsedSentence parse : sntc.getParses())
        {

            PhraseTree ptree = new PhraseTree(parse.getLeft());
            parse.setPhraseString(ptree.toString());

            RelationChunker relChunker = new RelationChunker();

            opencog.setParse(parse);
            //System.out.println(opencog.toString());
            String ocRelScheme = opencog.relSchemeToString();

            System.out.println(ocRelScheme);
//            System.out.println(opencog.linkSchemeToString());
//            System.out.println(opencog.printDocument(doco));

        }
    }

    public Sentence processSentence(String sentence)
    {
        starttime = System.currentTimeMillis();

        Sentence sntc = null;
        try
        {
            if (verbosity > 0)
            {
                starttime = System.currentTimeMillis();
            }
            sntc = parseSentence(sentence);
            if (verbosity > 0)
            {
                reportTime("Link-parsing: ");
            }

            for (ParsedSentence parse : sntc.getParses())
            {
                if (do_expand_preps)
                {
                    parse.getLeft().set("expand-preps", new FeatureNode("T"));
                }

                // The actual relation extraction is done here.
                sentenceAlgorithmApplier.applyAlgs(parse, relContext);
                if (do_stanford)
                {
                    sentenceAlgorithmApplier.extractStanford(parse, relContext);
                }
                if (do_penn_tagging)
                {
                    sentenceAlgorithmApplier.pennTag(parse, relContext);
                }

                // Also do a Penn tree-bank style phrase structure markup.
                if (do_tree_markup)
                {
                    phraseMarkup.markup(parse);

                    // Repair the entity-mangled tree-bank string.
                    PhraseTree pt = new PhraseTree(parse.getLeft());
                    parse.setPhraseString(pt.toString());
                }

            }

            // Assign a simple parse-ranking score, based on LinkGrammar data.
            sntc.simpleParseRank();

            // Perform anaphora resolution
            if (do_anaphora_resolution)
            {
                hobbs.addParse(sntc);
                hobbs.resolve(sntc);
            }
        } catch (Exception e)
        {
            System.err.println("Error: Failed to process sentence: " + sentence);
            e.printStackTrace();
        }
        if (verbosity > 0)
        {
            reportTime("RelEx processing: ");
        }
        return sntc;
    }

    private Sentence
    parseSentence(String sentence)
    {
        if (sentence == null)
        {
            return null;
        }

        Sentence sent = null;
        if (sentence.length() < DEFAULT_MAX_SENTENCE_LENGTH)
        {
            sent = parser.parse(sentence);
        } else
        {
            System.err.println("Sentence too long, len=" + sentence.length()
                    + " : " + sentence);
            sent = new Sentence();
        }
        return sent;
    }

    private void reportTime(String msg)
    {
        Long now = System.currentTimeMillis();
        Long elapsed = now - starttime;
        starttime = now;

        Long sum = sumtime.get(msg);
        Long cnt = cnttime.get(msg);
        if (sum == null)
        {
            sum = 0L;
            cnt = 0L;
        }
        cnt++;
        sum += elapsed;
        sumtime.put(msg, sum);
        cnttime.put(msg, cnt);

        Long avg = sum / cnt;
        System.err.println(msg + elapsed + " milliseconds (avg="
                + avg + " millisecs, cnt=" + cnt + ")");
    }


}
