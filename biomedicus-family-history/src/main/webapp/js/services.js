/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

'use strict';

/* Services */

var biomedicusServices = angular.module('biomedicusServices', ['ngResource']);

biomedicusServices.factory('Service', ['$resource',
    function ($resource) {
        return $resource('services/:serviceId.json', {}, {
            query: {method: 'GET', params: {serviceId: 'services'}, isArray: true}
        });
    }]);


biomedicusServices.value('collData',
    {
        entity_types: [
            {
                type: 'FamilyHistoryConstituent',
                /* The labels are used when displaying the annotation, in this case
                 we also provide a short-hand "Per" for cases where
                 abbreviations are preferable */
                labels: ['Constituent', 'Con'],
                // Blue is a nice colour for a person?
                bgColor: '#7fa2ff',
                // Use a slightly darker version of the bgColor for the border
                borderColor: 'darken'
            }
        ],

        relation_types: [
            {
                type: 'Predication',
                labels: ['Predication', 'Pred'],
                // dashArray allows you to adjust the style of the relation arc
                dashArray: '3,3',
                color: 'purple',
                /* A relation takes two arguments, both are named and can be constrained
                 as to which types they may apply to */
                args: [
                    //
                    {role: 'Family', targets: ['FamilyHistoryConstituent'] },
                    {role: 'Observation', targets: ['FamilyHistoryConstituent'] }
                ]
            }
        ],

        event_types: [
            {
                type: 'Indicator',
                labels: ['Possession', 'Posses'],
                bgColor: 'lightgreen',
                borderColor: 'darken',
                /* Unlike relations, events originate from a span of text and can take
                 several arguments */
                arcs: [
                    {type: 'Family', labels: ['Family', 'Fam'] },
                    // Just like the event itself, its arguments can be styled
                    {type: 'Observation', labels: ['Observation', 'Obs'], color: 'green' }
                ]
            }
        ]
    }
);

