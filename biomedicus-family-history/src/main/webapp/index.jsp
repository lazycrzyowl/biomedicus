<html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8"/>

    <link href="http://www.umn.edu/style.css" rel="stylesheet"
          type="text/css" />
    <title>BioMedICUS</title>
    <style type="text/css">
        <!--
        #Footer {
            position: relative;
            bottom: 0px;
        }
        -->
    </style>

    <link rel="icon" type="image/x-icon" href="/ner/favicon.ico" />
    <link rel="shortcut icon" type="image/x-icon"
          href="/ner/favicon.ico" />

</head>
<body>

<div>
    <h1>BioMedICUS Family History</h1>
    <FORM name="myform" METHOD="POST" ACTION="analyze" accept-charset="UTF-8">
        <table>
            <tr><td colspan=2>
                <br>Enter an example family history statement here:<br><br>
                <textarea valign=top name="input"
                          style="width: 450px; height: 9em" rows=31 cols=7>
                </textarea>
            </td></tr>

            <tr><td align=left>
                <input type="submit" name="Analyze"/>
                <input type="button" value="Clear"
                       onclick="this.form.elements['analyze'].value=''"/>
            </td></tr>
        </table>
    </FORM>
</div>
<div id="config_error">
</div>
<h2>Family Constituent(s):</h2>
<div id="family" style="width:700px">
    <div id="family_constituents">
        <p>Loading...</p>
    </div>
</div>

<h2>Observation Constituent(s):</h2>
<div id="observation" style="width:700px">
    <div id="observation_constituents">
        <p>Loading...</p>
    </div>
</div>

<h2>Vital Status:</h2>
<div id="vitals" style="width:700px">
    <div id="coref_loading">
        <p>Loading...</p>
    </div>
</div>

<h2>Negation:</h2>
<div id="negation" style="width:700px">
    <div id="basic_dep_loading">
        <p>Loading...</p>
    </div>
</div>

<h2>Age of Death:</h2>
<div id="age_of_death" style="width:700px">
    <div id="collapsed_dep_loading">
        <p>Loading...</p>
    </div>
</div>

<script type="text/javascript">
    // <![CDATA[
    bratLocation = "../brat";
    // ]]>
</script>
<link rel="stylesheet" type="text/css" href="../brat/style-vis.css"/>
<script type="text/javascript" src="../brat/client/lib/head.load.min.js"></script>
<script type="text/javascript" src="brat.js"></script>
<h>Visualizations by <a href="http://brat.nlplab.org/">brat</a>.</h>
<br/>
<div id="Footer">
    Copyright &copy; 2014,
    <a href="http://www.umn.edu">University of Minnesota NLP-IE</a>, All Rights Reserved.
</div>
</body>
</html>
