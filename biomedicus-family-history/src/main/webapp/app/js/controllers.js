'use strict';

/* Controllers */

var biomedicusControllers = angular.module('biomedicusControllers', []);

biomedicusControllers.controller('ServiceListCtrl', ['$scope', 'Service',
  function($scope, Service) {
    $scope.services = Service.query();
    $scope.orderProp = 'age';
  }]);

biomedicusControllers.controller('ServiceDetailCtrl', ['$scope', '$routeParams', 'Service',
  function($scope, $routeParams, Service) {
    $scope.service = Service.get({serviceId: $routeParams.serviceId}, function(service) {
      $scope.mainImageUrl = service.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);


biomedicusControllers.controller('FamilyHistoryCtrl', ['$scope', '$routeParams', 'Service',
    function($scope, $routeParams, Service) {
        $scope.service = Service.get({serviceId: $routeParams.serviceId}, function(service) {
            $scope.mainImageUrl = service.images[0];
        });

        $scope.setImage = function(imageUrl) {
            $scope.mainImageUrl = imageUrl;
        }
    }]);
