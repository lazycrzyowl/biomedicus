'use strict';

/* Services */

var biomedicusServices = angular.module('biomedicusServices', ['ngResource']);

biomedicusServices.factory('Service', ['$resource',
  function($resource){
    return $resource('services/:serviceId.json', {}, {
      query: {method:'GET', params:{serviceId:'services'}, isArray:true}
    });
  }]);
