'use strict';

/* App Module */

var biomedicusApp = angular.module('biomedicusApp', [
  'ngRoute',
  'biomedicusAnimations',

  'biomedicusControllers',
  'biomedicusFilters',
  'biomedicusServices'
]);

biomedicusApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/services', {
        templateUrl: 'views/main.html',
        controller: 'ServiceListCtrl'
      }).
      when('/services/:serviceId', {
        templateUrl: '../views/partials/service-detail.html',
        controller: 'ServiceDetailCtrl'
      }).
        when('/familyhistory', {
            templateUrl: 'views/familyhistory.html',
            controller: 'FamilyHistoryCtrl'
        }).
      otherwise({
        redirectTo: 'services'
      });
  }]);
