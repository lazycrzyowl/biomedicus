#  This script tests to see if all the senses listed in the
#  edu.umn.biomedicus.acronym gold corpus are included in the edu.umn.biomedicus.acronym manifest file

import csv, glob

# load the manifest file into a data store
manifestFile = open("AcronymCuiManifest.dat","r")
manifest = {}

class Concept:
  def __init__(self, acronym, senseCount, probability, term):
    self.acronym = acronym
    self.senseCount = senseCount
    self.probability = probability
    self.term = term.strip()
    self.cuis = []
    
  def setCuis(self, cuis):
    self.cuis = cuis
    
reader = csv.reader(manifestFile)
for line in reader:
  if line[0].startswith("Acronym"): continue # skip header
  concept = Concept(line[0], line[1], line[2], line[4])
  
  # turn CUI string into a list
  concept.setCuis([cui for cui in line[3].split("|") if line[3].strip()])
  
  # add to manifest dictionary
  manifest[line[4]] = concept
  
# get the gold corpus files and check each one to see if sense is in manifest
files = glob.glob("/workspace/Biomedicus/resources/corpora/edu.umn.biomedicus.acronym/original/*.txt")
missing = {}
for fname in files:
  f = open(fname, "r")
  for line in f.xreadlines():
    term, context, window = line.split("|")
    if not manifest.has_key(term):
      missing[term] = None
      
for k in missing.keys():
  print k
    
  
