
from java.sql import DriverManager
import java

java.lang.Class.forName("com.mysql.jdbc.Driver")
con = DriverManager.getConnection("jdbc:mysql://localhost/umls","umls_users","umls_2010")
sql = "SELECT distinct cui from MRCONSO where str like(?) and lat='ENG' and suppress='N'"
stmt = con.prepareStatement(sql)

#print dir(stmt)

man = open("AcronymManifest.dat","r")
for line in man.xreadlines():
    line = line.strip()
    if len(line)==0: continue
    nym, count, rate, sense = line.split("\t",3)
    sense = sense.strip()
    stmt.setString(1,sense)
    count = 0
    rs = stmt.executeQuery()
    cuis = []
    while rs.next():
        count += 1
        cuis.append(rs.getString(1))
	if count>1:
            print nym, sense, cuis
    count = 0
    #print line.split("\t",3) #nym, sense
'''
ADD attention deficit disorder [u'C0339002', u'C1263846']
AI artificial insemination [u'C0021587', u'C0699895']
AV aortic valve [u'C0003501', u'C1269005']
BP blood pressure [u'C0005823', u'C1272641']
CA cancer [u'C0006826', u'C1306459']
CNS central nervous system [u'C0927232', u'C1269563']
CPK creatine phosphokinase [u'C0010287', u'C0750863']
CVS cardiovascular system [u'C0007226', u'C1269562']
DC discharge [u'C0012621', u'C0600083']
ECG electrocardiogram [u'C0013798', u'C1623258']
ESR erythrocyte sedimentation rate [u'C0086250', u'C1619634']
IM intramuscular [u'C0442117', u'C1556154']
IM intramedullary [u'C0205526', u'C2732619']
IR interventional radiology [u'C0034602', u'C0344093']
LD learning disability [u'C0751265', u'C1321592']
LV Left ventricle [u'C0225897', u'C1269892']
MCA middle cerebral artery [u'C0149566', u'C1269012']
MCP metacarpophalangeal joint [u'C0025525', u'C1269602']
MDI Metered dose inhaler [u'C0418991', u'C0993596']
MRSA methicillin resistant staphylococcus aureus [u'C0343401', u'C1265292']
RA right atrium [u'C0225844', u'C1269890']
'''
