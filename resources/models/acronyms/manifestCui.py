# This script adds the CUIs to the edu.umn.biomedicus.acronym manifest file.

from gov.nih.nlm.nls.metamap import MetaMapApi;
from gov.nih.nlm.nls.metamap import MetaMapApiImpl
import csv

src = open("AcronymManifest.dat","r")
dest = open("AcronymCuiManifest.dat","w")
writer = csv.writer(dest)

api = MetaMapApiImpl()
api.setOptions("-a -I --negex -y -r 900 --longest_lexicon_match")

zeros = 0
ones = 0
twos = 0
threes = 0
fours = 0
lots = 0

count = 0
for line in src.xreadlines():
  if not line.strip(): continue
  if line.startswith("Acronym"): continue # skip header line
  data = line.split("\t")
  phrase = data[3].strip()
  result = api.processCitationsFromString(data[3].strip())
  for utterance in result:
    for pcm in utterance.getUtteranceList():
      all = 0
      cuis = {}
      for can in pcm.getPCMList():
        candidates = can.getCandidateList()
        all += len(candidates)
        maxmatch = 0
        if len(candidates) < 1:
          writer.writerow([data[0], data[1], data[2], "","" , phrase])
          continue
        for cui in candidates:
          termcount = len(cui.getMatchedWords())
          srctermcount = len(phrase.split())
          if termcount > maxmatch:
            maxmatch = termcount
        for cui in candidates:
          termcount = len(cui.getMatchedWords())
          if termcount >= maxmatch and termcount > 0:
            print "src:", phrase.split(), "matched:", cui.getMatchedWords()
            semTypes = cui.getSemanticTypes()
            if len(semTypes) < 1:
              writer.writerow([data[0], data[1], data[2], cui.getConceptId(), "", phrase])
            else:
              for semt in semTypes:
                writer.writerow([data[0], data[1], data[2], cui.getConceptId(), semt, phrase])
      print all, 
      if all==0: zeros += 1
      if all==1: ones += 1
      if all==2: twos += 1
      if all==3: threes += 1
      if all==4: fours += 1
      if all>4: lots += 1
      #print candidates
      for can in candidates:
        pass #print can
        #for ev in candidates:
        #	for position in ev.getPositionalInfo():
        #		x = position.getX()
        #		y = position.getY()
    #print data[0], data[1], data[2], "|".join(cuis.keys()), data[3].strip()
  count += 1
	
print "Count =", count
print "  zeros", zeros,  float(zeros)/float(count)
print "   ones", ones,   float(ones)/float(count)
print "   twos", twos,   float(twos)/float(count)
print " threes", threes, float(threes)/float(count)
print "  fours", fours,  float(fours)/float(count)
print "   lots", lots,   float(lots)/float(count)

print
print float(zeros)/float(count) + float(ones)/float(count) + float(twos)/float(count) + float(threes)/float(count) + float(fours)/float(count) + float(lots)/float(count)

