
#
#
# This program is used to read in a note file,
#			1. Detect all sentences like MSR: Page of, dictated by ...
#			2. Detect section headers and store them in a structure
#			3. Detect end of each section
#


package Splitter;

use strict;

use constant DEBUG_NON_MEDICAL_SENTENCE => 0;
use constant DEBUG_HEADER => 1;
use constant DEBUG_SECTION => 0;

use vars qw($VERSION);

$VERSION = '5.0';

sub norm_header;
sub slurp_read( $ );

#==================================================================
#
#		            	Class methods start here
#
#==================================================================
#

#
# Variables for reg rules, lists and etc
#

my $note = '';
my $content = '';
my @regular_header_regs = ();
my @other_header_regs = ();
my @ListItem_regs = ();
my @PE_sub_headers = ();
my %PE_sub_headers_hash = ();
my @ROS_sub_headers = ();
my %ROS_sub_headers_hash = ();
my %oper_headers_hash = ();
my %oper_header_groups_hash = ();


# An array to store all the sentences
# the sentence here may not be a complete sentence.
# sentences in original note may be splitted by word processing applications.
my @sentences = ();

# Variable to indicate that if the header format of notes is capitalized
my $cap_header_flag = 0;

#
# An hash to store header info, format: sent_id|header string|header_type
# header type:
#	0 - Regular header, on the begining of a sentence ;
# 	1 - non medical header;
#	2 - header inside a sentence.
my %header_info_hash = ();

# An array to store all the raw sections
# The raw sections are sections splitted based on just the headers, no domain knowledge considered like ROS list, etc
my @raw_sections = ();

#
# An array for sections after classification, the format is: parent_id|section_id|header_string|header_type|global_start|length
#
# Note:
#
# parent_id  	Number		if the section is a sub section, parent_id is the section id
#							of its parent section
# section_id 	Number 		start from 0, the section_id for root is 0.
# header_string String		header string
# header_type   Number		Type of the header
#								0 - Regular header, on the begining of a sentence ;
# 								1 - non medical header;
#								2 - header inside a sentence.
#								3 - procedure description section.
#								4 - PE section.
#								5 - ROS section.
# global_start	Number		global position of the section within the notes
# length 		Number		length of the section
#
my @sec_infos = ();
#========================================================================
#
#	                   START OF CLASS METHODS
#
#========================================================================

#
#  Method to create a new Splitter object
#  Input : NONE
#
sub new {

    my $self = {
        sec_infos  	=> [],
        cap_header_flag  	=> [],
        raw_sections  	=> [],
    };

    my $className = shift;
    my $paras_folder = shift;

	@sec_infos = ();
	$cap_header_flag = -1;
	@raw_sections = ();

    bless $self, $className;

    $self->_initialize( $paras_folder );

    return $self;
}


sub split {

	my $self = shift;
	$note = shift;

	@sec_infos = ();
	@raw_sections = ();
	%header_info_hash = ();
	$cap_header_flag = -1;

	# Get all content of a note, "documenttext= 1st ASS'T:"
	if ( $note =~ /documenttext=\s*(FINAL|PRELIMINARY)/s ){

		my $type = $1;
		$note =~ /documenttext=\s*$type\s*(.*)/sg;
		$content = $1 ;
	}
	else {
		$note =~ /documenttext=\s*(.*)/sg;
		$content = $1;
	}

	# Get all sentences, the sentence here may not be a complete sentence.
	# sentences in original note may be splitted by word processing applications.
	@sentences = split /[\n\r]/, $content;

	#
	# For each sentence, try to retrieve headers and
	#
	%header_info_hash = ();
	$self->_extract_headers;

	@raw_sections = ();
	$self->_collect_sections;
	$self->_classify_sections();

	@{ $self->{ sec_infos } } = @sec_infos;
	@{ $self->{ raw_sections } } = @raw_sections;
	$self->{ cap_header_flag } = $cap_header_flag;
}


#===================================================================================
#
# match_header:
#		Used to
#		1. For a paragraph, call _match_header_by_str, the sub will try to retrieve potiential
#			header of the paragraph and map it to a header type
#		2. For unmatched header of a paragraph, try to map it based on lenght, previous header,
#		   current main header ( like PE, ROS and etc )
#
#===================================================================================
sub _extract_headers{

	my $self = shift;
	# A string that indicate some non-medical string pattern
	my $search = '(cc:|CC:|D:|MT:|T:|Document:|LCN:|DSC:|Name:|MR#:|DOB:|Edina,|\/\>|Doctor:|Dictated\s*by|MRN:|Electronically signed|As dictated by|FAIRVIEW SOUTHDALE|<patient client|DICTATED\s*BY|FINAL)';

	#
	# Iterate each sentence and see if
	# there is a regular etrieve potential header from the start of the paragraph.
	#	if so check if there are headers inside the paragraph
	# or these is a non-medical sentence
	# or there are headers inside the sentence
	#
	my $header_id = 0;
	my $cap_header_num = 0;
	my $reg_header_num = 0;
	for ( my $sent_id = 0; $sent_id < scalar( @sentences ) ; $sent_id ++ ) {

		my $flag = 0;
		my $sent = $sentences[$sent_id];

		# First check if the sentence is a non-medical sentence
		foreach ( @other_header_regs ) {

			if ( $sent=~ /($_)/ && ( length( $sent ) < 45 or $sent =~ /$search/ ) ) {

				$header_info_hash{ $header_id++ } = "$sent_id\|$1\|1" ;
				$flag ++;
				last;
			}
		}

		# Check if there is a regular header in the start
		!$flag && do {

			foreach ( @regular_header_regs ) {

				if ( $sent=~ /($_)(.*)/ ) {

					my $header = $1;
					my $remainder = $2;

					# Check if the header is a wrong header.
					my $regular_header_flag = 1;

					$regular_header_flag -- if $header =~ /[ \_]\d+:/;
					$regular_header_flag -- if $header =~ /\b(were|was|be|is|could|can|will|would|shall|must|may|but|do|did|all)\b/i;

					if ( $regular_header_flag > 0 ){

						$header_info_hash{ $header_id++ } = "$sent_id\|$header\|0";

						$reg_header_num ++;
						$cap_header_num ++ if $header eq uc($header);

						# For headers like "recover room. Complications: none. "
						my @sub_sections = $remainder =~ /[\.\?\"\!\;\,] +([A-Z][^\.\?\"\!\;\,\(\)\{\[\]\}]+[\:\-] +)/smg;
						foreach( @sub_sections ){

							if ( !/[ \_]\d+:/
								&& !/:\d+/
								&& !/ Note:/
								&& !/ follow/
								&& !/ of:/
							   )
							{
								$header_info_hash{ $header_id++ } = "$sent_id\|$_\|2";
							}
						}
						$flag ++;
						last;
					}
				} # End of processing a possible header
			} # End of looking for regular headers
		};

		# If the sentenct is not a non-medical sentence and no regular header, see if there are headers inside the sentence
		!$flag && do {
			# For headers like "recover room. Complications: none. "
			my @sub_sections = $sent =~ /[\.\?\"\!\;\,] +([A-Z][^\.\?\"\!\;\,\(\)\{\[\]\}]+[\:\-] +)/smg;
			foreach( @sub_sections ){

				if ( !/[ \_]\d+:/
					&& !/:\d+/
					&& !/ Note:/i
					&& !/\bfollow/i
					&& !/ of:/
				   )
				{
					$header_info_hash{ $header_id++ } = "$sent_id\|$_\|2";
				}
			}
		};
	}
	# If more than one third of the regular headers are capitalized, the header format of the notes is capitalized
	if ( $reg_header_num > 0 ){
		$cap_header_flag = 1 if $cap_header_num/$reg_header_num > 1/3;
	}
}

# Extract all sections based on the header info hash.
# If a header is insider a sentence, extract it also. We don't remove it from the section that encompass it
# At this stage, we will not assign parent section to each section.
sub _collect_sections{

	#
	# Get all the sections into array.
	#
	my @headers = sort { $a <=> $b } keys %header_info_hash;

	# First check if there is section between the first header. sometmes the whole notes has no regular sections.
	my $header_info =  $header_info_hash{ $headers[0] };
	my ($sent_id, $header, $header_type ) = ( -1,'',-1 );
	$header_info =~ /(.*?)\|(.*?)\|(.*)/ && do{  ($sent_id, $header, $header_type ) = ( $1, $2, $3 ); };

	my $only_reg_section = '';
	if ( $sent_id != 0 ){

		my $str = '';
		for ( my $idx = 0 ; $idx < $sent_id; $idx ++ ){

			$str .= "\n$sentences[$idx ]";
		}
		$only_reg_section = $str if $str !~ /^\s*$/;
		my $len = length( $str );
		push @sec_infos, "-1\|-1\|-1\|-1\|0\|$len";
	}

	# Iterate each header now to find sections
	foreach( @headers  ){

		my $head_id = $_;

		my $header_info =  $header_info_hash{ $_ };
		my ($sent_id, $header, $header_type ) = ( -1,'',-1 );
		$header_info =~ /(.*?)\|(.*?)\|(.*)/ && do{  ($sent_id, $header, $header_type ) = ( $1, $2, $3 ); };

		# Get the next header
		# 	If this is a regular header then need to find another regular header or non-medical header
		# 	If this is a header inside a sentence then find a regular header
		#	   or another inside header or non-medical header
		# 	If this is a non-medical header, find another regular or non-medical header

		if ( $header_type == 1 or $header_type == 0){

			my ($sent_id_next, $header_next, $header_type_next ) = ( -1,'',-1 );
			for ( my $n = 1; $head_id + $n <= $#headers; $n ++ ){

				my $header_info_next =  $header_info_hash{ $head_id + $n };
				$header_info_next =~ /(.*?)\|(.*?)\|(.*)/ && do{  ($sent_id_next, $header_next, $header_type_next ) = ( $1, $2, $3 ); };

				last if $header_type_next == 0 or $header_type_next == 1;
			}

			my $first_sent = $sentences[ $sent_id ];
			my $section = '';
			my $search = replace_signs( $header ) ;
			$section = $1 if $first_sent =~ /$search(.*)/smg;

			for ( my $idx = $sent_id + 1; $idx < $sent_id_next; $idx ++ ){ $section .= "\n$sentences[$idx ]"; }
			push @raw_sections, $section;
		}
		else {

			my ($sent_id_next, $header_next, $header_type_next ) = ( -1,'',-1 );
			my $header_info_next =  $header_info_hash{ $head_id + 1 };
			$header_info_next =~ /(.*?)\|(.*?)\|(.*)/ && do{  ($sent_id_next, $header_next, $header_type_next ) = ( $1, $2, $3 ); };

			my $first_sent = $sentences[ $sent_id ];

			# Get the text between a inside header and next header
			my $text = '';
			my $search = replace_signs( $header ) ;
			$text = $1 if $first_sent =~ /$search(.*)/;
			for ( my $idx = $sent_id + 1; $idx < $sent_id_next; $idx ++ ){ $text .= "\n$sentences[$idx ]"; }

			# Get the section body.
			$text =~ /(.*?)\./smg;
			push @raw_sections, "$1\.";
		}
	}

	push @raw_sections, $only_reg_section if $only_reg_section ne '';
}

sub _classify_sections{


	my @headers = sort { $a <=> $b } keys %header_info_hash;
	my @temp_sections = ();
	my %parent_sec_hash = ();

	my $cur_major_header_id = -1;

	foreach( @headers ){

		my $header_id = $_;
		my $header_info =  $header_info_hash{ $header_id };
		my ($sent_id, $header, $header_type ) = ( -1,'',-1 );
		$header_info =~ /(.*?)\|(.*?)\|(.*)/ && do{  ($sent_id, $header, $header_type ) = ( $1, $2, $3 ); };

		# non-medical sections, put into final sections array.
		# Format parent_id|section_id|header_string|global_start|length
		if ( $header_type == 1 ){

			my $global_start = index $note, $raw_sections[ $header_id ];
			my $length = length( $raw_sections[ $header_id ] );

			push @temp_sections, "-1\|$header_id\|$header\|$header_type\|$global_start\|$length";
		}

		# Insider sentence header, find the parent of it
		if ( $header_type == 2 ){

			my $global_start = index $note, $raw_sections[ $header_id ];
			my $length = length( $raw_sections[ $header_id ] );

			# Find the first header before this header that is a regular header
			my ($sent_id_next, $header_next, $header_type_next ) = ( -1,'',-1 );
			my $parent_id = -1;
			for ( my $n = 0; $header_id - $n >= 0; $n ++ ){

				$header_info_hash{ $header_id - $n } =~ /(.*?)\|(.*?)\|(.*)/ && do{

					($sent_id_next, $header_next, $header_type_next ) = ( $1, $2, $3 );
				};

				$parent_id = $header_id - $n if $header_type_next == 0;
				last if $header_type_next == 0;
			}

			push @temp_sections, "$parent_id\|$header_id\|$header\|$header_type\|$global_start\|$length";
		}

		# A regular header.
		# See if it could be a sub section based on domain knowledge and header format clue
		if ( $header_type == 0 ){

			# Check if it is a Physical exame sub sections based on a list of pe sub sections
			my $str = trim( $header );
			$str = uc( trim( $1 ) ) if $header =~ /(.*?)[:-]/;
			my $flag = 0;
			foreach( @PE_sub_headers ){		$flag ++ if $str eq $_;		}


			# Check if it is a Review of system sub sections based on a list of ros sub sections
			if ( !$flag ){	foreach( @ROS_sub_headers ){		$flag ++ if $str eq $_;		}	}

			# If sub section, find the parent section
			my $global_start = index $note, $raw_sections[ $header_id ];
			my $length = length( $raw_sections[ $header_id ] );
			if ( $flag ){

				# Find the first header before this header that is a PE or ROW header
				my ($sent_id_next, $header_next, $header_type_next ) = ( -1,'',-1 );
				my $parent_id = -1;
				my $sub_type = -1;
				for ( my $n = 0; $header_id - $n >= 0; $n ++ ){

					$header_info_hash{ $header_id - $n } =~ /(.*?)\|(.*?)\|(.*)/ && do{

						($sent_id_next, $header_next, $header_type_next ) = ( $1, $2, $3 );
					};


					if (
						( $header_next =~ /physic/i && $header_next =~ /exam/i )
						or $header_next =~ /^objective$/i
						or ( $header_next =~ /review/i && $header_next =~ /system/i )
					   )
					{
						$parent_id = $header_id - $n;

						$sub_type = 4 if ( $header_next =~ /physic/i && $header_next =~ /exam/i ) or $header_next =~ /^objective$/i;
						$sub_type = 5 if $header_next =~ /review/i && $header_next =~ /system/i;

						last;
					}
				}
				$header_type = $sub_type if $sub_type != -1;

				push @temp_sections, "$parent_id\|$header_id\|$header\|$header_type\|$global_start\|$length";
			}
			else{

				# Check if the header is a sub header based on format. Sometimes a section like procedure may have several sub sections and the sub sections start with a capitalized letter like "Exploratory laparotomy:"


				# if the format is like "Exploratory laparotomy:
				if ( ucfirst( $header ) eq $header && uc( $header ) ne $header ){

					# Find the first capitalized header
					my ($sent_id_before, $header_before, $header_type_before ) = ( -1,'',-1 );
					my $parent_id = -1;
					for ( my $n = 1; $header_id - $n >= 0; $n ++ ){

						$header_info_hash{ $header_id - $n } =~ /(.*?)\|(.*?)\|(.*)/ && do{

							($sent_id_before, $header_before, $header_type_before ) = ( $1, $2, $3 );
							$parent_id = $header_id - $n if uc( $header_before ) eq $header_before;
							last if uc( $header_before ) eq $header_before;
						};
					}

					# If the capitalized header before it has word PROCEDURE, the this header may be a subsection of procedure descriptiong
					if ( $header_before =~ /PROCEDURE/ ){

						push @temp_sections, "$parent_id\|$header_id\|$header\|3\|$global_start\|$length";
					}
					else{
						push @temp_sections, "-1\|$header_id\|$header\|$header_type\|$global_start\|$length";
					}
				}
				else{
					push @temp_sections, "-1\|$header_id\|$header\|$header_type\|$global_start\|$length";
				}

			}
		}
	}

	# If the note has only one non-medical section, put the section on the end of the section info array
	if ( $#sec_infos == 0 ){

		my $sec_info = $sec_infos[0] ;
		pop @sec_infos;
		push @sec_infos, @temp_sections;
		push @sec_infos, $sec_info;

	}else{
		push @sec_infos, @temp_sections ;
	}
}
sub norm_header{

	my $str = shift;

	$str =~ s/^\s+//g; $str =~ s/\s+$//g; $str =~ s/\s{2,}/ /g; $str = uc( $str );

	return $str;

}

sub replace_signs( $ ){
	my $str = shift;
	$str =~ s/([\(\)\{\}\[\]\.\*\+\$\!\?\"\'\#\%\@\^\&\/\\])/\\$1/g;
	return $str;
}

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/^\n+//;
	$string =~ s/\s+$//;
	return $string;
}

#===================================================================================
#
# _initialize:
# 		Use to
#		1. Populate global parameter arrays, hashs and vaiables
# 		2. Detect and get rid of newlines in the middle of a sentence.
# 		3. Detect lines for non medical information like date, MRN number, hospital and etc
#
#===================================================================================

sub _initialize {

	my $self = shift;

	my $paras_folder = shift;

	# Regular expressions used to detect regular headers like PROCEDURE, HPI, PE and etc
	push ( @regular_header_regs, '^\s*[A-Z][A-Za-z \-\/]+[\:|\-]+\s+' );  				# 'Pre-operative\s+diagnosis\s+:\s+'
	push ( @regular_header_regs, '^\s*[A-Z\ \'\-\/]+[\:|\-\.]+\s+' );
	push ( @regular_header_regs, '^\s*[A-Z]([A-Z \-\/]|N[Oo]\.\s*\d+)+[\:|\-]+\s*$' );	# 'PRE-OPERATIVE N[oO]. 1\s+'
	push ( @regular_header_regs, '^\s*[A-Z][A-Za-z \-\/]+N[Oo]\.\s*\d+[\:|\-]+\s+' );	# 'PRE-operative N[oO]. 1 :\s+'
	push ( @regular_header_regs, '^\s*[A-Z][A-Za-z \-\/]+\#\s*\d+[\:|\-]+\s+' );
	push ( @regular_header_regs, '^[\w]+ *A(SS|ss)\'T *:' );
	push ( @regular_header_regs, '^[\w]+ *Assistant *:' );
	push ( @regular_header_regs, '^[\w]+ *ASSISTANT *:' );


	# Regulare expressions used to detect other sentences like Dictated by, Page of, MRN: and etc
	push ( @other_header_regs, qr/^((OPERATIVE\s+REPORT|Procedure\s+Date)):\s*$/i  );
	push ( @other_header_regs, qr/^([\w|\s|,|\.|-]+MD)$/  );
	push ( @other_header_regs, qr/^([\w|\s|,|\.|-]+MD, PHD)$/  );
	push ( @other_header_regs, qr/^([\w|\s|,|\.|-]+DPM)$/  );			#ROY MOELLER, DPM
	push ( @other_header_regs, qr/^([a-zA-Z \,\.\-]+[A-Z])$/  );		#ROY MOELLER, C
	push ( @other_header_regs, qr/^(Page *\d+ *of *\d+ *)$/i  );
	push ( @other_header_regs, qr/^([\d|\-|\/]+$)/  );
	push ( @other_header_regs, qr/^(Account *#:*)$/  );
	push ( @other_header_regs, qr/^([A-Z]*[0-9]+[A-Z]*)$/  );
	push ( @other_header_regs, qr/^(EM\s*#*\d+\_*\s*)$/ );				#EM#126 EM124_ EM #126
	push ( @other_header_regs, qr/^[A-Z ]+$/ );
	push ( @other_header_regs, qr/^[A-Z][a-z]+, +[A-Z][a-z]+$/ );

	my $other_str = '(cc:|CC:|D:|MT:|T:|Document:|LCN:|DSC:|Name:|MR#:|DOB:|Edina,|\/\>|Doctor:|Dictated\s*by|MRN:|Electronically signed|As dictated by|FAIRVIEW SOUTHDALE|<patient client|DICTATED\s*BY|FINAL)';
	push ( @other_header_regs, qr/^($other_str)/ );


	#
	# Regular expressions used to detect list item
	#
	push ( @ListItem_regs, '^\d *\.');
	push ( @ListItem_regs, '^[V|I]+ *\.');
	push ( @ListItem_regs, '^\w\.\d');
	push ( @ListItem_regs, '^\w\.\s+');
	push ( @ListItem_regs, '^(FIRST|SECOND|THIRD)+[[:space:]]+STAGE:[[:space:]]+');


	# A reg expression of a list of short sections
	my $short_sections_reg = qr/(anesthesia|Antibiotics|ASAClass|Assistant|EBL|BypassTime|Counts|Drains|EBL|IVF|INJECTABLES|Prep|Radiologist|ReferSurgeon|surgeon|UO|Tourniquet|NON_MEDICAL)/;

	#
	# Populate structures for operative headers, sub headers of PE and sub headers of ROS
	#
	my $file = slurp_read( "$paras_folder/ros_list.txt" );
	my @array = split ( /\n/, $file);

	foreach ( @array ) {

		push ( @ROS_sub_headers, norm_header( $_ ) );
		$ROS_sub_headers_hash{ norm_header( $_ )  } = 1;
	}

	$file = slurp_read( "$paras_folder/pe_list.txt" );
	@array = split ( /\n/, $file);

	foreach ( @array ) {

		push ( @PE_sub_headers, norm_header( $_ ) );
		$PE_sub_headers_hash{ norm_header( $_ ) } = 1;
	}



	#
	# Operative notes headers
	#

	$file = slurp_read( "$paras_folder/oper_headers_hash.txt" );
	@array = split ( /\n/, $file );

	foreach ( @array ) {

		!( /^(.*?)\|(.*)$/ ) && next;
		$oper_headers_hash{ norm_header( $1 ) } = norm_header( $2 ) if ( norm_header( $2 ) );
	}

	#
	# Populate operative notes header groups hash. the groups are collected from the Fairview operative notes
	# The hash is used to quickly find header type of a string, also will be used to match string to posible header
	#
	$file = slurp_read( "$paras_folder/oper_header_groups_hash.txt" );
	@array = split ( /\n/, $file );

	foreach ( @array ) {

	 	!( /^(.*?)\|(.*)$/ ) && next;
	 	$oper_header_groups_hash{ norm_header( $2 ) } = $1 if ( $1 );
	}
}


sub slurp_read( $ ){

	my $file_name = shift;
	local $/ = undef;
	open ( IN, "<$file_name") || return -1;
	my $file = <IN>;
	close IN;
	return $file;
}

1;



