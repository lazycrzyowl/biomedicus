#!/usr/bin/perl

use FindBin qw( $RealBin );
use lib "$RealBin/";
use Splitter;
use strict;
use Switch;
use warnings;


my $splitter = new Splitter("./");
my $note = $ARGV[0];

$splitter->split( $note );
my @sec_infos = @{ $splitter->{ sec_infos } };

for(my $idx = 0; $idx <= $#sec_infos; $idx++  ){

		my $sec_info =  $sec_infos[ $idx ];
		$sec_info =~ /(.*?)\|(.*?)\|(.*?)\|(.*?)\|(.*?)\|(.*)/;
		print "$sec_info\n";
	}

