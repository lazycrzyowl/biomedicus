from org.uimafit.component import JCasAnnotator_ImplBase
from org.uimafit.util import JCasUtil
from java.util import ArrayList
from edu.umn.biomedicus.type import Token
from edu.umn.biomedicus.type import Sentence
from edu.umn.biomedicus.type import Acronym
from org.junit import Assert

class SampleAnnotator(JCasAnnotator_ImplBase):
    
    def initialize(self, context):
        pass
            
    def process(self, jcas):
        # get sentence information
        Assert.assertNotNull(jcas.getDocumentText())
        print "Simple test of jython annotator must be successful if you see this."
