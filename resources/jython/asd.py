from org.uimafit.component import JCasAnnotator_ImplBase
from org.uimafit.util import JCasUtil
from java.util import ArrayList
from edu.umn.biomedicus.type import Token
from edu.umn.biomedicus.type import Sentence
from edu.umn.biomedicus.type import Acronym
from weka.core import Instances

class SampleAnnotator(JCasAnnotator_ImplBase):
    
    def initialize(self, context):
        pass
            
    def process(self, jcas):
        # get sentence information
        sbList = {}
        for sent in JCasUtil.iterate(jcas, Sentence):
            sbList.setdefault(sent.getEnd() - 1, None)
            print sent.getCoveredText()
        
        for acr in JCasUtil.iterate(jcas, Acronym):
            sbList.setdefault(acr.getEnd() -1, None)
            print acr.getCoveredText()
            
        tokenIteratorSet = JCasUtil.iterate(jcas, Token)
        historyToken = None
        targetToken = None
        futureToken = None
        tokenIterator = tokenIteratorSet.iterator()
        if tokenIterator.hasNext():
            historyToken = tokenIterator.next()
        if tokenIterator.hasNext():
            targetToken = tokenIterator.next()
        if tokenIterator.hasNext():
            futureToken = tokenIterator.next()

        # keep track of the tokens we need to add and drop
        addList = ArrayList();
        dropList = ArrayList();

        while targetToken != None:
            tokenText = targetToken.coveredText;
            if len(tokenText)==1 and tokenText=='.':
                if not sbList.has_key(targetToken.getBegin()):
                    tok = Token(jcas)
                    tok.setBegin(historyToken.getBegin())
                    tok.setEnd(targetToken.getEnd())
                    print "Adding token: ", historyToken.getBegin(), targetToken.getEnd()
                    addList.add(tok)
                    dropList.add(historyToken)
                    dropList.add(targetToken)
            historyToken = targetToken
            targetToken = futureToken
            if tokenIterator.hasNext():
                futureToken = tokenIterator.next()
            else:
                futureToken = None
        
        for token in dropList:
            print "Dropping token:", token.getBegin(), token.getEnd(), token.getCoveredText()
            token.removeFromIndexes()
        for token in addList:
            token.addToIndexes(jcas)
            print token.getBegin(), token.getEnd(), "<-- location of new token is here"
            print token.getCoveredText(), "<--- the new token is here."
                
            
        