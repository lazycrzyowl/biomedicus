package edu.umn.biomedicus.pos.tnt;

import org.apache.commons.math3.stat.Frequency;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

//import gov.nih.nlm.nls.lexCheck.Lib.LexRecord;

public class ConditionalFreqStore implements Serializable {
	private static final long serialVersionUID = 7526471155622776147L;

	public Frequency unigram;

	public HashMap<String, Frequency> wordFreq;

	public HashMap<String, Frequency> bigram;

	public HashMap<String, HashMap<String, Frequency>> trigram;

	public HashMap<String, Frequency> wordend;

	public HashMap<String, Frequency> wordendBigram;

	public HashMap<String, HashMap<String, Frequency>> wordendTrigram;

	public boolean caseSensitive;

	private String t_minus_one = "BOS";

	private String t_minus_two = "BOS";

	public double l1 = 0.0;

	public double l2 = 0.0;

	public double l3 = 0.0;

	public ConditionalFreqStore(boolean caseSensitive) {
		// Create lexical frequency storage for unigrams and words.
		unigram = new Frequency();
		wordFreq = new HashMap<String, Frequency>();

		// Create contextual frequency storage for bigrams.
		// The HashMap key is t-1, the value is a frequency object
		bigram = new HashMap<String, Frequency>();
		bigram.put("BOS", new Frequency());

		// Create contextual frequency storage for trigrams.
		// This uses nested HashMaps. The key in the hashmap named trigram
		// is t-2 and the value is another hashmap whose keys are t-1.
		// Values of the second HashMap are Frequency objects
		// that are incremented for each word
		trigram = new HashMap<String, HashMap<String, Frequency>>();
		trigram.put("BOS", new HashMap<String, Frequency>());
		trigram.get("BOS").put("BOS", new Frequency());

		// Create a word-ending frequency store. The optimal length of word
		// endings isn't clear. Numbers between 3-5 have proven optimal in other
		// papers, so it testing each of these to see which is optimal for the
		// clinical+genia corpus is best.
		// test #1 is 4 characters
		wordend = new HashMap<String, Frequency>();
		wordendBigram = new HashMap<String, Frequency>();
		wordendTrigram = new HashMap<String, HashMap<String, Frequency>>();

		this.caseSensitive = caseSensitive;
	}

	public boolean isKnown(String word) {
		return wordFreq.containsKey(word.toLowerCase());
	}

	public Iterator<Comparable<?>> getCandidateTagsForKnownWord(String word) {
		return wordFreq.get(word).valuesIterator();
	}

	public void newSentence() {
		// Add beginning of sentence
		t_minus_two = t_minus_one;
		t_minus_one = "BOS";
	}

	public void feedTape(String word, String tag) {
		// increment each frequency bucket
		unigram.addValue(tag);
		if (t_minus_one.equals("BOS") && tag != "NP") {
			word = word.toLowerCase();
		} else {
			boolean upper = true;
			for (char c : word.toCharArray()) {
				if (Character.isLowerCase(c)) {
					upper = false;
				}
			}
			if (upper) {
				word = word.toLowerCase();
			}
			word = word.toLowerCase();
		}

		if (wordFreq.containsKey(word)) {
			wordFreq.get(word).addValue(tag);
		} else {
			wordFreq.put(word, new Frequency());
			wordFreq.get(word).addValue(tag);
		}

		if (bigram.containsKey(t_minus_one)) {
			bigram.get(t_minus_one).addValue(tag);
		} else {
			bigram.put(t_minus_one, new Frequency());
			bigram.get(t_minus_one).addValue(tag);
		}

		if (trigram.containsKey(t_minus_two)) {
			if (trigram.get(t_minus_two).containsKey(t_minus_one)) {
				trigram.get(t_minus_two).get(t_minus_one).addValue(tag);
			} else {
				trigram.get(t_minus_two).put(t_minus_one, new Frequency());
				trigram.get(t_minus_two).get(t_minus_one).addValue(tag);
			}
		} else {
			trigram.put(t_minus_two, new HashMap<String, Frequency>());
			trigram.get(t_minus_two).put(t_minus_one, new Frequency());
			trigram.get(t_minus_two).get(t_minus_one).addValue(tag);
		}

		// capture word ending frequency
		int endingsize = 4;
		String ending = "";
		if (word.length() < endingsize) {
			ending = word;
		} else {
			ending = word.substring(word.length() - endingsize);
		}
		if (wordend.containsKey(ending)) {
			wordend.get(ending).addValue(tag);
		} else {
			wordend.put(word, new Frequency());
			wordend.get(word).addValue(tag);
		}

		if (wordendBigram.containsKey(t_minus_one)) {
			wordendBigram.get(t_minus_one).addValue(tag);
		} else {
			wordendBigram.put(t_minus_one, new Frequency());
			wordendBigram.get(t_minus_one).addValue(tag);
		}

		if (wordendTrigram.containsKey(t_minus_two)) {
			if (wordendTrigram.get(t_minus_two).containsKey(t_minus_one)) {
				wordendTrigram.get(t_minus_two).get(t_minus_one).addValue(tag);
			} else {
				wordendTrigram.get(t_minus_two).put(t_minus_one, new Frequency());
				wordendTrigram.get(t_minus_two).get(t_minus_one).addValue(tag);
			}
		} else {
			wordendTrigram.put(t_minus_two, new HashMap<String, Frequency>());
			wordendTrigram.get(t_minus_two).put(t_minus_one, new Frequency());
			wordendTrigram.get(t_minus_two).get(t_minus_one).addValue(tag);
		}

		t_minus_two = t_minus_one;
		t_minus_one = tag;
	}

	public void calculateLambdas() {
		double tl1 = 0.0;
		double tl2 = 0.0;
		double tl3 = 0.0;

		// loop through the context (history) beginning with -2 (trigram)
		Iterator<String> tminus2_iter = trigram.keySet().iterator();
		while (tminus2_iter.hasNext()) {
			t_minus_two = tminus2_iter.next();
			// loop through -1 (bigram) context
			Iterator<String> tminus1_iter = trigram.get(t_minus_two).keySet().iterator();
			while (tminus1_iter.hasNext()) {
				t_minus_one = tminus1_iter.next();
				// loop through each tag
				Iterator<Comparable<?>> tagiter = trigram.get(t_minus_two).get(t_minus_one).valuesIterator();
				while (tagiter.hasNext()) {
					String tag = (String) tagiter.next();
					double trigramFrequency = trigram.get(t_minus_two).get(t_minus_one).getCount(tag);
					// As advised in the Python version, ignore what occurred
					// once
					if (trigramFrequency < 2) {
						continue;
					}
					// @TODO: add safe div that returns -1 if the denominator is
					// 0
					double c3 = ((double) trigramFrequency - 1)
					                                / ((double) trigram.get(t_minus_two).get(t_minus_one).getSumFreq() - 1);
					double c2 = ((double) bigram.get(t_minus_one).getCount(tag) - 1)
					                                / ((double) bigram.get(t_minus_one).getSumFreq() - 1);
					double c1 = ((double) unigram.getCount(tag) - 1) / ((double) unigram.getSumFreq() - 1);

					// when max(c1,c2,c3) = c1
					if ((c1 > c3) && (c1 > c2)) {
						tl1 += trigramFrequency;
					}
					// when max(c1,c2,c3) = c2
					else if ((c2 > c3) && (c2 > c1)) {
						tl2 += trigramFrequency;
					}
					// when max(c1,c2,c3) = c3
					else if ((c3 > c2) && (c3 > c1)) {
						tl3 += trigramFrequency;
					}
					// when max(c1,c2,c3) = c2,c3
					else if ((c3 == c2) && (c3 > c1)) {
						tl2 += ((double) trigramFrequency) / 2.0;
						tl3 += ((double) trigramFrequency) / 2.0;
					}
					// when max(c1,c2,c3) = c1,c2
					else if ((c2 == c1) && (c1 > c3)) {
						tl1 += ((double) trigramFrequency) / 2.0;
						tl2 += ((double) trigramFrequency) / 2.0;
					}
					// there's a problem if here, should raise exception, but
					// not sure what yet.
					else { // some error handling; }
					}
				}
			}
		}
		// Lambda normalization: Make l1+l2+l3 = 1
		l1 = tl1 / (tl1 + tl2 + tl3);
		l2 = tl2 / (tl1 + tl2 + tl3);
		l3 = tl3 / (tl1 + tl2 + tl3);
	}

//	public void buildBaseDictionary(String pathToLexAccess) {
//		LexiconUtil lexicon = new LexiconUtil(pathToLexAccess);
//		for (LexRecord record : lexicon.getAllLexiconEntries()) {
//			if (wordFreq.containsKey(record.GetBase())) {
//				wordFreq.get(record.GetBase()).addValue(lexicon.specialist2ptb(record.GetCategory()));
//			} else {
//				wordFreq.put(record.GetBase(), new Frequency());
//				wordFreq.get(record.GetBase()).addValue(lexicon.specialist2ptb(record.GetCategory()));
//			}
//		}
//	}
}