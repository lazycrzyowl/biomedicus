package edu.umn.biomedicus.tagging;

import edu.umn.biomedicus.core.ResourceFactory;
import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * PosTagger base application.
 */
public final class PosTagger
{
    @NotNull
    protected AnalysisEngine ae;
    @NotNull
    protected CAS aCAS;
    @NotNull
    protected CAS systemView;
    @NotNull
    protected Feature pos;

    public static void main(String[] args) throws Exception
    {
        PosTagger tagger = new Builder().build();
        String test = "This is a test of the POS tagger.";
        tagger.process(test);
    }

    private PosTagger(Builder builder) throws ResourceInitializationException
    {
        ae = builder.getAE();
        aCAS = ae.newCAS();
        pos = aCAS.getTypeSystem().getType(Token.class.getCanonicalName()).getFeatureByBaseName("pos");
        systemView = CASUtil.getSystemView(aCAS);
    }

    public CAS newCAS() throws ResourceInitializationException { return ae.newCAS(); }

    public void process(CAS aCAS) throws AnalysisEngineProcessException { ae.process(aCAS); }

    public void process(String text) throws AnalysisEngineProcessException
    {
        aCAS.reset();
        CAS systemView = CASUtil.getSystemView(aCAS);
        systemView.setDocumentText(text);

        Tokenizer tokens = null;
        SentenceSegmenter sentences = null;
        try {
            tokens = new Tokenizer.Builder().build();
            sentences = new SentenceSegmenter.Builder().build();
        }
        catch (ResourceInitializationException e)
        {
            throw new AnalysisEngineProcessException(e);
        }
        tokens.process(systemView);
        sentences.process(systemView);
        ae.process(systemView);

        for (AnnotationFS token : AnnotationUtils.getAnnotations(systemView, Token.class))
        {
            System.out.println(String.format("%s : %s", token.getCoveredText(), token.getStringValue(pos)));
        }
    }

    public AnalysisEngine getAE() { return ae; }

    public static class Builder
    {
        private String descriptor = "edu/umn/biomedicus/tagging/tnt/tntPosAE.xml";
        private List<ServiceProviderDescription> serviceProviders  = new ArrayList<>();
        private Properties properties;
        private String serviceProviderClassName;
        private String modelResource;

        public Builder() {}

        public Builder(String descriptor) throws IOException
        {
            this.descriptor = descriptor;
        }


        public PosTagger build()  throws ResourceInitializationException
        {
            return new PosTagger(this);
        }

        public AnalysisEngine getAE() throws ResourceInitializationException
        {
            URL resource = this.getClass().getClassLoader().getResource(descriptor);
            return ResourceFactory.createAnalysisEngine(resource, serviceProviders);
        }
    }
}

