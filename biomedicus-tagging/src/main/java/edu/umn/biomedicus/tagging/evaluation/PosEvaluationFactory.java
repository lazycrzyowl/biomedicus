/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.tagging.evaluation;

/**
 * 
 * @author Philip Ogren
 * 
 */

//public class PosEvaluationFactory implements EvaluationFactory {
public class PosEvaluationFactory {

//	public AnalysisEngineDescription createEvaluationAggregate(File evaluationDirectory)
//	                                throws ResourceInitializationException {
//		AnalysisEngineDescription evaluator = AnalysisEngineFactory.createPrimitiveDescription(PosEvaluatorAE.class,
//		                                ComponentFactory.getTypeSystem(), PosEvaluatorAE.PARAM_OUTPUT_DIRECTORY,
//		                                evaluationDirectory.getPath());
//		return evaluator; // it's perfectly fine to return a primitive here -
//		// the interface doesn't care.
//	}
//
//	public void aggregateEvaluationResults(List<File> evaluationDirectories, File outputDirectory) throws Exception {
//		ConfusionMatrix aggregateConfusionMatrix = new ConfusionMatrix();
//
//		for (File evaluationDirectory : evaluationDirectories) {
//			ObjectInputStream input = new ObjectInputStream(new FileInputStream(new File(evaluationDirectory,
//			                                PosEvaluatorAE.CONFUSION_FILE_NAME)));
//			ConfusionMatrix confusionMatrix = (ConfusionMatrix) input.readObject();
//			aggregateConfusionMatrix.add(confusionMatrix);
//		}
//
//		PrintStream out = new PrintStream(new File(outputDirectory, PosEvaluatorAE.OUTPUT_FILE_NAME));
//		PosEvaluatorAE.printEvaluationResults(aggregateConfusionMatrix, out);
//		out.close();
//	}
//
}
