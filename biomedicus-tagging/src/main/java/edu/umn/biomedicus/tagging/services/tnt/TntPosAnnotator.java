/* 
 Copyright 2011-12 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.tagging.services.tnt;

import java.io.InputStream;
import java.util.List;

import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.pos.tnt.ConditionalFreqStore;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.resource.ResourceInitializationException;

import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;

public class TntPosAnnotator extends CasAnnotator_ImplBase {

	public static final String PARAM_MODEL_FILE = "modelFile";
	private String modelFile;
    private Type tokenType;
    private Feature posFeature;

	protected ConditionalFreqStore cf;
	private TagSearch search;

	@Override
	public void initialize(UimaContext uimaContext) throws ResourceInitializationException {
		super.initialize(uimaContext);

		// Load frequency data
        modelFile = (String) uimaContext.getConfigParameterValue(PARAM_MODEL_FILE);
		InputStream modelInputStream = getClass().getClassLoader().getResourceAsStream(modelFile);
		search = new TagSearch(modelInputStream, 100);
	}

	@Override
	public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS systemView = CASUtil.getSystemView(aCAS);
		AnnotationIndex<Sentence> sents = (AnnotationIndex<Sentence>) AnnotationUtils.getAnnotations(systemView, Sentence.class);
		AnnotationIndex<Token> tokens = (AnnotationIndex<Token>) AnnotationUtils.getAnnotations(systemView, Token.class);

		// Make sure there are sentence annotations in the CAS
		if (sents.size() == 0) {
			System.err.println("POS Tagging failed:  No annotations of 'Sentence' exist in the Cas object. Sentence annotations are required!");
			throw new AnalysisEngineProcessException();
		} else if (tokens.size() == 0) {
			System.err.println("POS Taggin failed: No annotations of 'Token' exist in the Cas object. Token annotations are required!");
			throw new AnalysisEngineProcessException();
		}

		// Loop through each sentence
		for (AnnotationFS sentence : sents) {
			// tell search class that we started a new sentence
			search.newSentence();


			// loop through the tokens of the sentence
            List<AnnotationFS> tokenList = AnnotationUtils.getCoveredAnnotations(sentence, Token.class);
			for (AnnotationFS token : tokenList) {
				String category = search.add(token.getCoveredText());
                token.setStringValue(posFeature, category);
			}
		}
	}

    public void typeSystemInit(TypeSystem ts)
    {
        tokenType = ts.getType(Token.class.getCanonicalName());
        posFeature = tokenType.getFeatureByBaseName("pos");
    }
}
