/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package edu.umn.biomedicus.tagging.services;

import edu.umn.biomedicus.core.utils.PathUtils;
import opennlp.model.MaxentModel;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.apache.uima.UIMAFramework;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.uimafit.descriptor.ConfigurationParameter;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class OpenNLPTaggingServiceProvider
        implements TaggingServiceProvider, SharedResourceObject {

    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(OpenNLPTaggingServiceProvider.class);
    @NotNull
    POSTaggerME tagger;
    @NotNull
    private POSModel posModel;

    private MaxentModel model;
    @ConfigurationParameter(mandatory = false, defaultValue = "posMEModel.bin")
    private String modelFileName;

    // AcronymFeatureExtractor afe = new AcronymFeatureExtractor();

    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {
        URL resource = aData.getUrl();
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        }
        catch (IOException e)
        {throw new ResourceInitializationException(e);}

        posModel = new POSModelLoader().load(file);
        tagger = new POSTaggerME(posModel);
    }

    @Override
    public String[] getTags(String[] tokensAsStrings)
    {
        String[] tags = tagger.tag(tokensAsStrings);
        return tags;
    }


}