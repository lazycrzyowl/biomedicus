/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package edu.umn.biomedicus.tagging.featureextractors;

import org.apache.uima.cas.text.AnnotationFS;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * This class extracts the feature list required for the trigram plus suffix max-ent part-of-speech tagger.
 *
 * @author Robert Bill
 */
public class TrigramAndSuffixFeatureExtractor {
    int suffixSize = 3;
    @NotNull
    private ArrayDeque<AnnotationFS> history;
    private ArrayList<String> posHistory;
    private ArrayList<String> textHistory;

    public TrigramAndSuffixFeatureExtractor(int suffixSize)
    {
        this.suffixSize = suffixSize;
        history = new ArrayDeque<>(suffixSize);
        posHistory = new ArrayList<>(suffixSize);
        textHistory = new ArrayList<>(suffixSize);
    }

    public String[] getTrigramFeatures(AnnotationFS token, String previousPOS)
    {
        // word, wordSuffix, pos-1, word-1Suffix, pos-2, word-2Suffix, pos-3,
        // word-3Suffix, suffix, suffix-1, suffix-2, lexAccessPos

        ArrayList<String> features = new ArrayList<>(8);

        // add the token
        features.add(token.getCoveredText());
        features.add(" ");
        features.add(getSuffix(token.getCoveredText()));
        features.add(" ");

        for (int i = 0; i < posHistory.size(); i++)
        {
            String preText = textHistory.get(i);
            String prePos = posHistory.get(i);
            features.add(prePos);
            features.add(" ");
            features.add(getSuffix(preText));
            features.add(" ");
        }

        String[] featureSet = new String[features.size()];
        return features.toArray(featureSet);
    }

    public void addHistory(AnnotationFS token)
    {
        String tokenPOS = token.getStringValue(token.getType().getFeatureByBaseName("pos"));
        posHistory.add(tokenPOS);
        textHistory.add(token.getCoveredText());

        if (posHistory.size() > 3)
        {
            posHistory.remove(0);
            textHistory.remove(0);
        }
    }

    private String getSuffix(String word)
    {
        if (word.length() < suffixSize)
        {
            return word;
        } else
        {
            return word.substring(word.length() - suffixSize);
        }
    }
}