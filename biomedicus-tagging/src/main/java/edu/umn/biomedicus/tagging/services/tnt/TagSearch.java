package edu.umn.biomedicus.tagging.services.tnt;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import edu.umn.biomedicus.pos.tnt.ConditionalFreqStore;

import edu.umn.biomedicus.type.Token;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.text.AnnotationFS;

public class TagSearch {
	private int beamSize;
	private ConditionalFreqStore cf;
	private String history1; // word one before token
	private String history2; // word two before token
	private String survivor;
	ArrayList<Token> sentence = new ArrayList<Token>();

    public TagSearch()
    {
        String modelFile = "edu/umn/biomedicus/tagging/genia-tnt-model.ser"; //default file
        this.beamSize = 100; //default beam
        InputStream modelInputStream = this.getClass().getClassLoader().getResourceAsStream(modelFile);
        // open and read model file that was created with the TrainTntTagger
        // analysis engine
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(modelInputStream);
            cf = (ConditionalFreqStore) ois.readObject();
        } catch (FileNotFoundException ex) {
            System.err.println("Unable to open the genia-tnt-model.ser resource.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.err.println("genia-tnt-model.ser file was opened, but unable to read file.");
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

	/**
	 * @param modelInputStream
	 *            The input stream of the serialized model file
	 * @param beamSize
	 *            The size of the n-best list to be returned.
	 */
	public TagSearch(InputStream modelInputStream, int beamSize) {
		this.beamSize = beamSize;
		// open and read model file that was created with the TrainTntTagger
		// analysis engine
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(modelInputStream);
			cf = (ConditionalFreqStore) ois.readObject();
		} catch (FileNotFoundException ex) {
			System.err.println("Unable to open the genia-tnt-model.ser resource.");
			ex.printStackTrace();
		} catch (IOException ex) {
			System.err.println("genia-tnt-model.ser file was opened, but unable to read file.");
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Adds an item to the beam. Prune low-probability candidates
	 *
	 * 
	 *            TODO: probability is a bit difficult to debug because small
	 *            numbers. switch to log(probability) so lowest score is
	 *            survivor
	 * 
	 */
	public String add(String token) {
		history2 = history1;
		history1 = survivor;
        String word = token.toLowerCase();
		String survivor = null;
		double odds = 0.0;
		HashMap<String, Double> candidates = new HashMap<String, Double>();// Q(ueue)
		HashMap<String, Double> survivors = new HashMap<String, Double>(); // E(nqueued)
		if (cf.isKnown(word)) {
            Iterator<Comparable<?>> candidateTags = cf.wordFreq.get(word.toLowerCase()).valuesIterator();
			for (int i = 0; candidateTags.hasNext(); i++) {
				long uniProb = 0;
				long biProb = 0;
				long triProb = 0;
				String candidate = (String) candidateTags.next();
				uniProb = cf.unigram.getCount(candidate);
				if (cf.bigram.containsKey(history1)) {
					biProb = cf.bigram.get(history1).getCount(candidate);

					if (cf.trigram.containsKey(history2) && cf.trigram.get(history2).containsKey(history1)) {
						triProb = cf.trigram.get(history2).get(history1).getCount(candidate);
					}
				}
				double wordProb = ((double) cf.wordFreq.get(word).getCount(candidate))
				                                / ((double) cf.unigram.getCount(candidate));
				double p = (cf.l1 * uniProb) + (cf.l2 * biProb) + (cf.l3 * triProb);
				double p2 = p * wordProb;
				candidates.put(candidate, p2);
				if (p2 > odds) {
					survivor = candidate;
					odds = p2;
					survivors.put(candidate, odds);
				}
			}
		} else {
			int endingsize = 4; // TODO: parameterize endingsize
			String ending = "";
			if (word.length() < endingsize) {
				ending = word;
			} else {
				ending = word.substring(word.length() - endingsize);
			}
			Iterator<Comparable<?>> candidateTags = null;
			if (cf.wordend.containsKey(ending)) {
				candidateTags = cf.wordend.get(ending).valuesIterator();
			} else {
				String text = word;
				// It's unknown and no guesses. Call it a NN for now,
				// but add capitalization (edu.umn.biomedicus.acronym) checking, etc.
				boolean isNumber = false;
				try {
					Double.parseDouble(text);
					isNumber = true;
				} catch (NumberFormatException e) {
					isNumber = false; // not necessary really
				}

				if (isNumber) {
					return "CD";
				} else {
                    return "NN";
                }
			}
			while (candidateTags.hasNext()) {
				String candidate = (String) candidateTags.next();
				if (cf.wordend.get(ending).getPct(candidate) > odds) {
					survivor = candidate;
					odds = cf.wordend.get(ending).getPct(candidate);
				}
			}
			return survivor;
		}
        return (survivor != null) ? survivor : "NN";
	}

	public void newSentence() {
		// reset hypothesis list at the start of a new sentence
		history2 = "BOS";
		history1 = "BOS";
		survivor = "Unk";
	}
}
