/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.tagging;

import edu.umn.biomedicus.core.ResourceFactory;
import edu.umn.biomedicus.core.factories.ServiceProviderDescription;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.segmenting.SentenceSegmenter;
import edu.umn.biomedicus.tagging.services.OpenNLPTaggingServiceProvider;
import edu.umn.biomedicus.tokenizing.Tokenizer;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Tokenizer base app
 *
 */
public final class Tagger
{
    @NotNull
    protected AnalysisEngine ae;

    public static void main( String[] args ) throws Exception
    {
        String test = "This is a punctuation-rich test about maybe a Dr. with Ph.D. weighing 180lbs that took cipro 100mb b.i.d.";

        Tokenizer tokenizer = new Tokenizer.Builder().build();
        SentenceSegmenter segmenter = new SentenceSegmenter.Builder().build();

        ServiceProviderDescription taggingServiceProvider = new ServiceProviderDescription("posTaggingServiceProvider")
                .serviceProvider(OpenNLPTaggingServiceProvider.class.getCanonicalName())
                .modelFile("edu/umn/biomedicus/tagging/en-pos-maxent.bin");
//        .serviceProvider(MaxEntTaggingServiceProvider.class.getCanonicalName());

        Tagger tagger = new Builder().addService(taggingServiceProvider).build();

        CAS aCAS = tagger.newCAS();
        CAS view = aCAS.createView("System");
        view.setDocumentText(test);

        tokenizer.process(aCAS);
        segmenter.process(aCAS);
        tagger.process(aCAS);
        for (AnnotationFS annot : view.getAnnotationIndex())
        {
            Type tokenType = TypeUtil.getType(aCAS, Token.class);
            if (annot.getType() == tokenType)
            {
                Feature pos = annot.getType().getFeatureByBaseName("pos");
                String tag = annot.getStringValue(pos);
                System.out.println(String.format("%s : %s : %s", annot.getType(), annot.getCoveredText(), tag));
            }
        }
    }

    private Tagger(Builder builder) throws ResourceInitializationException
    {
        ae = builder.getAE();
    }
    private AnalysisEngine getAE() { return ae; }
    public CAS newCAS() throws ResourceInitializationException { return ae.newCAS(); }
    public void process(CAS aCAS) throws AnalysisEngineProcessException { ae.process(aCAS); }

    public static class Builder
    {

        private String descriptor = "edu/umn/biomedicus/tagging/posTaggingAE.xml";
        private List<ServiceProviderDescription> serviceProviders  = new ArrayList<>();

        public Builder() {}
        public Builder(String descriptor)
        {
            this.descriptor = descriptor;
        }

        public Builder addService(ServiceProviderDescription serviceDesc)
        {
            serviceProviders.add(serviceDesc);
            return this;
        }

        public Tagger build()  throws ResourceInitializationException
        {
            return new Tagger(this);
        }

        public AnalysisEngine getAE() throws ResourceInitializationException
        {
            URL resource = this.getClass().getClassLoader().getResource(descriptor);
            return ResourceFactory.createAnalysisEngine(resource, serviceProviders);
        }
    }
}

