/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package edu.umn.biomedicus.tagging;

import edu.umn.biomedicus.core.featureranges.SentenceFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.tagging.services.TaggingServiceProvider;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * A simple maximum entropy part-of-speech annotator. (A "keep it simple" annotator).
 */
public class POSTaggingAE extends CasAnnotator_ImplBase {
    private static final String TAGGING_SERVICE_PROVIDER_KEY = "POS_TAGGING_SERVICE";

    @NotNull
    private Type tokenType;
    @NotNull
    private Feature pos;
    @NotNull
    private TaggingServiceProvider tagger;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {
        super.initialize(context);
        try
        {
            tagger = (TaggingServiceProvider) context.getResourceObject(TAGGING_SERVICE_PROVIDER_KEY);
        }
        catch (ResourceAccessException e)
        {
            throw new ResourceInitializationException(e);
        }

        if (tagger == null)
        {
            Exception e = new Exception("Unable to load pos tagging model");
            throw new ResourceInitializationException(e);
        }
    }

    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException
    {
        CAS sysView = aCAS.getView("System");

        String test = Token.class.getCanonicalName();
        String previousPOS = "";
        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysView, Sentence.class))
        {
            SentenceFeatureRange sent = new SentenceFeatureRange(sysView, sentenceAnnotation);
            List<AnnotationFS> tokenList = sent.getCoveredTokens();
            String[] tokensAsStrings = sent.getCoveredTokensAsStringArray();
            String[] tags = tagger.getTags(tokensAsStrings);
            if (tags.length == tokensAsStrings.length)
            {
                Feature pos = tokenType.getFeatureByBaseName("pos");
                for (int i = 0; i < tags.length; i++)
                {
                    AnnotationFS token = tokenList.get(i);
                    token.setStringValue(pos, tags[i]);
                }
            }
        }
    }

    @Override
    public void typeSystemInit(TypeSystem typeSystem)
    {
        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        pos = tokenType.getFeatureByBaseName("pos");
    }
}