/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package edu.umn.biomedicus.tagging.services;

/**
 * A simple maximum entropy part-of-speech annotator. (A "keep it simple" annotator).
 */
public class MaxEntTaggingServiceProvider
{
//        implements TaggingServiceProvider, SharedResourceObject {
//
//    private MaxentModel model;
//    @ConfigurationParameter(mandatory = false, defaultValue = "posMEModel.bin")
//    private String modelFileName;
//
//    // AcronymFeatureExtractor afe = new AcronymFeatureExtractor();
//
//    @Override
//    public void load(DataResource aData) throws ResourceInitializationException
//    {
//        URL resource = aData.getUrl();
//        File file = null;
//        try
//        {
//            file = PathUtils.resourceToTempFile(resource);
//        } catch (IOException e)
//        {
//            throw new ResourceInitializationException(e);
//        }
//
//        try
//        {
//            GenericModelReader reader = new GenericModelReader(file);
//            model = reader.getModel();
//            if (model == null)
//            {
//                throw new IOException("Failed to load MaxEnt POS tagging model.");
//            }
//        } catch (IOException e)
//        {
//            throw new ResourceInitializationException(e);
//        }
//    }
//
//    @Override
//    public String getTag(String[] featureSet)
//    {
//        double[] outcomes = model.eval(featureSet);
//        String tag = model.getBestOutcome(outcomes);
//        return tag;
//    }
//
//    public String[] getTags(String[] tokensAsStrings)
//    {
//
//    }
}