/* 
 Copyright 2010 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.tagging.evaluation;

//import org.cleartk.classifier.CleartkAnnotatorDescriptionFactory;
//import org.cleartk.classifier.DataWriterFactory;
//import org.cleartk.classifier.feature.extractor.outcome.DefaultOutcomeFeatureExtractor;
//import org.cleartk.classifier.libsvm.DefaultMultiClassLIBSVMDataWriterFactory;
//import org.cleartk.classifier.mallet.DefaultMalletCRFDataWriterFactory;
//import org.cleartk.classifier.mallet.DefaultMalletDataWriterFactory;
//import org.cleartk.classifier.opennlp.DefaultMaxentDataWriterFactory;
//import org.cleartk.classifier.svmlight.DefaultOVASVMlightDataWriterFactory;
//import org.cleartk.classifier.viterbi.ViterbiDataWriterFactory;
//import org.cleartk.token.pos.POSAnnotator;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class PosEngineFactory {

//    implements
//} EngineFactory {
//	public PosEngineFactory(String factoryClassName, boolean compressFeatures, int cutoff) {
//	}
//
//	@Override
//	public AnalysisEngineDescription createDataWritingAggregate(File modelDirectory)
//	                                throws ResourceInitializationException {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void train(File modelDirectory, String... trainingArguments) throws Exception {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public AnalysisEngineDescription createClassifierAggregate(File modelDirectory)
//	                                throws ResourceInitializationException {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
	// private String dataWriterFactoryClassName;
	// private Class<? extends DataWriterFactory<String>>
	// dataWriterFactoryClass;
	//
	// private boolean compressFeatures = true;
	//
	// private int featureCutoff = 5;
	//
	// @SuppressWarnings("unchecked")
	// public PosEngineFactory(String dataWriterFactoryClassName, boolean
	// compressFeatures, int featureCutoff)
	// throws ClassNotFoundException {
	// super();
	// this.dataWriterFactoryClassName = dataWriterFactoryClassName;
	// this.dataWriterFactoryClass = (Class<? extends
	// DataWriterFactory<String>>) Class
	// .forName(dataWriterFactoryClassName);
	// this.compressFeatures = compressFeatures;
	// this.featureCutoff = featureCutoff;
	// }
	//
	// /**
	// * If we are going to be getting POS tags out of the gold view, then we
	// have
	// * to assume that the gold view also has tokens. So, there is no need to
	// run
	// * the tokenizer over the gold view. Similarly, we will assume that
	// * sentences are provided as well.
	// */
	// public AnalysisEngineDescription createDataWritingAggregate(File
	// modelDirectory)
	// throws ResourceInitializationException {
	//
	// AggregateBuilder builder = new AggregateBuilder();
	//
	// // taggerDataWriter will be the PosTagger in data writing mode. Since
	// // ClearTK only supports two sequential data writer implementations,
	// // we will choose one or the other. So, if the value for
	// // dataWriterFactoryClassName corresponds to
	// // DefaultMalletCRFDataWriterFactory, then
	// // we will use that. Otherwise, we will assume ViterbiDataWriterFactory
	// // is the data writer factory and the provided value of
	// // dataWriterFactoryClassName is the delegated data writer factory name.
	// AnalysisEngineDescription taggerDataWriter;
	// if
	// (dataWriterFactoryClassName.equals(DefaultMalletCRFDataWriterFactory.class.getName()))
	// {
	// taggerDataWriter =
	// createPosTaggerMalletCRFDataWriter(modelDirectory.getPath(),
	// compressFeatures);
	// } else {
	// taggerDataWriter =
	// createPosTaggerViterbiDataWriter(modelDirectory.getPath(),
	// compressFeatures,
	// featureCutoff, dataWriterFactoryClass);
	// }
	//
	// builder.add(taggerDataWriter, CAS.NAME_DEFAULT_SOFA,
	// ViewNames.GOLD_VIEW);
	//
	// return builder.createAggregateDescription();
	// }
	//
	// /**
	// * Here I am assuming that we will always provide the tagger with
	// * gold-standard sentences and tokens because it simplifies evaluation
	// * because you can simply calculate accuracy rather than F-measure. So, we
	// * just need to copy sentences and tokens from the gold view to the system
	// * view and then run the part-of-speech tagger over the system view.
	// */
	// public AnalysisEngineDescription createClassifierAggregate(File
	// modelDirectory)
	// throws ResourceInitializationException {
	// AggregateBuilder builder = new AggregateBuilder();
	//
	// builder.add(ComponentFactory.createViewTextCopierDescription(ViewNames.GOLD_VIEW,
	// ViewNames.SYSTEM_VIEW));
	//
	// AnalysisEngineDescription tokenCopier =
	// AnalysisEngineFactory.createPrimitiveDescription(TokenCopier.class,
	// ComponentFactory.getTypeSystem());
	// builder.add(tokenCopier); // no need to map the views here since they
	// // are hard-wired into the copier
	// AnalysisEngineDescription sentenceCopier =
	// AnalysisEngineFactory.createPrimitiveDescription(
	// SentenceCopier.class, ComponentFactory.getTypeSystem());
	// builder.add(sentenceCopier); // no need to map the views here since they
	// // are hard-wired into the copier
	//
	// File modelFile = new File(modelDirectory, "model.jar");
	// AnalysisEngineDescription posTagger =
	// ComponentFactory.createPosAnnotator(modelFile.getPath());
	// builder.add(posTagger, CAS.NAME_DEFAULT_SOFA, ViewNames.SYSTEM_VIEW); //
	// we
	// // want
	// // to
	// // run
	// // the
	// // part-of-speech
	// // tagger
	// // on
	// // the
	// // system
	// // view
	// return builder.createAggregateDescription();
	// }
	//
	// /**
	// * After the training data has run, the training data file will be in the
	// * model directory along with enough meta data to know which trainer to
	// run.
	// * So, all that remains is to decide what training arguments to send to
	// the
	// * learner. These are always going to be the same arguments that you would
	// * pass in if you were invoking the learner directly.
	// */
	// public void train(File modelDirectory, String... trainingArguments)
	// throws Exception {
	// Evaluation.train(modelDirectory, trainingArguments);
	// }
	//
	// public static AnalysisEngineDescription
	// createPosTaggerViterbiDataWriter(String outputDirectoryName,
	// boolean compressFeatures, int featureCountCutoff,
	// Class<? extends DataWriterFactory<String>>
	// delegatedDataWriterFactoryClass)
	// throws ResourceInitializationException {
	//
	// AnalysisEngineDescription aed =
	// CleartkAnnotatorDescriptionFactory.createViterbiAnnotator(PosTagger.class,
	// ComponentFactory.getTypeSystem(), delegatedDataWriterFactoryClass,
	// outputDirectoryName);
	// ConfigurationParameterFactory.addConfigurationParameters(aed,
	// POSAnnotator.PARAM_FEATURE_EXTRACTOR_CLASS_NAME,
	// PosFeatureExtractor.class.getName(),
	// ViterbiDataWriterFactory.PARAM_OUTCOME_FEATURE_EXTRACTOR_NAMES,
	// new String[] { DefaultOutcomeFeatureExtractor.class.getName() },
	// DefaultMaxentDataWriterFactory.PARAM_COMPRESS, compressFeatures,
	// DefaultMalletDataWriterFactory.PARAM_COMPRESS, compressFeatures,
	// DefaultMalletCRFDataWriterFactory.PARAM_COMPRESS, compressFeatures,
	// DefaultMultiClassLIBSVMDataWriterFactory.PARAM_CUTOFF,
	// featureCountCutoff,
	// DefaultOVASVMlightDataWriterFactory.PARAM_CUTOFF, featureCountCutoff);
	// return aed;
	// }
	//
	// public static AnalysisEngineDescription
	// createPosTaggerMalletCRFDataWriter(String outputDirectoryName,
	// boolean compressFeatures) throws ResourceInitializationException {
	// AnalysisEngineDescription aed =
	// CleartkAnnotatorDescriptionFactory.createCleartkSequenceAnnotator(
	// PosTagger.class, ComponentFactory.getTypeSystem(),
	// DefaultMalletCRFDataWriterFactory.class, outputDirectoryName);
	// ConfigurationParameterFactory.addConfigurationParameters(aed,
	// POSAnnotator.PARAM_FEATURE_EXTRACTOR_CLASS_NAME,
	// PosFeatureExtractor.class.getName(),
	// DefaultMalletCRFDataWriterFactory.PARAM_COMPRESS, compressFeatures);
	// return aed;
	// }

}
