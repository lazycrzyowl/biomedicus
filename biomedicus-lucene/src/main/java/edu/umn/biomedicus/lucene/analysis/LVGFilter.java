package edu.umn.biomedicus.lucene.analysis;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.util.Arrays;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.KeywordAttribute;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

/** Transforms the token stream as per the Porter stemming algorithm.
 Note: the input to the stemming filter must already be in lower case,
 so you will need to use LowerCaseFilter or LowerCaseTokenizer farther
 down the Tokenizer chain in order for this to work properly!
 <P>
 To use this filter with other analyzers, you'll want to write an
 Analyzer class that sets up the TokenStream chain as you want it.
 To use this with LowerCaseTokenizer, for example, you'd write an
 analyzer like this:
 <P>
 <PRE class="prettyprint">
 class MyAnalyzer extends Analyzer {
 {@literal @Override}
 protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
 Tokenizer source = new LowerCaseTokenizer(version, reader);
 return new TokenStreamComponents(source, new PorterStemFilter(source));
 }
 }
 </PRE>
 <p>
 Note: This filter is aware of the {@link KeywordAttribute}. To prevent
 certain terms from being passed to the stemmer
 {@link KeywordAttribute#isKeyword()} should be set to <code>true</code>
 in a previous {@link TokenStream}.

 Note: For including the original term as well as the stemmed version, see
 {@link org.apache.lucene.analysis.miscellaneous.KeywordRepeatFilterFactory}
 </p>
 */
public final class LVGFilter extends TokenFilter {
    private final LVGNormalizer lvg = new LVGNormalizer();
    private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
    private final POSAttribute posAtt = addAttribute(POSAttribute.class);
    private final KeywordAttribute keywordAttr = addAttribute(KeywordAttribute.class);

    public LVGFilter(TokenStream in) {
        super(in);
    }

    @Override
    public final boolean incrementToken() throws IOException {
        if (!input.incrementToken())
            return false;

        String pos = posAtt.getPartOfSpeech();

        String candidate = String.copyValueOf(termAtt.buffer()).trim();
        String norm = lvg.normalize(candidate, pos);
        int bufferSize = termAtt.buffer().length;
        char[] normArray = Arrays.copyOf(norm.toCharArray(), bufferSize);
        if (norm != null)
            termAtt.copyBuffer(normArray, 0, normArray.length);

        return true;
    }
}
