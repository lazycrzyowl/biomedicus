package edu.umn.biomedicus.lucene.analysis;

import org.apache.lucene.util.Attribute;

public interface POSAttribute extends Attribute {

    public static enum PartOfSpeech {
        CC, CD, DT, EX, FW, IN, JJ, JJR, JJS, LS, MD, NN, NNP, NNPS, NNS, PDT, POS,
        PRP, PRP$, RB, RBR, RP, SYM, TO, UH, VB, VBD, VBG, VBN, VBP, VBZ, WDT, WP,
        WP$, WRB, UNK
    }

    public void setPartOfSpeech(String pos);

    public String getPartOfSpeech();
}