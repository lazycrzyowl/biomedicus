package edu.umn.biomedicus.lucene.analysis;

import java.io.IOException;
import java.util.Map;

import edu.umn.biomedicus.tagging.services.tnt.TagSearch;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
public class POSTaggingFilter extends TokenFilter {

    private POSAttribute posAttr;
    private TagSearch tagger = new TagSearch();
    private CharTermAttribute termAttr;
    private TokenStream suffixStream;
    private Map<String,String> suffixPosMap;

    public POSTaggingFilter(TokenStream input) throws IOException {
        super(input);

        // declare the POS attribute for both streams
        this.posAttr = (POSAttribute) addAttribute(POSAttribute.class);
        this.termAttr = (CharTermAttribute) addAttribute(CharTermAttribute.class);
        this.input.reset();
    }

    public final boolean incrementToken() throws IOException {
        String currentTerm = null;
        if (input.incrementToken()) {
            currentTerm = String.valueOf(termAttr.buffer()).trim();
        } else {
            return false;
        }
        termAttr.copyBuffer(currentTerm.toCharArray(), 0, currentTerm.length());
        // find the POS of the current word from Wordnet
        String pos = tagger.add(currentTerm);
        posAttr.setPartOfSpeech(pos);
        return true;
    }
}