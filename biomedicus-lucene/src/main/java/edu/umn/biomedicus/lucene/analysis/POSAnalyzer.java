package edu.umn.biomedicus.lucene.analysis;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


public class POSAnalyzer extends Analyzer {

    private Version matchVersion;
    private static String[] INPUT_TEXTS = {
            "Patient presents with chest pain but BP/HR and enzymes are normal.",
            "Family history of seizures on the maternal side and colon cancer on the paternal side.",
            "Hg1AC indicates insulin resistance.",
            "Patient complains of dizziness during exertion.",
            "Acronyms probably reduce accuracy, such as when the Dr. prescribes lasik b.i.d. to the patient."
    };

    @Override
    protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
        final Tokenizer source = new StandardTokenizer(Version.LUCENE_43, reader);
        TokenStream posFilter = null;

        try
        {
            posFilter = new POSTaggingFilter(source);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return new TokenStreamComponents(source, posFilter);
    }

    public static void main(String[] args) throws IOException {
        Analyzer analyzer = new POSAnalyzer();
        for (String inputText : INPUT_TEXTS) {
            System.out.println("Input: " + inputText);
            List<String> tags = new ArrayList<String>();
            TokenStream input = analyzer.tokenStream(
                    "field", new StringReader(inputText));
            input = new POSTaggingFilter(input);
            CharTermAttribute termAttribute =
                    (CharTermAttribute) input.addAttribute(CharTermAttribute.class);
            POSAttribute posAttribute =
                    (POSAttribute) input.addAttribute(POSAttribute.class);
            while (input.incrementToken()) {
                tags.add(String.copyValueOf(termAttribute.buffer()).trim() + "/" + posAttribute.getPartOfSpeech());
                char[] b = new char[termAttribute.length()];
                termAttribute.copyBuffer(b, 0, termAttribute.length());
            }
            input.end();
            input.close();
            StringBuilder tagBuf = new StringBuilder();
            tagBuf.append("Tagged: ").append(StringUtils.join(tags.iterator(), " "));
            System.out.println(tagBuf.toString());
        }
    }
}


