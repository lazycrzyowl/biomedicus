package edu.umn.biomedicus.lucene.analysis;

import edu.umn.biomedicus.lvg.LvgTokenNormalizer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.util.ArrayUtil;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * LVG Normalizer
 *
 * The normalizer class transforms a word into its root form.  The input
 * word can be provided a character at time (by calling add()), or at once
 * by calling one of the various stem(something) methods.
 */

class LVGNormalizer
{
    private LvgTokenNormalizer lvg;

    public LVGNormalizer() {
        // get environment variables for LVG and LexAccess home directories
        String lvg_home = System.getenv("LVG_HOME");
        String lex_home = System.getenv("LEX_ACCESS_HOME");

        // derive properties paths from home directories
        Path lvgFilePath = Paths.get(lvg_home, "/data/config/lvg.properties");
        Path lexFilePath = Paths.get(lex_home, "/data/config/lexAccess.properties");

        // use paths to instantiate the LVGNormalizer
        lvg = new LvgTokenNormalizer(
                lvgFilePath.toString(), lexFilePath.toString());
    }

    /**
     * reset() resets the lvg so it can normalize another word.  If you invoke
     * the normalizer by calling add(char) and then stem(), you must call reset()
     * before starting another word.
     */
    public void reset() {  }

    /**
     * Add a character to the word being stemmed.  When you are finished
     * adding characters, you can call stem(void) to process the word.
     */
    public void add(char ch) {    }

    /**
     * After a word has been stemmed, it can be retrieved by toString(),
     * or a reference to the internal buffer can be retrieved by getResultBuffer
     * and getResultLength (which is generally more efficient.)
     */
    @Override
    public String toString() {
        return "this";
    }

    /**
     * normalize a word provided as a String.  Returns the result as a String.
     */
    public String normalize(String s, String pos) {
        try {
            return lvg.norm(s, pos);
        } catch (Exception e)
        {
            e.printStackTrace();
            return s;
        }
    }


}

