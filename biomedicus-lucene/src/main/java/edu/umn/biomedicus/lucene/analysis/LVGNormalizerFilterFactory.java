package edu.umn.biomedicus.lucene.analysis;

import java.util.Map;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.util.TokenFilterFactory;

/**
 * Factory for {@link LVGFilter}
 */
public class LVGNormalizerFilterFactory extends TokenFilterFactory {

    /** Creates a new LVGNormalizerFilterFactory */
    public LVGNormalizerFilterFactory(Map<String,String> args) {
        super(args);
        if (!args.isEmpty()) {
            throw new IllegalArgumentException("Unknown parameters: " + args);
        }
    }

    @Override
    public PorterStemFilter create(TokenStream input) {
        return new PorterStemFilter(input);
    }
}
