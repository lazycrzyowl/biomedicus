package edu.umn.biomedicus.lucene.analysis;

import org.apache.lucene.util.AttributeImpl;

public class POSAttributeImpl extends AttributeImpl implements POSAttribute {

//    private static final long serialVersionUID = -8416041956010464591L;

    private String pos = "UNK";

    public String getPartOfSpeech() {
        return pos;
    }

    public void setPartOfSpeech(String pos) {
        this.pos = pos;
    }

    @Override
    public void clear() {
        this.pos = "UNK";
    }

    @Override
    public void copyTo(AttributeImpl target) {
        ((POSAttributeImpl) target).setPartOfSpeech(pos);
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof POSAttributeImpl) {
            return pos.equals(((POSAttributeImpl) other).getPartOfSpeech());
        }
        return false;
    }
}