
/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.webapp;


import edu.umn.biomedicus.applications.pipelines.FamilyHistoryCLIPipeline;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.FamilyHistory;
import org.apache.uima.UIMAException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.glassfish.jersey.server.mvc.Viewable;

import javax.inject.Singleton;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@WebService
@Path("familyhistory")
@Singleton
public class FamilyHistoryResource extends Application {
    TypeSystem ts;
    Type familyHistoryConstituentType;
    Type familyHistoryType;
    Type tokenType;
    Type chunkType;
    Feature labelFeature;
    @Context
    private ServletContext context;

    String collData = "{\n" +
            "    \"entity_types\": [\n" +
            "        {\n" +
            "            \"type\": \"FamilyHistoryConstituent\",\n" +
            "            \"labels\": [ \"Constituent\", \"Con\" ],\n" +
            "            \"bgColor\": \"#7fa2ff\",\n" +
            "            \"borderColor\": \"darken\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"relation_types\": [\n" +
            "        {\n" +
            "            \"type\": \"Predication\",\n" +
            "            \"labels\": [ \"Predication\", \"Pred\" ],\n" +
            "            \"dashArray\": \"3,3\",\n" +
            "            \"color\": \"purple\",\n" +
            "            \"args\": [\n" +
            "                {\n" +
            "                    \"role\": \"Family\",\n" +
            "                    \"targets\": [ \"FamilyHistoryConstituent\" ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"role\": \"Observation\",\n" +
            "                    \"targets\": [ \"FamilyHistoryConstituent\" ]\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    ],\n" +
            "    \"event_types\": [\n" +
            "        {\n" +
            "            \"type\": \"Indicator\",\n" +
            "            \"labels\": [ \"Possession\", \"Posses\" ],\n" +
            "            \"bgColor\": \"lightgreen\",\n" +
            "            \"borderColor\": \"darken\",\n" +
            "            \"arcs\": [\n" +
            "                {\n" +
            "                    \"type\": \"Family\",\n" +
            "                    \"labels\": [ \"Family\", \"Fam\" ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"type\": \"Observation\",\n" +
            "                    \"labels\": [ \"Observation\", \"Obs\" ],\n" +
            "                    \"color\": \"green\"\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    private FamilyHistoryCLIPipeline uimaPipeline;
    String message;

    public FamilyHistoryResource() throws ResourceInitializationException
    {
        System.setProperty("metamap.path", "/Users/bill0154/Applications/public_mm/bin/metamap13");
        try
        {
            uimaPipeline = new FamilyHistoryCLIPipeline();
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        } catch (InvalidXMLException e)
        {
            throw new ResourceInitializationException(e);
        }

    }

    @GET
    @Produces("text/plain")
    public Response solicit(@Context HttpServletRequest request)  throws MalformedURLException,
            ResourceInitializationException
    {
        request.setAttribute("collData", collData);
        CAS output = null;
        Viewable view = new Viewable("/test");
        return Response.ok(view).build();

    }


    @POST
    @Produces("text/html")
    //@Template(name="test.jsp")
    @Consumes("application/x-www-form-urlencoded")
    public Response analyze(@FormParam("input") String sample, @Context HttpServletRequest request)
    {
        request.setAttribute("sample", sample);
        request.setAttribute("collData", collData);
        CAS output = null;
        try
        {
            output = getCAS("FAMILY HISTORY:\n" + sample);
        }
        catch (ResourceInitializationException e)
        {
            request.setAttribute("error", e.getStackTrace().toString());
        }

        // housekeeping: init types, create model and get system view
        if (output != null) initTypes(output);
        Map<String, List<Constituent>> model = createModel();
        CAS systemView = CASUtil.getSystemView(output);

        getFamily(systemView, model);
        getObservations(systemView, model);
        getConstituents(systemView, model);
        getVitalStatus(systemView, model);

        List<Predication> predicationList = getPredications(systemView);
        request.setAttribute("predications", predicationList);

        Viewable view = new Viewable("/test", model);
        return Response.ok(view).build();

    }

    private void getFamily(CAS systemView, Map<String, List<Constituent>> model)
    {
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
        {
            Constituent con = new Constituent(annot);
            if (con.isFamily())
                model.get("family").add(con);
        }
    }

    private void getVitalStatus(CAS systemView, Map<String, List<Constituent>> model)
    {
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
        {
            Constituent con = new Constituent(annot);
            if (con.isVitalStatus())
                model.get("vitalStatus").add(con);
        }
    }

    private void getConstituents(CAS systemView, Map<String, List<Constituent>> model)
    {
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
        {
            Constituent con = new Constituent(annot);
            model.get("constituent").add(con);
        }
    }

    private void getObservations(CAS systemView, Map<String,List<Constituent>> model)
    {
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryConstituentType))
        {
            Constituent con = new Constituent(annot);
            if (con.isObservation())
                model.get("observation").add(con);
        }
    }

    private List<Predication> getPredications(CAS systemView)
    {
        List<Predication> predicationList = new ArrayList<>();
        for (AnnotationFS annot : systemView.getAnnotationIndex(familyHistoryType))
        {
            predicationList.add(new Predication(annot));
        }
        return predicationList;
    }

    private CAS getCAS(String sample) throws ResourceInitializationException
    {
        HashMap<String, Object> model = new HashMap<>();

        CAS output = null;
        if (uimaPipeline == null)
        {
            Exception e = new Exception("Unable to create UIMA pipeline.");
            throw new ResourceInitializationException(e);
        }

        try
        {
            output = uimaPipeline.process("command-line", sample);
        } catch (UIMAException e)
        {
            e.printStackTrace();
        }

        return output;
    }

    private void initTypes(CAS aCAS)
    {
        ts = aCAS.getTypeSystem();
        //familyHistoryType = ts.getType("edu.umn.biomedicus.type.FamilyHistory");
        familyHistoryType = TypeUtil.getType(aCAS, FamilyHistory.class);
        familyHistoryConstituentType = ts.getType("edu.umn.biomedicus.type.FamilyHistoryConstituent");
        tokenType = ts.getType("edu.umn.biomedicus.type.Token");
        chunkType = ts.getType("edu.umn.biomedicus.type.Chunk");
        labelFeature = familyHistoryConstituentType.getFeatureByBaseName("label");
    }

    public Map<String,List<Constituent>> createModel()
    {
        // create model
        Map<String, List<Constituent>> model = new HashMap<>();
        model.put("family", new ArrayList<Constituent>());
        model.put("observation", new ArrayList<Constituent>());
        model.put("vitalStatus", new ArrayList<Constituent>());
        model.put("negation", new ArrayList<Constituent>());
        model.put("constituent", new ArrayList<Constituent>());
        return model;
    }
}
