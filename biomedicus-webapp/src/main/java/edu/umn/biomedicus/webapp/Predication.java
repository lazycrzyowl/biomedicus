/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.webapp;

import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.FamilyHistory;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;

public class Predication {
    private String familyConstituent;
    private String familyMapping;
    private String observationConstituent;
    private String observationMapping;
    private String vitalStatus;
    private boolean negated;
    private String indicator;
    private String certainty;
    private String sideOfFamily;
    private String coveredText;
    private int begin;
    private int end;

    public Predication(AnnotationFS annot)
    {
        TypeSystem ts = annot.getCAS().getTypeSystem();
        TypeUtil.getType(annot.getCAS(), FamilyHistory.class);
        Type familyHistoryType = ts.getType("edu.umn.biomedicus.type.FamilyHistory");
        Feature familyConstituentFeature = familyHistoryType.getFeatureByBaseName("familyConstituent");
        Feature familyMappingFeature = familyHistoryType.getFeatureByBaseName("familyMapping");
        Feature observationConstituentFeature = familyHistoryType.getFeatureByBaseName("observationConstituent");
        Feature observationMappingFeature = familyHistoryType.getFeatureByBaseName("observationMapping");
        Feature vitalStatusFeature = familyHistoryType.getFeatureByBaseName("vitalStatus");
        Feature negatedFeature = familyHistoryType.getFeatureByBaseName("negated");
        Feature indicatorFeature = familyHistoryType.getFeatureByBaseName("indicator");
        Feature certaintyFeature = familyHistoryType.getFeatureByBaseName("certainty");
        Feature sideOfFamilyFeature = familyHistoryType.getFeatureByBaseName("sideOfFamily");
        Feature observationTypeFeature = familyHistoryType.getFeatureByBaseName("observationType");

        familyConstituent = annot.getStringValue(familyConstituentFeature);
        familyMapping = annot.getStringValue(familyMappingFeature);
        observationConstituent = annot.getStringValue(observationConstituentFeature);
        observationMapping = annot.getStringValue(observationMappingFeature);
        vitalStatus = annot.getStringValue(vitalStatusFeature);
        negated = annot.getBooleanValue(negatedFeature);
        indicator = annot.getStringValue(indicatorFeature);
        certainty = annot.getStringValue(certaintyFeature);
        sideOfFamily = annot.getStringValue(sideOfFamilyFeature);
        coveredText = annot.getCoveredText();
        begin = annot.getBegin();
        end = annot.getEnd();
    }

    public String getFamilyConstituent()
    {
        return familyConstituent;
    }

    public void setFamilyConstituent(String familyConstituent)
    {
        this.familyConstituent = familyConstituent;
    }

    public String getFamilyMapping()
    {
        return familyMapping;
    }

    public void setFamilyMapping(String familyMapping)
    {
        this.familyMapping = familyMapping;
    }

    public String getObservationConstituent()
    {
        return observationConstituent;
    }

    public void setObservationConstituent(String observationConstituent)
    {
        this.observationConstituent = observationConstituent;
    }

    public String getObservationMapping()
    {
        return observationMapping;
    }

    public void setObservationMapping(String observationMapping)
    {
        this.observationMapping = observationMapping;
    }

    public String getVitalStatus()
    {
        return vitalStatus;
    }

    public void setVitalStatus(String vitalStatus)
    {
        this.vitalStatus = vitalStatus;
    }

    public boolean getNegated()
    {
        return negated;
    }

    public void setNegated(boolean negated)
    {
        this.negated = negated;
    }

    public String getIndicator()
    {
        return indicator;
    }

    public void setIndicator(String indicator)
    {
        this.indicator = indicator;
    }

    public String getCertainty()
    {
        return certainty;
    }

    public void setCertainty(String certainty)
    {
        this.certainty = certainty;
    }

    public String getSideOfFamily()
    {
        return sideOfFamily;
    }

    public void setSideOfFamily(String sideOfFamily)
    {
        this.sideOfFamily = sideOfFamily;
    }
}
