<%--
  ~ Copyright (c) 2014 University of Minnesota
  ~
  ~  All rights reserved.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~  http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  --%>

<!doctype html>
<html>
<html lang="en" ng-app="biomedicusApp">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NLP-IE BioMedICUS - family history demonstration</title>

    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/css" href="http://localhost:8001/style-vis.css"/>
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css"/>

    <script src="bower_components/jquery/jquery.js"></script>
    <script src="bower_components/angular/angular.js"></script>
    <script src="bower_components/angular-animate/angular-animate.js"></script>
    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <script src="scripts/typesystem.js"></script>
    <script src="http://localhost:8001/client/lib/head.load.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/animations.js"></script>
    <script src="js/controllers.js"></script>
    <script src="js/filters.js"></script>
    <script src="js/services.js"></script>
    <script src="js/navbar.js"></script>
    <!--<script type="text/javascript" src="js/lib/json2.js"></script>-->

</head>
<body>

<div class="container" >

    <div class="page-header">
        <h1>BioMedICUS</h1>
        <h3>Family History Extraction Demonstration
            <br/><small>Automated extraction of family and observation predications from unstructured text.</small></h3>
    </div>

    <!-- working form -->
    <FORM name="familyhistorytext" METHOD="POST" ACTION="/fh/familyhistory" accept-charset="UTF-8">
        <table>
            <tr><td colspan=2>
                <br>Enter an example family history statement here:<br><br>
                <textarea valign=top name="input"
                          style="width: 450px; height: 9em" rows=31 cols=7></textarea>
            </td></tr>

            <tr><td align=left>
                <input type="submit" name="Analyze"/>
                <input type="button" value="Clear"
                       onclick="this.form.elements['analyze'].value=''"/>
            </td></tr>
        </table>
    </FORM>

    <script id="embedding-collData">
        var collData = {
            entity_types: [
                {
                    type: 'FamilyHistoryConstituent',
                    /* The labels are used when displaying the annotation, in this case
                     we also provide a short-hand "Per" for cases where
                     abbreviations are preferable */
                    labels: ['Constituent', 'Con'],
                    // Blue is a nice colour for a person?
                    bgColor: '#7fa2ff',
                    // Use a slightly darker version of the bgColor for the border
                    borderColor: 'darken'
                }
            ],

            relation_types: [
                {
                    type: 'Predication',
                    labels: ['Predication', 'Pred'],
                    // dashArray allows you to adjust the style of the relation arc
                    dashArray: '3,3',
                    color: 'purple',
                    /* A relation takes two arguments, both are named and can be constrained
                     as to which types they may apply to */
                    args: [
                        //
                        {role: 'Family', targets: ['FamilyHistoryConstituent'] },
                        {role: 'Observation', targets: ['FamilyHistoryConstituent'] }
                    ]
                }
            ],

            event_types: [
                {
                    type: 'Indicator',
                    labels: ['Possession', 'Posses'],
                    bgColor: 'lightgreen',
                    borderColor: 'darken',
                    /* Unlike relations, events originate from a span of text and can take
                     several arguments */
                    arcs: [
                        {type: 'Family', labels: ['Family', 'Fam'] },
                        // Just like the event itself, its arguments can be styled
                        {type: 'Observation', labels: ['Observation', 'Obs'], color: 'green' }
                    ]
                }
            ]}   ;

    </script>
    <br/>
    <h5>${collData}</h5>

    <script id="embedding-entity-doc">
        var docData = {
            // Our text of choice
            text     : "The father has heart disease and the mother has diabetes.",
            // The entities entry holds all entity annotations
            entities : [
                /* Format: [${ID}, ${TYPE}, [[${START}, ${END}]]]
                 note that range of the offsets are [${START},${END}) */
                ['T1', 'FamilyHistoryConstituent', [[4, 10]]],
                ['T2', 'FamilyHistoryConstituent', [[15, 28]]],
                ['T3', 'FamilyHistoryConstituent', [[37, 43]]],
                ['T4', 'FamilyHistoryConstituent', [[48, 56]]]
            ]
        };

        docData['attributes'] = [
            // Format: [${ID}, ${TYPE}, ${TARGET}]
            ['A1', 'family member', 'T1'],
            ['A2', 'family member', 'T2'],
            ['A3', 'observation', 'T3'],
            ['A4', 'observation', 'T4']
        ];

        docData['relations'] = [
            // Format: [${ID}, ${TYPE}, [[${ARGNAME}, ${TARGET}], [${ARGNAME}, ${TARGET}]]]
            ['R1', 'Predication', [['Family', 'T1'], ['Observation', 'T2']]],
            ['R2', 'Predication', [['Family', 'T3'], ['Observation', 'T4']]]
        ];

        docData['triggers'] = [
            // The format is identical to that of entities
            ['T5', 'Indicator', [[11, 14]]],
            ['T6', 'Indicator', [[44, 47]]]
        ];

        docData['events'] = [
            // Format: [${ID}, ${TRIGGER}, [[${ARGTYPE}, ${ARGID}], ...]]
            ['E1', 'T5', [['Experiencer', 'T1'], ['Observation', 'T2']]],
            ['E2', 'T6', [['Experiencer', 'T3'], ['Observation', 'T4']]]
        ];


    </script>

    <!--<div id="embedding-entity-example"></div>-->

    <div id="family-history-example"></div>

    <%--<div id="live-io">--%>
        <%--<p>--%>
            <%--<textarea id="coll-input" style="display:block;float:left;width:40%;height:400px;font-size:11px;border:2px inset"--%>
                      <%--placeholder="Enter JSON for the collection object here..."></textarea>--%>
            <%--<textarea id="doc-input" style="display:block;float:right;width:55%;height:400px;font-size:11px;border:2px inset"--%>
                      <%--placeholder="Enter JSON for the document object here..."></textarea>--%>
        <%--</p>--%>
    <%--</div>--%>

    <!-- override a few style-vis.css settings for the embedded version to make things larger -->
    <style type="text/css">
        text { font-size: 15px; }
        .span text { font-size: 10px; }
        .arcs text { font-size: 9px; }
    </style>

    <script src="js/typesystem.js"/>
    <script type="text/javascript">
        var bratLocation = 'http://localhost:8001/';
        head.js(
                // External libraries
                        bratLocation + '/client/lib/jquery.min.js',
                        bratLocation + '/client/lib/jquery.svg.min.js',
                        bratLocation + '/client/lib/jquery.svgdom.min.js',


                // brat helper modules
                        bratLocation + '/client/src/configuration.js',
                        bratLocation + '/client/src/util.js',
                        bratLocation + '/client/src/annotation_log.js',
                        bratLocation + '/client/lib/webfont.js',
                // brat modules
                        bratLocation + '/client/src/dispatcher.js',
                        bratLocation + '/client/src/url_monitor.js',
                        bratLocation + '/client/src/visualizer.js'
        );

        var webFontURLs = [
                    bratLocation + '/static/fonts/Astloch-Bold.ttf',
                    bratLocation + '/static/fonts/PT_Sans-Caption-Web-Regular.ttf',
                    bratLocation + '/static/fonts/Liberation_Sans-Regular.ttf'
        ];

        head.ready(function() {
            // Evaluate the code from the examples and show it to the user
        eval($('#embedding-entity-coll').text());
        eval($('#embedding-entity-doc').text());
            /* Make damn sure to copy the objects before handing them to brat
             since we will modify them later on */
        Util.embed('embedding-entity-example', $.extend({}, collData),
                $.extend({}, docData), webFontURLs);

        eval($('#embedding-attribute-coll').text());
        eval($('#embedding-attribute-doc').text());
        Util.embed('embedding-attribute-example', $.extend({}, collData),
                $.extend({}, docData), webFontURLs);

        eval($('#embedding-relation-coll').text());
        eval($('#embedding-relation-doc').text());
        Util.embed('embedding-relation-example', $.extend({}, collData),
                $.extend({}, docData), webFontURLs);

        eval($('#embedding-event-coll').text());
        eval($('#embedding-event-doc').text());
        Util.embed('embedding-event-example', $.extend({}, collData),
                $.extend({}, docData), webFontURLs);

            var collInput = $('#coll-input');
            var docInput = $('#doc-input');
            var liveDiv = $('#family-history-example');

            var liveDispatcher = Util.embed('family-history-example',
                    $.extend({'collection': null}, collData),
                    $.extend({}, docData), webFontURLs);

            var renderError = function() {
                // liveDiv.css({'border': '2px solid red'}); // setting this blows the layout
                collInput.css({'border': '2px solid red'});
                docInput.css({'border': '2px solid red'});
            };

            liveDispatcher.on('renderError: Fatal', renderError);

            var collInputHandler = function() {
                var collJSON;
                try {
                    collJSON = JSON.parse(collInput.val());
                    collInput.css({'border': '2px inset'});
                } catch (e) {
                    // Not properly formatted JSON...
                    collInput.css({'border': '2px solid red'});
                    return;
                }

                try {
                    liveDispatcher.post('collectionLoaded',
                            [$.extend({'collection': null}, collJSON)]);
                    //liveDiv.css({'border': '2px inset'});  // setting this blows the layout
                    docInput.css({'border': '2px inset'});
                } catch(e) {
                    console.error('collectionLoaded went down with:', e);
                    //liveDiv.css({'border': '2px solid red'});
                    collInput.css({'border': '2px solid red'});
                }
            };

            var docInputHandler = function() {
                var docJSON;
                try {
                    docJSON = JSON.parse(docInput.val());
                    docInput.css({'border': '2px inset'});
                } catch (e) {
                    docInput.css({'border': '2px solid red'});
                    return;
                }

                try {
                    liveDispatcher.post('requestRenderData', [$.extend({}, docJSON)]);
                    // liveDiv.css({'border': '2px inset'});  // setting this blows the layout
                    collInput.css({'border': '2px inset'});
                } catch(e) {
                    console.error('requestRenderData went down with:', e);
                    // liveDiv.css({'border': '2px solid red'});
                    collInput.css({'border': '2px solid red'});
                }
            };

            // Inject our current example as a start
            var collJSON = JSON.stringify(collData, undefined, '    ');
            docJSON = JSON.stringify(docData, undefined, '    ')
            // pack those just a bit
            var packJSON = function(s) {
                // replace any space with ' ' in non-nested curly brackets
                s = s.replace(/(\{[^\{\}\[\]]*\})/g,
                        function(a, b) { return b.replace(/\s+/g, ' '); });
                // replace any space with ' ' in [] up to nesting depth 1
                s = s.replace(/(\[(?:[^\[\]\{\}]|\[[^\[\]\{\}]*\])*\])/g,
                        function(a, b) { return b.replace(/\s+/g, ' '); });
                return s
            }
            collInput.text(packJSON(collJSON));
            docInput.text(packJSON(docJSON));

            var listenTo = 'propertychange keyup input paste';
            collInput.bind(listenTo, collInputHandler);
            docInput.bind(listenTo, docInputHandler);
        });
    </script>
</div>

</body>
</html>
