<%@page pageEncoding="UTF-8"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<!doctype html>
<html>
<html lang="en" ng-app="biomedicusApp">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NLP-IE BioMedICUS - family history demonstration</title>

    <link rel="stylesheet" href="../css/animations.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
    <link rel="stylesheet" type="text/css" href="http://localhost:8001/style-vis.css"/>
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css"/>

    <script src="../bower_components/jquery/jquery.js"></script>
    <script src="../bower_components/angular/angular.js"></script>
    <script src="../bower_components/angular-animate/angular-animate.js"></script>
    <script src="../bower_components/angular-route/angular-route.js"></script>
    <script src="../bower_components/angular-resource/angular-resource.js"></script>
    <script src="../js/typesystem.js"></script>
    <script src="http://localhost:8001/client/lib/head.load.min.js"></script>
    <script src="../js/app.js"></script>
    <script src="../js/animations.js"></script>
    <script src="../js/controllers.js"></script>
    <script src="../js/filters.js"></script>
    <script src="../js/services.js"></script>
    <script src="../js/navbar.js"></script>
    <!--<script type="text/javascript" src="js/lib/json2.js"></script>-->

</head>
<body>

<div class="container" >

    <div class="page-header">
        <h1>BioMedICUS</h1>

        <h3>Family History Extraction Demonstration<br/>
            <small>
                Automated extraction of family and observation
                predications from unstructured text.
            </small>
        </h3>

    </div>

    <!-- working form -->
    <FORM name="familyhistorytext" METHOD="POST" ACTION="familyhistory
        " accept-charset="UTF-8">
        <table>
            <tr><td colspan=2>
                <br/>Enter an example family history statement below.<br/>
		An example entry could include a section heading, such as:<br/>
		 <div class="example">
                  FAMILY HISTORY:<br/>
                  Heart disease on the father side of the family. Mother has arthritis.
                </div>
                <br/>

                <textarea valign=top name="input"
                          style="width: 450px; height: 9em" rows=31 cols=7></textarea>
            </td></tr>

            <tr><td align=left>
                <input type="submit" name="Analyze"/>
                <input type="button" value="Clear"
                       onclick="this.form.elements['analyze'].value=''"/>
            </td></tr>
        </table>
    </FORM>

<%-- Begin result display --%>
<h4>Results</h4>
<p>Supplied text: <i>"${sample}"</i></p>
<p>Extracted elements:
    <ul>
        <c:forEach var="con" items="${it.constituent}">
            <li>
                &nbsp;&nbsp;&nbsp;&nbsp;Constituent: <c:out value="${con.label}"/> {
                <c:out value="${con.coveredText}"/>:
                <c:out value="${con.identifier}"/>}
            </li>
        </c:forEach>
    </ul>
</p>

<%--<p>Observations: <br/>--%>
    <%--<ul>--%>
        <%--<c:forEach var="obs" items="${it.observation}">--%>
            <%--<li>--%>
                <%--<c:out value="${obs.coveredText}"/>,--%>
                <%--CUI = <c:out value="${obs.identifier}"/>,--%>
            <%--</li>--%>
        <%--</c:forEach>--%>
<%--</ul>--%>
<%--</p>--%>

<p>Predications: <br/>
<ul>
    <c:forEach var="predication" items="${predications}">
        <li>
            Family Member{ <c:out value="${predication.familyConstituent}"/> },
            Observation{ <c:out value="${predication.observationConstituent}"/> },
            Vital status{ <c:out value="${predication.vitalStatus}"/> },
            Negated{ <c:out value="${predication.negated}"/> }

        </li>
    </c:forEach>
</ul>
</p>

<script id="embedding-event-doc">

    var docData = {
        // Our text of choice
        text     : "The father has heart disease, and the mother has diabetes.",
        // The entities entry holds all entity annotations
        entities : [
            /* Format: [${ID}, ${TYPE}, [[${START}, ${END}]]]
             note that range of the offsets are [${START},${END}) */
            ['T1', 'FamilyHistoryConstituent', [[4, 10]]],
            ['T2', 'FamilyHistoryConstituent', [[15, 28]]],
            ['T3', 'FamilyHistoryConstituent', [[37, 43]]],
            ['T4', 'FamilyHistoryConstituent', [[48, 56]]]
        ]
    };

    docData['attributes'] = [
        // Format: [${ID}, ${TYPE}, ${TARGET}]
        ['A1', 'family member', 'T1'],
        ['A2', 'family member', 'T2'],
        ['A3', 'observation', 'T3'],
        ['A4', 'observation', 'T4']
    ];

    docData['relations'] = [
        // Format: [${ID}, ${TYPE}, [[${ARGNAME}, ${TARGET}], [${ARGNAME}, ${TARGET}]]]
        ['R1', 'Predication', [['Family', 'T1'], ['Observation', 'T2']]],
        ['R2', 'Predication', [['Family', 'T3'], ['Observation', 'T4']]]
    ];

    docData['triggers'] = [
        // The format is identical to that of entities
        ['T5', 'Indicator', [[11, 14]]],
        ['T6', 'Indicator', [[44, 47]]]
    ];

    docData['events'] = [
        // Format: [${ID}, ${TRIGGER}, [[${ARGTYPE}, ${ARGID}], ...]]
        ['E1', 'T5', [['Experiencer', 'T1'], ['Observation', 'T2']]],
        ['E2', 'T6', [['Experiencer', 'T3'], ['Observation', 'T4']]]
    ];

</script>

<%--<div id="embedding-event-example"></div>--%>

<div id="family-history-example"></div>

<!-- override a few style-vis.css settings for the embedded version to make things larger -->
<style type="text/css">
    text { font-size: 15px; }
    .span text { font-size: 10px; }
    .arcs text { font-size: 9px; }
</style>

<script type="text/javascript">
    var bratLocation = 'http://localhost:8001/';
    head.js(
            // External libraries
                    bratLocation + '/client/lib/jquery.min.js',
                    bratLocation + '/client/lib/jquery.svg.min.js',
                    bratLocation + '/client/lib/jquery.svgdom.min.js',


            // brat helper modules
                    bratLocation + '/client/src/configuration.js',
                    bratLocation + '/client/src/util.js',
                    bratLocation + '/client/src/annotation_log.js',
                    bratLocation + '/client/lib/webfont.js',
            // brat modules
                    bratLocation + '/client/src/dispatcher.js',
                    bratLocation + '/client/src/url_monitor.js',
                    bratLocation + '/client/src/visualizer.js'
    );

    var webFontURLs = [
                bratLocation + '/static/fonts/Astloch-Bold.ttf',
                bratLocation + '/static/fonts/PT_Sans-Caption-Web-Regular.ttf',
                bratLocation + '/static/fonts/Liberation_Sans-Regular.ttf'
    ];

    head.ready(function() {

        var collInput = $('#coll-input');
        var docInput = $('#doc-input');
        var liveDiv = $('#family-history-example');

        // Time for some "real" brat coding, let's hook into the dispatcher
        var liveDispatcher = Util.embed('family-history-example',
                $.extend({'collection': null}, collData),
                $.extend({}, docData), webFontURLs);

        var renderError = function() {
            // liveDiv.css({'border': '2px solid red'}); // setting this blows the layout
            collInput.css({'border': '2px solid red'});
            docInput.css({'border': '2px solid red'});
        };

        liveDispatcher.on('renderError: Fatal', renderError);

        var collInputHandler = function() {
            var collJSON;
            try {
                collJSON = JSON.parse(collInput.val());
                collInput.css({'border': '2px inset'});
            } catch (e) {
                // Not properly formatted JSON...
                collInput.css({'border': '2px solid red'});
                return;
            }

            try {
                liveDispatcher.post('collectionLoaded',
                        [$.extend({'collection': null}, collJSON)]);
                //liveDiv.css({'border': '2px inset'});  // setting this blows the layout
                docInput.css({'border': '2px inset'});
            } catch(e) {
                console.error('collectionLoaded went down with:', e);
                //liveDiv.css({'border': '2px solid red'});
                collInput.css({'border': '2px solid red'});
            }
        };

        var docInputHandler = function() {
            var docJSON;
            try {
                docJSON = JSON.parse(docInput.val());
                docInput.css({'border': '2px inset'});
            } catch (e) {
                docInput.css({'border': '2px solid red'});
                return;
            }

            try {
                liveDispatcher.post('requestRenderData', [$.extend({}, docJSON)]);
                // liveDiv.css({'border': '2px inset'});  // setting this blows the layout
                collInput.css({'border': '2px inset'});
            } catch(e) {
                console.error('requestRenderData went down with:', e);
                // liveDiv.css({'border': '2px solid red'});
                collInput.css({'border': '2px solid red'});
            }
        };

        // Inject our current example as a start
        var collJSON = JSON.stringify(collData, undefined, '    ');
        docJSON = JSON.stringify(docData, undefined, '    ')
        // pack those just a bit
        var packJSON = function(s) {
            // replace any space with ' ' in non-nested curly brackets
            s = s.replace(/(\{[^\{\}\[\]]*\})/g,
                    function(a, b) { return b.replace(/\s+/g, ' '); });
            // replace any space with ' ' in [] up to nesting depth 1
            s = s.replace(/(\[(?:[^\[\]\{\}]|\[[^\[\]\{\}]*\])*\])/g,
                    function(a, b) { return b.replace(/\s+/g, ' '); });
            return s
        }
        collInput.text(packJSON(collJSON));
        docInput.text(packJSON(docJSON));

        var listenTo = 'propertychange keyup input paste';
        collInput.bind(listenTo, collInputHandler);
        docInput.bind(listenTo, docInputHandler);
    });
</script>

</div>
</div>
<%--<div id="footer">--%>
    <%--<p class="footer-text">&copy; 2010-2014 the <a href="about.html" style="color:gray">brat contributors</a></p>--%>
    <%--<p class="footer-logo"><a href="http://creativecommons.org/licenses/by/3.0/"><img title="Content on this website is licensed under a Creative Commons Attribution 3.0 License. (Linked corpus data licensed separately.)" src="http://i.creativecommons.org/l/by/3.0/80x15.png"/></a></p>--%>
<%--</div>--%>

</body>
</html>
