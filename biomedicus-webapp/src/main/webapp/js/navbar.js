'use strict';
angular.module('biomedicusApp').
    directive('navbar', function () {
        return {
            restrict: 'E',
            scope: {
            },
            templateUrl: 'views/partials/navbar.html'
        };
    });