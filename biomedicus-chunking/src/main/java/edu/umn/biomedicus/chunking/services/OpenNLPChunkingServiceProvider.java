package edu.umn.biomedicus.chunking.services;


import edu.umn.biomedicus.core.utils.PathUtils;
import edu.umn.biomedicus.core.utils.Span;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import org.apache.uima.cas.CAS;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class OpenNLPChunkingServiceProvider
        implements ChunkingServiceProvider, SharedResourceObject {

    private List<Span> results = new ArrayList<>();


    @NotNull
    private ChunkerME chunker;

    @Override
    public Span[] analyze(String sentenceText, String[] sentenceAsTokens,
                                           Span[] tokensAsSpans, String[] tags)
    {
        results = new ArrayList<>();
        return runAnalysis(sentenceText, sentenceAsTokens, tokensAsSpans, tags, null);
    }

    private Span[] runAnalysis(String sentenceText, String[] sentenceAsTokens,
                               Span[] tokensAsSpans, String[] tags, CAS view)
    {
        opennlp.tools.util.Span[] localResults = chunker.chunkAsSpans(sentenceAsTokens, tags);

        for (opennlp.tools.util.Span chunk : localResults)
        {
            int startToken = chunk.getStart(); // note this is the token index, not char
            String startTokenText = sentenceAsTokens[startToken];
            Span startTokenSpan = tokensAsSpans[startToken];
            int begin = startTokenSpan.getStart(); // note this is the char index, not token

            int endToken = chunk.getEnd() - 1; // end are non-inclusive, so -1
            Span endTokenSpan = tokensAsSpans[endToken];
            int end = endTokenSpan.getEnd();

            results.add(new Span(begin, end).setLabel(chunk.getType()));
        }
        Span[] resultSpans = results.toArray(new Span[localResults.length]);
        return resultSpans;
    }

    public Span[] getSpans()
    {
        Span[] resultSpans = results.toArray(new Span[results.size()]);
        return resultSpans;
    }



    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {
        URL resource = aData.getUrl();
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        }
        catch (IOException e)
        {throw new ResourceInitializationException(e);}

//        posModel = new POSModelLoader().load(file);
//        tagger = new POSTaggerME(posModel);

        ChunkerModel model = null;

        try
        {
            model = new ChunkerModel(file);
        } catch (IOException e)
        {
            throw new ResourceInitializationException();
        }

        chunker = new ChunkerME(model);
    }

}
