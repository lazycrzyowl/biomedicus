package edu.umn.biomedicus.chunking;

import edu.umn.biomedicus.chunking.services.ChunkingServiceProvider;
import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.featureranges.SentenceFeatureRange;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.core.utils.TypeUtil;
import edu.umn.biomedicus.type.Sentence;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

import edu.umn.biomedicus.type.Token;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.*;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

/**
 * A shallow parser that uses the openNLP maximum entropy chunker to identify annotations
 */
public class ChunkerAE extends CasAnnotator_ImplBase
{

    private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
    private static final String CHUNKING_SERVICE_PROVIDER_KEY = "CHUNKING_SERVICE";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(ChunkerAE.class);
    @NotNull
    POSTaggerME tagger;
    @NotNull
    private Type chunkType;
    @NotNull
    private Type tokenType;
    @NotNull
    private Feature tokenStopwordFeature;
    @NotNull
    private Type sentenceType;
    @NotNull
    private Feature clinicalClassFeature;
    @NotNull
    private String[] sofaNames;
    @NotNull
    private POSModel posModel;
    @NotNull
    private ChunkingServiceProvider chunker;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException {

        super.initialize(context);
        try {
            chunker = (ChunkingServiceProvider) context.getResourceObject(CHUNKING_SERVICE_PROVIDER_KEY);
        } catch (ResourceAccessException e) {
            throw new ResourceInitializationException(e);
        }

    }

    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        CAS sysCAS = CASUtil.getSystemView(aCAS);

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, sentenceType)) {
            SentenceFeatureRange sentence = new SentenceFeatureRange(sysCAS, sentenceAnnotation);

            String sentenceText = sentenceAnnotation.getCoveredText();
            int sentenceStart = sentenceAnnotation.getBegin();
            edu.umn.biomedicus.core.utils.Span[] chunks = null;

            try {
                chunks = chunker.analyze(
                        sentenceText,
                        sentence.getCoveredTokensAsStringArray(),
                        sentence.getCoveredTokensAsSpans(),
                        sentence.getPOSTagsOfCoveredTokens());
            } catch (Exception e) {
                throw new AnalysisEngineProcessException(e);
            }

            for (edu.umn.biomedicus.core.utils.Span chunk : chunks) {
                int begin = chunk.getStart();
                int end = chunk.getEnd();
                String coveredText = sysCAS.getDocumentText().substring(begin, end);
                AnnotationFS chunkAnnotation = sysCAS.createAnnotation(chunkType, begin, end);
                sysCAS.addFsToIndexes(chunkAnnotation);
            }
        }
    }

    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {

        // Initialize types
        super.typeSystemInit(typeSystem);

        tokenType = typeSystem.getType(Token.class.getCanonicalName());
        tokenStopwordFeature = tokenType.getFeatureByBaseName("isStopword");
        chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
        sentenceType = typeSystem.getType(Sentence.class.getCanonicalName());
        clinicalClassFeature = sentenceType.getFeatureByBaseName("clinicalClass");
        logger.log(Level.FINEST, "Chunker type system initialized.");

    }

}

