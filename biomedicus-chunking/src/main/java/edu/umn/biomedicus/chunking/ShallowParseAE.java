package edu.umn.biomedicus.chunking;

import edu.umn.biomedicus.core.Views;
import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.PathUtils;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.util.Span;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * A shallow parser that uses the openNLP maximum entropy chunker to identify annotations
 */
public class ShallowParseAE extends CasAnnotator_ImplBase {

    private static final String TOKEN_ANNOTATION_NAME = "edu.umn.biomedicus.type.Token";
    private static final String CHUNK_ANNOTATION_NAME = "edu.umn.biomedicus.type.Chunk";
    private static final String SENTENCE_ANNOTATION_NAME = "edu.umn.biomedicus.type.Sentence";
    private static final String POS_FEATURE_NAME = "pos";
    private static final String LABEL_FEATURE_NAME = "label";
    private static final String SOURCE_FEATURE_NAME = "source";

    private static final String MODEL_FILE = "/edu/umn/biomedicus/chunking/en-chunker.bin";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(ShallowParseAE.class);
    @NotNull
    POSTaggerME tagger;
    @NotNull
    private Type chunkType;
    @NotNull
    private Type tokenType;
    @NotNull
    private Feature tokenStopwordFeature;
    @NotNull
    private Type sentenceType;
    @NotNull
    private String[] sofaNames;
    @NotNull
    private POSModel posModel;
    @NotNull
    private ChunkerME chunker;

    @Override
    public void initialize(UimaContext context) throws ResourceInitializationException
    {

        super.initialize(context);

        String modelResourcePath = "edu/umn/biomedicus/chunking/en-pos-maxent.bin";
        URL resource = getClass().getClassLoader().getResource(modelResourcePath);
        File file = null;
        try
        {
            file = PathUtils.resourceToTempFile(resource);
        }
        catch (IOException e)
        {throw new ResourceInitializationException(e);}

        posModel = new POSModelLoader().load(file);
        tagger = new POSTaggerME(posModel);

        InputStream is = getClass().getClassLoader().getResourceAsStream("edu/umn/biomedicus/chunking/en-chunker.bin");
        ChunkerModel model = null;

        try
        {
            model = new ChunkerModel(is);
        } catch (IOException e)
        {
            throw new ResourceInitializationException();
        }

        chunker = new ChunkerME(model);

        logger.setLevel(Level.OFF);
        logger.log(Level.INFO, "ShallowParser initialized.");
    }

    public void process(CAS aCAS)
    {
        CAS sysCAS = aCAS.getView(Views.SYSTEM_VIEW);
        String line;

        for (AnnotationFS sentenceAnnotation : AnnotationUtils.getAnnotations(sysCAS, sentenceType))
        {
            String sentence = sentenceAnnotation.getCoveredText();
            List<AnnotationFS> tokenizedSentence = AnnotationUtils.getCoveredAnnotations(sysCAS, sentenceAnnotation, tokenType);
            String[] tokensAsStrings = AnnotationUtils.annotationListToStringArray(tokenizedSentence, null);
            String[] tags = tokenize(tokenizedSentence, tokensAsStrings);

            //POSSample sample = new POSSample(tokensAsStrings, tags);

            Span[] span = chunker.chunkAsSpans(tokensAsStrings, tags);
            Feature labelFeature = chunkType.getFeatureByBaseName(LABEL_FEATURE_NAME);
            Feature sourceFeature = chunkType.getFeatureByBaseName(SOURCE_FEATURE_NAME);

            for (Span s : span)
            {
                // Because the chunker index numbers refer to tokens, but the CAS index numbers refer the characters,
                // it is necessary to retrieve the corresponding token to get the character index.
                AnnotationFS startToken = tokenizedSentence.get(s.getStart());
                AnnotationFS endToken = tokenizedSentence.get(s.getEnd() - 1);  // subtract 1 because end is non-inclusive
                int start = startToken.getBegin();
                int end = endToken.getEnd();

                AnnotationFS chunk = sysCAS.createAnnotation(chunkType, start, end);

                chunk.setStringValue(labelFeature, s.getType());
                chunk.setStringValue(sourceFeature, "OpenNLP MaxEnt");
                sysCAS.addFsToIndexes(chunk);
            }
        }
    }

    public String[] tokenize(List<AnnotationFS> tokenizedSentence, String[] tokensAsStrings)
    {

        Feature pos = tokenType.getFeatureByBaseName(POS_FEATURE_NAME);
        String[] tags = tagger.tag(tokensAsStrings);

        for (int i = 0; i < tokensAsStrings.length; i++)
        {
            AnnotationFS token = tokenizedSentence.get(i);
            token.setStringValue(pos, tags[i]);
        }

        return tags;
    }

    /**
     * Initialize the section types. No java cover classes are used, so typeSystemInit is required.
     *
     * @param typeSystem
     * @throws org.apache.uima.analysis_engine.AnalysisEngineProcessException
     */
    @Override
    public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException
    {

        // Initialize types
        super.typeSystemInit(typeSystem);

        tokenType = typeSystem.getType(TOKEN_ANNOTATION_NAME);
        tokenStopwordFeature = tokenType.getFeatureByBaseName("isStopword");
        chunkType = typeSystem.getType(CHUNK_ANNOTATION_NAME);
        sentenceType = typeSystem.getType(SENTENCE_ANNOTATION_NAME);
        logger.log(Level.INFO, "Sentence Annotator type system initialized.");

    }

}

