/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.chunking;


import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.resource.metadata.ResourceManagerConfiguration;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.NameClassPair;
import org.apache.uima.util.XMLInputSource;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class ChunkerBuilder {
    private static String descriptorFileReference;
    private static String serviceProviderReference;
    private static String modelFileReference;

    public static void setDescriptorFile(String descriptorFileReference)
    {
        URL resource = ChunkerBuilder.class.getClassLoader().getResource(descriptorFileReference);
        ChunkerBuilder.descriptorFileReference = resource.getFile();
    }

    public static void setServiceProvider(String serviceProviderReference)
    {
        ChunkerBuilder.serviceProviderReference = serviceProviderReference;
    }

    public static void setModelFilePath(String modelFileReference)
    {
        ChunkerBuilder.modelFileReference = modelFileReference;
    }

    public static void main(String[] args) throws Exception
    {
        ChunkerBuilder.setDescriptorFile("edu/umn/biomedicus/chunking/shallowParseAE.xml");
        AnalysisEngine ae = ChunkerBuilder.getChunker();
    }

    public static AnalysisEngine getChunker() throws ResourceInitializationException
    {
        XMLInputSource in = null;
        ResourceSpecifier specifier = null;
        AnalysisEngine mAnalysisEngine = null;

        try
        {
            in = new XMLInputSource(descriptorFileReference);
            specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(in);
        }
        catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
        catch (InvalidXMLException e)
        {
            throw new ResourceInitializationException(e);
        }

        ResourceManagerConfiguration resManConfig =
                (ResourceManagerConfiguration) specifier.getAttributeValue("resourceManagerConfiguration");


        for (ExternalResourceDescription erd : resManConfig.getExternalResources())
        {
            List<NameClassPair> atts = erd.listAttributes();
            ResourceSpecifier rs = erd.getResourceSpecifier();

        }
        //create Analysis Engine here
        mAnalysisEngine = UIMAFramework.produceAnalysisEngine(specifier);
        return mAnalysisEngine;
    }
}
