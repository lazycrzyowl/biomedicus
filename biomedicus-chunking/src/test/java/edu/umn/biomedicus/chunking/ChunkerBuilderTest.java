/*
 * Copyright (c) 2014 University of Minnesota
 *
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package edu.umn.biomedicus.chunking;

import edu.umn.biomedicus.core.utils.CASUtil;
import org.apache.uima.cas.CAS;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple Chunker.
 */
public class ChunkerBuilderTest {

    CAS aCAS;
    CAS system;

    @Before
    public void setup() throws Exception
    {
        Chunker Chunker = new Chunker.Builder().build();
        aCAS = Chunker.newCAS();
        system = CASUtil.getSystemView(aCAS);
    }

//    @Test
//    public void metamapChunkerBuilder() throws Exception
//    {
//        Chunker chunker = new Chunker.Builder()
//                .service(MetaMapChunkingServiceProvider.class)
//                .build();
//
//        assertNotNull(chunker);
//        aCAS.reset();
//        system = CASUtil.getSystemView(aCAS);
//        system.setDocumentText("This is an interesting test.");
//        chunker.process(aCAS);
//        AnnotationIndex chunkList = AnnotationUtils.getAnnotations(system, Chunk.class);
//        for (AnnotationFS chunk : AnnotationUtils.getAnnotations(system, Chunk.class))
//        {
//            String chunkText = chunk.getCoveredText();
//        }
//    }

    @Test
    public void testOpenNLPChunkerBuilder() throws Exception{

//        String serviceProviderClassName = OpenNLPChunkingServiceProvider.class.getCanonicalName();
//
//        Chunker Chunker = new Chunker.Builder()
//                .service(OpenNLPChunkingServiceProvider.class)
//                .model("edu/umn/biomedicus/Chunking/en-chunker.bin")
//                .build();
//
//        assertNotNull(Chunker);
//        aCAS.reset();
//        system = CASUtil.getSystemView(aCAS);
//        system.setDocumentText("This is a new test.");
//        Chunker.process(aCAS);
//        AnnotationIndex chunkList = AnnotationUtils.getAnnotations(system, Chunk.class);

    }
}
