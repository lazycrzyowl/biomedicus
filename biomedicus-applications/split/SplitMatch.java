/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.token.split;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A SplitMatch is the return value of {@link SplitMatcher#match(String)}. It
 * provides the character offsets where splits occur and the pattern id of the
 * matched pattern.
 * <p>
 * This data structure is also used by SplitMatcher to keep track of candidate
 * matches as it iterates over the characters in the input word.
 * 
 * @author Philip Ogren
 * 
 */

public class SplitMatch {

	private Node node;

	// the key is a pattern id. the value is a list of character offsets into
	// the string where splits should occur.
	protected Map<Integer, List<Integer>> splitIndexes;

	/**
	 * Create a SplitMatch for a given node.
	 * 
	 * @param node
	 */
	protected SplitMatch(Node node) {
		this.splitIndexes = new HashMap<Integer, List<Integer>>();
		this.node = node;
	}

	/**
	 * Create a SplitMatch for a given node and split indexes (usually from a
	 * previous SplitMatch).
	 * 
	 * @param node
	 */
	protected SplitMatch(Node node, Map<Integer, List<Integer>> splitIndexes) {
		this(node);

		if (splitIndexes != null) {
			this.splitIndexes.putAll(splitIndexes);
		}
	}

	/**
	 * Create a SplitMatch for a given node and split indexes (usually from a
	 * previous SplitMatch). This constructor also takes a parent node and the
	 * current character offset. If splits exists between the parent node and
	 * the given node, then they are added for the provided character
	 * index/offset.
	 * 
	 * @param node
	 *            the node for this SplitMatch
	 * @param splitIndexes
	 *            the split indexes to initialize this SplitMatch with
	 * @param parentNode
	 *            the parent node of the node parameter
	 * @param index
	 *            the current character offset of the word being matched.
	 */

	protected SplitMatch(Node node, Map<Integer, List<Integer>> splitIndexes, Node parentNode, int index) {
		this(node, splitIndexes);
		if (parentNode.getSplitIds(node) != null) {
			List<Integer> patternIds = parentNode.getSplitIds(node);
			for (int patternId : patternIds) {
				List<Integer> indexes = this.splitIndexes.get(patternId);
				if (indexes == null) {
					indexes = new ArrayList<Integer>();
					this.splitIndexes.put(patternId, indexes);
				}
				indexes.add(index);
			}
		}
	}

	protected boolean isComplete() {
		if (node.getPatternId() == -1) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * This method returns the token offsets where splitting should occur. It is
	 * possible that a split value could be the value 0. This can happen in
	 * situations where the split is preceded only by optional characters and
	 * none of those characters appear in the input text. For example, consider
	 * the pattern "a?|b". This would match the text "b" and the split would be
	 * at index 0. It is also possible for this method to return an empty array.
	 * This can happen if all of the characters that appear after a split are
	 * optional. For example, consider the pattern "a|b?". This will match the
	 * text "a" but no split is ever added before the match is completed.
	 * 
	 * @return the index offsets where token splitting should occur.
	 */
	public int[] getSplits() {
		int[] returnValues = getSplits(getPatternId());
		if (returnValues == null) {
			return new int[0];
		} else {
			return returnValues;
		}
	}

	protected int[] getSplits(int patternId) {
		List<Integer> indexes = splitIndexes.get(patternId);
		if (indexes == null) {
			return null;
		}

		int[] returnValues = new int[indexes.size()];
		for (int i = 0; i < indexes.size(); i++) {
			returnValues[i] = indexes.get(i);
		}
		return returnValues;
	}

	protected Map<Integer, List<Integer>> getSplitIndexes() {
		return Collections.unmodifiableMap(splitIndexes);
	}

	public int getPatternId() {
		return node.getPatternId();
	}

	protected Node getNode() {
		return node;
	}

}
