/* 
 Copyright 2011-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.token;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.pear.util.FileUtil;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.type.Token;

/**
 * This class assigns a boolean value to tokens that are in a specified stopword
 * file.
 * 
 * @author Robert Bill
 */
public class StopwordAnnotator extends JCasAnnotator_ImplBase {
	public static final String PARAM_STOPWORD_LIST = "stopwordsFilename";
	@ConfigurationParameter(name = PARAM_STOPWORD_LIST, mandatory = false, defaultValue = "PubMedStopwords.txt")
	private String stopwordsFilename;
	HashMap<String, Integer> stopwords = new HashMap<String, Integer>();
	String symbols = "\"`~!@#$%^&*()_-+=}]{[:;?/>.<,|'\\";

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		String resourceDir = new File(System.getenv("BIOMEDICUS_HOME"), "resources/dictionaries").toString();
		File stopwordsFile = new File(resourceDir, stopwordsFilename);
		String[] wordList = null;
		try {
			wordList = FileUtil.loadListOfStrings(stopwordsFile);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new ResourceInitializationException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new ResourceInitializationException();
		}

		if (wordList == null) {
			System.out.println("No stop words found in designated dictionary file: " + stopwordsFile);
			throw new ResourceInitializationException();
		}
		for (String word : wordList) {
			if (word.equals("") || word == null)
				continue;
			stopwords.put(word, 1);
		}
	}

	@Override
	public void process(JCas jCas) throws AnalysisEngineProcessException {
		for (Token token : JCasUtil.select(jCas, Token.class)) {
			String tokenText = token.getCoveredText().toLowerCase();
			if (stopwords.containsKey(tokenText)) {
				token.setIsStopword(true);
			} else if (tokenText.length() == 1 && symbols.contains(tokenText)) {
				token.setIsStopword(true);
			}

		}
	}
}
