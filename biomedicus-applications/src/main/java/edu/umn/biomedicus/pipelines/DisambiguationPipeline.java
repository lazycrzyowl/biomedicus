package edu.umn.biomedicus.pipelines;

import org.apache.uima.UIMAException;

import java.io.IOException;

//import edu.umn.biomedicus.BiomedicusDBResource_Impl;
//import edu.umn.biomedicus.ComponentFactory;
//import edu.umn.biomedicus.acronym.AcronymConceptMapper;
//import edu.umn.biomedicus.acronym.AcronymLongFormDisambiguationAE;
//import edu.umn.biomedicus.collectionreaders.SimpleCollectionReader;
//import edu.umn.biomedicus.indexing.LuceneDocumentAE;
//import edu.umn.biomedicus.lvg.NormAnnotatorFactory;
//import edu.umn.biomedicus.parser.BerkeleyParserAE;
//import edu.umn.biomedicus.util.Options_ImplBase;
//import edu.umn.biomedicus.wsd.SenseRelateAE;

public class DisambiguationPipeline {

//	public static class Options extends Options_ImplBase {
//		@Option(name = "-i", aliases = "--inputDirectory", usage = "The directory containing the raw text input files that should be ran through the pipeline.", required = true)
//		public String inputDirectory;
//
//		@Option(name = "-o", aliases = "--outputDirectory", usage = "The directory where the xmi (output) files should be written to.", required = true)
//		public String outputDirectory;
//	}

	public static void main(String[] args) throws UIMAException, IOException {
//		Options options = new Options();
//		options.parseArgument(args);
//
//		// get type system
//		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();
//
//		// get reader for source documents
//		CollectionReaderDescription dsc = CollectionReaderFactory.createDescription(SimpleCollectionReader.class,
//		                                typeSystem, SimpleCollectionReader.PARAM_INPUT_DIRECTORY,
//		                                options.inputDirectory);
//		CollectionReader reader = CollectionReaderFactory.createCollectionReader(dsc);
//
//		// get BiomedicusDB resource
//		// Create necessary DB resource
//		String biomedicusHome = System.getenv("BIOMEDICUS_HOME");
//		ExternalResourceDescription biomedicusDb = ExternalResourceFactory.createExternalResourceDescription(
//		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
//		                                biomedicusHome);
//
//		// Create analysis engines
//		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
//		AnalysisEngineDescription stopwords = ComponentFactory.createStopwordAnnotator();
//		AnalysisEngineDescription norms = NormAnnotatorFactory.createNormAnnotator();
//		AnalysisEngineDescription acronyms = ComponentFactory.createAcronymAnnotator();
//		AnalysisEngineDescription acronymCuis = AnalysisEngineFactory.createPrimitiveDescription(
//		                                AcronymConceptMapper.class, typeSystem);
//		AnalysisEngineDescription sents = ComponentFactory.createSentenceAnnotator();
//		AnalysisEngineDescription pos = ComponentFactory.createPosAnnotator();
//		AnalysisEngineDescription mm = ComponentFactory.createMetaMapAnnotator();
//		AnalysisEngineDescription senses = AnalysisEngineFactory.createPrimitiveDescription(SenseRelateAE.class,
//		                                typeSystem, SenseRelateAE.BIOMEDICUS_DB, biomedicusDb);
//		AnalysisEngineDescription acronymSenses = AnalysisEngineFactory.createPrimitiveDescription(
//		                                AcronymLongFormDisambiguationAE.class, typeSystem,
//		                                AcronymLongFormDisambiguationAE.BIOMEDICUS_DB, biomedicusDb);
//		AnalysisEngineDescription parser = AnalysisEngineFactory.createPrimitiveDescription(BerkeleyParserAE.class,
//		                                typeSystem);
//		AnalysisEngineDescription lucene = AnalysisEngineFactory.createPrimitiveDescription(LuceneDocumentAE.class,
//		                                typeSystem, LuceneDocumentAE.PARAM_INDEX_DIR, options.outputDirectory);
//
//		// output writer for the xmi files.
//		AnalysisEngineDescription xWriter = AnalysisEngineFactory.createPrimitiveDescription(XWriter.class, typeSystem,
//		                                XWriter.PARAM_OUTPUT_DIRECTORY_NAME, options.outputDirectory);
//
//		AggregateBuilder builder = new AggregateBuilder();
//		builder.add(tokens);
//		builder.add(stopwords);
//		builder.add(norms);
//		builder.add(acronyms);
//		builder.add(acronymCuis);
//		builder.add(sents);
//		builder.add(pos);
//		builder.add(mm);
//		builder.add(senses);
//		builder.add(acronymSenses);
//		builder.add(parser);
//		builder.add(lucene);
//		builder.add(xWriter);
//
//		// runs the collection reader and the aggregate AE.
//		SimplePipeline.runPipeline(reader, builder.createAggregate());
	}
}
