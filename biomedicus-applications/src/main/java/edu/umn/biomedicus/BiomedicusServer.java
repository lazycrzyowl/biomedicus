package edu.umn.biomedicus;

import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

/**
 * A simple application for testing the feasibility of connecting the AHC datacenter to MSI computational resources.
 * <p/>
 * This is an initial test. No significant processing takes place.
 */
public class BiomedicusServer {

    //TODO: Create a sample clinical document for here instead of short text.
    public static final String SAMPLE_DOCUMENT_TEXT = "some input text left to process";

    public static void main(String[] args) throws Exception {
        Logger logger = UIMAFramework.getLogger();
        logger.log(Level.INFO, "Initializing AHC2MSI test #1");

      /* The analysis engine for processing text */
        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
        logger.log(Level.INFO, "Uima asynchronous engine initialized.");
//        UimaAsBaseCallbackListener asyncListener = new ProcessCallbackListener(uimaAsEngine);//, tunnel);
        logger.log(Level.INFO, "Callback listener initialized.");

      /* Constructs a class to create and run a UIMA pipeline */
//        ServerPipeline uimaPipeline = new ServerPipeline(asyncListener, uimaAsEngine);//, tunnel);

      /* Run the sample document through the pipeline */
        System.out.println("Ready to process document...");
        // TODO: loop through the sample clinical document a few thousand times to get more useful info.
//        uimaPipeline.process(SAMPLE_DOCUMENT_TEXT);
    }
}