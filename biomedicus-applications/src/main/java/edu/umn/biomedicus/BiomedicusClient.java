package edu.umn.biomedicus;

import org.apache.uima.UIMAFramework;
import org.apache.uima.aae.client.UimaAsynchronousEngine;
import org.apache.uima.adapter.jms.client.BaseUIMAAsynchronousEngine_impl;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * A simple test of starting analysis agents.
 */
public class BiomedicusClient {

    public static void main(String[] args) throws Exception {
        Properties prop = BiomedicusClient.getProperties();
        Logger logger = UIMAFramework.getLogger();
        logger.log(Level.INFO, "Initializing AHC2MSI test #1");

      /* This test assumes the client runs on a computations service available over ssh */
      /* Initialize an SSH connection and forward ports */
        AHCConnector tunnel = new AHCConnector(prop.getProperty("rhost"),
                prop.getProperty("username"), prop.getProperty("password"), logger);

      /* The analysis engine for processing text */
        UimaAsynchronousEngine uimaAsEngine = new BaseUIMAAsynchronousEngine_impl();
//        UimaAsBaseCallbackListener asyncListener = new ProcessCallbackListener(uimaAsEngine);

      /* Constructs a class to create and run a UIMA pipeline */
//        AgentPipeline uimaPipeline = new AgentPipeline(asyncListener, uimaAsEngine);//, tunnel);

      /* Run the sample document through the pipeline */
        System.out.println("Ready to process document...");
    }

    public static Properties getProperties() throws Exception {
        Properties prop = new Properties();
        String dir = System.getProperty("user.home");
        InputStream in = new FileInputStream(dir + "/.biomedicus.properties");
        prop.load(in);
        in.close();
        return prop;
    }
}
