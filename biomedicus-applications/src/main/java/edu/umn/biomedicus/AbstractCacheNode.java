package edu.umn.biomedicus;

import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;

import java.io.IOException;

/**
 * ABC for infinispan cache
 */
public abstract class AbstractCacheNode {

    private final EmbeddedCacheManager cacheManager;
    public static final int CLUSTER_SIZE = 2;

    public AbstractCacheNode() throws IOException {
        this.cacheManager = new DefaultCacheManager("cluster.xml");
    }

    public void detach() {
        this.cacheManager.stop();
    }

    protected EmbeddedCacheManager getCacheManager() {
        return cacheManager;
    }

    protected void waitForClusterToForm() {
        if (!ClusterValidation.waitForClusterToForm(getCacheManager(), getNodeId(), CLUSTER_SIZE)) {
            throw new IllegalStateException("Error joining cluster.");
        }
    }


    protected abstract int getNodeId();
}
