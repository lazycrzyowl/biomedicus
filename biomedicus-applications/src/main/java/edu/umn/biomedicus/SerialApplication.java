package edu.umn.biomedicus;

import org.apache.uima.UIMAException;

import java.io.IOException;

/**
 * A test application.
 */
public class SerialApplication {

  public static final String SAMPLE_DOCUMENT_TEXT = "This is an example ClinicalAE note that will be processed by the system.";

  public static void main(String[] args) throws IOException, UIMAException {
    // constructs a class to create and run a UIMA pipeline
//    SerialPipeline uimaPipeline = new SerialPipeline();

    // run the sample document through the pipeline
    System.out.println("Processing document...");
    long before = System.currentTimeMillis();
//    JCas output = uimaPipeline.process(SAMPLE_DOCUMENT_TEXT);
    long after = System.currentTimeMillis();

    // display the time spent processing the text
    System.out.println("Time spent in pipeline: " + (after - before));

    // confirm that the expected annotations were added to the CAS
    System.out.println("Confirming what was added...");
//    FSIterator<Annotation> annotationsIterator = output.getAnnotationIndex().iterator();
//    while (annotationsIterator.hasNext()) {
//      AnnotationFS annotation = (AnnotationFS) annotationsIterator.next();
//      System.out.println("  Found: " + annotation.getClass().getName());
//    }
  }
}