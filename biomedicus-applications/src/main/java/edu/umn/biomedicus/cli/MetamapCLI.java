/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.cli;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.UmlsConcept;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.util.JCasUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

//import edu.umn.biomedicus.acronym.AcronymConceptMapper;
//import edu.umn.biomedicus.metamap.MetaMapAE;
//import edu.umn.biomedicus.sentence.MaxentSentenceBoundaryDetector;

/**
 * This is a command-line interface to using the Metamap annotator. This class
 * assumes the metamap server (mmserver10) is running on the localhost on the
 * default port
 */
public class MetamapCLI {
	public static void main(String[] args) throws ResourceInitializationException, UIMAException, IOException {

		// get type system
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();
//		ExternalResourceDescription biomedicusDb = ComponentFactory.getBiomedicusDb();

		// create descriptions for all the components used
//		AnalysisEngineDescription acronymDescription = ComponentFactory.createAcronymAnnotator();
//		AnalysisEngineDescription acronymConceptMapperDescription = AnalysisEngineFactory.createPrimitiveDescription(
//		                                AcronymConceptMapper.class, typeSystem, AcronymConceptMapper.BIOMEDICUS_DB,
//		                                biomedicusDb);
//
//		AnalysisEngineDescription tokenizerDescription = ComponentFactory.createTokenAnnotator();
//		AnalysisEngineDescription sentenceBoundaryDectorDescription = AnalysisEngineFactory.createPrimitiveDescription(
//		                                MaxentSentenceBoundaryDetector.class, typeSystem);
//		AnalysisEngine tokenizer = AnalysisEngineFactory.createAnalysisEngine(tokenizerDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//		AnalysisEngineDescription metamap = AnalysisEngineFactory.createPrimitiveDescription(MetaMapAE.class,
//		                                typeSystem, MetaMapAE.PARAM_METAMAP_SERVER, "localhost",
//		                                MetaMapAE.PARAM_METAMAP_PORT, "8066");

		// Build it
//		AggregateBuilder builder = new AggregateBuilder();
//		builder.add(acronymDescription);
//		builder.add(acronymConceptMapperDescription);
//		builder.add(sentenceBoundaryDectorDescription);
//		// builder.add(tokenizerDescription);
//		builder.add(metamap);

		// Get memory object (jCas) and input ready
		JCas jCas = ComponentFactory.getJCas();
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String line;
		System.out.print("> ");

		// Wait for command-line input and run the pipeline on that input
		HashMap<Integer, Integer> coveredAlready = new HashMap<Integer, Integer>();
		while ((line = input.readLine()) != null) {
			jCas.reset();
			jCas.setDocumentText(line);
//			SimplePipeline.runPipeline(jCas, builder.createAggregate());

			// Pretty-print the results by concept
			for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
				Integer begin = concept.getBegin();
				Integer end = concept.getEnd();
				if (coveredAlready.containsKey(begin) && coveredAlready.get(begin) == end) {
					// continue;
				}
				prettyPrint(concept);
				coveredAlready.put(begin, end);
			}
			System.out.println();
			System.out.print("> ");
		}
	}

	private static void prettyPrint(UmlsConcept concept) {
		FSArray cuis = concept.getCuis();
		if (cuis == null || cuis.size() == 0) {
			return;
		}
		System.out.print("UmlsConcept Information for text: " + concept.getCoveredText());
		System.out.printf(" (%d,%d)  (%s) \n", concept.getBegin(), concept.getEnd(), concept.getAnnotSrc());

		// Print context information such as negation
		StringArray context = concept.getContext();
		if (context != null && context.size() > 0) {
			System.out.print("    context: ");
			for (int i = 0; i < concept.getContext().size(); i++) {
				System.out.print(concept.getContext(i) + " ");
			}
			System.out.println();
		}

		// Print candidate CUIs
		for (int i = 0; i < cuis.size(); i++) {
			Cui myCui = (Cui) cuis.get(i);
			if (myCui == null)
				continue;
			System.out.printf("        %s: '%s' (%d,%d) ", myCui.getCuiId(), myCui.getCoveredText(), myCui.getBegin(),
			                                myCui.getEnd());
			StringArray semTypes = myCui.getSemanticType();
			if (semTypes != null) {
				for (String type : semTypes.toArray()) {
					System.out.printf("%s ", type);
				}
			}
			System.out.println();

		}

		System.out.println();
	}
}
