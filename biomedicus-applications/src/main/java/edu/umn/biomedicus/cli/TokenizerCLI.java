/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.cli;

//import edu.umn.biomedicus.acronym.AcronymDetectorAE;
//import edu.umn.biomedicus.sentence.MaxentSentenceBoundaryDetector;

public class TokenizerCLI {

	public static void main(String[] args) throws Exception {
//		AnalysisEngineDescription tokenizerDescription = ComponentFactory.createTokenAnnotator();
//		AnalysisEngine tokenizer = AnalysisEngineFactory.createAnalysisEngine(tokenizerDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//		AnalysisEngineDescription sentenceAnnotatorDescription = AnalysisEngineFactory.createPrimitiveDescription(
//		                                MaxentSentenceBoundaryDetector.class, ComponentFactory.getTypeSystem());
//		AnalysisEngine sentenceAnnotator = AnalysisEngineFactory.createAnalysisEngine(sentenceAnnotatorDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//
//		AnalysisEngineDescription acronymAnnotatorDescription = AnalysisEngineFactory.createPrimitiveDescription(
//		                                AcronymDetectorAE.class, ComponentFactory.getTypeSystem(),
//		                                AcronymDetectorAE.PARAM_LEXICON_CONFIG_FILE,
//		                                ComponentFactory.getResourcePath("config/lexAccess.properties"),
//		                                AcronymDetectorAE.PARAM_MODEL_FILE,
//		                                ComponentFactory.getResourcePath("models/acronyms/acronymMEModel.bin"));
//
//		AnalysisEngine acronymAnnotator = AnalysisEngineFactory.createAnalysisEngine(acronymAnnotatorDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//
//		JCas jCas = ComponentFactory.getJCas();
//
//		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
//
//		String line;
//		System.out.print("> ");
//		while ((line = input.readLine()) != null) {
//			jCas.reset();
//			jCas.setDocumentText(line);
//			SimplePipeline.runPipeline(jCas, tokenizer, sentenceAnnotator, acronymAnnotator); // builder.createAggregate());
//			for (Token token : JCasUtil.select(jCas, Token.class)) {
//				System.out.println(token.getCoveredText());
//			}
//
//			System.out.println();
//			System.out.print("> ");
//
//		}

	}
}
