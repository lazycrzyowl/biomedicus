/* 
 Copyright 2011-2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.cli;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import edu.umn.biomedicus.ComponentFactory;
//import edu.umn.biomedicus.lexis.NormAnnotatorFactory;
import edu.umn.biomedicus.type.Token;

/**
 * A simple command line lvg example.
 * 
 * @author Robert Bill
 */

public class LvgCLI {

	public static void main(String[] args) throws Exception {
		// Set LVG system property

//		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
//		AnalysisEngineDescription lvg = NormAnnotatorFactory.createNormAnnotator();
		JCas jCas = ComponentFactory.getJCas();

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String line;
		System.out.print("> ");
		while ((line = input.readLine()) != null) {
			jCas.reset();
			jCas.setDocumentText(line);

//			SimplePipeline.runPipeline(jCas, tokens, lvg);
//			for (Token token : JCasUtil.select(jCas, Token.class)) {
//				System.out.println(token.getCoveredText() + ", " + token.getNorm());
//			}
//
//			System.out.println();
//			System.out.print("> ");
		}
	}
}
