/* 
 Copyright 2011 University of Minnesota 
 
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.cli;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Acronym;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.factory.AggregateBuilder;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This is a command-line interface to using the edu.umn.biomedicus.acronym detector.
 */
public class AcronymDetectorCLI {
	public static void main(String[] args) throws ResourceInitializationException, UIMAException, IOException {
		// get type system
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();
//        AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
//		AnalysisEngineDescription acronyms = AnalysisEngineFactory.createPrimitiveDescription(AcronymDetectorAE_SVM.class,
//		                                typeSystem);

        AggregateBuilder builder = new AggregateBuilder();
//        builder.add(tokens);
//        builder.add(acronyms);
        AnalysisEngine ae = builder.createAggregate();

		// Get memory object (jCas) and input ready
		JCas jcas = ComponentFactory.getJCas();
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		String line;
		System.out.print("> ");

		// Wait for command-line input and run the pipeline on that input
		while ((line = input.readLine()) != null) {
			jcas.reset();
			jcas.setDocumentText(line);
			SimplePipeline.runPipeline(jcas, ae);

			// Pretty-print the results by concept
			for (Acronym ac : JCasUtil.select(jcas, Acronym.class)) {
				prettyPrint(ac);
			}
			System.out.println();
			System.out.print("> ");
		}
	}

	private static void prettyPrint(Acronym ac) {
		System.out.println("Found edu.umn.biomedicus.acronym: '" + ac.getCoveredText() + "' at position " + ac.getBegin());
	}
}
