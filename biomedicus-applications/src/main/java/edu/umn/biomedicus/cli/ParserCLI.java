/* 
 Copyright 2012 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.cli;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Phrase;
import edu.umn.biomedicus.type.Sentence;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.uimafit.util.JCasUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//import edu.umn.biomedicus.parser.BerkeleyParserAE;

/**
 * 
 * @author Robert Bill
 * 
 */

public class ParserCLI {

	public static void main(String[] args) throws Exception {
		String biomedicusHome = System.getenv("BIOMEDICUS_HOME");
		ComponentFactory.setBiomedicusHome(biomedicusHome);
		TypeSystemDescription typeSystem = ComponentFactory.getTypeSystem();

//		AnalysisEngineDescription tokens = ComponentFactory.createTokenAnnotator();
//		AnalysisEngine tokenSegmenter = AnalysisEngineFactory.createAnalysisEngine(tokens, CAS.NAME_DEFAULT_SOFA);
//		AnalysisEngineDescription segmenterDescription = ComponentFactory.createSentenceAnnotator();
//		AnalysisEngine sentSegmenter = AnalysisEngineFactory.createAnalysisEngine(segmenterDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//
//		AnalysisEngineDescription stanfordParserDescription = ComponentFactory.createStanfordParser();
//		AnalysisEngine stanfordParser = AnalysisEngineFactory.createAnalysisEngine(stanfordParserDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//
//		AnalysisEngineDescription berkeleyParserDescription = AnalysisEngineFactory.createPrimitiveDescription(
//		                                BerkeleyParserAE.class, typeSystem);
//		AnalysisEngine berkeleyParser = AnalysisEngineFactory.createAnalysisEngine(berkeleyParserDescription,
//		                                CAS.NAME_DEFAULT_SOFA);

		JCas jCas = ComponentFactory.getJCas();
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String line;
		System.out.print("> ");
		while ((line = input.readLine()) != null) {
			jCas.reset();
			jCas.setDocumentText(line);

//			SimplePipeline.runPipeline(jCas, tokenSegmenter, sentSegmenter, stanfordParser);
			for (Sentence sent : JCasUtil.select(jCas, Sentence.class)) {
				System.out.println("Parse: " + sent.getParse() + "\n \n");
				System.out.println("Phrases:");
				for (Phrase phrase : JCasUtil.selectCovered(Phrase.class, sent)) {
					System.out.printf("%s:  %s\n", phrase.getLabel(), phrase.getCoveredText());
				}
			}

			System.out.println();

			System.out.print("> ");
		}
	}
}
