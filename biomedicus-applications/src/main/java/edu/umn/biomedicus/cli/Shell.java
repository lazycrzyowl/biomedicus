package edu.umn.biomedicus.application.cli;

import org.python.core.PyException;
import org.python.util.InteractiveConsole;

/**
 * This class is an embedded jython interpreter that has some predetermined
 * variables. The goal is to do automated type iterations (e.g.,
 * "for token in jcas") on the jcas object and to do simplified subclassing of
 * the JCasAnnotator_ImplBase class.
 * 
 * TODO: iterator wrapping, jyCas, collectionreaderfactory.
 * 
 * @author Robert Bill
 * 
 */
public class Shell {

	public static void main(String[] args) throws PyException {
		InteractiveConsole interp = new InteractiveConsole();
		interp.exec("from edu.umn.biomedicus import ComponentFactory as factory");
		interp.exec("from org.uimafit.factory import AnalysisEngineFactory");
		interp.exec("from org.uimafit.pipeline import SimplePipeline as pipeline");
		interp.exec("from org.uimafit.factory import AggregateBuilder as Builder");
		interp.exec("from edu.umn.biomedicus.type import Token");
		interp.exec("from edu.umn.biomedicus.type import *");
		interp.exec("from org.uimafit.util import JCasUtil");
		interp.exec("runPipeline = pipeline.runPipeline");
		interp.interact();

	}
}
