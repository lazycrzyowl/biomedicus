package edu.umn.biomedicus.cli;

import edu.umn.biomedicus.type.Cui;
import edu.umn.biomedicus.type.UmlsConcept;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;

import java.io.IOException;
import java.sql.SQLException;

public class ConceptDisambiguationCLI {

//	public static class Options extends Options_ImplBase {
//		@Option(name = "-u", aliases = "--username", usage = "specify username for UMLS database", required = true)
//		public String username;
//
//		@Option(name = "-p", aliases = "--password", usage = "specify password for UMLS database", required = true)
//		public String password;
//	}

	public static void main(String[] args) throws InstantiationException, IllegalAccessException,
	                                ClassNotFoundException, SQLException, UIMAException, IOException {
//
//		Options options = new Options();
//		options.parseArgument(args);
//		String username = options.username;
//		String password = options.password;
//
//		if (username == null) {
//			System.out.println("Missing username and/or password!");
//			System.out.println("\nUSAGE:  ComceptDisambiguationCLI -u myUsername -p myPassword\n");
//			throw new InstantiationException();
//		}
//
//		ExternalResourceDescription umlsRes = ExternalResourceFactory.createExternalResourceDescription(
//		                                UMLSResource_Impl.class, UMLSResource_Impl.PARAM_UMLS_JDBCURL,
//		                                "jdbc:mysql://localhost:3306/umls", UMLSResource_Impl.PARAM_USERNAME, username,
//		                                UMLSResource_Impl.PARAM_PASSWORD, password);
//
//		// Create necessary DB resource
//		String biomedicusHome = System.getenv("BIOMEDICUS_HOME");
//		ExternalResourceDescription biomedicusDB = ExternalResourceFactory.createExternalResourceDescription(
//		                                BiomedicusDBResource_Impl.class, BiomedicusDBResource_Impl.PARAM_DATABASE_HOME,
//		                                biomedicusHome);
//
//		TypeSystemDescription typeDesc = ComponentFactory.getTypeSystem();
//		AnalysisEngineDescription acronyms = ComponentFactory.createAcronymAnnotator();
//		AnalysisEngineDescription sents = ComponentFactory.createSentenceAnnotator();
//		AnalysisEngineDescription mm = ComponentFactory.createMetaMapAnnotator();
//		AnalysisEngineDescription senses = AnalysisEngineFactory.createPrimitiveDescription(SenseRelateAE.class,
//		                                typeDesc, SenseRelateAE.BIOMEDICUS_DB, biomedicusDB);
//
//		// Get memory object (jCas) and input ready
//		JCas jCas = ComponentFactory.getJCas();
//		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
//		String line;
//		System.out.print("> ");
//
//		// Wait for command-line input and run the pipeline on that input
//		while ((line = input.readLine()) != null) {
//			jCas = ComponentFactory.getJCas(); // jCas.reset();
//			jCas.setDocumentText(line);
//
//			SimplePipeline.runPipeline(jCas, acronyms, sents, mm, senses);
//
//			// Pretty-print the results by concept
//			System.out.println("");
//			for (UmlsConcept concept : JCasUtil.select(jCas, UmlsConcept.class)) {
//				prettyPrint(concept);
//				// for (Acronym edu.umn.biomedicus.acronym : JCasUtil.selectCovered(Acronym.class,
//				// concept)) {
//				// System.out.printf("Acronym: %s (%s).\n",
//				// edu.umn.biomedicus.acronym.getCoveredText(), edu.umn.biomedicus.acronym.getExpansion());
//				// }
//			}
//			System.out.println();
//			System.out.print("> ");
//		}
	}

	private static void prettyPrint(UmlsConcept concept) {
		FSArray cuis = concept.getCuis();
		if (cuis == null || cuis.size() == 0)
			return;

		String conceptText = concept.getCoveredText();
		System.out.printf("UmlsConcept Information for text: %s   (%s)", concept.getCoveredText(),
		                                concept.getAnnotSrc());
		StringArray context = concept.getContext();
		if ((context != null) && context.size() > 0) {
			System.out.print("  context: ");
			for (int i = 0; i < concept.getContext().size(); i++) {
				System.out.print(concept.getContext(i) + " ");
			}
		}
		System.out.println();
		for (int i = 0; i < concept.getCuis().size() - 1; i++) {
			Cui cui = (Cui) concept.getCuis(i);
			if (cui == null || cui.getScore() < 900)
				continue;
			System.out.printf("     %-25s", cui.getConceptName());
			System.out.printf("[%s:%s]", cui.getBegin(), cui.getEnd());
			System.out.printf("   CUI: %9s", cui.getCuiId());
			System.out.print(new String("    Score: " + cui.getScore() + "              ").substring(0, 16));
			System.out.print("    Semantic Type: ");
			for (String semType : cui.getSemanticType().toArray()) {
				System.out.print(semType + " ");
			}
			System.out.println();
		}
		Cui sense = (Cui) concept.getCuiSense();
		if (sense == null) {
			System.out.println("    Semantic Relatedness scores missing. Can not disambiguate");
		} else {
			System.out.println("    Preferred sense = " + sense.getCuiId());
		}
		System.out.println();
	}
}
