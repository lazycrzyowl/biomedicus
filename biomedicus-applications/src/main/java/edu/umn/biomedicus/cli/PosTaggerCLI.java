/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */

package edu.umn.biomedicus.cli;

import edu.umn.biomedicus.ComponentFactory;
import edu.umn.biomedicus.type.Sentence;
import edu.umn.biomedicus.type.Token;
import org.apache.uima.jcas.JCas;
import org.uimafit.util.JCasUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * 
 * @author Philip Ogren
 * 
 */

public class PosTaggerCLI {

//	public static class Options extends Options_ImplBase {
//
//		@Option(name = "-m", aliases = "--modelFile", usage = "the part-of-speech tagging model")
//		public String modelFile;
//	}

	public static void main(String[] args) throws Exception {
//		Options options = new Options();
//		options.parseArgument(args);
//
//		AnalysisEngineDescription tokenizerDescription = ComponentFactory.createTokenAnnotator();
//		AnalysisEngineDescription posTaggerDescription;
//		if (options.modelFile != null) {
//			posTaggerDescription = ComponentFactory.createPosAnnotator(options.modelFile);
//		} else {
////			posTaggerDescription = ComponentFactory.createPosAnnotator();
//
//		}
//
		System.out.println("Loading part-of-speech tagging model....");
//		AnalysisEngine tokenizer = AnalysisEngineFactory.createAnalysisEngine(tokenizerDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//		AnalysisEngine posTagger = AnalysisEngineFactory.createAnalysisEngine(posTaggerDescription,
//		                                CAS.NAME_DEFAULT_SOFA);
//
		JCas jCas = ComponentFactory.getJCas();

		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		String line;
		String prompt = "enter sentence> ";
		System.out.print(prompt);
		while ((line = input.readLine()) != null) {
			jCas.reset();
			jCas.setDocumentText(line);
			new Sentence(jCas, 0, line.length()).addToIndexes();
//			SimplePipeline.runPipeline(jCas, tokenizer, posTagger);

			StringBuilder sb = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();

			for (Token token : JCasUtil.select(jCas, Token.class)) {
				sb.append(getSpaces(Math.max(0, token.getBegin() - sb.length())));
				sb.append(token.getPos());
				sb2.append(token.getCoveredText() + "/" + token.getPos() + " ");
			}

			System.out.println(getSpaces(prompt.length()) + sb.toString());
			System.out.println(sb2);
			System.out.println();
			System.out.print(prompt);

		}

	}

	private static String getSpaces(int spaces) {
		char[] chars = new char[spaces];
		Arrays.fill(chars, ' ');
		return new String(chars);
	}
}
