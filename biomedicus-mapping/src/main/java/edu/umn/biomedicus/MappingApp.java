package edu.umn.biomedicus;


import edu.umn.biomedicus.mapping.Configuration;
import edu.umn.biomedicus.mapping.MappingSerialPipeline;
import edu.umn.biomedicus.mapping.services.MetaMapResultMapping;
import org.apache.commons.cli.*;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.util.FileUtils;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Properties;

public class MappingApp {
    static final String mm = "/Users/bill0154/Applications/public_mm/bin/metamap13";
    @NotNull
    private static final Logger logger = UIMAFramework.getLogger(MappingApp.class);
    @NotNull
    private static Properties prop = new Properties();
    @NotNull
    private static Path inputDirectory;
    @NotNull
    private static Path outputDirectory;
    @NotNull
    private static String suffix;
    String test = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<!DOCTYPE MMOs PUBLIC \"-//NLM//DTD MetaMap Machine Output//EN\" \"http://metamap.nlm.nih.gov/DTD/MMOtoXML_v5.dtd\">\n" +
            "<MMOs><MMO><CmdLine><Command>metamap13.BINARY.Darwin -L 2013 --lexicon c -Z 2013AA -I -z --XMLn</Command><Options Count=\"8\"><Option><OptName>lexicon_year</OptName><OptValue>2013</OptValue></Option><Option><OptName>lexicon</OptName><OptValue>c</OptValue></Option><Option><OptName>mm_data_year</OptName><OptValue>2013AA</OptValue></Option><Option><OptName>show_cuis</OptName></Option><Option><OptName>term_processing</OptName></Option><Option><OptName>XMLn</OptName></Option><Option><OptName>infile</OptName><OptValue>user_input</OptValue></Option><Option><OptName>outfile</OptName><OptValue>user_output</OptValue></Option></Options></CmdLine><AAs Count=\"0\" /><Negations Count=\"0\" /><Utterances Count=\"1\"><Utterance><PMID>00000000</PMID><UttSection>tx</UttSection><UttNum>1</UttNum><UttText>Mother has diabetes and heart disease but father only has heart disease.</UttText><UttStartPos>0</UttStartPos><UttLength>72</UttLength><Phrases Count=\"1\"><Phrase><PhraseText>Mother has diabetes and heart disease but father only has heart disease.</PhraseText><SyntaxUnits Count=\"11\"><SyntaxUnit><SyntaxType>head</SyntaxType><LexMatch>mother</LexMatch><InputMatch>Mother</InputMatch><LexCat>noun</LexCat><Tokens Count=\"1\"><Token>mother</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>aux</SyntaxType><LexMatch>has</LexMatch><InputMatch>has</InputMatch><LexCat>aux</LexCat><Tokens Count=\"1\"><Token>has</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>mod</SyntaxType><LexMatch>diabetes</LexMatch><InputMatch>diabetes</InputMatch><LexCat>noun</LexCat><Tokens Count=\"1\"><Token>diabetes</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>conj</SyntaxType><LexMatch>and</LexMatch><InputMatch>and</InputMatch><LexCat>conj</LexCat><Tokens Count=\"1\"><Token>and</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>mod</SyntaxType><LexMatch>heart disease</LexMatch><InputMatch>heart disease</InputMatch><LexCat>noun</LexCat><Tokens Count=\"2\"><Token>heart</Token><Token>disease</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>conj</SyntaxType><LexMatch>but</LexMatch><InputMatch>but</InputMatch><LexCat>conj</LexCat><Tokens Count=\"1\"><Token>but</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>mod</SyntaxType><LexMatch>father</LexMatch><InputMatch>father</InputMatch><LexCat>noun</LexCat><Tokens Count=\"1\"><Token>father</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>adv</SyntaxType><LexMatch>only</LexMatch><InputMatch>only</InputMatch><LexCat>adv</LexCat><Tokens Count=\"1\"><Token>only</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>aux</SyntaxType><LexMatch>has</LexMatch><InputMatch>has</InputMatch><LexCat>aux</LexCat><Tokens Count=\"1\"><Token>has</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>mod</SyntaxType><LexMatch>heart disease</LexMatch><InputMatch>heart disease</InputMatch><LexCat>noun</LexCat><Tokens Count=\"2\"><Token>heart</Token><Token>disease</Token></Tokens></SyntaxUnit><SyntaxUnit><SyntaxType>punc</SyntaxType><InputMatch>.</InputMatch><Tokens Count=\"0\" /></SyntaxUnit></SyntaxUnits><PhraseStartPos>0</PhraseStartPos><PhraseLength>72</PhraseLength><Candidates Total=\"22\" Excluded=\"10\" Pruned=\"0\" Remaining=\"12\"><Candidate><CandidateScore>-787</CandidateScore><CandidateCUI>C0026591</CandidateCUI><CandidateMatched>Mother</CandidateMatched><CandidatePreferred>Mother (person)</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mother</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>famg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>1</TextMatchStart><TextMatchEnd>1</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>yes</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"10\"><Source>AOD</Source><Source>CHV</Source><Source>CSP</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>0</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-779</CandidateScore><CandidateCUI>C0085207</CandidateCUI><CandidateMatched>Maternal diabetes</CandidateMatched><CandidatePreferred>Gestational Diabetes</CandidatePreferred><MatchedWords Count=\"2\"><MatchedWord>maternal</MatchedWord><MatchedWord>diabetes</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"2\"><MatchMap><TextMatchStart>1</TextMatchStart><TextMatchEnd>1</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap><MatchMap><TextMatchStart>3</TextMatchStart><TextMatchEnd>3</TextMatchEnd><ConcMatchStart>2</ConcMatchStart><ConcMatchEnd>2</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>yes</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"13\"><Source>CHV</Source><Source>COSTAR</Source><Source>CSP</Source><Source>ICD10CM</Source><Source>MEDLINEPLUS</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>NDFRT</Source><Source>OMIM</Source><Source>SNM</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"2\"><ConceptPI><StartPos>0</StartPos><Length>6</Length></ConceptPI><ConceptPI><StartPos>11</StartPos><Length>8</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-732</CandidateScore><CandidateCUI>C1826438</CandidateCUI><CandidateMatched>MATER</CandidateMatched><CandidatePreferred>NLRP5 gene</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mater</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>gngm</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>1</TextMatchStart><TextMatchEnd>1</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>yes</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"3\"><Source>HGNC</Source><Source>MTH</Source><Source>OMIM</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>0</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-732</CandidateScore><CandidateCUI>C2347083</CandidateCUI><CandidateMatched>Maternal</CandidateMatched><CandidatePreferred>Maternal Relative</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>maternal</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>famg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>1</TextMatchStart><TextMatchEnd>1</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>yes</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>MTH</Source><Source>NCI</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>0</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-695</CandidateScore><CandidateCUI>C0721591</CandidateCUI><CandidateMatched>materna</CandidateMatched><CandidatePreferred>materna</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>materna</MatchedWord></MatchedWords><SemTypes Count=\"3\"><SemType>orch</SemType><SemType>phsu</SemType><SemType>vita</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>1</TextMatchStart><TextMatchEnd>1</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>5</LexVariation></MatchMap></MatchMaps><IsHead>yes</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"1\"><Source>CHV</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>0</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-632</CandidateScore><CandidateCUI>C0018799</CandidateCUI><CandidateMatched>Heart disease</CandidateMatched><CandidatePreferred>Heart Diseases</CandidatePreferred><MatchedWords Count=\"2\"><MatchedWord>heart</MatchedWord><MatchedWord>disease</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>5</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>2</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"17\"><Source>AOD</Source><Source>CCS</Source><Source>CHV</Source><Source>COSTAR</Source><Source>CSP</Source><Source>CST</Source><Source>ICD10CM</Source><Source>ICD9CM</Source><Source>MEDLINEPLUS</Source><Source>MSH</Source><Source>MTH</Source><Source>MTHICD9</Source><Source>NCI</Source><Source>NDFRT</Source><Source>SNM</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>24</StartPos><Length>13</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0011847</CandidateCUI><CandidateMatched>Diabetes</CandidateMatched><CandidatePreferred>Diabetes</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>diabetes</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>3</TextMatchStart><TextMatchEnd>3</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>LNC</Source><Source>MTH</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>11</StartPos><Length>8</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0011849</CandidateCUI><CandidateMatched>Diabetes</CandidateMatched><CandidatePreferred>Diabetes Mellitus</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>diabetes</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>3</TextMatchStart><TextMatchEnd>3</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"22\"><Source>AIR</Source><Source>AOD</Source><Source>CHV</Source><Source>COSTAR</Source><Source>CSP</Source><Source>CST</Source><Source>DXP</Source><Source>ICD10CM</Source><Source>ICD9CM</Source><Source>ICPC</Source><Source>LNC</Source><Source>MEDLINEPLUS</Source><Source>MSH</Source><Source>MTH</Source><Source>MTHICD9</Source><Source>NCI</Source><Source>NDFRT</Source><Source>OMIM</Source><Source>QMR</Source><Source>SNM</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>11</StartPos><Length>8</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0012634</CandidateCUI><CandidateMatched>Disease</CandidateMatched><CandidatePreferred>Disease</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>disease</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>6</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"11\"><Source>CHV</Source><Source>CSP</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>NDFRT</Source><Source>SNM</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>30</StartPos><Length>7</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0015671</CandidateCUI><CandidateMatched>Father</CandidateMatched><CandidatePreferred>Father (person)</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>father</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>famg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>8</TextMatchStart><TextMatchEnd>8</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"11\"><Source>AOD</Source><Source>CHV</Source><Source>CSP</Source><Source>HL7V3.0</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>42</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0018787</CandidateCUI><CandidateMatched>Heart</CandidateMatched><CandidatePreferred>Heart</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>heart</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>bpoc</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>5</TextMatchStart><TextMatchEnd>5</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"18\"><Source>AIR</Source><Source>AOD</Source><Source>CHV</Source><Source>CSP</Source><Source>FMA</Source><Source>HL7V2.5</Source><Source>ICF</Source><Source>ICF-CY</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>OMIM</Source><Source>SNM</Source><Source>SNMI</Source><Source>SNOMEDCT</Source><Source>UWDA</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>24</StartPos><Length>5</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0205171</CandidateCUI><CandidateMatched>Only</CandidateMatched><CandidatePreferred>Singular</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>only</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>qnco</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>9</TextMatchStart><TextMatchEnd>9</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"5\"><Source>CHV</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>49</StartPos><Length>4</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C1281570</CandidateCUI><CandidateMatched>Heart</CandidateMatched><CandidatePreferred>Entire heart</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>heart</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>bpoc</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>5</TextMatchStart><TextMatchEnd>5</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>MTH</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>24</StartPos><Length>5</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C1720467</CandidateCUI><CandidateMatched>Only</CandidateMatched><CandidatePreferred>Only - dosing instruction fragment</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>only</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>inpr</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>9</TextMatchStart><TextMatchEnd>9</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>MTH</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>49</StartPos><Length>4</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-616</CandidateScore><CandidateCUI>C3534591</CandidateCUI><CandidateMatched>Diabetic Heart Disease</CandidateMatched><CandidatePreferred>Diabetic Heart Disease</CandidatePreferred><MatchedWords Count=\"3\"><MatchedWord>diabetic</MatchedWord><MatchedWord>heart</MatchedWord><MatchedWord>disease</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"2\"><MatchMap><TextMatchStart>3</TextMatchStart><TextMatchEnd>3</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>3</LexVariation></MatchMap><MatchMap><TextMatchStart>5</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>2</ConcMatchStart><ConcMatchEnd>3</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"1\"><Source>MEDLINEPLUS</Source></Sources><ConceptPIs Count=\"2\"><ConceptPI><StartPos>11</StartPos><Length>8</Length></ConceptPI><ConceptPI><StartPos>24</StartPos><Length>13</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-565</CandidateScore><CandidateCUI>C1416989</CandidateCUI><CandidateMatched>MAL</CandidateMatched><CandidatePreferred>MAL gene</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mal</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>gngm</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>6</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"4\"><Source>HGNC</Source><Source>MSH</Source><Source>MTH</Source><Source>OMIM</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>30</StartPos><Length>7</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-565</CandidateScore><CandidateCUI>C1422572</CandidateCUI><CandidateMatched>MAL</CandidateMatched><CandidatePreferred>MKL1 gene</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mal</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>gngm</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>6</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"4\"><Source>HGNC</Source><Source>MTH</Source><Source>NCI</Source><Source>OMIM</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>30</StartPos><Length>7</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-565</CandidateScore><CandidateCUI>C1424810</CandidateCUI><CandidateMatched>MAL</CandidateMatched><CandidatePreferred>TIRAP gene</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mal</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>gngm</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>6</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"3\"><Source>HGNC</Source><Source>MTH</Source><Source>OMIM</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>30</StartPos><Length>7</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-565</CandidateScore><CandidateCUI>C1836721</CandidateCUI><CandidateMatched>MALS</CandidateMatched><CandidatePreferred>MALARIA, MILD, SUSCEPTIBILITY TO</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mals</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>fndg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>6</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"1\"><Source>OMIM</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>30</StartPos><Length>7</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-565</CandidateScore><CandidateCUI>C3273340</CandidateCUI><CandidateMatched>MAL</CandidateMatched><CandidatePreferred>MKL1 wt Allele</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mal</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>gngm</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>6</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>MTH</Source><Source>NCI</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>30</StartPos><Length>7</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-565</CandidateScore><CandidateCUI>C3273341</CandidateCUI><CandidateMatched>MAL</CandidateMatched><CandidatePreferred>MKL/Myocardin-Like Protein 1</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mal</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>aapp</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>6</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>2</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>MTH</Source><Source>NCI</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>30</StartPos><Length>7</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-549</CandidateScore><CandidateCUI>C0241863</CandidateCUI><CandidateMatched>DIABETIC</CandidateMatched><CandidatePreferred>DIABETIC</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>diabetic</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>fndg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>3</TextMatchStart><TextMatchEnd>3</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>3</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>CHV</Source><Source>DXP</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>11</StartPos><Length>8</Length></ConceptPI></ConceptPIs><Status>1</Status><Negated>0</Negated></Candidate></Candidates><Mappings Count=\"2\"><Mapping><MappingScore>-706</MappingScore><MappingCandidates Total=\"4\"><Candidate><CandidateScore>-787</CandidateScore><CandidateCUI>C0026591</CandidateCUI><CandidateMatched>Mother</CandidateMatched><CandidatePreferred>Mother (person)</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mother</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>famg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>1</TextMatchStart><TextMatchEnd>1</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>yes</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"10\"><Source>AOD</Source><Source>CHV</Source><Source>CSP</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>0</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-616</CandidateScore><CandidateCUI>C3534591</CandidateCUI><CandidateMatched>Diabetic Heart Disease</CandidateMatched><CandidatePreferred>Diabetic Heart Disease</CandidatePreferred><MatchedWords Count=\"3\"><MatchedWord>diabetic</MatchedWord><MatchedWord>heart</MatchedWord><MatchedWord>disease</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"2\"><MatchMap><TextMatchStart>3</TextMatchStart><TextMatchEnd>3</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>3</LexVariation></MatchMap><MatchMap><TextMatchStart>5</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>2</ConcMatchStart><ConcMatchEnd>3</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"1\"><Source>MEDLINEPLUS</Source></Sources><ConceptPIs Count=\"2\"><ConceptPI><StartPos>11</StartPos><Length>8</Length></ConceptPI><ConceptPI><StartPos>24</StartPos><Length>13</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0015671</CandidateCUI><CandidateMatched>Father</CandidateMatched><CandidatePreferred>Father (person)</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>father</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>famg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>8</TextMatchStart><TextMatchEnd>8</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"11\"><Source>AOD</Source><Source>CHV</Source><Source>CSP</Source><Source>HL7V3.0</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>42</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0205171</CandidateCUI><CandidateMatched>Only</CandidateMatched><CandidatePreferred>Singular</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>only</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>qnco</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>9</TextMatchStart><TextMatchEnd>9</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"5\"><Source>CHV</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>49</StartPos><Length>4</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate></MappingCandidates></Mapping><Mapping><MappingScore>-706</MappingScore><MappingCandidates Total=\"4\"><Candidate><CandidateScore>-787</CandidateScore><CandidateCUI>C0026591</CandidateCUI><CandidateMatched>Mother</CandidateMatched><CandidatePreferred>Mother (person)</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>mother</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>famg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>1</TextMatchStart><TextMatchEnd>1</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>yes</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"10\"><Source>AOD</Source><Source>CHV</Source><Source>CSP</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>0</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-616</CandidateScore><CandidateCUI>C3534591</CandidateCUI><CandidateMatched>Diabetic Heart Disease</CandidateMatched><CandidatePreferred>Diabetic Heart Disease</CandidatePreferred><MatchedWords Count=\"3\"><MatchedWord>diabetic</MatchedWord><MatchedWord>heart</MatchedWord><MatchedWord>disease</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>dsyn</SemType></SemTypes><MatchMaps Count=\"2\"><MatchMap><TextMatchStart>3</TextMatchStart><TextMatchEnd>3</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>3</LexVariation></MatchMap><MatchMap><TextMatchStart>5</TextMatchStart><TextMatchEnd>6</TextMatchEnd><ConcMatchStart>2</ConcMatchStart><ConcMatchEnd>3</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"1\"><Source>MEDLINEPLUS</Source></Sources><ConceptPIs Count=\"2\"><ConceptPI><StartPos>11</StartPos><Length>8</Length></ConceptPI><ConceptPI><StartPos>24</StartPos><Length>13</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C0015671</CandidateCUI><CandidateMatched>Father</CandidateMatched><CandidatePreferred>Father (person)</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>father</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>famg</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>8</TextMatchStart><TextMatchEnd>8</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"11\"><Source>AOD</Source><Source>CHV</Source><Source>CSP</Source><Source>HL7V3.0</Source><Source>LCH</Source><Source>LNC</Source><Source>MSH</Source><Source>MTH</Source><Source>NCI</Source><Source>SNMI</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>42</StartPos><Length>6</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate><Candidate><CandidateScore>-621</CandidateScore><CandidateCUI>C1720467</CandidateCUI><CandidateMatched>Only</CandidateMatched><CandidatePreferred>Only - dosing instruction fragment</CandidatePreferred><MatchedWords Count=\"1\"><MatchedWord>only</MatchedWord></MatchedWords><SemTypes Count=\"1\"><SemType>inpr</SemType></SemTypes><MatchMaps Count=\"1\"><MatchMap><TextMatchStart>9</TextMatchStart><TextMatchEnd>9</TextMatchEnd><ConcMatchStart>1</ConcMatchStart><ConcMatchEnd>1</ConcMatchEnd><LexVariation>0</LexVariation></MatchMap></MatchMaps><IsHead>no</IsHead><IsOverMatch>no</IsOverMatch><Sources Count=\"2\"><Source>MTH</Source><Source>SNOMEDCT</Source></Sources><ConceptPIs Count=\"1\"><ConceptPI><StartPos>49</StartPos><Length>4</Length></ConceptPI></ConceptPIs><Status>0</Status><Negated>0</Negated></Candidate></MappingCandidates></Mapping></Mappings></Phrase></Phrases></Utterance></Utterances></MMO></MMOs>";


    public MappingApp(Path outputDirectory, Iterator<Path> files) throws IOException, UIMAException
    {

        // Get pipeline
        MappingSerialPipeline uimaPipeline = new MappingSerialPipeline();

        while (files.hasNext())
        {
            Path currentFile = files.next();
            String documentText = FileUtils.file2String(currentFile.toFile());

            // skip empty documents
            if (documentText == null || documentText.trim() == "")
            {
                continue;
            }

            if (!currentFile.endsWith("97_100.txt")) continue;

            // run the sample document through the pipeline
            System.out.println("Processing document: " + currentFile.toString());
            long before = System.currentTimeMillis();
            CAS output = uimaPipeline.process(currentFile.toString(), documentText);
            long after = System.currentTimeMillis();
            CAS metadata = output.getView("MetaData");
            String outputName = new File(metadata.getDocumentText()).getName();
            File dest = new File(outputDirectory.toString(), outputName);
            try
            {
                FileOutputStream os = new FileOutputStream(dest);
                XmiCasSerializer.serialize(output, os);
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            } catch (SAXException e)
            {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception
    {
        // Get general properties
        Configuration _config = new Configuration();
        System.out.println(_config.getString("biomedicus_cli_banner"));

        // Start logging
        logger.setLevel(Level.OFF);

        // Construction commandline options, get values, and validate what was supplied
        Options cliOptions = getCLIOptions();
        CommandLine cliValues = getCLIValues(cliOptions, args);
        String suffix = ".txt";

        if (cliValues.hasOption("input") && cliValues.hasOption("destination"))
        {
            // set path for input and output
            inputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("input"));
            //outputDirectory = FileSystems.getDefault().getPath(cliValues.getOptionValue("destination"));

            // Use the configuration setting for output dir if one is not specified at cl
            outputDirectory = cliValues.getOptionValue("destination") != null ?
                    FileSystems.getDefault().getPath(cliValues.getOptionValue("destination")) :
                    FileSystems.getDefault().getPath(_config.getString("output_dir"));
            // get suffix, or set to default (.txt)
            String suffix_opt = cliValues.getOptionValue("suffix");
            if (suffix_opt != null && suffix_opt != "")
            {
                suffix = suffix_opt;
            }

            // display provided options
            System.out.println("Setting input directory to: " + inputDirectory.toString());
            System.out.println("Setting input file suffix to: " + suffix);
            System.out.println("Setting destination directory to: " + outputDirectory.toString());

        } else
        {
            printHelp("FamilyHistorySerialApplication", cliOptions);
        }

        // Create a DirectoryStream which accepts only filenames ending with specified suffix
        DirectoryStream<Path> ds = Files.newDirectoryStream(inputDirectory, "*" + suffix);

        // constructs a class to create and run a UIMA pipeline

        MappingApp app = new MappingApp(outputDirectory, ds.iterator());
    }

    private static Options getCLIOptions()
    {
        // create the Options
        Options options = new Options();
        options.addOption("i", "input", true, "Input directory containing text files (*.txt).");
        options.addOption("d", "destination", true, "Destination directory to output xmi files.");
        options.addOption("s", "suffix", true, "Suffix/extension of input files to process (default=.txt)");
        return options;
    }

    public static CommandLine getCLIValues(Options options, String[] args)
    {
        CommandLineParser parser = new PosixParser();
        CommandLine line = null;

        try
        {
            // parse the command line arguments
            line = parser.parse(options, args);
        } catch (ParseException e)
        {
            System.out.println("Unexpected exception when parsing command-line options" + e.getMessage());
        }
        return line;
    }

    private static void printHelp(String name, Options options)
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(name, options);
        System.exit(1);
    }

    public String getMapping(String s) throws Exception
    {

        Path plainTextPath = Files.createTempFile("metamap-temp", ".mminput.tmp");
        Path mmAnnotatedPath = Files.createTempFile("metamap-temp", ".mmoutput.tmp");

        // write source data to temp file
        FileOutputStream mmTextSource = new FileOutputStream(plainTextPath.toFile());
        mmTextSource.write(s.getBytes());
        // metamap needs at least one new line character at the end
        mmTextSource.write("\n\n".getBytes());
        mmTextSource.flush();


        // execute metamap on temp files and wait for it to finish
        ProcessBuilder pb = new ProcessBuilder(mm, "  -I -z --XMLn --negex", plainTextPath.toString(),
                mmAnnotatedPath.toString());
        Process process = pb.start();
        process.waitFor();

        // get results from outputfile
        FileInputStream mmResults = new FileInputStream(mmAnnotatedPath.toFile());
        StringBuilder sb = new StringBuilder();
        ByteBuffer bb = ByteBuffer.allocate(1024);
        String results = new String(Files.readAllBytes(mmAnnotatedPath), "UTF-8");

        //call the same delete on exit
        plainTextPath.toFile().delete();
        mmAnnotatedPath.toFile().delete();
        return results;
    }


    public void parse(String s) throws Exception
    {

        int start = s.indexOf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        String xml = s.substring(start);


        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        // Setup a new eventReader
        InputStream in = new ByteArrayInputStream(xml.getBytes("UTF-8"));
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
        // read the XML document


        MetaMapResultMapping results = new MetaMapResultMapping();

        while (eventReader.hasNext())
        {
            XMLEvent event = eventReader.nextEvent();

            if (event.isStartElement())
            {
                StartElement element = event.asStartElement();
                String name = element.getName().getLocalPart();
                if (element.isEndElement())
                {
                    continue;
                }

                results.dispatch(name, element, eventReader);
            }
        }
    }
}



      /*8****************************************

          public static void main(String[] args) throws IOException, UIMAException, ConfigurationException
          {
          }

      }/*

                if (startElement.getName().getLocalPart() == (ITEM))
                {
                    item = new Item();
                    // We read the attributes from this tag and add the date
                    // attribute to our object
                    Iterator<Attribute> attributes = startElement
                            .getAttributes();
                    while (attributes.hasNext())
                    {
                        Attribute attribute = attributes.next();
                        if (attribute.getName().toString().equals(DATE))
                        {
                            item.setDate(attribute.getValue());
                        }

                    }
                }

                if (event.isStartElement())
                {
                    if (event.asStartElement().getName().getLocalPart()
                            .equals(MODE))
                    {
                        event = eventReader.nextEvent();
                        item.setMode(event.asCharacters().getData());
                        continue;
                    }
                }
                if (event.asStartElement().getName().getLocalPart()
                        .equals(UNIT))
                {
                    event = eventReader.nextEvent();
                    item.setUnit(event.asCharacters().getData());
                    continue;
                }

                if (event.asStartElement().getName().getLocalPart()
                        .equals(CURRENT))
                {
                    event = eventReader.nextEvent();
                    item.setCurrent(event.asCharacters().getData());
                    continue;
                }

                if (event.asStartElement().getName().getLocalPart()
                        .equals(INTERACTIVE))
                {
                    event = eventReader.nextEvent();
                    item.setInteractive(event.asCharacters().getData());
                    continue;
                }
            }
            // If we reach the end of an item element, we add it to the list
            if (event.isEndElement())
            {
                EndElement endElement = event.asEndElement();
                if (endElement.getName().getLocalPart() == (ITEM))
                {
                    items.add(item);
                }
            }

        }
    }

    return items;

}
}


// python code

/*
    ## labeled data
    data_root = '/u/dgillick/workspace/sbd/'
    brown_data = data_root + 'whiskey/brown.1'
    wsj_data = data_root + 'whiskey/satz.1'
    poe_data = data_root + 'whiskey/poe.1'
    new_wsj_data = data_root + 'whiskey/wsj.1'

    ## install root
    install_root = '/u/dgillick/sbd/splitta/'

    ## options
    from optparse import OptionParser
    usage = 'usage: %prog [options] <text_file>'
    parser = OptionParser(usage=usage)
    parser.add_option('-v', '--verbose', dest='verbose', default=False,
                      action='store_true', help='verbose output')
    parser.add_option('-t', '--tokenize', dest='tokenize', default=False,
                      action='store_true', help='write tokenized output')
    parser.add_option('-m', '--model', dest='model_path', type='str', default='model_nb',
                      help='model path')
    parser.add_option('-o', '--output', dest='output', type='str', default=None,
                      help='write sentences to this file')
    parser.add_option('-x', '--train', dest='train', type='str', default=None,
                      help='train a new model using this labeled data file')
    parser.add_option('-c', '--svm', dest='svm', default=False,
                      action='store_true', help='use SVM instead of Naive Bayes for training')
    (options, args) = parser.parse_args()

    ## get test file
    if len(args) > 0:
        options.test = args[0]
        if not os.path.isfile(options.test): sbd_util.die('test path [%s] does not exist' %options.test)
    else:
        options.test = None
        if not options.train: sbd_util.die('you did not specify either train or test!')

    ## create model path
    if not options.model_path.endswith('/'): options.model_path += '/'
    if options.train:
        if not os.path.isfile(options.train): sbd_util.die('model path [%s] does not exist' %options.train)
        if os.path.isdir(options.model_path): sbd_util.die('model path [%s] already exists' %options.model_path)
        else: os.mkdir(options.model_path)
    else:
        if not os.path.isdir(options.model_path):
            options.model_path = install_root + options.model_path
            if not os.path.isdir(options.model_path):
                sbd_util.die('model path [%s] does not exist' %options.model_path)

    ## create a model
    if options.train:
        model = build_model(options.train, options)

    if not options.test: sys.exit()

    ## test
    if not options.train:
        if 'svm' in options.model_path: options.svm = True
        model = load_sbd_model(options.model_path, options.svm)
    if options.output: options.output = open(options.output, 'w')

    test = get_data(options.test, tokenize=True)
    test.featurize(model, verbose=True)
    model.classify(test, verbose=True)
    test.segment(use_preds=True, tokenize=options.tokenize, output=options.output)

 */