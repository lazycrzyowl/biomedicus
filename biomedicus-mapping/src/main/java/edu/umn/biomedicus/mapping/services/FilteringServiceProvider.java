package edu.umn.biomedicus.mapping.services;


public abstract class FilteringServiceProvider {
    public abstract boolean filter(String annotationType);
}
