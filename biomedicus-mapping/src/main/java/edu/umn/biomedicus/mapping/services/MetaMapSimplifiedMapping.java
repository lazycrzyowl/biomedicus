package edu.umn.biomedicus.mapping.services;


import edu.umn.biomedicus.ontology.SemanticType;
import opennlp.tools.util.Span;
import org.jetbrains.annotations.NotNull;

public class MetaMapSimplifiedMapping {
    @NotNull
    String cui;
    @NotNull
    String matchedText;
    @NotNull
    String preferredText;
    @NotNull
    int score;
    @NotNull
    int characterMatchBegin;  // mapped from PI start
    @NotNull
    int characterMatchEnd;  // mapped from  PI end
    @NotNull
    int tokenMatchBegin;  // mapped from text match start
    @NotNull
    int tokenMatchEnd;  // mapped from text match end
    @NotNull
    boolean isHead;
    @NotNull
    SemanticType[] semanticTypes;
    @NotNull
    String[] sources;
    @NotNull
    boolean negated;
    @NotNull
    String phraseText;
    @NotNull
    int phraseBegin;
    @NotNull
    int phraseEnd;


    @NotNull
    public int getPhraseBegin()
    {
        return phraseBegin;
    }

    public void setPhraseBegin(@NotNull int phraseBegin)
    {
        this.phraseBegin = phraseBegin;
    }

    @NotNull
    public int getPhraseEnd()
    {
        return phraseEnd;
    }

    public void setPhraseEnd(@NotNull int phraseEnd)
    {
        this.phraseEnd = phraseEnd;
    }

    @NotNull
    public Span[] getConceptPIs()
    {
        return conceptPIs;
    }

    public void setConceptPIs(@NotNull Span[] conceptPIs)
    {
        this.conceptPIs = conceptPIs;
    }

    @NotNull
    Span[] conceptPIs;

    public MetaMapSimplifiedMapping(String phraseText, int phraseBegin, int phraseEnd)
    {
        this.phraseText = phraseText;
        this.phraseBegin = phraseBegin;
        this.phraseEnd = phraseEnd;
    }

    @NotNull
    public String getCui()
    {
        return cui;
    }

    public void setCui(@NotNull String cui)
    {
        this.cui = cui;
    }

    @NotNull
    public String getMatchedText()
    {
        return matchedText;
    }

    public void setMatchedText(@NotNull String matchedText)
    {
        this.matchedText = matchedText;
    }

    @NotNull
    public String getPreferredText()
    {
        return preferredText;
    }

    public void setPreferredText(@NotNull String preferredText)
    {
        this.preferredText = preferredText;
    }

    @NotNull
    public int getScore()
    {
        return score;
    }

    public void setScore(@NotNull int score)
    {
        this.score = score;
    }

    @NotNull
    public int getCharacterMatchBegin()
    {
        return characterMatchBegin;
    }

    public void setCharacterMatchBegin(@NotNull int characterMatchBegin)
    {
        this.characterMatchBegin = characterMatchBegin;
    }

    @NotNull
    public int getCharacterMatchEnd()
    {
        return characterMatchEnd;
    }

    public void setCharacterMatchEnd(@NotNull int characterMatchEnd)
    {
        this.characterMatchEnd = characterMatchEnd;
    }

    @NotNull
    public int getTokenMatchBegin()
    {
        return tokenMatchBegin;
    }

    public void setTokenMatchBegin(@NotNull int tokenMatchBegin)
    {
        this.tokenMatchBegin = tokenMatchBegin;
    }

    @NotNull
    public int getTokenMatchEnd()
    {
        return tokenMatchEnd;
    }

    public void setTokenMatchEnd(@NotNull int tokenMatchEnd)
    {
        this.tokenMatchEnd = tokenMatchEnd;
    }

    @NotNull
    public boolean isHead()
    {
        return isHead;
    }

    public void setHead(@NotNull boolean isHead)
    {
        this.isHead = isHead;
    }

    @NotNull
    public SemanticType[] getSemanticTypes()
    {
        return semanticTypes;
    }

    public void setSemanticTypes(@NotNull SemanticType[] semanticTypes)
    {
        this.semanticTypes = semanticTypes;
    }

    @NotNull
    public String[] getSources()
    {
        return sources;
    }

    public void setSources(@NotNull String[] sources)
    {
        this.sources = sources;
    }

    @NotNull
    public boolean isNegated()
    {
        return negated;
    }

    public void setNegated(@NotNull boolean negated)
    {
        this.negated = negated;
    }

    @NotNull
    public String getPhraseText()
    {
        return phraseText;
    }

    public void setPhraseText(@NotNull String phraseText)
    {
        this.phraseText = phraseText;
    }
}
