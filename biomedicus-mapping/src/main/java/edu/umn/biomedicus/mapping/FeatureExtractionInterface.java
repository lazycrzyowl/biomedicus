package edu.umn.biomedicus.mapping;

import edu.umn.biomedicus.mapping.services.FeatureSet;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;

public interface FeatureExtractionInterface {

    public FeatureSet cas2FeatureSet(CAS aCAS);
    public AnnotationFS featureSet2Cas(FeatureSet featureSet);
}
