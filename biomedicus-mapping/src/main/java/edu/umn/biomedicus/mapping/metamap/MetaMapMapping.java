package edu.umn.biomedicus.mapping.metamap;


import gov.nih.nlm.nls.metamap.*;

import java.util.HashMap;
import java.util.List;

public class MetaMapMapping {
    int begin;
    int end;
    String cui;
    String stAbbrev;
    String semanticType;
    boolean negated;
    String negationTrigger;
    String mappingId;

    public static MetaMapMapping create(int begin, int end)
    {
        MetaMapMapping mapping = new MetaMapMapping();
        mapping.setBegin(begin).setEnd(end);
        return mapping;
    }

    public int getBegin() {return begin;}
    public MetaMapMapping setBegin(int begin) {this.begin = begin; return this;}

    public int getEnd() {return end;}
    public MetaMapMapping setEnd(int end) {this.end = end; return this;}

    public String getCui() {return cui;}
    public MetaMapMapping setCui(String cui) {this.cui = cui; return this;}

    public String getStAbbrev() {return stAbbrev;}
    public MetaMapMapping setStAbbrev(String stAbbrev) {this.stAbbrev = stAbbrev; return this;}

    public String getSemanticType() {return semanticType;}
    public MetaMapMapping setSemanticType(String semanticType) {this.semanticType = semanticType; return this;}

    public boolean isNegated() {return negated;}
    public MetaMapMapping setNegated(boolean negated) {this.negated = negated; return this;}

    public String getNegationTrigger() {return negationTrigger;}
    public MetaMapMapping setNegationTrigger(String negationTrigger) {
        this.negationTrigger = negationTrigger; return this;}

    public String getMappingId() {return mappingId;}
    public MetaMapMapping setMappingId(String mappingId) {this.mappingId = mappingId; return this;}

    public static HashMap<String,MetaMapMapping> extractNegations(Result result, int offset)
            throws MetaMapResultExtractionException, Exception
    {
        HashMap<String, MetaMapMapping> negations = new HashMap<>();
        for (gov.nih.nlm.nls.metamap.Negation neg : result.getNegationList()) {
            for (Position position : neg.getConceptPositionList()) {

                String trigger = neg.getTrigger();
                List<Position> tPosList = neg.getTriggerPositionList();
                if (tPosList.size() == 1) {
                    Position p = tPosList.get(0);
                    int begin = p.getX() + offset;
                    int end = begin + p.getY();
                }
                String type = neg.getType();
                List<ConceptPair> conceptPairList = neg.getConceptPairList();

                //int begin = position.getX() + sent.getBegin() + offset;
                //int end = begin + position.getY();
                int begin = position.getX() + offset;
                int end = begin + position.getY();
                //negations.put(begin, end);
            }
        }
        return negations;
    }

    public static HashMap<String, MetaMapMapping> extractAcronyms(Result result, int offset)
            throws MetaMapResultExtractionException, Exception
    {
        HashMap<String, MetaMapMapping> acronyms = new HashMap<>();

        for (AcronymsAbbrevs mmAbbrev : result.getAcronymsAbbrevsList()) {
            String candidate = mmAbbrev.getAcronym();

            // not sure how these where getting mapped, but skip for now.
            if (candidate.equals("to") || candidate.equals("not") || candidate.equals("in"))
                                continue;
            String expansion = mmAbbrev.getExpansion();
            List<Integer> countList = mmAbbrev.getCountList();
            List<String> cuiList = mmAbbrev.getCUIList();
        }

        return acronyms;
    }

    public static HashMap<String, MetaMapMapping> extractMappings(Result result, int offset)
            throws MetaMapResultExtractionException, Exception
    {
        HashMap<String, MetaMapMapping> mappings = new HashMap<>();

        return mappings;
    }


    public static void main(String[] args)
    {

    }
}
