package edu.umn.biomedicus.mapping.services;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Iterator;

public class MetaMapResultMapping {

    private int utteranceCount = 0;
    private int negationCount = 0;
    private int aasCount = 0;

    public void setUtteranceCount(int i)
    {
        utteranceCount = i;
    }

    public void dispatch(String elementName, StartElement se, XMLEventReader eventReader)
            throws XMLStreamException
    {
        Iterator<Attribute> attributes = null;
        switch (elementName) {
            case "Utterances" :
                attributes = se.getAttributes();
                utteranceCount = Integer.valueOf(attributes.next().getValue());
                break;
            case "Negations" :
                attributes = se.getAttributes();
                negationCount = Integer.valueOf(attributes.next().getValue());
                break;
            case "AAs" :
                attributes = se.getAttributes();
                aasCount = Integer.valueOf(attributes.next().getValue());
                break;
            case "Utterance" :
                MetaMapUtterance utt = getUtterance(eventReader);
            default:
                //System.out.println("EVENT NAME: " + elementName);
        }
    }


    private MetaMapUtterance getUtterance(XMLEventReader eventReader)
            throws XMLStreamException
    {
        MetaMapUtterance ut = new MetaMapUtterance();
        while (eventReader.hasNext())
        {
            XMLEvent event = eventReader.nextEvent();
            String currentField = "";
            if (event.isStartElement())
            {
                StartElement eventStart = event.asStartElement();
                System.out.println("EVENT: " + eventStart.getName().getLocalPart());
                if (eventStart.getName().getLocalPart().equals("PMID"))
                {
                    event = eventReader.nextEvent();
                    boolean isatt = event.isAttribute();
                    String pmid = event.asCharacters().getData();
                    ut.PMID = pmid;

                    // get PMID end tag
                    event = eventReader.nextEvent();
                    if (event.isEndElement()) continue;
                    throw new XMLStreamException("this shouldn't happen!");
                }


                if (eventStart.isCharacters())
                {
                    System.out.println(eventStart.asCharacters());
                }
                Iterator<Attribute> attributeIterator = event.asStartElement().getAttributes();
                while (attributeIterator.hasNext())
                {
                    Attribute att = attributeIterator.next();
                    System.out.println("ATTRIBUTE: " + att.getName() + " = " + att.getValue());

                }

            }
        }
        //Iterator<Attribute> attributeIterator = eventReader.getAttributes();
//        while (attributeIterator.hasNext())
//        {
//            Attribute a = attributeIterator.next();
//
//        }
        System.out.println("\n\tFOUND MM UTTERANCE\n") ;
        return new MetaMapUtterance();
    }
}
