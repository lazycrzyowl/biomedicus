/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.mapping.services;

import edu.umn.biomedicus.dictionary.SemanticTypeDictionary;
import edu.umn.biomedicus.type.*;
//import gov.nih.nlm.nls.metamap.*;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Logger;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * <p>
 * This class connects to a running metamap server (mmserver10) using the
 * metamap java api and places the metamap results into a
 * {@link edu.umn.biomedicus.type.UmlsConcept} annotation object.
 * </p>
 * <p>
 * Currently, this analysis engine sends sentences to mmserver10. There didn't
 * seem to be a huge speed motivation to send larger chuncks; however, the next
 * version will likely send smaller chunks in hopes of caching results and
 * speeding things up.
 * </p>
 * <p>
 * There are two ways to use the metamap analysis engine. If default values are
 * OK, then you can just use the  to
 * get an instance of the analysis engine:
 * 
 * <pre>
 * AnalysisEngineDescription metamap = ComponentFactory.createMetaMapAnnotator();
 *  - or -
 * AnalysisEngineDescription metamap = ComponentFactory.createMetaMapAnnotator(server, port);
 * </pre>
 * 
 * Alternatively, you can create an AnalysisEngineDescription like so:
 * 
 * <pre>
 * 
 * AnalysisEngineDescription metamap = AnalysisEngineFactory.createPrimitiveDescription(MetaMapAE.class, typeSystem,
 *                                 MetaMapAE.PARAM_METAMAP_SERVER, &quot;localhost&quot;, MetaMapAE.PARAM_METAMAP_PORT, &quot;8066&quot;);
 * </pre>
 * 
 * Note that in the example above, you can leave off the server and port
 * parameter settings because those are the defults. <br />
 * <br />
 * Either way, next you use the AnalsyisEngineDescription in a AggregateBuilder
 * object like so:
 * 
 * <pre>
 * AggregateBuilder builder = new AggregateBuilder();
 * builder.add(metmap);
 * builder.add(someCasConsumerOrWriter);
 * SimplePipeline.runPipeline(someCorpusReader, builder.createAggregate());
 * </pre>
 */
public class MetaMapServerAE extends CasAnnotator_ImplBase {
	/**
	 * The DNS name or IP address of the server running mmserver1x. The default
	 * value is "localhost"
	 */
	public static final String PARAM_METAMAP_SERVER = ConfigurationParameterFactory.createConfigurationParameterName(
	                                MetaMapServerAE.class, "metaMapServer");
	@ConfigurationParameter(description = "The DNS name or IP address of the server running the mmserver1x process", defaultValue = "localhost")
	private String metaMapServer = "localhost";

	/**
	 * The port on which mmserver1x is communicating. The default value is 8066.
	 */
	public static final String PARAM_METAMAP_PORT = ConfigurationParameterFactory.createConfigurationParameterName(
	                                MetaMapServerAE.class, "metaMapPort");
	@ConfigurationParameter(description = "The port on which the mmserver1x process is running", defaultValue = "8066")
	private String metaMapPort = "8066";

	/**
	 * The configuration string for MetaMap. The default value is "-y -r 900".
	 */
	public static final String PARAM_METAMAP_CONFIG = ConfigurationParameterFactory.createConfigurationParameterName(
	                                MetaMapServerAE.class, "metaMapConfig");
	@ConfigurationParameter(description = "The MetaMap configuration string", defaultValue = "-a -I --negex -y -r 900")
	private String metaMapConfig = "-a -I --negex -y -r 900";

	/**
	 * The path to the semantic type dictionary resource. Currently, this is a
	 * dictionary file that allows rapid lookups of long forms of semantic type
	 * abbreviations.
	 */
	public static final String PARAM_SEMTYPE_FILE = ConfigurationParameterFactory.createConfigurationParameterName(
	                                MetaMapServerAE.class, "semanticTypeDictionaryFile");
	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/dictionaries/SemanticTypeDictionary.csv")
	private String semanticTypeDictionaryFile;

	/**
	 * The annotation type to loop through. This should be Sentence.class
	 */
	public static final String PARAM_CONTAINER_CLASS = ConfigurationParameterFactory.createConfigurationParameterName(
	                                MetaMapServerAE.class, "containerClassName");
	@ConfigurationParameter(mandatory = true, defaultValue = "edu.umn.biomedicus.type.Sentence")
	private String containerClassName;
	private Class containerClass;

	private SemanticTypeDictionary semanticTypeDictionary = null;
//	MetaMapApi api;
	Logger logger;
	Sentence sent;

	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		// get class for container type
		try {
			containerClass = java.lang.Class.forName(containerClassName);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
			throw new ResourceInitializationException();
		}

		// get logger
		this.logger = context.getLogger();

		// get a connection to mmserver1x
		try {
//			api = new MetaMapApiImpl(metaMapServer, new Integer(metaMapPort));
//			api.setOptions(metaMapConfig);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// get semantic type lookup table so that expanded names of semantic
		// types will be included in the annotation
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(semanticTypeDictionaryFile);
		try {
			semanticTypeDictionary = new SemanticTypeDictionary(inputStream);
		} catch (IOException e) {
			System.err.println("Unable to open the SemanticTypeDictionary.csv file! Check the path and try again.");
			e.printStackTrace();
			throw new ResourceInitializationException();
		}

		// set options
		// if (mmOptions.size() > 0) {
		// api.setOptions(metaMapConfig);
		// }
		// logger.logrb(Level.INFO, "MetaMapAE", "initialize",
		// "edu.umn.biomedicus.metamap.MetaMapAE",
		// "metamap_concept_annotator_info_initialized");
	}

	/*
	 * @see
	 * org.apache.uima.analysis_component.JCasAnnotator_ImplBase#process(org
	 * .apache.uima.jcas.JCas)
	 */
	@Override
	public void process(CAS aCAS) throws AnalysisEngineProcessException {
            CAS sysCAS = aCAS.getView("System"); // always operate on System view
            String text = sysCAS.getDocumentText();
            int size = text.length();
            // Boolean[][] negation = new Boolean[100][100];
            HashMap<Integer, Integer> negation = new HashMap<Integer, Integer>();
            // for (Sentence sentence : JCasUtil.select(jcas, Sentence.class)) {


            /*
            for (Object container : JCasUtil.select(sysCAS, containerClass)) {
                    Annotation containerAnnotation = (Annotation) container;
                    String doc = containerAnnotation.getCoveredText();

                    if (containerClassName.endsWith("Line")) {
                            if (doc.indexOf('\n') > -1 && doc.indexOf('\n') != doc.length() - 1) {
                                System.err.println(" Line annotator failed on line: " + doc);
                            }
                    }
                // weird bug in mmserver11 that causes a crash if it starts with a
                // period followed by a newline character or a newline character
                int offset = 0;
                while (doc.charAt(offset) == '.' || doc.charAt(offset) == '\n')
                    offset++;

                List<Result> resultList = null;
                try
                {
                    resultList = api.processCitationsFromString(doc.substring(offset));
                } catch (Exception e)
                {
                    System.err.println("Fail while processing doc: " + doc);
                    e.printStackTrace();
                    throw new AnalysisEngineProcessException();
                }
                for (Result result : resultList)
                {
                    try
                    {
                        for (Negation neg : result.getNegationList())
                        {
                            for (Position position : neg.getConceptPositionList())
                            {
                                int begin = position.getX() + containerAnnotation.getBegin() + offset;
                                int end = begin + position.getY();
                                negation.put(begin, end);
                            }
                        }

                        // because metamap gives edu.umn.biomedicus.acronym info also, save that
                        // somewhere. Currently, this simple implementation is ok
                        // for metamap.
                        for (AcronymsAbbrevs mmAbbrev : result.getAcronymsAbbrevs())
                        {
                            for (Token token : JCasUtil.selectCovered(Token.class, containerAnnotation))
                            {
                                if (token.getCoveredText().equals(mmAbbrev.getAcronym()))
                                {
                                    int begin = token.getBegin();
                                    int end = token.getEnd();
                                    String candidate = text.substring(begin, end).toLowerCase();
                                    if (candidate.equals("to") || candidate.equals("not") || candidate.equals("in"))
                                    {
                                        continue;
                                    }
                                    Acronym abbrev = new Acronym(jcas, begin, end);
                                    abbrev.setExpansion(mmAbbrev.getExpansion());
                                    abbrev.setSource("MetaMap");
                                    abbrev.addToIndexes();
                                }
                            }
                        }

                        for (Utterance utterance : result.getUtteranceList())
                        {
                            UmlsConcept previousConcept = null;
                            for (PCM pcm : utterance.getPCMList())
                            {
                                UmlsConcept concept = new UmlsConcept(jcas);
                                HashMap<String, Ev> cuiSet = new HashMap<String, Ev>();
                                HashSet<String> contextSet = new HashSet<String>();
                                ArrayList<Cui> cuiAccumulator = new ArrayList<Cui>();

                                for (Ev ev : pcm.getCandidates())
                                {  //.getCandidateList()) {    //.getCandidates()) {
                                    // add a test for more than one positional info.
                                    // It is assumed that there is just one, but
                                    // must check for this.
                                    List<Position> positions = ev.getPositionalInfo();
                                    assert (positions.size() == 1);
                                    for (Position position : positions)
                                    {
                                        int x = position.getX();
                                        int y = position.getY();
                                        int begin = containerAnnotation.getBegin() + x;
                                        int end = begin + y;

                                        concept = new UmlsConcept(jcas, begin, end);
                                        if (negation.containsKey(x) && negation.get(x) == (x + y))
                                        {
                                            contextSet.add("negated");
                                        }
                                        String cuiId = ev.getConceptId();
                                        int score = ev.getScore();

                                        if (cuiId != null)
                                        {
                                            Cui cui = new Cui(jcas, begin, end);
                                            cui.setCuiId(cuiId);
                                            cui.setScore(Math.abs(ev.getScore()));
                                            cui.setConceptName(ev.getConceptName());
                                            List<String> semTypesList = ev.getSemanticTypes();
                                            StringArray semTypes = new StringArray(jcas, semTypesList.size());
                                            for (int j = 0; j < semTypesList.size(); j++)
                                            {
                                                String semTypeAbbrev = semTypesList.get(j);
                                                semTypes.set(j, semanticTypeDictionary.getLongForm(semTypeAbbrev));
                                            }
                                            cui.setSemanticType(semTypes);
                                            cuiAccumulator.add(cui);
                                        }
                                    }
                                }

                                // Converted accumulated CUIs into FSArray type
                                FSArray cuis = new FSArray(jcas, cuiAccumulator.size());
                                for (int i = 0; i < cuiAccumulator.size(); i++)
                                {
                                    cuis.set(i, cuiAccumulator.get(i));
                                }
                                concept.setCuis(cuis);
                                concept.setAnnotSrc("MetaMap Annotator");

                                if (contextSet.size() > 0)
                                {
                                    Iterator<String> contextIter = contextSet.iterator();
                                    StringArray context = new StringArray(jcas, contextSet.size());
                                    for (int j = 0; j < contextSet.size(); j++)
                                    {
                                        String myContext = contextIter.next();
                                        context.set(j, myContext);
                                    }
                                    concept.setContext(context);
                                } else
                                {
                                    StringArray context = new StringArray(jcas, 0);
                                    concept.setContext(context);
                                }
                                if (concept.getCoveredText().trim().length() > 0)
                                {
                                    concept.addToIndexes(jcas);
                                }
                                previousConcept = concept;
                            }
                        }
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }


              */
        }
}
