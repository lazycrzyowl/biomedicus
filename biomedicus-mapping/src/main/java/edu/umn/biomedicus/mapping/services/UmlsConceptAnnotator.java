/* 
 Copyright 2011-13 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.mapping.services;

import edu.umn.biomedicus.dictionary.SemanticTypeDictionary;
import edu.umn.biomedicus.mapping.CandidateTerm;
import edu.umn.biomedicus.type.Token;
import org.apache.log4j.Logger;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.mapdb.BTreeMap;
import org.mapdb.DB;
import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;
//import pitt.search.semanticvectors.FlagConfig;
//import pitt.search.semanticvectors.LuceneUtils;
//import pitt.search.semanticvectors.VectorSearcher;
//import pitt.search.semanticvectors.VectorStoreRAM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * This class uses a dictionary of normalized terms and semantic vectors to map
 * concepts to the UMLS.
 * 
 * @author Robert Bill
 */
public class UmlsConceptAnnotator extends JCasAnnotator_ImplBase {

	public enum Filter {
		NONE, EXISTS_IN_UMLS, PAIRS_COEXIST_IN_UMLS, ALL_TERMS_COEXIST_IN_UMLS;
	}

	public class ConceptMapping {
		String[] terms;
		double score;

		public ConceptMapping(String[] terms, double score) {
			this.terms = terms;
			this.score = score;
		}
	}

	/* ==== Configuration Parameters ==== */
	public static final String PARAM_FILTER = ConfigurationParameterFactory.createConfigurationParameterName(
	                                UmlsConceptAnnotator.class, "FilterLevel");
	@ConfigurationParameter(description = "The level of exact matches should be applied previous to semantic similarity scores.", defaultValue = "NONE")
	private Filter FilterLevel;

	public static final String PARAM_UMLS_KW_DB = ConfigurationParameterFactory.createConfigurationParameterName(
	                                UmlsConceptAnnotator.class, "umlsKeywordFileName");
	@ConfigurationParameter(description = "The name of the mapDB file that contains the umls keywords used.", defaultValue = "/edu/umn/biomedicus/dictionaries/umlsKeywordDB")
	private String umlsKeywordFileName;

	// The path to semantic type dictionary
	public static final String PARAM_SEMTYPE_DICTIONARY = ConfigurationParameterFactory
	                                .createConfigurationParameterName(UmlsConceptAnnotator.class,
	                                                                "semanticTypeDictionaryFile");
	@ConfigurationParameter(mandatory = true, defaultValue = "/edu/umn/biomedicus/dictionaries/SemanticTypeDictionary.csv")
	private String semanticTypeDictionaryFile;

	// The type to loop through. This should be Sentence.class or Line.class
	public static final String PARAM_CONTAINER_CLASS = ConfigurationParameterFactory.createConfigurationParameterName(
	                                UmlsConceptAnnotator.class, "containerClassName");
	@ConfigurationParameter(mandatory = true, defaultValue = "edu.umn.biomedicus.type.Sentence")
	private String containerClassName;

	// The threshold above which candidate cuis are marked as viable
	public static final String PARAM_THRESHOLD = ConfigurationParameterFactory.createConfigurationParameterName(
	                                UmlsConceptAnnotator.class, "threshold");
	@ConfigurationParameter(mandatory = true, defaultValue = "0.00")
	private float threshold;

	// instance fields
	private Class containerClass;
	private SemanticTypeDictionary semanticTypeDictionary;
	private DB umlsKeywordDbContainer;
	private BTreeMap<String, String> umlsKeywordDb;
	// private CloseableVectorStore queryVecReader;
	// private CloseableVectorStore resultsVecReader;
//	private VectorStoreRAM queryVecReader;
//	private VectorStoreRAM resultsVecReader;
//	private LuceneUtils luceneUtils;
//	private VectorSearcher vecSearcher;
//	private FlagConfig config;
	private UmlsTermFilter termVectorFilter;
	private Logger logger;

	/**
	 * Initialize vector search classes.
	 */
	public void initialize(UimaContext context) throws ResourceInitializationException {
//		super.initialize(context);
//		this.termVectorFilter = new UmlsTermFilter();
//
//		// get class for container type
//		try {
//			this.containerClass = java.lang.Class.forName(containerClassName);
//		} catch (ClassNotFoundException e1) {
//			e1.printStackTrace();
//			throw new ResourceInitializationException();
//		}
//
//		// get sui keyword lookup
//		File umlsKeywordFile = null;
//		try {
//			umlsKeywordFile = new File(this.getClass().getResource(umlsKeywordFileName).toURI().getRawPath());
//		} catch (URISyntaxException e) {
//			System.err.println("Failed to open normDB cache file because of invalid URI syntax: "
//			                                + this.getClass().getResource(umlsKeywordFileName));
//			throw new ResourceInitializationException();
//
//		}
//		// this.umlsKeywordDbContainer =
//		// DBMaker.newFileDB(umlsKeywordFile).readOnly().journalDisable()
//		// .asyncFlushDelay(100).closeOnJvmShutdown().make();
//		// this.umlsKeywordDb =
//		// umlsKeywordDbContainer.getTreeMap("umlsKeywords");
//
//		// get semantic type lookup table so that expanded names of semantic
//		// types will be included in the annotation
//		InputStream inputStream = this.getClass().getResourceAsStream(semanticTypeDictionaryFile);
//		try {
//			semanticTypeDictionary = new SemanticTypeDictionary(inputStream);
//		} catch (IOException e) {
//			System.err.println("Unable to open the SemanticTypeDictionary.csv file! Check the path and try again.");
//			e.printStackTrace();
//			throw new ResourceInitializationException();
//		}
//
//		/* Initialize vector reader files */
//		// Semantic vector variables
//		FlagConfig config = FlagConfig.getFlagConfig(new String[] {
//		                                "-queryvectorfile",
//		                                "/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/termvectors.bin",
//		                                "-searchvectorfile",
//		                                "/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/docvectors.bin",
//		                                "-luceneindexpath",
//		                                "/Users/robertbill/git/biomedicus/biomedicus/resources/luceneIndex" });
//
//		LuceneUtils luceneUtils = null;
//		String separator = config.batchcompareseparator();
//		VectorStore vecReader;
//		// VectorStoreRAM queryVecReader;
//		// VectorStoreRAM resultVecReader;
//
//		queryVecReader = new VectorStoreRAM(config);
//		resultsVecReader = new VectorStoreRAM(config);
//		try {
//			// ramReader.initFromFile(config.queryvectorfile());
//			queryVecReader.initFromFile(config.queryvectorfile());
//			resultsVecReader.initFromFile(config.searchvectorfile());
//			this.luceneUtils = new LuceneUtils(config);
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//
//		if (config.luceneindexpath() != null) {
//			try {
//				luceneUtils = new LuceneUtils(config);
//			} catch (IOException e) {
//				logger.info("Couldn't open Lucene index at " + config.luceneindexpath());
//			}
//		}
//		if (luceneUtils == null) {
//			logger.info("No Lucene index for query term weighting, " + "so all query terms will have same weight.");
//		}
//
//		// try {
//		// this.queryVecReader = VectorStoreReader
//		// .openVectorStore("/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/termvectors.bin",
//		// config);
//		// this.resultsVecReader = VectorStoreReader
//		// .openVectorStore("/Users/robertbill/git/biomedicus/biomedicus/resources/vectors/umlsNonExpandedRandomIndex/docvectors.bin",
//		// config);
//		// this.luceneUtils = new LuceneUtils(config);
//		// } catch (IOException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// }
//		this.config = config;

	}

	private HashSet<String> getCuiSet(String normTerm) {
		String cuis = umlsKeywordDb.get(normTerm);
		if (cuis == null) {
			return null;
		} else {
			return new HashSet<String>(Arrays.asList(cuis.split("\\|")));
		}
	}

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
//		long start = System.nanoTime();
//		String text = jcas.getDocumentText();
//		int size = text.length();
//
//		// loop through the chosen container class, either sentence or line
//		// TODO: add "window" and "phrase" as container classes
//		for (Sentence sent : JCasUtil.select(jcas, Sentence.class)) {
//			// Annotation containerAnnotation = (Annotation) sent;
//
//			// Loop through all tokens in the containing annotation and choose
//			// which tokens should be considered "candidates" for being UMLS
//			// concepts.
//			// Collect these candidates in the "candidateTerms" ArrayList
//			// created below.
//			ArrayList<CandidateTerm> candidateTerms = new ArrayList<CandidateTerm>();
//			for (Token tok : JCasUtil.selectCovered(Token.class, sent)) {
//
//				// Always filter the following tokens because they do not
//				// contribute to concept recognition:
//				// * Skip stop words
//				// * Skip symbols
//				// * Skip single character length tokens
//				if (tok.getIsStopword() || isSymbol(tok) || isSingleChar(tok)) {
//					continue;
//				}
//
//				// If filtering is used, apply first level of filtering
//				// here. The first level of filtering is that the normalized
//				// token must appear in the UMLS, otherwise it is skipped.
//				// an empty Cui set means the token does not appear in any UMLS
//				// SUI.
//				HashSet<String> cuis = getCuiSet(tok.getNorm());
//				if (this.FilterLevel != UmlsConceptAnnotator.Filter.NONE) {
//					if (cuis != null && cuis.size() > 0) {
//						candidateTerms.add(new CandidateTerm(tok, cuis));
//					}
//				} else {
//					// If arriving here, then there is no filtering.
//					// Always add candidate term
//					candidateTerms.add(new CandidateTerm(tok, cuis));
//				}
//			}
//			String resultSet = this.reduce(candidateTerms);
//			sent.setParse(resultSet);
//		}
//		System.out.println("Total time: " + ((System.nanoTime() - start) / 1000000000.0));
	}

	protected String reduce(ArrayList<CandidateTerm> candidates) {
//		/*
//		 * The loop in the "process" method identifies the candidates for umls
//		 * Concepts. The next step is to look at the combinations of those
//		 * tokens to find the set permutations that have a semantic similarity
//		 * score sufficiently high to trigger the addition of a UMLS concept
//		 * annotation in the CAS. The current design plan is to keep the sets
//		 * with the largest size that qualify for annotations. For example, the
//		 * set "coronary artery disease" triggers an annotation that excludes
//		 * any combinatorial subsets of those 3 terms. Create an initial
//		 * vector/set.
//		 *
//		 * combinatoric subset pattern steps step 1- start with all the
//		 * combinations for a vector size that is half the size of all terms
//		 * step 2- test for a cui relatedness score that is greater than the
//		 * threshold step 3- if relatedness from step 2 is above the threshold:
//		 * substep 1- add terms until dropping below the threshold step 4- if
//		 * relatedness from step 2 is below the threshold: substep 1- halve the
//		 * number of terms and go to step 2 step 5- if parameters are set as
//		 * "without replacement" then remove all terms that are in an identified
//		 * cui from the candidate list.
//		 *
//		 * TODO: test a weighting/costing scheme where the lexical distance
//		 * between the terms weights the semantic similarity scores.
//		 */
//
//		StringBuilder cuis = new StringBuilder();
//		// int testSize = candidates.size() / 2; // begin with half the space
//		// while (candidates.size() > 0) {
//		// if (testSize == 0) {
//		// testSize = candidates.size();
//		// }
//		// List<CandidateTerm> sample = candidates.subList(0, testSize);
//		// String[] aTerms = new String[sample.size()];
//		// for (int j = 0; j < sample.size(); j++) {
//		// aTerms[j] = sample.get(j).getCoveredText();
//		// }
//		//
//		// try {
//		// System.out.println("SAMPLE SIZE: " + sample.size());
//		// vecSearcher = new
//		// VectorSearcher.VectorSearcherCosine(this.queryVecReader,
//		// this.resultsVecReader,
//		// this.luceneUtils, this.config, aTerms);
//		// } catch (ZeroVectorException e) {
//		// // TODO Auto-generated catch block
//		// e.printStackTrace();
//		// }
//		//
//		// LinkedList<SearchResult> results =
//		// vecSearcher.getNearestNeighbors(3);
//		// if (results.get(0).getScore() < this.threshold) {
//		// testSize--;
//		// continue;
//		// }
//		// double max = 0.0;
//		// // debug print hypothesis
//		// System.out.print("Hypothesis vector: ");
//		// for (String s : aTerms) {
//		// System.out.print(String.format("%s, ", s));
//		// }
//		// System.out.println();
//		// for (SearchResult result : results) {
//		// System.out.println(String.format("%f:%s", result.getScore(),
//		// result.getObjectVector().getObject()
//		// .toString()));
//		// if (result.getScore() > max) {
//		// max = result.getScore();
//		// }
//		// }
//		//
//		// }
//		// return cuis;
//		// Create an instance of the subset generator
//		// Create an initial vector/set
//		CandidateTerm[] candidateArray = new CandidateTerm[candidates.size()];
//
//		ICombinatoricsVector<CandidateTerm> initialSet = Factory.createVector(candidates.toArray(candidateArray));
//		Generator<CandidateTerm> gen = Factory.createSubSetGenerator(initialSet);
//
//		HashSet<ConceptMapping> resultMap = new HashSet<ConceptMapping>();
//		// Print the subsets
//		for (ICombinatoricsVector<CandidateTerm> subSet : gen) {
//			List<CandidateTerm> list = subSet.getVector();
//
//			String[] aTerms = new String[list.size()];
//			for (int i = 0; i < list.size(); i++) {
//				aTerms[i] = list.get(i).getCoveredText();
//			}
//
//			// search semantic vectors for each permutation of terms
//			try {
//				vecSearcher = new VectorSearcher.VectorSearcherCosine(this.queryVecReader, this.resultsVecReader,
//				                                this.luceneUtils, this.config, aTerms);
//			} catch (ZeroVectorException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			LinkedList<SearchResult> results = vecSearcher.getNearestNeighbors(100);
//
//			for (SearchResult result : results) {
//				if (result.getScore() > this.threshold) {
//					resultMap.add(new ConceptMapping(aTerms, result.getScore()));
//					for (String term : aTerms) {
//						// System.out.print(String.format("%s, ", term));
//					}
//					String cui = new File((String) result.getObjectVector().getObject()).getName();
//					// System.out.println(String.format("%f:%s",
//					// result.getScore(), cui));
//					cuis.append(cui);
//					cuis.append(":");
//					cuis.append(result.getScore());
//					cuis.append("\n");
//					// result.getObjectVector().getObject()
//					// .toString()));
//				} else {
//					break;
//				}
//			}
//		}
//		return cuis.toString();
            return "";
	}


	@Override
	public void destroy() {
		// this.queryVecReader.close();
		// this.resultsVecReader.close();
		this.umlsKeywordDb.close();
		this.umlsKeywordDbContainer.close();
	}

	public boolean isSymbol(Token t) {
		int size = t.getEnd() - t.getBegin();

		// If the token is longer than one character, then it is not a symbol
		if (size > 1 || size < 1) {
			return false;
		}

		// Treat an empty normalized form as equivalent to a symbol because it
		// is usually a unicode symbol (such as "�") that gets normalized to an
		// empty string.
		if (t.getNorm().length() == 0) {
			return true;
		}

		// Get char from token
		char c = t.getNorm().charAt(0);
		return c == ',' || c == '.' || c == '!' || c == '?' || c == ':' || c == ';' || c == '#' || c == '%';
	}

	public boolean isSingleChar(Token t) {
		return (t.getNorm().length() == 1);
	}
}