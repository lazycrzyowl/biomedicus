package edu.umn.biomedicus.mapping.metamap;

import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class MetaMapAdapter {

    @NotNull private String mm;
    @NotNull Path plainTextPath;
    @NotNull Path mmAnnotatedPath;
    @NotNull FileOutputStream mmTextSource;
    @NotNull String mmOptions;
    @NotNull boolean mulligan = false;

    public MetaMapAdapter(String mmOptions) throws MetaMapConnectionException {
        this.mmOptions = mmOptions;
        mm = System.getProperty("metamap.path");
        if (mm == null)
        {
            throw new MetaMapConnectionException("Please set the system property metamap.path and try again.");
        }

        try {
            // create temp files for metamap to use
            plainTextPath = Files.createTempFile("metamap-temp", ".mminput.tmp");
            mmAnnotatedPath = Files.createTempFile("metamap-temp", ".mmoutput.tmp");
        } catch (IOException e) {
            throw new MetaMapConnectionException("Failed to open temp files required by metamap.");
        }
    }

    public MetaMapAdapter(Path mmPath, String mmOptions) throws IOException
    {
        if (mmPath == null && System.getProperty("metamap.path") == null)
            throw new IOException("The metamap.path variable and system property are null.");
    }

    public synchronized String getAnalysisString(String s) throws MetaMapProcessException
    {
        // write source data to temp file
        mmTextSource = writeSourceToTempFile(plainTextPath, s);

        // execute metamap on temp files and wait for it to finish
        ProcessBuilder pb = new ProcessBuilder(mm,
                mmOptions,
                plainTextPath.toString(),
                mmAnnotatedPath.toString());

        Process process = runMM(pb);

        // get results from outputfile
        String results = getResults(process);

        if (results == null)
        {
            String msg = String.format("Empty results returned from analyzing sentence: `%s`", s);
            // Don't throw exception yet. Investigate why the out of memory is happening.
            // throw new MetaMapProcessException(msg);
        }

        // clean up temp files
        plainTextPath.toFile().delete();
        mmAnnotatedPath.toFile().delete();

        return results;
    }

    private FileOutputStream writeSourceToTempFile(Path plainTextPath, String s)
            throws MetaMapProcessException
    {
        try
        {
            mmTextSource = new FileOutputStream(plainTextPath.toFile());
            mmTextSource.write(s.getBytes());

            // metamap needs at least one new line character at the end
            mmTextSource.write("\n\n".getBytes());
            mmTextSource.flush();

        } catch (IOException e)
        {
            throw new MetaMapProcessException(e);
        }
        return mmTextSource;
    }

    private Process runMM(ProcessBuilder pb) throws MetaMapProcessException
    {
        Process process = null;
        try {
            process = pb.start();
            process.waitFor();
        } catch (IOException | InterruptedException e)
        {
            throw new MetaMapProcessException(e);
        }
        int exitValue = process.exitValue();
        if (exitValue == 1)
        {
            // if empty results, raise error
            BufferedReader err =
                    new BufferedReader(new InputStreamReader(process.getErrorStream()));
            try {
                String errString = IOUtils.toString(err);
                System.err.println("Metamap processing error: " + errString);
                //throw new MetaMapProcessException(errString);  Usually an out of memory error

            } catch (IOException e) {
                throw new MetaMapProcessException(e);
            }
        }

        return process;
    }

    private String getResults(Process process) throws MetaMapProcessException
    {
        String results = null;
        try {
            results = new String(Files.readAllBytes(mmAnnotatedPath), "UTF-8");

            // if empty results, raise error
            if (results == "") {
                BufferedReader err =
                        new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String errString = IOUtils.toString(err);
                throw new MetaMapProcessException(errString);
            }
        } catch (IOException e)
        {
            throw new MetaMapProcessException(e);
        }
        // Removing logging information previous to the xml start
        int start = results.indexOf("<?xml");
        if (start == -1)
        {
            return null;
        }
        return results.substring(start);
    }

    public void close()
    {
    	try 
    	{
    		mmTextSource.close();
    	} catch (IOException e ){}
    }
}
