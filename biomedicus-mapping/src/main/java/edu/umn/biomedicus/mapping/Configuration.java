package edu.umn.biomedicus.mapping;


import org.apache.commons.configuration.*;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

public class Configuration
{

    @NotNull
    private CompositeConfiguration config;

    public static void main(String[] args) throws Exception
    {
        Configuration c = new Configuration();
    }

    public Configuration() throws ConfigurationException
    {
        config = new CompositeConfiguration();
        config.addConfiguration(new SystemConfiguration());
        config.addConfiguration(new EnvironmentConfiguration());

        // Get biomedicus.property file data and set in System.properties
        URL bioPropURL = Configuration.class.getResource("/edu/umn/biomedicus/mapping/mapping.properties");
        PropertiesConfiguration bp = new PropertiesConfiguration(bioPropURL);
        config.addConfiguration(bp);

        // Some properties in the biomedicus.property file need to be in system properties. Add these here
        String systemProps = config.getString("pipeline.properties");
        setSystemProps(systemProps);

        try
        {
            //Process p = Runtime.getRuntime().exec("cp /Users/bill0154/temp/backup/97_*.txt /Users/bill0154/temp/fh-output/");
        } catch (Exception e)
        {
            System.out.println(e);
        }

    }

    public void setSystemProps(String propKeysString)
    {
        if (propKeysString == null)
            return;

        String[] propKeys = propKeysString.split("\\s|\\s");

        for (String key : propKeys)
        {
            if (key.equals("|")) continue;
            String value = config.getString(key);
            System.setProperty(key, value);
        }
    }

    public void setProperty(String key, String value)
    {
        config.setProperty(key, value);
    }

    public String getString(String key) {
        return config.getString(key);
    }
}