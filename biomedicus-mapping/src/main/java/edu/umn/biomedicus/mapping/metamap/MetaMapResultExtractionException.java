package edu.umn.biomedicus.mapping.metamap;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;

public class MetaMapResultExtractionException extends AnalysisEngineProcessException {
    private static final long serialVersionUID = -2511678776941351837L;

    public MetaMapResultExtractionException() {
        super();
    }

    /**
     * Creates a new exception with the specified cause and a null message.
     *
     * @param aCause
     *          the original exception that caused this exception to be thrown, if any
     */
    public MetaMapResultExtractionException(Throwable aCause) {
        super(aCause);
    }

    /**
     * Creates a new exception with the specified message and cause.
     *
     * @param aMessage
     *          the base name of the resource bundle in which the message for this exception is
     *          located.
     */
    public MetaMapResultExtractionException(String aMessage) {
        super(new Exception(aMessage));
    }
}
