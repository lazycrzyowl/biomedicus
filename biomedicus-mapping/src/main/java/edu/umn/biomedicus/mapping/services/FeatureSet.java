package edu.umn.biomedicus.mapping.services;

import org.jetbrains.annotations.NotNull;

public abstract class FeatureSet {
    @NotNull
    String featureSet;

    @NotNull
    public String getFeatureSet()
    {
        return featureSet;
    }

    public void setFeatureSet(@NotNull String featureSet)
    {
        this.featureSet = featureSet;
    }
}
