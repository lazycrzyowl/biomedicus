package edu.umn.biomedicus.mapping.services;

import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

public class FamilyHistorySentenceFilteringServiceProvider
        extends FilteringServiceProvider
        implements SharedResourceObject {


    public boolean filter(String annotationClass)
    {
        // currently used to filter sentences so that only FH sentences are used.
        if (annotationClass.toLowerCase().equals("familyhistory"))
            return true;
        return false;
    }


    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {


    }
    // END
}