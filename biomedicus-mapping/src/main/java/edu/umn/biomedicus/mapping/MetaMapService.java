package edu.umn.biomedicus.mapping;

import edu.umn.biomedicus.mapping.metamap.MetaMapProcessException;
import edu.umn.biomedicus.dictionary.SemanticTypeDictionary;
import edu.umn.biomedicus.type.*;
import gov.nih.nlm.nls.metamap.*;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.jetbrains.annotations.NotNull;
import se.sics.prologbeans.PBTerm;

import java.util.*;

public class MetaMapService {


    // Location of default semantic types dictionary file
    private static final String defaultSemanticTypeDictionaryFile =
            "edu/umn/biomedicus/dictionary/SemanticTypeDictionary.csv";

    // Define the services: metamapapi, and the semantic type lookup
    @NotNull private SemanticTypeDictionary st;
    @NotNull private MMServerConnection mmCon;

    // Create containers for mm results
    @NotNull private HashMap<Integer, Integer> negation = new HashMap<>();
    //@NotNull private HashMap<>

    public MetaMapService(MMServerConnection mmCon, SemanticTypeDictionary st)
    {
        this.mmCon = mmCon;
        this.st = st;
    }

    public void process(@NotNull String text) throws Exception, MetaMapProcessException {
        int size = text.length();
        MetaMapInstance mm = mmCon.checkout();

        // weird bug in mmserver11 that causes a crash if it starts with a
        // period followed by a newline character or a newline character
        int offset = 0;
        while (text.charAt(offset) == '.' || text.charAt(offset) == '\n')
            offset++;

        List<Result> resultList = mm.getApi().processCitationsFromString(text.substring(offset));

        boolean newResult;
        int lastUtteranceEnd = 0;

        for (Result result : resultList) {
            newResult = true;
            try {
                for (Negation neg : result.getNegationList()) {
                    for (Position position : neg.getConceptPositionList()) {

                        String trigger = neg.getTrigger();
                        List<Position> tPosList = neg.getTriggerPositionList();
                        if (tPosList.size() == 1) {
                            Position p = tPosList.get(0);
                            int begin = p.getX() + offset;
                            int end = begin + p.getY();
                        }
                        String type = neg.getType();
                        List<ConceptPair> conceptPairList = neg.getConceptPairList();

                        //int begin = position.getX() + sent.getBegin() + offset;
                        //int end = begin + position.getY();
                        int begin = position.getX() + offset;
                        int end = begin + position.getY();
                        negation.put(begin, end);
                    }
                }

                // because metamap gives edu.umn.biomedicus.acronym info also, save that
                // somewhere. Currently, this simple implementation is ok
                // for metamap.
                for (AcronymsAbbrevs mmAbbrev : result.getAcronymsAbbrevs()) {
                    mmAbbrev.getAcronym();
//                    {
//                            int begin = token.getBegin();
//                            int end = token.getEnd();
//                            String candidate = text.substring(begin, end).toLowerCase();
//                            if (candidate.equals("to") || candidate.equals("not") || candidate.equals("in")) {
//                                continue;
//                            }
//                            Acronym abbrev = new Acronym(sysJCas, begin, end);
//                            abbrev.setExpansion(mmAbbrev.getExpansion());
//                            abbrev.addToIndexes();
//                        }
                }
//                }

                PBTerm mmopb = result.getMMOPBlist();
                for (Utterance utterance : result.getUtteranceList()) {
                    if (newResult) {
                        String currentString = utterance.getString();
                        int end = currentString.indexOf(' ');
                        if (end == -1)
                            end = currentString.length() - 1;
                        String testString = currentString.substring(0, end);
                        offset = text.indexOf(testString, lastUtteranceEnd);
                        newResult = false;
                    }
                    UmlsConcept previousConcept = null;
                    for (PCM pcm : utterance.getPCMList()) {
//                        UmlsConcept concept = new UmlsConcept(sysJCas);
                        HashMap<String, Ev> cuiSet = new HashMap<String, Ev>();
                        HashSet<String> contextSet = new HashSet<String>();
                        ArrayList<Cui> cuiAccumulator = new ArrayList<Cui>();

                        for (Mapping mapping : pcm.getMappingList()) { //.getCandidateList()) {
                            // add a test for more than one positional info.
                            // It is assumed that there is just one, but
                            // must check for this.
                            int mappingScore = Math.abs(mapping.getScore());
                            for (Ev ev : mapping.getEvList()) {

                                List<Position> positions = ev.getPositionalInfo();
                                assert (positions.size() == 1);
                                for (Position position : positions) {
                                    int x = position.getX();
                                    int y = position.getY();
                                    int begin = x + offset;
                                    int end = begin + y;
                                    lastUtteranceEnd = end;

                                    String debugStr = text.substring(begin, end);
                                    String debugRange = text.substring(Math.max(begin - 12, 0), Math.min(end + 12, size));

//                                    concept = new UmlsConcept(sysJCas, begin, end);
                                    if (negation.containsKey(begin) && negation.get(begin) == (end)) {
                                        contextSet.add("negated");
                                    }
                                    String cuiId = ev.getConceptId();
                                    int score = ev.getScore();

                                    if (cuiId != null) {
//                                        Cui cui = new Cui(sysJCas, begin, end);
//                                        cui.setCuiId(cuiId);
//                                        cui.setScore(Math.abs(ev.getScore()));
//                                        cui.setConceptName(ev.getConceptName());
                                        List<String> semTypesList = ev.getSemanticTypes();
//                                        StringArray semTypes = new StringArray(sysJCas, semTypesList.size());
//                                        for (int j = 0; j < semTypesList.size(); j++) {
//                                            String semTypeAbbrev = semTypesList.get(j);
//                                            semTypes.set(j, semanticTypeDictionary.getLongForm(semTypeAbbrev));
//                                        }
//                                        cui.setSemanticType(semTypes);
//                                        cuiAccumulator.add(cui);
                                    }
                                }
                            }
                        }

                        // Converted accumulated CUIs into FSArray type
//                        FSArray cuis = new FSArray(sysJCas, cuiAccumulator.size());
//                        for (int i = 0; i < cuiAccumulator.size(); i++) {
//                            cuis.set(i, cuiAccumulator.get(i));
//                        }
//                        concept.setCuis(cuis);
//
//                        if (contextSet.size() > 0) {
//                            Iterator<String> contextIter = contextSet.iterator();
//                            StringArray context = new StringArray(sysJCas, contextSet.size());
//                            for (int j = 0; j < contextSet.size(); j++) {
//                                String myContext = contextIter.next();
//                                context.set(j, myContext);
//                            }
//                            concept.setContext(context);
//                        } else {
//                            StringArray context = new StringArray(sysJCas, 0);
//                            concept.setContext(context);
//                        }
//                        if (concept.getCoveredText().trim().length() > 0) {
//                            concept.addToIndexes(sysJCas);
//                        }
//                        previousConcept = concept;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //}
        mmCon.checkin(mm);
    }
}