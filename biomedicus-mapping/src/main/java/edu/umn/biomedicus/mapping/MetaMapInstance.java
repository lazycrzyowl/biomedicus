package edu.umn.biomedicus.mapping;

import gov.nih.nlm.nls.metamap.MetaMapApi;

public class MetaMapInstance {
    private MetaMapApi api;
    private int port;
    private int id;

    public MetaMapInstance(int port, MetaMapApi api) {
        this.port = port;
        this.api = api;
    }

    public int getPort() {
        return port;
    }

    public MetaMapApi getApi() {
        return api;
    }
}