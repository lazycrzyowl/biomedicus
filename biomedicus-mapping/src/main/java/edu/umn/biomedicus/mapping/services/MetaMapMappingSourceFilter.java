package edu.umn.biomedicus.mapping.services;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

public class MetaMapMappingSourceFilter {

    @NotNull
    private HashMap<String, Boolean> approvedSources;

    public MetaMapMappingSourceFilter(String sourceName)
    {
        this.approvedSources = new HashMap<>();
    }

    public MetaMapMappingSourceFilter(List<String> sourcesList)
    {
        this.approvedSources = new HashMap<>();
        for (String sourceName : sourcesList)
        {
            approvedSources.put(sourceName, true);
        }
    }

    public MetaMapMappingSourceFilter(HashMap<String, Boolean> sourcesList)
    {
        this.approvedSources = sourcesList;
    }

    public void removeSource(String sourceName)
    {
        approvedSources.put(sourceName, false);
    }

    public void addSource(String sourceName)
    {
        approvedSources.put(sourceName, true);
    }
}
