package edu.umn.biomedicus.mapping.services;


import edu.umn.biomedicus.mapping.metamap.MetaMapProcessException;
import edu.umn.biomedicus.mapping.metamap.*;
import edu.umn.biomedicus.ontology.SemanticGroupMapping;
import edu.umn.biomedicus.ontology.SemanticType;
import opennlp.tools.util.Span;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MetaMapMappings {

    @NotNull
    List<MetaMapSimplifiedMapping> simpleMappings;
    @NotNull
    List<SyntaxUnit> syntaxUnitMappings;

    public MetaMapMappings(String xmlSegment, String sentenceText) throws MetaMapProcessException {
        JAXBContext jaxbContext = null;
        Unmarshaller unmarshaller = null;
        MMOs mmRootObject = null;

        try {
            InputStream is = IOUtils.toInputStream(xmlSegment, "UTF-8");
            jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
            unmarshaller = jaxbContext.createUnmarshaller();
            mmRootObject = (MMOs)unmarshaller.unmarshal(is);
        } catch (JAXBException|IOException e)
        {
            System.err.println("Sentence Text causing error: " + sentenceText);
            return;
        }

        List<MMO> mmObjList = mmRootObject.getMMO();
        for (MMO mmo : mmObjList)
        {
            int aasCount = Integer.valueOf(mmo.getAAs().getCount());
            int negationCount = Integer.valueOf(mmo.getNegations().getCount());
            int utterancesCount = Integer.valueOf(mmo.getUtterances().getCount());

            if (utterancesCount > 0)
            {
                List<Utterance> utteranceList = mmo.getUtterances().getUtterance();
                getMappings(utteranceList);
            }
        }
    }

    public static void main(String[] args) throws MetaMapProcessException
    {
        InputStream is = MetaMapMappings.class.getResourceAsStream(
                "edu/umn/biomedicus/mapping/MMXmlSampleOutput.xml");
        //MetaMapMappings mmxd = new MetaMapMappings(is);
    }

    public List<MetaMapSimplifiedMapping> getSimplifiedMappings(List<Mapping> mappingList, Phrase phrase)
            throws JAXBException, FileNotFoundException, IOException
    {
        ArrayList<MetaMapSimplifiedMapping> results = new ArrayList<>();
        for (Mapping mapping : mappingList)
        {
            int score = Integer.valueOf(mapping.getMappingScore());
            List<Candidate> candidateList = mapping.getMappingCandidates().getCandidate();

            for (Candidate candidate : candidateList)
            {
                int phraseBegin = Integer.valueOf(phrase.getPhraseStartPos());
                int phraseEnd = Integer.valueOf(phraseBegin + phrase.getPhraseLength());

                MetaMapSimplifiedMapping mappingObj = new MetaMapSimplifiedMapping(
                        phrase.getPhraseText(),
                        phraseBegin,
                        phraseEnd);

                String cui = candidate.getCandidateCUI();
                mappingObj.setCui(cui);

                List<ConceptPI> listPIs = candidate.getConceptPIs().getConceptPI();

                String matchedText = candidate.getCandidateMatched();
                mappingObj.setMatchedText(matchedText);

                String preferredText = candidate.getCandidatePreferred();
                mappingObj.setPreferredText(preferredText);

                int candidateScore = Integer.valueOf(candidate.getCandidateScore());
                mappingObj.setScore(candidateScore);

                Span[] conceptPIs = getConceptPIs(candidate.getConceptPIs());
                mappingObj.setConceptPIs(conceptPIs);

                mappingObj.setHead(Boolean.valueOf(candidate.getIsHead()));

                //getMatchMaps(candidate.getMatchMaps().getMatchMap());
                mappingObj.setSemanticTypes(getSemTypes(candidate.getSemTypes()));

                String[] sources = getSources(candidate.getSources());
                mappingObj.setSources(sources);

                //mappingObj.setStatus + candidate.getStatus());
                mappingObj.setNegated(Boolean.valueOf(candidate.getNegated()));

                results.add(mappingObj);
            }
        }

        return results;
    }

    public List<SyntaxUnit> getSyntaxUnitMappings()
    {
        return syntaxUnitMappings;
    }

    public List<MetaMapSimplifiedMapping> getSimpleMappings()
    {
        return simpleMappings;
    }

    public void getMappings(List<Utterance> utteranceList) throws MetaMapProcessException
    {
        for (Utterance utterance : utteranceList)
        {
            List<Phrase> phraseList = utterance.getPhrases().getPhrase();
            for (Phrase p : phraseList)
            {
                String phraseText = p.getPhraseText();
                List<Mapping> mappingList = p.getMappings().getMapping();
                List<Candidate> candidateList = p.getCandidates().getCandidate();
                syntaxUnitMappings = p.getSyntaxUnits().getSyntaxUnit();
                try {
                    simpleMappings = getSimplifiedMappings(mappingList, p);
                } catch (JAXBException|IOException e)
                {
                    throw new MetaMapProcessException(e);
                }

                utterance.getPMID();
            }
        }
    }

    private Span[] getConceptPIs(ConceptPIs cpis) throws JAXBException
    {
        int conceptPICount = Integer.valueOf((cpis.getCount()));
        ArrayList<Span> results = new ArrayList<>(conceptPICount);
        List<ConceptPI> conceptPIList = cpis.getConceptPI();
        for (ConceptPI cpi : conceptPIList)
        {
            int spanStart = Integer.valueOf(cpi.getStartPos());
            Span span = new Span(spanStart, Integer.valueOf(spanStart + cpi.getLength()));
            results.add(span);
        }
        Span[] resultArray = new Span[results.size()];
        return results.toArray(resultArray);
    }

    private String[] getMatchedWords(List<MatchedWord> matchedWordList)
    {
        ArrayList<String> results = new ArrayList<>();
        for (MatchedWord matchedWord : matchedWordList)
        {
            results.add(matchedWord.getvalue());
        }
        String[] resultArray = new String[results.size()];
        return results.toArray(resultArray);
    }

    private void getMatchMaps(List<MatchMap> matchMapList)
    {
        for (MatchMap matchMap : matchMapList)
        {
            System.out.println("\tText match start: " + matchMap.getTextMatchStart());
            System.out.println("\tText match end: " + matchMap.getTextMatchEnd());
            System.out.println("\tText lexical variation: " + matchMap.getLexVariation());
            System.out.println("\tText concat match: " + matchMap.getConcMatchEnd());
            System.out.println();
        }
    }

    private SemanticType[] getSemTypes(SemTypes st) throws FileNotFoundException, IOException
    {
        ArrayList<SemanticType> results = new ArrayList();
        SemanticGroupMapping sgm = new SemanticGroupMapping();
        int count = Integer.valueOf(st.getCount());

        SemanticType[] buf = new SemanticType[0];
        if (count == 0) return results.toArray(buf);

        List<SemType> semanticTypeList = st.getSemType();
        for (int i = 0; i < count; i++)
        {
            String semTypeAbbrev = semanticTypeList.get(i).getvalue();
            SemanticType sType = sgm.getSemanticTypeForAbbrev(semTypeAbbrev);
            results.add(sType);
        }
        SemanticType[] semTypeArray = new SemanticType[results.size()];
        return results.toArray(semTypeArray);
    }

    private String[] getSources(Sources sources)
    {
        ArrayList<String> results = new ArrayList<>();
        int count = Integer.valueOf(sources.getCount());
        if (count == 0) return new String[0];

        List<Source> sourceList = sources.getSource();

        for (int i = 0; i < count; i++)
        {
            results.add(sourceList.get(i).getvalue());
        }
        String[] resultArray = new String[results.size()];
        return results.toArray(resultArray);
    }
}