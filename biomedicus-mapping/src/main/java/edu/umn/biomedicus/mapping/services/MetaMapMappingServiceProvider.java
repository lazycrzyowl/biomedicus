package edu.umn.biomedicus.mapping.services;


import edu.umn.biomedicus.core.utils.AnnotationUtils;
import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.mapping.metamap.*;
import edu.umn.biomedicus.ontology.SemanticType;
import edu.umn.biomedicus.type.UmlsConcept;
import edu.umn.biomedicus.type.UmlsSemanticType;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class MetaMapMappingServiceProvider
    extends MappingServiceProvider
        implements SharedResourceObject {

    @NotNull String settings;
    @NotNull String[] semanticTypes;
    @NotNull MetaMapAdapter mm;

    //static String defaultSettings = "-r 400 -T -K -A -Y -I -x --XMLn --negex ";
    static String defaultSettings = "-r 200 -T -K -I --negex --XMLn ";
    static String[] defaultSemanticTypes = {"-J aapp", "acab", "acty", "aggp", "anab", "anst", "antb",
            "bacs", "bact", "bdsu", "bdsy", "bhvr", "blor", "bmod", "bodm", "bpoc", "bsoj",
            "carb", "celc", "cell", "cgab", "chem", "chvf", "chvs", "clas", "clna", "clnd", "cnce", "comd",
            "diap", "dora", "drdd", "dsyn", "edac", "eehu", "eico", "emod", "emst", "enty", "enzy", "evnt",
            "famg", "ffas", "fndg", "fngs", "food", "ftcn", "geoa", "gora", "grpa", "grup",
            "hcpp", "hcro", "hlca", "hops", "horm", "humn", "idcn", "imft", "inbe", "inch", "inpo", "inpr", "irda",
            "lang", "lbpr", "lbtr", "lipd", "mbrt", "mcha", "medd", "mnob", "mobd", "neop", "nnon", "npop", "nsba",
            "ocac", "ocdi", "opco", "orch", "orgm", "orgt", "ortf",
            "patf", "phob", "phpr", "phsf", "phsu", "plnt", "podg", "popg", "prog", "pros", "qlco", "qnco",
            "rcpt", "resa", "resd", "rnlw", "sbst", "shro", "socb", "sosy", "spco", "strd", "tisu", "tmco", "topp",
            "virs", "vita"};



//    public void inititialize(String optionsString, String[] semanticTypeCodeList)
//            throws IOException
//    {
//        String metamapSettings = (optionsString == null) ? defaultSettings : optionsString;
//        semanticTypes = (semanticTypeCodeList == null) ? defaultSemanticTypes : semanticTypeCodeList;
//        settings = metamapSettings + StringUtils.join(semanticTypes, ",");
//        MetaMapAdapter mm = new MetaMapAdapter(settings);
//    }

    public synchronized UmlsConcept getMapping(AnnotationFS sentence) throws AnalysisEngineProcessException
    {
        List<String> mappingCollection = new ArrayList<>();
        JCas jcas = CASUtil.getJCas(sentence);
        int offset = sentence.getBegin();
        List<UmlsConceptMap> results = new ArrayList<>();
        mm = new MetaMapAdapter(settings);
        String debugString = sentence.getCoveredText();
        for (String subjectText : StringUtils.splitByWholeSeparatorPreserveAllTokens(
                sentence.getCoveredText(), "\n\n")) {
            String analysis = mm.getAnalysisString(subjectText);
            if (analysis == null) return new UmlsConcept(jcas);
            InputStream analysisInputStream = getAnalysisInputStream(analysis);

            String[] segments = getSegments(analysisInputStream);
            for (String segment : segments) {
                if (segment == "") continue;

                // TODO:  have to split on \n\n, or "<?xml version="1.0" encoding="UTF-8"?>" after metamap is ran.
                // This is because metamap outputfiles for xml have a new xml header for each text section separated by
                // 2 returns (e.g., "\n\n")
                //String analysisResultString = StringUtils.
                MetaMapMappings mappings = new MetaMapMappings(segment, sentence.getCoveredText());
                //
                // TODO:  left off here. fix simple mappings.
                List<MetaMapSimplifiedMapping> mappingResults =
                        mappings.getSimpleMappings();
                mapConcepts(offset, mappingResults, jcas);
                mapSyntaxUnits(mappings, sentence);
            }
        }

        mm.close();
        return new UmlsConcept(jcas);
    }

    private String[] getSegments(InputStream is) throws AnalysisEngineProcessException
    {
        List<String> results = new ArrayList<>();
        String analysisResultString = null;
        try
        {
            analysisResultString = IOUtils.toString(is, "UTF-8");
            if (analysisResultString == null) throw new IOException("Empty results from Metamap");
        } catch (IOException e) {
            throw new AnalysisEngineProcessException(e);
        }

        return StringUtils.splitByWholeSeparatorPreserveAllTokens(
                analysisResultString, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    }

    private InputStream getAnalysisInputStream(String analysis) throws MetaMapProcessException
    {
        InputStream analysisInputStream = null;
        try {
            analysisInputStream = new ByteArrayInputStream(analysis.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e)
        {
            throw new MetaMapProcessException(e);
        }
        return analysisInputStream;
    }

    private void mapSyntaxUnits(MetaMapMappings mappings,
                                AnnotationFS sentence)
    {
        List<SyntaxUnit> syntaxUnitMappings = mappings.getSyntaxUnitMappings();
        String sentenceText = sentence.getCoveredText();
        int offset = sentence.getBegin();

        if (syntaxUnitMappings==null) return;
        for (SyntaxUnit su : syntaxUnitMappings)
        {
            int currentIndex = 0;
            String text = su.getInputMatch();
            int begin = sentenceText.indexOf(text, currentIndex) + offset;
            int end = begin + text.length();
            List<AnnotationFS> tokenList =
                    AnnotationUtils.getTokens(sentence.getCAS(), begin, end);

            //System.out.print("\t" + su.getLexCat());
            //System.out.print("\t" + su.getSyntaxType() + "\t");
            Tokens tokens = su.getTokens();

            for (Token token : tokens.getToken())
            {
            //    System.out.print("\t" + token.getvalue() + "\t");
            }
            //System.out.println();
        }
    }

    private void mapConcepts(int offset, List<MetaMapSimplifiedMapping> mappings, JCas jcas)
            throws MetaMapProcessException
    {
        // TODO: check if mappings is null
        if (mappings == null) return;
        for (MetaMapSimplifiedMapping m : mappings)
        {
            String matchedText = m.getMatchedText();
            String preferredText = m.getPreferredText();
            String cui = m.cui;
            int score = m.getScore();
            int begin = m.getConceptPIs()[0].getStart() + offset;
            int end = begin + matchedText.length();

            UmlsConcept concept = new UmlsConcept(jcas, begin, end);
            concept.setPreferredTerm(preferredText);
            concept.setCui(cui);
            concept.setScore(Math.abs(score));

            SemanticType[] stArray = m.getSemanticTypes();
            CAS cas = jcas.getCas();
            FSArray semTypes = new FSArray(jcas, stArray.length);
            for (int i = 0; i < stArray.length; i++)
            {
                SemanticType st = stArray[i];
                UmlsSemanticType ust = new UmlsSemanticType(jcas);
                ust.setCode(st.code);
                ust.setAbbrev(st.abbrev);
                ust.setName(st.name);
                ust.setGroupCode(st.semanticGroupCode);
                ust.setGroupName(st.semanticGroupName);
                semTypes.set(i, ust);
            }

            concept.setSemanticTypes(semTypes);
            concept.addToIndexes();

//            String[] sources = m.getSources();
//            for (String s : sources)  {
//                System.out.println("source: " + s);
//            }
//
//            System.out.println();
        }
    }

    public UmlsSyntaxMap getSyntaxUnits(String sentenceText) throws Exception
    {

        return new UmlsSyntaxMap();
    }

    @Override
    public void load(DataResource aData) throws ResourceInitializationException
    {
        settings = defaultSettings; // + StringUtils.join(defaultSemanticTypes, ",");
        try {
            MetaMapAdapter mm = new MetaMapAdapter(settings);
        } catch (MetaMapConnectionException e)
        {
            throw new ResourceInitializationException(e);
        }
    }
}
