package edu.umn.biomedicus.mapping.services;

import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;

public abstract class MappingServiceProvider {
    public abstract edu.umn.biomedicus.type.UmlsConcept getMapping(AnnotationFS annotation) throws Exception;
    public abstract UmlsSyntaxMap getSyntaxUnits(String sentenceText) throws Exception;
}
