package edu.umn.biomedicus.mapping;

import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.uimafit.factory.TypeSystemDescriptionFactory;

import java.io.File;
import java.io.IOException;

/**
 * Family History Serial pipeline
 */
public class MappingSerialPipeline {

    private TypeSystemDescription tsd;
    private AnalysisEngine analysisEngine;

    private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<>();
    static {
        TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription(
                "./descriptors/types/TypeSystem.xml");
        TYPE_SYSTEM_DESCRIPTION.set(tsd);
    }

    /**
     * Creates a UIMA analysis engine.
     */
    public MappingSerialPipeline() throws InvalidXMLException, IOException, ResourceInitializationException {
        File descriptorFile = new File("biomedicus-mapping/descriptors/serialApplications/mappingSerialAnalysisEngine.xml");
        XMLInputSource descriptorSource = new XMLInputSource(descriptorFile);

        System.out.println("Creating analysis engine");
        ResourceSpecifier specifier = UIMAFramework.getXMLParser().parseResourceSpecifier(
                descriptorSource);
        analysisEngine = UIMAFramework.produceAnalysisEngine(specifier);
    }

    /**
     * Uses the UIMA analysis engine to process the provided document text.
     */
    public CAS process(String documentPath, String text) throws UIMAException {
        CAS aCAS = analysisEngine.newCAS();
        aCAS.createView("MetaData").setSofaDataString(documentPath, "text/plain");
        analysisEngine.typeSystemInit(aCAS.getTypeSystem());
        aCAS.createView("System").setDocumentText(text);
        analysisEngine.process(aCAS);
        return aCAS;
    }
}