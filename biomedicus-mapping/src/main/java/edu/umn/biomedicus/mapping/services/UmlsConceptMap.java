/* 
 Copyright 2011 University of Minnesota  
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.mapping.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import edu.umn.biomedicus.type.Cui;

public class UmlsConceptMap implements Iterable<String> {
	private HashMap<String, HashSet<Cui>> cuiList;
	private HashMap<String, HashSet<String>> contextList;
	private HashMap<String, Boolean> acronymsAbbrevs;

	public UmlsConceptMap() {
		cuiList = new HashMap<String, HashSet<Cui>>();
		contextList = new HashMap<String, HashSet<String>>();
		acronymsAbbrevs = new HashMap<String, Boolean>();
	}

	private String generateKey(int start, int stop) {
		return start + ":" + stop;
	}

	public void addCui(Cui cui) {
		String key = this.generateKey(cui.getBegin(), cui.getEnd());
		if (!cuiList.containsKey(key)) {
			cuiList.put(key, new HashSet<Cui>());
		}
		cuiList.get(key).add(cui);
	}

	public HashSet<Cui> getCuiList(String key) {
		return cuiList.get(key);
	}

	public HashSet<String> getContextList(String key) {
		return contextList.get(key);
	}

	public void addContext(int begin, int end, String context) {
		String key = this.generateKey(begin, end);
		if (!contextList.containsKey(key)) {
			contextList.put(key, new HashSet<String>());
			contextList.get(key).add(context);
		} else {
			contextList.get(key).add(context);
		}
	}

	public void addAcronym(int begin, int end, String acronym, String expansion) {
		this.acronymsAbbrevs.put(this.generateKey(begin, end), new Boolean(true));
	}

	public void addNegation(int begin, int end) {
		this.addContext(begin, end, "negation");
	}

	@Override
	public Iterator<String> iterator() {
		return cuiList.keySet().iterator();
	}
}
