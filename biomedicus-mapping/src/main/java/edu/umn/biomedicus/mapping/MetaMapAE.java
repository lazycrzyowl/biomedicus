/* 
 Copyright 2014 University of Minnesota
 All rights reserved. 

 Licensed under the Apache License, Version 2.0 (the "License"); 
 you may not use this file except in compliance with the License. 
 You may obtain a copy of the License at 

 http://www.apache.org/licenses/LICENSE-2.0 

 Unless required by applicable law or agreed to in writing, software 
 distributed under the License is distributed on an "AS IS" BASIS, 
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 See the License for the specific language governing permissions and 
 limitations under the License.
 */
package edu.umn.biomedicus.mapping;

import edu.umn.biomedicus.core.utils.CASUtil;
import edu.umn.biomedicus.mapping.metamap.MetaMapProcessException;
import edu.umn.biomedicus.dictionary.SemanticTypeDictionary;
import edu.umn.biomedicus.type.*;
import gov.nih.nlm.nls.metamap.*;
import org.apache.uima.UIMAFramework;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.jetbrains.annotations.NotNull;
import se.sics.prologbeans.PBTerm;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.*;

/**
 * <p>
 * This class connects to a running metamap server (mmserver13) using the
 * metamap java api and places the metamap results into a
 * {@link edu.umn.biomedicus.type.UmlsConcept} annotation object.
 * </p>
 * <p/>
 * Currently, this analysis engine sends the entire document to metamap.
 */
public class MetaMapAE extends CasAnnotator_ImplBase {
    // The DNS name or IP address of the server running mmserver1x. Default is "localhost"
    public static final String PARAM_METAMAP_SERVER = "metaMapServer";

    // The port on which mmserver1x is communicating. The default value is 8066.
    public static final String PARAM_METAMAP_PORT = "metaMapPort";

    // The configuration string for MetaMap. The default value is "-y -r 900".
    public static final String PARAM_METAMAP_CONFIG = "metaMapConfig";

    /**
     * The path to the semantic type dictionary resource. Currently, this is a
     * dictionary file that allows rapid lookups of long forms of semantic type
     * abbreviations.
     */
    public static final String PARAM_SEMTYPE_FILE = "semanticTypeDictionaryFile";
    private static final long timeout = 100000;
    @NotNull
    MetaMapApi api;
    @NotNull
    Logger logger = UIMAFramework.getLogger(MetaMapAE.class);
    @NotNull
    String metaMapServer = "localhost";
    @NotNull
    String metaMapPort = "8066";
    @NotNull
    String metaMapConfig = "-acIxyzT --negex";
    @NotNull
    String semanticTypeDictionaryFile = "edu/umn/biomedicus/dictionary/SemanticTypeDictionary.csv";
    @NotNull
    SemanticTypeDictionary semanticTypeDictionary;
    @NotNull
    MMServerConnection mm;


    public void initialize(UimaContext context) throws ResourceInitializationException {
        super.initialize(context);
        logger.log(Level.FINEST, "Initializing MetaMap Annotator.");

        // Set parameters
        setStringParams(context, PARAM_METAMAP_SERVER, PARAM_METAMAP_PORT,
                PARAM_METAMAP_CONFIG, PARAM_SEMTYPE_FILE);

        try {
            mm = (MMServerConnection) context.getResourceObject("mmResourceController");
        } catch (ResourceAccessException e) {
            throw new ResourceInitializationException(e);
        }

        // Load the semantic type lookup table so that expanded names of semantic
        // types will be included in the annotation
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(semanticTypeDictionaryFile);
        try {
            semanticTypeDictionary = new SemanticTypeDictionary(inputStream);
        } catch (IOException e) {
            System.err.println("Unable to open the SemanticTypeDictionary.csv file! Check the path and try again.");
            e.printStackTrace();
            throw new ResourceInitializationException();
        }
    }

    public MetaMapInstance getMetaMapInstance() throws AnalysisEngineProcessException
    {
        // Get start time to check connection timeouts
        long start = System.currentTimeMillis();

        MetaMapInstance mmi = mm.checkout();
        while (mmi == null)
        {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e)
            {
                throw new MetaMapProcessException("MetaMap Processing was interrupted.");
            }
            mmi = mm.checkout();

            if (System.currentTimeMillis() - start > timeout)
                throw new AnalysisEngineProcessException(new Throwable("Unable to acquire a metamapapi connection."));
        }
        return mmi;
    }

    /*
     * @see
     * org.apache.uima.analysis_component.JCasAnnotator_ImplBase#process(org
     * .apache.uima.jcas.JCas)
     */
    @Override
    public void process(CAS aCAS) throws AnalysisEngineProcessException {
        MetaMapInstance mmi = getMetaMapInstance();

        MetaMapApi api = mmi.getApi();


        CAS sysCAS = CASUtil.getSystemView(aCAS);
        JCas sysJCas;
        try {
            sysJCas = sysCAS.getJCas();
            if (sysJCas == null) throw new CASException(new Throwable("Null jcas found."));
        } catch (CASException e) {
            throw new AnalysisEngineProcessException(e);
        }
        String text = sysCAS.getDocumentText();
        int size = text.length();
        // Boolean[][] negation = new Boolean[100][100];
        HashMap<Integer, Integer> negation = new HashMap<>();
        // for (Sentence sentence : JCasUtil.select(jcas, Sentence.class)) {

        //for (Sentence sent : JCasUtil.select(sysJCas, Sentence.class)) {
        //String doc = sent.getCoveredText();
        String doc = sysCAS.getDocumentText();

        // weird bug in mmserver11 that causes a crash if it starts with a
        // period followed by a newline character or a newline character
        int offset = 0;
        while (doc.charAt(offset) == '.' || doc.charAt(offset) == '\n')
            offset++;

        List<Result> resultList;
        try {
            resultList = api.processCitationsFromString(doc.substring(offset));
        } catch (Exception e) {
            throw new AnalysisEngineProcessException(e);
        }
        boolean newResult;
        int lastUtteranceEnd = 0;

        for (Result result : resultList) {
            newResult = true;
            try {
                for (Negation neg : result.getNegationList()) {
                    for (Position position : neg.getConceptPositionList()) {

                        String trigger = neg.getTrigger();
                        List<Position> tPosList = neg.getTriggerPositionList();
                        if (tPosList.size() == 1) {
                            Position p = tPosList.get(0);
                            int begin = p.getX() + offset;
                            NegationTrigger negTrigger = new NegationTrigger(sysJCas, begin, begin + p.getY());
                            negTrigger.addToIndexes();
                        }
                        String type = neg.getType();
                        List<ConceptPair> conceptPairList = neg.getConceptPairList();

                        //int begin = position.getX() + sent.getBegin() + offset;
                        //int end = begin + position.getY();
                        int begin = position.getX() + offset;
                        int end = begin + position.getY();
                        negation.put(begin, end);
                    }
                }

                // because metamap gives edu.umn.biomedicus.acronym info also, save that
                // somewhere. Currently, this simple implementation is ok
                // for metamap.
                for (AcronymsAbbrevs mmAbbrev : result.getAcronymsAbbrevs()) {
                    //*******mmAbbrev.
                    //mmAbbrev.
                    for (Token token : JCasUtil.select(sysJCas, Token.class)) { //Covered(Token.class, sent)) {
                        if (token.getCoveredText().equals(mmAbbrev.getAcronym())) {
                            int begin = token.getBegin();
                            int end = token.getEnd();
                            String candidate = text.substring(begin, end).toLowerCase();
                            if (candidate.equals("to") || candidate.equals("not") || candidate.equals("in")) {
                                continue;
                            }
                            Acronym abbrev = new Acronym(sysJCas, begin, end);
                            abbrev.setExpansion(mmAbbrev.getExpansion());
                            abbrev.addToIndexes();
                        }
                    }
                }

                PBTerm mmopb = result.getMMOPBlist();
                for (Utterance utterance : result.getUtteranceList()) {
                    if (newResult) {
                        String currentString = utterance.getString();
                        int end = currentString.indexOf(' ');
                        if (end == -1)
                            end = currentString.length() - 1;
                        String testString = currentString.substring(0, end);
                        offset = text.indexOf(testString, lastUtteranceEnd);
                        newResult = false;
                    }
                    UmlsConcept previousConcept = null;
                    for (PCM pcm : utterance.getPCMList()) {
                        UmlsConcept concept = new UmlsConcept(sysJCas);
                        HashMap<String, Ev> cuiSet = new HashMap<String, Ev>();
                        HashSet<String> contextSet = new HashSet<String>();
                        ArrayList<Cui> cuiAccumulator = new ArrayList<Cui>();

                        for (Mapping mapping : pcm.getMappingList()) { //.getCandidateList()) {
                            // add a test for more than one positional info.
                            // It is assumed that there is just one, but
                            // must check for this.
                            int mappingScore = Math.abs(mapping.getScore());
                            for (Ev ev : mapping.getEvList()) {

                                List<Position> positions = ev.getPositionalInfo();
                                assert (positions.size() == 1);
                                for (Position position : positions) {
                                    int x = position.getX();
                                    int y = position.getY();
                                    int begin = x + offset;
                                    int end = begin + y;
                                    lastUtteranceEnd = end;

                                    String debugStr = doc.substring(begin, end);
                                    String debugRange = doc.substring(Math.max(begin - 12, 0), Math.min(end + 12, size));

                                    concept = new UmlsConcept(sysJCas, begin, end);
                                    if (negation.containsKey(begin) && negation.get(begin) == (end)) {
                                        contextSet.add("negated");
                                    }
                                    String cuiId = ev.getConceptId();
                                    int score = ev.getScore();

                                    if (cuiId != null) {
                                        Cui cui = new Cui(sysJCas, begin, end);
                                        cui.setCuiId(cuiId);
                                        cui.setScore(Math.abs(ev.getScore()));
                                        cui.setConceptName(ev.getConceptName());
                                        List<String> semTypesList = ev.getSemanticTypes();
                                        StringArray semTypes = new StringArray(sysJCas, semTypesList.size());
                                        for (int j = 0; j < semTypesList.size(); j++) {
                                            String semTypeAbbrev = semTypesList.get(j);
                                            semTypes.set(j, semanticTypeDictionary.getLongForm(semTypeAbbrev));
                                        }
                                        cui.setSemanticType(semTypes);
                                        cuiAccumulator.add(cui);
                                    }
                                }
                            }
                        }

                        // Converted accumulated CUIs into FSArray type
                        FSArray cuis = new FSArray(sysJCas, cuiAccumulator.size());
                        for (int i = 0; i < cuiAccumulator.size(); i++) {
                            cuis.set(i, cuiAccumulator.get(i));
                        }
                        concept.setCuis(cuis);

                        if (contextSet.size() > 0) {
                            Iterator<String> contextIter = contextSet.iterator();
                            StringArray context = new StringArray(sysJCas, contextSet.size());
                            for (int j = 0; j < contextSet.size(); j++) {
                                String myContext = contextIter.next();
                                context.set(j, myContext);
                            }
                            concept.setContext(context);
                        } else {
                            StringArray context = new StringArray(sysJCas, 0);
                            concept.setContext(context);
                        }
                        if (concept.getCoveredText().trim().length() > 0) {
                            concept.addToIndexes(sysJCas);
                        }
                        previousConcept = concept;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mm.checkin(mmi);
    }

    /**
     * This method sets parameters to assigned values with default values as a fallback.
     *
     * @param context The UimaContext supplied during initialization.
     * @param paramNames The list of parameter (field) names to assign.
     * @throws ResourceInitializationException Thrown when a field does not exists or has permission errors.
     */
    private void setStringParams(UimaContext context, String... paramNames) throws ResourceInitializationException {
        for (String paramName : paramNames) {
            String paramValue = (String) context.getConfigParameterValue(paramName);

            // If no paramValue supplied to context, fallback to default value
            if (paramValue == null) return;

            // A paramValue was supplied, so assign supplied value to the field.
            Field paramField;
            try {
                paramField = getClass().getDeclaredField(paramName);
                paramField.set(this, paramValue);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                String msg = String.format("Unable to assign to field, %s, exists in MetaMapAE. ", paramName);
                logger.log(Level.SEVERE, msg + e.getMessage());
                throw new ResourceInitializationException(e);
            }
        }
    }
}
