package edu.umn.biomedicus.mapping;

import edu.umn.biomedicus.mapping.metamap.MetaMapConnectionException;
import edu.umn.biomedicus.mapping.metamap.MetaMapProcessException;
import gov.nih.nlm.nls.metamap.MetaMapApi;
import gov.nih.nlm.nls.metamap.MetaMapApiImpl;
import org.apache.uima.ResourceSpecifierFactory;
import org.apache.uima.UIMAFramework;
import org.apache.uima.impl.ResourceSpecifierFactory_impl;
import org.apache.uima.resource.*;
import org.apache.uima.resource.impl.DataResource_impl;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;


/**
 * This class manages launching the mmserver connections.
 */
public class MMServerConnection implements SharedResourceObject {

    Map<Integer, MetaMapInstance> connectionPool = new HashMap<>();
    Map<Integer, Boolean> checkedOut = new HashMap<>();

    int timeout = 600000;
    int connections = 12;
    String[] ports;

    @NotNull
    private String metaMapServer;
    @NotNull
    private String metaMapConfig;


    public void load(DataResource resource) throws ResourceInitializationException
    {
        // Get metamap options from the metamap properties file
        Properties props = getMetamapProperties(resource);
        metaMapServer = props.getProperty("server");
        metaMapConfig = props.getProperty("options");
        ports = props.getProperty("ports").split(",");

        String metaMapHome = System.getenv("METAMAP_HOME");
        File metaMapHomeDirectory = new File(metaMapHome);

        if (metaMapHome == null | !metaMapHomeDirectory.exists())
        {
            String mesg = "METAMAP_HOME environment variable is not set. Exiting.";
            throw new ResourceInitializationException(new Throwable(mesg));
        }

        // distribute connections in the pool across the available ports that have mmserver running
        int i = 0;
        while (connectionPool.size() < connections) {
            for (String portString : ports) {
                // setup process
                int port = Integer.valueOf(portString);

                // Could launch mmserver on each given port here, but this needs improved checking
                // for already running services and for closing down services. On hold for now...
                //            String mmCommandString = String.format("%s/bin/mmserver13", metaMapHome);
                //            CommandLine cmdLine = new CommandLine(mmCommandString);
                //            cmdLine.addArgument(portString);
                //            CommandLauncher launcher = CommandLauncherFactory.createVMLauncher();
                //            Process p = launcher.exec(cmdLine, new HashMap<>());
                //            Thread.sleep(2000); // wait for the services to fully start up
                //            if (p.isAlive()) {
                //                processPool.put(port, p);

                // Make connection to an ALREADY RUNNING mmserver
                MetaMapApi api = getConnection(port);
                if (api == null) continue;
                MetaMapInstance mmInstance = new MetaMapInstance(port, api);
                connectionPool.put(i, mmInstance);
                i++;
            }
        }
    }

    private Properties getMetamapProperties(DataResource resource) throws ResourceInitializationException
    {
        Properties props = new Properties();
        try
        {
            props.load(resource.getInputStream());
        } catch (IOException e)
        {
            throw new ResourceInitializationException(e);
        }
        return props;
    }

    public synchronized MetaMapInstance checkout() throws MetaMapProcessException {
        MetaMapInstance availableConnection = null;
        for (int i : connectionPool.keySet())
        {
            Boolean co = checkedOut.get(i);
            if (co != null && co) continue;
        }
        int index = new Random().nextInt(connectionPool.size());
        checkedOut.put(index, true);
        return connectionPool.get(index);
    }

    public synchronized void checkin(MetaMapInstance mm)
    {

    }

    public MetaMapApi getConnection(int metaMapPort) throws ResourceInitializationException
    {
        // return a connection instance
        MetaMapApi api = null;
        try {
            api = new MetaMapApiImpl(metaMapServer, metaMapPort);
            api.setOptions(metaMapConfig);
            api.setTimeout(600000); // 10 minute timeout
        } catch (Exception e) {
            throw new ResourceInitializationException(e);
        }
        return api;
    }

    public void destroy() {
        for (MetaMapInstance mm : connectionPool.values())
            mm.getApi().disconnect();
    }

//    public static MMServerConnection build() throws ResourceInitializationException
//    {
//        return build("edu/umn/biomedicus/mapping/metamap/metamap.properties");
//    }
//
//    public static MMServerConnection build(String dataFilePath) throws ResourceInitializationException
//    {
//        // get a blank resource specifier
//        FileResourceSpecifier dataResourceFile = UIMAFramework.getResourceSpecifierFactory().createFileResourceSpecifier();
//
//        // get the url for the mm properties file
//        URL resourceURL = MMServerConnection.class.getClassLoader().getResource(dataFilePath);
//        dataResourceFile.setFileUrl(resourceURL.toString());
//
//        // create the data resource
//        DataResource resource = (DataResource) UIMAFramework.produceResource(
//                dataResourceFile, new HashMap<String, Object>());
//
//        // build the connector
//        MMServerConnection mm = new MMServerConnection();
//        mm.load(resource);
//        return mm;
//    }
}
