package edu.umn.biomedicus.mapping;

import edu.umn.biomedicus.type.Token;

import java.util.HashSet;

public class CandidateTerm {

	Token token;
	HashSet<String> cuis;

	public CandidateTerm(Token token, HashSet<String> cuis) {
		this.token = token;
		if (cuis == null) {
			this.cuis = new HashSet<String>();
		} else {
			this.cuis = cuis;
		}
	}

	public Token getToken() {
		return this.token;
	}

	public void setToken(Token tok) {
		this.token = tok;
	}

	public HashSet<String> getCuis() {
		return this.cuis;
	}

	public void setCuis(HashSet<String> cuis) {
		if (cuis == null) {
			this.cuis = new HashSet<String>();
		} else {
			this.cuis = cuis;
		}
	}

	public String getCoveredText() {
		return this.token.getNorm();
	}

	/**
	 * This method returns summary information for debuggin/devel.
	 * 
	 * @return summary info
	 */
	public String getTermSummary() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getCoveredText());
		sb.append(" (");
		if (this.cuis.size() > 2) {
			sb.append(this.cuis.size());
			sb.append(" cuis");
		} else {
			sb.append(this.cuis);
		}
		sb.append(")");
		return sb.toString();
	}
}
