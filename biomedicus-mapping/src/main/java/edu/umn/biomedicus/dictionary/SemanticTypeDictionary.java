package edu.umn.biomedicus.dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.uima.pear.util.FileUtil;

/**
 * This class reads a file (comma-separated values) where each line has a
 * semantic type abbreviation for the first value and the long form of the
 * semantic type in the second field. The file is normally created by exporting
 * from UMLS with the SLQ: "SELECT DISTINCT(ABR), STY_RL from SRDEF;"
 * 
 * Using UMLS directly is fine; however, this is a lightweight approach that is
 * handy.
 * 
 */
public class SemanticTypeDictionary {
	protected HashMap<String, String> lookup = new HashMap<String, String>();

	public SemanticTypeDictionary(InputStream inputStream) throws IOException {
		String[] rows = FileUtil.loadListOfStrings(new BufferedReader(new InputStreamReader(inputStream)));
		for (String row : rows) {
			String[] entry = row.split(",");
			String abbreviation = entry[0].trim();
			String longForm = entry[1].trim();
			lookup.put(abbreviation, longForm);
		}
	}

	public String getLongForm(String abbreviation) {
		return lookup.get(abbreviation);
	}
}
