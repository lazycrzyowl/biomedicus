//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.02 at 09:16:21 AM CST 
//


package edu.umn.biomedicus.mapping.metamap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "candidateScore",
    "candidateCUI",
    "candidateMatched",
    "candidatePreferred",
    "matchedWords",
    "semTypes",
    "matchMaps",
    "isHead",
    "isOverMatch",
    "sources",
    "conceptPIs",
    "status"
})
@XmlRootElement(name = "Candidate")
public class Candidate {

    @XmlElement(name = "CandidateScore", required = true)
    protected String candidateScore;
    @XmlElement(name = "CandidateCUI", required = true)
    protected String candidateCUI;
    @XmlElement(name = "CandidateMatched", required = true)
    protected String candidateMatched;
    @XmlElement(name = "CandidatePreferred", required = true)
    protected String candidatePreferred;
    @XmlElement(name = "MatchedWords", required = true)
    protected MatchedWords matchedWords;
    @XmlElement(name = "SemTypes", required = true)
    protected SemTypes semTypes;
    @XmlElement(name = "MatchMaps", required = true)
    protected MatchMaps matchMaps;
    @XmlElement(name = "IsHead", required = true)
    protected String isHead;
    @XmlElement(name = "IsOverMatch", required = true)
    protected String isOverMatch;
    @XmlElement(name = "Sources", required = true)
    protected Sources sources;
    @XmlElement(name = "ConceptPIs", required = true)
    protected ConceptPIs conceptPIs;
    @XmlElement(name = "Status", required = true)
    protected String status;

    /**
     * Gets the value of the candidateScore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCandidateScore() {
        return candidateScore;
    }

    /**
     * Sets the value of the candidateScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCandidateScore(String value) {
        this.candidateScore = value;
    }

    /**
     * Gets the value of the candidateCUI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCandidateCUI() {
        return candidateCUI;
    }

    /**
     * Sets the value of the candidateCUI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCandidateCUI(String value) {
        this.candidateCUI = value;
    }

    /**
     * Gets the value of the candidateMatched property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCandidateMatched() {
        return candidateMatched;
    }

    /**
     * Sets the value of the candidateMatched property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCandidateMatched(String value) {
        this.candidateMatched = value;
    }

    /**
     * Gets the value of the candidatePreferred property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCandidatePreferred() {
        return candidatePreferred;
    }

    /**
     * Sets the value of the candidatePreferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCandidatePreferred(String value) {
        this.candidatePreferred = value;
    }

    /**
     * Gets the value of the matchedWords property.
     * 
     * @return
     *     possible object is
     *     {@link MatchedWords }
     *     
     */
    public MatchedWords getMatchedWords() {
        return matchedWords;
    }

    /**
     * Sets the value of the matchedWords property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchedWords }
     *     
     */
    public void setMatchedWords(MatchedWords value) {
        this.matchedWords = value;
    }

    /**
     * Gets the value of the semTypes property.
     * 
     * @return
     *     possible object is
     *     {@link SemTypes }
     *     
     */
    public SemTypes getSemTypes() {
        return semTypes;
    }

    /**
     * Sets the value of the semTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link SemTypes }
     *     
     */
    public void setSemTypes(SemTypes value) {
        this.semTypes = value;
    }

    /**
     * Gets the value of the matchMaps property.
     * 
     * @return
     *     possible object is
     *     {@link MatchMaps }
     *     
     */
    public MatchMaps getMatchMaps() {
        return matchMaps;
    }

    /**
     * Sets the value of the matchMaps property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchMaps }
     *     
     */
    public void setMatchMaps(MatchMaps value) {
        this.matchMaps = value;
    }

    /**
     * Gets the value of the isHead property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHead() {
        return isHead;
    }

    /**
     * Sets the value of the isHead property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHead(String value) {
        this.isHead = value;
    }

    /**
     * Gets the value of the isOverMatch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOverMatch() {
        return isOverMatch;
    }

    /**
     * Sets the value of the isOverMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOverMatch(String value) {
        this.isOverMatch = value;
    }

    /**
     * Gets the value of the sources property.
     * 
     * @return
     *     possible object is
     *     {@link Sources }
     *     
     */
    public Sources getSources() {
        return sources;
    }

    /**
     * Sets the value of the sources property.
     * 
     * @param value
     *     allowed object is
     *     {@link Sources }
     *     
     */
    public void setSources(Sources value) {
        this.sources = value;
    }

    /**
     * Gets the value of the conceptPIs property.
     * 
     * @return
     *     possible object is
     *     {@link ConceptPIs }
     *     
     */
    public ConceptPIs getConceptPIs() {
        return conceptPIs;
    }

    /**
     * Sets the value of the conceptPIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptPIs }
     *     
     */
    public void setConceptPIs(ConceptPIs value) {
        this.conceptPIs = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
